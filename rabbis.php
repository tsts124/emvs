<?php

	include 'includes/header.php'; 
	include 'includes/dbcon.php';

	if($_SESSION['user']==''){

		header('Location: emvs.php?action=index');
	}

	if (isset($_POST['action']) && ($_POST['action'] == "update")) {

        //echo $_POST['hid'];
		$upquery=$dbh->prepare("update newmember set activity='" . $_POST['act'] . "' where Id='" . $_POST['hid'] ."'");
		$upquery->execute();
	}

	if(isset($_POST['action'])&&($_POST['action']=="delete")) {

		$delquery=$dbh->prepare("UPDATE `newmember` SET `Delete`='1',`status`='Denied' WHERE Id=".$_POST['hid']."");
		$delquery->execute();
	}
    if(isset($_POST['action'])&&($_POST['action']=="multipleDeletion")){

        $checkboxes = explode(",",$_POST['hid']);

        for($i=0;$i<count($checkboxes);$i++) {

            $itemToDelete = $checkboxes[$i];
            $delquery=$dbh->prepare("UPDATE `newmember` SET `Delete`='1',`status`='Denied' WHERE Id=".$itemToDelete."");
            $delquery->execute();
        }
    }
?>
<script>
    function fnactive(id) {
        //alert(id);
		if (document.getElementById('active' + id).checked == true)
			document.getElementById('act').value = 1;
		else
			document.getElementById('act').value = 0;
		document.getElementById('action').value = "update";
		document.getElementById('hid').value = id;
		document.rabbis.submit();
	}
	function fndelete(id) {
		if( confirm("Do you really wish to delete this record?") ) {
				document.getElementById('action').value = "delete";
				document.getElementById('hid').value = id;
				document.rabbis.submit();
        }
	}

    function actMultipleDeletion(){

        var allVals = [];
        $.each($("input[name=rabbisMultipleDelete]:checked"), function(){
            allVals.push($(this).val());
        });
        if(allVals.length > 0) {
            document.getElementById('action').value = "multipleDeletion";
            document.getElementById('hid').value = allVals;
            document.rabbis.submit();
        }else {
            alert("It is required to select at least one entry to execute multiple deletion");
        }
    }

</script>
<style>
    .first {
        display:none;
    }
    .second {
        display:show;
    }
    #tooltip {
        text-align: center;
        color: #fff;
        background: #111;
        position: absolute;
        z-index: 100;
        padding: 15px;
    }
 
    #tooltip:after { /* triangle decoration */
        width: 0;
        height: 0;
        border-left: 10px solid transparent;
        border-right: 10px solid transparent;
        border-top: 10px solid #111;
        content: '';
        position: absolute;
        left: 50%;
        bottom: -10px;
        margin-left: -10px;
    }
 
    #tooltip.top:after {
        border-top-color: transparent;
        border-bottom: 10px solid #111;
        top: -20px;
        bottom: auto;
    }

    #tooltip.left:after {
        left: 10px;
        margin: 0;
    }

    #tooltip.right:after {
        right: 10px;
        left: auto;
        margin: 0;
    }
</style>

<script>

    $( function() {

        var targets = $( '[rel~=tooltip]' ),
            target  = false,
            tooltip = false,
            title   = false;

        targets.bind( 'mouseenter',
            function() {

                target  = $( this );
                tip     = target.attr( 'title' );
                tooltip = $( '<div id="tooltip"></div>' );

                if( !tip || tip == '' )
                    return false;

                target.removeAttr( 'title' );
                tooltip.css( 'opacity', 0 )
                       .html( tip )
                       .appendTo( 'body' );

                var init_tooltip = function() {

                    if( $( window ).width() < tooltip.outerWidth() * 1.5 )
                        tooltip.css( 'max-width', $( window ).width() / 2 );
                    else
                        tooltip.css( 'max-width', 340 );

                    var pos_left = target.offset().left + ( target.outerWidth() / 2 ) - ( tooltip.outerWidth() / 2 ),
                        pos_top  = target.offset().top - tooltip.outerHeight() - 20;

                    if( pos_left < 0 ) {

                        pos_left = target.offset().left + target.outerWidth() / 2 - 20;
                        tooltip.addClass( 'left' );
                    } else
                        tooltip.removeClass( 'left' );

                    if( pos_left + tooltip.outerWidth() > $( window ).width() ) {

                        pos_left = target.offset().left - tooltip.outerWidth() + target.outerWidth() / 2 + 20;
                        tooltip.addClass( 'right' );
                    } else
                        tooltip.removeClass( 'right' );

                    if( pos_top < 0 ) {

                        var pos_top  = target.offset().top + target.outerHeight();
                        tooltip.addClass( 'top' );
                    } else
                        tooltip.removeClass( 'top' );

                    tooltip.css( { left: pos_left, top: pos_top } )
                           .animate( { top: '+=10', opacity: 1 }, 50 );
                };

                init_tooltip();
                $( window ).resize( init_tooltip );

                var remove_tooltip = function() {

                    tooltip.animate( { top: '-=10', opacity: 0 }, 50,
                        function() {
                            $( this ).remove();
                        }
                    );

                    target.attr( 'title', tip );
                };

                target.bind( 'mouseleave', remove_tooltip );
                tooltip.bind( 'click', remove_tooltip );
            }
        );
    });
</script>
<script>
    $(document).ready(
        function(){
            $('[data-toggle="tooltip"]').tooltip();
        }
    );
</script>
<section>
    <div class="mainwrapper">
        <?php include 'includes/leftpanel.php'; ?>
            <div class="mainpanel">
                <div class="pageheader">
                    <div class="media">
                        <div class="media-body">
                            <h4>Manager</h4>
                        </div>
                    </div><!-- media -->
                </div><!-- pageheader -->
                <div class="contentpanel">
                    <div class="panel panel-primary-head">
                        <!--   <div class="panel-heading">
                            <a href="emvs.php?action=addrabbis" class="btn btn-warning">Add New Manager</a>
                        </div>panel-heading -->
                        <p></p>
                        <p class="mb20"></p>
                        <form name="rabbis" id="rabbis" method="post">
                        <table id="basicTable" class="basic-table table table-striped table-bordered responsive">
                            <thead class="">
                                <tr>
                                    <th style="text-align:center;">S.No</th>
                                    <th style="text-align:center">Last&nbsp;Name,&nbsp;Names</th>
                                    <th style="text-align:center;">Phone No</th>
                                    <th style="text-align:center;">Email</th>
                                    <th style="text-align:center;">Sites</th>
                                    <th style="text-align:center;">Active</th>
                                    <th style="text-align:center;">Edit</th>
                                    <th style="text-align:center;">Delete</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    #echo "select * from rabbis order by Id desc";
                                    $sql = $dbh->prepare("select * from newmember where roles='manager' && `Delete`='0' order by Lastname asc");
                                    $sql->execute();
                                    $count = $sql->rowCount();
                                    $i=1;
                                    while($data = $sql->fetch()){
                                ?>
                                <tr>
                                    <td><?=$i;?></td>
                                    <td>
                                        <label style="visibility:hidden;position: absolute;"><?=$data['Lastname'];?></label>
                                        <?=$data['Title'].'  '.$data['Lastname'].', '.$data['Firstname'].' '.$data2['middlename'];?>
                                    </td>
                                    <td>
                                        <?php
                                            $mid = $data['Id'];
                                            $sqlquery = $dbh->prepare("select * from managerphone where D_id ='$mid' && DPrimary!=' '");
                                            $sqlquery->execute();
                                            $dataquery1 = $sqlquery->fetch();
                                            echo $dataquery1['Phone_Number'];
                                        ?>
                                    </td>
                                    <td>
                                        <?=$data['emailid'];?>
                                    </td>
                                    <td>
                                        <?php $s = $data['Sites'];

                                            $sqlq = $dbh->prepare("select * from sites WHERE `Id`='$s'");
                                            $sqlq->execute();
                                            $dataq = $sqlq->fetch();
                                            echo $dataq['Sites'];

                                        ?>
                                    </td>
                                    <td>
                                        <input autocorrect="off"  type="checkbox" value="1" name="active" id="active<?=$data['Id']; ?>" onclick="fnactive(<?=$data['Id']; ?>)" <?php if ($data['activity'] == 1)echo "checked";?>/>
                                        <label for="active<?=$data['Id']; ?>"></label>
                                    </td>
                                    <td>
                                        <a href="emvs.php?action=addrabbis&&e=<?=base64_encode($data['Id']);?>">
                                            <img src="images/edit1.png" data-toggle="tooltip" title="Edit" border="0" alt="EMVS">
                                        </a>
                                    </td>
                                    <td>
                                        <a href="javascript:fndelete(<?=$data['Id'];?>)">
                                            <img src="images/delete1.gif" border="0" data-toggle="tooltip" title="Delete" alt="EMVS">
                                        </a>
                                        <input autocorrect="off"  type="checkbox" id="rabbisMultipleDelete<?=$data['Id'];?>"
                                               name="rabbisMultipleDelete" data-toggle="tooltip"
                                               title="Multiple Deletion" value="<?=$data['Id'];?>" style="float:right;"/>
                                    </td>
                                </tr>
                                <?php $i++; } ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="8">
                                        <input autocorrect="off"  type="button" value="Multiple Deletion" class="btn btn-primary" style="float: right;" onclick="actMultipleDeletion();"/>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                        <input autocorrect="off"  type="hidden" name="action" id="action" />
                        <input autocorrect="off"  type="hidden" name="hid" id="hid" />
                        <input autocorrect="off"  type="hidden" name="act" id="act" />
                    </form>
                </div><!-- panel -->
            </div><!-- contentpanel -->
        </div><!-- mainpanel -->
    </div><!-- mainwrapper -->
</section>

<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/jquery-migrate-1.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/modernizr.min.js"></script>
<script src="js/ppace.min.js"></script>

<script src="js/jquery.cookies.js"></script>

<script src="js/jquery.dataTables.min.js"></script>
<script src="js/dataTables.bootstrap.js"></script>
<script src="js/dataTables.responsive.js"></script>
<script src="js/select2.min.js"></script>

<script src="js/custom.js"></script>
<script>

    jQuery(document).ready(function(){

        jQuery('#basicTable').DataTable({
            responsive: true
        });

        var shTable = jQuery('#shTable').DataTable({
            "fnDrawCallback": function(oSettings) {
                jQuery('#shTable_paginate ul').addClass('pagination-active-dark');
            },
            responsive: true
        });

        // Show/Hide Columns Dropdown
        jQuery('#shCol').click(function(event){
            event.stopPropagation();
        });

        jQuery('#shCol input').on('click', function() {

            // Get the column API object
            var column = shTable.column($(this).val());

            // Toggle the visibility
            if ($(this).is(':checked'))
                column.visible(true);
            else
                column.visible(false);
        });

        var exRowTable = jQuery('#exRowTable').DataTable({
            responsive: true,
            "fnDrawCallback": function(oSettings) {
                jQuery('#exRowTable_paginate ul').addClass('pagination-active-success');
            },
            "ajax": "ajax/objects.txt",
            "columns": [
                {
                    "class":          'details-control',
                    "orderable":      false,
                    "data":           null,
                    "defaultContent": ''
                },
                { "data": "name" },
                { "data": "position" },
                { "data": "office" },
                { "data": "salary" }
            ],
            "order": [[1, 'asc']]
        });

        // Add event listener for opening and closing details
        jQuery('#exRowTable tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = exRowTable.row( tr );

            if ( row.child.isShown() ) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            }
            else {
                // Open this row
                row.child( format(row.data()) ).show();
                tr.addClass('shown');
            }
        });


        // DataTables Length to Select2
        jQuery('div.dataTables_length select').removeClass('form-control input-sm');
        jQuery('div.dataTables_length select').css({width: '60px'});
        jQuery('div.dataTables_length select').select2({
            minimumResultsForSearch: -1
        });

    });

    function format (d) {
        // `d` is the original data object for the row
        return '<table class="table table-bordered nomargin">'+
            '<tr>'+
                '<td>Full name:</td>'+
                '<td>'+d.name+'</td>'+
            '</tr>'+
            '<tr>'+
                '<td>Extension number:</td>'+
                '<td>'+d.extn+'</td>'+
            '</tr>'+
            '<tr>'+
                '<td>Extra info:</td>'+
                '<td>And any further details here (images etc)...</td>'+
            '</tr>'+
        '</table>';
    }
</script>

    </body>
</html>
