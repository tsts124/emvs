<?php
include 'includes/dbcon.php';

$number = $_POST['number'];

$sql = $dbh->prepare('SELECT *
                      FROM `collectorphone`
                      WHERE `Phone_Number` = :phonenumber
                      AND `delete` = 0 ;');
$sql->execute(['phonenumber' => $number]);

$duplicate1 = $sql->fetch();

$sql = $dbh->prepare('SELECT *
                      FROM `visit_details`
                      WHERE `detail_number` = :phonenumber
                      AND `detail_type` = :detailtype
                      AND `delete` = 0 ;');
$sql->execute(['phonenumber' => $number, 'detailtype' => 'contact']);

$duplicate2 = $sql->fetch();

if ($duplicate1 || $duplicate2) {
    echo 'error';
} else {
    echo 'ok';
}

