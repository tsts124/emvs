<?php 
	include 'includes/header.php'; 
	if($_SESSION['user']==''){
		header('Location: emvs.php?action=index');
	}
?>
        <section>
            <div class="mainwrapper">
                  <?php include 'includes/leftpanel.php'; ?>
                
                
                
                
                <div class="mainpanel">
                    <div class="pageheader">
                        <div class="media">                            
                            <div class="media-body">                               
                                <h4>Screen Designs</h4>
                            </div>
                        </div><!-- media -->
                    </div><!-- pageheader -->
                    
                    <div class="contentpanel">                        
                        <div class="panel-group" id="accordion2">
                                    <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion2" href="#collapseOne2" class="collapsed">
                                                    Screen 1 - Search
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseOne2" class="panel-collapse collapse" style="height: 0px;">
                                            <div class="panel-body">						
								
								<div class="col-md-6">
                                <form id="basicForm" action="#" novalidate="novalidate">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <div class="panel-btns" style="display: none;">
                                            <a href="" class="panel-minimize tooltips" data-toggle="tooltip" title="" data-original-title="Minimize Panel"><i class="fa fa-minus"></i></a>
                                            <a href="" class="panel-close tooltips" data-toggle="tooltip" title="" data-original-title="Close Panel"><i class="fa fa-times"></i></a>
                                        </div><!-- panel-btns -->                                       
                                    </div><!-- panel-heading -->
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">First Name <span class="asterisk">*</span></label>
                                                <div class="col-sm-9">
                                                    <input autocorrect="off"  type="text" name="name" class="form-control" placeholder="Type your name..." required="">
                                                </div>
                                            </div><!-- form-group -->
                                          
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Middle Name <span class="asterisk">*</span></label>
                                                <div class="col-sm-9">
                                                    <input autocorrect="off"  type="text" name="name" class="form-control" placeholder="Type your name..." required="">
                                                </div>
                                            </div><!-- form-group -->
                                          
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Last Name <span class="asterisk">*</span></label>
                                                <div class="col-sm-9">
                                                    <input autocorrect="off"  type="text" name="name" class="form-control" placeholder="Type your name..." required="">
                                                </div>
                                            </div><!-- form-group -->
                                            
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Document # <span class="asterisk">*</span></label>
                                                <div class="col-sm-9">
                                                    <input autocorrect="off"  type="text" name="name" class="form-control" placeholder="Type your name..." required="">
                                                </div>
                                            </div><!-- form-group -->
											
                                        </div><!-- row -->
                                    </div><!-- panel-body -->
                                    <div class="panel-footer">
                                      <div class="row">
                                        <div class="col-sm-9 col-sm-offset-3">
                                            <form class="form form-search" action="search-results.html">
                            <input autocorrect="off"  type="search" class="form-control" placeholder="Search">
                        </form>
                                        </div>
                                      </div>
                                    </div><!-- panel-footer -->  
                                </div><!-- panel -->
                                </form>
                                
                            </div>
							
							
								<div class="col-md-6">
                                <form id="basicForm2" action="#" novalidate="novalidate">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <div class="panel-btns" style="display: none;">
                                            <a href="" class="panel-minimize tooltips" data-toggle="tooltip" title="" data-original-title="Minimize Panel"><i class="fa fa-minus"></i></a>
                                            <a href="" class="panel-close tooltips" data-toggle="tooltip" title="" data-original-title="Close Panel"><i class="fa fa-times"></i></a>
                                        </div><!-- panel-btns -->
                                    </div><!-- panel-heading -->
                                    <div class="panel-body">
                                        <div class="errorForm"></div>
                                        <div class="row">
                                                                                        
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Comment <span class="asterisk">*</span></label>
                                                <div class="col-sm-9">
                                                    <textarea autocorrect="off"  rows="5" class="form-control" name="comment" title="Please type a comment at least 6 characters long!" placeholder="Type your comment..." required=""></textarea>
                                                </div>
                                            </div><!-- form-group -->
                                        </div><!-- row -->
                                    </div><!-- panel-body -->
                                    <div class="panel-footer">
                                        <div class="row">
                                            <div class="col-sm-9 col-sm-offset-3"><br><br><br><br><br>
                                                <button class="btn btn-primary mr5">Next</button>
                                            </div>
                                        </div>
                                    </div><!-- panel-footer -->
                                </div><!-- panel -->
                                </form>
                        
                            </div>
							</div>
                          </div>
                                    </div><!-- panel -->
                                    
                                    <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" class="collapsed" data-parent="#accordion2" href="#collapseTwo2">
                                                    Screen 2 - Personal Information Data
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseTwo2" class="panel-collapse collapse">
                                        
										<div class="panel-body">
                                <div class="col-md-6">
                                <form id="basicForm" action="#" novalidate="novalidate">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <div class="panel-btns" style="display: none;">
                                            <a href="" class="panel-minimize tooltips" data-toggle="tooltip" title="" data-original-title="Minimize Panel"><i class="fa fa-minus"></i></a>
                                            <a href="" class="panel-close tooltips" data-toggle="tooltip" title="" data-original-title="Close Panel"><i class="fa fa-times"></i></a>
                                        </div><!-- panel-btns -->
                                    </div><!-- panel-heading -->
                                    <div class="panel-body">
                                         <div class="row">
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">First Name <span class="asterisk">*</span></label>
                                                <div class="col-sm-9">
                                                    <input autocorrect="off"  type="text" name="name" class="form-control" placeholder="Type your name..." required="">
                                                </div>
                                            </div><!-- form-group -->
											
											<div class="form-group">
                                                <label class="col-sm-3 control-label">Middle Name <span class="asterisk">*</span></label>
                                                <div class="col-sm-9">
                                                    <input autocorrect="off"  type="text" name="name" class="form-control" placeholder="Type your name..." required="">
                                                </div>
                                            </div><!-- form-group -->
                                            
										   <div class="form-group">
                                                <label class="col-sm-3 control-label">Last Name <span class="asterisk">*</span></label>
                                                <div class="col-sm-9">
                                                    <input autocorrect="off"  type="text" name="name" class="form-control" placeholder="Type your name..." required="">
                                                </div>
                                            </div><!-- form-group -->
                                            
                                            
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Street <span class="asterisk">*</span></label>
                                                <div class="col-sm-9">
                                                    <textarea autocorrect="off"  rows="5" class="form-control" placeholder="Type your comment..." required=""></textarea>
                                                </div>
                                            </div><!-- form-group -->
                                        </div><!-- row -->
                                    </div><!-- panel-body -->
                                    <div class="panel-footer">
                                      <div class="row">
                                        <div class="col-sm-9 col-sm-offset-3"><br><br>
                                            <button class="btn btn-primary mr5">Prev</button>
                                        </div>
                                      </div>
                                    </div><!-- panel-footer -->  
                                </div><!-- panel -->
                                </form>
                                
                            </div>
							
							<div class="col-md-6">
                                <form id="basicForm" action="#" novalidate="novalidate">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <div class="panel-btns" style="display: none;">
                                            <a href="" class="panel-minimize tooltips" data-toggle="tooltip" title="" data-original-title="Minimize Panel"><i class="fa fa-minus"></i></a>
                                            <a href="" class="panel-close tooltips" data-toggle="tooltip" title="" data-original-title="Close Panel"><i class="fa fa-times"></i></a>
                                        </div><!-- panel-btns -->                                      
                                    </div><!-- panel-heading -->
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">City <span class="asterisk">*</span></label>
                                                <div class="col-sm-9">
                                                    <input autocorrect="off"  type="text" name="name" class="form-control" placeholder="Type your name..." required="">
                                                </div>
                                            </div><!-- form-group -->
											
											<div class="form-group">
                                                <label class="col-sm-3 control-label">State <span class="asterisk">*</span></label>
                                                <div class="col-sm-9">
                                                    <input autocorrect="off"  type="text" name="name" class="form-control" placeholder="Type your name..." required="">
                                                </div>
                                            </div><!-- form-group -->
                                            
										   <div class="form-group">
                                                <label class="col-sm-3 control-label">ZIP <span class="asterisk">*</span></label>
                                                <div class="col-sm-9">
                                                    <input autocorrect="off"  type="text" name="name" class="form-control" placeholder="Type your name..." required="">
                                                </div>
                                            </div><!-- form-group -->
											
											<div class="form-group">
                                                <label class="col-sm-3 control-label">Home Phone <span class="asterisk">*</span></label>
                                                <div class="col-sm-9">
                                                    <input autocorrect="off"  type="text" name="name" class="form-control" placeholder="Type your name..." required="">
                                                </div>
                                            </div><!-- form-group -->
											
											<div class="form-group">
                                                <label class="col-sm-3 control-label">Document #<span class="asterisk">*</span></label>
                                                <div class="col-sm-9">
                                                    <input autocorrect="off"  type="text" name="name" class="form-control" placeholder="Type your name..." required="">
                                                </div>
                                            </div><!-- form-group -->
                                            
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Document Country  <span class="asterisk">*</span></label>
                                                <div class="col-sm-9">
                                                    <input autocorrect="off"  type="text" name="name" class="form-control" placeholder="Type your name..." required="">
                                                </div>
                                            </div><!-- form-group -->
											
                                        </div><!-- row -->
                                    </div><!-- panel-body -->
                                    <div class="panel-footer">
                                      <div class="row">
                                        <div class="col-sm-9 col-sm-offset-3">
                                            <button class="btn btn-primary mr5">Next</button>
                                        </div>
                                      </div>
                                    </div><!-- panel-footer -->  
                                </div><!-- panel -->
                                </form>
                                
                            </div>
							
                           </div><!-- panel-body -->
				        </div>
                     </div><!-- panel -->
                                    
                                    <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" class="collapsed" data-parent="#accordion2" href="#collapseThree2">
                                                    Screen 3 - Trip Information
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseThree2" class="panel-collapse collapse">
                                            <div class="panel-body">
                                        <div class="row">
                                            <div class="col-sm-6">
                                               <div class="form-group">
                                                <label class="col-sm-3 control-label">US Street <span class="asterisk">*</span></label>
                                                <div class="col-sm-9">
                                                    <textarea autocorrect="off"  rows="5" class="form-control" placeholder="Type your comment..." required=""></textarea>
                                                </div>
                                            </div>
                                            </div><!-- col-sm-6 -->
                                            
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label">US City</label>
                                                    <input autocorrect="off"  type="text" name="lastname" class="form-control">
                                                </div><!-- form-group -->
                                            </div><!-- col-sm-6 -->
                                        </div><!-- row -->
                                  
                                        <div class="row">
                                            <div class="col-sm-6">
                                               <div class="form-group">
                                            <label class="col-sm-3 control-label">US State</label>
                                            <div class="col-sm-9">
                                                <div class="select2-container width100p" id="s2id_fruits"><a href="javascript:void(0)" class="select2-choice" tabindex="-1">  
												<span class="select2-chosen" id="select2-chosen-1">Choose One</span><abbr class="select2-search-choice-close"></abbr> 
												<span class="select2-arrow" role="presentation"><b role="presentation"></b></span></a><label for="s2id_autogen1" class="select2-offscreen"></label>
												<input autocorrect="off"  class="select2-focusser select2-offscreen" type="text" aria-haspopup="true" role="button" aria-labelledby="select2-chosen-1" id="s2id_autogen1"></div>
												<select id="fruits" data-placeholder="Choose One" class="width100p select2-offscreen" required="" tabindex="-1" title="">
                                                    <option value="">Choose One</option>
                                                    <option value="apple">Apple</option>
                                                    <option value="orange">Orange</option>
                                                    <option value="grapes">Grapes</option>
                                                    <option value="strawberry">Strawberry</option>
                                                </select>
                                                <label class="error" for="fruits"></label>
                                            </div>
                                        </div>
										
										<div class="form-group">
                                            <label class="col-sm-3 control-label">Host</label>
                                            <div class="col-sm-9">
                                                <div class="select2-container width100p" id="s2id_fruits"><a href="javascript:void(0)" class="select2-choice" tabindex="-1">  
												<span class="select2-chosen" id="select2-chosen-1">Choose One</span><abbr class="select2-search-choice-close"></abbr> 
												<span class="select2-arrow" role="presentation"><b role="presentation"></b></span></a><label for="s2id_autogen1" class="select2-offscreen"></label>
												<input autocorrect="off"  class="select2-focusser select2-offscreen" type="text" aria-haspopup="true" role="button" aria-labelledby="select2-chosen-1" id="s2id_autogen1"></div>
												<select id="fruits" data-placeholder="Choose One" class="width100p select2-offscreen" required="" tabindex="-1" title="">
                                                    <option value="">Choose One</option>
                                                    <option value="apple">Apple</option>
                                                    <option value="orange">Orange</option>
                                                    <option value="grapes">Grapes</option>
                                                    <option value="strawberry">Strawberry</option>
                                                </select>
                                                <label class="error" for="fruits"></label>
                                            </div>
                                        </div>
										
										<div class="form-group">
                                            <label class="col-sm-3 control-label">Driver</label>
                                            <div class="col-sm-9">
                                                <div class="select2-container width100p" id="s2id_fruits"><a href="javascript:void(0)" class="select2-choice" tabindex="-1">  
												<span class="select2-chosen" id="select2-chosen-1">Choose One</span><abbr class="select2-search-choice-close"></abbr> 
												<span class="select2-arrow" role="presentation"><b role="presentation"></b></span></a><label for="s2id_autogen1" class="select2-offscreen"></label>
												<input autocorrect="off"  class="select2-focusser select2-offscreen" type="text" aria-haspopup="true" role="button" aria-labelledby="select2-chosen-1" id="s2id_autogen1"></div>
												<select id="fruits" data-placeholder="Choose One" class="width100p select2-offscreen" required="" tabindex="-1" title="">
                                                    <option value="">Choose One</option>
                                                    <option value="apple">Apple</option>
                                                    <option value="orange">Orange</option>
                                                    <option value="grapes">Grapes</option>
                                                    <option value="strawberry">Strawberry</option>
                                                </select>
                                                <label class="error" for="fruits"></label>
                                            </div>
                                        </div>
                                            </div><!-- col-sm-6 -->
											
											<div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label">US Phone</label>
                                                    <input autocorrect="off"  type="text" name="lastname" class="form-control">
                                                </div><!-- form-group -->
                                            </div><!-- col-sm-6 -->
                                            
                                            
                                        </div><!-- row -->
                                    </div>
									<div class="panel-footer">
                                        <button class="btn btn-primary">Prev</button>
                                    </div>
                                        </div>
                                    </div><!-- panel -->
									
									<div class="panel panel-primary">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" class="collapsed" data-parent="#accordion2" href="#reason">
                                                    Screen 4 - Reason
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="reason" class="panel-collapse collapse">
                                        
										<div class="panel-body">
                                <div class="col-md-6">
                                <form id="basicForm" action="#" novalidate="novalidate">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <div class="panel-btns" style="display: none;">
                                            <a href="" class="panel-minimize tooltips" data-toggle="tooltip" title="" data-original-title="Minimize Panel"><i class="fa fa-minus"></i></a>
                                            <a href="" class="panel-close tooltips" data-toggle="tooltip" title="" data-original-title="Close Panel"><i class="fa fa-times"></i></a>
                                        </div><!-- panel-btns -->
                                    </div><!-- panel-heading -->
                                    <div class="panel-body">
                                         <div class="row">
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">City <span class="asterisk">*</span></label>
                                                <div class="col-sm-9">
                                                    <input autocorrect="off"  type="text" name="name" class="form-control" placeholder="Type your name..." required="">
                                                </div>
                                            </div><!-- form-group -->
											
											<div class="form-group">
                                                <label class="col-sm-3 control-label">State <span class="asterisk">*</span></label>
                                                <div class="col-sm-9">
                                                    <input autocorrect="off"  type="text" name="name" class="form-control" placeholder="Type your name..." required="">
                                                </div>
                                            </div><!-- form-group -->
                                            
										   <div class="form-group">
                                                <label class="col-sm-3 control-label">ZIP <span class="asterisk">*</span></label>
                                                <div class="col-sm-9">
                                                    <input autocorrect="off"  type="text" name="name" class="form-control" placeholder="Type your name..." required="">
                                                </div>
                                            </div><!-- form-group -->
											
											<div class="form-group">
                                                <label class="col-sm-3 control-label">Phone <span class="asterisk">*</span></label>
                                                <div class="col-sm-9">
                                                    <input autocorrect="off"  type="text" name="name" class="form-control" placeholder="Type your name..." required="">
                                                </div>
                                            </div><!-- form-group -->
                                            
                                            
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Street <span class="asterisk">*</span></label>
                                                <div class="col-sm-9">
                                                    <textarea autocorrect="off"  rows="5" class="form-control" placeholder="Type your comment..." required=""></textarea>
                                                </div>
                                            </div><!-- form-group -->
                                        </div><!-- row -->
                                    </div><!-- panel-body -->
                                    <div class="panel-footer">
                                      <div class="row">
                                        <div class="col-sm-9 col-sm-offset-3"><br><br>
                                            <button class="btn btn-primary mr5">Prev</button>
                                        </div>
                                      </div>
                                    </div><!-- panel-footer -->  
                                </div><!-- panel -->
                                </form>
                                
                            </div>
							
							<div class="col-md-6">
                                <form id="basicForm" action="#" novalidate="novalidate">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <div class="panel-btns" style="display: none;">
                                            <a href="" class="panel-minimize tooltips" data-toggle="tooltip" title="" data-original-title="Minimize Panel"><i class="fa fa-minus"></i></a>
                                            <a href="" class="panel-close tooltips" data-toggle="tooltip" title="" data-original-title="Close Panel"><i class="fa fa-times"></i></a>
                                        </div><!-- panel-btns -->                                      
                                    </div><!-- panel-heading -->
                                    <div class="panel-body">
                                        <div class="row">
										
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Purpose of Funds <span class="asterisk">*</span></label>
                                                <div class="col-sm-9">
                                                    <textarea autocorrect="off"  rows="5" class="form-control" placeholder="Type your comment..." required=""></textarea>
                                                </div>
                                            </div><!-- form-group -->
											
                                        </div><!-- row -->
                                    </div><!-- panel-body -->
                                    <div class="panel-footer">
                                      <div class="row">
                                        <div class="col-sm-9 col-sm-offset-3">	
                                            <button class="btn btn-primary mr5">Next</button>
                                        </div>
                                      </div>
                                    </div><!-- panel-footer -->  
                                </div><!-- panel -->
                                </form>
                                
                            </div>
							
                           </div><!-- panel-body -->
				        </div>
                     </div><!-- panel -->
					 
					 <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" class="collapsed" data-parent="#accordion2" href="#reference">
                                                    Screen 5 - References and Comments
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="reference" class="panel-collapse collapse">
                                        
										<div class="panel-body">
                                <div class="col-md-6">
                                <form id="basicForm" action="#" novalidate="novalidate">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <div class="panel-btns" style="display: none;">
                                            <a href="" class="panel-minimize tooltips" data-toggle="tooltip" title="" data-original-title="Minimize Panel"><i class="fa fa-minus"></i></a>
                                            <a href="" class="panel-close tooltips" data-toggle="tooltip" title="" data-original-title="Close Panel"><i class="fa fa-times"></i></a>
                                        </div><!-- panel-btns -->
                                    </div><!-- panel-heading -->
                                    <div class="panel-body">
                                         <div class="row">
                                           		
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">References <span class="asterisk">*</span></label>
                                                <div class="col-sm-9">
                                                    <textarea autocorrect="off"  rows="5" class="form-control" placeholder="Type your comment..." required=""></textarea>
                                                </div>
                                            </div><!-- form-group -->
                                        </div><!-- row -->
                                    </div><!-- panel-body -->
                                    <div class="panel-footer">
                                      <div class="row">
                                        <div class="col-sm-9 col-sm-offset-3">
                                            <button class="btn btn-primary mr5">Prev</button>
                                        </div>
                                      </div>
                                    </div><!-- panel-footer -->  
                                </div><!-- panel -->
                                </form>
                                
                            </div>
							
							<div class="col-md-6">
                                <form id="basicForm" action="#" novalidate="novalidate">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <div class="panel-btns" style="display: none;">
                                            <a href="" class="panel-minimize tooltips" data-toggle="tooltip" title="" data-original-title="Minimize Panel"><i class="fa fa-minus"></i></a>
                                            <a href="" class="panel-close tooltips" data-toggle="tooltip" title="" data-original-title="Close Panel"><i class="fa fa-times"></i></a>
                                        </div><!-- panel-btns -->                                      
                                    </div><!-- panel-heading -->
                                    <div class="panel-body">
                                        <div class="row">
										
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Comments <span class="asterisk">*</span></label>
                                                <div class="col-sm-9">
                                                    <textarea autocorrect="off"  rows="5" class="form-control" placeholder="Type your comment..." required=""></textarea>
                                                </div>
                                            </div><!-- form-group -->
											
                                        </div><!-- row -->
                                    </div><!-- panel-body -->
                                    <div class="panel-footer">
                                      <div class="row">
                                        <div class="col-sm-9 col-sm-offset-3">	
                                            <button class="btn btn-primary mr5">Next</button>
                                        </div>
                                      </div>
                                    </div><!-- panel-footer -->  
                                </div><!-- panel -->
                                </form>
                                
                            </div>
							
                           </div><!-- panel-body -->
				        </div>
                     </div><!-- panel -->
					 
					 <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" class="collapsed" data-parent="#accordion2" href="#photograph">
                                                    Screen 6 - Photograph and Signature
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="photograph" class="panel-collapse collapse">
                                        
										<div class="panel-body">
                                <div class="col-md-6">
                                <form id="basicForm" action="#" novalidate="novalidate">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <div class="panel-btns" style="display: none;">
                                            <a href="" class="panel-minimize tooltips" data-toggle="tooltip" title="" data-original-title="Minimize Panel"><i class="fa fa-minus"></i></a>
                                            <a href="" class="panel-close tooltips" data-toggle="tooltip" title="" data-original-title="Close Panel"><i class="fa fa-times"></i></a>
                                        </div><!-- panel-btns -->
                                    </div><!-- panel-heading -->
                                    <div class="panel-body">
                                         <div class="row">
                                           		
                                            <div class="form-group">
                                               <center><img src="images/photograph.png"></center><br>
											   <center><button class="btn btn-primary mr5">Take Pictures</button></center>
                                            </div><!-- form-group -->
                                        </div><!-- row -->
                                    </div><!-- panel-body -->
                                    <div class="panel-footer">
                                      <div class="row">
                                        <div class="col-sm-9 col-sm-offset-3">
                                            <button class="btn btn-primary mr5">Prev</button>
                                        </div>
                                      </div>
                                    </div><!-- panel-footer -->  
                                </div><!-- panel -->
                                </form>
                                
                            </div>
							
							<div class="col-md-6">
                                <form id="basicForm" action="#" novalidate="novalidate">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <div class="panel-btns" style="display: none;">
                                            <a href="" class="panel-minimize tooltips" data-toggle="tooltip" title="" data-original-title="Minimize Panel"><i class="fa fa-minus"></i></a>
                                            <a href="" class="panel-close tooltips" data-toggle="tooltip" title="" data-original-title="Close Panel"><i class="fa fa-times"></i></a>
                                        </div><!-- panel-btns -->                                      
                                    </div><!-- panel-heading -->
                                    <div class="row">
                            <div class="col-md-6">                                                           
                                    <div class="panel-body">
                                       <div id="canvas">
            <canvas class="roundCorners" id="newSignature"
            style="position: relative; margin: 0; padding: 0; border: 1px solid #c4caac;"></canvas>
        </div>
        <script>signatureCapture();</script>
        <button type="button" onclick="signatureSave()">Save signature</button>
        <button type="button" onclick="signatureClear()">Clear signature</button>
        </br>
        Saved Image
        </br>
        <img id="saveSignature" alt="Saved image png"/>
                                                                                                 
                            </div><!-- col-md-6 -->
                             
                        </div><!-- row -->
                                    <div class="panel-footer">
                                      <div class="row">
                                        <div class="col-sm-9 col-sm-offset-3">	
                                            <button class="btn btn-primary mr5">Next</button>
                                        </div>
                                      </div>
                                    </div><!-- panel-footer -->  
                                </div><!-- panel -->
                                </form>
                                
                            </div>
							
                           </div><!-- panel-body -->
				        </div>
                     </div><!-- panel -->
                 </div> 
				 
				 <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" class="collapsed" data-parent="#accordion2" href="#authorize">
                                                    Screen 7 - Authorize and Print
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="authorize" class="panel-collapse collapse">
                                        
										<div class="panel-body">
                                <div class="col-md-6">
                                <form id="basicForm3" action="form-validation.html" novalidate="novalidate">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <div class="panel-btns" style="display: none;">
                                            <a href="" class="panel-minimize tooltips" data-toggle="tooltip" title="" data-original-title="Minimize Panel"><i class="fa fa-minus"></i></a>
                                            <a href="" class="panel-close tooltips" data-toggle="tooltip" title="" data-original-title="Close Panel"><i class="fa fa-times"></i></a>
                                        </div>
                                    </div><!-- panel-heading -->
                                    <div class="panel-body">  
                                       <div class="form-group">
                                            <label class="col-sm-3 control-label">Interest <span class="asterisk">*</span></label>
                                            <div class="col-sm-9">
                                                <div class="ckbox ckbox-primary">
                                                    <input autocorrect="off"  type="checkbox" id="Substantial" value="m" name="int[]" required="">
                                                    <label for="Substantial">Substantial Donation</label>
                                                </div><!-- rdio -->
                                                
                                                <div class="ckbox ckbox-primary">
                                                    <input autocorrect="off"  type="checkbox" value="f" id="Generous" name="int[]">
                                                    <label for="Generous">Generous Donation</label>
                                                </div><!-- rdio -->
                                            
                                                <div class="ckbox ckbox-primary">
                                                    <input autocorrect="off"  type="checkbox" value="f" id="Standard" name="int[]">
                                                    <label for="Standard">Standard Donation</label>
                                                </div><!-- rdio -->
												
												<div class="ckbox ckbox-primary">
                                                    <input autocorrect="off"  type="checkbox" value="f" id="One_Meal" name="int[]">
                                                    <label for="One_Meal">One Meal Equivalent</label>
                                                </div><!-- rdio -->
												
												<div class="ckbox ckbox-primary">
                                                    <input autocorrect="off"  type="checkbox" value="f" id="Token" name="int[]">
                                                    <label for="Token">Token Donation</label>
                                                </div><!-- rdio -->
												
												<div class="ckbox ckbox-primary">
                                                    <input autocorrect="off"  type="checkbox" value="f" id="No_Special" name="int[]">
                                                    <label for="No_Special">No Special Recommendation</label>
                                                </div><!-- rdio -->
                                                <label id="intError" class="error" for="int[]"></label>
                                            </div>
                                        </div>                                        
                                    </div><!-- panel-body -->
                                    <div class="panel-footer">
                                        <div class="row">
                                            <div class="col-sm-9 col-sm-offset-3">
                                                <button class="btn btn-primary mr5">Prev</button>
                                            </div>
                                        </div>
                                    </div><!-- panel-footer -->
                                </div><!-- panel -->
                                </form>
                                
                            </div>
							
							<div class="col-md-6">
                                <form id="basicForm" action="#" novalidate="novalidate">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <div class="panel-btns" style="display: none;">
                                            <a href="" class="panel-minimize tooltips" data-toggle="tooltip" title="" data-original-title="Minimize Panel"><i class="fa fa-minus"></i></a>
                                            <a href="" class="panel-close tooltips" data-toggle="tooltip" title="" data-original-title="Close Panel"><i class="fa fa-times"></i></a>
                                        </div><!-- panel-btns -->                                      
                                    </div><!-- panel-heading -->
                                    <div class="panel-body">
                                        <div class="row">
										<div class="form-group">
                                            <label class="col-sm-3 control-label">US State</label>
                                            <div class="col-sm-9">
                                                <div class="select2-container width100p" id="s2id_fruits"><a href="javascript:void(0)" class="select2-choice" tabindex="-1">  
												<span class="select2-chosen" id="select2-chosen-1">Choose One</span><abbr class="select2-search-choice-close"></abbr> 
												<span class="select2-arrow" role="presentation"><b role="presentation"></b></span></a><label for="s2id_autogen1" class="select2-offscreen"></label>
												<input autocorrect="off"  class="select2-focusser select2-offscreen" type="text" aria-haspopup="true" role="button" aria-labelledby="select2-chosen-1" id="s2id_autogen1"></div>
												<select id="fruits" data-placeholder="Choose One" class="width100p select2-offscreen" required="" tabindex="-1" title="">
                                                    <option value="">Choose One</option>
                                                    <option value="apple">Apple</option>
                                                    <option value="orange">Orange</option>
                                                    <option value="grapes">Grapes</option>
                                                    <option value="strawberry">Strawberry</option>
                                                </select>
                                                <label class="error" for="fruits"></label>
                                            </div>
                                        </div>
											
                                        </div><!-- row -->
                                    </div><!-- panel-body -->
                                    <div class="panel-footer">
                                      <div class="row">
                                        <div class="col-sm-9 col-sm-offset-3">	
                                            <button class="btn btn-primary mr5">Print</button>
                                        </div>
                                      </div>
                                    </div><!-- panel-footer -->  
                                </div><!-- panel -->
                                </form>
                                
                            </div>
							
                           </div><!-- panel-body -->
				        </div>
                     </div><!-- panel -->
                                               
                    </div><!-- contentpanel -->
                </div><!-- mainpanel -->
            </div><!-- mainwrapper -->
        </section>

        <script src="js/jquery-1.11.1.min.js"></script>
        <script src="js/jquery-migrate-1.2.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/modernizr.min.js"></script>
        <script src="js/ppace.min.js"></script>

        <script src="js/jquery.cookies.js"></script>
        
        <script src="js/jquery.dataTables.min.js"></script>
        <script src="js/dataTables.bootstrap.js"></script>
        <script src="js/dataTables.responsive.js"></script>
        <script src="js/select2.min.js"></script>

        <script src="js/custom.js"></script>
        <script>
            jQuery(document).ready(function(){
                
                jQuery('#basicTable').DataTable({
                    responsive: true
                });
                
                var shTable = jQuery('#shTable').DataTable({
                    "fnDrawCallback": function(oSettings) {
                        jQuery('#shTable_paginate ul').addClass('pagination-active-dark');
                    },
                    responsive: true
                });
                
                // Show/Hide Columns Dropdown
                jQuery('#shCol').click(function(event){
                    event.stopPropagation();
                });
                
                jQuery('#shCol input').on('click', function() {

                    // Get the column API object
                    var column = shTable.column($(this).val());
 
                    // Toggle the visibility
                    if ($(this).is(':checked'))
                        column.visible(true);
                    else
                        column.visible(false);
                });
                
                var exRowTable = jQuery('#exRowTable').DataTable({
                    responsive: true,
                    "fnDrawCallback": function(oSettings) {
                        jQuery('#exRowTable_paginate ul').addClass('pagination-active-success');
                    },
                    "ajax": "ajax/objects.txt",
                    "columns": [
                        {
                            "class":          'details-control',
                            "orderable":      false,
                            "data":           null,
                            "defaultContent": ''
                        },
                        { "data": "name" },
                        { "data": "position" },
                        { "data": "office" },
                        { "data": "salary" }
                    ],
                    "order": [[1, 'asc']] 
                });
                
                // Add event listener for opening and closing details
                jQuery('#exRowTable tbody').on('click', 'td.details-control', function () {
                    var tr = $(this).closest('tr');
                    var row = exRowTable.row( tr );
             
                    if ( row.child.isShown() ) {
                        // This row is already open - close it
                        row.child.hide();
                        tr.removeClass('shown');
                    }
                    else {
                        // Open this row
                        row.child( format(row.data()) ).show();
                        tr.addClass('shown');
                    }
                });
               
                
                // DataTables Length to Select2
                jQuery('div.dataTables_length select').removeClass('form-control input-sm');
                jQuery('div.dataTables_length select').css({width: '60px'});
                jQuery('div.dataTables_length select').select2({
                    minimumResultsForSearch: -1
                });
    
            });
            
            function format (d) {
                // `d` is the original data object for the row
                return '<table class="table table-bordered nomargin">'+
                    '<tr>'+
                        '<td>Full name:</td>'+
                        '<td>'+d.name+'</td>'+
                    '</tr>'+
                    '<tr>'+
                        '<td>Extension number:</td>'+
                        '<td>'+d.extn+'</td>'+
                    '</tr>'+
                    '<tr>'+
                        '<td>Extra info:</td>'+
                        '<td>And any further details here (images etc)...</td>'+
                    '</tr>'+
                '</table>';
            }
        </script>
		
		<script>
            jQuery(document).ready(function(){
              
                // Basic Form
                jQuery("#basicForm").validate({
                    highlight: function(element) {
                        jQuery(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                    },
                    success: function(element) {
                        jQuery(element).closest('.form-group').removeClass('has-error');
                    }
                });
              
                // Error Message In One Container
                jQuery("#basicForm2").validate({
                    errorLabelContainer: jQuery("#basicForm2 div.errorForm")
                });
              
                // With Checkboxes and Radio Buttons
                
                jQuery('#genderError').attr('for','gender');
                jQuery('#intError').attr('for','int[]');
                
                jQuery("#basicForm3").validate({
                    highlight: function(element) {
                        jQuery(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                    },
                    success: function(element) {
                        jQuery(element).closest('.form-group').removeClass('has-error');
                    }
                });
                
                jQuery("#basicForm4").validate({
                    highlight: function(element) {
                        jQuery(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                    },
                    success: function(element) {
                        jQuery(element).closest('.form-group').removeClass('has-error');
                    },
                    ignore: null
                });
                
                // Validation with select boxes
                jQuery("#flowers, #fruits").select2({
                    minimumResultsForSearch: -1
                });
              
            });
        </script>

    </body>
</html>
