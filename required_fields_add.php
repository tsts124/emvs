<?php

/*
 * File was created to handle process of creating and updating Required Fields items
 * It works same as for previously coded look ups creation and update possibilities, but in this case functionality
 * can handle different types of required fields in one place
 * */

include 'includes/header.php';
include 'includes/dbcon.php';

echo $e = base64_decode($_GET['e']);
$fieldType = $_GET['fieldType'];

switch ($fieldType) {

    case "collectorDetails":
        $subjectTitle = "New Collector Details and Personal Info";
        break;
    case "visits":
        $subjectTitle = "Visits Tab";
        break;
    case "address":
        $subjectTitle = "Address Tab";
        break;
    case "funds":
        $subjectTitle = "Funds Tab";
        break;
    case "purpose":
        $subjectTitle = "Purpose Tab";
        break;
    case "reference":
        $subjectTitle = "Reference Tab";
        break;
    default:
        $subjectTitle = "unknown";
}

if ($_SESSION['user'] == '') {
    header('Location: emvs.php?action=index');
}

$requiredFieldTitle = $_POST['requiredFieldTitle'];
$requiredFieldAssignment = $_POST['requiredFieldAssignment'];
if (isset($_POST['active'])) $active = 1; else $active = 0;

if (isset($_POST['createRequiredField'])) {

    $query = $dbh->prepare("
                  SELECT 
                  * 
                  FROM 
                    `required_fields` 
                  WHERE 
                    `required_fields`.`title`='$requiredFieldTitle' AND `required_fields`.`tab` = '$fieldType' AND
                     `required_fields`.`assignment`='$requiredFieldAssignment'
                ");
    $query->execute();

    $response = $query->fetch();

    if (isset($response) && $response['title'] == '') {
        $count = $dbh->exec("
                INSERT INTO `required_fields`(`tab`, `title`,`assignment`,`active`,`deleted`) 
                VALUES ('$fieldType', '$requiredFieldTitle','$requiredFieldAssignment', '$active','0')
             ");

        if ($count == '1') {
            $err = '<div class="success1">';
            $err .= '<p>';
            $err .= '<img src="images/success.png" border="none" width="18" height="18"/> ';
            $err .= 'Required Field for ' . $subjectTitle . ' Added Successfully';
            $err .= '</p>';
            $err .= '</div>';
            echo '<meta http-equiv="refresh" content="0; URL=emvs.php?action=requiredFields&fieldType=' . $fieldType . '" />';
        }
    }
}

if ($e != '') {

    $sqlq = $dbh->prepare("
            SELECT 
            * 
            FROM 
            `required_fields` 
            WHERE 
            `required_fields`.`id`='$e'
        ");
    $sqlq->execute();
    $item = $sqlq->fetch();
}

if (isset($_POST['updateRequiredField'])) {

    $count = $dbh->exec("
            UPDATE `required_fields` 
            SET `required_fields`.`title`='$requiredFieldTitle',
             `required_fields`.`assignment` = '$requiredFieldAssignment',
             `required_fields`.`active`= '$active'
            WHERE `required_fields`.`id`='$e'
        ");

    if ($count == '1') {
        $err = '<div class="success1">';
        $err .= '<p>';
        $err .= '<img src="images/success.png" border="none" width="18" height="18">';
        $err .= 'Required Field For ' . $subjectTitle . ' Updated Successfully';
        $err .= '</p>';
        $err .= '</div>';
        echo '<meta http-equiv="refresh" content="0; URL=emvs.php?action=requiredFields&fieldType=' . $fieldType . '" />';
    }
}
?>

<script type="text/javascript" src="js/jquery-1.2.6.min.js"></script>

<SCRIPT type="text/javascript">

    function requiredFieldList(fieldType) {

        var val = document.getElementById("requiredFieldTitle").value;

        if (val.length >= 2) {

            var dataString = 'requiredFieldTitle=' + val + '&fieldType=' + fieldType;
            $.ajax({
                type: "POST",
                url: "check.php",
                data: dataString,
                success: function (msg) {

                    $('#status').html(msg);
                    if (msg == 'OK') {

                        document.getElementById('createRequiredField').disabled = false;
                        document.getElementById('updateRequiredField').disabled = false;
                    } else {

                        document.getElementById('createRequiredField').disabled = true;
                        document.getElementById('updateRequiredField').disabled = true;
                    }
                }
            });
        } else {

            $("#status").html('<font color="red">The Required Field should have at least <strong>2</strong> characters.</font>');
        }
    }
</script>

<script type="text/javascript" src="http://code.jquery.com/jquery-1.8.2.js"></script>

<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="js/jquery.validate.min.js"></script>

<script>

    (function ($) {

        jQuery.validator.setDefaults({
            errorPlacement: function (error, element) {
                error.appendTo('#invalid-' + element.attr('id'));
            }
        });
        $("#basicForm").validate({
            rules: {

                sites: {
                    required: true,
                    minlength: 3,
                },

                title: "required",
                image: "required",
                subject: "required",
                femail: {
                    required: true,
                    email: true,
                }
            },
            messages: {
                SSN: {
                    required: "Please enter your SSN",
                    minlength: "Invalid SSN(9 Digits atleast)",
                    maxlength: "Invalid SSN(9 Digits only)"
                },
                sites: {
                    required: "Please enter your sites",
                    minlength: "Please enter a valid site name",
                },
                title: "Please Choose your image",
                image: "Please Choose your image",
                subject: "Please enter your subject",
                femail: {
                    required: "Please enter your email",
                    minlength: "Please enter a valid email address",
                }
            }
        });
    })($);

</script>
<!-- Validaion-->
<link rel="stylesheet" type="text/css" href="css/jquery.validate.css"/>
<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/jquery.validate.js" type="text/javascript"></script>
<script src="js/jquery.validation.functions.js" type="text/javascript">
</script>
<script type="text/javascript">
    /* <![CDATA[ */
    jQuery(function () {
        jQuery("#sites").validate({
            expression: "if (VAL) return true; else return false;",
            message: "Please enter the Required field"
        });
        jQuery("#firstname").validate({
            expression: "if (VAL) return true; else return false;",
            message: "Please enter the Required field"
        });
        jQuery("#lastname").validate({
            expression: "if (VAL) return true; else return false;",
            message: "Please enter the Required field"
        });
        jQuery("#phoneno").validate({
            expression: "if (VAL.match(/^[0-9]*$/) && VAL) return true; else return false;",
            message: "Please enter a valid integer"
        });
        jQuery('.AdvancedForm').validated(function () {
            alert("Use this call to make AJAX submissions.");
        });
    });
    /* ]]> */
</script>
<section>
    <div class="mainwrapper">
        <?php include 'includes/leftpanel.php'; ?>

        <div class="mainpanel">
            <div class="pageheader">
                <div>
                    <div class="media-body">
                        <h4><?php if ($e == '') {
                                echo 'Add Required Field For ' . $subjectTitle;
                            } else {
                                echo 'Update Required Field For ' . $subjectTitle;
                            } ?></h4>
                    </div>
                </div><!-- media -->
            </div><!-- pageheader -->

            <div class="contentpanel">
                <div class="row">
                    <div class="col-md-6">
                        <form id="basicForm" action="#" method="post" novalidate="novalidate">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="form-group">
                                            <?= $err; ?>
                                        </div>


                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">
                                                Title
                                            </label>
                                            <div class="col-sm-9">
                                                <input autocorrect="off"  type="text" name="requiredFieldTitle"
                                                       onkeyup="requiredFieldList('<?php echo $fieldType; ?>')"
                                                       id="requiredFieldTitle"
                                                       class="form-control"
                                                       value="<?= $item['title']; ?>"
                                                       placeholder="" required=""/>
                                            </div>
                                        </div><!-- form-group -->

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">
                                                Assignment
                                            </label>
                                            <div class="col-sm-9">
                                                <input autocorrect="off"  type="text" name="requiredFieldAssignment"
                                                       id="requiredFieldAssignment"
                                                       class="form-control"
                                                       value="<?= $item['assignment']; ?>"
                                                       placeholder="" required=""/>
                                            </div>
                                        </div><!-- form-group -->

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"></label>
                                            <div class="col-sm-9">
                                                <div id="status"></div>
                                            </div>
                                        </div><!-- form-group -->
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Required</label>
                                            <div class="col-sm-9">
                                                <?php if ($e == '') { ?>
                                                    <div class="ckbox ckbox-primary">
                                                        <input autocorrect="off"  type="checkbox" checked id="activesites" value="1"
                                                               name="active" required="">
                                                        <label for="activesites"></label>
                                                    </div><!-- rdio --><?php } else { ?>
                                                    <div class="ckbox ckbox-primary">
                                                        <input autocorrect="off"  type="checkbox" value="1" name="active"
                                                               id="active<?= $item['Id']; ?>"
                                                               onclick="fnactive(<?= $item['id']; ?>)" <?php if ($item['active'] == 1) echo "checked"; ?>/>
                                                        <label for="active<?= $item['Id']; ?>"></label>
                                                    </div>

                                                <?php } ?>
                                            </div>
                                        </div><!-- form-group -->
                                    </div><!-- row -->
                                </div><!-- panel-body -->
                                <div class="panel-footer">
                                    <div class="row">
                                        <div class="col-sm-9 col-sm-offset-3">
                                            <?php if ($e == '') {
                                                echo '<button class="btn btn-primary mr5" id="createRequiredField" name="createRequiredField" type="submit">Add Field</button>';
                                                echo '<input autocorrect="off"  name="updateRequiredFieldS" id="updateRequiredField" type="hidden">';
                                            } else {
                                                echo '<button class="btn btn-primary mr5" name="updateRequiredField" id="updateRequiredField" type="submit">Update Field</button>';
                                                echo '<input autocorrect="off"  name="createRequiredFieldS" id="createRequiredField" type="hidden">';
                                            } ?>
                                            <a href="emvs.php?action=requiredFields&fieldType=<?php echo $fieldType; ?>"
                                               class="btn btn-dark"> Cancel</a>
                                        </div>
                                    </div>
                                </div><!-- panel-footer -->
                            </div><!-- panel -->
                        </form>

                    </div>
                </div><!-- row -->

            </div>
        </div><!-- mainpanel -->
    </div><!-- mainwrapper -->
</section>


<script src="js/jquery-migrate-1.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/modernizr.min.js"></script>
<script src="js/ppace.min.js"></script>

<script src="js/jquery.cookies.js"></script>

<script src="js/jquery.dataTables.min.js"></script>
<script src="js/dataTables.bootstrap.js"></script>
<script src="js/dataTables.responsive.js"></script>
<script src="js/select2.min.js"></script>

<script src="js/custom.js"></script>
<script>
    jQuery(document).ready(function () {

        jQuery('#basicTable').DataTable({
            responsive: true
        });

        var shTable = jQuery('#shTable').DataTable({
            "fnDrawCallback": function (oSettings) {
                jQuery('#shTable_paginate ul').addClass('pagination-active-dark');
            },
            responsive: true
        });

        // Show/Hide Columns Dropdown
        jQuery('#shCol').click(function (event) {
            event.stopPropagation();
        });

        jQuery('#shCol input').on('click', function () {

            // Get the column API object
            var column = shTable.column($(this).val());

            // Toggle the visibility
            if ($(this).is(':checked'))
                column.visible(true);
            else
                column.visible(false);
        });

        var exRowTable = jQuery('#exRowTable').DataTable({
            responsive: true,
            "fnDrawCallback": function (oSettings) {
                jQuery('#exRowTable_paginate ul').addClass('pagination-active-success');
            },
            "ajax": "ajax/objects.txt",
            "columns": [
                {
                    "class": 'details-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": ''
                },
                {"data": "name"},
                {"data": "position"},
                {"data": "office"},
                {"data": "salary"}
            ],
            "order": [[1, 'asc']]
        });

        // Add event listener for opening and closing details
        jQuery('#exRowTable tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = exRowTable.row(tr);

            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            }
            else {
                // Open this row
                row.child(format(row.data())).show();
                tr.addClass('shown');
            }
        });


        // DataTables Length to Select2
        jQuery('div.dataTables_length select').removeClass('form-control input-sm');
        jQuery('div.dataTables_length select').css({width: '60px'});
        jQuery('div.dataTables_length select').select2({
            minimumResultsForSearch: -1
        });

    });

    function format(d) {
        // `d` is the original data object for the row
        return '<table class="table table-bordered nomargin">' +
            '<tr>' +
            '<td>Full name:</td>' +
            '<td>' + d.name + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Extension number:</td>' +
            '<td>' + d.extn + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Extra info:</td>' +
            '<td>And any further details here (images etc)...</td>' +
            '</tr>' +
            '</table>';
    }
</script>

</body>
</html>
