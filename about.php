<?php
include 'includes/header.php'; 
	if($_SESSION['user']==''){
		header('Location: emvs.php?action=index');
	}
$user = $_SESSION['user'];
	include 'includes/dbcon.php';
		$sql = $dbh->prepare("select * from newmember where Username='$user'");
		$sql->execute();
		$count = $sql->rowCount();
		$data = $sql->fetch();
	
?>
<style>
		table { margin: 1em; border-collapse: collapse; }
td, th { padding: 5px; border: 1px #ccc solid; }
thead { background: #99CCFF; }
tbody { background: #fff; }
#highlight tr.hilight { background: #99CCFF; }
		</style>
		<script>
		function tableHighlightRow() {
  if (document.getElementById && document.createTextNode) {
    var tables=document.getElementsByTagName('table');
    for (var i=0;i<tables.length;i++)
    {
      if(tables[i].className=='hilite') {
        var trs=tables[i].getElementsByTagName('tr');
        for(var j=0;j<trs.length;j++)
{
          if(trs[j].parentNode.nodeName=='TBODY') {
            trs[j].onmouseover=function(){this.className='hilight';return false}
            trs[j].onmouseout=function(){this.className='';return false}
          }
        }
      }
    }
  }
}
window.onload=function(){tableHighlightRow();}
		</script>

        <section>
            <div class="mainwrapper">
                  <?php include 'includes/leftpanel.php'; ?>
                
                <div class="mainpanel">
                    <div class="pageheader">
                        <div class="media">                            
                            <div class="media-body">                               
                                <h4>About Membership Manager</h4>
								<h5><button class="btn btn-primary btn-xs"><a href="emvs.php?action=new_member" style="color:#fff"> New Member</a></button> <button class="btn btn-primary btn-xs"><a href="emvs.php?action=tools"  style="color:#fff"> Tools</a></button> <button class="btn btn-primary btn-xs"><a href="#"  style="color:#fff"> Help</a></button></h5>
						
                            </div>
                        </div><!-- media -->
                    </div><!-- pageheader -->
                    
                    <div class="contentpanel">                                               
                        <div class="row">
						<table class="hilite" id="highlight" style="width:90%">
<h2>PHP Configuration</h2>
<tbody>
<tr><td style="padding: 8px;">Name</td><td style="padding: 8px;">PHP,MYSQL MembershipProvider	</td></tr>
<tr><td style="padding: 8px;">PHP Version</td><td style="padding: 8px;"><?=phpversion();?></td></tr>
<tr><td style="padding: 8px;">Web Server Version</td><td style="padding: 8px;"><?=$_SERVER["SERVER_SOFTWARE"];?></td></tr>
<tr><td style="padding: 8px;">Browser Details</td><td style="padding: 8px;"><?=$_SERVER['HTTP_USER_AGENT'];?></td></tr>
<tr><td style="padding: 8px;">Enable Password Reset</td><td style="padding: 8px;">True</td></tr>
<tr><td style="padding: 8px;">Enable Password Retrieval</td><td style="padding: 8px;">False</td></tr>
<tr><td style="padding: 8px;">Maximum Invalid Password Attempts</td><td style="padding: 8px;"><?=$data['twrongcount'];?></td></tr>
<tr><td style="padding: 8px;">Password Attempt</td><td style="padding: 8px;"><?=$data['wrongcount'];?></td></tr>
<tr><td style="padding: 8px;">Password Format</td><td style="padding: 8px;">Hashing</td></tr>
<!--<p>Password Strength Regular Expressions</p>
<tr><td style="padding: 8px;">Machinekey Defined in web.conig</td><td style="padding: 8px;">False</td></tr>
<tr><td style="padding: 8px;">Requires Questions and Answer</td><td style="padding: 8px;">False</td></tr>-->
<tr><td style="padding: 8px;">Requires Unique Email</td><td style="padding: 8px;">True</td></tr>
<tr><td style="padding: 8px;">Role Manager Enabled</td><td style="padding: 8px;">True</td></tr>
</tbody>
</table>  
						<h2>PHP Manager capabilities</h2>
						<p>Based on your current profile setting in PHP.config.</p>
						<p>Password format is Hashed. Provider will allow Password Reset</p>
						<p>Since Enable Password Retrieval is disabled, the ChangePassword panels will working. </p>
						<h2>Version Information</h2>	
						<!--<p>QualityData.Membership</p>-->
						<p>Version <?=$_SERVER["SERVER_SOFTWARE"];?></p>
						<p>Copyright &copy; EMVS, Inc. 2015</p>
						<!--<p>Quality Data, Inc.</p>-->
						<p>Enhanced functionality for Personal Home Page(PHP) & MYSQL.</p>
						<!--<p>Visit us online</p>
							<a href="#" class="btn btn-warning btn-metro">OK</a>	-->
                        </div><!-- row -->
                    
                    </div>
                </div><!-- mainpanel -->
            </div><!-- mainwrapper -->
        </section>

        <script src="js/jquery-1.11.1.min.js"></script>
        <script src="js/jquery-migrate-1.2.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/modernizr.min.js"></script>
        <script src="js/ppace.min.js"></script>

        <script src="js/jquery.cookies.js"></script>
        
        <script src="js/jquery.dataTables.min.js"></script>
        <script src="js/dataTables.bootstrap.js"></script>
        <script src="js/dataTables.responsive.js"></script>
        <script src="js/select2.min.js"></script>

        <script src="js/custom.js"></script>
        <script>
            jQuery(document).ready(function(){
                
                jQuery('#basicTable').DataTable({
                    responsive: true
                });
                
                var shTable = jQuery('#shTable').DataTable({
                    "fnDrawCallback": function(oSettings) {
                        jQuery('#shTable_paginate ul').addClass('pagination-active-dark');
                    },
                    responsive: true
                });
                
                // Show/Hide Columns Dropdown
                jQuery('#shCol').click(function(event){
                    event.stopPropagation();
                });
                
                jQuery('#shCol input').on('click', function() {

                    // Get the column API object
                    var column = shTable.column($(this).val());
 
                    // Toggle the visibility
                    if ($(this).is(':checked'))
                        column.visible(true);
                    else
                        column.visible(false);
                });
                
                var exRowTable = jQuery('#exRowTable').DataTable({
                    responsive: true,
                    "fnDrawCallback": function(oSettings) {
                        jQuery('#exRowTable_paginate ul').addClass('pagination-active-success');
                    },
                    "ajax": "ajax/objects.txt",
                    "columns": [
                        {
                            "class":          'details-control',
                            "orderable":      false,
                            "data":           null,
                            "defaultContent": ''
                        },
                        { "data": "name" },
                        { "data": "position" },
                        { "data": "office" },
                        { "data": "salary" }
                    ],
                    "order": [[1, 'asc']] 
                });
                
                // Add event listener for opening and closing details
                jQuery('#exRowTable tbody').on('click', 'td.details-control', function () {
                    var tr = $(this).closest('tr');
                    var row = exRowTable.row( tr );
             
                    if ( row.child.isShown() ) {
                        // This row is already open - close it
                        row.child.hide();
                        tr.removeClass('shown');
                    }
                    else {
                        // Open this row
                        row.child( format(row.data()) ).show();
                        tr.addClass('shown');
                    }
                });
               
                
                // DataTables Length to Select2
                jQuery('div.dataTables_length select').removeClass('form-control input-sm');
                jQuery('div.dataTables_length select').css({width: '60px'});
                jQuery('div.dataTables_length select').select2({
                    minimumResultsForSearch: -1
                });
    
            });
            
            function format (d) {
                // `d` is the original data object for the row
                return '<table class="table table-bordered nomargin">'+
                    '<tr>'+
                        '<td>Full name:</td>'+
                        '<td>'+d.name+'</td>'+
                    '</tr>'+
                    '<tr>'+
                        '<td>Extension number:</td>'+
                        '<td>'+d.extn+'</td>'+
                    '</tr>'+
                    '<tr>'+
                        '<td>Extra info:</td>'+
                        '<td>And any further details here (images etc)...</td>'+
                    '</tr>'+
                '</table>';
            }
        </script>

    </body>
</html>

