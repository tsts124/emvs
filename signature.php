<script>
	window.onunload = refreshParent;
    function refreshParent() {
		if (window.opener){
			window.opener.document.getElementById("imgDiv").innerHTML=document.getElementById("newUserSignature").innerHTML;
		}
		//window.opener.location.reload();
    }    

	function signatureCapture1() {
	var canvas = document.getElementById("newSignature1");
	var context = canvas.getContext("2d");
	canvas.width = 400;
	canvas.height = 100;
	context.fillStyle = "#fff";
	context.strokeStyle = "#444";
	context.lineWidth = 5;
	context.lineCap = "round";
	context.fillRect(0, 0, canvas.width, canvas.height);
	var disableSave = true;
	var pixels = [];
	var cpixels = [];
	var xyLast = {};
	var xyAddLast = {};
	var calculate = false;
	{ 	//functions
		function remove_event_listeners() {
			canvas.removeEventListener('mousemove', on_mousemove, false);
			canvas.removeEventListener('mouseup', on_mouseup, false);
			canvas.removeEventListener('touchmove', on_mousemove, false);
			canvas.removeEventListener('touchend', on_mouseup, false);

			document.body.removeEventListener('mouseup', on_mouseup, false);
			document.body.removeEventListener('touchend', on_mouseup, false);
		}

		function get_coords(e) {
			var x, y;

			if (e.changedTouches && e.changedTouches[0]) {
				var offsety = canvas.offsetTop || 0;
				var offsetx = canvas.offsetLeft || 0;

				x = e.changedTouches[0].pageX - offsetx;
				y = e.changedTouches[0].pageY - offsety;
			} else if (e.layerX || 0 == e.layerX) {
				x = e.layerX;
				y = e.layerY;
			} else if (e.offsetX || 0 == e.offsetX) {
				x = e.offsetX;
				y = e.offsetY;
			}

			return {
				x : x, y : y
			};
		};

		function on_mousedown(e) {
			e.preventDefault();
			e.stopPropagation();

			canvas.addEventListener('mouseup', on_mouseup, false);
			canvas.addEventListener('mousemove', on_mousemove, false);
			canvas.addEventListener('touchend', on_mouseup, false);
			canvas.addEventListener('touchmove', on_mousemove, false);
			document.body.addEventListener('mouseup', on_mouseup, false);
			document.body.addEventListener('touchend', on_mouseup, false);

			empty = false;
			var xy = get_coords(e);
			context.beginPath();
			pixels.push('moveStart');
			context.moveTo(xy.x, xy.y);
			pixels.push(xy.x, xy.y);
			xyLast = xy;
		};

		function on_mousemove(e, finish) {
			e.preventDefault();
			e.stopPropagation();

			var xy = get_coords(e);
			var xyAdd = {
				x : (xyLast.x + xy.x) / 2,
				y : (xyLast.y + xy.y) / 2
			};

			if (calculate) {
				var xLast = (xyAddLast.x + xyLast.x + xyAdd.x) / 3;
				var yLast = (xyAddLast.y + xyLast.y + xyAdd.y) / 3;
				pixels.push(xLast, yLast);
			} else {
				calculate = true;
			}

			context.quadraticCurveTo(xyLast.x, xyLast.y, xyAdd.x, xyAdd.y);
			pixels.push(xyAdd.x, xyAdd.y);
			context.stroke();
			context.beginPath();
			context.moveTo(xyAdd.x, xyAdd.y);
			xyAddLast = xyAdd;
			xyLast = xy;

		};

		function on_mouseup(e) {
			remove_event_listeners();
			disableSave = false;
			context.stroke();
			pixels.push('e');
			calculate = false;
		};
	}
	canvas.addEventListener('touchstart', on_mousedown, false);
	canvas.addEventListener('mousedown', on_mousedown, false);
}
function signatureSave1(a) {
	var canvas = document.getElementById("newSignature1");// save canvas image as data url (png format by default)
	var dataURL = canvas.toDataURL("image/png");
	var a = a;
	document.getElementById("saveSignature1").src = dataURL;
	document.getElementById('imgsrc').value = dataURL;
	document.getElementById('a').value = a;
	
	var fd = new FormData(document.forms["basicForm"]);
                var xhr = new XMLHttpRequest();
                xhr.open('POST', 'upload_data1.php', true);
 
                xhr.upload.onprogress = function(t) {
                    if (t.lengthComputable) {
                        var percentComplete = (t.loaded / t.total) * 100;
						console.log(percentComplete + '% uploaded');
                        //alert('Succesfully uploaded');
                    }
                };
 
                xhr.onload = function() {
 
                };
                xhr.send(fd);
				setTimeOut11();   
								
            };

function signatureClear1() {
	var canvas = document.getElementById("newSignature1");
	var context = canvas.getContext("2d");
	document.getElementById("imgsrc").src = context;
	context.clearRect(0, 0, canvas.width, canvas.height);
}
function setTimeOut11()
{
	setTimeout(window.close, 1000);         
}

</script>

<form enctype="multipart/form-data" method="post" accept-charset="utf-8" class="panel-wizard" novalidate="novalidate" name="basicForm" id="basicForm">
Signature
 <input autocorrect="off"  id="imgsrc" type="hidden" name="imgsrc"/>
		<input autocorrect="off"  id="a" type="hidden" name="a" value="<?=$_GET['a'];?>"/>
		<div id="newUserSignature">
        <img id="saveSignature1"/>
		</div>
									   <div id="canvas">
            <canvas class="roundCorners" id="newSignature1" style="position: relative; margin: 0; padding: 0; border: 1px solid #c4caac;"></canvas>
        </div>
        <script>signatureCapture1();</script>
		&nbsp;
		<div class="row" style="width:180px;">
		<div class="col-sm-3" style="float:left;width:50px;">
											 <button id="refresh" onclick="signatureSave1(<?=$_GET['a'];?>);setTimeout11();" type="button">Update</button>
											  </div>
										 <div class="col-sm-3" style="float:right;width:120px;">
										 
											  <button type="button" onclick="signatureClear1()">Clear signature</button>
											  </div>
											  
										 </div>
		</form>								 
										 
	<script src="signature.js"></script>