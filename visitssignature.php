<script>
    window.onunload = refreshParent;

    function refreshParent() {
        if (window.opener){
            window.opener.document.getElementById("imgDiv").innerHTML = document.getElementById("newUserSignature").innerHTML;
        }
    }

    function signatureCapture1() {
        var canvas = document.getElementById("newSignature1");
        var ctx = canvas.getContext("2d");
        ctx.strokeStyle = "#000000";
        ctx.fillStyle = "#000000";
        ctx.lineWidth = 2;
        ctx.lineCap = "round";

        // Set up mouse events for drawing
        var drawing = false;
        var mousePos = {x: 0, y: 0};
        var lastPos = mousePos;
        canvas.addEventListener("mousedown", function (e) {
            drawing = true;
            lastPos = getMousePos(canvas, e);
        }, false);
        canvas.addEventListener("mouseup", function (e) {
            drawing = false;
        }, false);
        canvas.addEventListener("mousemove", function (e) {
            mousePos = getMousePos(canvas, e);
        }, false);

        // Get the position of the mouse relative to the canvas
        function getMousePos(canvasDom, mouseEvent) {
            var rect = canvasDom.getBoundingClientRect();
            return {
                x: mouseEvent.clientX - rect.left,
                y: mouseEvent.clientY - rect.top
            };
        }

        // Get a regular interval for drawing to the screen
        window.requestAnimFrame = (function (callback) {
            return window.requestAnimationFrame ||
                window.webkitRequestAnimationFrame ||
                window.mozRequestAnimationFrame ||
                window.oRequestAnimationFrame ||
                window.msRequestAnimaitonFrame ||
                function (callback) {
                    window.setTimeout(callback, 1000 / 60);
                };
        })();

        // Draw to the canvas
        function renderCanvas() {
            if (drawing) {
                ctx.moveTo(lastPos.x, lastPos.y);
                ctx.lineTo(mousePos.x, mousePos.y);
                ctx.stroke();
                lastPos = mousePos;
            }
        }

        // Allow for animation
        (function drawLoop() {
            requestAnimFrame(drawLoop);
            renderCanvas();
        })();

        // Set up touch events for mobile, etc
        canvas.addEventListener("touchstart", function (e) {
            mousePos = getTouchPos(canvas, e);
            var touch = e.touches[0];
            var mouseEvent = new MouseEvent("mousedown", {
                clientX: touch.clientX,
                clientY: touch.clientY
            });
            canvas.dispatchEvent(mouseEvent);
        }, false);
        canvas.addEventListener("touchend", function (e) {
            var mouseEvent = new MouseEvent("mouseup", {});
            canvas.dispatchEvent(mouseEvent);
        }, false);
        canvas.addEventListener("touchmove", function (e) {
            var touch = e.touches[0];
            var mouseEvent = new MouseEvent("mousemove", {
                clientX: touch.clientX,
                clientY: touch.clientY
            });
            canvas.dispatchEvent(mouseEvent);
        }, false);

        // Get the position of a touch relative to the canvas
        function getTouchPos(canvasDom, touchEvent) {
            var rect = canvasDom.getBoundingClientRect();
            return {
                x: touchEvent.touches[0].clientX - rect.left,
                y: touchEvent.touches[0].clientY - rect.top
            };
        }

        // Prevent scrolling when touching the canvas
        document.body.addEventListener("touchstart", function (e) {
            if (e.target == canvas) {
                e.preventDefault();
            }
        }, false);
        document.body.addEventListener("touchend", function (e) {
            if (e.target == canvas) {
                e.preventDefault();
            }
        }, false);
        document.body.addEventListener("touchmove", function (e) {
            if (e.target == canvas) {
                e.preventDefault();
            }
        }, false);
    }

    function signatureClear1() {
        var canvas = document.getElementById("newSignature1");
        canvas.width = canvas.width;

        var lineWidth = $('#line-width').val();

        var ctx = canvas.getContext("2d");
        ctx.strokeStyle = "#000000";
        ctx.fillStyle = "#000000";
        ctx.lineWidth = lineWidth;
        ctx.lineCap = "round";
    }

    function signatureSave1(a) {
        var canvas = document.getElementById("newSignature1");// save canvas image as data url (png format by default)
        var dataURL = canvas.toDataURL("image/png");
        document.getElementById("saveSignature1").src = dataURL;
        document.getElementById('imgsrc').value = dataURL;
        document.getElementById('a').value = a;
        var b = document.getElementById('b').value;

        var fd = new FormData(document.forms["basicForm"]);
        fd.append('a', a);
        fd.append('b', b);
        fd.append('imgsrc', dataURL);
        var xhr = new XMLHttpRequest();
        xhr.open('POST', 'upload_data.php', true);
        xhr.upload.onprogress = function (t) {
            if (t.lengthComputable) {
                var percentComplete = (t.loaded / t.total) * 100;
            }
        };
        xhr.send(fd);
    }


    //Line width change
    function lineWidthChange() {
        var canvas = document.getElementById("newSignature1");
        var ctx = canvas.getContext("2d");

        ctx.lineWidth = $('#line-width').val();
    }

    $(document).ready(function () {
        $('#line-width').on('change', lineWidthChange);
    });
</script>

<div class="col-sm-6">
    Draw your signature here
    <input autocorrect="off"  id="imgsrc" type="hidden" name="imgsrc" form="basicForm"/>
    <input autocorrect="off"  id="a" type="hidden" name="a" value="<?= $_GET['a']; ?>" form="basicForm"/>
    <input autocorrect="off"  id="b" type="hidden" name="b" value="<?php
    if (isset($_GET['b'])) {
        echo $_GET['b'];
    } else {
        echo $_SESSION['visit'];
    }
    ?>" form="basicForm"/>

    <div id="canvas">
        <canvas class="roundCorners" id="newSignature1" style="border: 1px solid #c4caac;" form="basicForm"></canvas>
    </div>

    <script>signatureCapture1();</script>
    <div style="display: flex; ">
        <button class="btn btn-success"
                onclick="signatureSave1(<?= $_GET['a']; ?>);"
                type="button"
                style="margin-right: 3px"
                form="basicForm">Update
        </button>
        <button class="btn btn-success"
                type="button"
                style="margin-right: 5px"
                onclick="signatureClear1()"
                form="basicForm">Clear signature
        </button>

        <select id="line-width"
                class="form-control"
                style="width: 60px; ">
            <option selected>&nbsp;1</option>
            <option>&nbsp;2</option>
            <option>&nbsp;3</option>
        </select>
    </div>
</div>

<script src="signature.js"></script>