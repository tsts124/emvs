<?php
include 'includes/header.php';
include 'includes/dbcon.php';

if ($_SESSION['user'] == '') {
    header('Location: emvs.php?action=index');
}

if (isset($_POST['action']) && ($_POST['action'] == "update")) {
    $upquery = $dbh->prepare("update collectors set personalcommentsActive='" . $_POST['act'] . "' where Id='" . $_POST['hid'] . "'");
    $upquery->execute();
}

?>
<script>
    function fnactive(id) {

        if (document.getElementById('alertcomments' + id).checked == true)
            document.getElementById('act').value = 1;
        else
            document.getElementById('act').value = 0;
        document.getElementById('action').value = "update";
        document.getElementById('hid').value = id;
        document.collector.submit();
    }
</script>

<style>

    table#basicTable tbody tr td:not(:nth-child(2)):not(:nth-child(7)) {
        text-align: center;
    }

    .link {
        color: #353434;
    }

    .link:hover {
        color: #C80109;
    }

    span.thisCheck > a {
        color: red;
    }

    .first {
        display: none;
    }

    .second {
        display: show;
    }

    #tooltip {
        text-align: center;
        color: #fff;
        background: #111;
        position: absolute;
        z-index: 100;
        padding: 15px;
    }

    #tooltip:after /* triangle decoration */
    {
        width: 0;
        height: 0;
        border-left: 10px solid transparent;
        border-right: 10px solid transparent;
        border-top: 10px solid #111;
        content: '';
        position: absolute;
        left: 50%;
        bottom: -10px;
        margin-left: -10px;
    }

    #tooltip.top:after {
        border-top-color: transparent;
        border-bottom: 10px solid #111;
        top: -20px;
        bottom: auto;
    }

    #tooltip.left:after {
        left: 10px;
        margin: 0;
    }

    #tooltip.right:after {
        right: 10px;
        left: auto;
        margin: 0;
    }

    #remove > div.personal-details.personal-label.my-label,
    #referenceId > div.personal-details.personal-label.my-label {
        width: 100%;
    }

    #referenceId > div.personal-details > input {
        margin-top: 7px;
    }

    #basicTable_filter > label > input {
        background-color: #eceff4;
        margin-top: 3px;
        margin-left: 0;
        color: #8493aa;
        border-radius: 3px;
        padding: 10px;
        height: auto;
        box-shadow: none;
        font-size: 13px;
        background-image: none;
        border: 1px solid #ccc;
        line-height: 1.42857143;
        display: block;
        width: 100%;
    }

    #basicTable_filter > label {
        white-space: nowrap;
        color: #8493a8;
        font-family: "Open Sans";
        font-size: 14px;
        font-weight: 700;
        margin-top: 1.7%;
        margin-bottom: 0px !important;
    }

    #basicTable_filter {
        margin-top: 0;
        height: 72.5px;
    }

    #remove > div:nth-child(2) {
        width: 100%;
    }

    #hiden {
        display: none;
    }

    #title {
        height: 40px;
    }

    .alertVisits a {
        color: red;
    }
</style>

<script>
    function refIdSearch(val) {

        var dataString = 'refVal=' + $(val).val() + '&isSearch=RefId';
        $.ajax({
            type: "POST",
            url: "ajax_search.php",
            data: dataString,
            success: function (res) {
                window.open(res.urlRef, "_self");
            }, error: function (jqXhr, textStatus, errorThrown) {
                alert('Can\'t find visit with provided Reference ID.');
            }
        });
    }
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>

<body>
<section>
    <div class="mainwrapper">
        <?php include 'includes/leftpanel.php'; ?>
        <div class="mainpanel">
            <div class="pageheader">
                <div>
                    <div class="media-body">
                        <h4>Collector List</h4>
                    </div>
                </div><!-- media -->
            </div><!-- pageheader -->
            <div class="contentpanel">
                <div class="panel panel-primary-head content-padding">
                    <div class="row">
                        <div class="col-sm-12" id="rowSearch">
                            <div id="remove" class="col-md-5">
                                <div>
                                        <?php
                                        if (strlen($dataquery['dob']) > 4) {
                                            $onlyYear = false;

                                            $parts = parserDate($dataquery['dob']);
                                            $month = $parts['month'];
                                            $day = $parts['day'];
                                            $year = $parts['year'];
                                        } elseif (strlen($dataquery['dob']) < 5 && $dataquery['dob']) {
                                            $onlyYear = true;
                                        }

                                        $months = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];

                                        $days = 31;
                                        switch ($month) {
                                            case '2':
                                                $days = 29;
                                                break;
                                            case '4':
                                            case '6':
                                            case '9':
                                            case '11':
                                                $days = 30;
                                                break;
                                            //Jan, Mar, May, Jul, Aug, Oct, Dec
                                            default:
                                                $days = 31;
                                        }

                                        $years = [];
                                        for ($i = 1930; $i <= 2017; $i++) {
                                            $years[] = $i;
                                        }
                                        ?>
                                        <select tabindex="4" name="month" id="month" class="select-birth-collector">
                                            <option value="0"> Month</option>
                                            <?php foreach ($months as $m): ?>
                                                <option <?php if ($m == $month) {
                                                    echo 'selected';
                                                } ?> ><?= $m; ?></option>
                                            <?php endforeach; ?>
                                        </select>

                                        <select tabindex="5" name="day" id="day" class="select-birth-collector">
                                            <option value="0"> Day</option>
                                            <?php for ($d = 1; $d <= $days; $d++): ?>
                                                <option <?php if ($day == $d) {
                                                    echo 'selected';
                                                } ?> ><?= $d; ?></option>
                                            <?php endfor; ?>
                                        </select>

                                        <select tabindex="6" name="year" id="year" class="select-birth-collector">
                                            <option value="0"> Year</option>
                                            <?php foreach ($years as $y): ?>
                                                <option <?php
                                                if (($y == $year) || ($y == $dataquery['dob'])) {
                                                    echo 'selected';
                                                } ?> ><?= $y; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>

                            </div>
                            <div id="referenceId" class="col-md-4">
                                <div class="personal-details">
                                    <input autocorrect="off" type="text"
                                           tabindex="7"
                                           onkeypress="Maxval(this.value,6,this.id); return onlyAlphabets(event,this)"
                                           name="referenceId"
                                           id="referenceId"
                                           value="" class="form-control personal-input text-capitalize "
                                           onchange="refIdSearch(this)"
                                           required
                                           placeholder="Reference Id"
                                    >
                                    <div id="counterreferenceId"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div id="hiden">
                                <div class="col-md-3">
                                    <select id="title"
                                            name="title"
                                            class="form-control personal-input text-capitalize "
                                            required
                                            tabindex="8">
                                        <option value="">Choose One</option>
                                        <?php
                                        $sitessql = $dbh->prepare('SELECT *
                                                                   FROM `title`
                                                                   WHERE `Delete` = 0; ');
                                        $sitessql->execute();
                                        while ($sitesdata = $sitessql->fetch()) : ?>
                                            <option <?php
                                            if ($dataquery['title'] == $sitesdata['title']) {
                                                echo 'selected';
                                            } ?> ><?= $sitesdata['title'] ?></option>
                                        <?php endwhile; ?>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <input autocorrect="off" type="text"
                                           tabindex="9"
                                           onkeypress="Maxval(this.value,30,this.id)"
                                           name="firstname"
                                           id="firstname"
                                           value=""
                                           class="form-control personal-input text-capitalize  text-capitalize"
                                           required
                                           placeholder="First Name"
                                    >
                                    <div id="counterfirstname"></div>
                                </div>
                                <div class="col-md-3">
                                    <input autocorrect="off" type="text"
                                           tabindex="10"
                                           onkeypress="Maxval(this.value,20,this.id)"
                                           name="lastname"
                                           id="lastname"
                                           value="" class="form-control personal-input text-capitalize "
                                           required
                                           placeholder="Last Name"
                                    >
                                    <div id="counterlastname"></div>
                                </div>
                                <button disabled class="btn btn-warning col-md-3" style="line-height: 25px;margin-top: 5px;">
                                    Add New Collector
                                </button>
                            </div>
                        </div>
                    </div>


                    <form action="" name="collector" id="collector" method="POST">
                        <div class="adsearchtable">
                            <table id="basicTable" class="basic-table table table-striped table-bordered responsive">
                                <thead class="">
                                <tr>
                                    <th style="text-align:center">I.R.No</th>
                                    <th style="text-align:center">First Name, Last Name</th>
                                    <th style="text-align:center">Country</th>
                                    <th style="text-align:center">ID Type</th>
                                    <th style="text-align:center">Primary ID No.</th>
                                    <th style="text-align:center">DOB</th>
                                    <th style="text-align:center">Phone No.</th>
                                    <th style="text-align:center">Alerts</th>
                                    <th style="text-align:center">#Visits</th>
                                    <th style="text-align:center">Action</th>
                                    <th style="text-align:center">Delete</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                        <input type="hidden" name="action" id="action"/>
                        <input type="hidden" name="hid" id="hid"/>
                        <input type="hidden" name="act" id="act"/>
                    </form>
                </div><!-- panel -->
            </div><!-- contentpanel -->
        </div><!-- mainpanel -->
    </div><!-- mainwrapper -->
</section>

<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/jquery-migrate-1.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/modernizr.min.js"></script>
<script src="js/ppace.min.js"></script>

<script src="js/jquery.cookies.js"></script>

<script src="js/jquery.dataTables.min.js"></script>
<script src="js/dataTables.bootstrap.js"></script>
<script src="js/dataTables.responsive.js"></script>
<script src="js/select2.min.js"></script>

<script src="js/custom.js"></script>

<script>

    function fndelete(id) {
        if (confirm("Do you really wish to delete this record?")) {
            var data = 'id=' + id;
            $.ajax({
                type: 'POST',
                url: 'ajax_delete_collector.php',
                data: data,
                success: function (msg) {
                    location.reload();
                }
            });
        }
    }

    $(document).ready(function () {
        var alertCollectorClass;
        var alertVisitsClass;
        var classes;
        var mytable = $('#basicTable').DataTable({
            order: [[0, "desc"]],
            "processing": true,
            "serverSide": false,
            "bFilter": true,
            "bLengthChange": false,
            "ajax": "ajax/dataTableCollector.php",
            "responsive": true,
            "oLanguage": { "sSearch": "" },
            "deferRender": true,
            "autoWidth": false,
            "columnDefs": [
                {
                    "render": function (data, type, row) {
                        alertCollectorClass = row[6].length ? 'thisCheck' : '';
                        alertVisitsClass = row[15] > 0 ? 'alertVisits' : '';
                        classes = ' ' + alertCollectorClass + ' ' + alertVisitsClass + ' ';

                        var content = '<span class="' + classes +'">';
                        content += '<a class="link" href="emvs.php?action=novisit&&no=' + row[0] + '">';
                        content += row[0];
                        content += '</a>';
                        content += '</span>';

                        return content;
                    },
                    "targets": 0
                },
                {
                    "render": function (data, type, row) {
                        var content = '<span class="' + classes +'">';
                        content += '<a class="link" href="emvs.php?action=novisit&&no=' + row[0] + '">';
                        content += row[1] + ' ' + row[2] + ' ' + row[3] + ' ' + row[4];
                        content += '</a>';
                        content += '</span>';

                        return content;
                    },
                    "targets": 1
                },
                {
                    "render": function (data, type, row) {

                        var content = "";
                        if (row[12] != null) {
                            content += '<span class="' + classes + '">';
                            content += '<a class="link" href="emvs.php?action=novisit&&no=' + row[0] + '">';
                            content += row[12];
                            content += '</a>';
                            content += '</span>';
                        }

                        return content;
                    },
                    "targets": 2
                },
                {
                    "render": function (data, type, row) {

                        var content = "";
                        if (row[10] != null) {
                            content += '<span class="' + classes + '">';
                            content += '<a class="link" href="emvs.php?action=novisit&&no=' + row[0] + '">';
                            content += row[10];
                            content += '</a>';
                            content += '</span>';
                        }
                        return content;
                    },
                    "targets": 3
                },
                {
                    "render": function (data, type, row) {

                        var content = "";
                        if (row[13] != null) {
                            content += '<span class="' + classes + '">';
                            content += '<a class="link" href="emvs.php?action=novisit&&no=' + row[0] + '">';
                            content += row[13];
                            content += '</a>';
                            content += '</span>';
                        }
                        return content;
                    },
                    "targets": 4
                },
                {
                    "render": function (data, type, row) {
                        var content = '<span class="' + classes + '">';
                        content += '<a class="link" href="emvs.php?action=novisit&&no=' + row[0] + '">';
                        content += row[7];
                        content += '</a>';
                        content += '</span>';

                        return content;
                    },
                    "targets": 5
                },
                {
                    "render": function (data, type, row) {

                        var content = "";
                        if (row[11] != null) {
                            content += '<span class="' + classes + '">';
                            content += '<a class="link" href="emvs.php?action=novisit&&no=' + row[0] + '">';
                            content += row[11];
                            content += '</a>';
                            content += '</span>';
                        }
                        return content;
                    },
                    "targets": 6
                },
                {
                    "render": function (data, type, row) {

                        var img = "";

                        if (alertCollectorClass.length || alertVisitsClass.length) {
                            img = '<img src="images/ticksmall.gif"width="20" height="20">';
                        } else {
                            img = '<img src="images/empty.png" width="17" height="17">';
                        }

                        return img;
                    },
                    "targets": 7
                },
                {
                    "render": function (data, type, row) {

                        var content = "";
                        if (row[14] != null) {
                            content += '<span class="' + classes + '">';
                            content += '<a class="link" href="emvs.php?action=novisit&&no=' + row[0] + '">';
                            if (row[14] != 0) {
                                content += row[14];
                            }
                            content += '</a>';
                            content += '</span>';
                        }
                        return content;
                    },
                    "targets": 8
                },
                {
                    "render": function (data, type, row) {
                        var content = '<span class="' + classes + '">';
                        content += '<a class="link" href="emvs.php?action=member&&abc=' + row[0] + '">';
                        content += '<img src="images/edit1.png" border="0" data-toggle="tooltip" title="Edit Details" alt="EMVS"></a>';
                        content += '</span>';
                        return content;
                    },
                    "targets": 9
                },
                {
                    "render": function (data, type, row) {
                        var content = '<a href="javascript:fndelete(' + row[0] + ')">' +
                            '<img src="images/delete1.gif" border="0" data-toggle="tooltip"' +
                            ' title="Delete" alt="EMVS"></a>';
                        return content;
                    },
                    "targets": 10
                }
            ]
        });

        var shTable = jQuery('#shTable').DataTable({
            "fnDrawCallback": function (oSettings) {
                jQuery('#shTable_paginate ul').addClass('pagination-active-dark');
            },
            responsive: true
        });

        // Show/Hide Columns Dropdown
        jQuery('#shCol').click(function (event) {
            event.stopPropagation();
        });

        jQuery('#shCol input').on('click', function () {

            // Get the column API object
            var column = shTable.column($(this).val());

            // Toggle the visibility
            if ($(this).is(':checked'))
                column.visible(true);
            else
                column.visible(false);
        });

        // DataTables Length to Select2
        jQuery('div.dataTables_length select').removeClass('form-control input-sm');
        jQuery('div.dataTables_length select').css({width: '60px'});
        jQuery('div.dataTables_length select').select2({
            minimumResultsForSearch: -1
        });

        $('table').on('click touch', '.thisCheck', promptPassword);

        $.fn.dataTable.ext.search.push(
            function (settings, data, dataIndex) {
                var year = $('#year').val();
                var month = $('#month').val();
                var day = $('#day').val();
                var firstName = $('#firstname').val();
                var lastName = $('#lastname').val();

                var collectorYear = data[5].split('-')[2] || '';
                var collectorDay = data[5].split('-')[1] || '';
                var collectorMonth = data[5].split('-')[0] || '';
                var collectorFirstName = data[1].split(' ')[1] || '';
                var collectorLastName = data[1].split(' ')[3] || '';

                if ((collectorYear.indexOf(year) + 1 || (year === '0'))
                    && (collectorMonth.indexOf(month) + 1 || (month === '0'))
                    && (collectorDay.indexOf(day) + 1 || (day === '0'))
                    && (collectorFirstName.toLowerCase().substr(0, firstName.length).indexOf(firstName.toLowerCase()) + 1 || (firstName === ''))
                    && (collectorLastName.toLowerCase().substr(0, lastName.length).indexOf(lastName.toLowerCase()) + 1 || (lastName === ''))
                ) {
                    return true;
                }
                return false;
            }
        );

        $('#lastname , #firstname').keyup(function () {
            $('#basicTable').DataTable().draw();
            validForm();
        });

        $('#year, #day, #month, #hidenYear').change(function () {
            $('#basicTable').DataTable().draw();

            $('#hiden').css('display', 'block');
            validForm();
        });
        $('#hiden > button').click(function () {
            var year = $('#year').val();
            var month = $('#month').val();
            var day = $('#day').val();
            var firstName = $('#firstname').val();
            var lastName = $('#lastname').val();

            var query = '&title=' + $('#title').val()
                + '&firstname=' + firstName
                + '&lastname=' + lastName
                + '&year=' + year
                + '&month=' + month
                + '&day=' + day;

            window.location = "emvs.php?action=member" + query;
        });

        $('#basicTable_filter').addClass('col-md-3');
        $('#rowSearch').prepend($('#basicTable_filter'));
        $('#rowSearch').prepend($('#remove'));
        $('#basicTable_filter > label > input').attr('placeholder', 'Search')
    });//end ready

    function validForm() {
        var year = $('#year').val();
        var firstName = $('#firstname').val();
        var lastName = $('#lastname').val();

        if (year !== '0' && firstName !== '' && lastName !== '') {
            $('#hiden > button').attr('disabled', false);
        } else {
            $('#hiden > button').attr('disabled', true);
        }

    }


    function format(d) {
        // `d` is the original data object for the row
        return '<table class="table table-bordered nomargin">' +
            '<tr>' +
            '<td>Full name:</td>' +
            '<td>' + d.name + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Extension number:</td>' +
            '<td>' + d.extn + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Extra info:</td>' +
            '<td>And any further details here (images etc)...</td>' +
            '</tr>' +
            '</table>';
    }
</script>

</body>
