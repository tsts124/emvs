<?php 
	include 'includes/header.php'; 
	include 'includes/dbcon.php'; 
if($_SESSION['user']==''){
		header('Location: emvs.php?action=index');
	}
?>
<style type="text/css">
.test{
	color:#ffffff;
}
.test:hover {
	color:#ffffff;
	background-color:#EEEEEE;
	padding:5px;
}
</style>
        <section>
            <div class="mainwrapper">
                  <?php include 'includes/leftpanel.php'; ?>
                
                <div class="mainpanel">
                    <div class="pageheader">
                        <div class="media">                            
                            <div class="media-body">                               
                                <h4>Collector Search</h4>
                            </div>
                        </div><!-- media -->
                    </div><!-- pageheader -->
                    
                    <div class="contentpanel">                        
                        <div class="panel panel-primary-head">						    
                           
                            
                            <table id="basicTable" class="basic-table table table-striped table-bordered responsive">
							
                                <thead class="">
                                    <tr>
                                        <th>S.No</th>
										<th>Title</th>
                                        <th>First Name</th>                                        
                                        <th>Last Name</th>
                                        <th>DOB</th>                                        
										<th>No.of Visits</th>
										<th>Action</th>
										
										<th>Add Visits</th>
                                    </tr>
                                </thead>
                         
                                <tbody>
									<?php
										#echo "select * from collectors order by Id desc";
										$sql2 = $dbh->prepare("select * from collectors order by Id desc");
										$sql2->execute();
										$i=1;
										while($data2 = $sql2->fetch()){
									?>
                                    <tr class="test">
                                        <td><a href="#"><?=$i;?></a></td>
										<td><a href="#"><?=$data2['title'];?></a></td>
                                        <td><a href="#"><?=$data2['firstname'];?></a></td>                                 
										<td><a href="#"><?=$data2['lastname'];?></a></td>
                                        <td><a href="#"><?php $dateofbirth = parserDate($data2['dob']); echo $dateofbirth['day'].'-'.$dateofbirth['year'].'-'.$dateofbirth['month'];?></a></td>
										<td><a href="#"><?php  $vi = $data2['Id'];
											$vsql = $dbh->prepare("select MAX(visitid) AS visitid from visitstable where collectorsid='$vi'");
											$vsql->execute();
											$vdata = $vsql->fetch(); echo '(&nbsp'.$vdata['visitid'].'&nbsp)'; ?></a></td>
										<td><a href="emvs.php?action=member&&abc=<?=$data2['Id'];?>"><img src="images/edit1.png" border="0" alt="EMVS"></a> </td>
										<td><a href="emvs.php?action=visitsadd&&a=<?=$data2['Id'];?>"><img src="images/addvisits.png" border="0" alt="EMVS"></a> </td>
										
										
										
                                    </tr>
										<?php $i++; } ?>
                                                                  
                                </tbody>
                            </table>
                        </div><!-- panel -->
                        
                        <br />
                                              
                    </div><!-- contentpanel -->
                </div><!-- mainpanel -->
            </div><!-- mainwrapper -->
        </section>

        <script src="js/jquery-1.11.1.min.js"></script>
        <script src="js/jquery-migrate-1.2.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/modernizr.min.js"></script>
        <script src="js/ppace.min.js"></script>

        <script src="js/jquery.cookies.js"></script>
        
        <script src="js/jquery.dataTables.min.js"></script>
        <script src="js/dataTables.bootstrap.js"></script>
        <script src="js/dataTables.responsive.js"></script>
        <script src="js/select2.min.js"></script>

        <script src="js/custom.js"></script>
        <script>
            jQuery(document).ready(function(){
                
                jQuery('#basicTable').DataTable({
                    responsive: true
                });
                
                var shTable = jQuery('#shTable').DataTable({
                    "fnDrawCallback": function(oSettings) {
                        jQuery('#shTable_paginate ul').addClass('pagination-active-dark');
                    },
                    responsive: true
                });
                
                // Show/Hide Columns Dropdown
                jQuery('#shCol').click(function(event){
                    event.stopPropagation();
                });
                
                jQuery('#shCol input').on('click', function() {

                    // Get the column API object
                    var column = shTable.column($(this).val());
 
                    // Toggle the visibility
                    if ($(this).is(':checked'))
                        column.visible(true);
                    else
                        column.visible(false);
                });
                
                var exRowTable = jQuery('#exRowTable').DataTable({
                    responsive: true,
                    "fnDrawCallback": function(oSettings) {
                        jQuery('#exRowTable_paginate ul').addClass('pagination-active-success');
                    },
                    "ajax": "ajax/objects.txt",
                    "columns": [
                        {
                            "class":          'details-control',
                            "orderable":      false,
                            "data":           null,
                            "defaultContent": ''
                        },
                        { "data": "name" },
                        { "data": "position" },
                        { "data": "office" },
                        { "data": "salary" }
                    ],
                    "order": [[1, 'asc']] 
                });
                
				
                // Add event listener for opening and closing details
                jQuery('#exRowTable tbody').on('click', 'td.details-control', function () {
                    var tr = $(this).closest('tr');
                    var row = exRowTable.row( tr );
             
                    if ( row.child.isShown() ) {
                        // This row is already open - close it
                        row.child.hide();
                        tr.removeClass('shown');
                    }
                    else {
                        // Open this row
                        row.child( format(row.data()) ).show();
                        tr.addClass('shown');
                    }
                });
               
                
                // DataTables Length to Select2
                jQuery('div.dataTables_length select').removeClass('form-control input-sm');
                jQuery('div.dataTables_length select').css({width: '60px'});
                jQuery('div.dataTables_length select').select2({
                    minimumResultsForSearch: -1
                });
    
            });
            
            function format (d) {
                // `d` is the original data object for the row
                return '<table class="table table-bordered nomargin">'+
                    '<tr>'+
                        '<td>Full name:</td>'+
                        '<td>'+d.name+'</td>'+
                    '</tr>'+
                    '<tr>'+
                        '<td>Extension number:</td>'+
                        '<td>'+d.extn+'</td>'+
                    '</tr>'+
                    '<tr>'+
                        '<td>Extra info:</td>'+
                        '<td>And any further details here (images etc)...</td>'+
                    '</tr>'+
                '</table>';
            }
        </script>

    </body>
</html>