<?php
$ssn = $_POST['ssn'];
$ssncount = $_POST['ssncount'];

if ($_POST['number'] != 'undefined') {
    $number = $_POST['number'];
} else {
    $number = '';
}

if ($_POST['country'] != 'undefined') {
    $country = $_POST['country'];
} else {
    $country = '';
}

include 'includes/dbcon.php';
?>
<!-- SSN Number Format-->
<script type="text/javascript" src="js/jquery.SimpleMask.js"></script>
<script type="text/javascript">
    $('#ssn<?=$ssncount;?>').simpleMask({'mask': 'ssn', 'nextInput': false});
    function blockSpecialChar(e) {
        var k = e.keyCode;
        return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 45 || (k >= 48 && k <= 57));
    }
    function idnumdup(val, lang) {
        var dataString = 'idvalue=' + val + '&idlang=' + lang;
        $.ajax({
            type: "POST",
            url: "duplication.php",
            data: dataString,
            success: function (msg) {
                $('#idnumdup' + lang).html(msg);
                var iddup = document.getElementById("duplication").style.visibility;
            }
        });
    }
</script>
<?php if ($ssn == 'SSN / ID Number') { ?>
    <div class="col-sm-2 col-xs-2">
        <input autocorrect="off" name="Collcountry[]"
               id="identificationcountry<?= $ssncount; ?>"
               onfocus="identCountryListener(this.id)"
               lang="<?= $ssncount; ?>"
               onkeypress="Maxval(this.value,30,this.id); return onlyAlphabets(event,this)"
               value="<?php if ($country) {
                   echo $country;
               } else {
                   echo 'USA';
               } ?>"
               class="form-control "
               placeholder="Country"
               style="text-transform:uppercase;">
        <div id="counteridentificationcountry<?= $ssncount; ?>"></div>
    </div>
    <div class="col-sm-2 col-xs-2">
        <input autocorrect="off" type="hidden" value="<?= $ssncount; ?>" id="identiid">
        <input autocorrect="off" type="text"
               lang="<?= $ssncount; ?>"
               name="Collphone[]"
               value="<?= $number ?>"
               placeholder="Number"
               autocomplete="off"
               onkeypress="Maxval(this.value,20,this.id)"
               onchange="$(this).simpleMask( { 'mask': 'ssn', 'nextInput': false } );"
               class="form-control "
               id="ssn<?= $ssncount; ?>">
        <div id="counterssn<?= $ssncount; ?>"></div>
    </div>

<?php } else if ($ssn == 'Teudat Zeut') { ?>
    <div class="col-sm-2 col-xs-2">
        <input autocorrect="off" name="Collcountry[]"
               lang="<?= $ssncount; ?>"
               id="identificationcountry<?= $ssncount; ?>"
               onfocus="identCountryListener(this.id)"
               onkeypress="Maxval(this.value,30,this.id); return onlyAlphabets(event,this)"
               value="<?php if ($country) {
                   echo $country;
               } else {
                   echo 'ISRAEL';
               } ?>"
               placeholder="Country"
               class="form-control "
               style="text-transform:uppercase;">
        <div id="counteridentificationcountry<?= $ssncount; ?>"></div>
    </div>
    <div class="col-sm-2 col-xs-2">
        <input autocorrect="off" type="text"
               class="form-control "
               autocomplete="off"
               name="Collphone[]"
               value="<?= $number ?>"
               onclick="Chkval();"
               lang="<?= $ssncount; ?>"
               placeholder="Number"
               onkeypress="Maxval(this.value,20,this.id)"
               id="ssnid<?= $ssncount; ?>">
        <div id="counterssnid<?= $ssncount; ?>"></div>
    </div>
<?php } elseif ($ssn == 'Passport') { ?>
    <div class="col-sm-2 col-xs-2">
        <input autocorrect="off" name="Collcountry[]"
               lang="<?= $ssncount; ?>"
               id="identificationcountry<?= $ssncount; ?>"
               onfocus="identCountryListener(this.id)"
               onkeypress="Maxval(this.value,30,this.id); return onlyAlphabets(event,this)"
               class="form-control "
               value="<?= $country ?>"
               placeholder="Country"
               style="text-transform: uppercase;">
        <div id="counteridentificationcountry<?= $ssncount; ?>"></div>
    </div>
    <div class="col-sm-2 col-xs-2">
        <input autocorrect="off" type="text"
               class="form-control "
               autocomplete="off"
               name="Collphone[]"
               value="<?= $number ?>"
               onclick="Chkval();"
               lang="<?= $ssncount; ?>"
               onkeypress="Maxval(this.value,20,this.id)"
               placeholder="Number"
               id="ssnid<?= $ssncount; ?>">
        <div id="counterssnid<?= $ssncount; ?>"></div>
    </div>
    <div class="col-sm-2 col-xs-2">
        <input autocorrect="off" name="passport_name[]"
               placeholder="Passport Name"
               onkeypress="Maxval(this.value,35,this.id); return onlyAlphabets(event,this)"
               lang="<?= $ssncount; ?>"
               id="passport_name<?= $ssncount; ?>"
               class="form-control ">
        <div id="counterpassport_name<?= $ssncount; ?>"></div>
    </div>
<?php } else { ?>
    <div class="col-sm-2 col-xs-2">
        <input autocorrect="off" type="text"
               style="text-transform:uppercase;"
               class="form-control  valid"
               id="identificationcountry<?= $ssncount; ?>"
               name="Collcountry[]"
               placeholder="Country"
               value="<?= $country ?>"
               onkeypress="Maxval(this.value,30,this.id); return onlyAlphabets(event,this)"
               onfocus="identCountryListener(this.id)"
               lang="<?= $newId; ?>"
               onchange="myselectId1(this.value,this.lang);IdValidation(this.lang,this.value);">
        <div id="counteridentificationcountry<?= $ssncount; ?>"></div>
    </div>

    <div class="col-sm-2 col-xs-2">
        <input autocorrect="off" type="text"
               autocomplete="off"
               lang="<?= $ssncount; ?>"
               class="form-control "
               name="Collphone[]"
               placeholder="Number"
               value="<?= $number ?>"
               onclick="Chkval();"
               onkeypress="Maxval(this.value,20,this.id)"
               id="ssnid<?= $ssncount; ?>">
        <div id="counterssnid<?= $ssncount; ?>"></div>
    </div>
<?php } ?>
