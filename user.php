<?php 
	include 'includes/header.php'; 
	if($_SESSION['user']==''){
		header('Location: emvs.php?action=index');
	}
?>
        <section>
            <div class="mainwrapper">
                  <?php include 'includes/leftpanel.php'; ?>
                
                
                
                
                <div class="mainpanel">
                                         <div class="pageheader">
                        <div class="media">                            
                            <div class="media-body">                               
                                <h4>Admin Details</h4>
								<h5><button class="btn btn-primary"><a href="emvs.php?action=new_member" style="color:#fff"> New Member</a></button> <button class="btn btn-primary"><a href="emvs.php?action=tools"  style="color:#fff"> Tools</a></button> <button class="btn btn-primary"><a href="#"  style="color:#fff"> Help</a></button></h5>
                            </div>
                        </div><!-- media -->
                    </div><!-- pageheader -->
                    
                    <div class="contentpanel">                                               
                        <div class="row">
                            <div class="col-md-6">
                               <div class="panel panel-default">
                                    <div class="panel-heading">                                        
                                        <h4 class="panel-title">Email</h4>                                      
                                    </div><!-- panel-heading -->
                                    <div class="panel-body">
                                        <p><button class="btn btn-primary btn-block">david@ultimatetechnical.com</button></p>
                                        <p><button class="btn btn-success btn-sm btn-block">david@dgbrenner.com </button></p>
										<p><button class="btn btn-warning btn-sm btn-block">Egerstenfeld@comcast.net</button></p>
										<p><button class="btn btn-danger btn-sm btn-block">kirk.mavroulis@ultimatetechnical.com</button></p>
										<p><button class="btn btn-info btn-sm btn-block">fidelitystock@yahoo.com </button></p>										
                                        <p><button class="btn btn-default btn-lg btn-block">stcornberg83@gmail.com</button></p>
                                    </div><!-- btn-body -->
                                </div>
                                
                            </div><!-- col-md-6 -->
                            
                            <div class="col-md-6"> 
                                <div class="panel panel-default">
                                    <div class="panel-heading">                                       
                                        <h4 class="panel-title">User Name</h4>
                                        <p>This is an example of form with block styled label.</p>
                                    </div>
                                    
                                </div><!-- panel -->
                                
								<div class="panel panel-default">
                                    <div class="panel-heading">                                       
                                        <h4 class="panel-title">Email Address</h4>
                                        <p>Prabu@tossolutions.net</p>
                                    </div>
                                    
                                </div><!-- panel -->
								
								<div class="panel panel-default">
                                    <div class="panel-heading">                                       
                                        <h4 class="panel-title">Roles</h4>
                                        <p>None</p>
                                    </div>
                                    
                                </div><!-- panel -->
								
								<div class="panel panel-default">
                                    <div class="panel-heading">                                       
                                        <h4 class="panel-title">Status</h4>
                                        <p>None</p>
                                    </div>
                                    
                                </div><!-- panel -->
								
								<div class="panel panel-default">
                                    <div class="panel-heading">                                       
                                        <h4 class="panel-title">Roles</h4>
                                        <p>Not Approved</p>
                                    </div>
                                    
                                </div><!-- panel -->
								
								<div class="panel panel-default">
                                    <div class="panel-heading">                                       
                                        <h4 class="panel-title">Roles</h4>
                                        <p>None</p>
										<p>Created By</p>
                                    </div>
                                    
                                </div><!-- panel -->
								
								<div class="panel panel-default">
                                    <div class="panel-heading">                                       
                                        <h4 class="panel-title">Login</h4>
                                        <p>Last Login</p>
										<p>Last Activity</p>
                                    </div>
                                    
                                </div><!-- panel -->
								
								<div class="panel panel-default">
                                    <div class="panel-heading">                                       
                                        <h4 class="panel-title">Password</h4>
                                        <p>************</p>										
                                    </div>
                                    
                                </div><!-- panel -->
								
								<div class="panel panel-default">
                                    <div class="panel-heading">                                       
                                        <h4 class="panel-title">Question and Answer</h4>
                                        <p>Question</p>
                                    </div>
                                    
                                </div><!-- panel -->
								
								<div class="panel panel-default">
                                    <div class="panel-heading">                                       
                                        <h4 class="panel-title">Comments</h4>                                       
                                    </div>
                                    
                                </div><!-- panel -->
								<div class="panel panel-default">
                                    <div class="panel-heading">                                       
                                        <h4 class="panel-title">User ID</h4>                                       
                                    </div>
                                    
                                </div><!-- panel -->
								
								
                            </div><!-- col-md-6 --> 
                        </div><!-- row -->
                    
                    </div>
                </div><!-- mainpanel -->
            </div><!-- mainwrapper -->
        </section>

        <script src="js/jquery-1.11.1.min.js"></script>
        <script src="js/jquery-migrate-1.2.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/modernizr.min.js"></script>
        <script src="js/ppace.min.js"></script>

        <script src="js/jquery.cookies.js"></script>
        
        <script src="js/jquery.dataTables.min.js"></script>
        <script src="js/dataTables.bootstrap.js"></script>
        <script src="js/dataTables.responsive.js"></script>
        <script src="js/select2.min.js"></script>

        <script src="js/custom.js"></script>
        <script>
            jQuery(document).ready(function(){
                
                jQuery('#basicTable').DataTable({
                    responsive: true
                });
                
                var shTable = jQuery('#shTable').DataTable({
                    "fnDrawCallback": function(oSettings) {
                        jQuery('#shTable_paginate ul').addClass('pagination-active-dark');
                    },
                    responsive: true
                });
                
                // Show/Hide Columns Dropdown
                jQuery('#shCol').click(function(event){
                    event.stopPropagation();
                });
                
                jQuery('#shCol input').on('click', function() {

                    // Get the column API object
                    var column = shTable.column($(this).val());
 
                    // Toggle the visibility
                    if ($(this).is(':checked'))
                        column.visible(true);
                    else
                        column.visible(false);
                });
                
                var exRowTable = jQuery('#exRowTable').DataTable({
                    responsive: true,
                    "fnDrawCallback": function(oSettings) {
                        jQuery('#exRowTable_paginate ul').addClass('pagination-active-success');
                    },
                    "ajax": "ajax/objects.txt",
                    "columns": [
                        {
                            "class":          'details-control',
                            "orderable":      false,
                            "data":           null,
                            "defaultContent": ''
                        },
                        { "data": "name" },
                        { "data": "position" },
                        { "data": "office" },
                        { "data": "salary" }
                    ],
                    "order": [[1, 'asc']] 
                });
                
                // Add event listener for opening and closing details
                jQuery('#exRowTable tbody').on('click', 'td.details-control', function () {
                    var tr = $(this).closest('tr');
                    var row = exRowTable.row( tr );
             
                    if ( row.child.isShown() ) {
                        // This row is already open - close it
                        row.child.hide();
                        tr.removeClass('shown');
                    }
                    else {
                        // Open this row
                        row.child( format(row.data()) ).show();
                        tr.addClass('shown');
                    }
                });
               
                
                // DataTables Length to Select2
                jQuery('div.dataTables_length select').removeClass('form-control input-sm');
                jQuery('div.dataTables_length select').css({width: '60px'});
                jQuery('div.dataTables_length select').select2({
                    minimumResultsForSearch: -1
                });
    
            });
            
            function format (d) {
                // `d` is the original data object for the row
                return '<table class="table table-bordered nomargin">'+
                    '<tr>'+
                        '<td>Full name:</td>'+
                        '<td>'+d.name+'</td>'+
                    '</tr>'+
                    '<tr>'+
                        '<td>Extension number:</td>'+
                        '<td>'+d.extn+'</td>'+
                    '</tr>'+
                    '<tr>'+
                        '<td>Extra info:</td>'+
                        '<td>And any further details here (images etc)...</td>'+
                    '</tr>'+
                '</table>';
            }
        </script>

    </body>
</html>
