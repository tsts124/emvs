<?php
//header("Cache-Control: no-cache, must-revalidate");
include 'includes/header.php';

/* @var $dbh PDO */
include 'includes/dbcon.php';

require_once './importHelper.php';

$currentUser = $_SESSION['user'];
$sth = $dbh->prepare("select * from newmember where emailid='$currentUser'");
$sth->execute();
$data = $sth->fetch();
$roles = $_SESSION['roles'] = $data['roles'];

?>

    <section>
        <div class="mainwrapper">
            <?php include 'includes/leftpanel.php'; ?>
            <div class="mainpanel">
                <div class="pageheader">
                    <div>
                        <div class="media-body">
                            <h4>Import data from XLSM</h4>
                        </div>
                    </div>
                    <!-- media -->
                </div>
                <?php
                if (isset($_GET['cleanup'])) {
                    $dbh->query('TRUNCATE `collectorphone`;');
                    $dbh->query('TRUNCATE `collectors`;');
                    $dbh->query('TRUNCATE `collident`;');

                    $sql = $dbh->prepare("SELECT * FROM `visitstable`;");
                    $sql->execute();
                    $visits = $sql->fetchAll();
                    foreach ($visits as $visit) {
                        if (file_exists('uploads/' . $visit['signname'])) {
                            unlink('uploads/' . $visit['signname']);
                        }
                        if (file_exists('uploads/' . $visit['photoname'])) {
                            unlink('uploads/' . $visit['photoname']);
                        }
                    }

                    $dbh->query('TRUNCATE `visitstable`;');

                    $sql = $dbh->prepare("SELECT * FROM `collphoto`;");
                    $sql->execute();
                    $photos = $sql->fetchAll();
                    foreach ($photos as $photo) {
                        unlink('uploads/' . $photo['name']);
                    }
                    $dbh->query('TRUNCATE `collphoto`;');
                    ?>
                    <div class="alert alert-danger" role="alert" style="margin-top: 50px; margin-left: 1%;">All
                        collectors data deleted
                    </div>
                <?php } ?>

                <?php if (isset($_FILES['uploadedFile']) && preg_match('~\.xlsm~', $_FILES['uploadedFile']['name']) == 1) { ?>
                    <?php
                    set_time_limit(0);

                    $startTime = microtime(true); //test

                    @mkdir('uploads/tmp/', 0777, TRUE);
                    $dest = "uploads/tmp/" . date('Y-m-d-H-i-s') . '.' . $_FILES['uploadedFile']['name'];

                    if (move_uploaded_file($_FILES['uploadedFile']['tmp_name'], $dest)) {

                        error_log(var_export(['file was uploaded', microtime(true) - $startTime], true)); //test

                        $documentData = ImportHelper::readDocument($dest);

                        error_log(var_export(['file was read', microtime(true) - $startTime], true)); //test


                        if (isset($_SESSION['dbg'])) {
                            ImportHelper::printDocument($dest);

                            error_log(var_export(['file was printed', microtime(true) - $startTime], true)); //test

                        }

                        $errors = ImportHelper::validateData($documentData);

                        error_log(var_export(['file was validated', microtime(true) - $startTime], true)); //test

                        if (count($errors) > 0) {
                            ImportHelper::printErrors($errors);

                            error_log(var_export(['errors were printed', microtime(true) - $startTime], true)); //test


                        } else {
                            ImportHelper::importData($documentData);

                            error_log(var_export(['file was imported', microtime(true) - $startTime], true)); //test

                        }
                    }

                    error_reporting(0);
                    ?>
                <?php } elseif (isset($_FILES['uploadedFile'])) {
                    echo
                    '<div class="alert alert-danger" role="alert" style="margin-top: 50px; margin-left: 1%;">
                                  <span class="sr-only">Error:</span>
                                  Please upload an xlsm file
                              </div>';
                } ?>

                <!-- pageheader -->
                <div class="contentpanel">
                    <div class="row row-stat">
                        <div class="col-md-6 mb8">
                            <form class="form-inline" method="POST" enctype="multipart/form-data"
                                  action="emvs.php?action=import&upload">
                                <div class="form-group">
                                    <label for="uploadedFile" class="sr-only">Choose file</label>
                                    <input autocorrect="off"  type="file" class="form-control" id="uploadedFile" name="uploadedFile"
                                           placeholder="Uploaded file">
                                </div>
                                <button type="submit" class="btn btn-default">Load file</button>
                            </form>
                        </div>
                        <div class="col-md-6 mb8 text-right">
                            <a href="template.xlsm" download class="btn btn-info-alt">Download template file</a>
                            <a id="cleanup" href="emvs.php?action=import&cleanup" class="btn btn-danger-alt">Cleanup
                                collectors data</a>
                        </div>
                    </div>
                </div>
                <!-- contentpanel -->
            </div>
            <!-- mainpanel -->
        </div>
        <!-- mainwrapper -->
    </section>
    <script>
        $(document).ready(function () {
            $('#cleanup').on('click', function (event) {
                if (!confirm('Are you sure?')) {
                    event.preventDefault();
                }
            });
        });
    </script>
<?php
include 'includes/footer.php';
