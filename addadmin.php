<?php
include 'includes/header.php';

include 'includes/function.php';
include 'includes/dbcon.php';
if ($_SESSION['user'] == '') {
    header('Location: emvs.php?action=index');
}
if (isset($_POST['createuser'])) {
    $userroles = addslashes(htmlspecialchars($_POST['userroles']));
    $username = addslashes(htmlspecialchars($_POST['username']));
    $firstname = addslashes(htmlspecialchars($_POST['firstname']));
    $lastname = addslashes(htmlspecialchars($_POST['lastname']));
    $emailid = addslashes(htmlspecialchars($_POST['emailid']));
    $status = addslashes(htmlspecialchars($_POST['status']));
    $pass1 = $_POST['password1'];
    $sites = addslashes(htmlspecialchars($_POST['sites']));

    $password = mc_encrypt($pass1, ENCRYPTION_KEY);

    $usercomments = addslashes(htmlspecialchars($_POST['usercomments']));
    $today = date('YmdHi');
    $startDate = date('YmdHi', strtotime('2012-03-14 09:06:00'));
    $range = $today - $startDate;
    $rand = rand(126234, $range);
    $a1 = "$rand" . ($startDate + $rand);
    $b = 'abcdef' . 'UVWXYZ';
    $d = $a1 . $b;
    $c = str_shuffle($d);
    $arr = str_split($c, 6);
    $userid = implode("-", $arr);
    if ($emailid) {
        $count = $dbh->prepare('INSERT INTO newmember (Username,
                                                       Sites,
                                                       Firstname,
                                                       Lastname,
                                                       password,
                                                       emailid,
                                                       userid,
                                                       roles,
                                                       status,
                                                       Addeddate,
                                                       comments)
                         VALUES (:uname,
                                 :sites,
                                 :fname,
                                 :lname,
                                 :pwd,
                                 :email,
                                 :userid,
                                 :roles,
                                 :stat,
                                 now(),
                                 :comments) ;');
        $count->execute([
            'uname' => $username,
            'sites' => $sites,
            'fname' => $firstname,
            'lname' => $lastname,
            'pwd' => $password,
            'email' => $emailid,
            'userid' => $userid,
            'roles' => $userroles,
            'stat' => $status,
            'comments' => $usercomments
        ]);
    }

    echo '<script type="text/javascript">window.location = "emvs.php?action=Administrator"</script>';
}
$a = htmlspecialchars(trim($_GET['a']));

if ($a) {
    $sqlquery = $dbh->prepare('select *
                               from `newmember`
                               where `emailid` = :id ;');
    $sqlquery->execute(['id' => $a]);
    $dataquery = $sqlquery->fetch();
}

if (isset($_POST['updatemember'])) {

    $userid = addslashes(htmlspecialchars($_POST['userid']));
    $firstname = addslashes(htmlspecialchars($_POST['firstname']));
    $lastname = addslashes(htmlspecialchars($_POST['lastname']));
    $emailid = addslashes(htmlspecialchars($_POST['emailid']));
    $status = addslashes(htmlspecialchars($_POST['status']));
    $sites = addslashes(htmlspecialchars($_POST['sites']));
    $pass1 = $_POST['password2'];
    $password = mc_encrypt($pass1, ENCRYPTION_KEY);
    $usercomments = addslashes(htmlspecialchars($_POST['usercomments']));
    $userroles = addslashes(htmlspecialchars($_POST['userroles']));
    if ($_GET['a'] == '') {
        $a = $_POST['useridd'];
    } else {
        $a = $_GET['a'];
    }

    if ($pass1 != '') {
        $sql2 = $dbh->prepare("select *
                               from newmember
                               where emailid ='$a'");
        $sql2->execute();
        $data2 = $sql2->fetch();
        $pl = $data2['pwdcount'] + 1;
        $password = mc_encrypt($pass1, ENCRYPTION_KEY);

        $count = $dbh->prepare('UPDATE `newmember`
                                SET `Firstname` = :firstname,
                                    `Lastname` = :lastname,
                                    `password` = :password,
                                    `emailid` = :email,
                                    `roles` = :role,
                                    `status` = :status,
                                    `comments` = :comments,
                                    `Sites` = :sites
                                WHERE `emailid` = :userid ;');
        $count->execute([
            'sites' => $sites,
            'firstname' => $firstname,
            'lastname' => $lastname,
            'email' => $emailid,
            'userid' => $a,
            'role' => $userroles,
            'password' => $password,
            'status' => $status,
            'comments' => $usercomments
        ]);
        $count1 = $dbh->prepare('UPDATE `newmember`
                                 SET `passwordchanged` = now(),
                                     `pwdcount` = :pwdcount
                                 WHERE `emailid` = :id ;');
        $count1->execute([
            'pwdcount' => $pl,
            'id' => $a
        ]);
        echo '<script type="text/javascript">window.location = "emvs.php?action=Administrator"</script>';
    } else {
        $count = $dbh->prepare('UPDATE `newmember`
                                SET `Firstname` = :firstname,
                                    `Lastname` = :lastname,
                                    `emailid` = :email,
                                    `roles` = :role,
                                    `status` = :status,
                                    `comments` = :comments,
                                    `Sites` = :sites
                                WHERE `emailid` = :userid ;');
        $count->execute([
            'sites' => $sites,
            'firstname' => $firstname,
            'lastname' => $lastname,
            'email' => $emailid,
            'userid' => $a,
            'role' => $userroles,
            'status' => $status,
            'comments' => $usercomments
        ]);
        echo '<script type="text/javascript">window.location = "emvs.php?action=Administrator"</script>';
    }
    if ($count == '1') {
        $err = '<div class="success1"><p><img src="images/success.png" border="none" width="18" height="18"> Driver Updated Successfully</p></div>';
        echo '<script type="text/javascript">window.location = "emvs.php?action=Administrator"</script>';
    }
}

?>
<script>

    //When DOM loaded we attach click event to button
    $(document).ready(function () {

        //attach keypress to input
        $('.input').keydown(function (event) {
            // Allow special chars + arrows
            if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9
                || event.keyCode == 27 || event.keyCode == 13
                || (event.keyCode == 65 && event.ctrlKey === true)
                || (event.keyCode >= 35 && event.keyCode <= 39)) {
                return;
            } else {
                // If it's not a number stop the keypress
                if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                    event.preventDefault();
                }
            }
        });

    });

</script>
<script>

    function isNumber(id, number) {
        number = number.replace(/[A-Za-z+@!#\/$%^&*\[\]{}|=()<>:\\;"'?.,~`-]/ig, "");
        id.value = number;
        return true;

    }

    function isName(id, name) {
        name = name.replace(/[0-9]/, "");
        id.value = name;
        return true;
    }

    function isNumOrLetter(evt) {


        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;

        if (charCode == 45 || charCode == 32)return true;
        if (charCode < 48 || (charCode > 57 && charCode < 65) ||
            (charCode > 90 && charCode < 97) ||
            charCode > 122) {
            return false;
        }

        return true;

    }

</script>
<script type="text/javascript" src="js/jquery-1.2.6.min.js"></script>

<script type="text/javascript">
    function myfun(b) {
        var dataString = 'id=' + b;
        $.ajax({
            type: "POST",
            url: "ajax_changepwd.php",
            data: dataString,
            success: function (msg) {
                $('#pwd').html(msg);
            }
        });
    }
</script>
<link rel="stylesheet" href="css/jquery-ui-1.10.3.custom.min.css"/>
<script src="js/jquery-1.10.2.min.js"></script>
<script src="js/jquery-ui-1.10.3.custom.min.js"></script>
<!-- Validaion-->
<link rel="stylesheet" type="text/css" href="css/jquery.validate.css"/>

<script src="js/jquery.validate.js" type="text/javascript"></script>
<script src="js/jquery.validation.functions.js" type="text/javascript">
</script>
<script type="text/javascript">
    jQuery(function () {
        jQuery("#password1").validate({
            expression: "if (VAL.length > 5 && VAL) return true; else return false;",
            message: "Please enter a valid 6 digit Password"
        });
        jQuery("#confirm").validate({
            expression: "if ((VAL == jQuery('#password1').val()) && VAL) return true; else return false;",
            message: "Confirm password field doesn't match the password field"
        });

        jQuery("#sites").validate({
            expression: "if (VAL != 'Choose One') return true; else return false;",
            message: "Please make a selection"
        });
        jQuery("#userroles").validate({
            expression: "if (VAL != '0') return true; else return false;",
            message: "Please make a selection"
        });
        jQuery('.AdvancedForm').validated(function () {
            alert("Use this call to make AJAX submissions.");
        });
    });
</script>
<section>
    <div class="mainwrapper">
        <?php include 'includes/leftpanel.php'; ?>

        <div class="mainpanel">
            <div class="pageheader">
                <div>
                    <div class="media-body">

                        <h4><?php if ($a == '') {
                                echo 'Add Adminstrator';
                            } else {
                                echo 'Update Adminstrator';
                            } ?></h4>
                    </div>
                </div><!-- media -->
            </div><!-- pageheader -->

            <div class="contentpanel">
                <div class="row">


                    <div class="col-md-12">
                        <form enctype="multipart/form-data" method="post" id="valWizard" name="valWizard"
                              accept-charset="utf-8" class="panel-wizard" novalidate="novalidate">

                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="form-group">
                                            <?= $err; ?>
                                        </div>
                                        <?php $m = $_GET['m']; ?>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Role<span
                                                    class="asterisk">*</span></label>
                                            <div class="col-sm-4">

                                                <input autocorrect="off"  type="text" readonly value="Administrator" name="userroles"
                                                       id="userroles" class="form-control" required>

                                            </div>
                                        </div><!-- form-group -->
                                        <div class="form-group" style="margin-bottom:1%;">
                                            <div class="col-sm-3">
                                                Sites
                                            </div>
                                            <div class="col-sm-4 field">
                                                <?php
                                                $s = $dataa['Sites'];
                                                $sitessql = $dbh->prepare("select * from sites where Id='$s' AND `Delete`='0'");
                                                $sitessql->execute();
                                                $sitesdata = $sitessql->fetch();
                                                ?>
                                                <input autocorrect="off"  id="sites1" name="sites1" type="hidden"
                                                       value="<?= $sitesdata['Id']; ?>">
                                                <select required data-placeholder="" id="sites" name="sites"
                                                        class="form-control">
                                                    <?php
                                                    $t = $sitesdata['Sites'];
                                                    if ($dataa['Sites'] != '') {
                                                        $sitessql = $dbh->prepare("SELECT * FROM `sites` WHERE Sites!='" . $t . "' && `Active`=1 && `Delete`=0 order by Id desc");
                                                    } else {
                                                        $sitessql = $dbh->prepare("SELECT * FROM `sites` WHERE `Delete`=0 && `Active`=1 && `Delete`=0 order by Id desc");
                                                    }
                                                    $sitessql->execute(); ?>
                                                    <option value="<?php if ($sitesdata['Sites'] != '') {
                                                        echo $sitesdata['Id'];
                                                    } else {
                                                        echo 'Choose One';
                                                    } ?>"><?php if ($sitesdata['Sites'] != '') {
                                                            echo $sitesdata['Sites'];
                                                        } else {
                                                            echo 'Choose One';
                                                        } ?>
                                                    </option>
                                                    <?php
                                                    while ($sitesdata = $sitessql->fetch()) {
                                                        if ($dataquery['Sites'] !== $sitesdata['Id']) {
                                                            ?>
                                                            <option
                                                                value="<?= $sitesdata['Id'] ?>"><?= $sitesdata['Sites']; ?></option>
                                                        <?php } else { ?>
                                                            <option selected
                                                                    value="<?= $sitesdata['Id'] ?>"><?= $sitesdata['Sites']; ?></option>
                                                        <?php }
                                                    } ?>
                                                </select>
                                            </div>
                                            <div class="col-sm-2">
                                                &nbsp;
                                            </div>
                                        </div>
                                        <div class="result" id="result">
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Login<span
                                                        class="asterisk">*</span></label>
                                                <div class="col-sm-4" id="emailDiv">
                                                    <input autocorrect="off"  type="hidden"
                                                           id="current-email"
                                                           value="<?= $dataquery['emailid']; ?>">
                                                    <input autocorrect="off"  type="text"
                                                           name="emailid"
                                                           id="emailid"
                                                           onkeypress="Maxval(this.value,30,this.id); loginChars()"
                                                           onblur="<?php if ($a == ''): ?>
                                                               checkEmail(this.value, 'createuser');
                                                           <?php else: ?>
                                                               checkEmail(this.value, 'updatemember');
                                                           <?php endif; ?>"

                                                           value="<?php if ($dataquery['emailid']) {
                                                               echo $dataquery['emailid'];
                                                           } ?>"
                                                           class="form-control"
                                                           required>
                                                    <div id="counteremailid"></div>
                                                </div>
                                            </div><!-- form-group -->

                                            <div id="checkres">
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">First Name</label>
                                                    <div class="col-sm-4">
                                                        <input autocorrect="off"  type="text" name="firstname"
                                                               onkeyup="isName(this,this.value);"
                                                               onkeypress="return isNumOrLetter(event);" id="firstname"
                                                               value="<?= $dataquery['Firstname']; ?>"
                                                               class="form-control"
                                                               required>
                                                    </div>
                                                </div><!-- form-group -->
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">Last Name</label>
                                                    <div class="col-sm-4">
                                                        <input autocorrect="off"  type="text" name="lastname"
                                                               onkeyup="isName(this,this.value);"
                                                               onkeypress="return isNumOrLetter(event);" id="lastname"
                                                               value="<?= $dataquery['Lastname']; ?>"
                                                               class="form-control" required>
                                                    </div>
                                                </div><!-- form-group -->

                                                <div class="form-group" style="border:2px;">
                                                    <label class="col-sm-3 control-label">Login Access<span
                                                            class="asterisk">*</span></label>
                                                    <div class="col-sm-4 field" style="border:2px;">
                                                        <div class="row">
                                                            <div class="col-md-4 login-access">
                                                                <div class="rdio rdio-primary">
                                                                    <input autocorrect="off"  type="radio" value="Access" checked
                                                                           id="Access"<?php if ($dataquery['status'] == 'Access') {
                                                                        echo 'checked';
                                                                    } ?> name="status">
                                                                    <label for="Access">Access</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 login-access">
                                                                <div class="rdio rdio-primary">
                                                                    <input autocorrect="off"  type="radio" value="Denied"
                                                                           id="Denied" <?php if ($dataquery['status'] == 'Denied') {
                                                                        echo 'checked';
                                                                    } ?> name="status">
                                                                    <label for="Denied">Denied</label>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div><!-- form-group -->

                                                <?php if ($a != '') { ?>
                                                    <div id="pwd">
                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-label">Password<span
                                                                    class="asterisk">*</span></label>

                                                            <div class="col-sm-4 field">
                                                                <input autocorrect="off"  type="password" readonly name="userpassword1"
                                                                       id="userpassword1"
                                                                       value="<?= $result = substr($dataquery['password'], 0, 10); ?>"
                                                                       class="form-control" required>
                                                            </div>

                                                            <div class="col-sm-4 field">
                                                                <input autocorrect="off"  class="change btn btn-primary mr5" type="text"
                                                                       name="change"
                                                                       id="change"
                                                                       onclick="myfun('<?= $dataquery['Id']; ?>')"
                                                                       value="Change Password"
                                                                       style="border:none;cursor:pointer;">
                                                            </div>
                                                        </div><!-- form-group -->
                                                    </div> <?php } else { ?>
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Password<span
                                                                class="asterisk">*</span></label>

                                                        <div class="col-sm-4 field">
                                                            <input autocorrect="off"  type="password" name="password1" id="password1"
                                                                   value="<?= $result = substr($dataquery['password'], 0, 10); ?>"
                                                                   class="form-control" required>
                                                        </div>


                                                    </div><!-- form-group -->
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Confirm Password<span
                                                                class="asterisk">*</span></label>

                                                        <div class="col-sm-4 field">
                                                            <input autocorrect="off"  type="password" name="confirm" id="confirm"
                                                                   value="<?= $result = substr($dataquery['password'], 0, 10); ?>"
                                                                   class="form-control" required>
                                                        </div>


                                                    </div><!-- form-group -->
                                                <?php } ?>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">Notes</label>
                                                    <div class="col-sm-4 field">
                                                        <input autocorrect="off"  type="text" name="usercomments" id="usercomments"
                                                               value="<?= $dataquery['comments']; ?>"
                                                               class="form-control" required>
                                                    </div>
                                                </div><!-- form-group -->


                                                <!-- row -->
                                                <!-- panel-body -->
                                                <div class="panel-footer">
                                                    <div class="row">
                                                        <div class="col-sm-9 col-sm-offset-3">
                                                            <?php if ($a != '') { ?>
                                                                <button class="btn btn-primary mr5" name="updatemember"
                                                                        type="submit">Update Adminstrator
                                                                </button>
                                                            <?php } else {
                                                                echo '<button class="btn btn-primary mr5" name="createuser" disabled type="submit">Add Adminstrator</button>';
                                                            } ?>

                                                            <a href="emvs.php?action=Administrator"
                                                               class="btn btn-primary mr5" type="submit">Cancel</a>
                                                        </div>
                                                    </div>
                                                </div><!-- panel-footer -->
                                            </div><!-- form-group -->
                                        </div>
                                    </div>

                                </div>

                            </div><!-- panel -->
                        </form>

                    </div>

                </div><!-- row -->

            </div>

        </div><!-- mainpanel -->
    </div><!-- mainwrapper -->
</section>


<script src="js/jquery-migrate-1.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/modernizr.min.js"></script>
<script src="js/ppace.min.js"></script>

<script src="js/jquery.cookies.js"></script>

<script src="js/jquery.dataTables.min.js"></script>
<script src="js/dataTables.bootstrap.js"></script>
<script src="js/dataTables.responsive.js"></script>
<script src="js/select2.min.js"></script>
<script src="js/custom.js"></script>
</body>
</html>
