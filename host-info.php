<?php
include 'includes/dbcon.php';

$hostName = $_POST['hostName'];

$parts = explode(' ', $hostName);
$firstName = $parts[0];
$lastName = $parts[1];

$sql = $dbh->prepare('SELECT *
                      FROM `hosts`
                      WHERE `firstname` = :firstname
                      AND `lastname` = :lastname
                      AND `Delete` = 0 ;');
$sql->execute([
    'firstname' => $firstName,
    'lastname' => $lastName
]);
$hostData = $sql->fetch();

$sql = $dbh->prepare('SELECT *
                      FROM `hostphone`
                      WHERE `H_id` = :hostid
                      AND HPrimary != "" ;');
$sql->execute([
    'hostid' => $hostData['Id']
]);
$hostPhoneData = $sql->fetch();

echo json_encode([
    'address' => $hostData['Address'],
    'phone' => $hostPhoneData['Phone_Number']
]);
