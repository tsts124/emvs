<?php

/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simply to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */

// DB table to use
$table = 'collectors';

// Table's primary key
$primaryKey = 'Id';

// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
$columns = array(
  array( 'db' => 'Id',              'dt' => 0 ),
  array( 'db' => 'title',           'dt' => 1 ),
  array( 'db' => 'firstname',       'dt' => 2 ),
  array( 'db' => 'middlename',      'dt' => 3 ),
  array( 'db' => 'lastname',        'dt' => 4 ),
  array( 'db' => 'statecountry',    'dt' => 5 ),
  array( 'db' => 'personalcomments','dt' => 6 ),
  array( 'db' => 'dob',             'dt' => 7 ),
  array( 'db' => 'gender',          'dt' => 8 ),
  array( 'db' => 'alertcheck',      'dt' => 9 ),
);

$dependent = array(
  array( 'db' => 'Collidenti',      'dt' => 10),
  array( 'db' => 'Collphone',       'dt' => 11),
  array( 'db' => 'Collcountry',     'dt' => 12),
  array( 'db' => 'Collidentphone',  'dt' => 13),
  array( 'db' => 'visitCount',      'dt' => 14),
  array( 'db' => 'alertVisitCount', 'dt' => 15),
);

// SQL server connection information
$sql_details = array(
    'user' => 'tossoy4c_emvs',
    'pass' => 'tosEmvs@123',
    'db'   => 'tossoy4c_emvsdb',
    'host' => 'localhost'
);


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */

require( '../includes/ssp.class.php' );

echo json_encode(
  SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $dependent )
);