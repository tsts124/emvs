<script type="text/javascript" src="js/jquery.SimpleMask.js"></script>
<script type="text/javascript">
    function blockSpecialCharNum(e) {
        var k = e.keyCode;
        return (k == 46 || k == 32 || k == 8 || (k >= 48 && k <= 57) || k == 40 || k == 41 || k == 45 || k == 43);
    }

    //Contact Details
    function contactChangeCountry(country, id, lang) {
        var countrySelect = undefined;
        countrySelect = $("#" + id);
        if (countrySelect) {
            var newVal = countrySelect.val() + country;
            countrySelect.val(newVal);
            countrySelect.focus();

            if ($.trim(countrySelect.val()) == 'USA' || $.trim(countrySelect.val()) == 'usa') {
                countrySelect.closest('div[data-id]').find('#phone' + lang).simpleMask({
                    'mask': 'usa',
                    'nextInput': false
                });
            } else {
                countrySelect.closest('div[data-id]').find('#phone' + lang).simpleMask({
                    'mask': 'other',
                    'nextInput': false
                });
            }

            $("[name='Phone_Country[]']").trigger('change');

        }
    }

    function contactCountryListener(val, id, lang) {
        var btn_isr = $('#contact_isr');
        var btn_usa = $('#contact_usa');
        var btn_uk = $('#contact_uk');
        //console.log(id + 'one');
        btn_isr.off('click');
        btn_usa.off('click');
        btn_uk.off('click');
        btn_isr.on('click', function () {
            contactChangeCountry(' ISRAEL', id, lang)
        });
        btn_usa.on('click', function () {
            contactChangeCountry(' USA', id, lang)
        });
        btn_uk.on('click', function () {
            contactChangeCountry(' UNITED KINGDOM', id, lang)
        });

    }

    var countClicks = 0;
    function chkphoneval() {
        addphone();
    }

</script>
<script type="text/javascript">

    function removephone(a) {
        localStorage.setItem("pagename", document.getElementById('pagename').value);
        if ($('#pagename').val() == 'adddrivers') {
            var data = 'id=' + a;
            $.ajax({
                method: 'POST',
                url: 'driver-contact-delete.php',
                data: data
            });
        }
        $('#remove' + a).remove();
        countClicks--;
    }
    function addphone() {
        countClicks++;
        if (document.getElementById('phoneid') !== null) {
            document.getElementById('phoneid').value = countClicks;
        }
        if (document.getElementById('pagename') !== null) {
            var pagename = document.getElementById('pagename').value;
        } else {
            pagename = localStorage.getItem("pagename");
        }
        var rabbisId = $('#coll-id').val();
        var visitid = $('#the-visit-id').val();

        var dataString = 'countClicks=' + countClicks +
            '&pagename=' + pagename +
            '&abc=' + window.location.search +
            '&rabbisId=' + rabbisId +
            '&visitid=' + visitid;

        $.ajax({
            type: "POST",
            url: "ajax_addmore.php",
            data: dataString,
            success: function (msg) {
                var strVal = msg.split("$$$$$");
                var strDiv = document.getElementById("addmore").outerHTML;
                document.getElementById("addmore").outerHTML = msg + strDiv;
            }
        });
    }


    <!-- check disable/enable the country combobox -->
    function chkphonetypeselect(val, rowno) {
        document.getElementById("countrycode" + rowno).disabled = false;
        var cntryval = document.getElementById("countrycode" + rowno).value;
        /*if(cntryval == '')
         {
         if((val=='Home')||(val=='Work')||(val=='Fax')){
         document.getElementById("countrycode"+rowno).value = "ISRAEL";
         document.getElementById("p_new"+rowno).value = "+972";
         $('#phone'+rowno ).simpleMask( { 'mask': 'isrphne'     , 'nextInput': false } );
         document.getElementById("phone"+rowno).disabled = false;
         $('isrphne').focus();
         }else {
         document.getElementById("countrycode"+rowno).value = "ISRAEL";
         document.getElementById("p_new"+rowno).value = "+972";
         $('#phone'+rowno ).simpleMask( { 'mask': 'isr'     , 'nextInput': false } );
         document.getElementById("phone"+rowno).disabled = false;
         $('isr').focus();
         }
         /!*document.getElementById("countrycode"+rowno).value = "UNITED STATES";
         document.getElementById("p_new"+rowno).value = "+1";*!/
         }
         if(cntryval == 'ISRAEL')
         {
         var phnetype = document.getElementById("Phonetpe"+rowno).value;
         if((phnetype=='Home')||(phnetype=='Work')||(phnetype=='Fax')){
         $('#phone'+rowno ).simpleMask( { 'mask': 'isrphne'     , 'nextInput': false } );
         $('isrphne').focus();
         }else {
         $('#phone'+rowno ).simpleMask( { 'mask': 'isr'     , 'nextInput': false } );
         $('isr').focus();
         }
         } */
        var country = $("#countrycode" + rowno).val();

        if (($.trim(country) == 'USA') || ($.trim(country) == 'usa') || ($.trim(country) == 'UNITED STATES')) {
            $('#phone' + rowno).simpleMask({'mask': 'usa', 'nextInput': false});
            document.getElementById("phone" + rowno).disabled = false;
        } else {
            $('#phone' + rowno).simpleMask({'mask': 'other', 'nextInput': false});
        }
    }

    <!-- check disable/enable the phone entry -->
    function chkcountryselect(val, rowno) {
        //alert(rowno);
        /*var dataString = 'Contryname='+val+'&langno='+rowno;
         $.ajax({
         type: "POST",
         url: "ajax_country.php",
         data: dataString,
         success: function(msg) {
         //$('#p_new'+rowno).val(msg);
         }
         });*/

        var phnetype = document.getElementById("Phonetpe" + rowno).value;
        document.getElementById("phone" + rowno).disabled = false;
        /*if(val =="ISRAEL")
         {
         if((phnetype=='Home')||(phnetype=='Work')||(phnetype=='Fax')){
         $('#phone'+rowno ).simpleMask( { 'mask': 'isrphne'     , 'nextInput': false } );
         $('isrphne').focus();
         }else {
         $('#phone'+rowno ).simpleMask( { 'mask': 'isr'     , 'nextInput': false } );
         $('isr').focus();
         }
         }
         else if(val =="UNITED STATES"){
         $('#phone'+rowno ).simpleMask( { 'mask': 'usa'     , 'nextInput': false } );
         $('usa').focus();
         }else {
         $('#phone'+rowno ).simpleMask( { 'mask': 'other'     , 'nextInput': false } );
         $('other').focus();
         }*/
    }

    function Isduplicatephone() {

        var arrval = [];
        var testarr = [];

        $("select[name='Phonenum[]'").each(
            function (index) {
                var i = $(this).attr("id").slice(8);
                var id = document.getElementById("phone" + i);
                var val = id.value;
                arrval.push(val);
                testarr.push(val);
            }
        )
        ;

        var sorted_arr = arrval.sort();
        var results = [];
        for (var j = 0; j < arrval.length; j++) {
            if (sorted_arr[j + 1] == sorted_arr[j]) {
                for (var m = 0; m < testarr.length; m++) {
                    // to identify the m value
                    if (sorted_arr[j] == testarr[m]) {
                        //alert(sorted_arr[j]);

                        //document.getElementById('submitchk').style.cursor = "none";
                        //document.getElementById("duperror" + m).style.visibility = "visible";
                        document.getElementById("submitchk").setAttribute('class', 'btn btn-primary mr5 disabled');
                        document.getElementById('upsubmitchk').disabled = true;
                    }
                    if (sorted_arr[j + 1] == testarr[m]) {
                        document.getElementById("duperror" + m).style.visibility = "visible";
                        document.getElementById("submitchk").setAttribute('class', 'btn btn-primary mr5 disabled');
                        document.getElementById('upsubmitchk').disabled = true;
                    }
                }
                return false;
            }
            else {
                for (var m = 0; m < arrval.length; m++) {
                    //document.getElementById("duperror" + m).style.visibility = "hidden";
                    document.getElementById("submitchk").setAttribute('class', 'btn btn-primary mr5');
                    document.getElementById('upsubmitchk').disabled = false;
                }
            }
        }
        return true;

    }

    function validateForm() {
        var flag = false;
        var validphoneflag = true;

        //validation, ... at least one item required to save a visit ???
        if (!(($("select[name='Phonenum1'").length === 0 ) && ($("select[name='Phonenum[]'").length === 0))) {
            flag = true;
        }

        $("select[name='Phonenum[]'").each(
            function (index) {
                var i = $(this).attr("id").slice(8);
                //setTimeout(100);
                var id = document.getElementById("phone" + i);
                var code = document.getElementById("countrycode" + i).value;
                var val = id.value;
                var phnetype = document.getElementById("Phonetpe" + i).value;
                if (code == "ISRAEL") {
                    //alert(val.length);
                    if ((val.length == 10) || (val.length == 11)) {
                        flag = true;
                        document.getElementById("error" + i).style.visibility = "hidden";
                        document.getElementById('submitchk').disabled = true;
                        document.getElementById('upsubmitchk').disabled = true;
                    }
                    else {
                        flag = false;
                        document.getElementById("error" + i).style.visibility = "visible";
                        document.getElementById('upsubmitchk').disabled = true;
                        document.getElementById('submitchk').disabled = true;
                        validphoneflag = false;
                    }
                }
                else if (code == "UNITED STATES") {
                    if (val.length == 14) {
                        flag = true;
                        document.getElementById("error" + i).style.visibility = "hidden";
                    } else {
                        flag = false;
                        //alert("error captured with error code = error" + i);
                        document.getElementById("error" + i).style.visibility = "visible";
                        validphoneflag = false;
                    }
                } else {

                    if (val.length == 10) {
                        flag = true;
                        document.getElementById("error" + i).style.visibility = "hidden";
                    } else {
                        flag = false;
                        //alert("error captured with error code = error" + i);
                        document.getElementById("error" + i).style.visibility = "visible";
                        validphoneflag = false;
                    }
                }
            }
        );
        if (flag == true && validphoneflag == true) {

            document.getElementById('upsubmitchk').disabled = false;
            document.getElementById('submitchk').disabled = false;
            //document.getElementById('upsubmitchk').disabled = false;
            if (Isduplicatephone() == true) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    function ChkphoneDup(num, lang, pagename) {
        var num = document.getElementById("phone" + lang).value;
        var isdnum = document.getElementById("p_new" + lang).value;
        //alert(isdnum);
        var dataString = 'Phonenumber=' + num + '&isdnumber=' + isdnum + '&langno=' + lang + '&pagename=' + pagename;
        //alert(dataString);
        $.ajax({
            type: "POST",
            url: "chkphone.php",
            data: dataString,
            success: function (msg) {
                //alert(msg);
                $('#phnedup' + lang).html(msg);
                var hiddenval = document.getElementById('used' + lang).style.visibility;
                //alert(hiddenval);
                if (hiddenval == 'hidden') {
                    document.getElementById('submitchk').disabled = false;
                    document.getElementById('upsubmitchk').disabled = false;
                    //document.getElementById('Cntsubmitchk').disabled = true;
                    return true;
                }
                else {
                    document.getElementById('submitchk').disabled = true;
                    document.getElementById('upsubmitchk').disabled = true;
                    //document.getElementById('Cntsubmitchk').disabled = true;
                    $(this).html(msg);
                    return false;
                }
            }
        });
    }
</script>

<input autocorrect="off"  name="pagename" id="pagename" type="hidden" value="<?= $_GET['action']; ?>">

<div id="addmore"></div>
<div id="res"></div>
