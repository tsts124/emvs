<?php
include '../includes/dbcon.php';

$a = $_POST['a'];
$b = $_POST['b'];

$filename = rand(1, 1000) . time() . '.jpg';

echo json_encode($filename);

$status = (boolean)move_uploaded_file($_FILES['photo']['tmp_name'], '../uploads/' . $filename);

$response = (object)[
    'status' => $status
];

$sql = $dbh->prepare('UPDATE `visitstable` 
		                  SET `photo` = :filename,
		                  	`photoname` = :filename 
		                  WHERE `collectorsid` = :collid
		                  AND `visitid` = :visitid
	                    ');
$sql->execute([
    'filename' => $filename,
    'collid' => $a,
    'visitid' => $b
]);
