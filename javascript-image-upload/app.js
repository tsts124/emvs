var resizedFiles = [];
var ids = [];

//Not finished. For attachment at the member.php
function uploadAttachment() {
    var formData = new FormData();
    formData.append('photos', resizedFiles);
    formData.append('ids', ids);

    var request = new XMLHttpRequest();
    request.open('POST', 'javascript-image-upload/processAttachment.php');
    request.responseType = 'json';

    request.onreadystatechange = function () {
        if (request.readyState == 4) {
            if (request.status != 200) {
                alert("ERR");
            }
            else {
                alert("SUCCESS");
                alert(request.responseText);
                alert(request.status);
                alert(request.response);
            }
        }
    };
    request.send(formData);
}

function photoResize(id) {
    'use strict';
    // Initialise resize library
    var resize = new window.resize();
    resize.init();


    // Upload photo
    var upload = function (photo, callback) {
        if (id == 'mobilePhoto') {
            $('#current-photo').css('display', 'block');
        } else {
            $('#coll-documents-galery > .loading').css('display', 'block');
        }
        var a = document.querySelector('#a').getAttribute("value");
        var b = document.querySelector('#b').getAttribute("value");

        var formData = new FormData();
        formData.append('photo', photo);
        formData.append('a', a);
        formData.append('b', b);
        var request = new XMLHttpRequest();
        request.onreadystatechange = function () {
            if (request.readyState === 4) {
                if (id == 'mobilePhoto') {
                    $('#current-photo').css('display', 'none');
                } else {
                    $('#coll-documents-galery > .loading').css('display', 'none');
                }
                callback(request.response);
            }
        };
        if (id == 'docMobilePhoto') {
            request.open('POST', 'javascript-image-upload/docMobile.php');
        } else {
            request.open('POST', 'javascript-image-upload/process.php');
        }
        request.responseType = 'json';
        request.send(formData);
    };

    var fileSize = function (size) {
        var i = Math.floor(Math.log(size) / Math.log(1024));
        return (size / Math.pow(1024, i)).toFixed(2) * 1 + ' ' + ['B', 'kB', 'MB', 'GB', 'TB'][i];
    };

    var resizing = function (event) {
        event.preventDefault();

        var files = event.target.files;
        for (var i in files) {

            if (typeof files[i] !== 'object') return false;

            (function () {

                var initialSize = files[i].size;

                resize.photo(files[i], 800, 'file', function (resizedFile) {

                    var resizedSize = resizedFile.size;

                    var $preview = $('#preview');

                    if (id == 'mobilePhoto') {
                        upload(resizedFile, function (response) {

                            resize.photo(resizedFile, 800, 'dataURL', function (thumbnail) {
                                $preview.html(' ');
                                $preview.prepend('<div class="doc-wrapper jpg picture" id="' + response + '">' +
                                    '<div class="loading"></div>' +
                                    '<a href="' + thumbnail + '" class="doc-picture">' +
                                    '<img  class="docPicture" src="' + thumbnail + '" height="150px">' +
                                    '</a>' +
                                    '<div data-animate-angle="0" data-rotate-angle="0" ' +
                                    'class="rotate btn btn-warning">ROTATE PICTURE</div>' +
                                    '<div class="cross btn btn-danger">DELETE PICTURE</div>' + '</div>');

                                $('#' + id).remove();
                                $('#results').html(' ');
                                $('#mobilePhotoDiv').append(
                                    '<input type="file" ' +
                                    'capture="camera" ' +
                                    'accept="image/*" ' +
                                    'id="mobilePhoto" ' +
                                    'name="mobilePhoto" ' +
                                    'class="mobile-photo-div">'
                                );
                                $('#mobilePhoto').on('click', function (event) {
                                    photoResize(this.id)
                                });
                            });

                        });
                    } else if (id == 'docMobilePhoto') {
                        upload(resizedFile, function (response) {

                            resize.photo(resizedFile, 800, 'dataURL', function (thumbnail) {
                                $('#coll-documents-galery').prepend('<div class="doc-wrapper jpg crop picture" id="' + response + '">' +
                                    '<div class="loading"></div>' +
                                    '<a href="' + thumbnail + '" class="doc-picture">' +
                                    '<img  class="docPicture" src="' + thumbnail + '" height="150px">' +
                                    '</a>' +
                                    '<div class="cross"></div>' +
                                    '<div data-animate-angle="0" data-rotate-angle="0" class="rotate"></div>' +
                                    '</div>');

                                $('#' + id).remove();
                                $('#docMobilePhotoDiv').append(
                                    '<input type="file" ' +
                                    'capture="camera" ' +
                                    'accept="image/*" ' +
                                    'id="docMobilePhoto" ' +
                                    'name="docMobilePhoto" ' +
                                    'class="mobile-photo-div">'
                                );
                                $('#docMobilePhoto').on('click', function (event) {
                                    photoResize(this.id)
                                });
                            });

                        });

                    } else {
                        var docId = $('#' + id).attr('lang');
                        ids[ids.length] = docId;
                        resizedFiles[resizedFiles.length] = resizedFile;
                    }

                    resize.photo(resizedFile, 600, 'dataURL', function (thumbnail) {

                    });
                });
            }());
        }
    };

    document.querySelector('#' + id).removeEventListener('change', resizing);
    document.querySelector('#' + id).addEventListener('change', function (event) {
        resizing(event)
    });
}

