<?php
session_start();
$action = $_GET['action'];

$routes = [
    'admin'             => 'admin',
    'Administrator'     => 'adminstrator',
    'addadmin'          => 'addadmin',
    'addmanager'        => 'adminmanager',
    'add_newmember'     => 'add_newmember',
    'add_newmember1'    => 'add_newmember1',
    'appoptions'        => 'appoptions',
    'addappoption'      => 'addappoption',
    'addRequiredField'  => 'required_fields_add',
    'adddrivers'        => 'driver_addnew',
    'addhosts'          => 'hosts_addnew',
//    'collector'         => 'collector',
    'collectorInputs'   => 'collectorInputs',
    'collector_entry'   => 'collector_entry',
    'colladd'           => 'colladd',
    'collectorsearch'   => 'collectorsearch',
    'editvisit'         => 'visitsedit',
    'editvisit1'        => 'visitsedit1',
    'index'             => 'index',
    'welcome'           => 'welcome',
    'member'            => 'member',
    'drivers'           => 'drivers',
    'hosts'             => 'hosts',
    'rabbis'            => 'rabbis',
    'sites'             => 'sites',
    'modificationDocs'  => 'modificationDocs',
    'tools'             => 'tools',
    'new_role'          => 'new_role',
    'about'             => 'about',
    'new_roles'         => 'new_roles',
    'new_member'        => 'new_member',
    'show_users'        => 'show_users',
    'memberlogin'       => 'memberlogin',
    'signout'           => 'signout',
    'pdf2/examples/example11' => 'pdf2/examples/example11',
    'pdf2/example11'    => 'pdf2/example11',
    'pdf2/res/pdf'      => 'pdf2/res/pdf',
    'pdf2/pdfprint'     => 'pdf2/pdfprint',
    'sites_addnew'      => 'sites_addnew',
    'novisit'           => 'novisit',
    'visitsadd1'        => 'viaitaadd',
    'title'             => 'title',
    'new_member1'       => 'new_member1',
    'manager'           => 'manager',
    'import'            => 'import',
    'collector'         => 'FirstPageCollectors'
];

if (isset($routes[$action])) {
    if (file_exists($routes[$action] . '.php')) {

        include($routes[$action] . '.php');
    } else {
        echo '404 not found';
    }

} else if ($action == "requiredFields") {

    if (isset($_GET['fieldType'])) {

        $fieldsType = $_GET['fieldType'];
        include('required_fields.php');
    } else {
        include('index.php');
    }
}
?>

