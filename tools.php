<?php 
	include 'includes/header.php'; 
	include 'includes/dbcon.php'; 
	if($_SESSION['user']==''){
		header('Location: emvs.php?action=index');
	}
		
?>

        <section>
            <div class="mainwrapper">
                  <?php include 'includes/leftpanel.php'; ?>
                
                <div class="mainpanel">
                    <div class="pageheader">
                        <div class="media">                            
                            <div class="media-body">                               
                                <h4>Tools</h4>
							<h5><button class="btn btn-primary btn-xs"><a href="emvs.php?action=new_member" style="color:#fff"> New Member</a></button> <button class="btn btn-primary btn-xs"><a href="emvs.php?action=tools"  style="color:#fff"> Tools</a></button> <button class="btn btn-primary btn-xs"><a href="#"  style="color:#fff"> Help</a></button></h5>
								
                            </div>
                        </div><!-- media -->
                    </div><!-- pageheader -->
                    
                    <div class="contentpanel">                        
                        <div class="panel panel-default">
                                    <div class="panel-heading">                                        
                                        <h4 class="panel-title"><strong>&nbsp;&nbsp;&nbsp;&nbsp;Membership Manager Tools</strong></h4>
                                    </div>
                                    <div class="panel-body nopadding">
                                        <form class="form-bordered">
                                            <div class="form-group">
                                                <h4 class="panel-title"><strong>&nbsp;&nbsp;Current Status</strong></h4>
                                                <div class="col-sm-8 control-label">                                                    
                                                </div>
                                            </div>
        <?php
		$sqlquery=$dbh->prepare("select * from newmember");	
		$sqlquery->execute();
		$count = $sqlquery->rowCount();
		?>
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Total Members</label>
                                                <div class="col-sm-8 control-label">
                                                    <div class="toggle toggle-primary" style="height: 20px; width: 50px;"><div class="toggle-slide active"><div class="toggle-inner" style="width: 80px; margin-left: 0px;"><div class="toggle-on active" style="height: 20px; width: 40px; text-align: center; text-indent: -10px; line-height: 20px;"><?=$count;?></div><div class="toggle-blob" style="height: 20px; width: 20px; margin-left: -10px;"></div><div class="toggle-off" style="height: 20px; width: 40px; margin-left: -10px; text-align: center; text-indent: 10px; line-height: 20px;"></div></div></div></div>
                                                </div>
                                            </div>
												<?php
													$sqlquery1=$dbh->prepare("select count(online)as online from newmember where online='1'");	
													$sqlquery1->execute();
													$sqldata1 = $sqlquery1->fetch();
												?>
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Currently online</label>
                                                <div class="col-sm-8 control-label">
                                                    <div class="toggle toggle-success" style="height: 20px; width: 50px;"><div class="toggle-slide active"><div class="toggle-inner" style="width: 80px; margin-left: 0px;"><div class="toggle-on active" style="height: 20px; width: 40px; text-align: center; text-indent: -10px; line-height: 20px;"><?=$sqldata1['online'];?></div><div class="toggle-blob" style="height: 20px; width: 20px; margin-left: -10px;"></div><div class="toggle-off" style="height: 20px; width: 40px; margin-left: -10px; text-align: center; text-indent: 10px; line-height: 20px;"></div></div></div></div>
                                                </div>
                                            </div>
                                          
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Total roles</label>
                                                <div class="col-sm-8 control-label">
															<?php
															$sqlquery1=$dbh->prepare("select count(Id)as Id from newrole");	
															$sqlquery1->execute();
															$sqldata1 = $sqlquery1->fetch();
															?>
            
                                                    <div class="toggle toggle-warning" style="height: 20px; width: 50px;"><div class="toggle-slide active"><div class="toggle-inner" style="width: 80px; margin-left: 0px;"><div class="toggle-on active" style="height: 20px; width: 40px; text-align: center; text-indent: -10px; line-height: 20px;"><?=$sqldata1['Id'];?></div><div class="toggle-blob" style="height: 20px; width: 20px; margin-left: -10px;"></div><div class="toggle-off" style="height: 20px; width: 40px; margin-left: -10px; text-align: center; text-indent: 10px; line-height: 20px;">OFF</div></div></div></div>
                                                </div>
                                            </div>
									<div class="panel-heading">                                        
                                        <h4 class="panel-title"><strong><a href="emvs.php?action=new_role">&nbsp;&nbsp;&nbsp;Membership Manager Tools</a></strong></h4><br>
										<p>&nbsp;&nbsp;&nbsp;&nbsp;Add or remove Roles and review role membership</p><br>
										 <h4 class="panel-title"><strong><a href="emvs.php?action=about">&nbsp;&nbsp;&nbsp;About</a></strong></h4><br>
										<p>&nbsp;&nbsp;&nbsp;&nbsp;View version and provider configuration information</p>	
                                    </div>
                                        </form>
                                    </div><!-- panel-body -->
                                </div>   
                                               
                    </div><!-- contentpanel -->
                </div><!-- mainpanel -->
            </div><!-- mainwrapper -->
        </section>

        <script src="js/jquery-1.11.1.min.js"></script>
        <script src="js/jquery-migrate-1.2.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/modernizr.min.js"></script>
        <script src="js/ppace.min.js"></script>

        <script src="js/jquery.cookies.js"></script>
        
        <script src="js/jquery.dataTables.min.js"></script>
        <script src="js/dataTables.bootstrap.js"></script>
        <script src="js/dataTables.responsive.js"></script>
        <script src="js/select2.min.js"></script>

        <script src="js/custom.js"></script>
        <script>
            jQuery(document).ready(function(){
                
                jQuery('#basicTable').DataTable({
                    responsive: true
                });
                
                var shTable = jQuery('#shTable').DataTable({
                    "fnDrawCallback": function(oSettings) {
                        jQuery('#shTable_paginate ul').addClass('pagination-active-dark');
                    },
                    responsive: true
                });
                
                // Show/Hide Columns Dropdown
                jQuery('#shCol').click(function(event){
                    event.stopPropagation();
                });
                
                jQuery('#shCol input').on('click', function() {

                    // Get the column API object
                    var column = shTable.column($(this).val());
 
                    // Toggle the visibility
                    if ($(this).is(':checked'))
                        column.visible(true);
                    else
                        column.visible(false);
                });
                
                var exRowTable = jQuery('#exRowTable').DataTable({
                    responsive: true,
                    "fnDrawCallback": function(oSettings) {
                        jQuery('#exRowTable_paginate ul').addClass('pagination-active-success');
                    },
                    "ajax": "ajax/objects.txt",
                    "columns": [
                        {
                            "class":          'details-control',
                            "orderable":      false,
                            "data":           null,
                            "defaultContent": ''
                        },
                        { "data": "name" },
                        { "data": "position" },
                        { "data": "office" },
                        { "data": "salary" }
                    ],
                    "order": [[1, 'asc']] 
                });
                
                // Add event listener for opening and closing details
                jQuery('#exRowTable tbody').on('click', 'td.details-control', function () {
                    var tr = $(this).closest('tr');
                    var row = exRowTable.row( tr );
             
                    if ( row.child.isShown() ) {
                        // This row is already open - close it
                        row.child.hide();
                        tr.removeClass('shown');
                    }
                    else {
                        // Open this row
                        row.child( format(row.data()) ).show();
                        tr.addClass('shown');
                    }
                });
               
                
                // DataTables Length to Select2
                jQuery('div.dataTables_length select').removeClass('form-control input-sm');
                jQuery('div.dataTables_length select').css({width: '60px'});
                jQuery('div.dataTables_length select').select2({
                    minimumResultsForSearch: -1
                });
    
            });
            
            function format (d) {
                // `d` is the original data object for the row
                return '<table class="table table-bordered nomargin">'+
                    '<tr>'+
                        '<td>Full name:</td>'+
                        '<td>'+d.name+'</td>'+
                    '</tr>'+
                    '<tr>'+
                        '<td>Extension number:</td>'+
                        '<td>'+d.extn+'</td>'+
                    '</tr>'+
                    '<tr>'+
                        '<td>Extra info:</td>'+
                        '<td>And any further details here (images etc)...</td>'+
                    '</tr>'+
                '</table>';
            }
        </script>

    </body>
</html>
