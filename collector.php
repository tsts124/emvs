<?php
include 'includes/header.php';
include 'includes/dbcon.php';

if ($_SESSION['user'] == '') {
    header('Location: emvs.php?action=index');
}

if (isset($_POST['action']) && ($_POST['action'] == "update")) {
    $upquery = $dbh->prepare("update collectors set personalcommentsActive='" . $_POST['act'] . "' where Id='" . $_POST['hid'] . "'");
    $upquery->execute();
}

?>
<script>
    function fnactive(id) {

        if (document.getElementById('alertcomments' + id).checked == true)
            document.getElementById('act').value = 1;
        else
            document.getElementById('act').value = 0;
        document.getElementById('action').value = "update";
        document.getElementById('hid').value = id;
        document.collector.submit();
    }
</script>

<style>

    table#basicTable tbody tr td:not(:nth-child(2)):not(:nth-child(7)) {
        text-align: center;
    }

    .link {
        color: #353434;
    }

    .link:hover {
        color: #C80109;
    }

    span.thisCheck > a {
        color: red;
    }

    .first {
        display: none;
    }

    .second {
        display: show;
    }

    #tooltip {
        text-align: center;
        color: #fff;
        background: #111;
        position: absolute;
        z-index: 100;
        padding: 15px;
    }

    #tooltip:after /* triangle decoration */
    {
        width: 0;
        height: 0;
        border-left: 10px solid transparent;
        border-right: 10px solid transparent;
        border-top: 10px solid #111;
        content: '';
        position: absolute;
        left: 50%;
        bottom: -10px;
        margin-left: -10px;
    }

    #tooltip.top:after {
        border-top-color: transparent;
        border-bottom: 10px solid #111;
        top: -20px;
        bottom: auto;
    }

    #tooltip.left:after {
        left: 10px;
        margin: 0;
    }

    #tooltip.right:after {
        right: 10px;
        left: auto;
        margin: 0;
    }
</style>

<script>
    function refIdSearch(val) {

        var dataString = 'refVal=' + $(val).val() + '&isSearch=RefId';
        $.ajax({
            type: "POST",
            url: "ajax_search.php",
            data: dataString,
            success: function (res) {
                window.open(res.urlRef, "_self");
            }, error: function (jqXhr, textStatus, errorThrown) {
                alert(' Can\'t find visit with provided Reference ID.');
            }
        });
    }
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>

<body>
<section>
    <div class="mainwrapper">
        <?php include 'includes/leftpanel.php'; ?>
        <div class="mainpanel">
            <div class="pageheader">
                <div>
                    <div class="media-body">
                        <h4>Collector List</h4>
                    </div>
                </div><!-- media -->
            </div><!-- pageheader -->
            <div class="contentpanel">
                <div class="panel panel-primary-head content-padding">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-sm-2 field">
                                <a href="emvs.php?action=member" class="btn btn-warning">Add New Collector</a>
                            </div>
                            <div class="col-sm-6 field">
                                <label class="col-sm-4 col-sm-offset-1 reference-title">Reference Id:</label>
                                <div class="col-sm-6 field">
                                    <div id="adsearch">
                                        <input autocorrect="off" class="form-control input-sm reference-input"
                                               onchange="refIdSearch(this)">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <form action="" name="collector" id="collector" method="POST">
                        <div id="adsearchtable" class="adsearchtable">
                            <table id="basicTable" class="basic-table table table-striped table-bordered responsive">
                                <thead class="">
                                <tr>
                                    <th style="text-align:center">I.R.No</th>
                                    <th style="text-align:center">First Name, Last Name</th>
                                    <th style="text-align:center">Country</th>
                                    <th style="text-align:center">ID Type</th>
                                    <th style="text-align:center">Primary ID No.</th>
                                    <th style="text-align:center">DOB</th>
                                    <th style="text-align:center">Phone No.</th>
                                    <th style="text-align:center">Alerts</th>
                                    <th style="text-align:center">#Visits</th>
                                    <th style="text-align:center">Action</th>
                                    <th style="text-align:center">Delete</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                        <input type="hidden" name="action" id="action"/>
                        <input type="hidden" name="hid" id="hid"/>
                        <input type="hidden" name="act" id="act"/>
                    </form>
                </div><!-- panel -->
            </div><!-- contentpanel -->
        </div><!-- mainpanel -->
    </div><!-- mainwrapper -->
</section>

<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/jquery-migrate-1.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/modernizr.min.js"></script>
<script src="js/ppace.min.js"></script>

<script src="js/jquery.cookies.js"></script>

<script src="js/jquery.dataTables.min.js"></script>
<script src="js/dataTables.bootstrap.js"></script>
<script src="js/dataTables.responsive.js"></script>
<script src="js/select2.min.js"></script>

<script src="js/custom.js"></script>

<script>

    function fndelete(id) {
        if (confirm("Do you really wish to delete this record?")) {
            var data = 'id=' + id;
            $.ajax({
                type: 'POST',
                url: 'ajax_delete_collector.php',
                data: data,
                success: function (msg) {
                    location.reload();
                }
            });
        }
    }

    $(document).ready(function () {
        var classcheck;
        var mytable = $('#basicTable').DataTable({
            order: [[0, "desc"]],
            "processing": true,
            "serverSide": false,
            "ajax": "ajax/dataTableCollector.php",
            "responsive": true,
            "autoWidth": false,
            "columnDefs": [
                {
                    "render": function (data, type, row) {
                        if (row[6] != '') {
                            classcheck = 'thisCheck';
                        } else {
                            classcheck = '';
                        }
                        var content = '<span class="' + classcheck + '">';
                        content += '<a class="link" target="_blank" href="emvs.php?action=novisit&&no=' + row[0] + '">';
                        content += row[0];
                        content += '</a>';
                        content += '</span>';

                        return content;
                    },
                    "targets": 0
                },
                {
                    "render": function (data, type, row) {
                        var content = '<span class="' + classcheck + '">';
                        content += '<a class="link" target="_blank" href="emvs.php?action=novisit&&no=' + row[0] + '">';
                        content += row[1] + ' ' + row[2] + ' ' + row[4];
                        content += '</a>';
                        content += '</span>';

                        return content;
                    },
                    "targets": 1
                },
                {
                    "render": function (data, type, row) {

                        var content = "";
                        if (row[12] != null) {
                            content += '<span class="' + classcheck + '">';
                            content += '<a class="link" target="_blank" href="emvs.php?action=novisit&&no=' + row[0] + '">';
                            content += row[12];
                            content += '</a>';
                            content += '</span>';
                        }

                        return content;
                    },
                    "targets": 2
                },
                {
                    "render": function (data, type, row) {

                        var content = "";
                        if (row[10] != null) {
                            content += '<span class="' + classcheck + '">';
                            content += '<a class="link" target="_blank" href="emvs.php?action=novisit&&no=' + row[0] + '">';
                            content += row[10];
                            content += '</a>';
                            content += '</span>';
                        }
                        return content;
                    },
                    "targets": 3
                },
                {
                    "render": function (data, type, row) {

                        var content = "";
                        if (row[13] != null) {
                            content += '<span class="' + classcheck + '">';
                            content += '<a class="link" target="_blank" href="emvs.php?action=novisit&&no=' + row[0] + '">';
                            content += row[13];
                            content += '</a>';
                            content += '</span>';
                        }
                        return content;
                    },
                    "targets": 4
                },
                {
                    "render": function (data, type, row) {
                        var content = '<span class="' + classcheck + '">';
                        content += '<a class="link" target="_blank" href="emvs.php?action=novisit&&no=' + row[0] + '">';
                        content += row[7];
                        content += '</a>';
                        content += '</span>';

                        return content;
                    },
                    "targets": 5
                },
                {
                    "render": function (data, type, row) {

                        var content = "";
                        if (row[11] != null) {
                            content += '<span class="' + classcheck + '">';
                            content += '<a class="link" target="_blank" href="emvs.php?action=novisit&&no=' + row[0] + '">';
                            content += row[11];
                            content += '</a>';
                            content += '</span>';
                        }
                        return content;
                    },
                    "targets": 6
                },
                {
                    "render": function (data, type, row) {

                        var img = "";

                        if (row[6] == "") {
                            img = '<img src="images/empty.png" width="17" height="17">';
                        } else {
                            img = '<img src="images/ticksmall.gif"width="20" height="20">';
                        }

                        return img;
                    },
                    "targets": 7
                },
                {
                    "render": function (data, type, row) {

                        var content = "";
                        if (row[14] != null) {
                            content += '<span class="' + classcheck + '">';
                            content += '<a class="link" target="_blank" href="emvs.php?action=novisit&&no=' + row[0] + '">';
                            if (row[14] != 0) {
                                content += row[14];
                            }
                            content += '</a>';
                            content += '</span>';
                        }
                        return content;
                    },
                    "targets": 8
                },
                {
                    "render": function (data, type, row) {
                        var content = '<span class="' + classcheck + '">';
                        content += '<a class="link" target="_blank" href="emvs.php?action=member&&abc=' + row[0] + '">';
                        content += '<img src="images/edit1.png" border="0" data-toggle="tooltip" title="Edit Details" alt="EMVS"></a>';
                        content += '</span>';
                        return content;
                    },
                    "targets": 9
                },
                {
                    "render": function (data, type, row) {
                        var content = '<a href="javascript:fndelete(' + row[0] + ')">' +
                            '<img src="images/delete1.gif" border="0" data-toggle="tooltip"' +
                            ' title="Delete" alt="EMVS"></a>';
                        return content;
                    },
                    "targets": 10
                }
            ]
        });

        var shTable = jQuery('#shTable').DataTable({
            "fnDrawCallback": function (oSettings) {
                jQuery('#shTable_paginate ul').addClass('pagination-active-dark');
            },
            responsive: true
        });

        // Show/Hide Columns Dropdown
        jQuery('#shCol').click(function (event) {
            event.stopPropagation();
        });

        jQuery('#shCol input').on('click', function () {

            // Get the column API object
            var column = shTable.column($(this).val());

            // Toggle the visibility
            if ($(this).is(':checked'))
                column.visible(true);
            else
                column.visible(false);
        });

        // DataTables Length to Select2
        jQuery('div.dataTables_length select').removeClass('form-control input-sm');
        jQuery('div.dataTables_length select').css({width: '60px'});
        jQuery('div.dataTables_length select').select2({
            minimumResultsForSearch: -1
        });

        $.fn.dataTable.ext.search.push(
            function (settings, data, dataIndex) {
                var serch = $('#basicTable_filter input').val();

                if (data[0].toLowerCase().indexOf(serch.toLowerCase()) + 1
                    || data[1].toLowerCase().indexOf(serch.toLowerCase()) + 1
                    || data[2].toLowerCase().indexOf(serch.toLowerCase()) + 1
                    || data[3].toLowerCase().indexOf(serch.toLowerCase()) + 1
                    || data[4].toLowerCase().indexOf(serch.toLowerCase()) + 1
                    || data[5].toLowerCase().indexOf(serch.toLowerCase()) + 1
                    || data[6].toLowerCase().indexOf(serch.toLowerCase()) + 1
                    || data[7].toLowerCase().indexOf(serch.toLowerCase()) + 1) {
                    return true;
                }
                return false;
            }
        );

        $('#basicTable_filter input').keyup(function () {
            $('#basicTable').DataTable().draw();
        });

    });

    function format(d) {
        // `d` is the original data object for the row
        return '<table class="table table-bordered nomargin">' +
            '<tr>' +
            '<td>Full name:</td>' +
            '<td>' + d.name + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Extension number:</td>' +
            '<td>' + d.extn + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Extra info:</td>' +
            '<td>And any further details here (images etc)...</td>' +
            '</tr>' +
            '</table>';
    }
</script>

</body>
