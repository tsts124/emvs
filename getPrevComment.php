<?php
include 'includes/dbcon.php';

$collId = $_POST['collectorId'];
$visitId = $_POST['visitId'];
$column = $_POST['column'];

$sql = $dbh->prepare('SELECT *
                      FROM `visitstable`
                      WHERE `collectorsid` = :collId
                      AND `visitid` = :visitId - 1 ;');
$sql->execute(['collId' => $collId,
    'visitId' => $visitId]);
$comment = $sql->fetch()[$column];

$sql = $dbh->prepare('UPDATE `visitstable`
                      SET ' . $column . ' = :comment
                      WHERE `collectorsid` = :collId
                      AND `visitid` = :visitId ;');
$sql->execute([
    'comment' => $comment,
    'collId' => $collId,
    'visitId' => $visitId]);

echo $comment;
