<?php
include 'includes/header.php';
include 'includes/dbcon.php';

$query = $dbh->prepare(
    "
			SELECT
			*
			FROM
			`app_options`
			WHERE `key`  IN('show_site', 'default_path_sign', 'default_site')
			"
);

$query->execute();
$arr = $query->fetchAll();
$defaultSite = $arr[1]['value'];
$showSite = $arr[0]['value'];
$query = $dbh->prepare(
    "
		SELECT `Sites` FROM `newmember` WHERE `emailid` = '{$_SESSION['user']} AND `Delete` = 0'
		  "
);
$query->execute();
$defaultSitesAdmin = $query->fetch();

$e = base64_decode($_GET['e']);

if (!$e && !isset($_POST['createdriver'])) {
    $sql = $dbh->prepare("SELECT * FROM `drivers` ORDER BY `Id` DESC; ");
    $sql->execute();
    $drivers = $sql->fetch();
    $newId = $drivers['Id'] + 1;

    $sql = $dbh->prepare("DELETE FROM `driversphone` WHERE `D_id` = :id ;");
    $sql->execute([':id' => $newId]);
}

if ($_SESSION['user'] == '') {
    header('Location: emvs.php?action=index');
}
$title = addslashes(htmlspecialchars(trim($_POST['title'])));
$firstname = addslashes(htmlspecialchars(trim($_POST['firstname'])));
$lastname = addslashes(htmlspecialchars(trim($_POST['lastname'])));

$active = addslashes(htmlspecialchars(trim($_POST['active'])));
$sites = addslashes(htmlspecialchars(trim($_POST['sites'])));
$passportcountry = addslashes(htmlspecialchars(trim($_POST['passportcountry'])));
$passportno = addslashes(htmlspecialchars(trim($_POST['passportno'])));
$drivercity = addslashes(htmlspecialchars(trim($_POST['drivercity'])));
$exdate = addslashes(htmlspecialchars(trim($_POST['exdate'])));
$exdate1 = addslashes(htmlspecialchars(trim($_POST['exdate1'])));
$driverstate = addslashes(htmlspecialchars(trim($_POST['driverstate'])));
$driverlicense = addslashes(htmlspecialchars(trim($_POST['driverlicense'])));
$phone_primary = addslashes(htmlspecialchars(trim($_POST['phone_primary'])));
if (isset($_POST['createdriver'])) {
    $path = "uploads/scan/";
    $actual_image_name = '';
    $pass_actual_image_name = '';

    if (empty($sites)) {
        $sites = $defaultSitesAdmin['Sites'];
    }
    
    $valid_formats = array("jpg", "png", "gif", "bmp", "jpeg", "PNG", "JPG", "JPEG", "GIF", "BMP");

    function getExtension($str)
    {
        $i = strrpos($str, ".");
        if (!$i) {
            return "";
        }
        $l = strlen($str) - $i;
        $ext = substr($str, $i + 1, $l);
        return $ext;
    }

    if (($_FILES['photoimg']['name']) || ($_FILES['passimg']['name'])) {
        $passimg = $_FILES['passimg']['name'];
        $imagename = $_FILES['photoimg']['name'];
        $size = $_FILES['photoimg']['size'];

        $ext = strtolower(getExtension($imagename));
        $passext = strtolower(getExtension($passimg));
        if (in_array($ext, $valid_formats) && $size < (1024 * 1024)
        ) {
            $actual_image_name = time() . "." . $ext;
            $uploadedfile = $_FILES['photoimg']['tmp_name'];
            $driverimg = move_uploaded_file($uploadedfile, $path . $actual_image_name);

            if (!$driverimg) {
                $actual_image_name = '';
            }
        }

        if (in_array($passext, $valid_formats) && $size < (1024 * 1024)
        ) {
            $pass_actual_image_name = time() . "pass." . $passext;
            $pasduploadedfile = $_FILES['passimg']['tmp_name'];
            $passportimg = move_uploaded_file($pasduploadedfile, $path . $pass_actual_image_name);

            if (!$passportimg) {
                $pass_actual_image_name = '';
            }
        }
    }

    $count = $dbh->exec("
						INSERT INTO drivers(Title,Firstname,Lastname,Sites,Active,Driverlicense,Driverstate,
						                    Drivercity,Driverimg,exdate,Passportno,Passportcountry,Passimage,
						                    exdate1,Addeddate, `Delete`) 
						VALUES ('$title','$firstname','$lastname','$sites','$active','$driverlicense',
						        '$driverstate','$drivercity','$actual_image_name','$exdate','$passportno',
						        '$passportcountry', '$pass_actual_image_name', '$exdate1',now(),'0')");
    $dsql = $dbh->prepare("select * from drivers order by Id desc");
    $dsql->execute();
    $ddata = $dsql->fetch();
    $Did = $ddata['Id'];
    $dcount = count($_POST['Phonenum']);
    echo '<meta http-equiv="refresh" content="0; URL=emvs.php?action=drivers" />';
}
if ($e) {
    $sql = $dbh->prepare("select * from `drivers` where `Id` = '$e' ");
    $sql->execute();
    $data1 = $sql->fetch();
}
if (isset($_POST['updatedriver'])) {
    $path = "uploads/scan/";
    $actual_image_name = $data1['Driverimg'];
    $pass_actual_image_name = $data1['Passimage'];
    $valid_formats = array("jpg", "png", "gif", "bmp", "jpeg", "PNG", "JPG", "JPEG", "GIF", "BMP");

    function getExtension($str)
    {
        $i = strrpos($str, ".");
        if (!$i) {
            return "";
        }
        $l = strlen($str) - $i;
        $ext = substr($str, $i + 1, $l);
        return $ext;
    }

    if (($_FILES['photoimg']['name']) || ($_FILES['passimg']['name'])) {
        $passimg = $_FILES['passimg']['name'];

        $imagename = $_FILES['photoimg']['name'];

        $size = $_FILES['photoimg']['size'];
        $passsize = $_FILES['passimg']['size'];

        $ext = strtolower(getExtension($imagename));
        $passext = strtolower(getExtension($passimg));
        if (in_array($ext, $valid_formats) || (in_array($passext, $valid_formats)
            && (($size < (1024 * 1024))) || ($passsize < (1024 * 1024)))
        ) {
            $actual_image_name = time() . "." . $ext;
            $pass_actual_image_name = time() . "pass." . $passext;

            $uploadedfile = $_FILES['photoimg']['tmp_name'];
            $pasduploadedfile = $_FILES['passimg']['tmp_name'];

            $driverimg = move_uploaded_file($uploadedfile, $path . $actual_image_name);
            $passportimg = move_uploaded_file($pasduploadedfile, $path . $pass_actual_image_name);

            if (!$driverimg) {
                $actual_image_name = $data1['Driverimg'];
            }

            if (!$passportimg) {
                $pass_actual_image_name = $data1['Passimage'];
            }
        } else {
            $error = "Invalid file format..";
        }
    }
    $strSites = "";
    $strSites = ",Sites='$sites'";

    $count = $dbh->exec("
							UPDATE 
								drivers 
							SET 
								Title='$title',Firstname='$firstname',Lastname='$lastname',Phonenum='$Phonenum',
								Phoneno='$phoneno'" . $strSites . ",Active='$active',Driverlicense='$driverlicense',
								Driverstate='$driverstate',Drivercity='$drivercity',Driverimg='$actual_image_name',
								exdate='$exdate',Passportno='$passportno',Passportcountry='$passportcountry',
								Passimage='$pass_actual_image_name',exdate1='$exdate1',Updateddate=now() 
							WHERE Id='$e'
						");

    echo '<meta http-equiv="refresh" content="0; URL=emvs.php?action=drivers" />';
}
if (isset($_POST['action']) && ($_POST['action'] == "deletedriver")) {
    $sql = $dbh->prepare("select * from `driversphone` where `Id`='" . $_POST['act'] . "'");
    $sql->execute();
    $deldata = $sql->fetch();
    if ($deldata['DPrimary'] == '') {
        $delquery1 = $dbh->prepare("update  `driversphone` set `delete` = 1 where `Id`='" . $_POST['act'] . "'");
        $delquery1->execute();
    } else {
        echo '<script>alert("Primary Identification Number Can`t Delete")</script>';
    }

}
if (isset($_POST['action']) && ($_POST['action'] == "driverphoneupdate")) {
    $upquery1 = $dbh->prepare("update `driversphone` set `DPrimary`=' ' where `D_id`='" . $_POST['act'] . "'");
    $upquery1->execute();
    $upquery = $dbh->prepare("update `driversphone` set `DPrimary`='" . $_POST['act'] . "' where Id='" . $_POST['hid'] . "'");
    $upquery->execute();
}
?>
<script type="text/javascript" src="ajaxuploadresize/js/jquery.min.js"></script>
<script type="text/javascript" src="ajaxuploadresize/js/jquery.form.js"></script>
<script src="js/jquery-1.11.1.min.js"></script>
<link href="datepicker/jquery-ui.css" rel="Stylesheet" type="text/css"/>
<script type="text/javascript" src="datepicker/jquery-ui.js"></script>
<script language="javascript">
    function newid1(a, b) {
        document.getElementById('act').value = b;
        document.getElementById('action').value = "driverphoneupdate";
        document.getElementById('hid').value = a;
    }
    $(document).ready(function () {
        $("#txtdate").datepicker({
            minDate: 0,
            changeMonth: true,
            changeYear: true,

            onChangeMonthYear: function (year, month) {
                $("#startYear").val(year);
                $("#startMonth").val(month);
            }
        });
    });
    $(document).ready(function () {
        $("#txtdate1").datepicker({
            minDate: 0,
            changeMonth: true,
            changeYear: true

        });
    });
    $(".ui-datepicker-month").prepend("<option value='' selected='selected'>Month</option>");

    $(".ui-datepicker-year").prepend("<option value='' selected='selected'>Year</option>");
</script>
<!-- Validaion-->
<link rel="stylesheet" type="text/css" href="css/jquery.validate.css"/>
<script src="js/jquery.validate.js" type="text/javascript"></script>
<script src="js/jquery.validation.functions.js" type="text/javascript"></script>


<script type="text/javascript">
    pic1 = new Image(16, 16);
    pic1.src = "loader.gif";

    $(document).ready(function () {
        $("#lastname").keyup(function () {
            var usr = $("#lastname").val();
            var driverfname = document.getElementById("firstname").value;
            $("#status").html('<img src="loader.gif" align="absmiddle">&nbsp;Checking availability...');
            $.ajax({
                type: "POST",
                url: "check.php",
                data: "driverfname=" + driverfname + "&driverlname=" + usr,
                success: function (msg) {
                    $("#status").ajaxComplete(function (event, request, settings) {
                        if (msg == 'OK') {
                            document.getElementById('submitchk').disabled = false;
                            $("#lastname").removeClass('object_error'); // if necessary
                            $("#lastname").addClass("object_ok");
                            $(this).html('&nbsp;<img src="username_checker/tick.gif" align="absmiddle">');
                        }
                        else {
                            document.getElementById('submitchk').disabled = true;
                            $("#lastname").removeClass('object_ok'); // if necessary
                            $("#lastname").addClass("object_error");
                            $(this).html(msg);
                        }
                    });
                }
            });
            $("#firstname").removeClass('object_ok'); // if necessary
            $("#firstname").addClass("object_error");
        });

    });

    function isName(id, name) {
        name = name.replace(/[0-9]/, "");
        id.value = name;
        return true;
    }
    function isNumOrLetter(evt) {

        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;

        if (charCode == 45 || charCode == 32) return true;
        if (charCode < 48 || (charCode > 57 && charCode < 65) ||
            (charCode > 90 && charCode < 97) ||
            charCode > 122) {
            return false;
        }
        return true;
    }
    function removedriver(val) {
        if (confirm("Do you really wish to delete this record?")) {
            document.getElementById('act').value = val;
            document.getElementById('action').value = "deletedriver";

            document.imageform.submit();
        }
    }
</script>
<!-- popup window-->
<script type="text/javascript" src="js/slimbox2.js"></script>
<link rel="stylesheet" href="css/slimbox2.css" type="text/css" media="screen"/>
<section>
    <div class="mainwrapper">
        <?php include 'includes/leftpanel.php'; ?>
        <div class="mainpanel">
            <div class="pageheader">
                <div>
                    <div class="media-body">
                        <h4><?php if ($e == '') {
                                echo 'Add Driver';
                            } else {
                                echo 'Update Driver';
                            } ?></h4>
                    </div>
                </div>
                <!-- media -->
            </div>
            <!-- pageheader -->
            <div class="contentpanel">
                <div class="row">
                    <div class="col-md-9">
                        <form id="imageform" enctype="multipart/form-data" method="post" name="imageform"
                              accept-charset="utf-8" class="panel-wizard" novalidate="novalidate">
                            <?php
                            $sql = $dbh->prepare("select * from drivers order by Id desc");
                            $sql->execute();
                            $driver = $sql->fetch();
                            $idd = $driver['Id'] + 1;
                            ?>
                            <input autocorrect="off"  name="action" id="action" type="hidden">
                            <input autocorrect="off"  name="act" id="act" type="hidden" value="<?php if ($e) {
                                echo $e;
                            } else {
                                echo $idd;
                            } ?>">
                            <input autocorrect="off"  name="hid" id="hid" type="hidden">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="form-group">
                                            <?= $error; ?>
                                            <?= $runqury; ?>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Title</label>
                                            <div class="col-sm-5">
                                                <select id="mySelect" name="title" id="title"
                                                        class="form-control">
                                                    <?php
                                                    $t = $data1['Title'];

                                                    $sitessql = $dbh->prepare('SELECT *
                                                                               FROM `title`
                                                                               WHERE `Delete` = 0 ;');
                                                    $sitessql->execute(['t' => $t]);
                                                    ?>

                                                    <option value=" ">Choose One</option>

                                                    <?php while ($sitesdata = $sitessql->fetch()): ?>
                                                        <option <?php if ($sitesdata['title'] == $t) {
                                                            echo 'selected';
                                                        } ?> ><?= $sitesdata['title']; ?></option>
                                                    <?php endwhile; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <!-- form-group -->
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">First Name</label>
                                            <div class="col-sm-5">
                                                <input autocorrect="off"  type="text" name="firstname" id="firstname" class="form-control"
                                                       placeholder="First Name" value="<?= $data1['Firstname']; ?>">
                                            </div>
                                        </div>
                                        <!-- form-group -->
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Last Name</label>
                                            <div class="col-sm-5">
                                                <input autocorrect="off"  type="text" name="lastname" id="lastname" class="form-control"
                                                       placeholder="Last Name" value="<?= $data1['Lastname']; ?>">
                                            </div>
                                            <div id="status"></div>
                                            <div id="counter"></div>
                                        </div>
                                        <div class="form-group" style=" border-bottom: 1px solid #ededed;">
                                            <label class="col-sm-7 control-label"
                                                   style="width:200px; font-weight:bold;">
                                                Contact Details
                                                <a href="javascript:void(0);" onclick="chkphoneval();"
                                                   class="add-button" title="Add field">
                                                    <img src="images/add-icon.png" style="height: 25px;"/><br/>
                                                </a>
                                            </label>
                                            <div class="col-sm-2 field country-buttons"
                                                 style="padding:0px; width: 320px;">
                                                <button class="btn btn-primary btn-xs" type="button" id="contact_isr">
                                                    ISR
                                                </button>
                                                <button class="btn btn-primary btn-xs" type="button" id="contact_usa">
                                                    USA
                                                </button>
                                                <button class="btn btn-primary btn-xs" type="button" id="contact_uk">UK
                                                </button>
                                            </div>
                                        </div>

                                        <div class="form-group">

                                            <div class="col-sm-1 field">
                                                Primary
                                            </div>
                                            <label class="col-sm-1 control-label ident-type">
                                                Type
                                            </label>
                                            <div class="col-sm-3 col-xs-3 field ident-country-label">
                                                Country
                                            </div>
                                            <div class="col-sm-2 field ident-number">
                                                Phone
                                            </div>

                                        </div>

                                        <div id="addinput" class="add-info">
                                            <?php if ($e != '') {
                                                $driverid = $data1['Id'];
                                                $sqlquery = $dbh->prepare("select * from driversphone where D_id ='$driverid'");
                                                $sqlquery->execute();
                                                while ($dataquery1 = $sqlquery->fetch()) {

                                                    if ($dataquery1['delete'] != 1) { ?>
                                                        <div class="row form-group"
                                                             data-id="<?= $dataquery1['Id']; ?>">
                                                            <div class="row col-sm-1 field">
                                                                <input autocorrect="off"  lang="<?= $dataquery1['Id']; ?>" name="DPrimary"
                                                                       value="<?= $dataquery1['Id']; ?>"
                                                                       style="margin: 15px 0px 0px 0px;"
                                                                       required
                                                                       type="radio"<?php if ($dataquery1['DPrimary'] != '') {
                                                                    echo 'checked';
                                                                } ?>>
                                                            </div>
                                                            <label class="row col-sm-1 control-label"
                                                                   style="width: 150px;">
                                                                <select lang="<?= $dataquery1['Id']; ?>"
                                                                        class="form-control" required name="Phone_Type">
                                                                    <option value="<?php
                                                                    if ($dataquery1['Phone_Type'] != '') {
                                                                        echo $dataquery1['Phone_Type'];
                                                                    } else {
                                                                        echo '';
                                                                    }
                                                                    ?>  ">
                                                                        <?php
                                                                        if ($dataquery1['Phone_Type'] != '') {
                                                                            echo $dataquery1['Phone_Type'];
                                                                        } else {
                                                                            echo 'Choose One';
                                                                        }
                                                                        ?>
                                                                    </option>
                                                                    <option>Work</option>
                                                                    <option>Home</option>
                                                                    <option>Mobile</option>
                                                                    <option>Fax</option>
                                                                    <option>Other</option>
                                                                </select>
                                                            </label>
                                                            <div class="col-sm-3">
                                                                <?php $domain = $dataquery1['Phone_Country']; ?>

                                                                <input autocorrect="off"  type="text" style="text-transform: uppercase"
                                                                       lang="<?= $dataquery1['Id']; ?>"
                                                                       id="countrycode<?= $dataquery1['Id']; ?>"
                                                                       name="Phone_Country"
                                                                       class="form-control"
                                                                       onfocus="contactCountryListener(this.value, this.id, this.lang)"
                                                                       required
                                                                       onKeyPress="return onlyAlphabets(event,this)"
                                                                       value="<?= $domain; ?>">
                                                            </div>

                                                            <div class="row col-sm-3 field" id='phone_number'>
                                                                <input autocorrect="off"  type="text" name="Phone_Number"
                                                                       value="<?= $dataquery1['Phone_Number']; ?>"
                                                                       onkeypress="return blockSpecialCharNum(event)"
                                                                       onkeyup="Isduplicatephone();validateForm();ChkphoneDup(this.value,this.lang,'member');"
                                                                       lang="<?= $dataquery1['Id']; ?>"
                                                                       id="phone<?= $dataquery1['Id']; ?>"
                                                                       class="form-control" placeholder="Number"
                                                                       required
                                                                >
                                                            </div>
                                                            <div class="row col-sm-1 field" id='phone_number'>

                                                                <a href="#"
                                                                   onclick="removephne(<?= $dataquery1['Id']; ?>)"><img
                                                                        class="delete-img"
                                                                        src="images/remove-icon.png" border="0"
                                                                        data-toggle="tooltip" title="Remove Details"
                                                                        alt="Remove"
                                                                        data-original-title="Remove Details"></a>
                                                            </div>
                                                        </div>
                                                    <?php }
                                                }
                                            } ?>
                                            <?php include 'add_phone_wo_row.php'; ?>
                                        </div>

                                        <?php //include 'addphone.php'; ?>
                                        <script type="text/javascript" src="js/jquery.SimpleMask.js"></script>

                                        <?php if ($showSite){ ?>
                                        <div class="form-group">
                                            <?php }else{ ?>
                                            <div class="form-group" style="display: none">
                                                <?php }
                                                $sql = $dbh->prepare("SELECT * FROM `sites` WHERE `active` = 1 and `Delete` = 0; ");
                                                $sql->execute();
                                                $sites = $sql->fetchAll();

                                                $s = $data1['Sites'];
                                                $sql = $dbh->prepare("select * from sites where Id='$s'");
                                                $sql->execute();
                                                $driverSite = $sql->fetch();
                                                ?>
                                                <label class="col-sm-3 control-label">Sites <span
                                                        class="asterisk">*</span></label>
                                                <div class="col-sm-5">
                                                    <select id="sites" name="sites" class="form-control"
                                                            style="width:100%;margin-bottom: 1%;" required>
                                                        <?php if (!$e): ?>
                                                            <option value="Choose One" disabled selected>Choose One
                                                            </option>
                                                            <?php foreach ($sites as $site): ?>
                                                                <option
                                                                    value="<?= $site['Id'] ?>"><?= $site['Sites'] ?></option>
                                                            <?php endforeach; ?>
                                                        <?php else: ?>
                                                            <option value="<?= $driverSite['Id'] ?>"
                                                                    selected><?= $driverSite['Sites'] ?></option>
                                                            <?php foreach ($sites as $site): ?>
                                                                <?php if ($site['Id'] != $driverSite['Id']): ?>
                                                                    <option
                                                                        value="<?= $site['Id'] ?>"><?= $site['Sites'] ?></option>
                                                                <?php endif; ?>
                                                            <?php endforeach; ?>
                                                        <?php endif; ?>
                                                    </select>
                                                </div>
                                            </div>

                                            <!-- form-group -->
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Active</label>
                                                <div class="col-sm-5">
                                                    <?php if ($e == '') { ?>
                                                        <div class="ckbox ckbox-primary">
                                                            <input autocorrect="off"  type="checkbox" checked id="activedriver" value="1"
                                                                   name="active" required="">
                                                            <label for="activedriver"></label>
                                                        </div>
                                                        <!-- rdio --><?php } else if ($e != '') { ?>
                                                        <div class="ckbox ckbox-primary">
                                                            <input autocorrect="off"  type="checkbox" value="1" name="active"
                                                                   id="active<?= $data1['Id']; ?>"
                                                                   onclick="fnactive(<?= $data1['Id']; ?>)"
                                                                <?php if ($data1['Active'] == 1) {
                                                                    echo "checked";
                                                                } ?>/>
                                                            <label for="active<?= $data1['Id']; ?>"></label>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <!-- form-group -->
                                            <div class="col-md-8" style="margin-bottom: 10px;text-align: justify;">Note
                                                : The following vital information is the only reliable means of
                                                accessing interstate and international records. If avaliable, Please
                                                record
                                            </div>
                                            </br></br></br>
                                            <div class="form-group" style=" border-bottom: 1px solid #ededed;">
                                                <label class="col-sm-7 control-label" style="font-weight:bold;">Drivers
                                                    License</label>

                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Drivers License/Other ID
                                                    Number</label>
                                                <div class="col-sm-5">
                                                    <input autocorrect="off"  type="text" name="driverlicense" id="driverlicense"
                                                           class="form-control" value="<?= $data1['Driverlicense']; ?>"
                                                           placeholder="Drivers License/Other ID Number">
                                                </div>
                                            </div>
                                            <!-- form-group -->
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">State</label>
                                                <div class="col-sm-5">
                                                    <input autocorrect="off"  type="text" name="driverstate" id="driverstate"
                                                           class="form-control" value="<?= $data1['Driverstate']; ?>"
                                                           placeholder="State" onkeyup="isName(this,this.value);"
                                                           onkeypress="return isNumOrLetter(event);">
                                                </div>
                                            </div>
                                            <!-- form-group -->
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">City</label>
                                                <div class="col-sm-5">
                                                    <input autocorrect="off"  type="text" name="drivercity" id="drivercity"
                                                           class="form-control" value="<?= $data1['Drivercity']; ?>"
                                                           placeholder="City" onkeyup="isName(this,this.value);"
                                                           onkeypress="return isNumOrLetter(event);">
                                                </div>
                                            </div>
                                            <!-- form-group -->
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Expiry Date </label>
                                                <div class="col-sm-5">
                                                    <input autocorrect="off"  type="text" readonly name="exdate" id="txtdate"
                                                           class="form-control" value="<?= $data1['exdate']; ?>">
                                                </div>
                                            </div>
                                            <!-- form-group -->
                                            <div id="scan" class="form-group">
                                                <label class="col-sm-3 control-label">Scan/Photograph Document</label>
                                                <div class="col-sm-5">
                                                    <?php $dimg = $data1['Driverimg']; ?>
                                                    <input autocorrect="off"  class="btn btn-primary btn-bordered" type="file"
                                                           name="photoimg" id="photoimg"
                                                           style="width:305px;height:40px;"/>
                                                </div>
                                                <?php if ($dimg && file_exists('uploads/scan/' . $dimg)): ?>
                                                    <div class="doc-wrapper crop"
                                                         id="<?= $dimg; ?>">
                                                        <a href="<?= 'uploads/scan/' . $dimg; ?>"
                                                           class="doc-picture"><img class="docPicture"
                                                                                    src="<?= 'uploads/scan/' . $dimg ?>"
                                                                                    height="80"></a>
                                                    </div>
                                                <?php endif; ?>
                                                <!-- form-group -->    </br>
                                                <?php if (!$dimg) {
                                                    echo '</br></br>';
                                                } ?>
                                                <div class="form-group" style=" border-bottom: 1px solid #ededed;">
                                                    <label class="col-sm-7 control-label" style="font-weight:bold;">Passport</label>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">Passport Number</label>
                                                    <div class="col-sm-5">
                                                        <input autocorrect="off"  type="text" name="passportno" id="passportno"
                                                               class="form-control" value="<?= $data1['Passportno']; ?>"
                                                               placeholder="Passport Number">
                                                    </div>
                                                </div>
                                                <!-- form-group -->
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">Country</label>
                                                    <div class="col-sm-5">
                                                        <input autocorrect="off"  type="text" name="passportcountry" id="passportcountry"
                                                               class="form-control"
                                                               value="<?= $data1['Passportcountry']; ?>"
                                                               placeholder="Country" onkeyup="isName(this,this.value);"
                                                               onkeypress="return isNumOrLetter(event);">
                                                    </div>
                                                </div>
                                                <!-- form-group -->
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">Expiry Date </label>
                                                    <div class="col-sm-5">
                                                        <input autocorrect="off"  type="text" readonly name="exdate1" id="txtdate1"
                                                               class="form-control" value="<?= $data1['exdate1']; ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">Scan/Passport Document</label>
                                                    <div class="col-sm-5">
                                                        <?php $pimg = $data1['Passimage']; ?>
                                                        <input autocorrect="off"  class="btn btn-primary btn-bordered" type="file"
                                                               name="passimg" id="passimg"
                                                               style="width:305px;height:40px;"/>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <?php if ($pimg && file_exists('uploads/scan/' . $pimg)): ?>
                                                            <div class="doc-wrapper crop"
                                                                 id="<?= $pimg; ?>">
                                                                <a href="<?= 'uploads/scan/' . $pimg; ?>"
                                                                   class="doc-picture"><img class="docPicture"
                                                                                            src="<?= 'uploads/scan/' . $pimg ?>"
                                                                                            height="80"></a>
                                                            </div>
                                                        <?php endif; ?>
                                                        <!-- form-group -->    </br>
                                                        <?php if ($pimg == '') {
                                                            echo '</br></br>';
                                                        } ?>
                                                    </div>
                                                </div>
                                                <!-- form-group -->
                                            </div>
                                            <!-- row -->
                                        </div>
                                        <!-- panel-body -->
                                        <div class="panel-footer">
                                            <div class="row">
                                                <div class="col-sm-9 col-sm-offset-3">
                                                    <?php if ($e == '') {
                                                        echo '<button class="btn btn-primary mr5" name="createdriver" id="submitchk" type="submit">Add Driver</button>';
                                                    } else {
                                                        echo '<button class="btn btn-primary mr5" name="updatedriver" id="submitchk" type="submit">Update Driver</button>';
                                                    } ?>
                                                    <a href="emvs.php?action=drivers" class="btn btn-dark"> Cancel</a>
                                                    <input autocorrect="off"  id="upsubmitchk" name="next123" type="text"
                                                           style="margin-left: 5px;margin-top: 1px;display:none;">
                                                </div>
                                            </div>
                                        </div>
                                        <!-- panel-footer -->
                                    </div>
                                    <!-- panel -->
                        </form>
                    </div>
                </div>
                <!-- row -->
            </div>
        </div>
        <!-- mainpanel -->
    </div>
    <!-- mainwrapper -->
</section>

<script src="js/jquery-migrate-1.2.1.min.js">
</script>
<script src="js/bootstrap.min.js"></script>
<script src="js/modernizr.min.js"></script>
<script src="js/ppace.min.js"></script>

<script src="js/jquery.cookies.js"></script>
<script src="js/jquery.dataTables.min.js"></script>
<script src="js/dataTables.bootstrap.js"></script>
<script src="js/dataTables.responsive.js"></script>
<script src="js/select2.min.js"></script>
<script src="js/custom.js"></script>
<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script>
    function getGet(name) {
        var s = window.location.search;
        s = s.match(new RegExp(name + '=([^&=]+)'));
        return s ? s[1] : false;
    }

    $(document).ready(function () {
        $('#scan').magnificPopup({
            delegate: 'a',
            type: 'image',
            tLoading: 'Loading image #%curr%...',
            mainClass: 'mfp-img-mobile',
            gallery: {
                enabled: true,
                navigateByImgClick: true,
                preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
            },
            image: {
                tError: '<a href="%url%">The image #%curr%</a> could not be loaded.'
            }
        });

        $('#addinput').on('focusout', 'input, select', function () {
            var dataString = '';
            var valueElement = $(this).val();
            var thisName = $(this).attr('name');
            var id = $(this).attr('lang');
            var driver = $('#act').val();
            dataString += 'valueElement=' + valueElement + '&id=' + id + '&name=' + thisName + '&thisTable=driversphone' + '&a=' + driver;
            $.ajax({
                type: "POST",
                url: "ajax_saveFocusout.php",
                data: dataString,
                success: function (msg) {
                }
            });
        });
    });

    function removephne(id) {
        if (confirm("Do you really wish to delete this record?")) {
            $('[data-id="' + id + '"]').remove();
            var data = 'id=' + id;
            $.ajax({
                method: 'POST',
                url: 'driver-contact-delete.php',
                data: data,
                success: function () {
                    //console.log('yes');
                }
            });
        }
    }

    $("#imageform").validate({
        rules: {
            gender: {
                required: true
            }
        }
    });
</script>
<!--jQuery Image Rotate-->
<script src="js/jquery-rotate.js"></script>
<!-- Magnific Popup core JS file -->
<script src="magnific-popupp/dist/jquery.magnific-popup.js"></script>
<!--Magnific Popup min JS file-->
<script src="magnific-popupp/dist/jquery.magnific-popup.min.js"></script>
</body>
</html>
