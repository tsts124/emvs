<?php
include 'includes/dbcon.php';
$thisTable = $_POST['thisTable'];
$a = $_POST['a'];
$b = $_POST['b'];
$valueElement = addslashes(htmlentities(trim($_POST['valueElement'])));
$field = $_POST['name'];
$visitid = $_POST['visitid'];
if ($_POST['thisTable'] == 'visit_details') {
    $idRow = $_POST['idRow'];

    if ($field == 'Primary1' || $field == 'CPrimary') {
        if ($field == 'Primary1') {
            $type = 'identification';
        } else {
            $type = 'contact';
        }

        $sql = $dbh->exec("UPDATE `$thisTable`
                           SET `primary_number` = ''
                           WHERE `visit_id` = '$visitid'
                           AND `detail_type` = '$type' ; ");

        $sql = $dbh->exec("UPDATE `$thisTable`
                           SET `primary_number` = 1
                           WHERE `id` = '$idRow' ");
    } else {
        switch ($field) {
            case('Collidenti[]'):
            case('Phone_Type[]'):
                $field = 'type';
                break;
            case('Collcountry[]'):
            case('Phone_Country[]'):
                $field = 'country';
                break;
            case('Collphone[]'):
            case('Phone_Number[]'):
                $field = 'detail_number';
                break;
            case('passport_name[]'):
                $field = 'passport_name';
                break;
            default:
                $field = $_POST['name'];
        }

        $sql = $dbh->prepare("UPDATE `visit_details`
                          SET `$field` = :the_value
                          WHERE `id` = :id ; ");
        $sql->execute([
            'the_value' => $valueElement,
            'id' => $idRow
        ]);
    }

} elseif ($_POST['thisTable'] == 'driversphone') {
    $id = $_POST['id'];
    $a = $_POST['a'];
    if ($field == 'DPrimary') {
        $sql = $dbh->exec("UPDATE `$thisTable` SET `$field`='' WHERE `D_id` = '$a';");
        $sql = $dbh->exec("UPDATE `$thisTable` SET `$field`='$a' WHERE `Id` = '$id';");
    } elseif ($field == 'Phone_Country[]') {
        $sql = $dbh->exec("UPDATE `$thisTable` SET `Phone_Country`='$valueElement' WHERE `Id` = $id;");
    } elseif ($field == 'Phone_Type[]') {
        $sql = $dbh->exec("UPDATE `$thisTable` SET `Phone_Type`='$valueElement' WHERE `Id` = $id;");
    } elseif ($field == 'Phone_Number[]') {
        $sql = $dbh->exec("UPDATE `$thisTable` SET `Phone_Number`='$valueElement' WHERE `Id` = $id;");
    } else {
        $sql = $dbh->exec("UPDATE `$thisTable` SET `$field`='$valueElement' WHERE `Id` = $id;");
    }
} elseif ($_POST['thisTable'] == 'visitstable') {

    switch ($field) {
        case ('free_formed_host'):
        case ('free_formed_driver'):
        case ('usaddress1'):
        case ('zipcity'):
        case ('uscountry'):
        case ('baltiaddress1'):
        case ('institname'):
        case ('zipstate'):
        case ('institaddress1'):
        case ('institcity'):
        case ('institstate'):
        case ('institcountry'):
            $valueElement = ucwords($valueElement);
            break;
        default:
            break;
    }

    $flag = true;
    if (($field == 'visitid' || $field == 'refid')
        && (!$valueElement || !filter_var($valueElement, FILTER_VALIDATE_INT))
    ) {
        $flag = false;
    }
    if ($field == 'refid' && $valueElement && filter_var($valueElement, FILTER_VALIDATE_INT)) {
        $sql = $dbh->prepare('SELECT *
                              FROM `visitstable`
                              WHERE `Refid` = :fieldValue ;');
        $sql->execute([':fieldValue' => $valueElement]);

        if (!$sql->fetch()) {
            $sql = $dbh->prepare('UPDATE `visitstable`
                                  SET `Refid`= :fieldValue
                                  WHERE `visitid`= :visitId
                                  AND `collectorsid`= :collId ;');
            $sql->execute([':fieldValue' => $valueElement, ':visitId' => $b, 'collId' => $a]);

            echo 'saved';
        }
        $flag = false;
    } elseif ($field == 'last-refid' && $valueElement && filter_var($valueElement, FILTER_VALIDATE_INT)) {
        $sql = $dbh->prepare('SELECT *
                              FROM `visitstable`
                              WHERE `Refid` = :fieldValue ;');
        $sql->execute([':fieldValue' => $valueElement]);

        if (!$sql->fetch()) {
            $sql = $dbh->prepare('UPDATE `visitstable`
                                  SET `Refid`= :fieldValue
                                  WHERE `visitid`= :visitId - 1
                                  AND `collectorsid`= :collId ;');
            $sql->execute([':fieldValue' => $valueElement, ':visitId' => $b, 'collId' => $a]);

            echo 'saved';
        }

    } elseif ($flag) {
        $sql = $dbh->prepare("UPDATE `$thisTable`
                              SET `$field`='$valueElement'
                              WHERE `visitid`='$b'
                              AND `collectorsid`='$a'");
        $sql->execute();
    }
//
//    $personalInfoFields = [
//        'title',
//        'firstname',
//        'lastname',
//        'middlename',
//        'dob',
//        'address1',
//        'address2',
//        'city',
//        'statecountry',
//        'country',
//        'zipcode',
//        'personalcomments'
//    ];
//    if (in_array($field, $personalInfoFields)) {
//        $sql = $dbh->prepare("UPDATE `collectors` SET `$field`='$valueElement' WHERE `Id`='$a'");
//        $sql->execute();
//    }
}

