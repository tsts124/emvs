<?php
include 'includes/header.php';
include 'includes/dbcon.php';
$e = base64_decode($_GET['e']);

$loginemail = $_SESSION['user'];

$loginsql = $dbh->prepare("SELECT *
                                   FROM `newmember`
                                   WHERE emailid='$loginemail'
                                   AND `Delete` = 0");
$loginsql->execute();
$logindata = $loginsql->fetch();

$r = $logindata['roles'];

$query = $dbh->prepare(
    "
		SELECT
		*
		FROM
		`app_options`
		WHERE `app_options`.`key` = 'show_site' AND `app_options`.`value` = 1;
	  "
);

$query->execute();
$showSite = $query->rowCount();

$query = $dbh->prepare(
    "SELECT
	  *
	  FROM
	  `app_options`
	  WHERE `app_options`.`key` = 'default_site'"
);
$query->execute();
$defaultSiteValue = $query->fetch();

$query = $dbh->prepare(
    "SELECT
		  *
		  FROM
		  `app_options`
		  WHERE `app_options`.`key` = 'default_site_visitday_count'"
);
$query->execute();
$defaultVisitDayCountValue = $query->fetch();


$query = $dbh->prepare(
    "
	  SELECT
	  *
	  FROM
	  `sites`
	  WHERE 
	  `sites`.`Sites` = '" . $defaultSiteValue['value'] . "' 
	  AND `sites`.`Delete` = 0
	  AND `sites`.`visitsday` = '" . $defaultVisitDayCountValue['value'] . "'"
);
$query->execute();
$siteDetails = $query->fetch();


if ($_SESSION['user'] == '') {
    header('Location: emvs.php?action=index');
}
$title = addslashes(htmlspecialchars(strip_tags($_POST['title'])));
$firstname = addslashes(htmlspecialchars(strip_tags($_POST['firstname'])));
$lastname = addslashes(htmlspecialchars(strip_tags($_POST['lastname'])));
$phoneno1 = addslashes(htmlspecialchars(strip_tags($_POST['phoneno'])));
$phoneno = implode(",", $phoneno1);
$Phonenum1 = addslashes(htmlspecialchars(strip_tags($_POST['Phonenum'])));
$Phonenum = implode(",", $Phonenum1);
$address = addslashes(htmlspecialchars(strip_tags($_POST['address'])));
$active = $_POST['active'];
$sites = addslashes(htmlspecialchars(strip_tags($_POST['sites'])));

if (!$sites) {
    $sites = $siteDetails['Id'];
}

if (isset($_POST['createhosts'])) {

    $title = addslashes(htmlspecialchars(strip_tags($_POST['title'])));
    $firstname = addslashes(htmlspecialchars(strip_tags($_POST['firstname'])));
    $lastname = addslashes(htmlspecialchars(strip_tags($_POST['lastname'])));
    $address = addslashes(htmlspecialchars(strip_tags($_POST['address'])));
    $active = $_POST['active'];
    $sites = addslashes(htmlspecialchars(strip_tags($_POST['sites'])));

    $count = $dbh->exec("INSERT INTO hosts(Title,Firstname,Lastname,Sites,Address,Active,Addeddate) VALUES ('$title','$firstname','$lastname','$sites','$address','$active',now())");

    $hsql = $dbh->prepare("select * from hosts order by Id desc");
    $hsql->execute();
    $hdata = $hsql->fetch();
    $Hid = $hdata['Id'];

    $phone_primary1 = $_POST['CPrimary'];
    $dcount = count($_POST['phoneno']);
    for ($i = 0; $i <= $dcount; $i++) {
        $primary_phone_count = $_POST['primary_phone_count_new'][$i];
        $Phonetype = $_POST['Phone_Type'][$i];
        $Phonecntry = $_POST['Phone_Country'][$i];
        $phonenodup = $_POST['phoneno'][$i];
        $phoneno = $phonenodup;
        if ($_POST['phoneno'][$i] != '') {
            $count = $dbh->exec("INSERT INTO `hostphone`(`H_id`,
                                                       `HPrimary`,
                                                       `PrimaryCount`,
                                                       `Phone_Type`,
                                                       `Phone_Country`,
                                                       `Phone_Number`,
                                                       `Addeddate`)
                                 VALUES ('$Hid',
                                          0,
                                          '$primary_phone_count',
                                          '$Phonetype',
                                          '$Phonecntry',
                                          '$phoneno',
                                           now())");
            if ($phone_primary1) {
                $updateval = $dbh->prepare("update `hostphone` set HPrimary='' where `H_id`=$Hid");
                $updateval->execute();
                $dval = $dbh->prepare("UPDATE `hostphone`
                                       SET `HPrimary`='$Hid'
                                       WHERE `H_id`='$Hid'
                                       AND `PrimaryCount`='$phone_primary1';");
                $dval->execute();
            }
        }
        if ($count == '1') {
            $err = '<div class="success1"><p><img src="images/success.png" border="none" width="18" height="18"> Host Inserted Successfully</p></div>';
            echo '<meta http-equiv="refresh" content="0; URL=emvs.php?action=hosts" />';
        }
    }
    if ($e != '') {
        $ss = "select * from hosts where Id='$e'";
        $sql1 = $dbh->prepare("select * from hosts where Id='$e'");
        $sql1->execute();
        $data1 = $sql1->fetch();
    }

}
if (isset($_POST['updatehosts'])) {
    $phoneno1 = $_POST['phoneno'];
    $phoneno = implode(",", $phoneno1);
    $Phonenum1 = $_POST['Phonenum'];
    $Phonenum = implode(",", $Phonenum1);
    $active = $_POST['active'];
    $strSites = $sites;

    $count = $dbh->exec("
			UPDATE `hosts`
			SET `Sites` = '$sites',
			    `Title` ='$title',
			    `Firstname` ='$firstname',
			    `Lastname` ='$lastname',
			    `Phonenum` ='$Phonenum',
				`Phoneno` ='$phoneno',
				`Address` ='$address',
				`Active` = '$active',
				`Updateddate` = now()
				WHERE Id='$e'
		");

    //New numbers saving
    $phone_primary1 = $_POST['CPrimary'];
    $dcount = count($_POST['phoneno']);
    $Hid = $e;
    for ($i = 0; $i <= $dcount; $i++) {
        $primary_phone_count = $_POST['primary_phone_count_new'][$i];
        $Phonetype = $_POST['Phone_Type'][$i];
        $Phonecntry = $_POST['Phone_Country'][$i];
        $phoneno = $_POST['phoneno'][$i];

        if ($_POST['phoneno'][$i] != '') {
            $count = $dbh->exec("INSERT INTO hostphone(Id,
                                                       H_id,
                                                       HPrimary,
                                                       Phone_Type,
                                                       Phone_Country,
                                                       Phone_Number,
                                                       Addeddate)
                                        VALUES ( '$primary_phone_count',
                                                 '$Hid',
                                                 0,
                                                 '$Phonetype',
                                                 '$Phonecntry',
                                                 '$phoneno',
                                                  now())");

        }
    }

    //Old numbers saving
    $dcount = count($_POST['phoneno1']);
    $Hid = $e;
    for ($i = 0; $i <= $dcount; $i++) {
        $Phonetype = $_POST['Phonenum1'][$i];
        $Phonecntry = $_POST['countrycode1'][$i];
        $phoneno = $_POST['phoneno1'][$i];
        $contactDetailId = $_POST['contactDetailId'][$i];
        if ($_POST['phoneno1'][$i] != '') {
            $count = $dbh->exec("UPDATE `hostphone`
                                 SET `Phone_Type` = '$Phonetype',
                                     `Phone_Country` = '$Phonecntry',
                                     `Phone_Number` = '$phoneno',
                                     `Addeddate` = now()
                                 WHERE `Id` = '$contactDetailId' ;
                                        ");
        }
    }

    if ($phone_primary1) {
        $updateval = $dbh->prepare("update `hostphone` set `HPrimary` = '' where `H_id`=$Hid");
        $updateval->execute();
        $dval = $dbh->prepare("UPDATE `hostphone`
                                       SET `HPrimary`='$Hid'
                                       WHERE `H_id`='$Hid'
                                       AND `Id`='$phone_primary1';");
        $dval->execute();
    }

    if ($count == '1') {
        $err = '<div class="success1"><p><img src="images/success.png" border="none" width="18" height="18"> Host Updated Successfully</p></div>';
        echo '<meta http-equiv="refresh" content="0; URL=emvs.php?action=hosts" />';
    }

    echo '<meta http-equiv="refresh" content="0; URL=emvs.php?action=hosts" />';
}
if (isset($_POST['action']) && ($_POST['action'] == "delete")) {
    $delquery1 = $dbh->prepare("delete from hostphone where Id='" . $_POST['act'] . "'");
    $delquery1->execute();
}


?>
<script type="text/javascript">
    function newid1(a, b) {
        document.getElementById('act').value = b;
        document.getElementById('action').value = "phoneupdate";
        document.getElementById('hid').value = a;
    }
    function removephne(val) {
        if (confirm("Do you really wish to delete this record?")) {
            document.getElementById('act').value = val;
            document.getElementById('action').value = "delete";
            //document.getElementById('hid').value = a;
            document.basicForm.submit();
        }
    }
    function checkallerr() {
        var a = document.getElementById("status").innerHTML;
        var b = document.getElementById("status1").innerHTML;
        var c = document.getElementsByName("userphn").length;
        for (var er = 0; er < c; er++) {
            var d = document.getElementById("used" + er).style.visibility;
            var e = document.getElementById("duperror" + er).style.visibility;
            if ((d == 'visible') || (e == 'visible')) {
                var flag = 1;
                document.getElementById('submitchk').disabled = false;
            }
        }
        var m = a.search("hidden");
        var n = b.search("hidden");
        if (((m < 0) || (n < 0))) {
            document.getElementById('submitchk').disabled = false;
        }
    }
</script>

<script type="text/javascript" src="js/jquery-1.2.6.min.js"></script>
<SCRIPT type="text/javascript">
    pic1 = new Image(16, 16);
    pic1.src = "loader.gif";

    $(document).ready(function () {
        $("#lastname").keyup(function () {
            var fname = document.getElementById("firstname").value;

            var usr1 = $("#lastname").val();
            $("#status1").html('<img src="loader.gif" align="absmiddle">&nbsp;Checking availability...');
            $.ajax({
                type: "POST",
                url: "check.php",
                data: "firstname=" + fname + "&lastname=" + usr1,
                success: function (msg) {

                    $("#status1").ajaxComplete(function (event, request, settings) {

                        if (msg == 'OK') {
                            $("#lastname").removeClass('object_error');
                            $("#lastname").addClass("object_ok");
                            $(this).html('<span style="display:none;">hidden</span>&nbsp;<img src="username_checker/tick.gif" align="absmiddle">');
                        }
                        else {
                            $("#lastname").removeClass('object_ok');
                            $("#lastname").addClass("object_error");
                            $(this).html(msg);
                        }

                    });

                }

            });

            $("#lastname").removeClass('object_ok');
            $("#lastname").addClass("object_error");


        });

    });

    //-->
</SCRIPT>
<SCRIPT type="text/javascript">
    pic1 = new Image(16, 16);
    pic1.src = "loader.gif";

    $(document).ready(function () {

        $("#firstname").keyup(function () {
            var usr = $("#firstname").val();
            $("#status").html('<img src="loader.gif" align="absmiddle">&nbsp;Checking availability...');

            $.ajax({
                type: "POST",
                url: "check.php",
                data: "firstname=" + usr,
                success: function (msg) {

                    $("#status").ajaxComplete(function (event, request, settings) {

                        if (msg == 'OK') {
                            $("#firstname").removeClass('object_error');
                            $("#firstname").addClass("object_ok");
                            $(this).html('<span style="display:none;">hidden</span>&nbsp;<img src="username_checker/tick.gif" align="absmiddle">');
                        }
                        else {
                            $("#firstname").removeClass('object_ok');
                            $("#firstname").addClass("object_error");
                            $(this).html(msg);
                        }

                    });

                }

            });

            $("#firstname").removeClass('object_ok');
            $("#firstname").addClass("object_error");


        });

    });

    //-->
</SCRIPT>

<?php
$sql1 = $dbh->prepare("select * from hosts where Id='$e'");
$sql1->execute();
$data1 = $sql1->fetch();
?>
<section>
    <div class="mainwrapper">
        <?php include 'includes/leftpanel.php'; ?>

        <div class="mainpanel">
            <div class="pageheader">
                <div>
                    <div class="media-body">
                        <h4><?php if ($e == '') {
                                echo 'Add Host';
                            } else {
                                echo 'Update Host';
                            } ?></h4>
                    </div>
                </div><!-- media -->
            </div><!-- pageheader -->

            <div class="contentpanel">
                <div class="row">
                    <div class="col-md-9">
                        <form id="basicForm" name="basicForm" action="#" method="post" novalidate="novalidate">
                            <input autocorrect="off"  name="action" id="action" type="hidden">
                            <input autocorrect="off"  name="act" id="act" type="hidden">
                            <input autocorrect="off"  name="hid" id="hid" type="hidden">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="form-group">
                                            <?= $err; ?>
                                            <?= $runqury; ?>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Title</label>
                                            <div class="col-sm-6">
                                                <select id="mySelect"
                                                        name="title"
                                                        id="title"
                                                        class="form-control">
                                                    <?php
                                                    $t = $data1['Title'];
                                                    $sitessql = $dbh->prepare('SELECT *
                                                                               FROM `title`
                                                                               WHERE `Delete` = 0 ;');
                                                    $sitessql->execute();
                                                    ?>
                                                    <option value=" ">Choose One</option>
                                                    <?php while ($sitesdata = $sitessql->fetch()): ?>
                                                        <option <?php if ($sitesdata['title'] == $t) {
                                                            echo ' selected ';
                                                        } ?>>
                                                            <?= $sitesdata['title']; ?>
                                                        </option>
                                                    <?php endwhile; ?>

                                                </select>
                                            </div>
                                        </div><!-- form-group -->

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">First Name<span
                                                        class="asterisk">*</span></label>
                                            <div class="col-sm-6">
                                                <input autocorrect="off"  type="text" name="firstname" id="firstname" class="form-control"
                                                       value="<?= $data1['Firstname']; ?>" placeholder="First Name"
                                                       required="">
                                            </div>
                                            <div id="status"></div>
                                        </div>


                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Last Name<span
                                                        class="asterisk">*</span></label>
                                            <div class="col-sm-6">
                                                <input autocorrect="off"  type="text" name="lastname" id="lastname" class="form-control"
                                                       placeholder="Last Name" value="<?= $data1['Lastname']; ?>"
                                                       required="">
                                            </div>
                                            <div id="status1"></div>
                                        </div><!-- form-group -->
                                        <div class="form-group" style=" border-bottom: 1px solid #ededed;">
                                            <label class="col-sm-7 control-label"
                                                   style="font-weight:bold; width: 22%; ">
                                                Contact Details
                                                <a href="javascript:void(0);" onclick="chkphoneval();"
                                                   class="add-button" title="Add field">
                                                    <img src="images/add-icon.png" style="height: 25px;"/><br/>
                                                </a>
                                            </label>
                                            <div>
                                                <button class="btn btn-primary btn-xs" type="button" id="contact_isr">
                                                    ISR
                                                </button>
                                                <button class="btn btn-primary btn-xs" type="button" id="contact_usa">
                                                    USA
                                                </button>
                                                <button class="btn btn-primary btn-xs" type="button" id="contact_uk">
                                                    UK
                                                </button>
                                            </div>
                                        </div>
                                        <div class="form-group">

                                            <div class="col-sm-1 field">
                                                Primary
                                            </div>
                                            <label class="col-sm-1 control-label ident-type">
                                                Type
                                            </label>
                                            <div class="col-sm-3 col-xs-3 field ident-country-label">
                                                Country
                                            </div>
                                            <div class="col-sm-2 field ident-number">
                                                Phone
                                            </div>

                                        </div>
                                        <!-- /********* edit & update Code ************/-->
                                        <div class="add-info">
                                            <?php if ($e != '') {
                                                $hostid = $data1['Id'];

                                                $sqlquery = $dbh->prepare("select * from hostphone where H_id ='$hostid'");
                                                $sqlquery->execute();
                                                while ($dataquery1 = $sqlquery->fetch()) {
                                                    ?>
                                                    <div class="row form-group" id="addinput">
                                                        <div class="row col-sm-1 field">
                                                            <input autocorrect="off"  type="hidden" name="contactDetailId[]"
                                                                   value="<?= $dataquery1['Id']; ?>">
                                                            <input autocorrect="off"  name="CPrimary" value="<?= $dataquery1['Id']; ?>"
                                                                   style="margin: 15px 0px 0px 0px;"
                                                                   onclick="newid1(<?= $dataquery1['Id']; ?>,<?= $dataquery1['H_id']; ?>)"
                                                                   type="radio"<?php if ($dataquery1['HPrimary'] != '') {
                                                                echo 'checked';
                                                            } ?>>
                                                        </div>
                                                        <label class="row col-sm-1 control-label" style="width: 150px;">

                                                            <select class="form-control" name="Phonenum1[]">
                                                                <option value="<?php
                                                                if ($dataquery1['Phone_Type'] != '') {
                                                                    echo $dataquery1['Phone_Type'];
                                                                } else {
                                                                    echo '';
                                                                }
                                                                ?>  "><?php
                                                                    if ($dataquery1['Phone_Type'] != '') {
                                                                        echo $dataquery1['Phone_Type'];
                                                                    } else {
                                                                        echo 'Choose One';
                                                                    }
                                                                    ?>  </option>
                                                                <option>Work</option>
                                                                <option>Home</option>
                                                                <option>Mobile</option>
                                                                <option>Fax</option>
                                                                <option>Other</option>
                                                            </select>
                                                        </label>
                                                        <div
                                                                class="col-sm-3"><?php $domain = $dataquery1['Phone_Country']; ?>
                                                            <input autocorrect="off"  type="text" style="text-transform:uppercase"
                                                                   class="form-control valid"
                                                                   onChange="chkcountryselect(this.value,this.lang,this.id);"
                                                                   onfocus="contactCountryListener(this.value, this.id, this.lang);"
                                                                   id="countrycode<?php echo $dataquery1['Id']; ?>"
                                                                   name="countrycode1[]"
                                                                   required
                                                                   placeholder="Country"
                                                                   onKeyPress="return onlyAlphabets(event,this)"
                                                                   lang="<?php echo $dataquery1['Id']; ?>"
                                                                   value="<?php if ($dataquery1['Phone_Country'] != '') {
                                                                       echo $domain;
                                                                   } ?>">
                                                        </div>
                                                        <?php $isd = strstr($dataquery1['Phone_Number'], '-', true); ?>

                                                        <div class="row col-sm-3 field" id='phone_number'>
                                                            <input autocorrect="off"  type="text" name="phoneno1[]"
                                                                   value="<?= $dataquery1['Phone_Number']; ?>"
                                                                   id="p_new"
                                                                   class="form-control" placeholder="Phone Number"
                                                                   required
                                                            >
                                                        </div>
                                                        <div class="row col-sm-1 field" id='phone_number'>
                                                            <a href="#" onclick="removephne(<?= $dataquery1['Id']; ?>)"><img
                                                                        class="delete-img"
                                                                        src="images/remove-icon.png" border="0"
                                                                        data-toggle="tooltip" title="Remove Details"
                                                                        alt="Remove"
                                                                        data-original-title="Remove Details"></a>
                                                        </div>
                                                    </div>
                                                <?php }
                                            } ?>
                                            <!-- /********* edit & update complete here ************/-->
                                            <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
                                            <script type="text/javascript" src="js/jquery.SimpleMask.js"></script>
                                            <?php include 'add_phone_wo_row.php'; ?>
                                            <?php //include 'addphone.php'; ?>
                                        </div>
                                        </br>
                                        <div class="form-group"
                                            <?php if (!$showSite) { ?>
                                                style="display: none;"
                                            <?php } ?>
                                        >
                                            <label class="col-sm-3 control-label">
                                                Sites <span class="asterisk">*</span>
                                            </label>
                                            <div class="col-sm-6">

                                                <select data-placeholder="Choose One" id="sites" name="sites"
                                                        class="form-control" style="width:100%;margin-bottom: 1%;"
                                                        required>
                                                    <?php

                                                    $s = $e ? $data1['Sites'] : $siteDetails['Id'];
                                                    $sql11 = $dbh->prepare("select * from sites where Id='$s'");
                                                    $sql11->execute();
                                                    $data11 = $sql11->fetch();
                                                    echo '<option value=' . $data11['Id'] . '>' . $data11['Sites'] . '</option>';

                                                    if ($data11['Sites'] != '') {
                                                        $s = $data11['Sites'];
                                                        $sitessql = $dbh->prepare("SELECT distinct(Sites),Id FROM `sites` WHERE `Active`=1 and `Sites`!='" . $s . "' and `Delete`='0' order by Id desc");
                                                    } else {
                                                        $sitessql = $dbh->prepare("SELECT distinct(Sites),Id FROM `sites` WHERE `Active`=1 and `Sites`!='' and `Delete`='0' order by Id desc");
                                                    }

                                                    $sitessql->execute();
                                                    while ($sitesdata = $sitessql->fetch()) {
                                                        ?>
                                                        <option
                                                                value="<?= $sitesdata['Id'] ?>"><?= $sitesdata['Sites']; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div><!-- form-group -->

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Address</label>
                                            <div class="col-sm-6">
                                                <textarea autocorrect="off"  name="address"
                                                          class="form-control"><?= $data1['Address']; ?></textarea>
                                            </div>
                                        </div><!-- form-group -->
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Active <span class="asterisk">*</span></label>
                                            <div class="col-sm-9">
                                                <?php if ($e == '') { ?>
                                                    <div class="ckbox ckbox-primary">
                                                        <input autocorrect="off"  type="checkbox" checked id="activehost" value="1"
                                                               name="active" required="">
                                                        <label for="activehost"></label>
                                                    </div><?php } elseif ($e != '') { ?>
                                                    <div class="ckbox ckbox-primary">
                                                        <input autocorrect="off"  type="checkbox" value="1" name="active"
                                                               id="active<?= $data1['Id']; ?>"
                                                               onclick="fnactive(<?= $data1['Id']; ?>)"
                                                            <?php if ($data1['Active'] == 1) {
                                                                echo "checked";
                                                            } ?>/>
                                                        <label for="active<?= $data1['Id']; ?>"></label>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div><!-- form-group -->
                                    </div><!-- row -->
                                </div><!-- panel-body -->
                                <div class="panel-footer">
                                    <div class="row">
                                        <div class="col-sm-9 col-sm-offset-3">
                                            <?php if ($e == '') {
                                                echo '<button class="btn btn-primary mr5" name="createhosts" type="submit" id="submitchk" onclick="checkallerr()">Add Host</button>';
                                            } else {
                                                echo '<button class="btn btn-primary mr5" id="submitchk" name="updatehosts" type="submit">Update Host</button>';
                                            } ?>
                                            <a href="emvs.php?action=hosts" class="btn btn-dark"> Cancel</a>
                                            <input autocorrect="off"  id="upsubmitchk" name="next123" type="text"
                                                   style="margin-left: 5px;margin-top: 1px;display:none;">
                                        </div>
                                    </div>
                                </div><!-- panel-footer -->
                            </div><!-- panel -->
                        </form>

                    </div>
                </div><!-- row -->

            </div>
        </div><!-- mainpanel -->
    </div><!-- mainwrapper -->
</section>


<script src="js/jquery-migrate-1.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/modernizr.min.js"></script>
<script src="js/ppace.min.js"></script>

<script src="js/jquery.cookies.js"></script>

<script src="js/jquery.dataTables.min.js"></script>
<script src="js/dataTables.bootstrap.js"></script>
<script src="js/dataTables.responsive.js"></script>
<script src="js/select2.min.js"></script>

<script src="js/custom.js"></script>
<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script>
    $("#basicForm").validate({
        rules: {
            gender: {
                required: true
            }

        }
    });

</script>

</body>
</html>
