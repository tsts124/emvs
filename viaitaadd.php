<?php

error_reporting(0);
session_start();

include 'includes/dbcon.php';

function nullCoalescence($value1, $value2)
{
    return isset($value1) && !is_null($value1) ? $value1 : $value2;
}

function updateAppOption($optionName, $value, $dbh)
{
    /* @var PDO $dbh */
    $query = $dbh->prepare("UPDATE `app_options`
                            SET `value` = :value
  	                        WHERE `key` = :key ");
    $query->execute([':key' => $optionName, ':value' => $value]);
}

$a = $_GET['a'];

$sth = $dbh->prepare('SELECT `Id`
                      FROM `collectors`
                      WHERE `Id` = :collid ;');
$sth->execute(['collid' => $a]);

if (!$sth->fetch()) {
    header('Location: emvs.php?action=collector');
}

$query = $dbh->prepare(
    "
			SELECT
			*
			FROM
			`app_options`
			WHERE `app_options`.`key` = 'show_site' AND `app_options`.`value` = 1;
			"
);

$query->execute();
$showSite = $query->rowCount();


$query = $dbh->prepare(
    "
  	SELECT
  	*
  	FROM
  	`app_options`
  	WHERE
  	`app_options`.`key` = 'reference_required'
  "
);
$query->execute();
$reference = $query->fetch();
$query = $dbh->prepare(
    "
  	SELECT
  	*
  	FROM
  	`app_options`
  	WHERE
  	`app_options`.`key` = 'purpose_funds_required'
  "
);

$query->execute();
$purposeFundsRequired = $query->fetch();

$query = $dbh->prepare(
    "
  	SELECT
  	`value`
  	FROM
  	`app_options`
  	WHERE
  	`app_options`.`key` = 'reference_id_from'
  "
);
$query->execute();
$valRefId = $query->fetch();

$query = $dbh->prepare(
    "
	  SELECT
	  *
	  FROM
	  `app_options`
	  WHERE
	  `app_options`.`key` = 'default_path_photo'
	"
);

$query->execute();
$defaultPathPhoto = $query->fetch();

// by default,  to prevent app to be broken value is set to 'uploads'/
$defaultPathPhoto = ($defaultPathPhoto['value'] == '') ? "uploads/" : $defaultPathPhoto['value'];

$query = $dbh->prepare(
    "
		SELECT
		*
		FROM
		`app_options`
		WHERE
		`app_options`.`key` = 'default_path_sign'
	  "
);

$query->execute();
$defaultPathSign = $query->fetch();

// by default,  to prevent app to be broken value is set to 'uploads'/
$defaultPathSign = ($defaultPathSign['value'] == '') ? "uploads/" : $defaultPathSign['value'];

$sql = $dbh->prepare("
                    SELECT
					COUNT(*) AS `count`
					FROM
					visitstable WHERE collectorsid = " . $_POST['memberid']);
$sql->execute();
$pretest = $sql->fetch();

$muser = $_SESSION['user'];
$managersql = $dbh->prepare("
		SELECT *
		FROM newmember
		WHERE emailid ='$muser' && `Delete` ='0'
	");
$managersql->execute();
$managerdata = $managersql->fetch();

$site = $managerdata['Sites'];
$sql = $dbh->prepare("SELECT * FROM `sites` WHERE `Id`='$site' AND `Delete` = '0' ; ");
$sql->execute();
$site = $sql->fetch();
$option_default_site = $site['Sites'];

if ($managerdata['roles'] == 'Manager' && $option_default_site) {
    $managerSiteError = false;
} elseif ($managerdata['roles'] == 'Administrator' && $option_default_site) {
    $administratorSiteError = false;
} else {
    $query = $dbh->prepare(
        "
			SELECT
			*
			FROM
			`app_options`
			WHERE
			`app_options`.`key` = 'default_site'
		"
    );
    $query->execute();
    $option_site = $query->fetch();
    $option_default_site = $option_site['value'];
}

if (!$option_default_site && ($managerdata['roles'] == 'Manager')) {
    $managerSiteError = true;
} elseif (!$option_default_site && ($managerdata['roles'] == 'Administrator')) {
    $administratorSiteError = true;
}


//default donation
$query = $dbh->prepare(
    "
			SELECT
			*
			FROM
			`app_options`
			WHERE
			`app_options`.`key` = 'default_suggested_donation'
		"
);
$query->execute();
$defaultDonation = $query->fetch();

$query = $dbh->prepare(
    "
			SELECT
			*
			FROM
			`app_options`
			WHERE
			`app_options`.`key` = 'default_site_visitday_count'
		"
);
$query->execute();
$option_default_site_visitday_count = $query->fetch();

if ($option_default_site['value'] != "") {

    $query = $dbh->prepare(
        "
			SELECT
			*
			FROM
			`sites`
			WHERE
			`sites`.`Sites` = '" . $option_default_site['value'] . "' AND
			`sites`.`visitsday` = '" . $option_default_site_visitday_count['value'] . "' AND
			`sites`.`active` = 1
			"
    );
    $query->execute();
    $default_site_entry = $query->fetch();
}


$required_fields_query = $dbh->prepare("SELECT * FROM `required_fields` WHERE `deleted`='0';");
$required_fields_query->execute();
$required_fields = $required_fields_query->fetchAll();

function findAssignment($requiredFields, $tab, $assignment)
{

    $isRequired = false;
    foreach ($requiredFields as $row) {
        if ($row['tab'] == $tab
            && $row['assignment'] == $assignment
            && $row['active'] == 1
        ) {
            $isRequired = true;
            break;
        }
    }
    return $isRequired;
}

if ($_SESSION['user'] == '') {
    header('Location: emvs.php?action=index');
}


$managersql = $dbh->prepare("
		SELECT *
		FROM newmember
		WHERE emailid ='$muser' && activity ='1' && `Delete` ='0'
	");

$managersql->execute();
$managerdata = $managersql->fetch();

$abc = $_GET['abc'];


$sql = $dbh->prepare("SELECT *
        FROM  `visitstable`
        WHERE  `Refid`={$_POST['refid']}");
$sql->execute();
$countRow = $sql->rowCount();

$visit = $_POST['visit'];
$_SESSION['visit'] = $visit;
$memberid = $_POST['memberid'];
$visistprev = 'visistprev';
$refid = $_POST['refid'];


if (isset($_POST['next1']) && (($visit) || ($memberid))) {
    $sqlquery = $dbh->prepare("
				SELECT *
				FROM visitstable
				WHERE collectorsid ='$memberid' AND visitid='$visit'
			");
    $sqlquery->execute();
    $dataquerys = $sqlquery->fetch();

    $m1 = $dataquerys['collectorsid'];
    $v1 = $dataquerys['visitid'];

    $query = $dbh->prepare("SELECT *
				                   FROM `visitstable`
				                   WHERE `collectorsid` = :collId
				                   ORDER BY `visitid` DESC
				                   LIMIT 1; ");
    $query->execute(['collId' => $memberid]);
    $queryResult = $query->fetch();

    $sqlquery = $dbh->prepare("SELECT *
                                       FROM `collectors`
                                       WHERE `Id` = :collId ;");
    $sqlquery->execute(['collId' => $memberid]);

    $dataqueryval = isset($queryResult['Id']) ? $queryResult : $sqlquery->fetch();

    $memtitile = $dataqueryval['title'];
    $memfname = addslashes($dataqueryval['firstname']);
    $memmname = addslashes($dataqueryval['middlename']);
    $memlname = addslashes($dataqueryval['lastname']);
    $memcity = $dataqueryval['city'];
    $memstate = addslashes($dataqueryval['statecountry']);
    $memcountry = addslashes($dataqueryval['country']);
    $memzipcode = addslashes($dataqueryval['zipcode']);
    $memadd1 = addslashes($dataqueryval['address1']);
    $memadd2 = addslashes($dataqueryval['address2']);
    $memcomments = addslashes($dataqueryval['personalcomments']);
    $memphone = addslashes($dataqueryval['phoneno']);
    $memssnid = addslashes($dataqueryval['ssnid']);
    $memgender = addslashes($dataqueryval['gender']);
    $memdob = addslashes($dataqueryval['dob']);
    $memregdate = addslashes($dataqueryval['Addeddate']);
    $authfrom = trim($_POST['authfrom']);
    $authto = trim($_POST['authto']);
    $sites = trim($_POST['sites']);
    $sites15 = trim($_POST['sites15']);
    $visitmanager = trim($_POST['rabbis']);
    $visithost = trim($_POST['visit-host']);
    $managerid = trim($_POST['managerid']);
    $visitdriver = trim($_POST['driver']);
    $visitdays = trim($_POST['visitdays']);
    $user = $_SESSION['user'];

    $freeHost = addslashes(ucwords(trim($_POST['free_formed_host1'])));
    $freeDriver = addslashes(ucwords(trim($_POST['free_formed_driver'])));
    $freeDriverComment = addslashes(trim($_POST['free_formed_driver_comment']));

    $freeLastDate = $_POST['free_formed_last_visit_date'];
    $freeLastNumber = addslashes(trim($_POST['free_formed_last_visit_number']));

    $sql = $dbh->prepare("SELECT * FROM `sites` WHERE `Sites` = '$option_default_site' AND `Delete` = 0; ");
    $sql->execute();
    $siteData = $sql->fetch();

    if ($v1 != '') {
        $strSites = $siteData['Id'];

        $sqlquery = $dbh->prepare("
					SELECT *
					FROM `visitstable`
					WHERE `collectorsid` = '$memberid' and `visitid` = '$visit'");
        $sqlquery->execute();
        $dataquerys = $sqlquery->fetch();

        $sql = $dbh->prepare("UPDATE `visitstable`
                                  SET `rabbis` = '$visitmanager',
                                      `host` = '$visithost',
                                      `free_formed_host` = '$freeHost',
                                      `driver` = '$visitdriver',
                                      `free_formed_driver` = '$freeDriver',
                                      `free_formed_driver_comment` = '$freeDriverComment',
                                      `free_formed_last_visit_number` = '$freeLastNumber',
                                      `free_formed_last_visit_date` = '$freeLastDate'
                                  WHERE `collectorsid` = '$memberid' AND `visitid` = '$visit'; ");
        $sql->execute();

        if ($refid && filter_var($refid, FILTER_VALIDATE_INT)) {
            updateAppOption('reference_id_from', $refid, $dbh);

            $sql = $dbh->prepare('SELECT *
                                      FROM `visitstable`
                                      WHERE `Refid` = :refid ;');
            $sql->execute([':refid' => $refid]);
            if (!$sql->fetch()) {
                $sql = $dbh->prepare("UPDATE `visitstable`
                                          SET `Refid` = '$refid'
                                          WHERE `collectorsid` = '$memberid' AND `visitid` = '$visit'; ");
                $sql->execute();
            }
        } else {
            $sql = $dbh->prepare("UPDATE `visitstable`
                                          SET `Refid` = '$valRefId'
                                          WHERE `collectorsid` = '$memberid' AND `visitid` = '$visit'; ");
            $sql->execute();
        }
    } elseif ($visit != 0 && $_POST['visit']) {
        $strSites = $siteData['Id'];

        $count =
            $dbh
                ->prepare('
					INSERT INTO `visitstable`(`collectorsid`, `user`, `visitid`, `title`, `firstname`,
					                           `middlename`, `lastname`, `city`, `country`, `statecountry`,
					                           `zipcode`, `address1`, `address2`, `personalcomments`, `phoneno`,
						                       `ssnid`, `dob`, `sites`, `regdate`, `authofromdate`,
						                       `authotodate`,`rabbis`, `rabbiid`, `driver`,`host`,
						                       `free_formed_host`, `free_formed_driver`, `free_formed_driver_comment`, `balticity`, `baltistate`,
						                       `balticountry`, `baltizip`, `free_formed_last_visit_date`,`free_formed_last_visit_number`, `Refid`,
						                       `us_name`, `usaddress1`, `zipcity`, `zipstate`, `uscountry`,
						                       `uszipcode`, `zipphone`, `zipcomments`, `funds`, `institname`, `institaddress1`,
						                       `institaddress2`, `institcity`, `institstate`, `institcountry`, `institzip`,
						                       `institphone`, `institcomments`, `purpose`, `purposeother`, `purposecomments`,
						                       `donation`, `internal_purpose`, `general_comments`, `fundsreference`, `referenceother`,
						                       `referencecomments`, `sign`, `signname`, `photo`, `photoname`,
						                       `usaddress2`)
					VALUES (:collid, :usermail, :visit, :title, :firstname,
					        :middlename, :lastname, :city, :country, :state,
					        :zip, :addr1, :addr2, :comments, :phone,
						    :ssn, :dob, :sites, :regdate, :authfrom,
						    :authto,:manager, :managerid, :driver, :host,
						    :freehost, :freedriver, :drivercomment, :balticity, :baltistate,
						    :balticountry, :baltizip, :freelastdate, :freelastnumber, :Refid,
						    :us_name, :usaddress1, :zipcity, :zipstate, :uscountry,
						    :uszipcode, :zipphone, :zipcomments, :funds, :institname, :institaddress1,
						    :institaddress2, :institcity, :institstate, :institcountry, :institzip,
						    :institphone, :institcomments, :purpose, :purposeother, :purposecomments,
						    :donation, :internal_purpose, :general_comments, :fundsreference, :referenceother,
						    :referencecomments, :sign, :signname, :photo, :photoname,
						    :usaddress2);');
        $count->execute([
            'collid'            => $memberid,
            'usermail'          => $user,
            'visit'             => $visit,
            'title'             => $memtitile,
            'firstname'         => $memfname,
            'middlename'        => $memmname,
            'lastname'          => $memlname,
            'city'              => $memcity,
            'country'           => $memcountry,
            'state'             => $memstate,
            'zip'               => $memzipcode,
            'addr1'             => $memadd1,
            'addr2'             => $memadd2,
            'comments'          => $memcomments,
            'phone'             => $memphone,
            'ssn'               => $memssnid,
            'dob'               => $memdob,
            'sites'             => $strSites,
            'regdate'           => $memregdate,
            'authfrom'          => $authfrom,
            'authto'            => $authto,
            'manager'           => $visitmanager,
            'managerid'         => $managerid,
            'driver'            => $visitdriver,
            'host'              => $visithost,
            'freehost'          => trim($_POST['free_formed_host1']),
            'freedriver'        => $freeDriver,
            'drivercomment'     => $freeDriverComment,
            'balticity'         => 'Baltimore',
            'baltistate'        => 'Md',
            'balticountry'      => 'US',
            'baltizip'          => '',
            'freelastdate'      => $freeLastDate,
            'freelastnumber'    => $freeLastNumber,
            'Refid'             => $refid,
            'us_name'           => nullCoalescence($dataqueryval['us_name'], ''),
            'usaddress1'        => nullCoalescence($dataqueryval['usaddress1'], ''),
            'usaddress2'        => nullCoalescence($dataqueryval['usaddress2'], ''),
            'zipcity'           => nullCoalescence($dataqueryval['zipcity'], ''),
            'zipstate'          => nullCoalescence($dataqueryval['zipstate'], ''),
            'uscountry'         => nullCoalescence($dataqueryval['uscountry'], ''),
            'uszipcode'         => nullCoalescence($dataqueryval['uszipcode'], ''),
            'zipphone'          => nullCoalescence($dataqueryval['zipphone'], ''),
            'zipcomments'       => nullCoalescence($dataqueryval['zipcomments'], ''),
            'funds'             => nullCoalescence($dataqueryval['funds'], ''),
            'institname'        => nullCoalescence($dataqueryval['institname'], ''),
            'institaddress1'    => nullCoalescence($dataqueryval['institaddress1'], ''),
            'institaddress2'    => nullCoalescence($dataqueryval['institaddress2'], ''),
            'institcity'        => nullCoalescence($dataqueryval['institcity'], ''),
            'institstate'       => nullCoalescence($dataqueryval['institstate'], ''),
            'institcountry'     => nullCoalescence($dataqueryval['institcountry'], ''),
            'institzip'         => nullCoalescence($dataqueryval['institzip'], ''),
            'institphone'       => nullCoalescence($dataqueryval['institphone'], ''),
            'institcomments'    => nullCoalescence($dataqueryval['institcomments'], ''),
            'purpose'           => nullCoalescence($dataqueryval['purpose'], ''),
            'purposeother'      => nullCoalescence($dataqueryval['purposeother'], ''),
            'purposecomments'   => nullCoalescence($dataqueryval['purposecomments'], ''),
            'donation'          => nullCoalescence($dataqueryval['donation'], ''),
            'internal_purpose'  => nullCoalescence($dataqueryval['internal_purpose'], ''),
            'general_comments'  => nullCoalescence($dataqueryval['general_comments'], ''),
            'fundsreference'    => isset($dataqueryval['fundsreference']) ? $dataqueryval['fundsreference'] : 'Documents,Identification,References,',
            'referenceother'    => nullCoalescence($dataqueryval['referenceother'], ''),
            'referencecomments' => nullCoalescence($dataqueryval['referencecomments'], ''),
            'sign'              => nullCoalescence($dataqueryval['sign'], ''),
            'signname'          => nullCoalescence($dataqueryval['signname'], ''),
            'photo'             => nullCoalescence($dataqueryval['photo'], ''),
            'photoname'         => nullCoalescence($dataqueryval['photoname'], ''),
        ]);

        $sqlquery = $dbh->prepare('SELECT *
					               FROM `visitstable`
					               WHERE `collectorsid` = :id
					               AND `visitid` = :visitid ');
        $sqlquery->execute(['id' => $memberid, 'visitid' => $visit]);
        $dataquerys = $sqlquery->fetch();

        $sqlquery = $dbh->prepare('SELECT `Id`
                                            FROM `visitstable`
                                            WHERE `collectorsid` = :collid
                                            ORDER BY `visitid` DESC
                                            LIMIT 2;');

        $sqlquery->execute([
            'collid' => $memberid
        ]);
        $contactVisitId = $sqlquery->fetchAll()[1]['Id'];

        if (isset($contactVisitId)) {
            $sql = $dbh->prepare('SELECT *
                              FROM `visit_details`
                              WHERE `visit_id` = :visitid
                              AND `detail_type` = :detailType
                              AND `delete` = 0 ; ');
            $sql->execute(['visitid' => $contactVisitId, 'detailType' => 'contact']);
            $contactDetails = $sql->fetchAll();
        } else {
            $sql = $dbh->prepare('SELECT *
                              FROM `collectorphone`
                              WHERE `collid` = :collid
                              AND `delete` = 0 ; ');
            $sql->execute(['collid' => $memberid]);
            $contactDetails = $sql->fetchAll();
        }

        $sql = $dbh->prepare('SELECT *
                              FROM `collident`
                              WHERE `Collid` = :collid
                              AND `delete` = 0 ; ');
        $sql->execute(['collid' => $memberid]);
        $identDetails = $sql->fetchAll();

        foreach ($contactDetails as $detail) {
            $sql = $dbh->prepare('INSERT INTO `visit_details` (`visit_id`,
                                                               `detail_type`,
                                                               `primary_number`,
                                                               `type`,
                                                               `country`,
                                                               `detail_number`)
                                  VALUES (:visit_id,
                                          :detail_type,
                                          :primary_number,
                                          :number_type,
                                          :country,
                                          :detail_number) ; ');
            $sql->execute([
                'visit_id'       => $dataquerys['Id'],
                'detail_type'    => 'contact',
                'primary_number' => nullCoalescence($detail['CPrimary'], $detail['primary_number']),
                'number_type'    => nullCoalescence($detail['Phone_Type'], $detail['type']),
                'country'        => nullCoalescence($detail['Phone_Country'], $detail['country']),
                'detail_number'  => nullCoalescence($detail['Phone_Number'], $detail['detail_number'])
            ]);
        }

        foreach ($identDetails as $detail) {
            $sql = $dbh->prepare('INSERT INTO `visit_details` (`visit_id`,
                                                               `detail_type`,
                                                               `primary_number`,
                                                               `type`,
                                                               `country`,
                                                               `detail_number`,
                                                               `passport_name`)
                              VALUES (:visit_id,
                                      :detail_type,
                                      :primary_number,
                                      :number_type,
                                      :country,
                                      :detail_number,
                                      :passport_name) ; ');
            $sql->execute([
                'visit_id'       => $dataquerys['Id'],
                'detail_type'    => 'identification',
                'primary_number' => $detail['Primary1'],
                'number_type'    => $detail['Collidenti'],
                'country'        => $detail['Collcountry'],
                'detail_number'  => $detail['Collphone'],
                'passport_name'  => $detail['passport_name']
            ]);
        }
        if (is_array($valRefId)) {
            $valRefId = $valRefId[0];
        }

        if ($refid && filter_var($refid, FILTER_VALIDATE_INT)) {
            updateAppOption('reference_id_from', $refid, $dbh);

            $sql = $dbh->prepare('SELECT *
                                      FROM `visitstable`
                                      WHERE `Refid` = :refid ;');
            $sql->execute([':refid' => $refid]);
            if (!$sql->fetch()) {
                $sql = $dbh->prepare("UPDATE `visitstable`
                                          SET `Refid` = '$refid'
                                          WHERE `collectorsid` = '$memberid' AND `visitid` = '$visit'; ");
                $sql->execute();
            }
        } else {

            $sql = $dbh->prepare("UPDATE `visitstable`
                                          SET `Refid` = '$valRefId'
                                          WHERE `collectorsid` = '$memberid' AND `visitid` = '$visit'; ");
            $sql->execute();
        }
    }
}

$sqlquery = $dbh->prepare("
				SELECT *
				FROM `visitstable`
				WHERE `collectorsid` = '$memberid' AND `visitid` = '$visit';
			");
$sqlquery->execute();
$dataquerys = $sqlquery->fetch();
$personalComments = $dataquerys['personalcomments'];
if (isset($_POST['tempadd'])) {

    $memberid = $_POST['memberid'];
    $visitid = $_POST['visitid'];
    $personalinfonext = 'personalinfonext';

    $sqlquery = $dbh->prepare("select * from collectors where Id ='$memberid'");
    $sqlquery->execute();
    $dataquery = $sqlquery->fetch();
}

if (isset($_POST['localadd'])) {

    $memberid = $_POST['memberid'];
    $visitid = $_POST['visitid'];
}

if (isset($_POST['update'])) {

    $title = addslashes($_POST['title']);
    $address1 = $_POST['address1'];
    $firstname = $_POST['firstname'];
    $address2 = $_POST['address2'];
    $middlename = $_POST['middlename'];
    $city = $_POST['city'];
    $lastname = $_POST['lastname'];
    $state = $_POST['state'];
    $phone = $_POST['phone'];
    $zipcode = $_POST['zipcode'];
    $ssnid = $_POST['ssnid'];
    $dob = $_POST['dob'];
    $gender = $_POST['gender'];
    $memberids = $_POST['memberids'];
    $visitid = $_POST['visitid'];
    $personinfohide = $_POST['personinfohide'];
    $sql = $dbh->exec("UPDATE collectors SET title='$title',firstname='$firstname',middlename='$middlename',lastname='$lastname',city='$city',statecountry='$state',country='$country',zipcode='$zipcode',address1='$address1',address2='$address2',phoneno='$phone',ssnid='$ssnid',gender='$gender',dob='$dob',Updateddate=now() WHERE Id=$abc");
}
$sql = $dbh->prepare("SELECT * FROM collectors ORDER BY Id DESC");
$sql->execute();
$data = $sql->fetch();
$memberid1 = $data['Id'];


$title = addslashes($_POST['title']);
$address1 = $_POST['address1'];
$firstname = $_POST['firstname'];
$address2 = $_POST['address2'];
$middlename = $_POST['middlename'];
$city = $_POST['city'];
$lastname = $_POST['lastname'];
$state = $_POST['state'];
$phone = $_POST['phone'];
$zipcode = $_POST['zipcode'];
$ssnid = $_POST['ssnid'];
$dob = $_POST['dob'];
$gender = $_POST['gender'];
$memberids = $_POST['memberids'];

if (isset($_POST['prev1'])) {

    $memberid = $_POST['memberid'];
    $sqlquery = $dbh->prepare("select * from collectors where Id = '$memberid'");
    $sqlquery->execute();
    $dataquery12 = $sqlquery->fetch();
    $visistprev = 'visistprev';
}

if (isset($_POST['action']) && ($_POST['action'] == "collupdate")) {

    $upquery1 = $dbh->prepare("UPDATE collident SET Primary1=' ' WHERE Collid='" . $_POST['act'] . "'");
    $upquery1->execute();
    $upquery = $dbh->prepare("UPDATE collident SET Primary1='" . $_POST['act'] . "' WHERE Id='" . $_POST['hid'] . "'");
    $upquery->execute();
}

if (isset($_POST['action']) && ($_POST['action'] == "collphoneupdate")) {

    $upquery1 = $dbh->prepare("UPDATE collectorphone SET phone_primary=' ' WHERE Collid='" . $_POST['act'] . "'");
    $upquery1->execute();
    $upquery = $dbh->prepare("UPDATE collectorphone SET phone_primary='" . $_POST['act'] . "' WHERE Id='" . $_POST['hid'] . "'");
    $upquery->execute();
}

if (isset($_POST['action']) && ($_POST['action'] == "delete")) {

    $sql = $dbh->prepare("SELECT * FROM collectorphone WHERE Id='" . $_POST['act'] . "'");
    $sql->execute();
    $deldata = $sql->fetch();
    $visitsnext = 'visitsnext';
    if ($deldata['CPrimary'] == '') {

        $delquery1 = $dbh->prepare("DELETE FROM collectorphone WHERE Id='" . $_POST['act'] . "'");
        $delquery1->execute();
    } else {

        echo '<script>alert("Primary Phone Number Can`t Delete")</script>';
    }
}

if (isset($_POST['action']) && ($_POST['action'] == "removeIdent")) {

    $sql = $dbh->prepare("SELECT * FROM collident WHERE Id='" . $_POST['act'] . "'");
    $sql->execute();
    $deldata = $sql->fetch();
    $visitsnext = 'visitsnext';

    if ($deldata['Primary1'] == '') {
        $delquery1 = $dbh->prepare('UPDATE `collident`
                                    SET `delete` = 1
                                    WHERE `Id` = :id');
        $delquery1->execute(['id' => $_POST['act']]);
    } else {

        echo '<script>alert("Primary Identification Number Can`t Delete")</script>';
    }
}

$sql = $dbh->prepare("select * from `collectors` where `Id` = '$a'");
$sql->execute();
$dataquery11 = $sql->fetch();

$sql = $dbh->prepare("SELECT *
                              FROM `visitstable`
                              WHERE `collectorsid` = :id
                              ORDER BY `visitid` DESC; ");
$sql->execute([':id' => $a]);
$lastVisitData = $sql->fetch();

$sql = $dbh->prepare('SELECT `Id`
                      FROM `visitstable`
                      WHERE `collectorsid` = :id
                      ORDER BY `visitid`
                      DESC; ');
$sql->execute(['id' => $a]);

if ($lastVisitData['authofromdate'] && count($sql->fetchAll()) > 0) {
    $lastVisit = date_create($lastVisitData['authofromdate']);
    $lastVisit = date_format($lastVisit, 'm/d/y');
} else {
    $lastVisit = '';
}

if (strlen($dataquery11['dob']) > 4) {
    $parts = explode('-', $dataquery11['dob']);
    $year = $parts[2] ? $parts[2] : $parts[1];

    if (preg_match('/[a-zA-Z]/', $parts[0])) {
        $date = date_create($dataquery11['dob']);
        $date = date_format($date, 'm/d/y');
        $newParts = explode('/', $date);

        $date = $newParts[0] . '/' . $newParts[1] . '/' . $year[2] . $year[3];
    } else {
        if (strlen($parts[0]) == 1) {
            $parts[0] = '0' . $parts[0];
        }

        if (strlen($parts[1]) == 1) {
            $parts[1] = '0' . $parts[1];
        }

        $date = $parts[0] . '/' . $parts[1] . '/' . $year[2] . $year[3];
    }

} else {
    $date = $dataquery11['dob'];
}


$pagename = $dataquery11['title'] . ' ' .
    $dataquery11['firstname'] . ' ' .
    $dataquery11['lastname'] .
    '&nbsp;&nbsp; Birthdate: ' .
    $date . '&nbsp;&nbsp; Last Visit: ' .
    $lastVisit;

include 'includes/header.php';
?>
<!-- popup window-->

<script type="text/javascript" src="js/slimbox2.js"></script>
<link rel="stylesheet" href="css/slimbox2.css" type="text/css" media="screen"/>
<link rel="stylesheet" href="css/style.default.css" type="text/css" media="screen"/>
<!--Select box validation-->
<script src="js/jquery-2.1.0.min.js"></script>
<script src="js/jquery.validation.js"></script>

<script src="pellepim-jstimezonedetect/dist/jstz.min.js"></script>
<script src="js/moment.min.js"></script>
<script src="js/moment-timezone-with-data-2010-2020.min.js"></script>

<link rel="stylesheet" href="css/jquery.validation.css">
<!-- End Validation-->
<script>
    function show1() {
        document.getElementById('div1').style.display = 'none';
        document.getElementById('div2').style.display = 'block';
    }
    function show2() {
        document.getElementById('div1').style.display = 'block';
        document.getElementById('div2').style.display = 'none';
    }
    function newid(a, b) {
        document.getElementById('act').value = b;
        document.getElementById('action').value = "collupdate";
        document.getElementById('hid').value = a;
        document.valWizard.submit();
    }
    function newid1(a, b) {
        document.getElementById('act').value = b;
        document.getElementById('action').value = "collphoneupdate";
        document.getElementById('hid').value = a;
        document.valWizard.submit();
    }
</script>
<link href="datepicker/jquery-ui.css" rel="Stylesheet" type="text/css"/>
<script type="text/javascript" src="datepicker/jquery-ui.js"></script>
<script language="javascript">
    $(document).ready(function () {
        var pageName = $('#page-name').val();
        $('.pagename').html("<span>New Visit : </span><span class='two'> " + pageName + "</span>");

        //setting passport name label for identification details
        $("select[name='type']").each(function (index, element) {
            if ($(this).val() == 'Passport') {
                $('#passport-name-label').html('Passport');
            }
        });

        //getting user time zone
        var timeZone = jstz.determine();
        var zone = timeZone.name();
        var visitId = $('#visitId').val();
        var authPeriodFrom = $('#txtdate');
        var $regDate = $('#txtdate3');

        var format = 'MMM-DD-YYYY';
        var today = moment().tz(zone).format(format);

        if (visitId == 1 || $regDate.val() == '') {
            $regDate.val(today);
        }
        if (authPeriodFrom.val() == '') {
            authPeriodFrom.val(today);
        }

        $('#collident').on('focusout change', 'input, select', function () {
            var valueElement = $(this).val();  //val element onfocusout
            var thisName = $(this).attr('name'); // collum this BD
            var idRow = $(this).closest('div[data-id]').data('id');  //row ID DB
            var visitid = $('#the-visit-id').val();

            var dataString = 'valueElement=' + valueElement +
                '&a=' + <?=$a?> +
                    '&name=' + thisName +
                '&visitid=' + visitid +
                '&idRow=' + idRow +
                '&thisTable=visit_details';
            $.ajax({
                type: "POST",
                url: "ajax_saveFocusout.php",
                data: dataString,
                success: function (msg) {

                }
            });
        });
        $('#visits').on('change focusout', 'input, select, textarea', function () {
            var dataString = '';
            var valueElement = $(this).val();  //val element onfocusout
            $(this).val(valueElement);
            var thisName = $(this).attr('name'); // collum this BD
            dataString += 'valueElement=' + valueElement + '&a=' + <?=$a?> +'&b=' + <?=$_SESSION['visit']?> +'&name=' + thisName + '&thisTable=visitstable';
            $.ajax({
                type: "POST",
                url: "ajax_saveFocusout.php",
                data: dataString,
                success: function (msg) {
                }
            });
        });

        $('#contact').on('change focusout', 'input, select', function () {
            var valueElement = $(this).val();  //val element onfocusout
            var thisName = $(this).attr('name'); // collum this BD
            var idRow = $(this).closest('div[data-id]').data('id');  //row ID DB
            var visitid = $('#the-visit-id').val();

            dataString = 'valueElement=' + valueElement + '&a=' + <?=$a?> +'&name=' +
                thisName + '&thisTable=visit_details' + '&idRow=' + idRow + '&visitid=' + visitid;
            $.ajax({
                type: "POST",
                url: "ajax_saveFocusout.php",
                data: dataString
            });
        });

        $('#host').on('change', function () {
            var dataString = '';
            var valueElement = $(this).val();  //val element onfocusout
            var thisName = 'host'; // collum this BD
            dataString += 'valueElement=' + valueElement + '&a=' + <?=$a?> +'&b=' + <?=$_SESSION['visit']?> +'&name=' + thisName + '&thisTable=visitstable';

            $.ajax({
                type: "POST",
                url: "ajax_saveFocusout.php",
                data: dataString
            });
        });

        $('#free-formed-host').on('change', function () {
            var dataString = '';
            var valueElement = $(this).val();  //val element onfocusout
            var thisName = 'free_formed_host'; // collum this BD
            dataString += 'valueElement=' + valueElement + '&a=' + <?=$a?> +'&b=' + <?=$_SESSION['visit']?> +'&name=' + thisName + '&thisTable=visitstable';
            $.ajax({
                type: "POST",
                url: "ajax_saveFocusout.php",
                data: dataString
            });
        });

        $('#tab3-4').on('change', 'input, select, textarea', function () {
            var dataString = '';
            var valueElement = $(this).val();  //val element onfocusout
            var thisName = $(this).attr('name'); // collum this BD
            dataString += 'valueElement=' + valueElement + '&a=' + <?=$a?> +'&b=' + <?=$_SESSION['visit']?> +'&name=' + thisName + '&thisTable=visitstable';
            $.ajax({
                type: "POST",
                url: "ajax_saveFocusout.php",
                data: dataString,
                success: function (msg) {
                }
            });
        });
        $('#tab4-4').on('change', 'input, select, textarea', function () {
            var dataString = '';
            var valueElement = $(this).val();  //val element onfocusout
            var thisName = $(this).attr('name'); // collum this BD
            dataString += 'valueElement=' + valueElement + '&a=' + <?=$a?> +'&b=' + <?=$_SESSION['visit']?> +'&name=' + thisName + '&thisTable=visitstable';
            $.ajax({
                type: "POST",
                url: "ajax_saveFocusout.php",
                data: dataString,
                success: function (msg) {
                }
            });
        });
        $('#tab5-4').on('change', 'input, select, textarea', function () {
            var dataString = '';
            var str = '';
            $('input[name="active[]"]:checked').each(function () {
                str += $(this).val() + ',';
            });
            var valueElement = $(this).val();  //val element onfocusout
            var thisName = $(this).attr('name'); // collum this BD
            if (thisName == 'active[]') {
                thisName = 'purpose';
                valueElement = str;
            }
            dataString += 'valueElement=' + valueElement + '&a=' + <?=$a?> +'&b=' + <?=$_SESSION['visit']?> +'&name=' + thisName + '&thisTable=visitstable';
            $.ajax({
                type: "POST",
                url: "ajax_saveFocusout.php",
                data: dataString,
                success: function (msg) {
                }
            });
        });
        $('#tab6-4').on('change', 'input, select, textarea', function () {
            var dataString = '';
            var str1 = '';
            $('input[name="active1[]"]:checked').each(function () {
                str1 += $(this).val() + ',';
            });
            var valueElement = $(this).val();  //val element onfocusout
            var thisName = $(this).attr('name'); // collum this BD
            if (thisName == 'active1[]') {
                thisName = 'fundsreference';
                valueElement = str1;
            }
            dataString += 'valueElement=' + valueElement + '&a=' + <?=$a?> +'&b=' + <?=$_SESSION['visit']?> +'&name=' + thisName + '&thisTable=visitstable';
            $.ajax({
                type: "POST",
                url: "ajax_saveFocusout.php",
                data: dataString,
                success: function (msg) {
                }
            });
        });

        //Default references saving
        var dataString = '';
        var str1 = '';
        $('input[name="active1[]"]:checked').each(function () {
            str1 += $(this).val() + ',';
        });
        var thisName = 'fundsreference';

        dataString += 'valueElement=' + str1 + '&a=' + <?=$a?> +'&b=' + <?=$_SESSION['visit']?> +'&name=' + thisName + '&thisTable=visitstable';
        $.ajax({
            type: "POST",
            url: "ajax_saveFocusout.php",
            data: dataString,
            success: function (msg) {
            }
        });

        $("#txtdate").datepicker({
            //minDate: 0,
            changeMonth: true,
            changeYear: true,

            onChangeMonthYear: function (year, month) {
                $("#startYear").val(year);
                $("#startMonth").val(month);
            }
        });
    });
    $(".ui-datepicker-month").prepend("<option value='' selected='selected'>Month</option>");

    $(".ui-datepicker-year").prepend("<option value='' selected='selected'>Year</option>");

    $(document).ready(function () {
        $("#txtdate1").datepicker({
            maxDate: 0,
            changeMonth: true,
            changeYear: true,

            onChangeMonthYear: function (year, month) {
                $("#startYear").val(year);
                $("#startMonth").val(month);
            }
        });

        $("#last-visit-date").datepicker({
            changeMonth: true,
            changeYear: true,
            defaultDate: "-1y",
            showButtonPanel: true,
            beforeShow: function (input) {
                setTimeout(function () {
                    var buttonPane = $(input)
                        .datepicker("widget")
                        .find(".ui-datepicker-buttonpane").html('');

                    var btn = $('<button class="ui-datepicker-current ui-state-default ui-priority-secondary ui-corner-all" type="button">Clear</button>');
                    btn.unbind("click").bind("click", function () {
                        $.datepicker._clearDate(input);
                    });

                    btn.appendTo(buttonPane);

                }, 1);
            }
        });

        $("#txtdate3").datepicker({
            //maxDate: 0,
            changeMonth: true,
            changeYear: true,

            onChangeMonthYear: function (year, month) {
                // $("#startYear").val(year);
                //$("#startMonth").val(month);
            }
        });
    });
    $(".ui-datepicker-month").prepend("<option value='' selected='selected'>Month</option>");

    $(".ui-datepicker-year").prepend("<option value='' selected='selected'>Year</option>");
</script>

<script type="text/javascript">

    function show1() {
        document.getElementById('div1').style.display = 'none';
        document.getElementById('div2').style.display = 'block';
    }
    function show2() {
        document.getElementById('div1').style.display = 'block';
        document.getElementById('div2').style.display = 'none';
    }

    function newid(a, b) {
        document.getElementById('act').value = b;
        document.getElementById('action').value = "collupdate";
        document.getElementById('hid').value = a;
        document.novalidate.submit();
    }
    function newid1(a, b) {
        document.getElementById('act').value = b;
        document.getElementById('action').value = "collphoneupdate";
        document.getElementById('hid').value = a;
        document.novalidate.submit();
    }
    $(function () {
        $('#next1').on('click', function () {
            if ($('#visitdriver').val()) {
                //return true;
            } else {
                //alert('Required field "Driver"');
                //return false;
            }

        });
        $('#next2').on('click', function () {
            if ($('#visitdriver').val()) {
                //return true;
            } else {
                //alert('Required field "Driver"');
                //return false;
            }
        });
    });

</script>
<script type="text/javascript">

    $(document).ready(function () {

        $(".form-control").change(
            function () {
                var id = $(this).val();
                var dataString = 'id=' + id;
                $.ajax
                ({
                    type: "POST",
                    url: "ajax_city.php",
                    data: dataString,
                    cache: false,
                    success: function (html) {
                        $(".city").html(html);
                    }
                });
            }
        );
    });
</script>

<style type="text/css">
    /* padding-bottom and top for image */
    .mfp-no-margins img.mfp-img {
        padding: 0;
    }

    /* position of shadow behind the image */
    .mfp-no-margins .mfp-figure:after {
        top: 0;
        bottom: 0;
    }

    /* padding for main container */
    .mfp-no-margins .mfp-container {
        padding: 0;
    }

    .rotated {
        -webkit-transform: rotate(180deg);
        -moz-transform: rotate(180deg);
        -o-transform: rotate(180deg);
        -ms-transform: rotate(180deg);
        transform: rotate(180deg);
    }

    .cross {
        position: absolute;
        left: 220px;
        top: 112px;
        z-index: 1;
    }

    .rotate {
        background-size: 100%;
        background-repeat: no-repeat;
        position: absolute;
        left: 220px;
        top: 0px;
        z-index: 1;
    }

    .doc-wrapper {
        position: relative;
        padding-bottom: 20px;
    }

    #coll-documents-galery {
        display: flex;
        flex-wrap: wrap;
        justify-content: space-around;
        align-content: flex-start;
        align-items: stretch;
        clear: both;
        padding-top: 20px;
        width: 100%;
    }

    .visit-row.visit-row.visit-row td {
        background-color: #FFFDB5;
    }

    /*this css is used for align checkboxes*/
    input[type=checkbox] {
        margin: 0;
    }

    input[type=checkbox] + label {
        padding: 0px 10px 10px 0px;
    }
</style>

<style type="text/css">
    /* This CSS is used for the Show/Hide functionality. */
    .override-error {
        color: #428bca;
        font-size: 9pt;
    }

    #example {
        display: none;
    }

    #example1 {
        display: none;
    }

    a.show-link, a.hide-link {
        text-decoration: none;
        color: #36f;
        padding-left: 8px;
        background: transparent url(down.gif) no-repeat left;
    }

    a.hide-link {
        background: transparent url(up.gif) no-repeat left;
    }

    a.show-link:hover, a.hide-link:hover {
        border-bottom: 1px dotted #36f;
    }
</style>

<style>
    .show {
        display: visible;
    }

    .hide {
        display: none;
    }
</style>

<script language="javascript" type="text/javascript">

    function showHide(shID) {

        //alert(shID);
        if (document.getElementById(shID)) {
            if (document.getElementById(shID + '-show').style.display != 'show') {
                document.getElementById(shID + '-show').style.display = 'show';
                document.getElementById(shID).style.display = 'block';
            } else {
                document.getElementById(shID + '-show').style.display = 'inline';
                document.getElementById(shID).style.display = 'none';
            }
        }
    }
    function showHide1(shID1) {
        //alert(shID1);
        if (document.getElementById(shID1 + '-show').style.display != 'none') {
            document.getElementById(shID1 + '-show').style.display = 'none';
            document.getElementById(shID1).style.display = 'none';
        }
    }
</script>
<script type="text/javascript">

    function myfunction(a, b) {
        //document.getElementById('visitval').value = "select";
        //document.getElementById('action').value = a;
        var dataString = 'edit=' + a + '&editcoll=' + b;
        $.ajax({
            type: "POST",
            url: "editprocess.php",
            data: dataString,
            success: function (msg) {
                $('#result').html(msg);
            }
        });
        //return false;
    }

    function usadd(a, b) {
        var dataString = 'usaddress1=' + a + '&visitid=' + b;
        $.ajax({
            type: "POST",
            url: "ajax_address.php",
            data: dataString,
            success: function (msg) {
                $('#rresult').html(msg);
            }
        });
        //return false;
    }

    function baltiadd(val1, val2) {
        var usaddress1 = document.getElementById('tempaddress1').value;
        var usaddress2 = document.getElementById('tempaddress2').value;
        var uscity = document.getElementById('tempcity').value;
        var usstate = document.getElementById('tempstate').value;
        var uscountry = document.getElementById('tempcountry').value;
        var uszipcode = document.getElementById('tempzipcode').value;
        var usphone = document.getElementById('tempphone').value;
        var uscomments = document.getElementById('tempcomments').value;
        //var dataString = 'memberid='+val+'&visitid='+visitid;

        //var dataString = 'memberid=' + val1 + '&visitid=' + val2 + '&usaddress1=' + usaddress1 + '&usaddress2=' + usaddress2 + '&uscity=' + uscity + '&usstate=' + usstate + '&uscountry=' + uscountry + '&uszipcode=' + uszipcode + '&usphone=' + usphone + '&uscomments=' + uscomments;
        jQuery.ajax({
            type: "POST",
            url: "ajax_local_address.php",
            data: dataString,
            success: function (msg) {
                $('#localaddress').html(msg);
            }
        });
        //return false;
    }
</script>
<script language="JavaScript">

    function setVisibility(id, visi) {
        document.getElementById(id).style.visibility = visi;
    }

</script>

<script type="text/javascript">
    function other1(x) {
        x.className = (x.className == "first") ? "second" : (x.className == "second") ? "first" : "second";
    }

</script>

<style>
    .first {
        display: none;
    }

    .second {
        display: show;
    }

    #tooltip {
        text-align: center;
        color: #fff;
        background: #111;
        position: absolute;
        z-index: 100;
        padding: 15px;
    }

    #tooltip:after /* triangle decoration */
    {
        width: 0;
        height: 0;
        border-left: 10px solid transparent;
        border-right: 10px solid transparent;
        border-top: 10px solid #111;
        content: '';
        position: absolute;
        left: 50%;
        bottom: -10px;
        margin-left: -10px;
    }

    #tooltip.top:after {
        border-top-color: transparent;
        border-bottom: 10px solid #111;
        top: -20px;
        bottom: auto;
    }

    #tooltip.left:after {
        left: 10px;
        margin: 0;
    }

    #tooltip.right:after {
        right: 10px;
        left: auto;
        margin: 0;
    }
</style>
<script>
    $(function () {
        var targets = $('[rel~=tooltip]'),
            target = false,
            tooltip = false,
            title = false;

        targets.bind('mouseenter', function () {
            target = $(this);
            tip = target.attr('title');
            tooltip = $('<div id="tooltip"></div>');

            if (!tip || tip == '')
                return false;

            target.removeAttr('title');
            tooltip.css('opacity', 0)
                .html(tip)
                .appendTo('body');

            var init_tooltip = function () {
                if ($(window).width() < tooltip.outerWidth() * 1.5)
                    tooltip.css('max-width', $(window).width() / 2);
                else
                    tooltip.css('max-width', 340);

                var pos_left = target.offset().left + ( target.outerWidth() / 2 ) - ( tooltip.outerWidth() / 2 ),
                    pos_top = target.offset().top - tooltip.outerHeight() - 20;

                if (pos_left < 0) {
                    pos_left = target.offset().left + target.outerWidth() / 2 - 20;
                    tooltip.addClass('left');
                }
                else
                    tooltip.removeClass('left');

                if (pos_left + tooltip.outerWidth() > $(window).width()) {
                    pos_left = target.offset().left - tooltip.outerWidth() + target.outerWidth() / 2 + 20;
                    tooltip.addClass('right');
                }
                else
                    tooltip.removeClass('right');

                if (pos_top < 0) {
                    var pos_top = target.offset().top + target.outerHeight();
                    tooltip.addClass('top');
                }
                else
                    tooltip.removeClass('top');

                tooltip.css({left: pos_left, top: pos_top})
                    .animate({top: '+=10', opacity: 1}, 50);
            };

            init_tooltip();
            $(window).resize(init_tooltip);

            var remove_tooltip = function () {
                tooltip.animate({top: '-=10', opacity: 0}, 50, function () {
                    $(this).remove();
                });

                target.attr('title', tip);
            };

            target.bind('mouseleave', remove_tooltip);
            tooltip.bind('click', remove_tooltip);
        });
    });
</script>

<!----------- Add more JS------------>
<script src="js/jquery.maskedinput.js" type="text/javascript"></script>
<script type="text/javascript">
    //Phone Number format
    jQuery(function ($) {
        $('#phone0').simpleMask({'mask': 'usa', 'nextInput': false});
        $('#tempphone').simpleMask({'mask': 'usa', 'nextInput': false});
        $('#baltiphone').simpleMask({'mask': 'usa', 'nextInput': false});

        $('#ssn0').simpleMask({'mask': 'ssn', 'nextInput': false});
    });
    //Check the value set in phone number
    function chkAllVal1() {
        var pno = document.getElementsByName("phoneno[]");
        var count = 0;
        for (var i = 0; i < pno.length; i++) {
            var id = document.getElementById("phone" + i);
            var val = id.value;
            var actVal = val.replace("_", "");
            alert(actVal.length);
            if (actVal.length == 14) {
                count++;
            }
        }
        if (count == pno.length) {
        }
        else {
            document.getElementById("phone" + count).focus();
            alert('Phone number invalid');
            return false;
        }
    }

    function chkAllVal() {

        var pno = document.getElementsByName("phoneno[]");
        var count = 0;

        for (var i = 0; i < pno.length; i++) {

            var id = document.getElementById("phone" + i);
            var val = id.value;
            var actVal = val.replace("_", "");
            if (actVal.length == 14) {
                count++;
            }
        }
        if (count == pno.length) {

            var less = i - 1;
            hello();
            document.getElementById("error" + less).style.visibility = "hidden";
        } else {
            document.getElementById("phone" + count).focus();

            document.getElementById("error" + count).style.visibility = "visible";
            return false;
        }
    }
    //********Remove Phone Number using AJAX Page
    function hello1(a) {
        $("#remove" + a).remove();
        a--;
    }
    function hellomem(a) {
        $("#remove" + a).remove();
        a--;
    }
    //********Add Phone Number using AJAX Page
    function hello() {
        //alert("Hello there!");
        countClicks++;
        var visitadd = 'visitadd';
        document.getElementById('phoneid').value = countClicks;
        var dataString = 'countClicks=' + countClicks + '&visitadd=' + visitadd;
        $.ajax({
            type: "POST",
            url: "ajax_addmore_member.php",
            data: dataString,
            success: function (msg) {
                //$('#addmore').html(msg);
                var strVal = msg.split("$$$$$");
                var strDiv = document.getElementById("addmore").outerHTML;
                document.getElementById("addmore").outerHTML = msg + strDiv;
                $("#phone" + countClicks).mask("(999) 999-9999");
            }
        });
    }
    var countClicks = 0;
    //Remove Identification Number using AJAX Page
    function helloidenti1(a) {
        //alert(a);
        $("#remove1" + a).remove();
        a--;
    }
    //Add Identification Number using AJAX Page
    function helloidenti() {
        countClicks1++;
        document.getElementById('identiid').value = countClicks1;
        // alert(countClicks1);
        var dataString = 'countClicks1=' + countClicks1;
        $.ajax({
            type: "POST",
            url: "ajax_addmore_memberidenti.php",
            data: dataString,
            success: function (msg) {
                //$('#addmore').html(msg);
                var strVal = msg.split("$$$$$");
                var strDiv = document.getElementById("addmore1").outerHTML;
                document.getElementById("addmore1").outerHTML = msg + strDiv;
                $("#ssn" + countClicks1).mask("999-99-9999");
            }
        });
    }
    var countClicks1 = 0;

    //Check SSN Number Value set
    function Chkval() {
        var pno = document.getElementsByName("Identno[]");

        for (var i = 0; i < pno.length; i++) {
            var id = document.getElementById("ssn" + i);
            var val = id.value;
            var actVal = val.replace("_", "");
            if (val.length < 14) {
                document.getElementById("ssn" + i).focus();
                //document.getElementById("phone"+i).setSelectionRange(actVal.length-1,actVal.length);
                return false;
            }
        }
    }
</script>

<script type="text/javascript">


    function myfun() {

        var a = document.getElementById("sites").value;
        var sitesvisid = document.getElementById("sitesvisid").value;
        var sitesmemid = document.getElementById("sitesmemid").value;
        var dataString = 'id=' + a + '&sitesmemid=' + sitesmemid + '&sitesvisid=' + sitesvisid;
        jQuery.ajax({
            type: "POST",
            url: "ajax_sites.php",
            data: dataString,
            success: function (msg) {
                myfunval();
            }
        });
    }

    function toggleClass() {
        alert('Please create your visits');
    }

    function myfunval() {
        var a = document.getElementById("txtdate").value;
        var b = document.getElementById("visitdays").value;
        var c = document.getElementById("sitesmemid").value;
        var d = document.getElementById("sitesvisid").value;
        var dataString = 'date=' + a + '&visitdays=' + b + '&member=' + c + '&visit=' + d;

        $.ajax({
            type: "POST",
            url: "ajax_date.php",
            data: dataString,
            success: function (msg) {
                $('#todate').html(msg);
                var x = document.getElementById("dateerrormsg").value;
                x = x.split(/[0-9]+\)/);
                var messages = '';
                var i = 1;
                x.forEach(function (item, i, arr) {
                    if (item != '') {
                        messages += i + ') ' + item + '<br>';
                        i++;
                    }
                });
                document.getElementById("dateerrormsg1").innerHTML = messages;
                Validdate();
            }
        });
    }

    function Validdate() {
        var Vdate = document.getElementById('ValidDate').value;
        var errormsg = document.getElementById('dateerrormsg').value;
        if (errormsg != '') {
            document.getElementById('next1').disabled = true;
        } else {
            document.getElementById('next1').disabled = false;
        }
    }

    function mysite() {
        var b = document.getElementById("sites15").value;
        var c = document.getElementById("sites").value;
        if (b == '') {
            var a = document.getElementById("sites").value;
            if (a == '') {
                //alert('Choose site');
            }
        } else if ((b != '') && (c == '')) {
            var dataString = 'id=' + b;
            $.ajax({
                type: "POST",
                url: "ajax_sites.php",
                data: dataString,
                success: function (msg) {
                    jQuery('#sites1').html(msg);
                }
            });
        }
    }
</script>

<style type="text/css">
    .step_box {
        border: 1.0px solid rgb(247, 247, 247);
    }

    .step_box:hover, #selected_step_box, .QuickStartLong:hover {
        background: rgb(247, 247, 247);
    }

    .selected {
        background-color: #ddd;
    }
</style>

<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery('.step_wrapper').on('click', '.step_box', function () {
            jQuery('.step_box').removeClass('selected');
            jQuery(this).addClass('selected')
        });
    });
</script>

<style type="text/css">
    .Lime {
        background-color: Lime;
        border: 1px solid black;
        padding: 10px;
    }
</style>
<script type="text/javascript">
    function Change_Class_Name(My_Element, My_Class) {
        My_Element.className = My_Class;
        setTimeout(2000);
    }
</script>

<?php
$loginemail = $_SESSION['user'];

$loginsql = $dbh->prepare("SELECT * FROM `newmember` WHERE `emailid`='$loginemail' AND `Delete` = 0");
$loginsql->execute();
$logindata = $loginsql->fetch();
$r = $logindata['roles'];
?>

<body onload="myfun();">


<script>
    var popupWindow = null;
    function signadd(addval1, addval2) {

        popupWindow = window.open("visitssignature.php?a=" + addval1 + '&b=' + addval2, "_blank", "toolbar=yes, scrollbars=yes, resizable=yes, top=235, left=550, width=500, height=200");
        popupWindow.focus();
        document.onmousedown = focusPopup;
        document.onkeyup = focusPopup;
        document.onmousemove = focusPopup;
    }

    function parent_disable() {
        if (popupWindow && !popupWindow.closed)
            popupWindow.focus();
    }
</script>
<script>
    function check(e, value) {
        //Check Charater
        var unicode = e.charCode ? e.charCode : e.keyCode;
        if (value.indexOf(".") != -1)if (unicode == 46)return false;
        if (unicode != 8)if ((unicode < 48 || unicode > 57) && unicode != 46)return false;
    }
    function removephne(id) {
        if (confirm("Do you really wish to delete this record?")) {
            var visitid = $('#the-visit-id').val();
            var type = 'contact';
            var dataString = 'id=' + id +
                '&visitid=' + visitid +
                '&type=' + type +
                '&thisTable=visit_details';
            $.ajax({
                type: "POST",
                url: "ajax_delete.php",
                data: dataString,
                success: function (msg) {
                    if (msg == 'true') {
                        $("div[data-id='" + id + "']").css('display', 'none');
                    } else {
                        alert('At least one row of Contact Details is required');
                    }
                }
            });
        }
    }


    setInterval(function () {
        if (document.getElementById('alertComment')) {
            var alert = document.getElementById('alertComment');
            if (alert.style.color == 'rgb(132, 147, 168)') {
                alert.style.color = '#fff';
            } else {
                alert.style.color = 'rgb(132, 147, 168)';
            }
        }
    }, 600);

</script>
<?php
$sqlid = $dbh->prepare("select Id,firstname,lastname,DATE_FORMAT(Addeddate,'%b-%d-%Y') Addeddate from collectors where Id='$regdate'");
$sqlid->execute();
$dataid = $sqlid->fetch();
$sqlid1 = $dbh->prepare("select * from visitstable where collectorsid='$a' order by `visitid` desc");
$sqlid1->execute();
$dataid1 = $sqlid1->fetch();
$d = $dataid1['visitid'];


if ($a != '') {
    $sql = $dbh->prepare("select * from collectors where Id={$a}");
    $sql->execute();
    $dataCollector = $sql->fetch();
    $regdate = $dataCollector['Id'];
} else {
    $sql = $dbh->prepare("SELECT * FROM collectors ORDER BY Id DESC");
    $sql->execute();
    $data = $sql->fetch();
    $regdate = $data['Id'];
}
?>
<section>
    <input autocorrect="off" type="hidden" id="page-name" value="<?= $pagename ?>"/>
    <div class="mainwrapper">
        <?php include 'includes/leftpanel.php'; ?>
        <div class="mainpanel">
            <div class="pageheader">
                <div class="media-body">
                    <div class="row">
                        <div class="col-sm-12" style="width: 100%;font-size: 20px;">
                            <?php
                            if ($visitid) {
                                $id = $visitid;
                            } elseif ($visit) {
                                $id = $_SESSION['visit'];
                            } else {
                                $id = false;
                            }

                            $sql = $dbh->prepare("SELECT * FROM `visitstable` WHERE `visitid` ='$id' AND `collectorsid` = '$memberid';");
                            $sql->execute();
                            $visitInfo = $sql->fetch();

                            if ($visitInfo) {
                                $refId = $_POST['refid'];
                                $regDate = $_POST['regdate'];
                                $sqlUpdate = $dbh->prepare("UPDATE `visitstable`
                                                            SET `authofromdate`='$authfrom', `authotodate`='$authto',
                                                                `regdate` = '$regDate'
                                                            WHERE `visitid` ='$id'
                                                            AND `collectorsid` = '$memberid';");
                                $sqlUpdate->execute();
                                $updatedInfo = $sql->fetch();
                            }
                            ?>
                            <h4 id="visitNumber">Add Visit #:
                                <?php if ($visitid) {
                                    echo $visitid;
                                } elseif ($visit) {
                                    echo $_SESSION['visit'];
                                } elseif ($dataid1['visitid']) {
                                    echo $dataid1['visitid'] + 1;
                                } else {
                                    echo '1';
                                }
                                ?>
                            </h4>
                        </div>
                    </div>
                    <div class="row">
                        <div id="duperr"
                             class="duperr"
                             style="padding: 10px; margin-left: 215px;">
                            <span id="duperrordispaly" style="visibility: hidden;">
                                THIS ID IS READY USED, THE INFORMATION WON`T BE SAVED, PLEASE VERIFY IT!!
                            </span>
                        </div>
                    </div>
                </div>
            </div><!-- pageheader -->
            <div class="media">
                <form enctype="multipart/form-data" method="post" id="valWizard" name="valWizard"
                      accept-charset="utf-8"
                      class="panel-wizard" novalidate="novalidate">
                    <input autocorrect="off" name="requiredFunds" id="requiredFunds" type="hidden" value="<?php
                    if (isset($purposeFundsRequired)) {
                        echo $purposeFundsRequired['value'];
                    } else {
                        echo "true";
                    }

                    ?>"/>
                    <input autocorrect="off" name="reference" id="requiredReference" type="hidden" value="<?php
                    if (isset($reference)) {
                        echo $reference['value'];
                    } else {
                        echo "true";
                    }

                    ?>"/>
                    <input autocorrect="off" name="visitval" id="visitval" type="hidden">
                    <input autocorrect="off" name="action" id="action" type="hidden">
                    <input autocorrect="off" name="act" id="act" type="hidden">
                    <input autocorrect="off" name="hid" id="hid" type="hidden">
                    <?php
                    if (($memberid != '') || ($visitid != '')) { ?>
                        <ul class="nav nav-justified nav-wizard nav-disabled-click nav-pills">
                            <li id="visitsTab" style="  float: left;"
                                class="<?php if (($visistprev == 'visistprev') || ($personalinfonext == '')) {
                                    echo 'active';
                                } ?>"><a href="#tab2-4" data-toggle="tab" style="font-size:14px">Visits</a></li>
                            <li id="collectorDetailsTab" style="  float: left;"
                                class="<?php if (($personalinfonext == '') && ($visistprev == '')) {
                                    echo 'active';
                                } ?>"><a href="#tab1-4" data-toggle="tab" style="font-size:14px">Personal Info</a>
                            </li>
                            <li id="addressTab" style="  float: left;"
                                class="<?php if (($personalinfonext == 'personalinfonext') || ($addressprev == 'addressprev')) {
                                    echo 'active';
                                } ?>"><a href="#tab3-4" data-toggle="tab" style="font-size:14px">Address</a></li>
                            <li id="fundsTab" style="  float: left;"
                                class="<?php if (($personalinfonext == '') && ($visistprev == '')) {
                                    echo 'active';
                                } ?>"><a href="#tab4-4" data-toggle="tab" style="font-size:14px">Funds</a></li>
                            <li id="purposeTab" style="  float: left;"
                                class="<?php if (($personalinfonext == '') && ($visistprev == '')) {
                                    echo 'active';
                                } ?>"><a href="#tab5-4" data-toggle="tab" style="font-size:14px">Purpose</a></li>
                            <li id="referenceTab" style="  float: left;"
                                class="<?php if (($personalinfonext == '') && ($visistprev == '')) {
                                    echo 'active';
                                } ?>"><a href="#tab6-4" data-toggle="tab" style="font-size:14px">Reference</a></li>
                            <li style="  float: left;"
                                class="<?php if (($personalinfonext == '') && ($visistprev == '')) {
                                    echo 'active';
                                } ?>"><a href="#tab7-4" data-toggle="tab" style="font-size:14px">Sign | Print</a>
                            </li>
                            <li style="  float: left;"
                                class="<?php if (($personalinfonext == '') && ($visistprev == '')) {
                                    echo 'active';
                                } ?>"><a href="#tab8-4" data-toggle="tab" style="font-size:14px">Visit History</a>
                            </li>
                        </ul>
                    <?php } elseif ($visit == '') { ?>
                        <ul id="menu1" class="nav nav-justified nav-wizard nav-disabled-click nav-pills">
                            <li id="visitsTab" style="  float: left;"
                                class="<?php
                                if (($visistprev == 'visistprev') || ($personalinfonext == '')) {
                                    echo 'active';
                                } ?>"><a href="#tab2-4" data-toggle="" style="font-size:14px">Visits</a></li>
                        </ul>
                    <?php } ?>

                    <div class="tab-content member">

                        <?php if ($dataCollector['personalcomments'] || $personalComments): ?>
                            <script>
                                $(document).ready(function () {
                                    $('.alert-block').css('display', 'block');
                                });
                            </script>
                        <?php endif; ?>

                        <div class="alert-block">
                            <div class="row font-size">
                                <span id="alertComment" class="alert-comment">&nbsp;ALERT&nbsp;</span>
                                - <?php if ($personalComments) {
                                    echo $personalComments;
                                } else {
                                    echo $dataCollector['personalcomments'];
                                } ?>
                            </div>
                        </div>

                        <div class="<?php if (($visistprev == 'visistprev') || ($personalinfonext == '')) {
                            echo 'tab-pane active';
                        } else {
                            echo 'tab-pane';
                        } ?>" id="tab2-4">

                            <div>
                                <input autocorrect="off" name="memberid" type="hidden" value="<?= $regdate; ?>">
                                <input autocorrect="off" name="todaydate" type="hidden" value="<?= date('M-d-Y'); ?>">
                                <input autocorrect="off" name="sitesmemid" id="sitesmemid" type="hidden"
                                       value="<?= $a; ?>">
                                <input autocorrect="off" name="sitesvisid" id="sitesvisid" type="hidden"
                                       value="<?php if ($visitid) {
                                           echo $visitid;
                                       } elseif ($visit) {
                                           echo $_SESSION['visit'];
                                       } elseif ($dataid1['visitid']) {
                                           echo $dataid1['visitid'] + 1;
                                       } else {
                                           echo '1';
                                       } ?>">
                                <?php
                                $sqlid = $dbh->prepare("
                                            SELECT Id,firstname,lastname,DATE_FORMAT(Addeddate,'%b-%d-%Y') Addeddate
                                            FROM collectors
                                            WHERE Id='$regdate'
                                        ");
                                $sqlid->execute();
                                $dataid = $sqlid->fetch();
                                ?>
                            </div>
                            <div class="first-block">
                                <?php if ($managerSiteError): ?>
                                    <br>
                                    <div class="alert alert-danger" role="alert">Contact System
                                        Administrator - No Site Available.
                                    </div>
                                <?php endif; ?>
                                <?php if ($administratorSiteError): ?>
                                    <br>
                                    <div class="alert alert-danger" role="alert">Assign a default site
                                        to this administrator or enter in a default system site.
                                    </div>
                                <?php endif; ?>
                                <div class="personal-info">
                                    <div class="result col-md-6" id="result">
                                        <label class="col-sm-3 control-label my-label"
                                            <?php
                                            if (findAssignment($required_fields, 'visits', 'visit')) {
                                                echo " style='color:#428bca;' ";
                                            }

                                            ?>
                                        >Visit #</label>
                                        <div class="col-sm-7">
                                            <input autocorrect="off" type="text" name="visit"
                                                   id="visitId"
                                                   onkeypress="Maxval(this.value,7,this.id)"
                                                   value="<?php if ($visitid) {
                                                       echo $visitid;
                                                   } elseif ($visit) {
                                                       echo $_SESSION['visit'];
                                                   } elseif ($dataid1['visitid']) {
                                                       echo $dataid1['visitid'] + 1;
                                                   } else {
                                                       echo '1';
                                                   } ?>"
                                                   class="form-control my-input"
                                                   required>
                                            <div id="countervisitId" class="my-input-margin"></div>
                                            <span id="visitIdErr"
                                                  style="color: red"></span>
                                        </div>
                                        <label class="col-sm-3 control-label my-label"
                                            <?php
                                            if (findAssignment($required_fields, 'visits', 'refid')) {
                                                echo " style='color:#428bca;' ";
                                            }

                                            ?>
                                        >Reference No
                                        </label>
                                        <div class="col-sm-7">
                                            <?php
                                            if ($valRefId[0] != 0 && $valRefId[0] != '' && $visitid != '') {
                                                $refid = $dataid1['Refid'];
                                            } elseif ($visit != '') {
                                                $sqlTest = $dbh->prepare('SELECT *
                                                                          FROM `visitstable`
                                                                          WHERE `collectorsid` = :id
                                                                          ORDER BY `visitid`
                                                                          DESC ; ');
                                                $sqlTest->execute(['id' => $a]);
                                                $test = $sqlTest->fetch();
                                                $refid = $test['Refid'];
                                            } else {
                                                $refid = $valRefId[0] + 1;

                                                for ($refid; $refid < 1000000; $refid++) {
                                                    $sqlTest = $dbh->prepare('SELECT *
                                                                          FROM `visitstable`
                                                                          WHERE `Refid` = :refId; ');
                                                    $sqlTest->execute(['refId' => $refid]);
                                                    $existingVisit = $sqlTest->fetch();

                                                    if (!isset($existingVisit['Id'])) {
                                                        break;
                                                    }
                                                }
                                            }
                                            ?>
                                            <input autocorrect="off" type="text" name="refid"
                                                   id="refid"
                                                   lang="<?= $refid ?>"
                                                   value="<?= $refid ?>"
                                                   onkeypress="Maxval(this.value,7,this.id); return check(event,value)"
                                                   class="form-control valid my-input"
                                            >
                                            <div id="counterrefid" class="my-input-margin"></div>
                                            <span class="ref-id-err" style="color: red;"></span>
                                        </div>

                                        <div <?php
                                        if (!$showSite) {
                                            echo " style='display:none;' ";
                                        }
                                        ?>>
                                            <label class="col-sm-3 control-label my-label"
                                                <?php
                                                if (findAssignment($required_fields, 'visits', 'sites')) {
                                                    echo " style='color:#428bca;' ";
                                                }

                                                ?>
                                            >Site</label>
                                            <div class="col-sm-7">
                                                <select required
                                                        onchange="myfun();"
                                                        name="sites"
                                                        id="sites"
                                                        class="form-control my-input"
                                                >
                                                    <option
                                                            value="<?php if ($option_default_site) {
                                                                echo $option_default_site;
                                                            } else {
                                                                echo 'Choose One';
                                                            } ?>"
                                                            selected><?php if ($option_default_site) {
                                                            echo $option_default_site;
                                                        } else {
                                                            echo 'Choose One';
                                                        } ?></option>
                                                </select>
                                            </div>
                                        </div>

                                        <?php
                                        if ($visitid != '') {
                                            $visId = $visitid;
                                        } elseif ($visit != '') {
                                            $visId = $_SESSION['visit'];
                                        } elseif ($dataid1['visitid'] != '') {
                                            $visId = $dataid1['visitid'] + 1;
                                        } else {
                                            $visId = '1';
                                        }

                                        $sql = $dbh->prepare('SELECT *
                                                              FROM `visitstable`
                                                              WHERE `collectorsid` = :collid
                                                              AND `visitid` = :visitid ;');
                                        $sql->execute([
                                            'collid'  => $a,
                                            'visitid' => $visId - 1
                                        ]);
                                        $lastVisitData = $sql->fetch();

                                        $sql = $dbh->prepare('SELECT *
                                                              FROM `visitstable`
                                                              WHERE `visitid` = :visitid
                                                              AND `collectorsid` = :collid ;');
                                        $sql->execute([
                                            'collid'  => $a,
                                            'visitid' => $visId
                                        ]);
                                        $data11 = $sql->fetch();
                                        ?>

                                        <label
                                                class="col-sm-3 control-label my-label"
                                            <?php
                                            if (findAssignment($required_fields, 'visits', 'regdate')) {
                                                echo " style='color:#428bca;' ";
                                            }

                                            ?>
                                        >Registration Date</label>
                                        <div class="col-sm-7">
                                            <input autocorrect="off" type="text"
                                                   readonly
                                                   id="txtdate3"
                                                   value="<?php if ($data11['regdate']) {
                                                       echo $data11['regdate'];
                                                   } ?>"
                                                   name="regdate"
                                                   class="form-control my-input">
                                        </div>

                                        <label
                                                class="col-sm-3 control-label my-label"
                                            <?php
                                            if (findAssignment($required_fields, 'visits', 'authfrom')) {
                                                echo " style='color:#428bca;' ";
                                            }

                                            ?>
                                        >Authorized Period From</label>
                                        <div class="col-sm-7">
                                            <input autocorrect="off" id="txtdate"
                                                   onclick="mysite()"
                                                   readonly
                                                   onchange="myfunval()"
                                                   name="authfrom"
                                                   class="form-control my-input"
                                                   value="<?php if ($data11['authofromdate']) {
                                                       echo $data11['authofromdate'];
                                                   } ?>"
                                                   type="text">
                                            <div id="dateerrormsg1" class="dateerrormsg1"
                                                 style="color: #FF0808;font-size: 12px;"></div>
                                        </div>
                                        <label class="col-sm-3 control-label my-label"
                                            <?php
                                            if (findAssignment($required_fields, 'visits', 'authto')) {
                                                echo " style='color:#428bca;' ";
                                            }

                                            ?>
                                        >Authorized Period To</label>
                                        <div class="col-sm-7"
                                             id="todate"
                                        >
                                            <input autocorrect="off" type="text"
                                                   readonly
                                                   value="<?= $data11['authotodate']; ?>"
                                                   name="authto"
                                                   class="form-control my-input"
                                            >
                                        </div>

                                        <label class="col-sm-3 control-label my-label">Last
                                            Visit Date</label>
                                        <div class="col-sm-7">
                                            <div>
                                                <input autocorrect="off" type="text" readonly
                                                       id="last-visit-date"
                                                       value="<?php
                                                       if ($data11['free_formed_last_visit_date']) {
                                                           echo $data11['free_formed_last_visit_date'];
                                                       } elseif ($lastVisitData['authofromdate']) {
                                                           echo $lastVisitData['authofromdate'];
                                                       } ?>"
                                                    <?php if ((!$data11['free_formed_last_visit_date']
                                                            && !$lastVisitData['authofromdate']
                                                            && $data11['visitid'] == 1)
                                                        || !$data11['visitid']
                                                    ): ?>
                                                        placeholder="First Visit"
                                                    <?php endif; ?>
                                                       name="free_formed_last_visit_date"
                                                       class="form-control my-input"
                                                >
                                            </div>
                                        </div>

                                        <label class="col-sm-3 control-label my-label">Last
                                            Visit Number
                                        </label>
                                        <div class="col-sm-7">
                                            <input autocorrect="off" type="text"
                                                   placeholder="<?php
                                                   if (!$data11['free_formed_last_visit_number']
                                                       && !$lastVisitData['Refid']
                                                       && ($data11['visitid'] < 2)
                                                   ) {
                                                       echo 'First Visit';
                                                   }
                                                   ?>"
                                                   value="<?php
                                                   if ($data11['free_formed_last_visit_number']) {
                                                       echo $data11['free_formed_last_visit_number'];
                                                   } elseif ($lastVisitData['Refid']) {
                                                       echo $lastVisitData['Refid'];
                                                   }
                                                   ?>"
                                                   lang="<?php
                                                   if (!$lastVisitData['Refid']) {
                                                       echo 'First Visit';
                                                   } else {
                                                       echo $lastVisitData['Refid'];
                                                   }
                                                   ?>"
                                                   name="free_formed_last_visit_number"
                                                   class="form-control my-input"
                                            >
                                            <span class="ref-id-err"
                                                  style="color: red;"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-6" id="sites1">
                                        <?php
                                        $sql2 = $dbh->prepare("SELECT * FROM `sites` WHERE `Sites` ='$option_default_site' AND `Delete` = 0;");
                                        $sql2->execute();
                                        $data2 = $sql2->fetch();

                                        ?>

                                        <label class="col-sm-3 control-label my-label">Visits
                                            days
                                        </label>
                                        <div class="col-sm-7">
                                            <input autocorrect="off" name="visitdays" id="visitdays"
                                                   class="form-control my-input" type="text"
                                                   value="<?= $data2['visitsday']; ?>"
                                                   readonly>
                                        </div>

                                        <label class="col-sm-3 control-label my-label"
                                            <?php
                                            if (findAssignment($required_fields, 'visits', 'visitmanager')) {
                                                echo " style='color:#428bca;' ";
                                            }

                                            ?>
                                        >Manager
                                        </label>
                                        <div class="col-sm-7">
                                            <?php
                                            $sql = $dbh->prepare("SELECT * FROM `newmember` WHERE `emailid` ='" . $_SESSION['user'] . "' && `activity` ='1' && `Delete` ='0'");
                                            $sql->execute();
                                            $data = $sql->fetch();

                                            $sql = $dbh->prepare("SELECT * FROM `sites` WHERE `Sites` = '$option_default_site' AND `active` = 1 AND `Delete` = 0 ;");
                                            $sql->execute();
                                            $siteData = $sql->fetch();
                                            $siteId = $siteData['Id'];

                                            $sql = $dbh->prepare("SELECT * FROM `newmember` WHERE `Sites` = '$siteId' AND `Delete` = 0 AND `Status` = 'Access' AND `roles` = 'Manager';");
                                            $sql->execute();
                                            $managers = $sql->fetchAll();

                                            if ($data['roles'] == 'Manager') {
                                                ?>

                                                <select name="rabbis"
                                                        id="visitmanager"
                                                        class="form-control my-input">
                                                    <?php if ($data11['rabbis']): ?>

                                                        <option
                                                                selected><?= $data11['rabbis'] ?></option>
                                                        <?php if ($data11['rabbis'] != $data['Firstname'] . ' ' . $data['Lastname']): ?>
                                                            <option><?= $data['Firstname'] . ' ' . $data['Lastname']; ?></option>
                                                        <?php endif; ?>
                                                        <?php foreach ($managers as $manager): ?>
                                                            <?php if (($data['Firstname'] . ' ' . $data['Lastname'] != $manager['Firstname'] . ' ' . $manager['Lastname']) && $dataid1['rabbis'] != $manager['Firstname'] . ' ' . $manager['Lastname']): ?>
                                                                <option><?= $manager['Firstname'] . ' ' . $manager['Lastname'] ?></option>
                                                            <?php endif; ?>
                                                        <?php endforeach; ?>

                                                    <?php else: ?>

                                                        <option
                                                                selected><?= $data['Firstname'] . ' ' . $data['Lastname']; ?></option>
                                                        <?php foreach ($managers as $manager): ?>
                                                            <?php if (($data['Firstname'] . ' ' . $data['Lastname'] != $manager['Firstname'] . ' ' . $manager['Lastname']) && $dataid1['rabbis'] != $manager['Firstname'] . ' ' . $manager['Lastname']): ?>
                                                                <option><?= $manager['Firstname'] . ' ' . $manager['Lastname'] ?></option>
                                                            <?php endif; ?>
                                                        <?php endforeach; ?>

                                                    <?php endif; ?>
                                                </select>
                                            <?php } else {
                                                ?>
                                                <select name="rabbis"
                                                        id="visitmanager"
                                                        class="form-control my-input">
                                                    <?php if ($data11['rabbis']): ?>
                                                        <option
                                                                selected><?= $data11['rabbis']; ?></option>
                                                        <?php foreach ($managers as $manager): ?>
                                                            <?php if ($data11['rabbis'] != $manager['Firstname'] . ' ' . $manager['Lastname']): ?>
                                                                <option><?= $manager['Firstname'] . ' ' . $manager['Lastname']; ?></option>
                                                            <?php endif; ?>
                                                        <?php endforeach; ?>
                                                    <?php else: ?>
                                                        <option selected disabled>Choose
                                                            One
                                                        </option>
                                                        <?php foreach ($managers as $manager): ?>
                                                            <option><?= $manager['Firstname'] . ' ' . $manager['Lastname']; ?></option>
                                                        <?php endforeach; ?>
                                                    <?php endif; ?>
                                                </select>
                                            <?php } ?>
                                        </div>

                                        <label class="col-sm-3 control-label my-label"
                                            <?php
                                            if (findAssignment($required_fields, 'visits', 'visithost')) {
                                                echo " style='color:#428bca;' ";
                                            }

                                            ?>
                                        >Host</label>
                                        <div class="col-sm-7">
                                            <?php
                                            $sql1 = $dbh->prepare("select * from `hosts` where `Sites` = '$siteId' && Active ='1' && `Delete` ='0'");
                                            $sql1->execute();
                                            $hostsData = $sql1->fetchAll();
                                            ?>
                                            <select name="visit-host"
                                                    id="visithost"
                                                    class="form-control my-input">
                                                <?php if ($data11['host']): ?>
                                                    <option selected><?= $data11['host']; ?></option>

                                                    <?php foreach ($hostsData as $host): ?>
                                                        <?php if ($data11['host'] != $host['Firstname'] . ' ' . $host['Lastname']): ?>
                                                            <option><?= $host['Firstname'] . ' ' . $host['Lastname']; ?></option>
                                                        <?php endif; ?>
                                                    <?php endforeach; ?>

                                                    <option value="0">Choose One</option>
                                                <?php else: ?>
                                                    <option value="0" selected>Choose One</option>
                                                    <?php foreach ($hostsData as $host): ?>
                                                        <option><?= $host['Firstname'] . ' ' . $host['Lastname'] ?></option>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </select>

                                            <!--Free formed host name-->
                                            <input autocorrect="off"
                                                   class="form-control text-capitalize my-input"
                                                   name="free_formed_host1"
                                                   value="<?php if ($data11['free_formed_host']) {
                                                       echo $data11['free_formed_host'];
                                                   } ?>"
                                            />

                                        </div>

                                        <label class="col-sm-3 control-label my-label"
                                            <?php
                                            if (findAssignment($required_fields, 'visits', 'visitdriver')) {
                                                echo " style='color:#428bca;' ";
                                            }
                                            ?>
                                        >Driver</label>
                                        <div class="col-sm-7">
                                            <?php
                                            $sql1 = $dbh->prepare("select * from drivers where Sites ='$siteId' && Active ='1' && `Delete` ='0'");
                                            $sql1->execute();
                                            $data1 = $sql1->fetchAll();
                                            ?>

                                            <select name="driver"
                                                    id="visitdriver"
                                                    class="form-control my-input">
                                                <?php if ($data11['driver']): ?>
                                                    <option><?= $data11['driver'] ?></option>
                                                    <?php if ($data11['driver'] != 'Self-Drive'): ?>
                                                        <option>Self-Drive</option>
                                                    <?php endif; ?>

                                                    <?php foreach ($data1 as $driver): ?>
                                                        <?php if ($data11['driver'] != $driver['Firstname'] . ' ' . $driver['Lastname']): ?>
                                                            <option><?= $driver['Firstname'] . ' ' . $driver['Lastname'] ?></option>
                                                        <?php endif; ?>
                                                    <?php endforeach; ?>

                                                    <option value="0">Choose One</option>
                                                <?php else: ?>
                                                    <option value="0">Choose One</option>
                                                    <option>Self-Drive</option>
                                                    <?php foreach ($data1 as $driver): ?>
                                                        <option><?= $driver['Firstname'] . ' ' . $driver['Lastname'] ?></option>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </select>

                                            <!--Free formed driver name-->
                                            <input autocorrect="off"
                                                   class="form-control text-capitalize my-input"
                                                   name="free_formed_driver"
                                                   value="<?php if ($data11['free_formed_driver']) {
                                                       echo $data11['free_formed_driver'];
                                                   } ?>"/>
                                            <!--Free formed driver comment-->
                                            <textarea autocorrect="off" class="form-control my-input"
                                                      name="free_formed_driver_comment"
                                                      rows="3"
                                            ><?php if ($data11['free_formed_driver_comment']) {
                                                    echo $data11['free_formed_driver_comment'];
                                                } else {
                                                    echo '';
                                                } ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="my-footer">
                                <div id="buttons">
                                    <button class="btn btn-primary mr5" id="next1"
                                            onclick="mysite();"
                                            name="next1" type="submit"> Save
                                    </button>
                                    <button id="cancel"
                                            style=" opacity: 0;"></button>
                                    <a href="emvs.php?action=collector"
                                       class="btn btn-primary mr5">Cancel</a>

                                </div>
                            </div>

                        </div><!-- tab-pane -->
                        <!-- tab Personal Info-->
                        <div class="<?php if (($personalinfonext == '') &&
                            ($visistprev == '')
                        ) {
                            echo 'tab-pane active';
                        } else {
                            echo 'tab-pane';
                        } ?>" id="tab1-4">
                            <div class="first-block">
                                <input autocorrect="off" name="memberids" type="hidden" value="<?php if ($a != '') {
                                    echo $a;
                                } else {
                                    if ($memberid != '') {
                                        echo $memberid;
                                    } else {
                                        echo $memberid1 + 1;
                                    }
                                } ?>">
                                <input autocorrect="off" name="visitid" type="hidden"
                                       value="<?= $_SESSION['visit']; ?>">

                                <div class="panel-body">

                                    <div class="row">
                                        <div class="block-name pers-details mb30">
                                            Personal <span style="font-weight: 400">Details</span>
                                        </div>
                                    </div>

                                    <?php

                                    $sql = $dbh->prepare("
											SELECT
											COUNT(*) AS `count`
											FROM
											visitstable
											WHERE collectorsid = " . $_POST['memberid'] . "
										");
                                    $sql->execute();
                                    $pretest = $sql->fetch();
                                    if ($pretest['count'] > 0) {

                                        $sql = $dbh->prepare("
											SELECT
											*
											FROM
											visitstable
											WHERE collectorsid = " . $_POST['memberid'] . " AND visitid =
												(
												SELECT MAX(visitid) FROM visitstable WHERE  collectorsid = " . $_POST['memberid'] . "
												)
										");
                                        $sql->execute();
                                        $thisVisit = $sql->fetch();
                                    }
                                    ?>
                                    <div id="visits">

                                        <div class="col-sm-6">
                                            <div class="personal-details personal-label my-label"
                                                <?php
                                                if (findAssignment($required_fields, 'collectorDetails', 'title')) {
                                                    echo " style='color:#428bca;' ";
                                                }

                                                ?>
                                            >
                                                Title
                                            </div>
                                            <div class="personal-details">
                                                <input autocorrect="off" name="title1"
                                                       type="hidden"
                                                       value="<?= $data11['title']; ?>">
                                                <select tabindex="1"
                                                        name="title"
                                                        id="title"
                                                        class="form-control personal-input">
                                                    <?php if ($data11['title']): ?>
                                                        <option>
                                                            <?= $data11['title']; ?>
                                                        </option>
                                                    <?php else: ?>
                                                        <option selected disabled>
                                                            Choose One
                                                        </option>
                                                    <?php endif; ?>

                                                    <?php
                                                    $sql1 = $dbh->prepare("SELECT *
                                                                           FROM `title`
                                                                           WHERE `Delete` = 0
                                                                           AND `Delete` = 0
                                                                           ORDER BY `Id`
                                                                           DESC");
                                                    $sql1->execute();
                                                    while ($data1 = $sql1->fetch()) { ?>
                                                        <?php if ($data11['title'] != $data1['title']): ?>
                                                            <option value="<?= $data1['title'] ?>">
                                                                <?= $data1['title']; ?>
                                                            </option>
                                                        <?php endif; ?>
                                                    <?php } ?>
                                                </select>
                                            </div>

                                            <div class="personal-details personal-label my-label"
                                                <?php
                                                if (findAssignment($required_fields, 'collectorDetails', 'firstname')) {
                                                    echo " style='color:#428bca;' ";
                                                }

                                                ?>
                                            >
                                                First Name
                                            </div>
                                            <div class="personal-details">
                                                <input autocorrect="off" type="text" tabindex="2"
                                                       onkeypress="Maxval(this.value,30,this.id)"
                                                       name="firstname" id="firstname"
                                                       value="<?= $data11['firstname']; ?>"
                                                       class="form-control personal-input text-capitalize">
                                                <div id="counterfirstname"></div>
                                            </div>

                                            <div class="personal-details personal-label my-label"
                                                <?php
                                                if (findAssignment($required_fields, 'collectorDetails', 'lastname')) {
                                                    echo " style='color:#428bca;' ";
                                                }

                                                ?>
                                            >
                                                Last Name
                                            </div>
                                            <div class="personal-details">
                                                <input autocorrect="off" type="text" tabindex="3"
                                                       onkeypress="Maxval(this.value,20,this.id)"
                                                       name="lastname"
                                                       id="lastname"
                                                       value="<?= $data11['lastname']; ?>"
                                                       class="form-control personal-input text-capitalize">
                                                <div id="counterlastname"></div>
                                            </div>

                                            <div class="personal-details personal-label my-label"
                                                <?php
                                                if (findAssignment($required_fields, 'collectorDetails', 'dob')) {
                                                    echo " style='color:#428bca;' ";
                                                }

                                                ?>
                                            >
                                                Date of Birth
                                            </div>
                                            <div class="personal-details">
                                                <div>
                                                    <?php
                                                    if (strlen($dataquery11['dob']) > 4) {
                                                        $onlyYear = false;
                                                        $parts = parserDate($dataquery11['dob']);
                                                        $month = $parts['month'];
                                                        $day = $parts['day'];
                                                        $year = $parts['year'];
                                                    } elseif (strlen($dataquery11['dob']) < 5 && $dataquery11['dob']) {
                                                        $onlyYear = true;
                                                    }

                                                    $months = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];

                                                    $days = 31;
                                                    switch ($month) {
                                                        case '2':
                                                            $days = 29;
                                                            break;
                                                        case '4':
                                                        case '6':
                                                        case '9':
                                                        case '11':
                                                            $days = 30;
                                                            break;
                                                        //Jan, Mar, May, Jul, Aug, Oct, Dec
                                                        default:
                                                            $days = 31;
                                                    }

                                                    $years = [];
                                                    for ($i = 1930; $i <= 2017; $i++) {
                                                        $years[] = $i;
                                                    }
                                                    ?>

                                                    <select tabindex="4" name="month" id="month" class="select-birth">
                                                        <option value="0"> Month</option>
                                                        <?php foreach ($months as $m): ?>
                                                            <option <?php if ($m == $month) {
                                                                echo 'selected';
                                                            } ?> ><?= $m; ?></option>
                                                        <?php endforeach; ?>
                                                    </select>

                                                    <select tabindex="5" name="day" id="day" class="select-birth">
                                                        <option value="0"> Day</option>
                                                        <?php for ($d = 1; $d <= $days; $d++): ?>
                                                            <option <?php if ($day == $d) {
                                                                echo 'selected';
                                                            } ?> ><?= $d; ?></option>
                                                        <?php endfor; ?>
                                                    </select>

                                                    <select tabindex="6" name="year" id="year" class="select-birth">
                                                        <option value="0"> Year</option>
                                                        <?php foreach ($years as $y): ?>
                                                            <option <?php
                                                            if (($y == $year) || ($y == $dataquery11['dob'])) {
                                                                echo 'selected';
                                                            } ?> ><?= $y; ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                    <button tabindex="7"
                                                            class="btn btn-primary hide-btn-bg birthDateSave"
                                                    >
                                                        Save
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="personal-details personal-label my-label align-top"
                                                <?php
                                                if (findAssignment($required_fields, 'collectorDetails',
                                                    'personalcomments')) {
                                                    echo " style='color:#428bca;' ";
                                                }

                                                ?>>Alert
                                            </div>
                                            <div class="personal-details">
                                                <div class="ckbox ckbox-primary">
                                                    <input autocorrect="off" name="alerts" id="alerts" type="checkbox"
                                                           value="1"
                                                           onClick="alertcomments(this.checked)"
                                                        <?php
                                                        if ($personalComments) {
                                                            echo 'checked';
                                                        } ?>>
                                                    <label class="my-ckbox box"
                                                           tabindex="7"
                                                           for="alerts"
                                                           style="position: absolute; ">
                                                    </label>
                                                    <textarea autocorrect="off" class="personal-input personal-comments"
                                                              tabindex="8"
                                                              style="<?php
                                                              if (!$personalComments) {
                                                                  echo 'display: none; ';
                                                              } ?>"
                                                              name="personalcomments"
                                                              id="personalcomments"
                                                              rows="5"
                                                              cols="97"><?= $personalComments; ?></textarea>

                                                    <a class="btn btn-primary btn-xs view-alert"
                                                       id="view-alert"
                                                       href="#personal-comments-div"
                                                       style="<?php
                                                       if (!$personalComments) {
                                                           echo 'display: none; ';
                                                       } ?>">
                                                        View
                                                    </a>

                                                    <pre id="personal-comments-div"
                                                         class="personal-comments-div white-popup-block mfp-hide">
                                                        <?= ($personalComments) ?>
                                                    </pre>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="personal-details personal-label my-label"
                                                <?php
                                                if (findAssignment($required_fields, 'collectorDetails', 'address1')) {
                                                    echo ' style="color:#428bca;" ';
                                                }

                                                ?>
                                            >
                                                Address 1
                                            </div>
                                            <div class="personal-details">
                                                <input autocorrect="off" type="text"
                                                       tabindex="8"
                                                       onkeypress="Maxval(this.value,35,this.id)"
                                                       name="address1"
                                                       id="address1"
                                                       value="<?= $data11['address1']; ?>"
                                                       class="form-control personal-input text-capitalize"
                                                >
                                                <div id="counteraddress1"></div>
                                            </div>

                                            <div class="personal-details personal-label my-label"
                                                <?php
                                                if (findAssignment($required_fields, 'collectorDetails', 'address2')) {
                                                    echo " style='color:#428bca;' ";
                                                }

                                                ?>>
                                                Address 2
                                            </div>
                                            <div class="personal-details">
                                                <input autocorrect="off" type="text" tabindex="9"
                                                       onkeypress="Maxval(this.value,50,this.id)"
                                                       name="address2" id="address2"
                                                       value="<?= $data11['address2']; ?>"
                                                       class="form-control personal-input text-capitalize"
                                                >
                                                <div id="counteraddress2"></div>
                                            </div>

                                            <div class="personal-details personal-label my-label"
                                                <?php
                                                if (findAssignment($required_fields, 'collectorDetails', 'city')) {
                                                    echo " style='color:#428bca;' ";
                                                }

                                                ?>
                                            >
                                                City

                                                <div class="city-buttons">
                                                    <div class="btn btn-primary btn-xs"
                                                         id="BB">BB
                                                    </div>
                                                    <div class="btn btn-primary btn-xs"
                                                         id="B">B
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="personal-details">
                                                <input autocorrect="off" type="text" tabindex="10"
                                                       onkeypress="Maxval(this.value,25,this.id); return onlyAlphabets(event,this)"
                                                       name="city"
                                                       id="city"
                                                       value="<?= $data11['city']; ?>"
                                                       class="form-control personal-input text-capitalize">
                                                <div id="countercity"></div>
                                            </div>

                                            <div class="personal-details personal-label my-label"
                                                <?php
                                                if (findAssignment($required_fields, 'collectorDetails', 'state')) {
                                                    echo " style='color:#428bca;' ";
                                                }

                                                ?>
                                            >
                                                State / Province
                                            </div>
                                            <div class="personal-details">
                                                <input autocorrect="off" type="text" tabindex="11"
                                                       onkeypress="Maxval(this.value,17,this.id)"
                                                       name="statecountry" id="state"
                                                       value="<?= $data11['statecountry']; ?>"
                                                       class="form-control personal-input text-capitalize">
                                                <div id="counterstate"></div>
                                            </div>

                                            <div class="personal-details personal-label my-label"
                                                <?php
                                                if (findAssignment($required_fields, 'collectorDetails', 'country')) {
                                                    echo " style='color:#428bca;' ";
                                                }

                                                ?>
                                            >
                                                Country
                                            </div>
                                            <div class="personal-details">
                                                <input autocorrect="off" type="text" tabindex="12"
                                                       onkeypress="Maxval(this.value,10,this.id)"
                                                       name="country" id="country"
                                                       class="form-control personal-input text-capitalize"
                                                       value="<?= $data11['country']; ?>">
                                                <div id="countercountry"></div>
                                            </div>

                                            <div class="personal-details personal-label my-label"
                                                <?php
                                                if (findAssignment($required_fields, 'collectorDetails', 'zipcode')) {
                                                    echo " style='color:#428bca;' ";
                                                }

                                                ?>
                                            >
                                                Zip / Postal Code
                                            </div>
                                            <div class="personal-details">
                                                <input autocorrect="off" type="text" tabindex="13"
                                                       onkeypress="Maxval(this.value,10,this.id); return onlyAlphabets(event,this)"
                                                       class="form-control personal-input text-capitalize"
                                                       name="zipcode"
                                                       id="zipcode"
                                                       value="<?php echo $data11['zipcode']; ?>">
                                                <div id="counterzipcode"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="second-block">
                                <div class="row">
                                    <div class="block-name panel-wizard">
                                        Identification <span style="font-weight: 400">Details</span>
                                    </div>
                                </div>

                                <div id="collident">
                                    <div class="row form-group my-label headers">
                                        <div class="col-sm-1 col-xs-1 field primary-ident-label">
                                            Primary
                                        </div>
                                        <div class="col-sm-1 col-xs-1 ident-type">
                                            Type
                                        </div>
                                        <div class="col-sm-2 col-xs-2 field ident-country-label">
                                            <div style="float: left; margin-right: 5%; ">Country</div>
                                            <div class="country-buttons">
                                                <button class="btn btn-primary btn-xs" type="button" id="ident_isr">ISR
                                                </button>
                                                <button class="btn btn-primary btn-xs" type="button" id="ident_usa">USA
                                                </button>
                                                <button class="btn btn-primary btn-xs" type="button" id="ident_uk">UK
                                                </button>
                                            </div>
                                        </div>
                                        <div class="col-sm-2 col-xs-2 field ident-number">
                                            Number
                                        </div>
                                        <div class="col-sm-2 col-xs-2 field" id="passport-name-label">
                                        </div>
                                    </div>
                                    <?php
                                    $collsql1 = $dbh->prepare('SELECT *
                                                               FROM `visit_details`
                                                               WHERE `visit_id` = :visitid
                                                               AND `detail_type` = :detailtype
                                                               AND `delete` = 0 ;');
                                    $collsql1->execute([
                                        'visitid'    => $data11['Id'],
                                        'detailtype' => 'identification'
                                    ]);

                                    $collidentCount = $collsql1->rowCount();
                                    while ($colldata1 = $collsql1->fetch()) {
                                        ?>
                                        <div class="row collident form-group"
                                             data-id="<?= $colldata1['id']; ?>">
                                            <div class="col-sm-1 col-xs-1 field new-primary-ident">
                                                <div class="select2-container"
                                                     style="width: 100%; border: none;">
                                                    <input autocorrect="off" name="Primary1"
                                                           value="<?= $colldata1['id']; ?>"
                                                           type="radio" <?php if ($colldata1['primary_number']) {
                                                        echo 'checked';
                                                    } ?>>
                                                </div>
                                            </div>
                                            <div class="col-sm-2 field" id="phone_number">
                                                <script src="js/jquery.maskedinput.js"
                                                        type="text/javascript"></script>
                                                <select name="type"
                                                        id="ident-type<?= $colldata1['id']; ?>"
                                                        class="form-control"
                                                        lang="<?= $colldata1['id']; ?>"
                                                        onchange="myssn(this.value,this.lang, this); ">
                                                    <option value="0">Choose One</option>
                                                    <option <?php
                                                    if ($colldata1['type'] == 'Passport') {
                                                        echo 'selected';
                                                    } ?>>
                                                        Passport
                                                    </option>
                                                    <option <?php
                                                    if ($colldata1['type'] == 'Teudat Zeut') {
                                                        echo 'selected';
                                                    } ?>>
                                                        Teudat Zeut
                                                    </option>
                                                    <option <?php
                                                    if ($colldata1['type'] == 'SSN / ID Number'
                                                        || $colldata1['type'] == 'SSN'
                                                    ) {
                                                        echo 'selected';
                                                    } ?>>
                                                        SSN / ID Number
                                                    </option>
                                                    <option <?php
                                                    if ($colldata1['type'] == 'National ID'
                                                        || $colldata1['type'] == 'NIN'
                                                    ) {
                                                        echo 'selected';
                                                    } ?>>
                                                        NIN
                                                    </option>
                                                    <option <?php
                                                    if ($colldata1['type'] == 'Driver License') {
                                                        echo 'selected';
                                                    } ?>>
                                                        Driver License
                                                    </option>
                                                    <option <?php
                                                    if ($colldata1['type'] == 'Green Card') {
                                                        echo 'selected';
                                                    } ?>>
                                                        Green Card
                                                    </option>
                                                    <option <?php
                                                    if ($colldata1['type'] == 'Number') {
                                                        echo 'selected';
                                                    } ?>>
                                                        Number
                                                    </option>
                                                </select>
                                            </div>
                                            <div id="myssn<?= $colldata1['id']; ?>">
                                                <div class="col-sm-2">
                                                    <input autocorrect="off" type="text"
                                                           name="country"
                                                           class="form-control"
                                                           id="identificationcountry<?php echo $colldata1['id']; ?>"
                                                           value="<?= $colldata1['country']; ?> "
                                                           onkeypress="Maxval(this.value,30,this.id); return onlyAlphabets(event,this)"
                                                           lang="<?= $colldata1['id']; ?>"
                                                           onfocus="identCountryListener(this.id);"
                                                           placeholder="Country"
                                                           style="text-transform:uppercase">
                                                    <div
                                                            id="counteridentificationcountry<?php echo $colldata1['id']; ?>"></div>
                                                </div>
                                                <div class="col-sm-2 field">
                                                    <input autocorrect="off" type="text" name="detail_number"
                                                           class="form-control"
                                                           id="phone<?php echo $colldata1['id']; ?>"
                                                           onkeypress="Maxval(this.value,20,this.id)"
                                                           value="<?= $colldata1['detail_number']; ?> "
                                                        <?php if ($colldata1['type'] == 'SSN / ID Number'): ?>
                                                            onkeydown="$(this).simpleMask( { 'mask': 'ssn', 'nextInput': false } );"
                                                        <?php endif; ?>
                                                    >
                                                    <div id="counterphone<?php echo $colldata1['id']; ?>"></div>
                                                </div>
                                                <?php if ($colldata1['type'] == 'Passport'): ?>
                                                    <div class="col-sm-2 field">
                                                        <input autocorrect="off" type="text" class="form-control"
                                                               onkeypress="Maxval(this.value,35,this.id); return onlyAlphabets(event,this)"
                                                               id="old-passport-name<?= $colldata1['id']; ?>"
                                                               name="passport_name"
                                                               placeholder="Passport Name"
                                                               value="<?php if ($colldata1['passport_name']) {
                                                                   echo $colldata1['passport_name'];
                                                               } ?>">
                                                        <div
                                                                id="counterold-passport-name<?php echo $colldata1['id']; ?>"></div>
                                                    </div>
                                                <?php endif; ?>
                                            </div>

                                            <div class="row col-sm-1 field">
                                                <a onclick="removeIdent(<?= $colldata1['id']; ?>)">
                                                    <img class="delete-img"
                                                         src="images/remove-icon.png"
                                                         border="0"
                                                         title="Remove"
                                                         alt="Remove"></a>
                                            </div>
                                        </div>

                                    <?php } ?>
                                    <?php include 'addidenti.php'; ?>
                                    <a href="javascript:void(0); " onclick="ChkvalId();"
                                       class="add-button"
                                       title="Add New Identification Details Item">
                                        <img src="images/add-icon.png" style="height:25px;"/>
                                    </a>
                                </div>
                            </div>
                            <div class="second-block" id="contact">
                                <div class="row">
                                    <div class="block-name panel-wizard">
                                        Contact <span style="font-weight: 400">Details</span>
                                    </div>
                                </div>
                                <div class="row my-label headers">
                                    <div class="col-sm-1 col-xs-1 primary-contact-label">
                                        Primary
                                    </div>
                                    <div class="col-sm-1 col-xs-1 phone-type-label" style="">
                                        Type
                                    </div>
                                    <div class="col-sm-2 col-xs-2" style="margin-right: 8.4%;">
                                        <div style="float: left; margin-right: 5%; ">Country</div>
                                        <div class="country-buttons">
                                            <button class="btn btn-primary btn-xs" type="button" id="contact_isr">
                                                ISR
                                            </button>
                                            <button class="btn btn-primary btn-xs" type="button" id="contact_usa">
                                                USA
                                            </button>
                                            <button class="btn btn-primary btn-xs" type="button" id="contact_uk">
                                                UK
                                            </button>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 col-xs-1 contact-number-label">
                                        Phone
                                    </div>

                                </div>

                                <?php
                                $sqlquery = $dbh->prepare('SELECT *
                                                               FROM `visit_details`
                                                               WHERE `visit_id` = :visitid
                                                               AND `detail_type` = :detailtype
                                                               AND `delete` = 0 ;');
                                $sqlquery->execute([
                                    'visitid'    => $data11['Id'],
                                    'detailtype' => 'contact'
                                ]);

                                while ($dataquery1 = $sqlquery->fetch()) {
                                    ?>
                                    <div class="row form-group" id="addinput"
                                         data-id="<?= $dataquery1['id']; ?>">
                                        <div class="row col-sm-1 field input-primary">
                                            <input autocorrect="off" name="CPrimary" value="<?= $dataquery1['id']; ?>"
                                                   type="radio"<?php if ($dataquery1['primary_number']) {
                                                echo 'checked';
                                            } ?>>
                                        </div>
                                        <label class="row col-sm-3 control-label" style="width: 150px;">

                                            <select class="form-control"
                                                    name="type"
                                                    onchange="contactCountryFocus(this);">
                                                <option value="<?php
                                                if ($dataquery1['type']) {
                                                    echo $dataquery1['type'];
                                                } else {
                                                    echo '';
                                                }
                                                ?>  "><?php
                                                    if ($dataquery1['type']) {
                                                        echo $dataquery1['type'];
                                                    } else {
                                                        echo 'Choose One';
                                                    }
                                                    ?>  </option>
                                                <option>Work</option>
                                                <option>Home</option>
                                                <option>Mobile</option>
                                                <option>Fax</option>
                                                <option>Other</option>
                                            </select>
                                        </label>
                                        <div class="col-sm-3"><?php $domain = $dataquery1['country']; ?>
                                            <input autocorrect="off" type="text" style="text-transform:uppercase"
                                                   class="form-control valid"
                                                   lang="<?= $dataquery1['id']; ?>"
                                                   onfocus="contactCountryListener(this.value, this.id, this.lang)"
                                                   onkeypress="Maxval(this.value,30,this.id); return onlyAlphabets(event,this)"
                                                   onchange="chkphonetypeselect(this.value,this.lang);"
                                                   id="countrycode<?php echo $dataquery1['id']; ?>"
                                                   name="country"
                                                   placeholder="Country"
                                                   value="<?php echo $domain; ?>">
                                            <div id="countercountrycode<?php echo $dataquery1['id']; ?>"></div>
                                        </div>
                                        <div class=" row col-sm-3 field">
                                            <input autocorrect="off" type="text" name="detail_number"
                                                   value="<?= $dataquery1['detail_number']; ?>"
                                                   id="phone<?= $dataquery1['id']; ?>" class="form-control"
                                                   placeholder="Number"
                                                   onkeypress="Maxval(this.value,20,this.id); return blockSpecialCharNum(event)"
                                            >
                                            <div id="counterphone<?= $dataquery1['id']; ?>"></div>
                                        </div>
                                        <div class="row col-sm-1 field" id='phone_number'>
                                            <a onclick="removephne(<?= $dataquery1['id']; ?>)">
                                                <img class="delete-img"
                                                     src="images/remove-icon.png" border="0"
                                                     data-toggle="tooltip" title="Remove Details"
                                                     alt="Remove"
                                                     data-original-title="Remove Details"></a>
                                        </div>
                                    </div>
                                <?php } ?>

                                <script type="text/javascript" src="js/jquery.SimpleMask.js"></script>
                                <?php include 'add_phone_wo_row.php'; ?>
                                <div class="row">
                                    <a href="javascript:void(0);" onclick="chkphoneval();"
                                       class="add-button"
                                       title="Add field">
                                        <img src="images/add-icon.png" style="height: 25px;"/><br/>
                                    </a>
                                </div>
                            </div>
                        </div><!-- tab-content member -->

                        <!-- tab Adress-->
                        <div
                                class="<?php if (($personalinfonext == 'personalinfonext') || ($addressprev == 'addressprev')) {
                                    echo 'tab-pane active';
                                } else {
                                    echo 'tab-pane';
                                } ?>" id="tab3-4">
                            <div class="panel panel-default">

                                <div class="panel-heading" style="padding:9px 0 2px 12px">
                                    <button class="btn btn-primary mr5"
                                            id="copyPersonalAddress"
                                            name="tempadd" type="button" style="margin-bottom:1%; ">Temporary US Address
                                    </button>
                                </div><!-- panel-heading -->
                                <div id="rresult" class="first-block rresult">
                                    <input autocorrect="off" name="memberid" id="memberid" type="hidden"
                                           value="<?= $regdate; ?>">
                                    <input autocorrect="off" name="visitid" id="visitid" type="hidden"
                                           value="<?= $_SESSION['visit']; ?>">
                                    <input autocorrect="off" name="the-visit-id" id="the-visit-id" type="hidden"
                                           value="<?= $data11['Id'] ?>">
                                    <?php
                                    $v = $_SESSION['visit'];

                                    $sql = $dbh->prepare("select * from visitstable where collectorsid ='$regdate' and visitid='$v'");
                                    $sql->execute();
                                    $data1 = $sql->fetch();
                                    ?>
                                    <div class="first-block">
                                        <div class="row">
                                            <div class="col-sm-1 my-label">
                                                Name
                                            </div>
                                            <div class="col-sm-4">
                                                <input autocorrect="off" type="text" tabindex="1"
                                                       onkeypress="Maxval(this.value,35,this.id)"
                                                       name="us_name"
                                                       id="usname"
                                                       value="<?php
                                                       echo $data1['us_name'];
                                                       ?>" class="form-control text-capitalize my-input">
                                                <div id="counterusname" class="my-input-margin"></div>
                                            </div>
                                            <div class="col-sm-1">
                                                &nbsp;
                                            </div>
                                            <div class="col-sm-1 my-label"
                                                <?php
                                                if (findAssignment($required_fields, 'address', 'uscountry')) {
                                                    echo " style='color:#428bca;' ";
                                                }

                                                ?>
                                            >
                                                Country
                                            </div>
                                            <div class="col-sm-4">
                                                <input autocorrect="off" type="text" tabindex="6"
                                                       onkeypress="Maxval(this.value,17,this.id); return onlyAlphabets(event,this)"
                                                       name="uscountry"
                                                       id="tempcountry"
                                                       value="<?php if ($dataquery != '') {
                                                           echo $dataquery['statecountry'];
                                                       } else {
                                                           echo $data1['uscountry'];
                                                       } ?>" class="form-control text-capitalize my-input">
                                                <div id="countertempcountry" class="my-input-margin"></div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-1 my-label"
                                                <?php
                                                if (findAssignment($required_fields, 'address', 'usaddress1')) {
                                                    echo " style='color:#428bca;' ";
                                                } ?>>
                                                Address
                                            </div>
                                            <div class="col-sm-4">
                                                <input autocorrect="off" type="text"
                                                       onkeypress="Maxval(this.value,50,this.id)"
                                                       tabindex="2"
                                                       name="usaddress1" id="tempaddress1"
                                                       value="<?php
                                                       echo $data1['usaddress1'];
                                                       ?>" class="form-control text-capitalize my-input" placeholder="">
                                                <div id="countertempaddress1" class="my-input-margin"></div>
                                            </div>
                                            <div class="col-sm-1">
                                                &nbsp;
                                            </div>
                                            <div class="col-sm-1 my-label"
                                                <?php
                                                if (findAssignment($required_fields, 'address', 'uszipcode')) {
                                                    echo " style='color:#428bca;' ";
                                                }

                                                ?>
                                            >
                                                Zip / Postal Code
                                            </div>
                                            <div class="col-sm-4">
                                                <input autocorrect="off" type="text"
                                                       onkeypress="Maxval(this.value,10,this.id); return onlyAlphabets(event,this)"
                                                       tabindex="7"
                                                       name="uszipcode" id="tempzipcode"
                                                       value="<?php if ($dataquery != '') {
                                                           echo $dataquery['zipcode'];
                                                       } else {
                                                           echo $data1['uszipcode'];
                                                       } ?>" class="form-control my-input">
                                                <div id="countertempzipcode" class="my-input-margin"></div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-1 my-label"
                                                <?php
                                                if (findAssignment($required_fields, 'address', 'zipcity')) {
                                                    echo " style='color:#428bca;' ";
                                                }

                                                ?>
                                            >
                                                City
                                            </div>
                                            <div class="col-sm-4">
                                                <input autocorrect="off" type="text"
                                                       onkeypress="Maxval(this.value,25,this.id); return onlyAlphabets(event,this)"
                                                       tabindex="3"
                                                       name="zipcity" id="tempcity"
                                                       value="<?php if ($dataquery != '') {
                                                           echo $dataquery['city'];
                                                       } else {
                                                           echo $data1['zipcity'];
                                                       } ?>" class="form-control text-capitalize my-input">
                                                <div id="countertempcity" class="my-input-margin"></div>
                                            </div>
                                            <div class="col-sm-1">
                                                &nbsp;
                                            </div>
                                            <div class="col-sm-1 my-label field" id="phone_number"
                                                <?php
                                                if (findAssignment($required_fields, 'address', 'zipphone')) {
                                                    echo " style='color:#428bca;' ";
                                                }

                                                ?>
                                            >
                                                Phone
                                            </div>
                                            <div class="col-sm-4">
                                                <input autocorrect="off" type="text"
                                                       onkeypress="Maxval(this.value,15,this.id)"
                                                       tabindex="8"
                                                       name="zipphone"
                                                       class="form-control my-input"
                                                       id="tempphone"
                                                       value="<?php if ($dataquery != '') {
                                                           echo $dataquery['phoneno'];
                                                       } else {
                                                           echo $data1['zipphone'];
                                                       } ?>">
                                                <div id="countertempphone" class="my-input-margin"></div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-1 my-label"
                                                <?php
                                                if (findAssignment($required_fields, 'address', 'zipstate')) {
                                                    echo " style='color:#428bca;' ";
                                                }

                                                ?>
                                            >
                                                State / Province
                                            </div>
                                            <div class="col-sm-4">
                                                <input autocorrect="off" type="text"
                                                       onkeypress="Maxval(this.value,17,this.id)"
                                                       tabindex="4"
                                                       name="zipstate"
                                                       id="tempstate"
                                                       value="<?php if ($dataquery != '') {
                                                           echo $dataquery['statecountry'];
                                                       } else {
                                                           echo $data1['zipstate'];
                                                       } ?>"
                                                       class="form-control text-capitalize my-input">
                                                <div id="countertempstate" class="my-input-margin"></div>
                                            </div>

                                        </div>
                                        <div class="row">
                                            <div class="col-sm-1 my-label"
                                                <?php
                                                if (findAssignment($required_fields, 'address', 'zipcomments')) {
                                                    echo " style='color:#428bca;' ";
                                                }

                                                ?>
                                            >
                                                Comments
                                            </div>
                                            <div class="col-sm-10">
                                            <textarea autocorrect="off" class="form-control textarea-comment"
                                                      tabindex="5" rows="4"
                                                      cols="10"
                                                      placeholder="" name="zipcomments"
                                                      id="tempcomments"><?php echo $data1['zipcomments']; ?></textarea>
                                                <div id="countertempcomments" class="my-input-margin"></div>

                                            </div>
                                        </div>
                                    </div><!-- panel-body -->
                                </div>
                            </div><!-- panel -->
                            <div class="col-md-12" style="padding: 0px;">

                                <?php $regdate;
                                $v = $_SESSION['visit'];
                                $sql = $dbh->prepare("select * from visitstable where collectorsid ='$regdate' and visitid='$v'");
                                $sql->execute();
                                $data1 = $sql->fetch();
                                $ad = $data1['address1'];
                                $sitesql = $data1['sites'];

                                $managername = $data1['rabbis'];
                                $msql = $dbh->prepare("select * from newmember where Firstname ='$managername' && activity ='1' && `Delete` ='0'");
                                $msql->execute();
                                $mdata = $msql->fetch();
                                $sqlsites = $mdata['Sites'];


                                $sql3 = $dbh->prepare("select * from sites where Id ='$sitesql'");
                                $sql3->execute();
                                $data13 = $sql3->fetch();
                                ?>

                                <div class="panel panel-default">
                                    <div class="panel-heading" style="padding:9px 0 2px 12px">
                                        <!-- REVIEW IT LATER -->
                                        <button class="btn btn-primary mr5"
                                                id="copyUsAddress"
                                                name="localadd" type="button" style="margin-bottom:1%; ">
                                            <span><?= $data13['Sites']; ?></span>
                                            Temporary Baltimore Address
                                        </button>
                                    </div>
                                    <div id="localaddress" class="first-block localaddress">
                                        <div class="first-block">
                                            <div class="row">
                                                <div class="col-sm-1 my-label"
                                                    <?php
                                                    if (findAssignment($required_fields, 'address', 'baltiaddress1')) {
                                                        echo " style='color:#428bca;' ";
                                                    }

                                                    ?>
                                                >
                                                    Name
                                                </div>
                                                <div class="col-sm-4">
                                                    <input autocorrect="off" type="text"
                                                           tabindex="9" name="free_formed_host"
                                                           id="free-formed-host"
                                                           class="form-control text-capitalize my-input"
                                                           onkeypress="Maxval(this.value,35,this.id)"
                                                           value="<?php
                                                           if ($data11['host']) {
                                                               echo $data11['host'];
                                                           } else {
                                                               echo $data11['free_formed_host'];
                                                           }
                                                           ?>">
                                                    <div id="counterfree-formed-host" class="my-input-margin"></div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-1 my-label"
                                                    <?php
                                                    if (findAssignment($required_fields, 'address', 'baltiaddress2')) {
                                                        echo " style='color:#428bca;' ";
                                                    }

                                                    ?>
                                                >
                                                    Address
                                                </div>
                                                <div class="col-sm-4">
                                                    <input autocorrect="off" type="text"
                                                           onkeypress="Maxval(this.value,50,this.id)"
                                                           tabindex="10" name="baltiaddress1" id="baltiaddress1"
                                                           value="<?php
                                                           if ($data11['host']) {
                                                               $parts = explode(' ', $data11['host']);
                                                               $firstName = $parts[0];
                                                               $lastName = $parts[1];

                                                               $sql = $dbh->prepare('SELECT *
                                                                                         FROM `hosts`
                                                                                         WHERE `firstname` = :firstname
                                                                                         AND `lastname` = :lastname
                                                                                         AND `Delete` = 0 ;');
                                                               $sql->execute([
                                                                   'firstname' => $firstName,
                                                                   'lastname'  => $lastName
                                                               ]);

                                                               $hostData = $sql->fetch();

                                                               $sql = $dbh->prepare('SELECT *
                                                                                         FROM `hostphone`
                                                                                         WHERE `H_id` = :hostid
                                                                                         AND HPrimary != "" ;');
                                                               $sql->execute(['hostid' => $hostData['Id']]);
                                                               $hostPhoneData = $sql->fetch();
                                                           }
                                                           if ($hostData) {
                                                               echo $hostData['Address'];
                                                           } else {
                                                               echo $data1['baltiaddress1'];
                                                           }
                                                           ?>" class="form-control text-capitalize my-input"
                                                           placeholder="">
                                                    <div id="counterbaltiaddress1" class="my-input-margin"></div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-1 my-label field" id="phone_number"
                                                    <?php
                                                    if (findAssignment($required_fields, 'address', 'baltiphone')) {
                                                        echo " style='color:#428bca;' ";
                                                    }

                                                    ?>
                                                >
                                                    Phone
                                                </div>
                                                <div class="col-sm-4">
                                                    <input autocorrect="off" type="text"
                                                           onkeypress="Maxval(this.value,15,this.id)"
                                                           tabindex="16"
                                                           name="baltiphone"
                                                           class="form-control my-input"
                                                           id="baltiphone"
                                                           value="<?php if ($data11['host'] && $hostPhoneData) {
                                                               echo $hostPhoneData['Phone_Number'];
                                                           } else {
                                                               echo $data1['baltiphone'];
                                                           } ?>" required>
                                                    <div id="counterbaltiphone" class="my-input-margin"></div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-1 my-label"
                                                    <?php
                                                    if (findAssignment($required_fields, 'address', 'balticomments')) {
                                                        echo " style='color:#428bca;' ";
                                                    }

                                                    ?>
                                                >
                                                    Comments
                                                </div>
                                                <div class="col-sm-10">
                                                        <textarea autocorrect="off"
                                                                  class="form-control textarea-comment" tabindex="13"
                                                                  rows="4" cols="10"
                                                                  placeholder=""
                                                                  name="balticomments"><?php if ($dataquery1 != '') {
                                                                echo $dataquery1['zipcomments'];
                                                            } else {
                                                                echo $data1['balticomments'];
                                                            } ?></textarea>
                                                    <div id="counterbalticomments" class="my-input-margin"></div>

                                                </div>
                                            </div>
                                        </div><!-- panel-body -->
                                    </div>
                                </div><!-- panel -->
                            </div>
                        </div><!-- tab-pane -->

                        <!-- tab Funds-->
                        <div
                                class="<?php if (!$personalinfonext && !$visistprev) {
                                    echo 'tab-pane active';
                                } else {
                                    echo 'tab-pane';
                                } ?>" id="tab4-4">

                            <?php

                            $sql = $dbh->prepare("
											SELECT
											COUNT(*) AS `count`
											FROM
											visitstable
											WHERE collectorsid = " . $_POST['memberid'] . "
										");
                            $sql->execute();
                            $pretest = $sql->fetch();
                            if ($pretest['count'] > 0) {

                                $sql = $dbh->prepare("
											SELECT
											*
											FROM
											visitstable
											WHERE collectorsid = " . $_POST['memberid'] . " AND visitid =
												(
												SELECT MAX(visitid) FROM visitstable WHERE  collectorsid = " . $_POST['memberid'] . "
												)
										");
                                $sql->execute();
                                $dataV2 = $sql->fetch();
                            }

                            ?>

                            <div class="first-block">
                                <input autocorrect="off" name="memberid" type="hidden" value="<?= $regdate; ?>">
                                <input autocorrect="off" name="visitid" type="hidden"
                                       value="<?= $_SESSION['visit']; ?>">
                                <div>
                                    <div class="col-sm-5"><b> The Purpose of these funds is for</b></div>
                                    <div class="col-sm-2">
                                        <input autocorrect="off" type="radio" value="Institution"
                                               onclick="yesnoCheck(this.value, this);"
                                            <?php
                                            if (isset($dataV2)) {
                                                echo 'checked';
                                            }
                                            ?> name="funds" id="yesCheck">
                                        <label class="label-funds" for="yesCheck">Institution</label>
                                    </div>
                                    <div class="col-sm-2">
                                        <input autocorrect="off" type="radio" value="Individual"
                                               onclick="yesnoCheck(this.value, this);"<?php if ($data1['funds'] == 'Individual') {
                                            echo 'checked';
                                        } ?> name="funds" id="noCheck">
                                        <label class="label-funds" for="noCheck">Individual</label>
                                    </div>
                                    <br><!-- rdio -->
                                </div>
                                <div id="result1">
                                    <input autocorrect="off" name="bothval" type="hidden" value="<?= $bothval; ?>">

                                    <div class="panel-body"
                                        <?php if ($data1['funds'] == 'Individual'): ?>
                                            style="display: none;"
                                        <?php endif; ?>
                                    >
                                        <div class="row">
                                            <div class="col-sm-1 my-label"
                                                <?php
                                                if (findAssignment($required_fields, 'funds', 'institname')) {
                                                    echo " style='color:#428bca;' ";
                                                }

                                                ?>
                                            >
                                                Name
                                            </div>
                                            <div class="col-sm-10">
                                                <input autocorrect="off" type="text"
                                                       onkeypress="Maxval(this.value,50,this.id);"
                                                       tabindex="1" name="institname" id="institname"
                                                       value="<?php if (isset($dataV2)) {
                                                           echo trim($dataV2['institname'], "\t.");
                                                       }
                                                       ?>" class="form-control text-capitalize textarea-comment">
                                                <div id="counterinstitname" class="my-input-margin"></div>
                                            </div>

                                        </div>
                                        <div class="row">
                                            <div class="col-sm-1 my-label"
                                                <?php
                                                if (findAssignment($required_fields, 'funds', 'institaddress1')) {
                                                    echo " style='color:#428bca;' ";
                                                }

                                                ?>
                                            >
                                                Address 1
                                            </div>
                                            <div class="col-sm-4">
                                                <input autocorrect="off" type="text"
                                                       onkeypress="Maxval(this.value,50,this.id)"
                                                       tabindex="2" name="institaddress1"
                                                       id="institaddress1"
                                                       value="<?php
                                                       if (isset($dataV2)) {
                                                           echo $dataV2['institaddress1'];
                                                       } ?>" class="form-control text-capitalize my-input"
                                                       placeholder=""
                                                       required="">
                                                <div id="counterinstitaddress1" class="my-input-margin"></div>
                                            </div>
                                            <div class="col-sm-1">
                                                &nbsp;
                                            </div>
                                            <div class="col-sm-1 my-label"
                                                <?php
                                                if (findAssignment($required_fields, 'funds', 'institcountry')) {
                                                    echo " style='color:#428bca;' ";
                                                }

                                                ?>
                                            >
                                                Country
                                            </div>
                                            <div class="col-sm-4">
                                                <input autocorrect="off" type="text"
                                                       onkeypress="Maxval(this.value,17,this.id); return onlyAlphabets(event,this)"
                                                       tabindex="6"
                                                       name="institcountry" id="institcountry"
                                                       value="<?php
                                                       if (isset($dataV2['institcountry'])) {
                                                           echo $dataV2['institcountry'];
                                                       } ?>" class="form-control text-capitalize my-input">
                                                <div id="counterinstitcountry" class="my-input-margin"></div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-1 my-label"
                                                <?php
                                                if (findAssignment($required_fields, 'funds', 'institaddress2')) {
                                                    echo " style='color:#428bca;' ";
                                                }
                                                ?>>
                                                Address 2
                                            </div>
                                            <div class="col-sm-4">
                                                <input autocorrect="off" type="text"
                                                       onkeypress="Maxval(this.value,50,this.id)"
                                                       tabindex="2" name="institaddress2"
                                                       id="institaddress2" value="<?php
                                                if (isset($dataV2)) {
                                                    echo $dataV2['institaddress2'];
                                                } ?>" class="form-control text-capitalize my-input" placeholder="">
                                                <div id="counterinstitaddress2" class="my-input-margin"></div>
                                            </div>
                                            <div class="col-sm-1">
                                                &nbsp;
                                            </div>
                                            <div class="col-sm-1 my-label"
                                                <?php
                                                if (findAssignment($required_fields, 'funds', 'institzipcode')) {
                                                    echo " style='color:#428bca;' ";
                                                }
                                                ?>>
                                                Zip / Postal Code
                                            </div>
                                            <div class="col-sm-4">
                                                <input autocorrect="off" type="text" tabindex="7"
                                                       onkeypress="Maxval(this.value,10,this.id); return onlyAlphabets(event,this)"
                                                       name="institzip" id="institzipcode"
                                                       value="<?php if (isset($dataV2['institzip'])) {
                                                           echo $dataV2['institzip'];
                                                       }
                                                       ?>"
                                                       class="form-control my-input">
                                                <div id="counterinstitzipcode" class="my-input-margin"></div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-1 my-label"
                                                <?php
                                                if (findAssignment($required_fields, 'funds', 'institcity')) {
                                                    echo " style='color:#428bca;' ";
                                                }
                                                ?>>
                                                City

                                                <div class="instit-city-buttons">
                                                    <div class="btn btn-primary btn-xs"
                                                         id="BB">BB
                                                    </div>
                                                    <div class="btn btn-primary btn-xs"
                                                         id="B">B
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="col-sm-4">
                                                <input autocorrect="off" type="text"
                                                       onkeypress="Maxval(this.value,25,this.id); return onlyAlphabets(event,this)"
                                                       tabindex="3"
                                                       name="institcity"
                                                       id="institcity"
                                                       value="<?php if (isset($dataV2)) {
                                                           echo $dataV2['institcity'];
                                                       }
                                                       ?>" class="form-control text-capitalize my-input">
                                                <div id="counterinstitcity" class="my-input-margin"></div>
                                            </div>
                                            <div class="col-sm-1 my-label">
                                                &nbsp;
                                            </div>
                                            <div class="col-sm-1 my-label field" id="phone_number"
                                                <?php
                                                if (findAssignment($required_fields, 'funds', 'institphone')) {
                                                    echo " style='color:#428bca;' ";
                                                }
                                                ?>>
                                                Phone
                                            </div>
                                            <div class="col-sm-4">
                                                <input autocorrect="off" type="text" tabindex="8"
                                                       onkeypress="Maxval(this.value,10,this.id)"
                                                       name="institphone" class="form-control my-input"
                                                       id="institphone" value="<?php if (isset($dataV2)) {
                                                    echo $dataV2['institphone'];
                                                }
                                                ?>" class="form-control" placeholder="" required="">
                                                <div id="counterinstitphone" class="my-input-margin"></div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-1 my-label"
                                                <?php
                                                if (findAssignment($required_fields, 'funds', 'institstate')) {
                                                    echo " style='color:#428bca;' ";
                                                }
                                                ?>
                                            >
                                                State / Province
                                            </div>
                                            <div class="col-sm-4">
                                                <input autocorrect="off" type="text"
                                                       onkeypress="Maxval(this.value,17,this.id)"
                                                       tabindex="4" name="institstate" id="institstate"
                                                       value="<?php if (isset($dataV2)) {
                                                           echo $dataV2['institstate'];
                                                       }
                                                       ?>" class="form-control text-capitalize my-input">
                                                <div id="counterinstitstate" class="my-input-margin"></div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-1 my-label"
                                                <?php
                                                if (findAssignment($required_fields, 'funds', 'institcomments')) {
                                                    echo " style='color:#428bca;' ";
                                                }
                                                ?>>
                                                Comments
                                            </div>
                                            <div class="col-sm-10">
                                      <textarea autocorrect="off" class="form-control textarea-comment"
                                                id="institcomments"
                                                tabindex="5"
                                                rows="4"
                                                cols="10"
                                                name="institcomments"><?php
                                          echo $data11['institcomments'];
                                          ?></textarea>
                                                <div id="counterinstitcomments" class="my-input-margin"></div>
                                            </div>
                                        </div>
                                    </div><!--panel-body -->
                                </div>
                            </div><!-- block -->
                        </div><!-- tab-pane -->
                        <div style=""
                             class="<?php if (!$personalinfonext && !$visistprev) {
                                 echo 'tab-pane active';
                             } else {
                                 echo 'tab-pane';
                             } ?>" id="tab5-4">

                            <div class="first-block">
                                <div class="row">
                                    <div class="col-sm-4" <?php
                                    if (findAssignment($required_fields, 'purpose', 'active[]')) {
                                        echo " style='color:#428bca;' ";
                                    }
                                    ?>><b>Enter all that apply</b></div>
                                </div>
                                <input autocorrect="off" name="memberid" type="hidden" value="<?= $regdate; ?>">
                                <input autocorrect="off" name="visitid" type="hidden"
                                       value="<?= $_SESSION['visit']; ?>">
                                <div class="panel-body tab-purpose">
                                    <div class="row purpose-inner">
                                        <div class="purpose-item">
                                            <div class="ckbox ckbox-primary">
                                                <?php
                                                $purposedata = explode(",", $data1['purpose']);
                                                ?>
                                                <input autocorrect="off" type="checkbox" id="Yeshiva"
                                                       value="Yeshiva" <?php
                                                if (in_array('Yeshiva', $purposedata)
                                                    || preg_match('/Yeshiva/', $data1['purpose'])
                                                ) {
                                                    echo "checked";
                                                } ?> name="active[]" required="">
                                                <label class="my-label" for="Yeshiva">Yeshiva </label>
                                            </div>
                                        </div>
                                        <div class="purpose-item">
                                            <div class="ckbox ckbox-primary">
                                                <input autocorrect="off" type="checkbox" id="Kiruv"
                                                       value="Kiruv" <?php
                                                if (in_array("Kiruv", $purposedata)
                                                    || preg_match('/Kiruv/', $data1['purpose'])
                                                ) {
                                                    echo "checked";
                                                } ?> name="active[]" required="">
                                                <label class="my-label" for="Kiruv">Kiruv </label>
                                            </div>
                                        </div>
                                        <div class="purpose-item">
                                            <div class="ckbox ckbox-primary">
                                                <input autocorrect="off" type="checkbox" id="Free Loan"
                                                       value="Free Loan" <?php
                                                if (in_array("Free Loan", $purposedata)
                                                    || preg_match('/Free Loan/', $data1['purpose'])
                                                ) {
                                                    echo "checked";
                                                } ?> name="active[]" required="">
                                                <label class="my-label" for="Free Loan">Free Loan </label>
                                            </div>
                                        </div>
                                        <div class="purpose-item">
                                            <div class="ckbox ckbox-primary">
                                                <input autocorrect="off" type="checkbox" id="Hanchnosas"
                                                       value="Hanchnosas" <?php
                                                if (in_array("Hanchnosas", $purposedata)
                                                    || preg_match('/Hanchosas/', $data1['purpose'])
                                                ) {
                                                    echo "checked";
                                                } ?> name="active[]" required="">
                                                <label class="my-label" for="Hanchnosas">Hachnosas<br/>Kallah</label>
                                            </div>
                                        </div>
                                        <div class="purpose-item">
                                            <div class="ckbox ckbox-primary">
                                                <input autocorrect="off" type="checkbox" id="Medical"
                                                       value="Medical" <?php
                                                if (in_array("Medical", $purposedata)
                                                    || preg_match('/Medical/', $data1['purpose'])
                                                ) {
                                                    echo "checked";
                                                } ?> name="active[]" required="">
                                                <label class="my-label" for="Medical">Medical</label>
                                            </div>
                                        </div>
                                        <div class="purpose-item">
                                            <div class="ckbox ckbox-primary">
                                                <input autocorrect="off" type="checkbox" id="Mental"
                                                       value="Mental"<?php
                                                if (in_array("Mental", $purposedata)
                                                    || preg_match('/Mental/', $data1['purpose'])
                                                ) {
                                                    echo "checked";
                                                } ?> name="active[]" required="">
                                                <label class="my-label" for="Mental">Mental </label>
                                            </div>
                                        </div>
                                        <div class="purpose-item">
                                            <div class="ckbox ckbox-primary">
                                                <input autocorrect="off" type="checkbox" id="Torah"
                                                       value="Torah"<?php
                                                if (in_array("Torah", $purposedata)
                                                    || preg_match('/Torah/', $data1['purpose'])
                                                ) {
                                                    echo "checked";
                                                } ?> name="active[]" required="">
                                                <label class="my-label" for="Torah">Torah<br/>Education </label>
                                            </div>
                                        </div>
                                        <div class="purpose-item">
                                            <div class="ckbox ckbox-primary">
                                                <input autocorrect="off" type="checkbox" id="Kollel"
                                                       value="Kollel"<?php
                                                if (in_array("Kollel", $purposedata)
                                                    || preg_match('/Kollel/', $data1['purpose'])
                                                ) {
                                                    echo "checked";
                                                } ?> name="active[]" required="">
                                                <label class="my-label" for="Kollel">Kollel </label>
                                            </div>
                                        </div>
                                        <div class="purpose-item">
                                            <div class="ckbox ckbox-primary">
                                                <input autocorrect="off" type="checkbox" id="Charity"
                                                       value="Charity"<?php
                                                if (in_array("Charity", $purposedata)
                                                    || preg_match('/Charity/', $data1['purpose'])
                                                ) {
                                                    echo "checked";
                                                } ?> name="active[]" required="">
                                                <label class="my-label" for="Charity">Charity </label>
                                            </div>
                                        </div>
                                        <div class="purpose-item">
                                            <div class="ckbox ckbox-primary">
                                                <input autocorrect="off" type="checkbox" id="Business"
                                                       value="Business"<?php
                                                if (in_array("Business", $purposedata)
                                                    || preg_match('/Business/', $data1['purpose'])
                                                ) {
                                                    echo "checked";
                                                } ?> name="active[]" required="">
                                                <label class="my-label" for="Business">Business </label>
                                            </div>
                                        </div>
                                        <div class="purpose-item">
                                            <div class="ckbox ckbox-primary">
                                                <input autocorrect="off" type="checkbox" id="Parnosso"
                                                       value="Parnosso"<?php
                                                if (in_array("Parnosso", $purposedata)
                                                    || preg_match('/Parnosso/', $data1['purpose'])
                                                ) {
                                                    echo "checked";
                                                } ?> name="active[]" required="">
                                                <label class="my-label" for="Parnosso">Parnosso </label>
                                            </div>
                                        </div>
                                        <div class="purpose-item">
                                            <div class="ckbox ckbox-primary">
                                                <input autocorrect="off" type="checkbox" id="Debets"
                                                       value="Debets"<?php
                                                if (in_array("Debets", $purposedata)
                                                    || preg_match('/Debets/', $data1['purpose'])
                                                ) {
                                                    echo "checked";
                                                } ?> name="active[]" required="">
                                                <label class="my-label" for="Debets">Debts</label>
                                            </div>
                                        </div>
                                        <div class="purpose-item">
                                            <div class="ckbox ckbox-primary">
                                                <input autocorrect="off" type="checkbox" id="Girls"
                                                       value="Girls"<?php
                                                if (in_array("Girls", $purposedata)
                                                    || preg_match('/Girls/', $data1['purpose'])
                                                ) {
                                                    echo "checked";
                                                } ?> name="active[]" required="">
                                                <label class="my-label" for="Girls">Girls<br/>Education</label>
                                            </div>
                                        </div>
                                        <div class="purpose-item">
                                            <div class="ckbox ckbox-primary">
                                                <input autocorrect="off" type="checkbox" id="Sefer"
                                                       value="Sefer"<?php
                                                if (in_array("Sefer", $purposedata)
                                                    || preg_match('/Sefer/', $data1['purpose'])
                                                ) {
                                                    echo "checked";
                                                } ?> name="active[]">
                                                <label class="my-label" for="Sefer">Sefer </label>
                                            </div>
                                        </div>
                                        <div class="purpose-item">
                                            <div class="ckbox ckbox-primary">
                                                <input autocorrect="off" type="checkbox" id="Chesed"
                                                       value="Chesed"<?php
                                                if (in_array("Chesed", $purposedata)
                                                    || preg_match('/Chesed/', $data1['purpose'])
                                                ) {
                                                    echo "checked";
                                                } ?> name="active[]">
                                                <label class="my-label" for="Chesed">Chesed </label>
                                            </div>
                                        </div>
                                        <div class="purpose-item">
                                            <div class="ckbox ckbox-primary">
                                                <input autocorrect="off" type="checkbox" id="Judaism"
                                                       value="Judaism"<?php
                                                if (in_array("Judaism", $purposedata)
                                                    || preg_match('/Judaism/', $data1['purpose'])
                                                ) {
                                                    echo "checked";
                                                } ?> name="active[]">
                                                <label class="my-label" for="Judaism">Judaism </label>
                                            </div>
                                        </div>
                                        <div class="purpose-item">
                                            <div class="ckbox ckbox-primary">
                                                <input autocorrect="off" type="checkbox" id="Adult"
                                                       value="Adult"<?php
                                                if (in_array("Adult", $purposedata)
                                                    || preg_match('/Adult/', $data1['purpose'])
                                                ) {
                                                    echo "checked";
                                                } ?> name="active[]">
                                                <label class="my-label" for="Adult">Adult<br/>Education </label>
                                            </div>
                                        </div>
                                        <div class="purpose-item">
                                            <div class="ckbox ckbox-primary">
                                                <input autocorrect="off" type="checkbox" id="Chinuch"
                                                       value="Chinuch"<?php
                                                if (in_array("Chinuch", $purposedata)
                                                    || preg_match('/Chinuch/', $data1['purpose'])
                                                ) {
                                                    echo "checked";
                                                } ?> name="active[]">
                                                <label class="my-label" for="Chinuch">Chinuch</label>
                                            </div>
                                        </div>
                                        <div class="purpose-item width">
                                            <div class="ckbox ckbox-primary">
                                                <input autocorrect="off" type="checkbox" id="Other"
                                                       onclick="purpose_other(this.checked)"
                                                       value="Other"<?php
                                                if (in_array("Other", $purposedata) || $data1['purposeother']) {
                                                    echo "checked";
                                                } ?> name="active[]" required="">
                                                <label class="my-label" for="Other">Other </label>
                                                <div class="col-xs-9">
                                                    <?php if ($data1['purposeother']) { ?>
                                                        <input autocorrect="off" type="text" id="target"
                                                               onkeypress="Maxval(this.value,30,this.id)"
                                                               name="purposeother"
                                                               value="<?php echo $data1['purposeother']; ?>"
                                                               class="form-control my-input" placeholder="">
                                                        <?php $data1['purposeother'];
                                                    } else { ?>
                                                        <input autocorrect="off" type="text"
                                                               onkeypress="Maxval(this.value,30,this.id)"
                                                               id="target" style="visibility:hidden;"
                                                               name="purposeother"
                                                               value="<?php echo $datarow['purposeother']; ?>"
                                                               class="form-control my-input" placeholder="">
                                                    <?php } ?>

                                                    <div id="countertarget" class="my-input-margin"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3 my-label"
                                            <?php
                                            if (findAssignment($required_fields, 'purpose', 'purposecomments')) {
                                                echo " style='color:#428bca;' ";
                                            }
                                            ?>
                                        ><span>Public Purpose for Fund Comment:</span>
                                            <?php if ($lastVisitData): ?>
                                                <br><br>
                                                <button class="btn btn-primary"
                                                        id="copyPublicComment">Copy prev.
                                                </button>
                                            <?php endif; ?>
                                        </div>
                                        <div class="col-sm-8">
                                        <textarea autocorrect="off" class="form-control my-input" rows="8"
                                                  cols="15" required placeholder=""
                                                  name="purposecomments"
                                                  id="purposecomments"><?php
                                            echo $dataquerys['purposecomments'];
                                            ?></textarea>
                                            <div id="counterpurposecomments" class="purpose-text"></div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3 my-label"
                                            <?php
                                            if (findAssignment($required_fields, 'purpose', 'purposedonation')) {
                                                echo " style='color:#428bca;' ";
                                            }

                                            ?>
                                        >
                                            <span>We suggest you help the above with a :</span>
                                        </div>
                                        <div class="col-sm-8">
                                            <select id="purposedonation"
                                                    name="donation" class="form-control my-input">

                                                <option value="" disabled selected>Choose One</option>
                                                <option
                                                        value="Substantial Donation" <?php if ($data1['donation'] == 'Substantial Donation' || ($defaultDonation['value'] == 'Substantial Donation') && $data1['donation'] == '') {
                                                    echo 'selected';
                                                } ?>>Substantial Donation
                                                </option>
                                                <option
                                                        value="Generous Donation" <?php if ($data1['donation'] == 'Generous Donation' || ($defaultDonation['value'] == 'Generous Donation') && $data1['donation'] == '') {
                                                    echo 'selected';
                                                } ?>>Generous Donation
                                                </option>
                                                <option
                                                        value="Standard Donation" <?php if ($data1['donation'] == 'Standard Donation' || ($defaultDonation['value'] == 'Standard Donation') && $data1['donation'] == '') {
                                                    echo 'selected';
                                                } ?>>Standard Donation
                                                </option>
                                                <option
                                                        value="One Meal Equivalent" <?php if ($data1['donation'] == 'One Meal Equivalent' || ($defaultDonation['value'] == 'One Meal Equivalent') && $data1['donation'] == '') {
                                                    echo 'selected';
                                                } ?>>One Meal Equivalent
                                                </option>
                                                <option
                                                        value="Token Donation" <?php if ($data1['donation'] == 'Token Donation' || ($defaultDonation['value'] == 'Token Donation') && $data1['donation'] == '') {
                                                    echo 'selected';
                                                } ?>>Token Donation
                                                </option>
                                                <option
                                                        value="No Special Donation" <?php if ($data1['donation'] == 'No Special Donation' || ($defaultDonation['value'] == 'No Special Donation') && $data1['donation'] == '') {
                                                    echo 'selected';
                                                } ?>>No Specific Recommendation
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3 my-label">
                                            Internal Purpose for Fund Comment:
                                            <?php if ($lastVisitData): ?>
                                                <br><br>
                                                <button class="btn btn-primary"
                                                        id="copyInternalComment">Copy prev.
                                                </button>
                                            <?php endif; ?>
                                        </div>
                                        <div class="col-sm-8">
                                            <textarea autocorrect="off" class="form-control my-input" rows="8"
                                                      cols="15" required placeholder=""
                                                      name="internal_purpose"
                                                      id="internal-purpose"><?php
                                                echo $dataquerys['internal_purpose'];
                                                ?></textarea>
                                            <div id="counter-internal-comments" class="purpose-text"></div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3 my-label">
                                            General Comments:
                                            <?php if ($lastVisitData): ?>
                                                <br><br>
                                                <button class="btn btn-primary"
                                                        id="copyGeneralComment">Copy prev.
                                                </button>
                                            <?php endif; ?>
                                        </div>
                                        <div class="col-sm-8">
                                            <textarea autocorrect="off" class="form-control my-input" rows="8"
                                                      cols="15" required placeholder=""
                                                      name="general_comments"
                                                      id="general-comments"><?php
                                                echo $dataquerys['general_comments'];
                                                ?></textarea>
                                            <!--<div id="counterpurposecomments"></div>-->
                                        </div>
                                    </div>
                                </div><!-- panel-body -->
                            </div>

                        </div><!-- tab-pane -->
                        <div style=""
                             class="<?php if (!$personalinfonext && !$visistprev) {
                                 echo 'tab-pane active';
                             } else {
                                 echo 'tab-pane';
                             } ?>" id="tab6-4">

                            <div class="first-block">
                                <div class="row ml10">
                                    <input autocorrect="off" name="memberid" type="hidden" value="<?= $regdate; ?>">
                                    <input autocorrect="off" name="visitid" type="hidden"
                                           value="<?= $_SESSION['visit']; ?>">
                                    <div class="col-sm-6 my-label">Examination Type : (check all that apply)</div>
                                </div>

                                <div class="panel-body">
                                    <div class="row msg-reply">
                                        <div class="col-sm-2">
                                            <div class="ckbox ckbox-primary">
                                                <?php $fundsdata = explode(",", $data1['fundsreference']); ?>
                                                <input autocorrect="off"
                                                       type="checkbox"
                                                    <?php if (in_array('Documents',
                                                        $fundsdata)
                                                    ): ?>
                                                        checked
                                                    <?php endif; ?>
                                                       id="Documents"
                                                       value="Documents"
                                                       name="active1[]">
                                                <label class="my-label checkbox" for="Documents">Documents </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="ckbox ckbox-primary">
                                                <input autocorrect="off"
                                                       type="checkbox"
                                                    <?php if (in_array('Identification',
                                                        $fundsdata)
                                                    ): ?>
                                                        checked
                                                    <?php endif; ?>
                                                       id="Identification"
                                                       value="Identification"
                                                       name="active1[]">
                                                <label class="my-label" for="Identification">Identification </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="ckbox ckbox-primary">
                                                <input autocorrect="off"
                                                       type="checkbox"
                                                    <?php if (in_array('References',
                                                        $fundsdata)
                                                    ): ?>
                                                        checked
                                                    <?php endif; ?>
                                                       id="References"
                                                       value="References"
                                                       name="active1[]">
                                                <label class="my-label" for="References">References </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-1 width" id="fix">
                                            <div class="ckbox ckbox-primary">
                                                <input autocorrect="off" type="checkbox"
                                                       onclick="reference_other(this.checked)"
                                                    <?php
                                                    if (in_array("Other1", $fundsdata)) {
                                                        echo "checked";
                                                    } ?>
                                                       id="Other1"
                                                       value="Other1"
                                                       name="active1[]">
                                                <label class="my-label" for="Other1">Other </label>
                                                <div class="col-xs-9">
                                                    <?php if ($data1['referenceother']) { ?>
                                                        <input autocorrect="off" type="text" id="target1"
                                                               onkeypress="Maxval(this.value,30,this.id)"
                                                               name="referenceother"
                                                               value="<?php echo $data1['referenceother']; ?>"
                                                               class="form-control textarea-comment">
                                                        <?php $data1['referenceother'];
                                                    } else { ?>
                                                        <input autocorrect="off" type="text"
                                                               onkeypress="Maxval(this.value,30,this.id)"
                                                               id="target1" style="visibility:hidden;"
                                                               name="referenceother"
                                                               value="<?php echo $datarow['referenceother']; ?>"
                                                               class="form-control textarea-comment">
                                                    <?php } ?>

                                                    <div id="countertarget1" class="my-input-margin"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row tab-reference">
                                        <div class="col-sm-2 my-label">Comments
                                        </div>
                                        <div class="col-sm-8">
                                        <textarea autocorrect="off" class="form-control textarea-comment tab-reference"
                                                  rows="8"
                                                  cols="15" placeholder="" name="referencecomments"
                                                  id="referencecomments"><?php if ($dataquery1 != '') {
                                                echo $dataquery1['referencecomments'];
                                            } else {
                                                echo $data1['referencecomments'];
                                            } ?></textarea>
                                            <div id="counterreferencecomments" class="purpose-text"></div>
                                        </div>
                                    </div>
                                </div><!-- panel-body -->
                            </div>
                        </div><!-- tab-pane -->
                        <?php
                        $array = [
                            $personalinfonext,
                            $visistprev,
                            $addressprev,
                            $visitsnext,
                            $fundsprev,
                            $addressnext,
                            $purposeprev,
                            $purposenext,
                            $signprev,
                            $fundsnext,
                            $referenceprev,
                            $signnext
                        ];
                        $flag = true;
                        foreach ($array as $element) {
                            if ($element) {
                                $flag = false;
                                break;
                            }
                        }
                        ?>
                        <div style=""
                             class="<?php if ($referencenext == 'referencenext' && $flag) {
                                 echo 'tab-pane active';
                             } else {
                                 echo 'tab-pane';
                             } ?>" id="tab7-4">

                            <input autocorrect="off" name="memberid" type="hidden" value="<?= $regdate; ?>">
                            <input autocorrect="off" name="visitid" type="hidden" value="<?= $_SESSION['visit']; ?>">

                            <!-- updated stuff -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title" style="padding: 10px 0px 0px 10px;">Photo
                                        <span style="float:right;"> Print </span
                                    </h4>
                                </div><!-- panel-heading -->
                                <div class="panel-body">
                                    <div class="col-md-4">

                                        <input autocorrect="off" name="imgsrc1" id="imgsrc1" type="hidden">
                                        <input autocorrect="off" id="a1" type="hidden" name="a1"/>
                                        <input autocorrect="off" id="b1" type="hidden" name="b1"/>
                                        <div id="my_camera"></div>
                                        <div id="mobilePhotoDiv"
                                             style="margin-right:5px; float:left; padding-top: 8px; border-radius: 3px; text-align: center; overflow: hidden; width: 125px; height: 37px; background-color: #5eb95e;"
                                        >
                                            <div style="color: #fff; font-size: 14px;">Take Snapshot
                                            </div>
                                            <input autocorrect="off" type="file"
                                                   capture="camera"
                                                   accept="image/*"
                                                   id="mobilePhoto"
                                                   name="mobilePhoto"
                                                   class="mobile-photo-div">
                                        </div>
                                        <button id="webButton1" class="btn btn-primary" type="button"
                                                onClick="funsign()">Camera
                                        </button>
                                        <button id="webButton2" class="btn btn-success" type="button"
                                                onClick="take_snapshot('<?= $regdate; ?>','<?= $_SESSION['visit']; ?>')">
                                            Take Snapshot
                                        </button>

                                    </div>
                                    <div class="col-md-4  picture">
                                        <h4 class="panel-title" style="margin-left: 2%;">
                                            Current Photo</h4>

                                        <div class="loading" id="current-photo"></div>

                                        <div id="results" style="display: inline-block">
                                            <?php if ($data1['photoname'] != ''): ?>
                                                <div class="doc-wrapper"
                                                     id="<?= $data1['photoname']; ?>">
                                                    <a href="<?= $defaultPathPhoto . $data1['photoname']; ?>?v=<?= time() ?>"
                                                       class="doc-picture"><img class="docPicture"
                                                                                src="<?= $defaultPathPhoto . $data1['photoname']; ?>?v=<?= time() ?>"
                                                                                height=150></a>
                                                    <div data-animate-angle="0" data-rotate-angle="0"
                                                         class="rotate btn btn-warning">ROTATE PICTURE
                                                    </div>
                                                    <div class="cross btn btn-danger">DELETE PICTURE</div>
                                                </div>
                                            <?php endif; ?>
                                        </div>

                                        <div id="preview"
                                             style="float: left; margin-top: 5%; margin-right: 2%;"></div>

                                        <script type="text/javascript" src="webcam.js"></script>
                                        <script language="JavaScript">
                                            function funsign() {
                                                Webcam.set({
                                                    width: 250,
                                                    height: 210,
                                                    dest_width: 250,
                                                    dest_height: 210,
                                                    image_format: 'jpg',
                                                    jpeg_quality: 360
                                                });
                                                Webcam.attach('#my_camera');
                                            }
                                        </script>
                                    </div>
                                    <div class="col-md-4">
                                        <div id="sub3" style="float:right;">
                                            <a id="printRegistrationOfIdentity"
                                               href="Registration_Identity/Registration_Identity/Registration_Identity.php?a=<?= $regdate; ?>&&b=<?= $_SESSION['visit']; ?>"
                                               style="color:#fff" target="blank" class="">
                                                <img src="images/pdf_yes1.png"
                                                     class="btn btn-success"
                                                     title="REGISTRATION OF IDENTITY"
                                                     rel="tooltip" style="width:72px; height:56px;">
                                            </a>
                                            <a id="printRegistrationOfIdentityFake"
                                               href="Registration_Identity/Registration_Identity/Registration_Identity.php?a=<?= $a ?>&&b=<?= $_SESSION['visit'] ?>"
                                               style="color:#fff; visibility:hidden;" target="blank"
                                               class="">
                                                <br/><br/>
                                                <a id="printCharityInformation"
                                                   href="charity_Baltimore/pdfprint.php?a=<?= $regdate; ?>&&b=<?= $_SESSION['visit']; ?>&green=<?= true ?>"
                                                   style="color:#fff" target="blank" class="">
                                                    <img src="images/pdf_yes2.png"
                                                         class="btn btn-success"
                                                         title="CHARITY INFORMATION"
                                                         rel="tooltip" style="width:72px; height:56px;">
                                                </a>
                                                <a id="printCharityInformationFake"
                                                   href="charity_Baltimore/pdfprint.php?a=<?= $a; ?>&&b=<?= $_SESSION['visit']; ?>&green=<?= true ?>"
                                                   style="color:#fff; visibility:hidden;" target="blank"
                                                   class="">
                                                </a>
                                                <br/><br/>
                                                <a id="printCharityInformationBlank"
                                                   href="charity_Baltimore/pdfprint_blank.php?a=<?= $regdate; ?>&&b=<?= $_SESSION['visit']; ?>"
                                                   style="color:#fff; display: none; " target="blank">
                                                    <img src="images/pdf_yes2.png"
                                                         class="btn btn-success"
                                                         title="CHARITY INFORMATION BLANK"
                                                         rel="tooltip"
                                                         style="width:72px; height:56px;">
                                                </a>
                                                <a id="printCharityInformationBlankFake"
                                                   href="charity_Baltimore/pdfprint_blank.php?a=<?= $a; ?>&&b=<?= $_SESSION['visit']; ?>"
                                                   style="color:#fff; visibility:hidden;"
                                                   target="blank" class="">
                                                </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title" style="padding: 10px 0px 0px 10px;">
                                        Collector's Signature </h4>
                                </div><!-- panel-heading -->
                                <div class="panel-body">
                                    <?php include "visitssignature.php"; ?>
                                    <div class="col-sm-6">
                                        Previously Saved Signature
                                        <div id="imgDiv">
                                            <?php $signid = $data1['Id'];
                                            $sql1 = $dbh->prepare("select * from visitstable where Id='$signid'");
                                            $sql1->execute();
                                            $data1 = $sql1->fetch();
                                            if ($data1['signname'] != '') {
                                                echo '<img style="width:350px;" src="' . $defaultPathSign . $data1['signname'] . '">';
                                            }
                                            ?>
                                        </div>
                                        <div id="newUserSignature">
                                            Updated Signature
                                            <div>
                                                <img id="saveSignature1" form="basicForm"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- panel -->
                        </div><!-- tab-pane -->
                        <?php
                        $array = [
                            $personalinfonext,
                            $visistprev,
                            $addressprev,
                            $visitsnext,
                            $fundsprev,
                            $addressnext,
                            $purposeprev,
                            $purposenext,
                            $signprev,
                            $fundsnext,
                            $referenceprev,
                            $referencenext
                        ];
                        $flag = true;
                        foreach ($array as $element) {
                            if ($element) {
                                $flag = false;
                                break;
                            }
                        }
                        ?>
                        <div style=""
                             class="<?php if ($signnext == 'signnext' && $flag) {
                                 echo 'tab-pane active';
                             } else {
                                 echo 'tab-pane';
                             } ?>" id="tab8-4">

                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="loading"></div>
                                    <p></p>
                                    <?php
                                    $no = $_GET['a'];
                                    $sqlquery = $dbh->prepare("select * from visitstable where collectorsid='$no'");
                                    $sqlquery->execute();
                                    $dataquery = $sqlquery->fetch();
                                    ?>
                                    <table id="basicTable"
                                           class="basic-table table table-striped table-bordered responsive">
                                        <thead class="">
                                        <tr>
                                            <th style="text-align:center"> Last Name, Names</th>
                                            <th style="text-align:center"> Reference Id</th>
                                            <th style="text-align:center"> Visit Id</th>
                                            <th style="text-align:center"> Authorized From</th>
                                            <th style="text-align:center"> To</th>
                                            <?php if ($showSite) { ?>
                                                <th style="text-align:center"> Sites</th>
                                            <?php } ?>
                                            <th style="text-align:center"> Status</th>
                                            <th style="text-align:center"> Edit</th>
                                            <th style="text-align:center"> Pictures</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $collectorSql = $dbh->prepare("select * from collectors where Id='$no'");
                                        $collectorSql->execute();
                                        $collecrtorData = $collectorSql->fetch();


                                        $sql2 = $dbh->prepare("
                                                               SELECT *
                                                               FROM
                                                                 `visitstable`
                                                               WHERE
                                                                 `collectorsid` = '$no'
                                                           ");
                                        $sql2->execute();
                                        while ($data2 = $sql2->fetch()) {
                                            ?>

                                            <tr class="test <?php if ($data2['visitid'] == $visit) {
                                                echo 'visit-row';
                                            } ?>" style="">
                                                <td>
                                                    <label
                                                            style="visibility:hidden;position: absolute;"><?= $collecrtorData['lastname']; ?> </label>
                                                    <?= $collecrtorData['title'] . '  ' . $collecrtorData['firstname'] . ' ' . $collecrtorData['lastname']; ?>
                                                </td>
                                                <td>
                                                    <?= $data2['Refid']; ?>
                                                </td>
                                                <td>
                                                    <?= $data2['visitid']; ?>
                                                </td>
                                                <td>
                                                    <?= $data2['authofromdate']; ?>
                                                </td>
                                                <td>
                                                    <?= $data2['authotodate']; ?>
                                                </td>
                                                <?php if ($showSite) { ?>
                                                    <td>
                                                        <?php
                                                        $s = $data2['sites'];
                                                        $sqlq = $dbh->prepare("SELECT * FROM `sites` WHERE `Id`='$s'");
                                                        $sqlq->execute();
                                                        $dataq = $sqlq->fetch();
                                                        echo $dataq['Sites'];
                                                        ?>
                                                    </td>
                                                <?php } ?>
                                                <td>
                                                    <?php
                                                    $curdate = date('M-d-Y');
                                                    $authdate = $data2['authotodate'];
                                                    $authfrmdate = $data2['authofromdate'];

                                                    if (strtotime($curdate) < strtotime($authfrmdate)) {
                                                        echo 'Planned';
                                                    } elseif (strtotime($curdate) > strtotime($authdate)) {
                                                        echo 'Expired';
                                                    } elseif (strtotime($curdate) >= strtotime($authfrmdate) && strtotime($curdate) <= strtotime($authdate)) {
                                                        echo 'Current';
                                                    } else {
                                                        echo 'Error';
                                                    }
                                                    ?>
                                                </td>
                                                <td>
                                                    <a href="emvs.php?action=editvisit1&&a=<?= $data2['collectorsid']; ?>&&b=<?= $data2['visitid']; ?>">
                                                        <img class="visit-edit"
                                                             src="images/edit1.png" title="edit"
                                                             data-toggle="tooltip" border="0">
                                                    </a>
                                                </td>
                                                <td>
                                                    <?php
                                                    $sql = $dbh->prepare("SELECT * FROM `visitstable` WHERE `collectorsid` = '$no'  AND `visitid` = {$data2['visitid']};");
                                                    $sql->execute();
                                                    $visitData = $sql->fetch();
                                                    ?>
                                                    <?php if ($visitData['photoname']): ?>
                                                        <a class="image-popup-vertical-fit"
                                                           href="<?= 'uploads/' . $visitData['photoname']; ?>"
                                                           id="<?= $visitData['Id'] ?>">view</a>
                                                    <?php else: ?>
                                                        0
                                                    <?php endif; ?>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <?php if ($lastVisitData['internal_purpose']): ?>
                                <div id="last-internal-comment" class="my-block">
                                    <div class="row font-size">
                                        <span style="color:#428bca; font-weight: bold;">Internal Purpose for Fund Comment:&nbsp;&nbsp;</span>
                                        <?= $lastVisitData['internal_purpose'] ?>
                                    </div>
                                </div>
                            <?php endif; ?>

                            <?php if ($lastVisitData['general_comments']): ?>
                                <div id="last-general-comment" class="my-block">
                                    <div class="row font-size">
                                        <span
                                                style="color:#428bca; font-weight: bold;">General Comments:&nbsp;&nbsp;</span>
                                        <?= $lastVisitData['general_comments'] ?>
                                    </div>
                                </div>
                            <?php endif; ?>

                            <div class="my-block">
                                <div id="docMobilePhotoDiv"
                                     style="margin-right:5px; float:left; padding-top: 8px; border-radius: 3px; text-align: center; overflow: hidden; width: 125px; height: 37px; background-color: #5eb95e;">
                                    <div style="color: #fff; font-size: 14px;">Take Picture
                                    </div>

                                    <input autocorrect="off" type="file"
                                           capture="camera"
                                           accept="image/*"
                                           id="docMobilePhoto"
                                           name="docMobilePhoto"
                                           class="mobile-photo-div">
                                </div>
                                <div id="coll-documents-galery"
                                     class="coll-documents-gallery">
                                    <?php
                                    $id = $_GET['a'];
                                    $sql = $dbh->prepare("SELECT * FROM `collphoto` WHERE `coll_id` = '$id' AND `deleted` = 0 ORDER BY `id` DESC;");
                                    $sql->execute();
                                    $docPictures = $sql->fetchAll();
                                    ?>
                                    <?php if (count($docPictures) > 0): ?>
                                        <?php foreach ($docPictures as $picture): ?>
                                            <?php if (file_exists('uploads/' . $picture['name'])): ?>
                                                <div class="doc-wrapper crop picture"
                                                     id="<?= $picture['name']; ?>">
                                                    <div class="loading"></div>
                                                    <a href="<?= 'uploads/' . $picture['name']; ?>?v=<?= time() ?>"
                                                       class="doc-picture"><img
                                                                class="docPicture"
                                                                src="<?= 'uploads/' . $picture['name']; ?>?v=<?= time() ?>"
                                                                height="150px"></a>
                                                    <div data-animate-angle="0" data-rotate-angle="0"
                                                         class="rotate btn btn-warning">ROTATE PICTURE
                                                    </div>
                                                    <div class="cross btn btn-danger">DELETE PICTURE</div>
                                                </div>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    <?php endif; ?>

                                    <?php if ($data1['photoname'] != ''): ?>
                                        <div class="doc-wrapper crop picture"
                                             id="<?= $data1['photoname']; ?>">
                                            <div class="loading"></div>
                                            <a href="<?= $defaultPathPhoto . $data1['photoname']; ?>?v=<?= time() ?>"
                                               class="doc-picture"><img class="docPicture"
                                                                        src="<?= $defaultPathPhoto . $data1['photoname']; ?>?v=<?= time() ?>"
                                                                        height=150></a>
                                            <div data-animate-angle="0" data-rotate-angle="0"
                                                 class="rotate btn btn-warning">ROTATE PICTURE
                                            </div>
                                            <div class="cross btn btn-danger">DELETE PICTURE</div>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div><!-- tab-pane -->

                    </div><!-- tab-content -->
                    <form enctype="multipart/form-data" method="post" accept-charset="utf-8" class="panel-wizard"
                          novalidate="novalidate" name="basicForm" id="basicForm">
                    </form>
            </div><!-- mainpanel -->
        </div><!-- mainwrapper -->
</section>

<script>

    //Checking visit id duplication
    function visitIdDuplicationCheck() {
        var collId = $('#memberid').val();
        var visitId = $('#visitId').val();
        var thisVisitId = $('input[name="visitsid"]').val();
        var data = 'collId=' + collId + '&visitId=' + visitId + '&thisVisitId=' + thisVisitId;
        $.ajax({
            type: 'POST',
            url: 'visitIdDuplicationCheck.php',
            data: data,
            success: function (msg) {
                if (msg == 'duplication') {
                    $('#visitId').val('');
                    $('#visitIdErr').html('Visit #' + visitId + ' already exists');
                } else if (msg == 'ok') {
                    $('#visitIdErr').html(' ');
                }
            }
        });
    }

    //Checking reference id duplication
    function refIdDuplicationCheck() {
        var reference = $('#refid');
        var refId = reference.val();
        var prevId = reference.attr('lang');
        var data = 'refId=' + refId + '&prevId=' + prevId;
        $.ajax({
            type: 'POST',
            url: 'refIdDuplicationCheck.php',
            data: data,
            success: function (msg) {
                if (msg == 'duplication') {
                    reference.parent().find('.ref-id-err').html('Reference Id #' + refId + ' already exists');
                    reference.parent().find('.ref-id-err').css('display', 'inline');
                } else if (msg == 'ok') {
                    reference.parent().find('.ref-id-err').html(' ');
                    reference.parent().find('.ref-id-err').css('display', 'none');
                }
            }
        });
    }

    $(document).ready(function () {
        var collectorId = $('input[name="memberids"]').val();
        var visitId = $('input[name="visitid"]').val();

        $('#copyPublicComment').on('click', function (event) {
            event.preventDefault();
            getPrevComment('purposecomments', 'purposecomments', collectorId, visitId);
        });

        $('#copyInternalComment').on('click', function (event) {
            event.preventDefault();
            getPrevComment('internal-purpose', 'internal_purpose', collectorId, visitId);
        });

        $('#copyGeneralComment').on('click', function (event) {
            event.preventDefault();
            getPrevComment('general-comments', 'general_comments', collectorId, visitId);
        });

        //Setting duplication check for visit id
        $('#visitId').on('keyup', function () {
            visitIdDuplicationCheck();
        });

        $('#visitId').on('keypress', function (e) {
            var k = e.keyCode;
            console.log(e.keyCode);
            return (k == 8 || (k >= 48 && k <= 57) || k == 43 || k == 63);
        });

        //Setting duplication check for ref id
        $('#refid').on('keyup', function () {
            refIdDuplicationCheck();
        });


        $('input[name="last-refid"]').on('keypress', function (e) {
            var k = e.keyCode;
            return (k == 8 || (k >= 48 && k <= 57) || k == 43);
        });

        //Setting mask for USA numbers
        $('#contact').find('input[name="country"]').each(function () {
            if ($.trim($(this).val()) == 'USA' || $.trim($(this).val()) == 'usa') {
                $(this).closest('div[data-id]').find('input[name="detail_number"]').simpleMask({
                    'mask': 'usa',
                    'nextInput': false
                });
            }
        });

        //getting user's time zone
        var timeZone = jstz.determine();
        var zone = timeZone.name();
        var format = 'MM-DD-YYYY';
        var today = moment().tz(zone).format(format);

        //adding today's date to the pdf links
        var link;
        var registration = $('#printRegistrationOfIdentityFake');
        link = registration.attr('href');
        link = link + '&&c=' + today;
        registration.attr('href', link);

        var charity = $('#printCharityInformationFake');
        link = charity.attr('href');
        link = link + '&&c=' + today;
        charity.attr('href', link);
    });
    //Copying address from the personal tab
    function copyPersonalAddress() {
        var address1 = $('#address1').val();
        var address2 = $('#address2').val();
        var city = $('#city').val();
        var state = $('#state').val();
        var country = $('#country').val();
        var zipcode = $('#zipcode').val();
        var phoneNumber = $("input[name='CPrimary']:checked").closest('.form-group').find("input[name='Phone_Number']").val();

        $('#tempaddress1').val(address1);
        $('#tempaddress2').val(address2);
        $('#tempcity').val(city);
        $('#tempstate').val(state);
        $('#tempcountry').val(country);
        $('#tempzipcode').val(zipcode);
        $('#tempphone').val(phoneNumber);

        $('#tempaddress1').trigger('change');
        $('#tempaddress2').trigger('change');
        $('#tempcity').trigger('change');
        $('#tempstate').trigger('change');
        $('#tempcountry').trigger('change');
        $('#tempzipcode').trigger('change');
        $('#tempphone').trigger('change');
    }

    //Copying address from US address
    function copyUsAddress() {
        var address1 = $('#tempaddress1').val();
        var address2 = $('#tempaddress2').val();
        var city = $('#tempcity').val();
        var state = $('#tempstate').val();
        var country = $('#tempcountry').val();
        var zipcode = $('#tempzipcode').val();
        var phoneNumber = $("#tempphone").val();

        $('#baltiaddress1').val(address1);
        $('#baltiaddress2').val(address2);
        $('#balticity').val(city);
        $('#baltistate').val(state);
        $('#balticountry').val(country);
        $('#baltizipcode').val(zipcode);
        $('#baltiphone').val(phoneNumber);

        $('#baltiaddress1').trigger('change');
        $('#baltiaddress2').trigger('change');
        $('#balticity').trigger('change');
        $('#baltistate').trigger('change');
        $('#balticountry').trigger('change');
        $('#baltizipcode').trigger('change');
        $('#baltiphone').trigger('change');
    }

    $(document).ready(function () {
        $('#copyPersonalAddress').on('click', copyPersonalAddress);

        $('#copyUsAddress').on('click', copyUsAddress);

        $('body').on('click', '.cross', deleteDoc);
        $('body').on('click', '.rotate', rotateDoc);

        $('#view-alert').magnificPopup({
            type: 'inline',
            preloader: false
        });

        $('.image-popup-vertical-fit').magnificPopup({
            type: 'image',
            closeOnContentClick: true,
            mainClass: 'mfp-img-mobile',
            image: {
                verticalFit: true
            }

        });

        $('#coll-documents-galery').magnificPopup({
            delegate: 'a',
            type: 'image',
            tLoading: 'Loading image #%curr%...',
            mainClass: 'mfp-img-mobile',
            gallery: {
                enabled: true,
                navigateByImgClick: true,
                preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
            },
            image: {
                tError: '<a href="%url%">The image #%curr%</a> could not be loaded.'
            }
        });

        $('#results').magnificPopup({
            delegate: 'a',
            type: 'image',
            tLoading: 'Loading image #%curr%...',
            mainClass: 'mfp-img-mobile',
            gallery: {
                enabled: true,
                navigateByImgClick: true,
                preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
            },
            image: {
                tError: '<a href="%url%">The image #%curr%</a> could not be loaded.'
            }
        });

        $('#preview').magnificPopup({
            delegate: 'a',
            type: 'image',
            tLoading: 'Loading image #%curr%...',
            mainClass: 'mfp-img-mobile',
            gallery: {
                enabled: true,
                navigateByImgClick: true,
                preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
            },
            image: {
                tError: '<a href="%url%">The image #%curr%</a> could not be loaded.'
            }
        });

        $('.popup-gallery').magnificPopup({
            delegate: 'a',
            type: 'image',
            tLoading: 'Loading image #%curr%...',
            mainClass: 'mfp-img-mobile',
            closeOnBgClick: true,
            gallery: {
                enabled: true,
                navigateByImgClick: true,
                preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
            },
            image: {
                tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
                titleSrc: function (item) {
                    return item.el.attr('title') + '<small>by Marsel Van Oosten</small>';
                }
            }
        });

        $('.visit-edit').on('click', function (event) {
            if (!confirm('Are you sure you want to change visits?')) {
                event.preventDefault();
            }
        });

        $('#mobilePhoto').on('click', function (event) {
            photoResize(this.id)
        });

        $('#docMobilePhoto').on('click', function (event) {
            photoResize(this.id)
        });


        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
            $("#mobilePhotoDiv").css("display", "inline-block");
            $("#preview").css("display", "inline-block");
            $("#webButton1").css("display", "none");
            $("#webButton2").css("display", "none");
        } else {
            $("#preview").css("display", "none");
            $("#mobilePhotoDiv").css("display", "none");
        }

        var SSN_Tab_out = false;
        jQuery("#Identno").blur(function () {
            //alert(SSN_Tab_out);
            if (!SSN_Tab_out && jQuery(this).val().trim().length == 9) {
                //alert(Identno);
                var Identno = jQuery("#Identno").val();

                jQuery("#Identno").val(Identno.substring(0, 3) + "-" + Identno.substring(3, 5) + "-" + Identno.substring(5));
                self.Identno(jQuery("#Identno").val());
                SSN_Tab_out = true;
            }
        });

        jQuery("#Identno").focusin(function () {
            self.Identno(jQuery("#Identno").val().replace(/-/g, ""));
            SSN_Tab_out = false;
        });

        validatePost();
    });

    function validatePost() {
        jQuery.post("check.php", {allowedFields: "allowed"}, function (data) {
            validateFunction(data);
        });
    }

    function validateFunction(data) {
        var customedObj = {};
        customed = {};
        customedObj = jQuery.parseJSON(data);

        for (var i = 0; i < customedObj.length; i++) {

            field = customedObj[i].assignment;

            customed[field] = {required: false};
        }
        customed['active[]'] = {required: false};
        setTimeout(function () {
            jQuery("#valWizard").validate({
                rules: customed,
                errorClass: "override-error"
            });
        }, 0);
    }
</script>

<script src="javascript-image-upload/vendor/canvas-to-blob.min.js"></script>
<script src="javascript-image-upload/resize.js"></script>
<?php
echo '<script src="javascript-image-upload/app.js?v=' . time() . '" ></script>';
?>

<!--DataTables-->
<script>

    jQuery(document).ready(function () {

        jQuery('#basicTable').DataTable({
            responsive: true
        });

        var shTable = jQuery('#shTable').DataTable({
            "fnDrawCallback": function (oSettings) {
                jQuery('#shTable_paginate ul').addClass('pagination-active-dark');
            },
            responsive: true
        });

        // Show/Hide Columns Dropdown
        jQuery('#shCol').click(function (event) {
            event.stopPropagation();
        });

        jQuery('#shCol input').on('click', function () {

            // Get the column API object
            var column = shTable.column($(this).val());

            // Toggle the visibility
            if ($(this).is(':checked'))
                column.visible(true);
            else
                column.visible(false);
        });

        // Add event listener for opening and closing details
        jQuery('#exRowTable tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = exRowTable.row(tr);

            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            }
            else {
                // Open this row
                row.child(format(row.data())).show();
                tr.addClass('shown');
            }
        });


        // DataTables Length to Select2
        jQuery('div.dataTables_length select').removeClass('form-control input-sm');
        jQuery('div.dataTables_length select').css({width: '60px'});
        jQuery('div.dataTables_length select').select2({
            minimumResultsForSearch: -1
        });

    });

    function format(d) {
        // `d` is the original data object for the row
        return '<table class="table table-bordered nomargin">' +
            '<tr>' +
            '<td>Full name:</td>' +
            '<td>' + d.name + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Extension number:</td>' +
            '<td>' + d.extn + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Extra info:</td>' +
            '<td>And any further details here (images etc)...</td>' +
            '</tr>' +
            '</table>';
    }
</script>

<script type="text/javascript" src="js/jquery.validate.min.js"></script>

<script src="js/jquery-migrate-1.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/modernizr.min.js"></script>
<script src="js/ppace.min.js"></script>

<script src="js/jquery.cookies.js"></script>

<script src="js/jquery.dataTables.min.js"></script>
<script src="js/dataTables.bootstrap.js"></script>
<script src="js/dataTables.responsive.js"></script>
<script src="js/select2.min.js"></script>

<script src="js/custom.js"></script>

<script src="signature.js"></script>
<script type="text/javascript" src="js/jquery.limit.js"></script>

<script>


    jQuery(function () {
        if (jQuery("#sites").val() != "") {
            jQuery("#sites").change();
        }

        jQuery("#printRegistrationOfIdentityFake").on("click",

            function (event) {

            }
        );

        jQuery("#printCharityInformationFake").on("click",

            function (event) {

            }
        );

        jQuery("#printCharityInformationBlankFake").on("click",

            function (event) {

            }
        );

        jQuery("#printRegistrationOfIdentity").on("click",

            function (event) {

                event.preventDefault();

                var fakeToBeCalled = "#printRegistrationOfIdentityFake";
                validateBeforePrintPost(fakeToBeCalled);

            });

        jQuery("#printCharityInformation").on("click",

            function (event) {

                event.preventDefault();

                var fakeToBeCalled = "#printCharityInformationFake";
                validateBeforePrintPost(fakeToBeCalled);

            });

        jQuery("#printCharityInformationBlank").on("click",

            function (event) {

                event.preventDefault();

                var fakeToBeCalled = "#printCharityInformationBlankFake";
                validateBeforePrintPost(fakeToBeCalled);

            });

    });

    function validateBeforePrintPost(fakeToBeCalled) {

        jQuery.post(
            "check.php",
            {
                requiredFields: "required"
            },
            function (response) {


            }
        ).done(function (response) {

            if (response != "ERROR") {
                validateBeforePrintProcessing(response, fakeToBeCalled);
            } else {
                allowPrintProcessing(fakeToBeCalled);
            }

        });
    }

    function allowPrintProcessing(fakeToBeCalled) {
        $link = jQuery(fakeToBeCalled);
        $link[0].click();
    }

    function validateBeforePrintProcessing(data, fakeToBeCalled) {

        var printable = true;

        var tabs = {
            visits: "Visits",
            collectorDetails: "Personal Info",
            address: "Address",
            funds: "Funds",
            purpose: "Purpose",
            reference: "Reference"
        };

        var tabAssignments = Object.keys(tabs);

        for (i = 0; i < tabAssignments.length; i++) {

            var tabToHighlight = "#" + tabAssignments[i] + "Tab a";
            jQuery(tabToHighlight).css("text-decoration", "none");
            jQuery(tabToHighlight).css("color", "white");
        }
        var required_fields_list = jQuery.parseJSON(data);

        for (var i = 0; i < required_fields_list.length; i++) {

            var el = "[name='" + required_fields_list[i].assignment + "']";

            if (jQuery(el).get(0) !== undefined) {
                if (el == "[name='active[]']") {
                    var checked = [];
                    $('input[name="active[]"]:checked').each(function () {
                        checked.push($(this).val());
                    });
                    if (checked == '') {
                        var tabToHighlight = "#" + required_fields_list[i].tab + "Tab a";
                        jQuery(tabToHighlight).css("text-decoration", "underline");
                        jQuery(tabToHighlight).css("color", "red");

                        printable = false;
                    }
                } else {
                    var elVal = jQuery(el).val();
                }

                var elType = jQuery(el).get(0).tagName;

                if (jQuery(el).val() == "" || jQuery(el).val() == null) {
                    var tabToHighlight = "#" + required_fields_list[i].tab + "Tab a";
                    jQuery(tabToHighlight).css("text-decoration", "underline");
                    jQuery(tabToHighlight).css("color", "red");

                    printable = false;

                }
            }

        }
        if (printable) {
            $link = jQuery(fakeToBeCalled);
            $link[0].click();
        } else {
            alert("Please, fill required fields to print out the document");
        }

    }

    jQuery(document).ready(function () {

        //setting comment length limit
        $("#purposecomments").limit({
            limit: 370,
            id_result: "counterpurposecomments",
            alertClass: "alert"
        });

        //setting comment length limit
        $("#internal-purpose").limit({
            limit: 510,
            id_result: "counter-internal-comments",
            alertClass: "alert"
        });

        //setting comment length limit
        $("#referencecomments").limit({
            limit: 890,
            id_result: "counterreferencecomments",
            alertClass: "alert"
        });

        // Wizard With Form Validation
        var $validator = jQuery("#valWizard").validate({
            highlight: function (element) {
                jQuery(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            success: function (element) {
                jQuery(element).closest('.form-group').removeClass('has-error');
            }
        });
    });

    $("#valWizard").on("submit",
        function () {

            var hasClass = jQuery("#purposeTab").hasClass("active");

            if (hasClass) {

                if ($("input[name='requiredFunds']").val() == 'true') {


                    if (!jQuery("input[name='active[]']:checked").length) {

                        alert("At least one Purpose of funds needs to be checked");
                        return false;
                    }
                }
            }
            hasClass = jQuery("#referenceTab").hasClass("active");
            if (hasClass) {
                if (jQuery("input[name='reference']").val() == 'true') {
                    if (!jQuery("input[name='active1[]']:checked").length) {
                        alert("At least one Purpose of funds needs to be checked");
                        return false;
                    }
                }
            }

        });


</script>

<!--jQuery Image Rotate-->
<script src="js/jquery-rotate.js"></script>
<!-- Magnific Popup core JS file -->
<script src="magnific-popupp/dist/jquery.magnific-popup.js"></script>
<!--Magnific Popup min JS file-->
<script src="magnific-popupp/dist/jquery.magnific-popup.min.js"></script>


