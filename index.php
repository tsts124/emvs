<?php
if ($_SERVER['HTTP_HOST'] == '52.38.38.181') {
    header('Location: https://baltimore.emvsystem.org/');
    die;
}

error_reporting(0);
session_start();
include 'includes/function.php';
include 'includes/dbcon.php';
$emailid = $_POST['emailid'];
$password = $_POST['password'];
$rememberMe = $_POST['rememberMe'];

if (isset($_POST['signin'])) {
    $x = 0;
    $pass = mc_encrypt($password, ENCRYPTION_KEY);
    $sql = $dbh->prepare('SELECT *
                          FROM  `newmember`
                          WHERE `emailid` = :login
                          AND `status` = :status
                          AND `Delete` = :deleted ;');
    $sql->execute([
        'login' => $emailid,
        'status' => 'Access',
        'deleted' => 0
    ]);
    $count = $sql->rowCount();
    if ($count == '1') {
        while ($data = $sql->fetch()) {
            $lostactivity = $data['lostactivity'];
            $wrongcount = $data['wrongcount'];
            $twrongcount = $data['twrongcount'];
            $roles = explode(",", $data['roles']);
            $endata = $data['password'];
            $pass = mc_decrypt($endata, ENCRYPTION_KEY);
            if ($password == $pass) {
                $sqlquery = $dbh->prepare('SELECT *
                                           FROM `newrole`
                                           ORDER BY `Id`
                                           DESC ;');
                $sqlquery->execute();
                while ($sqldata = $sqlquery->fetch()) {
                    $rolename = $sqldata['Rolename'] . ',';
                    $roles1 = explode(",", $rolename);
                    $names[] = preg_split('/(,{1}[\s]?|\sand\s)+/', $rolename);
                }
                $nc = count($names);
                for ($i = 0; $i <= $nc; $i++) {
                    $nn[] = $names[$i][0];
                }
                $result = array_intersect($nn, $roles);
                $_SESSION['roles'] = $result;

                $_SESSION['user'] = $data['emailid'];
                $sql = $dbh->prepare('UPDATE `newmember`
                                      SET `online` = :online, `Lastlogin` = now()
                                      WHERE `emailid` = :login ;');
                $sql->execute([
                    'login' => $emailid,
                    'online' => '1'
                ]);
                $err1 = '<div class="success1"><p><img src="images/success.png" border="none" width="18" height="18"> Sucessfully Login</p></div>';

                $sql = $dbh->prepare('SELECT *
                                      FROM  `newmember`
                                      WHERE `emailid` = :login
                                      AND `status`= :status
                                      AND `Delete` = :deleted ;');
                $sql->execute([
                    'login' => $emailid,
                    'status' => 'Access',
                    'deleted' => 0
                ]);
                $userData = $sql->fetch();

                header('Location: emvs.php?action=collector');

//                if ($userData['roles'] == 'Manager') {
//                    header('Location: emvs.php?action=collector');
//                } elseif ($userData['roles'] == 'Administrator') {
//                    header('Location: emvs.php?action=welcome');
//                }
            } else if ($password != $pass) {
                $c = $wrongcount + 1;
                $err = '<div class="error1"><p><img src="images/error.png" border="none" width="18" height="18"> Check your Password.</p></div>';
                $sql = $dbh->prepare('UPDATE `newmember`
                                      SET `wrongcount` = :wcount, twrongcount = :twcount
                                      WHERE `emailid` = :login ;');
                $sql->execute([
                    'wcount' => $c,
                    'twcount' => 1 + $twrongcount,
                    'login' => $emailid
                ]);
            } else if ($password == '') {
                $c = $wrongcount + 1;
                $err = '<div class="error1"><p><img src="images/error.png" border="none" width="18" height="18"> Check your Password.</p></div>';
            }
        }
        if ($err != '') {
            $sql = $dbh->prepare('UPDATE `newmember`
                                  SET `wrongcount` = :wcount, twrongcount = :twcount
                                  WHERE `emailid` = :login ;');
            $sql->execute([
                'wcount' => $c,
                'twcount' => 1 + $twrongcount,
                'login' => $emailid
            ]);
        }
        if ($err1 != '') {
            if ($lostactivity == '0000-00-00') {
                $sql = $dbh->prepare('UPDATE `newmember`
                                      SET `lostlogin` = now(), `lostactivity` = now()
                                      WHERE `emailid` = :login ;');
                $sql->execute([
                    'login' => $emailid
                ]);
            } else {
                $sql = $dbh->prepare('UPDATE `newmember`
                                      SET `lostlogin` = :laslogin, `lostactivity` = now()
                                      WHERE `emailid` = :login ;');
                $sql->execute([
                    'login' => $emailid,
                    'lastlogin' => $lostactivity
                ]);
            }
        }
    } else {
        $err = '<div class="error1"><p><img src="images/error.png" border="none" width="18" height="18"> User ID and/or Password is incorrect</p></div>';
    }
    $x++;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Electronic Meshulach Verification System</title>

    <link href="css/style.default.css" rel="stylesheet">

</head>

<body class="signin">


<section>

    <div class="panel panel-signin">
        <div class="panel-body">
            <div class="logo text-center">
                <img src="images/logo.png" alt="Chain Logo">
            </div>

            <p class="text-center">Sign in to your account</p>

            <div class="mb30"></div>

            <form action="#" method="post">
                <div class="input-group mb15">
                    <span class="input-group-addon"></span>
                    <input autocorrect="off"  value="<?= $emailid ?>" type="text" class="form-control" name="emailid" placeholder="Email" autofocus>
                </div><!-- input-group -->
                <div class="input-group mb15">
                    <span class="input-group-addon"></span>
                    <input autocorrect="off"  value="<?= $password ?>" type="password" class="form-control" name="password"
                           placeholder="Password">
                </div><!-- input-group -->

                <div class="clearfix">
                    <div class="pull-right">
                        <button type="submit" name="signin" value="signin" id="signin"
                                class="btn btn-primary btn-block">Sign In
                        </button>
                    </div>
                </div>
            </form>
            <h4 class="text-center mb5"><?php
                if ($err) {
                    echo $err;
                } else if ($err1) {
                    echo $err1;
                }
                ?></h4>
        </div><!-- panel-body -->
    </div><!-- panel -->

</section>

<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/jquery-migrate-1.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/modernizr.min.js"></script>
<script src="js/ppace.min.js"></script>

<script src="js/jquery.cookies.js"></script>

<script src="js/custom.js"></script>

</body>
</html>
