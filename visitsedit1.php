<?php
include 'includes/dbcon.php';

$abc = $_GET['abc'];
$a = $_GET['a'];
$b = $_GET['b'];

$sth = $dbh->prepare('SELECT `Id`
                      FROM `visitstable`
                      WHERE `visitid` = :visitid
                      AND `collectorsid` = :collid ;');
$sth->execute(['visitid' => $b, 'collid' => $a]);

if (!$sth->fetch()) {
    header('Location: emvs.php?action=collector');
}

$required_fields_query = $dbh->prepare("
	SELECT *
	FROM
		`required_fields`
	WHERE `deleted` = '0';
");

$required_fields_query->execute();
$required_fields = $required_fields_query->fetchAll();

$query = $dbh->prepare(
    "
			SELECT
			*
			FROM
			`app_options`
			WHERE `app_options`.`key` = 'show_site' AND `app_options`.`value` = 1;
			"
);

$query->execute();
$showSite = $query->rowCount();


$query = $dbh->prepare(
    "
  	SELECT
  	*
  	FROM
  	`app_options`
  	WHERE
  	`app_options`.`key` = 'purpose_funds_required'
  "
);
$query->execute();
$purposeFundsRequired = $query->fetch();

//default donation
$query = $dbh->prepare(
    "
 			SELECT
 			*
 			FROM
 			`app_options`
 			WHERE
 		`app_options`.`key` = 'default_suggested_donation'
 		"
);
$query->execute();
$defaultDonation = $query->fetch();

$query = $dbh->prepare(
    "
  	SELECT
  	*
  	FROM
  	`app_options`
  	WHERE
  	`app_options`.`key` = 'reference_required'
  "
);
$query->execute();
$reference = $query->fetch();


$query = $dbh->prepare(
    "
  	SELECT
  	*
  	FROM
  	`app_options`
  	WHERE
  	`app_options`.`key` = 'default_path_photo'
  "
);
$query->execute();
$defaultPathPhoto = $query->fetch();


// by default,  to prevent app to be broken value is set to 'uploads'/
$defaultPathPhoto = ($defaultPathPhoto['value'] == '') ? "uploads/" : $defaultPathPhoto['value'];

$query = $dbh->prepare(
    "
  	SELECT
  	*
  	FROM
  	`app_options`
  	WHERE
  	`app_options`.`key` = 'default_path_sign'
  "
);
$query->execute();
$defaultPathSign = $query->fetch();
$defaultPathSign = ($defaultPathSign['value'] == '') ? "uploads/" : $defaultPathSign['value'];

function findAssignment($requiredFields, $tab, $assignment)
{

    $isRequired = false;
    foreach ($requiredFields as $row) {
        if ($row['tab'] == $tab
            && $row['assignment'] == $assignment
            && $row['active'] == 1
        ) {
            $isRequired = true;
            break;
        }
    }
    return $isRequired;
}

if ($_SESSION['user'] == '') {
    header('Location: emvs.php?action=index');
}


//Default Site
$muser = $_SESSION['user'];
$managersql = $dbh->prepare("
		SELECT *
		FROM newmember
		WHERE emailid ='$muser' && `Delete` ='0'
	");
$managersql->execute();
$managerdata = $managersql->fetch();

$site = $managerdata['Sites'];
$sql = $dbh->prepare("SELECT * FROM `sites` WHERE `Id`='$site' AND `Delete` = '0' ; ");
$sql->execute();
$site = $sql->fetch();
$optionDefaultSite = $site['Sites'];

if ($managerdata['roles'] == 'Manager' && $optionDefaultSite) {
    $managerSiteError = false;
} elseif ($managerdata['roles'] == 'Administrator' && $optionDefaultSite) {
    $administratorSiteError = false;
} else {
    $query = $dbh->prepare(
        "SELECT
			*
			FROM
			`app_options`
			WHERE
			`app_options`.`key` = 'default_site'
		"
    );
    $query->execute();
    $option_site = $query->fetch();
    $optionDefaultSite = $option_site['value'];
}

if (!$optionDefaultSite && ($managerdata['roles'] == 'Manager')) {
    $managerSiteError = true;
} elseif (!$optionDefaultSite && ($managerdata['roles'] == 'Administrator')) {
    $administratorSiteError = true;
}

$sqlrow = $dbh->prepare('select *
                         from `visitstable`
                         where `collectorsid` = :collid
                         and `visitid` = :visitid ;
				');
$sqlrow->execute([
    'collid' => $a,
    'visitid' => $b
]);
$datarow = $sqlrow->fetch();
/******Visits Next ******/
$memberid = $_POST['collmemid'];
$visitsid = $_POST['visitsid'];
$visit = $_POST['visit'];
$sites = $_POST['sites'];
$regdate = $_POST['regdate'];
$authfrom = $_POST['authfrom'];
$authto = $_POST['authto'];
$visitdays = $_POST['visitdays'];
$visitmanager = $_POST['visitmanager'];

$siteshost = $_POST['siteshost'];
$sitesdriver = $_POST['sitesdriver'];
$refid = $_POST['refid'];
if (isset($_POST['tempadd'])) {
    $memberid = $_POST['memberid'];
    $visitid = $_POST['visitid'];
    $personalinfonext = 'personalinfonext';
    $sqlquery = $dbh->prepare("select * from collectors where Id ='$memberid'");
    $sqlquery->execute();
    $datarow1 = $sqlquery->fetch();

}
if (isset($_POST['baltiadd'])) {

    $memberid = $_POST['memberid'];
    $visitid = $_POST['visitid'];
    $sqlquery = $dbh->prepare("select * from visitstable where collectorsid ='$memberid' and visitid='$visitid'");
    $sqlquery->execute();
    $datarow1 = $sqlquery->fetch();
    $tempaddress1 = $_POST['tempaddress1'];

    $tempcountry = $_POST['tempcountry'];
    $tempaddress2 = $_POST['tempaddress2'];
    $tempzipcode = $_POST['tempzipcode'];
    $tempcity = $_POST['tempcity'];
    $tempphone = $_POST['tempphone'];
    $tempstate = $_POST['tempstate'];
    $tempcomments = $_POST['tempcomments'];
    $sql = $dbh->exec("UPDATE visitstable
SET usaddress1='$tempaddress1',usaddress2='$tempaddress2',uscountry='$tempcountry',uszipcode='$tempzipcode',zipcity='$tempcity',zipphone='$tempphone',zipstate='$tempstate',zipcomments='$tempcomments' where collectorsid ='$memberid' and visitid='$visitid'");
    $personalinfonext = 'personalinfonext';
    $sqlquery = $dbh->prepare("select * from visitstable  where collectorsid ='$memberid' and visitid='$visitid'");
    $sqlquery->execute();
    $datarow2 = $sqlquery->fetch();

}
if (isset($_POST['update'])) {
    $title = addslashes($_POST['title']);
    $address1 = $_POST['address1'];
    $firstname = $_POST['firstname'];
    $address2 = $_POST['address2'];
    $middlename = $_POST['middlename'];
    $city = $_POST['city'];
    $lastname = $_POST['lastname'];
    $state = $_POST['state'];
    $phone = $_POST['phone'];
    $zipcode = $_POST['zipcode'];
    $ssnid = $_POST['ssnid'];
    $dob = $_POST['dob'];
    $gender = 'm';
    $memberids = $_POST['memberids'];
    $visitid = $_POST['visitid'];
    $personinfohide = $_POST['personinfohide'];
    $sql = $dbh->exec("UPDATE collectors SET title='$title',firstname='$firstname',middlename='$middlename',lastname='$lastname',city='$city',statecountry='$state',country='$country',zipcode='$zipcode',address1='$address1',address2='$address2',phoneno='$phone',ssnid='$ssnid',gender='$gender',dob='$dob',Updateddate=now() WHERE Id=$abc");
}
$sql = $dbh->prepare("select * from collectors order by Id desc");
$sql->execute();
$data = $sql->fetch();
$memberid1 = $data['Id'];

$sql = $dbh->prepare("select * from `collectors` where `Id` = '$a'");
$sql->execute();
$dataquery11 = $sql->fetch();
$a = $dataquery11['Id'];
$dataquery11['gender'];


$title = addslashes($_POST['title']);
$address1 = $_POST['address1'];
$firstname = $_POST['firstname'];
$address2 = $_POST['address2'];
$middlename = $_POST['middlename'];
$city = $_POST['city'];
$lastname = $_POST['lastname'];
$state = $_POST['state'];
$phone = $_POST['phone'];
$zipcode = $_POST['zipcode'];
$ssnid = $_POST['ssnid'];
$dob = $_POST['dob'];
$gender = $_POST['gender'];
$memberids = $_POST['memberids'];
if (isset($_POST['prev1'])) {
    $memberid = $_POST['memberid'];
    $sqlquery = $dbh->prepare("select * from collectors where Id = '$memberid'");
    $sqlquery->execute();
    $dataquery12 = $sqlquery->fetch();
    $visistprev = 'visistprev';

}
if (isset($_POST['action']) && ($_POST['action'] == "delete")) {
    $selsql = $dbh->prepare("select * from collectorphone where Id='" . $_POST['act'] . "'");
    $selsql->execute();
    $deldata = $selsql->fetch();
    $visitsnext = 'visitsnext';
    if ($deldata['CPrimary'] == '') {
        $delquery1 = $dbh->prepare('UPDATE `collectorphone`
                                    SET `delete` = 1
                                    WHERE `Id` = :id ;');
        $delquery1->execute(['id' => $_POST['act']]);
    } else {
        echo '<script>alert("Primary Phone Number Can`t Delete")</script>';
    }
}
if (isset($_POST['action']) && ($_POST['action'] == "removeIdent")) {
    $sql = $dbh->prepare("select * from collident where Collid='" . $a . "' AND `delete` = 0");
    $sql->execute();
    $deldata = $sql->rowCount();
    $visitsnext = 'visitsnext';

    $delquery1 = $dbh->prepare("update collident set `delete` = 1 where Id='" . $_POST['act'] . "'");
    $delquery1->execute();
}

$sql = $dbh->prepare("select * from `collectors` where `Id` = '$a' ;");
$sql->execute();
$dataCollector = $sql->fetch();
$regdate = $data['Id'];

$sql = $dbh->prepare('SELECT *
                      FROM `visitstable`
                      WHERE `collectorsid` = :id
                      ORDER BY `visitid`
                      DESC; ');
$sql->execute(['id' => $a]);
$lastVisitData = $sql->fetch();

$sql = $dbh->prepare('SELECT `Id`
                      FROM `visitstable`
                      WHERE `collectorsid` = :id
                      ORDER BY `visitid`
                      DESC; ');
$sql->execute(['id' => $a]);

if ($lastVisitData['authofromdate'] && count($sql->fetchAll()) > 1) {
    $lastVisit = date_create($lastVisitData['authofromdate']);
    $lastVisit = date_format($lastVisit, 'm/d/y');
} else {
    $lastVisit = '';
}

if (strlen($dataCollector['dob']) > 4) {
    $parts = explode('-', $dataCollector['dob']);
    $year = $parts[2] ? $parts[2] : $parts[1];

    if (preg_match('/[a-zA-Z]/', $parts[0])) {
        $date = date_create($dataCollector['dob']);
        $date = date_format($date, 'm/d/y');
        $newParts = explode('/', $date);

        $date = $newParts[0] . '/' . $newParts[1] . '/' . $year[2] . $year[3];
    } else {
        if (strlen($parts[0]) == 1) {
            $parts[0] = '0' . $parts[0];
        }

        if (strlen($parts[1]) == 1) {
            $parts[1] = '0' . $parts[1];
        }

        $date = $parts[0] . '/' . $parts[1] . '/' . $year[2] . $year[3];
    }

} else {
    $date = $dataCollector['dob'];
}

$pagename = $dataCollector['title'] . ' ' .
    $dataCollector['firstname'] . ' ' .
    $dataCollector['lastname'] .
    '&nbsp;&nbsp; Birthdate: ' .
    $date . '&nbsp;&nbsp; Last Visit: ' .
    $lastVisit;


include 'includes/header.php';
?>

<!-- popup window-->
<script type="text/javascript" src="js/slimbox2.js"></script>
<link rel="stylesheet" href="css/slimbox2.css" type="text/css" media="screen"/>
<!--Select box validation-->
<script src="js/jquery-2.1.0.min.js"></script>
<script src="js/jquery.validation.js"></script>
<link rel="stylesheet" href="css/jquery.validation.css">
<!-- End Validation-->

<script type="text/javascript">
    function show1() {
        document.getElementById('div1').style.display = 'none';
        document.getElementById('div2').style.display = 'block';
    }
    function show2() {
        document.getElementById('div1').style.display = 'block';
        document.getElementById('div2').style.display = 'none';
    }
    function newid(a, b) {
        document.getElementById('act').value = b;
        document.getElementById('action').value = "collupdate";
        document.getElementById('hid').value = a;
        //document.novalidate.submit();
    }
    function newid1(a, b) {
        document.getElementById('act').value = b;
        document.getElementById('action').value = "collphoneupdate";
        document.getElementById('hid').value = a;
        //document.novalidate.submit();
    }
</script>
<link href="datepicker/jquery-ui.css" rel="Stylesheet" type="text/css"/>
<script type="text/javascript" src="datepicker/jquery-ui.js"></script>
<script>
    $(document).ready(function () {
        var pageName = $('#page-name').val();
        $('.pagename').html("<span>Visit Edit : </span><span class='two'> " + pageName + "</span>");

        //setting passport name label for identification details
        $("select[name='type']").each(function (index, element) {
            if ($(this).val() == 'Passport') {
                $('#passport-name-label').html('Passport');
            }
        });

        $("#txtdate").datepicker({
            minDate: 0,
            changeMonth: true,
            changeYear: true,

            onChangeMonthYear: function (year, month) {
                $("#startYear").val(year);
                $("#startMonth").val(month);
            }
        });
    });
    $(".ui-datepicker-month").prepend("<option value='' selected='selected'>Month</option>");

    $(".ui-datepicker-year").prepend("<option value='' selected='selected'>Year</option>");
</script>

<script type="text/javascript">
    setInterval(function () {
        if (document.getElementById('alertComment')) {
            var alert = document.getElementById('alertComment');
            if (alert.style.color == 'rgb(132, 147, 168)') {
                alert.style.color = '#fff';
            } else {
                alert.style.color = 'rgb(132, 147, 168)';
            }
        }
    }, 600);
</script>
<style type="text/css">
    /*this css is used for align checkboxes*/
    input[type=checkbox] {
        margin: 0;
    }

    input[type=checkbox] + label {
        padding: 0px 10px 10px 0px;
    }
</style>
<style>
    .show {
        display: visible;
    }

    .hide {
        display: none;
    }
</style>
<script language="javascript" type="text/javascript">
    function blockSpecialCharNum(e) {
        var k = e.keyCode;
        return (k == 8 || (k >= 48 && k <= 57) || k == 40 || k == 41 || k == 45 || k == 43);
    }

    function showHide(shID) {
        //alert(shID);
        if (document.getElementById(shID)) {
            if (document.getElementById(shID + '-show').style.display != 'show') {
                document.getElementById(shID + '-show').style.display = 'show';
                document.getElementById(shID).style.display = 'block';
            }
            else {
                document.getElementById(shID + '-show').style.display = 'inline';
                document.getElementById(shID).style.display = 'none';
            }
        }
    }
    function showHide1(shID1) {
        //alert(shID1);
        if (document.getElementById(shID1 + '-show').style.display != 'none') {
            document.getElementById(shID1 + '-show').style.display = 'none';
            document.getElementById(shID1).style.display = 'none';
        }

    }
    $(document).ready(function () {
        $("#txtdate1").datepicker({
            maxDate: 0,
            changeMonth: true,
            changeYear: true,

            onChangeMonthYear: function (year, month) {
                $("#startYear").val(year);
                $("#startMonth").val(month);
            }
        });
    });
    $(".ui-datepicker-month").prepend("<option value='' selected='selected'>Month</option>");

    $(".ui-datepicker-year").prepend("<option value='' selected='selected'>Year</option>");
</script>
<script type="text/javascript">
    function myfunction(a, b) {
        var dataString = 'edit=' + a + '&editcoll=' + b;
        $.ajax({
            type: "POST",
            url: "editprocess.php",
            data: dataString,
            success: function (msg) {
                $('#result').html(msg);
            }
        });
        //return false;
    }
    function usadd(a, b) {
        var dataString = 'usaddress1=' + a + '&visitid=' + b;
        //alert(dataString);

    }
</script>
<script language="JavaScript">
    function setVisibility(id, visi) {
        document.getElementById(id).style.visibility = visi;
    }
</script>
<script type="text/javascript">

    function other1(x) {
        alert(x);
        x.className = (x.className == "first") ? "second" : (x.className == "second") ? "first" : "second";
    }
</script>
<style>
    .first {
        display: none;
    }

    .second {
        display: show;
    }

    #tooltip {
        text-align: center;
        color: #fff;
        background: #111;
        position: absolute;
        z-index: 100;
        padding: 15px;
    }

    #tooltip:after /* triangle decoration */
    {
        width: 0;
        height: 0;
        border-left: 10px solid transparent;
        border-right: 10px solid transparent;
        border-top: 10px solid #111;
        content: '';
        position: absolute;
        left: 50%;
        bottom: -10px;
        margin-left: -10px;
    }

    #tooltip.top:after {
        border-top-color: transparent;
        border-bottom: 10px solid #111;
        top: -20px;
        bottom: auto;
    }

    #tooltip.left:after {
        left: 10px;
        margin: 0;
    }

    #tooltip.right:after {
        right: 10px;
        left: auto;
        margin: 0;
    }
</style>
<script>
    $(function () {
        var targets = $('[rel~=tooltip]'),
            target = false,
            tooltip = false,
            title = false;

        targets.bind('mouseenter', function () {
            target = $(this);
            tip = target.attr('title');
            tooltip = $('<div id="tooltip"></div>');

            if (!tip || tip == '')
                return false;

            target.removeAttr('title');
            tooltip.css('opacity', 0)
                .html(tip)
                .appendTo('body');

            var init_tooltip = function () {
                if ($(window).width() < tooltip.outerWidth() * 1.5)
                    tooltip.css('max-width', $(window).width() / 2);
                else
                    tooltip.css('max-width', 340);

                var pos_left = target.offset().left + ( target.outerWidth() / 2 ) - ( tooltip.outerWidth() / 2 ),
                    pos_top = target.offset().top - tooltip.outerHeight() - 20;

                if (pos_left < 0) {
                    pos_left = target.offset().left + target.outerWidth() / 2 - 20;
                    tooltip.addClass('left');
                }
                else
                    tooltip.removeClass('left');

                if (pos_left + tooltip.outerWidth() > $(window).width()) {
                    pos_left = target.offset().left - tooltip.outerWidth() + target.outerWidth() / 2 + 20;
                    tooltip.addClass('right');
                }
                else
                    tooltip.removeClass('right');

                if (pos_top < 0) {
                    var pos_top = target.offset().top + target.outerHeight();
                    tooltip.addClass('top');
                }
                else
                    tooltip.removeClass('top');

                tooltip.css({left: pos_left, top: pos_top})
                    .animate({top: '+=10', opacity: 1}, 50);
            };

            init_tooltip();
            $(window).resize(init_tooltip);

            var remove_tooltip = function () {
                tooltip.animate({top: '-=10', opacity: 0}, 50, function () {
                    $(this).remove();
                });

                target.attr('title', tip);
            };

            target.bind('mouseleave', remove_tooltip);
            tooltip.bind('click', remove_tooltip);
        });
    });
</script>
<script type="text/javascript">
    function toggleClass() {
        alert('Please create your visits');
    }
</script>
<style type="text/css">
    /* padding-bottom and top for image */
    .mfp-no-margins img.mfp-img {
        padding: 0;
    }

    /* position of shadow behind the image */
    .mfp-no-margins .mfp-figure:after {
        top: 0;
        bottom: 0;
    }

    /* padding for main container */
    .mfp-no-margins .mfp-container {
        padding: 0;
    }

    .rotated {
        -webkit-transform: rotate(180deg);
        -moz-transform: rotate(180deg);
        -o-transform: rotate(180deg);
        -ms-transform: rotate(180deg);
        transform: rotate(180deg);
    }

    .cross {
        position: absolute;
        left: 220px;
        top: 112px;
        z-index: 1;
    }

    .rotate {
        background-size: 100%;
        background-repeat: no-repeat;
        position: absolute;
        left: 220px;
        top: 0px;
        z-index: 1;
    }

    .doc-wrapper {
        position: relative;
        padding-bottom: 20px;
    }

    #coll-documents-galery {
        display: flex;
        flex-wrap: wrap;
        justify-content: space-around;
        align-content: flex-start;
        align-items: stretch;
        clear: both;
        padding-top: 20px;
        width: 100%;
    }

    .step_box {
        border: 1px solid rgb(247, 247, 247);
    }

    .step_box:hover, #selected_step_box, .QuickStartLong:hover {
        background: rgb(247, 247, 247);
    }

    .selected {
        background-color: #ddd;
    }
</style>

<script type="text/javascript">
    $(document).ready(function () {
        $('.step_wrapper').on('click', '.step_box', function () {
            $('.step_box').removeClass('selected');
            $(this).addClass('selected')
        });
    });
</script>
<style type="text/css">
    .Lime {
        background-color: Lime;
        border: 1px solid black;
        padding: 10px;
    }
</style>
<script type="text/javascript">
    function Change_Class_Name(My_Element, My_Class) {
        My_Element.className = My_Class;
        setTimeout(2000);
    }
</script>
<script>
    //When DOM loaded we attach click event to button
    $(document).ready(function () {

        //attach keypress to input
        $('.input').keydown(function (event) {
            // Allow special chars + arrows
            if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9
                || event.keyCode == 27 || event.keyCode == 13
                || (event.keyCode == 65 && event.ctrlKey === true)
                || (event.keyCode >= 35 && event.keyCode <= 39)) {

            } else {
                // If it's not a number stop the keypress
                if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                    event.preventDefault();
                }
            }
        });

    });

</script>

<script>
    var popupWindow = null;
    function signadd(val1, val2) {
        popupWindow = window.open("visitssignature.php?a=" + val1 + '&b=' + val2, "_blank", "toolbar=yes, scrollbars=yes, resizable=yes, top=235, left=550, width=500, height=200");
        popupWindow.focus();
        document.onmousedown = focusPopup;
        document.onkeyup = focusPopup;
        document.onmousemove = focusPopup;
    }
    function parent_disable() {
        if (popupWindow && !popupWindow.closed)
            popupWindow.focus();
    }
</script>
<!----------- Add more JS------------>
<script src="js/jquery.maskedinput.js" type="text/javascript"></script>
<script type="text/javascript">
    //Phone Number format
    jQuery(function ($) {
        $('#phone0').simpleMask({'mask': 'usa', 'nextInput': false});
        $('#tempphone').simpleMask({'mask': 'usa', 'nextInput': false});

        var baltiPhone = $('#baltiphone');

        if (!/[a-zA-Z]/.test(baltiPhone.val())) {
            baltiPhone.simpleMask({'mask': 'usa', 'nextInput': false});
        }

        $('#ssn0').simpleMask({'mask': 'ssn', 'nextInput': false});
    });
    //Check the value set in phone number
    function chkAllVal() {
        var pno = document.getElementsByName("phoneno[]");
        for (var i = 0; i < pno.length; i++) {
            var id = document.getElementById("phone" + i);
            var val = id.value;
            var actVal = val.replace("_", "");
            if (actVal.length < 14) {
                document.getElementById("phone" + i).focus();
                return false;
            }
        }
    }
    //********Remove Phone Number using AJAX Page
    function hello1(a) {
        alert(a);
        $("#remove" + a).remove();
        a--;
    }
    function hellomem(a) {
        $("#remove" + a).remove();
        a--;
    }
    //********Add Phone Number using AJAX Page
    function hello() {
        //alert("Hello there!");
        countClicks++;
        var visitadd = 'visitadd';
        document.getElementById('phoneid').value = countClicks;
        var dataString = 'countClicks=' + countClicks + '&visitadd=' + visitadd;
        $.ajax({
            type: "POST",
            url: "ajax_addmore_member.php",
            data: dataString,
            success: function (msg) {
                //$('#addmore').html(msg);
                var strVal = msg.split("$$$$$");
                var strDiv = document.getElementById("addmore").outerHTML;
                document.getElementById("addmore").outerHTML = msg + strDiv;
                $("#phone" + countClicks).mask("(999) 999-9999");
            }
        });
    }
    var countClicks = 0;
    //Remove Identification Number using AJAX Page
    function helloidenti1(a) {
        $("#remove1" + a).remove();
        a--;
    }
    //Add Identification Number using AJAX Page
    function helloidenti() {
        countClicks1++;
        document.getElementById('identiid').value = countClicks1;
        var dataString = 'countClicks1=' + countClicks1;
        $.ajax({
            type: "POST",
            url: "ajax_addmore_memberidenti.php",
            data: dataString,
            success: function (msg) {
                //$('#addmore').html(msg);
                var strVal = msg.split("$$$$$");
                var strDiv = document.getElementById("addmore1").outerHTML;
                document.getElementById("addmore1").outerHTML = msg + strDiv;
                $("#ssn" + countClicks1).mask("999-99-9999");
            }
        });
    }
    var countClicks1 = 0;

    //Check SSN Number Value set
    function Chkval() {
        var pno = document.getElementsByName("Collphone");

        for (var i = 0; i < pno.length; i++) {
            var id = document.getElementById("ssn" + i);
            var val = id.value;
            var actVal = val.replace("_", "");
            if (actVal.length < 14) {
                document.getElementById("ssn" + i).focus();
                return false;
            }
        }
    }
    function baltimoreadd(val1, val2) {
        var usaddress1 = document.getElementById('tempaddress1').value;
        var usaddress2 = document.getElementById('tempaddress2').value;
        var uscity = document.getElementById('tempcity').value;
        var usstate = document.getElementById('tempstate').value;
        var uscountry = document.getElementById('tempcountry').value;
        var uszipcode = document.getElementById('tempzipcode').value;
        var usphone = document.getElementById('tempphone').value;
        var uscomments = document.getElementById('tempcomments').value;

        var dataString = 'memberid=' + val1 + '&visitid=' + val2 + '&usaddress1=' + usaddress1 + '&usaddress2=' + usaddress2 + '&uscity=' + uscity + '&usstate=' + usstate + '&uscountry=' + uscountry + '&uszipcode=' + uszipcode + '&usphone=' + usphone + '&uscomments=' + uscomments;
        $.ajax({
            type: "POST",
            url: "ajax_local_address.php",
            data: dataString,
            success: function (msg) {
                $('#localaddress').html(msg);
            }
        });
    }
</script>


<script type="text/javascript">
    function myfun() {
        var sitesvisid = document.getElementById("sitesvisid").value;
        var sitesmemid = document.getElementById("sitesmemid").value;

        var dataString = 'id=' + a + '&sitesmemid=' + sitesmemid + '&sitesvisid=' + sitesvisid;
        $.ajax({
            type: "POST",
            url: "ajax_sites.php",
            data: dataString,
            success: function (msg) {
                $('#sites1').html(msg);
            }
        });
    }
    function toggleClass() {
        alert('Please create your visits');
    }
</script>

<script>
    $(document).ready(function () {
        var x = document.getElementById("Other").checked;
        var y = document.getElementById("Other1").checked;
        if (x == true) {
            document.getElementById("target").style.visibility = "visible";
        }
        if (y == true) {
            document.getElementById("target1").style.visibility = "visible";
        }

        $('#collident').on('focusout change', 'input, select', function () {
            var valueElement = $(this).val();  //val element onfocusout
            var thisName = $(this).attr('name'); // collum this BD
            var idRow = $(this).closest('div[data-id]').data('id');  //row ID DB
            var visitid = $('#the-visit-id').val();

            var dataString = 'valueElement=' + valueElement +
                '&a=' + <?=$a?> +
                    '&b=' + <?=$b?> +
                    '&name=' + thisName +
                '&idRow=' + idRow +
                '&thisTable=visit_details' +
                '&visitid=' + visitid;
            $.ajax({
                type: "POST",
                url: "ajax_saveFocusout.php",
                data: dataString
            });
        });
        $('#visits').on('change focusout', 'input, select, textarea', function () {
            var dataString = '';
            var valueElement = $(this).val();  //val element onfocusout
            var thisName = $(this).attr('name'); // collum this BD
            dataString += 'valueElement=' + valueElement + '&a=' + <?= $a ?> +'&b=' + <?= $b ?> +'&name=' + thisName + '&thisTable=visitstable';
            $.ajax({
                type: "POST",
                url: "ajax_saveFocusout.php",
                data: dataString
            });
        });

        function updateVisitTabValues(_this){
            var dataString = '';
            var valueElement = _this.val();  //val element onfocusout
            var thisName = _this.attr('name'); // collum this BD
            dataString += 'valueElement=' + valueElement + '&a=' + <?=$a?> +'&b=' + <?=$b?> +'&name=' + thisName + '&thisTable=visitstable';
            setTimeout(function () {
                if (thisName == 'authofromdate' && ($('#dateerrormsg1').html() != '')) {
                } else if (thisName == 'authofromdate' && ($('#dateerrormsg1').html() == '')) {
                    $.ajax({
                        type: "POST",
                        url: "ajax_saveFocusout.php",
                        data: dataString,
                        success: function (msg) {
                        }
                    });
                    var data = '';
                    var value = $("input[name='authto']").val();  //val element onfocusout
                    var name = 'authotodate'; // collum this BD
                    data += 'valueElement=' + value + '&a=' + <?=$a?> +'&b=' + <?=$b?> +'&name=' + name + '&thisTable=visitstable';
                    $.ajax({
                        type: "POST",
                        url: "ajax_saveFocusout.php",
                        data: data,
                        success: function (msg) {
                        }
                    });
                } else {
                    $.ajax({
                        type: "POST",
                        url: "ajax_saveFocusout.php",
                        data: dataString,
                        success: function (msg) {
                            if (thisName == 'visitid'
                                && !isNaN(valueElement)
                                && valueElement) {
                                document.location.href =
                                    "emvs.php?action=editvisit1&&a=" + <?=$a?> +"&&b=" + valueElement;
                            } else if (thisName == 'last-refid'
                                && !isNaN(valueElement)
                                && msg == 'saved') {
                                $('input[name="last-refid"]').attr('lang', valueElement);
                            }
                        }
                    });
                }
            }, 1000);
        }

        $('#tab2-4').on('change', 'input:not("#refid"), select, textarea', function () {
            updateVisitTabValues($(this));
        });

        var delayRefIdTimeout;
        $('#refid').on('keyup', function(){
            var _this = $(this);
            clearTimeout(delayRefIdTimeout);
            delayRefIdTimeout = setTimeout(function(){
                updateVisitTabValues(_this);
            }, 900);

        });
        $('#tab3-4').on('change', 'input, select, textarea', function () {
            var dataString = '';
            var valueElement = $(this).val();  //val element onfocusout
            var thisName = $(this).attr('name'); // collum this BD
            dataString += 'valueElement=' + valueElement + '&a=' + <?=$a?> +'&b=' + <?=$b?> +'&name=' + thisName + '&thisTable=visitstable';
            $.ajax({
                type: "POST",
                url: "ajax_saveFocusout.php",
                data: dataString
            });
        });
        $('#tab4-4').on('change', 'input, select, textarea', function () {
            var dataString = '';
            var valueElement = $(this).val();  //val element onfocusout
            var thisName = $(this).attr('name'); // collum this BD
            dataString += 'valueElement=' + valueElement + '&a=' + <?=$a?> +'&b=' + <?=$b?> +'&name=' + thisName + '&thisTable=visitstable';
            $.ajax({
                type: "POST",
                url: "ajax_saveFocusout.php",
                data: dataString,
                success: function (msg) {
                }
            });
        });
        $('#tab5-4').on('change focusout', 'input, select, textarea', function () {
            var dataString = '';
            var str = '';
            $('input[name="active[]"]:checked').each(function () {
                str += $(this).val() + ',';
            });
            var valueElement = $(this).val();  //val element onfocusout
            var thisName = $(this).attr('name'); // collum this BD
            if (thisName == 'active[]') {
                thisName = 'purpose';
                valueElement = str;
            }
            dataString += 'valueElement=' + valueElement + '&a=' + <?=$a?> +'&b=' + <?=$b?> +'&name=' + thisName + '&thisTable=visitstable';
            $.ajax({
                type: "POST",
                url: "ajax_saveFocusout.php",
                data: dataString,
                success: function (msg) {

                }
            });
        });
        $('#tab6-4').on('change', 'input, select, textarea', function () {
            var dataString = '';
            var str1 = '';
            $('input[name="active1[]"]:checked').each(function () {
                str1 += $(this).val() + ',';
            });
            var valueElement = $(this).val();  //val element onfocusout
            var thisName = $(this).attr('name'); // collum this BD
            if (thisName == 'active1[]') {
                thisName = 'fundsreference';
                valueElement = str1;
            }
            dataString += 'valueElement=' + valueElement + '&a=' + <?=$a?> +'&b=' + <?=$b?> +'&name=' + thisName + '&thisTable=visitstable';
            $.ajax({
                type: "POST",
                url: "ajax_saveFocusout.php",
                data: dataString,
                success: function (msg) {

                }
            });
        });
        $('#contact').on('change focusout', 'input, select', function () {
            var valueElement = $(this).val();  //val element onfocusout
            var thisName = $(this).attr('name'); // collum this BD
            var idRow = $(this).closest('div[data-id]').data('id');  //row ID DB
            var visitid = $('#the-visit-id').val();

            var dataString = 'valueElement=' + valueElement +
                '&a=' + <?=$a?> +
                    '&b=' + <?=$b?> +
                    '&name=' + thisName +
                '&thisTable=visit_details' +
                '&idRow=' + idRow +
                '&visitid=' + visitid;
            $.ajax({
                type: "POST",
                url: "ajax_saveFocusout.php",
                data: dataString,
                success: function (msg) {
                }
            });
        });
    });

</script>
<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script>
    setTimeout(function () {
        validatePost();
    }, 0);

    function validatePost() {
        $.post("check.php", {allowedFields: "allowed"}, function (data) {
            validateFunction(data);
        });
    }

    function validateFunction(data) {
        var customedObj = {};
        customed = {};
        customedObj = jQuery.parseJSON(data);

        for (var i = 0; i < customedObj.length; i++) {
            field = customedObj[i].assignment;
            customed[field] = {required: false};
        }

        customed['active[]'] = {required: false};
        setTimeout(function () {
            $("#valWizard").validate({
                rules: customed,
                errorClass: "override-error"
            });
        }, 0);

    }
    $(function () {
        $('#submitchk').on('click', function () {
            if ($('input[name="Primary1"]:checked').val()) {
                return true;
            } else {
                alert('Select at least one field for primary "Identification Details"');
                return false;
            }

        });
        $('#Cntsubmitchk').on('click', function () {
            if ($('input[name="Primary1"]:checked').val()) {
                return true;
            } else {
                alert('Select at least one field for primary "Identification Details"');
                return false;
            }

        });

    });

    function myfunval() {
        var a = document.getElementById("authfrom").value;
        var b = document.getElementById("visitdays").value;
        var c = document.getElementById("sitesmemid").value;
        var d = document.getElementById("sitesvisid").value;
        var dataString = 'date=' + a + '&visitdays=' + b + '&member=' + c + '&visit=' + d;
        //alert(dataString);
        $.ajax({
            type: "POST",
            url: "ajax_date.php",
            data: dataString,
            success: function (msg) {
                $('#todate').html(msg);

                var x = document.getElementById("dateerrormsg").value;
                x = x.split(/[0-9]+\)/);

                var messages = '';
                var i = 1;
                x.forEach(function (item, i, arr) {
                    if (item != '') {
                        messages += i + ') ' + item + '<br>';
                        i++;
                    }
                });
                document.getElementById("dateerrormsg1").innerHTML = messages;
            }
        });
    }

    $(document).ready(function () {
        $('select[name="type"]').on('change', function () {
            $(this).closest('#addinput').find('input[name="country"]').focus();
        });

        $("#regdate").datepicker({
            changeMonth: true,
            changeYear: true,
            onChangeMonthYear: function (year, month) {

            }
        });

        $("#authfrom").datepicker({
            changeMonth: true,
            changeYear: true,

            onChangeMonthYear: function (year, month) {

            }
        });

        $("#last-visit-date").datepicker({
            changeMonth: true,
            changeYear: true,
            defaultDate: "-1y",
            showButtonPanel: true,
            beforeShow: function (input) {
                setTimeout(function () {
                    var buttonPane = $(input)
                        .datepicker("widget")
                        .find(".ui-datepicker-buttonpane").html('');

                    var btn = $('<button class="ui-datepicker-current ui-state-default ui-priority-secondary ui-corner-all" type="button">Clear</button>');
                    btn.unbind("click").bind("click", function () {
                        $.datepicker._clearDate(input);
                    });

                    btn.appendTo(buttonPane);

                }, 1);
            }
        });
    });

</script>
<?php
$loginemail = $_SESSION['user'];

$loginsql = $dbh->prepare("SELECT * FROM `newmember` WHERE `mailid`='$loginemail' AND `Delete` = 0");
$loginsql->execute();
$logindata = $loginsql->fetch();
$r = $logindata['roles'];
?>
<body>
<section>
    <input autocorrect="off" type="hidden" id="page-name" value="<?= $pagename ?>"/>
    <div class="mainwrapper">
        <?php include 'includes/leftpanel.php'; ?>
        <div class="mainpanel">
            <div class="pageheader">
                <div class="media-body">
                    <div class="row">
                        <?php
                        $sqlid = $dbh->prepare("select Id,title, firstname,lastname,middlename, DATE_FORMAT(Addeddate,'%b-%d-%Y') Addeddate from collectors where Id='$a'");
                        $sqlid->execute();
                        $dataid = $sqlid->fetch();
                        ?>
                        <div class="col-sm-12" style=" font-size: 20px;">
                        </div>
                    </div>
                    <?php
                    $errmsg = implode(" ", $err);
                    ?>
                </div>
            </div>
            <!-- pageheader -->
            <div class="media" style="border:none;">
                <form enctype="multipart/form-data" method="post" id="valWizard" name="valWizard" accept-charset="utf-8"
                      class="panel-wizard" novalidate="novalidate">

                    <input autocorrect="off" name="requiredFunds" id="requiredFunds" type="hidden" value="<?php
                    if (isset($purposeFundsRequired)) {
                        echo $purposeFundsRequired['value'];
                    } else {
                        echo "true";
                    }

                    ?>"/>
                    <input autocorrect="off" name="reference" id="requiredReference" type="hidden" value="<?php
                    if (isset($reference)) {
                        echo $reference['value'];
                    } else {
                        echo "true";
                    }
                    ?>"/>

                    <input autocorrect="off" name="visitval" id="visitval" type="hidden">
                    <input autocorrect="off" name="action" id="action" type="hidden">
                    <input autocorrect="off" name="act" id="act" type="hidden">
                    <input autocorrect="off" name="hid" id="hid" type="hidden">

                    <ul class="nav nav-justified nav-wizard nav-disabled-click nav-pills">
                        <li id="visitsTab" style=" float: left; " class="active"><a href="#tab2-4" data-toggle="tab"
                                                                                    style="font-size:14px">Visits</a>
                        </li>
                        <li id="collectorDetailsTab" style=" float: left; "><a href="#tab1-4" data-toggle="tab"
                                                                               style="font-size:14px">Personal Info</a>
                        </li>
                        <li id="addressTab" style=" float: left; "><a href="#tab3-4" data-toggle="tab"
                                                                      style="font-size:14px">Address</a>
                        </li>
                        <li id="fundsTab" style=" float: left; "><a href="#tab4-4" data-toggle="tab"
                                                                    style="font-size:14px">Funds</a>
                        </li>
                        <li id="purposeTab" style=" float: left; "><a href="#tab5-4" data-toggle="tab"
                                                                      style="font-size:14px">Purpose</a>
                        </li>
                        <li id="referenceTab" style=" float: left; "><a href="#tab6-4" data-toggle="tab"
                                                                        style="font-size:14px">Reference</a>
                        </li>
                        <li style=" float: left; "><a href="#tab7-4" data-toggle="tab" style="font-size:14px">Sign |
                                Print</a>
                        </li>
                        <li style=" float: left; " id="visitsTab"><a href="#tab8-4" data-toggle="tab"
                                                                     style="font-size:14px">Visit History</a>
                        </li>
                    </ul>

                    <div class="tab-content member margin">

                        <?php if ($datarow['personalcomments']): ?>
                            <script>
                                $(document).ready(function () {
                                    $('.alert-block').css('display', 'block');
                                });
                            </script>
                        <?php endif; ?>

                        <div class="alert-block">
                            <div class="row font-size">
                                <span id="alertComment" class="alert-comment">&nbsp;ALERT&nbsp;</span>&nbsp;-
                                <span><?= $datarow['personalcomments']; ?></span>
                            </div>
                        </div>

                        <div class="<?php if (($visistprev == 'visistprev') || ($personalinfonext == '')
                            && ($signnext == '')
                        ) {
                            echo 'tab-pane active';
                        } else {
                            echo 'tab-pane';
                        } ?>" id="tab2-4">

                            <div>
                                <input autocorrect="off" name="collmemid" type="hidden" value="<?= $a; ?>">
                                <input autocorrect="off" name="visitsid" type="hidden" value="<?= $b; ?>">
                                <input autocorrect="off" name="sitesmemid" id="sitesmemid" type="hidden"
                                       value="<?= $a; ?>">
                                <input autocorrect="off" name="sitesvisid" id="sitesvisid" type="hidden"
                                       value="<?= $b; ?>">
                                <?php
                                $sqlid = $dbh->prepare("select Id,title,firstname,lastname,middlename,DATE_FORMAT(Addeddate,'%b-%d-%Y') Addeddate from collectors where Id='$a'");
                                $sqlid->execute();
                                $dataid = $sqlid->fetch();
                                $sqlid1 = $dbh->prepare("select * from visitstable where collectorsid='$regdate' order by id desc");
                                $sqlid1->execute();
                                $dataid1 = $sqlid1->fetch();
                                $d = $dataid1['visitid'];
                                ?>
                            </div>

                            <div class="first-block">
                                <div class="panel-body personal-info">
                                    <div class="result" id="result">
                                        <div class="col-md-6">
                                            <label class="col-sm-3 control-label my-label"
                                                <?php
                                                if (findAssignment($required_fields, 'visits', 'visit')) {
                                                    echo " style='color:#428bca;' ";
                                                }

                                                ?>
                                            >Visit #</label>
                                            <div class="col-sm-7">
                                                <input autocorrect="off" type="text" name="visitid"
                                                       id="visitId"
                                                       value="<?= $b; ?>"
                                                       onkeypress="Maxval(this.value,7,this.id)"
                                                       class="form-control my-input"
                                                >
                                                <div id="countervisitId" class="my-input-margin"></div>
                                                <span id="visitIdErr"
                                                      style="color: red;"></span>
                                            </div>
                                            <label class="col-sm-3 control-label my-label"
                                                <?php
                                                if (findAssignment($required_fields, 'visits', 'refid')) {
                                                    echo " style='color:#428bca;' ";
                                                }

                                                ?>
                                            >Reference Id</label>
                                            <div class="col-sm-7">
                                                <input autocorrect="off" type="text"
                                                       name="refid"
                                                       id="refid"
                                                       onkeypress="Maxval(this.value,7,this.id)"
                                                       onKeyPress="return check(event,value)"
                                                       value="<?= $datarow['Refid']; ?>"
                                                       class="form-control my-input"
                                                       lang="<?= $datarow['Refid']; ?>"
                                                >
                                                <div id="counterrefid" class="my-input-margin"></div>
                                                <span class="ref-id-err"
                                                      style="color: red;"></span>
                                            </div>
                                            <?php if (!$showSite) { ?>
                                            <div
                                                style="display: none">
                                                <?php } else { ?>
                                                <div>
                                                    <?php } ?>
                                                    <label class="col-sm-3 control-label my-label"
                                                        <?php
                                                        if (findAssignment($required_fields, 'visits', 'sites')) {
                                                            echo " style='color:#428bca;' ";
                                                        }

                                                        ?>
                                                    >Site</label>
                                                    <div class="col-sm-7">
                                                        <?php
                                                        $muser = $_SESSION['user'];
                                                        $managersql = $dbh->prepare("
                                                                            SELECT *
                                                                            FROM newmember
                                                                            WHERE emailid ='$muser' && `Delete` ='0'
                                                                            ");
                                                        $managersql->execute();
                                                        $managerdata = $managersql->fetch();

                                                        $site = $managerdata['Sites'];
                                                        $sql = $dbh->prepare("SELECT * FROM `sites` WHERE `Id`='$site' AND `Delete` = '0' ; ");
                                                        $sql->execute();
                                                        $site = $sql->fetch();
                                                        $optionDefaultSite = $site['Sites'];

                                                        if ($managerdata['roles'] == 'Manager' && $optionDefaultSite) {
                                                            $managerSiteError = false;
                                                        } elseif ($managerdata['roles'] == 'Administrator' && $optionDefaultSite) {
                                                            $administratorSiteError = false;
                                                        } else {
                                                            $query = $dbh->prepare(
                                                                "              SELECT
                                                                                *
                                                                                FROM
                                                                                `app_options`
                                                                                WHERE
                                                                                `app_options`.`key` = 'default_site'
                                                                                "
                                                            );
                                                            $query->execute();
                                                            $option_site = $query->fetch();
                                                            $optionDefaultSite = $option_site['value'];
                                                        }
                                                        ?>
                                                        <input autocorrect="off"
                                                               class="form-control my-input"
                                                               type="text" name="sites"
                                                               value="<?php
                                                               if ($optionDefaultSite) {
                                                                   echo $optionDefaultSite;
                                                               } else {
                                                                   echo 'Choose One';
                                                               } ?>" readonly>
                                                    </div>
                                                </div><!-- form-group -->

                                                <label class="col-sm-3 control-label my-label"
                                                    <?php
                                                    if (findAssignment($required_fields, 'visits', 'regdate')) {
                                                        echo " style='color:#428bca;' ";
                                                    }

                                                    ?>
                                                >Registration Date</label>
                                                <div class="col-sm-7">
                                                    <input autocorrect="off" type="text"
                                                           id="regdate"
                                                           placeholder="mm/dd/yyyy"
                                                           readonly
                                                           value="<?= $datarow['regdate']; ?>"
                                                           name="regdate"
                                                           class="form-control my-input"
                                                    >
                                                </div>
                                                <label class="col-sm-3 control-label my-label"
                                                    <?php
                                                    if (findAssignment($required_fields, 'visits', 'authfrom')) {
                                                        echo " style='color:#428bca;' ";
                                                    }

                                                    ?>
                                                >Authorized Period From</label>
                                                <div class="col-sm-7">
                                                    <input autocorrect="off" readonly
                                                           id="authfrom"
                                                           onchange="myfunval()"
                                                           name="authofromdate"
                                                           class="form-control my-input"
                                                           value="<?php if ($datarow['authofromdate'] != '') {
                                                               echo $datarow['authofromdate'];
                                                           } else {
                                                               echo date('M-d-Y');
                                                           } ?>" type="text"
                                                    >
                                                    <div id="dateerrormsg1" class="dateerrormsg1"
                                                         style="width:100%;color: #FF0808;font-size: 12px; border:none;"></div>
                                                </div>
                                                <label class="col-sm-3 control-label my-label"
                                                    <?php
                                                    if (findAssignment($required_fields, 'visits', 'authfrom')) {
                                                        echo " style='color:#428bca;' ";
                                                    }

                                                    ?>
                                                >Authorized Period To</label>
                                                <div class="col-sm-7" id="todate">
                                                    <input autocorrect="off" type="text"
                                                           readonly
                                                           value="<?= $datarow['authotodate']; ?>"
                                                           name="authotodate"
                                                           class="form-control my-input"
                                                    >
                                                </div>
                                                <?php
                                                $sql = $dbh->prepare("SELECT * FROM `visitstable` WHERE `collectorsid` = '$a' AND `visitid` = '$b' - 1; ");
                                                $sql->execute();
                                                $lastVisitData = $sql->fetch();
                                                ?>
                                                <label class="col-sm-3 control-label my-label">Last
                                                    Visit Date</label>
                                                <div class="col-sm-7">
                                                    <input autocorrect="off" type="text"
                                                           id="last-visit-date"
                                                           readonly
                                                        <?php if (!$datarow['free_formed_last_visit_date']
                                                            && !$lastVisitData['authofromdate']
                                                            && $datarow['visitid'] == 1
                                                        ): ?>
                                                            placeholder="First Visit"
                                                        <?php endif; ?>
                                                           value="<?php
                                                           if ($datarow['free_formed_last_visit_date']) {
                                                               echo $datarow['free_formed_last_visit_date'];
                                                           } elseif ($lastVisitData['authofromdate']) {
                                                               echo $lastVisitData['authofromdate'];
                                                           }
                                                           ?>"
                                                           name="free_formed_last_visit_date"
                                                           class="form-control my-input"
                                                    >
                                                </div>
                                                <label class="col-sm-3 control-label my-label">Last
                                                    Visit Number</label>
                                                <div class="col-sm-7" id="last-visit-number">
                                                    <input autocorrect="off" type="text"
                                                           placeholder="<?php
                                                           if (!$datarow['free_formed_last_visit_number']
                                                               && !$lastVisitData['Refid']
                                                               && $datarow['visitid'] == 1
                                                           ) {
                                                               echo 'First Visit';
                                                           }
                                                           ?>"
                                                           value="<?php
                                                           if ($datarow['free_formed_last_visit_number']) {
                                                               echo $datarow['free_formed_last_visit_number'];
                                                           } elseif ($lastVisitData['Refid']) {
                                                               echo $lastVisitData['Refid'];
                                                           }
                                                           ?>"
                                                           lang="<?php
                                                           if (!$lastVisitData) {
                                                               echo 'First Visit';
                                                           } else {
                                                               echo $lastVisitData['Refid'];
                                                           }
                                                           ?>"
                                                           name="free_formed_last_visit_number"
                                                           id="free_formed_last_visit_number"
                                                           class="form-control my-input"
                                                    >
                                                    <span class="ref-id-err"
                                                          style="color: red;"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <?php
                                            $muser = $_SESSION['user'];
                                            $id = $_POST['id'];
                                            $managersql = $dbh->prepare("select *
                                                                         from rabbis
                                                                         where emailid ='$muser'");
                                            $managersql->execute();
                                            $managerdata = $managersql->fetch();
                                            $sql2 = $dbh->prepare("select *
                                                                   from `sites`
                                                                   where `Sites` = '$optionDefaultSite'
                                                                   AND `Delete` = 0;");
                                            $sql2->execute();
                                            $data2 = $sql2->fetch();
                                            ?>
                                            <label class="col-sm-3 control-label my-label"
                                                <?php
                                                if (findAssignment($required_fields, 'visits', 'visitdays')) {
                                                    echo " style='color:#428bca;' ";
                                                }

                                                ?>
                                            >Visits days
                                            </label>
                                            <div class="col-sm-7">
                                                <input autocorrect="off" name="visitdays" id="visitdays"
                                                       class="form-control my-input" type="text"
                                                       value="<?= $data2['visitsday']; ?>"
                                                       readonly>
                                            </div>
                                            <label class="col-sm-3 control-label my-label"
                                                <?php
                                                if (findAssignment($required_fields, 'visits', 'visitmanager')) {
                                                    echo " style='color:#428bca;' ";
                                                }

                                                ?>
                                            >Manager
                                            </label>
                                            <div class="col-sm-7">
                                                <?php $site = $datarow['sites'];

                                                $sql = $dbh->prepare("SELECT * from `sites` WHERE `Sites` = '$optionDefaultSite' AND `Delete` = 0 AND `active` = 1;");
                                                $sql->execute();
                                                $siteData = $sql->fetch();

                                                $siteId = $siteData['Id'];

                                                $sql1 = $dbh->prepare("select * from `newmember` where `Sites` = '$siteId' AND `Delete` = 0 AND `Status` = 'Access' AND `roles` = 'Manager'; ");
                                                $sql1->execute();
                                                $memberData = $sql1->fetchAll(); ?>

                                                <select data-placeholder="Choose One"
                                                        name="rabbis"
                                                        id="visitmanager"
                                                        class="form-control my-input"
                                                >
                                                    <?php if ($datarow['rabbis']): ?>
                                                        <option
                                                            selected><?= $datarow['rabbis'] ?></option>
                                                        <?php foreach ($memberData as $manager): ?>
                                                            <?php if ($datarow['rabbis'] != $manager['Firstname'] . ' ' . $manager['Lastname']): ?>
                                                                <option><?= $manager['Firstname'] . ' ' . $manager['Lastname'] ?></option>
                                                            <?php endif; ?>
                                                        <?php endforeach; ?>
                                                    <?php else: ?>
                                                        <option disabled selected>Choose
                                                            One
                                                        </option>
                                                        <?php foreach ($memberData as $manager): ?>
                                                            <option><?= $manager['Firstname'] . ' ' . $manager['Lastname'] ?></option>
                                                        <?php endforeach; ?>
                                                    <?php endif; ?>
                                                </select>
                                            </div>
                                            <label class="col-sm-3 control-label my-label"
                                                <?php
                                                if (findAssignment($required_fields, 'visits', 'siteshost')) {
                                                    echo " style='color:#428bca;' ";
                                                }

                                                ?>
                                            >Host
                                            </label>
                                            <div class="col-sm-7">
                                                <?php
                                                $site = $datarow['sites'];

                                                $sql1 = $dbh->prepare("select *
                                                                       from `hosts`
                                                                       where `Sites` = '$siteId'
                                                                       AND `Delete` = 0
                                                                       AND `Active` = 1; ");
                                                $sql1->execute();
                                                $hostsData = $sql1->fetchAll();

                                                if ($visitsexpried == '') {
                                                    ?>
                                                    <select
                                                        data-placeholder="Choose One"
                                                        name="host" id="siteshost"
                                                        class="form-control my-input"
                                                    >
                                                        <?php if ($datarow['host']): ?>
                                                            <option><?= $datarow['host']; ?></option>

                                                            <?php foreach ($hostsData as $host): ?>
                                                                <?php if ($datarow['host'] != $host['Firstname'] . ' ' . $host['Lastname']): ?>
                                                                    <option><?= $host['Firstname'] . ' ' . $host['Lastname'] ?></option>
                                                                <?php endif; ?>
                                                            <?php endforeach; ?>

                                                            <option value="0">Choose One</option>
                                                        <?php else: ?>
                                                            <option value="0">Choose One</option>
                                                            <?php foreach ($hostsData as $host): ?>
                                                                <option><?= $host['Firstname'] . ' ' . $host['Lastname'] ?></option>
                                                            <?php endforeach; ?>
                                                        <?php endif; ?>
                                                    </select>

                                                    <!--Free formed host name-->
                                                    <input autocorrect="off" class="form-control my-input"
                                                           name="free_formed_host"
                                                           style="text-transform: capitalize"
                                                           value="<?php if ($datarow['free_formed_host']) {
                                                               echo $datarow['free_formed_host'];
                                                           } ?>"/>

                                                    <?php
                                                } else { ?>
                                                    <?php if ($datarow['host']): ?>
                                                        <input autocorrect="off" class="form-control my-input"
                                                               name="host"
                                                               readonly
                                                               style="text-transform: capitalize"
                                                               value="<?php
                                                               echo $datarow['host'];
                                                               ?>"/>
                                                    <?php else: ?>
                                                        <input autocorrect="off" class="form-control my-input"
                                                               name="free_formed_host"
                                                               readonly
                                                               style="float: left;
                                                                    text-transform: capitalize"
                                                               value="<?php
                                                               echo $datarow['free_formed_host'];
                                                               ?>"/>
                                                    <?php endif; ?>
                                                <?php } ?>
                                            </div>
                                            <label class="col-sm-3 control-label my-label"
                                                <?php
                                                if (findAssignment($required_fields, 'visits', 'driver')) {
                                                    echo " style='color:#428bca;' ";
                                                }

                                                ?>
                                            >Driver</label>
                                            <div class="col-sm-7">
                                                <?php
                                                $site = $datarow['sites'];
                                                $driverName = $datarow['driver'];

                                                $sql1 = $dbh->prepare("select * from `drivers` where `Sites` = '$siteId' AND `Delete` = 0 AND `Active` = 1; ");
                                                $sql1->execute();
                                                $driversData = $sql1->fetchAll();

                                                if ($visitsexpried == '') {
                                                    ?>

                                                    <select
                                                        data-placeholder="Choose One"
                                                        name="driver" id="sitesdriver"
                                                        class="form-control my-input"
                                                    >
                                                        <?php if ($driverName): ?>
                                                            <option><?= $driverName ?></option>

                                                            <?php if ($driverName != 'Self-Drive'): ?>
                                                                <option>Self-Drive
                                                                </option>
                                                            <?php endif; ?>

                                                            <?php foreach ($driversData as $driver): ?>
                                                                <?php if ($driverName != $driver['Firstname'] . ' ' . $driver['Lastname']): ?>
                                                                    <option><?= $driver['Firstname'] . ' ' . $driver['Lastname'] ?></option>
                                                                <?php endif; ?>
                                                            <?php endforeach; ?>

                                                            <option value="0">Choose One</option>
                                                        <?php else: ?>
                                                            <option value="0">Choose One</option>
                                                            <option>Self-Drive</option>
                                                            <?php foreach ($driversData as $driver): ?>
                                                                <option><?= $driver['Firstname'] . ' ' . $driver['Lastname'] ?></option>
                                                            <?php endforeach; ?>
                                                        <?php endif; ?>
                                                    </select>
                                                    <!--Free formed driver name-->
                                                    <input autocorrect="off" class="form-control my-input"
                                                           name="free_formed_driver"
                                                           style="text-transform: capitalize;"
                                                           value="<?php if ($datarow['free_formed_driver']) {
                                                               echo $datarow['free_formed_driver'];
                                                           } ?>"/>

                                                    <!--Free formed driver comment-->
                                                    <textarea autocorrect="off" class="form-control my-input"
                                                              name="free_formed_driver_comment"
                                                              rows="3"
                                                    ><?php
                                                        echo $datarow['free_formed_driver_comment']; ?></textarea>
                                                <?php } else { ?>
                                                    <?php if ($datarow['driver']): ?>
                                                        <input autocorrect="off" class="form-control my-input"
                                                               name="driver"
                                                               readonly
                                                               style=" text-transform: capitalize"
                                                               value="<?php
                                                               echo $datarow['driver'];
                                                               ?>"/>
                                                    <?php else: ?>
                                                        <input autocorrect="off" class="form-control my-input"
                                                               name="free_formed_driver"
                                                               readonly
                                                               style="float: left;
                                                                        text-transform: capitalize"
                                                               value="
                                                                    <?php
                                                               echo $datarow['free_formed_driver'];
                                                               ?>"/>
                                                    <?php endif; ?>

                                                    <!--Free formed driver comment-->
                                                    <textarea autocorrect="off" class="form-control my-input"
                                                              name="free_formed_driver_comment"
                                                              rows="3"
                                                              readonly
                                                    ><?php if ($datarow['free_formed_driver_comment']) {
                                                            echo $datarow['free_formed_driver_comment'];
                                                        } ?></textarea>
                                                <?php } ?>
                                                <div id="dateerrormsg1" class="dateerrormsg1">
                                                    <?php
                                                    $curdate = date('M-d-Y');
                                                    $authdate = $datarow['authotodate'];
                                                    if (strtotime($curdate) < strtotime($authdate)) {
                                                        echo '';
                                                    } else {
                                                        $visitsexpried = 'Visits Expired Can`t edit Driver & Host Name';
                                                    }
                                                    ?>
                                                    <?= $visitsexpried; ?>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div
                                class="tab-pane" id="tab1-4">
                                <div class="first-block">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="block-name pers-details mb30">
                                                Personal <span style="font-weight: 400">Details</span>
                                            </div>
                                        </div>
                                        <div id="visits">
                                            <div class="col-sm-6">
                                                <div class="personal-details personal-label my-label"
                                                    <?php
                                                    if (findAssignment($required_fields, 'collectorDetails', 'title')) {
                                                        echo ' style="color:#428bca;" ';
                                                    }
                                                    ?>
                                                >
                                                    Title
                                                </div>
                                                <div class="personal-details">
                                                    <select tabindex="1"
                                                            id="mySelect"
                                                            name="title"
                                                            id="title"
                                                            class="form-control personal-input">
                                                        <?php
                                                        $t = $dataquery11['title'];
                                                        $sitessql = $dbh->prepare("SELECT *
                                                                                   FROM `title`
                                                                                   WHERE `Delete` = 0;");
                                                        $sitessql->execute();
                                                        ?>
                                                        <?php if ($datarow['title']): ?>
                                                            <option>
                                                                <?= $datarow['title']; ?>
                                                            </option>
                                                        <?php else: ?>
                                                            <option selected disabled>
                                                                Choose One
                                                            </option>
                                                        <?php endif; ?>
                                                        <?php
                                                        while ($sitesdata = $sitessql->fetch()) { ?>
                                                            <?php if ($datarow['title'] != $sitesdata['title']): ?>
                                                                <option><?= $sitesdata['title']; ?></option>
                                                            <?php endif; ?>
                                                        <?php } ?>
                                                    </select>
                                                </div>

                                                <div class="personal-details personal-label my-label"
                                                    <?php
                                                    if (findAssignment($required_fields, 'collectorDetails', 'firstname')) {
                                                        echo " style='color:#428bca;' ";
                                                    }

                                                    ?>
                                                >
                                                    First Name
                                                </div>
                                                <div class="personal-details">
                                                    <input autocorrect="off" type="text" tabindex="2"
                                                           onkeypress="Maxval(this.value,30,this.id)"
                                                           name="firstname"
                                                           id="firstname"
                                                           value="<?php echo $datarow['firstname']; ?>"
                                                           class="form-control personal-input">
                                                    <div id="counterfirstname"></div>
                                                </div>

                                                <div class="personal-details personal-label my-label"
                                                    <?php
                                                    if (findAssignment($required_fields, 'collectorDetails', 'lastname')) {
                                                        echo " style='color:#428bca;' ";
                                                    }

                                                    ?>
                                                >
                                                    Last Name
                                                </div>
                                                <div class="personal-details">
                                                    <input autocorrect="off" type="text" tabindex="3"
                                                           onkeypress="Maxval(this.value,20,this.id)"
                                                           name="lastname"
                                                           id="lastname"
                                                           value="<?php echo $datarow['lastname']; ?>"
                                                           class="form-control personal-input">
                                                    <div id="counterlastname"></div>
                                                </div>

                                                <div class="personal-details personal-label my-label"
                                                    <?php
                                                    if (findAssignment($required_fields, 'collectorDetails', 'dob')) {
                                                        echo ' style="color:#428bca;" ';
                                                    }
                                                    ?>
                                                >
                                                    Date of Birth
                                                </div>
                                                <div class="personal-details">
                                                    <div>
                                                        <?php
                                                        if (strlen($dataquery11['dob']) > 4) {
                                                            $onlyYear = false;
                                                            $parts = parserDate($dataquery11['dob']);
                                                            $month = $parts['month'];
                                                            $day = $parts['day'];
                                                            $year = $parts['year'];
                                                        } elseif (strlen($dataquery11['dob']) < 5 && $dataquery11['dob']) {
                                                            $onlyYear = true;
                                                        }

                                                        $months = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];

                                                        $days = 31;
                                                        switch ($month) {
                                                            case '2':
                                                                $days = 29;
                                                                break;
                                                            case '4':
                                                            case '6':
                                                            case '9':
                                                            case '11':
                                                                $days = 30;
                                                                break;
                                                            //Jan, Mar, May, Jul, Aug, Oct, Dec
                                                            default:
                                                                $days = 31;
                                                        }

                                                        $years = [];
                                                        for ($i = 1930; $i <= 2017; $i++) {
                                                            $years[] = $i;
                                                        }
                                                        ?>

                                                        <select tabindex="4" name="month" id="month"
                                                                class="select-birth">
                                                            <option value="0"> Month</option>
                                                            <?php foreach ($months as $m): ?>
                                                                <option <?php if ($m == $month) {
                                                                    echo 'selected';
                                                                } ?> ><?= $m; ?></option>
                                                            <?php endforeach; ?>
                                                        </select>

                                                        <select tabindex="5" name="day" id="day" class="select-birth">
                                                            <option value="0"> Day</option>
                                                            <?php for ($d = 1; $d <= $days; $d++): ?>
                                                                <option <?php if ($day == $d) {
                                                                    echo 'selected';
                                                                } ?> ><?= $d; ?></option>
                                                            <?php endfor; ?>
                                                        </select>

                                                        <select tabindex="6" name="year" id="year" class="select-birth">
                                                            <option value="0"> Year</option>
                                                            <?php foreach ($years as $y): ?>
                                                                <option <?php
                                                                if (($y == $year) || ($y == $dataquery11['dob'])) {
                                                                    echo 'selected';
                                                                } ?> ><?= $y; ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                        <button tabindex="7"
                                                                class="btn btn-primary hide-btn-bg birthDateSave"
                                                        >Save
                                                        </button>
                                                        <button tabindex="7"
                                                                class="btn btn-primary hide-btn birthDateSave"
                                                        >Save
                                                        </button>
                                                    </div>
                                                </div>
                                                <div class="personal-details personal-label my-label align-top" <?php
                                                if (findAssignment($required_fields, 'collectorDetails', 'personalcomments')) {
                                                    echo " style='color:#428bca;' ";
                                                }

                                                ?>>Alert
                                                </div>
                                                <div class="ckbox ckbox-primary personal-details">
                                                    <input autocorrect="off" name="alerts"
                                                           id="alerts"
                                                           type="checkbox"
                                                           value="1"
                                                           onClick="alertcomments(this.checked)"
                                                        <?php
                                                        if ($datarow['personalcomments']) {
                                                            echo 'checked';
                                                        } ?>>
                                                    <label class="my-ckbox box"
                                                           tabindex="7"
                                                           for="alerts"
                                                           style="position: absolute; ">
                                                    </label>

                                                    <textarea autocorrect="off" class="personal-input personal-comments"
                                                              tabindex="8"
                                                              style="<?php
                                                              if (!$datarow['personalcomments']) {
                                                                  echo 'display: none; ';
                                                              } ?>"
                                                              name="personalcomments"
                                                              id="personalcomments"
                                                              rows="5"
                                                              cols="97"><?= ($datarow['personalcomments']) ?></textarea>

                                                    <a class="btn btn-primary btn-xs view-alert"
                                                       id="view-alert"
                                                       href="#personal-comments-div"
                                                       style="<?php
                                                       if (!$datarow['personalcomments']) {
                                                           echo 'display: none; ';
                                                       } ?>">
                                                        View
                                                    </a>

                                                    <pre id="personal-comments-div"
                                                         class="personal-comments-div white-popup-block mfp-hide">
                                                        <?= ($datarow['personalcomments']) ?>
                                                    </pre>
                                                </div>

                                            </div>
                                            <div class="col-sm-6">
                                                <div class="personal-details personal-label my-label"
                                                    <?php
                                                    if (findAssignment($required_fields, 'collectorDetails', 'address1')) {
                                                        echo " style='color:#428bca;' ";
                                                    }

                                                    ?>
                                                >
                                                    <?php
                                                    $sql = $dbh->prepare("select * from collectors where Id='$a'");
                                                    $sql->execute();
                                                    $collectorData = $sql->fetch();
                                                    ?>
                                                    Address 1
                                                </div>
                                                <div class="personal-details">
                                                    <input autocorrect="off" type="text" tabindex="8"
                                                           onkeypress="Maxval(this.value,50,this.id)"
                                                           name="address1" id="address1"
                                                           value="<?= $datarow['address1']; ?>"
                                                           class="form-control personal-input">
                                                    <div id="counteraddress1"></div>
                                                </div>

                                                <div class="personal-details personal-label my-label"
                                                    <?php
                                                    if (findAssignment($required_fields, 'collectorDetails', 'address2')) {
                                                        echo " style='color:#428bca;' ";
                                                    }

                                                    ?>
                                                >
                                                    Address 2
                                                </div>
                                                <div class="personal-details">
                                                    <input autocorrect="off" type="text" tabindex="9"
                                                           onkeypress="Maxval(this.value,50,this.id)"
                                                           name="address2"
                                                           id="address2"
                                                           value="<?= $datarow['address2']; ?>"
                                                           class="form-control personal-input">
                                                    <div id="counteraddress2"></div>
                                                </div>

                                                <div class="personal-details personal-label my-label"
                                                    <?php
                                                    if (findAssignment($required_fields, 'collectorDetails', 'city')) {
                                                        echo " style='color:#428bca;' ";
                                                    }

                                                    ?>
                                                >
                                                    City

                                                    <div class="city-buttons">
                                                        <div class="btn btn-primary btn-xs"
                                                             id="BB">BB
                                                        </div>
                                                        <div class="btn btn-primary btn-xs"
                                                             id="B">B
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="personal-details">
                                                    <input autocorrect="off" type="text" tabindex="10"
                                                           onkeypress="Maxval(this.value,25,this.id); return onlyAlphabets(event,this)"
                                                           name="city" id="city"
                                                           value="<?= $datarow['city']; ?>"
                                                           class="form-control personal-input">
                                                    <div id="countercity"></div>
                                                </div>

                                                <div class="personal-details personal-label my-label"
                                                    <?php
                                                    if (findAssignment($required_fields, 'collectorDetails', 'state')) {
                                                        echo " style='color:#428bca;' ";
                                                    }

                                                    ?>
                                                >
                                                    State / Province
                                                </div>
                                                <div class="personal-details">
                                                    <input autocorrect="off" type="text" tabindex="11"
                                                           onkeypress="Maxval(this.value,17,this.id)"
                                                           name="statecountry" id="state"
                                                           value="<?= $datarow['statecountry']; ?>"
                                                           class="form-control personal-input">
                                                    <div id="counterstate"></div>
                                                </div>

                                                <div class="personal-details personal-label my-label"
                                                    <?php
                                                    if (findAssignment($required_fields, 'collectorDetails', 'country')) {
                                                        echo " style='color:#428bca;' ";
                                                    }

                                                    ?>
                                                >
                                                    Country
                                                </div>
                                                <div class="personal-details">
                                                    <input autocorrect="off" type="text" tabindex="12"
                                                           onkeypress="Maxval(this.value,10,this.id)"
                                                           name="country" id="country"
                                                           class="form-control personal-input"
                                                           value="<?= $datarow['country']; ?>">
                                                    <div id="countercountry"></div>
                                                </div>

                                                <div class="personal-details personal-label my-label"
                                                    <?php
                                                    if (findAssignment($required_fields, 'collectorDetails', 'zipcode')) {
                                                        echo " style='color:#428bca;' ";
                                                    }

                                                    ?>
                                                >
                                                    Zip / Postal Code
                                                </div>
                                                <div class="personal-details">
                                                    <input autocorrect="off" type="text" tabindex="13"
                                                           onkeypress="Maxval(this.value,10,this.id); return onlyAlphabets(event,this)"
                                                           class="form-control personal-input" name="zipcode"
                                                           id="zipcode"
                                                           value="<?= $datarow['zipcode']; ?>">
                                                    <div id="counterzipcode"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="second-block">
                                    <div class="row">
                                        <div class="block-name panel-wizard">
                                            Identification <span style="font-weight: 400">Details</span>
                                        </div>
                                    </div>
                                    <div id="collident">
                                        <div class="row form-group my-label headers">
                                            <div class="col-sm-1 col-xs-1 field primary-ident-label">
                                                Primary
                                            </div>
                                            <div class="col-sm-1 col-xs-1 ident-type">
                                                Type
                                            </div>
                                            <div class="col-sm-2 col-xs-2 field ident-country-label">
                                                <div style="float: left; margin-right: 5%; ">Country</div>
                                                <div class="country-buttons">
                                                    <button class="btn btn-primary btn-xs" type="button" id="ident_isr">
                                                        ISR
                                                    </button>
                                                    <button class="btn btn-primary btn-xs" type="button" id="ident_usa">
                                                        USA
                                                    </button>
                                                    <button class="btn btn-primary btn-xs" type="button" id="ident_uk">
                                                        UK
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="col-sm-2 col-xs-2 field ident-number">
                                                Number
                                            </div>
                                            <div class="col-sm-2 col-xs-2 field" id="passport-name-label">
                                            </div>
                                        </div>
                                        <?php
                                        $collsql1 = $dbh->prepare('SELECT *
                                                                   FROM `visit_details`
                                                                   WHERE `visit_id` = :visitid
                                                                   AND `detail_type` = :detailtype
                                                                   AND `delete` = 0 ;');
                                        $collsql1->execute([
                                            'visitid' => $datarow['Id'],
                                            'detailtype' => 'identification'
                                        ]);
                                        $collidentCount = $collsql1->rowCount();
                                        while ($colldata1 = $collsql1->fetch()) {
                                            ?>
                                            <div class="row collident form-group" data-id="<?= $colldata1['id']; ?>">
                                                <div class="col-sm-1 col-xs-1 field new-primary-ident">
                                                    <div class="select2-container"
                                                         style="width: 100%; border: none;">
                                                        <input autocorrect="off" name="Primary1"
                                                               value="<?= $colldata1['id']; ?>"
                                                               type="radio" <?php if ($colldata1['primary_number']) {
                                                            echo 'checked';
                                                        } ?>>
                                                    </div>
                                                </div>
                                                <div class="col-sm-2 field" id="phone_number">
                                                    <select name="type"
                                                            lang="<?= $colldata1['id']; ?>"
                                                            class="form-control"
                                                            id="ident-type<?= $colldata1['id'] ?>"
                                                            onchange="myssn(this.value, this.lang, this);">

                                                        <option value=" ">Choose One</option>
                                                        <option <?php
                                                        if ($colldata1['type'] == 'Passport') {
                                                            echo 'selected';
                                                        } ?>>Passport
                                                        </option>
                                                        <option <?php
                                                        if ($colldata1['type'] == 'Teudat Zeut') {
                                                            echo 'selected';
                                                        } ?>>
                                                            Teudat Zeut
                                                        </option>
                                                        <option
                                                            value="SSN / ID Number" <?php
                                                        if ($colldata1['type'] == 'SSN / ID Number'
                                                            || $colldata1['type'] == 'SSN'
                                                        ) {
                                                            echo 'selected';
                                                        } ?>>
                                                            SSN / ID Number
                                                        </option>
                                                        <option
                                                            value="NIN" <?php
                                                        if ($colldata1['type'] == 'National ID'
                                                            || $colldata1['type'] == 'NIN'
                                                        ) {
                                                            echo 'selected';
                                                        } ?>>
                                                            NIN
                                                        </option>
                                                        <option <?php
                                                        if ($colldata1['type'] == 'Driver License') {
                                                            echo 'selected';
                                                        } ?>>
                                                            Driver License
                                                        </option>
                                                        <option <?php
                                                        if ($colldata1['type'] == 'Green Card') {
                                                            echo 'selected';
                                                        } ?>>
                                                            Green Card
                                                        </option>
                                                        <option
                                                            value="Number" <?php
                                                        if ($colldata1['type'] == 'Number') {
                                                            echo 'selected';
                                                        } ?>>
                                                            Number
                                                        </option>
                                                    </select>
                                                </div>
                                                <div id="myssn<?= $colldata1['id']; ?>">
                                                    <div class="col-sm-2">
                                                        <input autocorrect="off" type="text" name="country"
                                                               class="form-control"
                                                               id="country<?= $colldata1['id']; ?>"
                                                               value="<?= $colldata1['country']; ?>"
                                                               onfocus="identCountryListener(this.id);"
                                                               placeholder="Country"
                                                               onkeypress="Maxval(this.value,30,this.id); return onlyAlphabets(event,this)"
                                                               style="text-transform:uppercase">
                                                        <div id="countercountry<?= $colldata1['id']; ?>"></div>
                                                    </div>
                                                    <div class="col-sm-2 field">
                                                        <input autocorrect="off" type="text" name="detail_number"
                                                               class="form-control"
                                                               id="phone<?= $colldata1['id']; ?>"
                                                               value="<?= $colldata1['detail_number']; ?> "
                                                               onkeypress="Maxval(this.value,20,this.id)"
                                                            <?php if ($colldata1['type'] == 'SSN / ID Number'
                                                                || $colldata1['type'] == 'SSN'
                                                            ): ?>
                                                                onkeydown="$(this).simpleMask( { 'mask': 'ssn', 'nextInput': false } );"
                                                            <?php endif; ?>
                                                        >
                                                        <div id="counterphone<?= $colldata1['id']; ?>"></div>
                                                    </div>

                                                    <?php if ($colldata1['type'] == 'Passport'): ?>
                                                        <div class="col-sm-2 field">
                                                            <input autocorrect="off" type="text" class="form-control"
                                                                   id="old-passport-name<?= $colldata1['id']; ?>"
                                                                   name="passport_name"
                                                                   onkeypress="Maxval(this.value,35,this.id); return onlyAlphabets(event,this)"
                                                                   placeholder="Passport Name"
                                                                   value="<?= $colldata1['passport_name']; ?>">
                                                            <div
                                                                id="counterold-passport-name<?= $colldata1['id']; ?>"></div>
                                                        </div>
                                                    <?php endif; ?>
                                                </div>

                                                <div class="col-sm-1 field row">
                                                    <a onclick="removeIdent(<?= $colldata1['id']; ?>)"><img
                                                            class="delete-img"
                                                            src="images/remove-icon.png"
                                                            border="0"
                                                            title="Remove"
                                                            alt="Remove"></a>
                                                </div>
                                            </div>
                                        <?php }
                                        include 'addidenti.php'; ?>
                                        <a href="javascript:void(0); " onclick="ChkvalId();"
                                           class="add-button"
                                           title="Add New Identification Details Item">
                                            <img src="images/add-icon.png"
                                                 style="height:25px;"/>
                                        </a>
                                        <br/>
                                    </div>
                                </div>
                                <div class="second-block" id="contact">
                                    <div class="row">
                                        <div class="block-name panel-wizard">
                                            Contact <span style="font-weight: 400">Details</span>
                                        </div>
                                    </div>
                                    <div class="row my-label headers">
                                        <div class="col-sm-1 col-xs-1 primary-contact-label">
                                            Primary
                                        </div>
                                        <div class="col-sm-1 col-xs-1 phone-type-label" style="">
                                            Type
                                        </div>
                                        <div class="col-sm-2 col-xs-2" style="margin-right: 8.4%;">
                                            <div style="float: left; margin-right: 5%; ">Country</div>
                                            <div class="country-buttons">
                                                <button class="btn btn-primary btn-xs" type="button" id="contact_isr">
                                                    ISR
                                                </button>
                                                <button class="btn btn-primary btn-xs" type="button" id="contact_usa">
                                                    USA
                                                </button>
                                                <button class="btn btn-primary btn-xs" type="button" id="contact_uk">
                                                    UK
                                                </button>
                                            </div>
                                        </div>
                                        <div class="col-sm-2 col-xs-1 contact-number-label">
                                            Phone
                                        </div>

                                    </div>
                                    <?php $sqlquery = $dbh->prepare('SELECT *
                                                                     FROM `visit_details`
                                                                     WHERE `visit_id` = :visitid
                                                                     AND `detail_type` = :detailtype
                                                                     AND `delete` = 0 ;');
                                    $sqlquery->execute([
                                        'visitid' => $datarow['Id'],
                                        'detailtype' => 'contact'
                                    ]);
                                    while ($dataquery1 = $sqlquery->fetch()) { ?>
                                        <div class="row form-group" id="addinput"
                                             data-id="<?= $dataquery1['id']; ?>">
                                            <div class="row col-sm-1 field input-primary">
                                                <input autocorrect="off" name="CPrimary"
                                                       value="<?= $dataquery1['id']; ?>"
                                                       type="radio"<?php if ($dataquery1['primary_number']) {
                                                    echo 'checked';
                                                } ?>>
                                            </div>
                                            <label class="row col-sm-3 control-label" style="width: 150px;">
                                                <select class="form-control" name="type">
                                                    <option value="<?php
                                                    if ($dataquery1['type']) {
                                                        echo $dataquery1['type'];
                                                    } else {
                                                        echo '';
                                                    }
                                                    ?>  ">
                                                        <?php
                                                        if ($dataquery1['type']) {
                                                            echo $dataquery1['type'];
                                                        } else {
                                                            echo 'Choose One';
                                                        }
                                                        ?>
                                                    </option>
                                                    <option>Work</option>
                                                    <option>Home</option>
                                                    <option>Mobile</option>
                                                    <option>Fax</option>
                                                    <option>Other</option>
                                                </select>
                                            </label>
                                            <div class="col-sm-3">
                                                <input autocorrect="off" type="text" style="text-transform: uppercase"
                                                       lang="<?= $dataquery1['id']; ?>"
                                                       id="countrycode<?= $dataquery1['id']; ?>"
                                                       name="country"
                                                       onkeypress="Maxval(this.value,30,this.id); return onlyAlphabets(event,this)"
                                                       class="form-control valid"
                                                       onfocus="contactCountryListener(this.value, this.id, this.lang)"
                                                       onchange="chkphonetypeselect(this.value,this.lang);"
                                                       placeholder="Country"
                                                       value="<?= $dataquery1['country']; ?>">
                                                <div id="counterphone<?php echo $colldata1['id']; ?>"></div>
                                            </div>

                                            <div class="row col-sm-3 field" id='phone_number'>
                                                <input autocorrect="off" type="text" name="detail_number"
                                                       value="<?= $dataquery1['detail_number']; ?>"
                                                       onkeypress="Maxval(this.value,20,this.id); return blockSpecialCharNum(event)"
                                                       lang="<?= $dataquery1['id']; ?>"
                                                       id="phone<?= $dataquery1['id']; ?>"
                                                       class="form-control"
                                                       placeholder="Number">
                                                <div id="counterphone<?php echo $colldata1['id']; ?>"></div>
                                            </div>
                                            <div class="row col-sm-1 field">

                                                <a onclick="removephne(<?= $dataquery1['id']; ?>)"><img
                                                        class="delete-img"
                                                        src="images/remove-icon.png" border="0"
                                                        title="Remove Details"
                                                        alt="Remove"></a>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php include 'add_phone_wo_row.php'; ?>
                                    <div class="row">
                                        <a href="javascript:void(0);" onclick="chkphoneval();"
                                           class="add-button"
                                           title="Add field">
                                            <img src="images/add-icon.png" style="height: 25px;"/><br/>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div
                                class="<?php if (($personalinfonext == 'personalinfonext')
                                    || ($addressprev == 'addressprev')
                                ) {
                                    echo 'tab-pane active';
                                } else {
                                    echo 'tab-pane';
                                } ?>" id="tab3-4">
                                <div class="panel panel-default">
                                    <div class="panel-heading" style="padding:9px 0 2px 12px">
                                        <button class="btn btn-primary mr5" type="button"
                                                name="tempadd" id="copyPersonalAddress" style="margin-bottom:1%; ">
                                            Temporary
                                            US Address
                                        </button>
                                    </div><!-- panel-heading -->
                                    <div id="rresult" class="first-block rresult">
                                        <input autocorrect="off" name="memberid" type="hidden" value="<?= $a; ?>">
                                        <input autocorrect="off" name="visitid" type="hidden" value="<?= $b; ?>">
                                        <input autocorrect="off" name="the-visit-id" id="the-visit-id" type="hidden"
                                               value="<?= $datarow['Id'] ?>">
                                        <?php
                                        $v = $_SESSION['visit'];
                                        $sql = $dbh->prepare("select * from visitstable where collectorsid ='$a' and visitid='$b'");
                                        $sql->execute();
                                        $newdata = $sql->fetch();
                                        ?>
                                        <div class="first-block">
                                            <div class="row">
                                                <div class="col-sm-1 my-label">
                                                    Name
                                                </div>
                                                <div class="col-sm-4">
                                                    <input autocorrect="off" type="text" tabindex="1"
                                                           onkeypress="Maxval(this.value,35,this.id)"
                                                           name="us_name"
                                                           value="<?php
                                                           echo $newdata['us_name'];
                                                           ?>"
                                                           class="form-control my-input text-capitalize"
                                                    >
                                                </div>
                                                <div class="col-sm-1">
                                                    &nbsp;
                                                </div>
                                                <div class="col-sm-1 my-label"
                                                    <?php
                                                    if (findAssignment($required_fields, 'address', 'uscountry')) {
                                                        echo " style='color:#428bca;' ";
                                                    }

                                                    ?>
                                                >
                                                    Country
                                                </div>
                                                <div class="col-sm-4">
                                                    <input autocorrect="off" type="text" tabindex="6"
                                                           onkeypress="Maxval(this.value,17,this.id); return onlyAlphabets(event,this)"
                                                           name="uscountry" id="uscountry"
                                                           value="<?php if ($newdata['uscountry'] != '') {
                                                               echo $newdata['uscountry'];
                                                           } else {
                                                               echo $datarow1['statecountry'];
                                                           } ?>" class="form-control my-input text-capitalize">
                                                    <div id="counteruscountry" class="my-input-margin"></div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-1 my-label"
                                                    <?php
                                                    if (findAssignment($required_fields, 'address', 'usaddress1')) {
                                                        echo " style='color:#428bca;' ";
                                                    } ?>>
                                                    Address
                                                </div>
                                                <div class="col-sm-4">
                                                    <input autocorrect="off" type="text"
                                                           onkeypress="Maxval(this.value,50,this.id)"
                                                           tabindex="2"
                                                           name="usaddress1" id="usaddress1"
                                                           value="<?php
                                                           echo $newdata['usaddress1'];
                                                           ?>" class="form-control my-input text-capitalize"
                                                           placeholder="">
                                                    <div id="counterusaddress1" class="my-input-margin"></div>
                                                </div>
                                                <div class="col-sm-1">
                                                    &nbsp;
                                                </div>
                                                <div class="col-sm-1 my-label"
                                                    <?php
                                                    if (findAssignment($required_fields, 'address', 'uszipcode')) {
                                                        echo " style='color:#428bca;' ";
                                                    }

                                                    ?>
                                                >
                                                    Zip / Postal Code
                                                </div>
                                                <div class="col-sm-4">
                                                    <input autocorrect="off" type="text"
                                                           onkeypress="Maxval(this.value,10,this.id); return onlyAlphabets(event,this)"
                                                           tabindex="7"
                                                           class="form-control my-input" name="uszipcode"
                                                           id="tempzipcode"
                                                           value="<?php if ($newdata['uszipcode'] != '') {
                                                               echo $newdata['uszipcode'];
                                                           } else {
                                                               echo $datarow1['zipcode'];
                                                           } ?>">
                                                    <div id="countertempzipcode" class="my-input-margin"></div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-1 my-label"
                                                    <?php
                                                    if (findAssignment($required_fields, 'address', 'zipcity')) {
                                                        echo " style='color:#428bca;' ";
                                                    }

                                                    ?>
                                                >
                                                    City
                                                </div>
                                                <div class="col-sm-4">
                                                    <input autocorrect="off" type="text"
                                                           onkeypress="Maxval(this.value,25,this.id); return onlyAlphabets(event,this)"
                                                           tabindex="3"
                                                           name="zipcity" id="zipcity"
                                                           value="<?php if ($newdata['zipcity']) {
                                                               echo $newdata['zipcity'];
                                                           } else {
                                                               echo $datarow1['city'];
                                                           } ?>" class="form-control my-input text-capitalize">
                                                    <div id="counterzipcity" class="my-input-margin"></div>
                                                </div>
                                                <div class="col-sm-1">
                                                    &nbsp;
                                                </div>
                                                <div class="col-sm-1 my-label field" id="phone_number"
                                                    <?php
                                                    if (findAssignment($required_fields, 'address', 'zipphone')) {
                                                        echo " style='color:#428bca;' ";
                                                    }

                                                    ?>
                                                >
                                                    Phone
                                                </div>
                                                <div class="col-sm-4">
                                                    <input autocorrect="off" type="text"
                                                           onkeypress="Maxval(this.value,15,this.id);"
                                                           tabindex="8"
                                                           name="zipphone"
                                                           class="form-control my-input"
                                                           id="tempphone"
                                                           value="<?php if ($newdata['zipphone'] != '') {
                                                               echo $newdata['zipphone'];
                                                           } else {
                                                               echo $datarow1['phoneno'];
                                                           } ?>">
                                                    <div id="countertempphone" class="my-input-margin"></div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <?php
                                                $querySql = $dbh->prepare("
															select *
															from
															required_fields
															where `tab`='address' AND assignment ='tempstate'
														");
                                                $querySql->execute();
                                                $dataSql = $querySql->fetch();
                                                ?>
                                                <div class="col-sm-1 my-label"
                                                    <?php
                                                    if (findAssignment($required_fields, 'address', 'zipstate')) {
                                                        echo " style='color:#428bca;' ";
                                                    }

                                                    ?>
                                                >
                                                    State / Province
                                                </div>
                                                <div class="col-sm-4">
                                                    <input autocorrect="off" type="text"
                                                           onkeypress="Maxval(this.value,17,this.id)"
                                                           tabindex="4"
                                                           name="zipstate" id="tempstate"
                                                           value="<?php if ($newdata['zipstate'] != '') {
                                                               echo $newdata['zipstate'];
                                                           } else {
                                                               echo $datarow1['statecountry'];
                                                           } ?>" class="form-control my-input text-capitalize">
                                                    <div id="countertempstate" class="my-input-margin"></div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-1 my-label"
                                                    <?php
                                                    if (findAssignment($required_fields, 'address', 'zipcomments')) {
                                                        echo " style='color:#428bca;' ";
                                                    }

                                                    ?>
                                                >
                                                    Comments
                                                </div>
                                                <div class="col-sm-10">
                                                <textarea autocorrect="off" class="form-control textarea-comment"
                                                          tabindex="5" rows="4"
                                                          cols="10"
                                                          placeholder="" name="zipcomments"
                                                          id="tempcomments"><?php if ($newdata['zipcomments'] != '') {
                                                        echo $newdata['zipcomments'];
                                                    } else {
                                                        echo $datarow1['zipcomments'];
                                                    } ?></textarea>
                                                    <div id="countertempcomments" class="my-input-margin"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- panel-body -->
                                </div>
                                <div class="col-md-12" style="padding: 0px;">
                                    <?php $regdate;
                                    $v = $_SESSION['visit'];
                                    $sql = $dbh->prepare("select * from visitstable where collectorsid ='$a' and visitid='$b'");
                                    $sql->execute();
                                    $data1 = $sql->fetch();
                                    $ad = $data1['address1'];
                                    $sqlsites = $data1['sites'];
                                    $sql3 = $dbh->prepare("select * from sites where Id ='$sqlsites'");
                                    $sql3->execute();
                                    $data13 = $sql3->fetch();
                                    ?>
                                    <div class="panel panel-default">
                                        <div class="panel-heading" style="padding:9px 0 2px 12px">
                                            <button class="btn btn-primary mr5" name="baltiadd"
                                                    id="copyUsAddress"
                                                    type="button"
                                                    style="margin-bottom:1%; "><span
                                                    style="text-transform: capitalize;"><?= $data13['Sites']; ?></span>
                                                Temporary Baltimore Address
                                            </button>
                                        </div><!-- panel-heading -->
                                        <div id="localaddress" class="first-block localaddress">
                                            <div class="first-block">
                                                <div class="row">
                                                    <div class="col-sm-1 my-label"
                                                        <?php
                                                        if (findAssignment($required_fields, 'address', 'baltiaddress1')) {
                                                            echo " style='color:#428bca;' ";
                                                        }

                                                        ?>
                                                    >
                                                        Name
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <input autocorrect="off" required type="text"
                                                               tabindex="9" name="free_formed_host"
                                                               id="free-formed-host"
                                                               value="<?php
                                                               if ($data1['host']) {
                                                                   echo $data1['host'];
                                                               } else {
                                                                   echo $data1['free_formed_host'];
                                                               }
                                                               ?>"
                                                               class="form-control text-capitalize my-input">
                                                    </div>
                                                    <div class="col-sm-1">
                                                        &nbsp;
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-1 my-label"
                                                        <?php
                                                        if (findAssignment($required_fields, 'address', 'baltiaddress2')) {
                                                            echo " style='color:#428bca;' ";
                                                        }

                                                        ?>
                                                    >
                                                        Address
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <input autocorrect="off" type="text"
                                                               onkeypress="Maxval(this.value,50,this.id)"
                                                               tabindex="10" name="baltiaddress1" id="baltiaddress1"
                                                               value="<?php
                                                               if ($data1['host']) {
                                                                   $parts = explode(' ', $data1['host']);
                                                                   $firstName = $parts[0];
                                                                   $lastName = $parts[1];

                                                                   $sql = $dbh->prepare('SELECT *
                                                                                         FROM `hosts`
                                                                                         WHERE `firstname` = :firstname
                                                                                         AND `lastname` = :lastname
                                                                                         AND `Delete` = 0 ;');
                                                                   $sql->execute([
                                                                       'firstname' => $firstName,
                                                                       'lastname' => $lastName
                                                                   ]);

                                                                   $hostData = $sql->fetch();

                                                                   $sql = $dbh->prepare('SELECT *
                                                                                         FROM `hostphone`
                                                                                         WHERE `H_id` = :hostid
                                                                                         AND HPrimary != "" ;');
                                                                   $sql->execute(['hostid' => $hostData['Id']]);
                                                                   $hostPhoneData = $sql->fetch();
                                                               }
                                                               if ($hostData) {
                                                                   echo $hostData['Address'];
                                                               } else {
                                                                   echo $datarow['baltiaddress1'];
                                                               }
                                                               ?>"
                                                               class="form-control my-input text-capitalize"
                                                               placeholder="">
                                                        <div id="counterbaltiaddress1" class="my-input-margin"></div>
                                                    </div>
                                                    <div class="col-sm-1">
                                                        &nbsp;
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-1 my-label field" id="phone_number"
                                                        <?php
                                                        if (findAssignment($required_fields, 'address', 'baltiphone')) {
                                                            echo " style='color:#428bca;' ";
                                                        }
                                                        ?>
                                                    >
                                                        Phone
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <input autocorrect="off" type="text"
                                                               onkeypress="Maxval(this.value,15,this.id)"
                                                               tabindex="11"
                                                               name="baltiphone"
                                                               class="form-control my-input"
                                                               id="baltiphone"
                                                               value="<?php
                                                               if ($data1['host'] && $hostPhoneData) {
                                                                   echo $hostPhoneData['Phone_Number'];
                                                               } else {
                                                                   echo $datarow['baltiphone'];
                                                               } ?>">
                                                        <div id="counterbaltiphone" class="my-input-margin"></div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-1 my-label"
                                                        <?php
                                                        if (findAssignment($required_fields, 'address', 'balticomments')) {
                                                            echo " style='color:#428bca;' ";
                                                        }

                                                        ?>
                                                    >
                                                        Comments
                                                    </div>
                                                    <div class="col-sm-10">
                                                            <textarea autocorrect="off"
                                                                      class="form-control textarea-comment"
                                                                      tabindex="13"
                                                                      rows="4" cols="10"
                                                                      name="balticomments"
                                                                      id="balticomments"><?php
                                                                if ($datarow2['zipcomments']) {
                                                                    echo $datarow2['zipcomments'];
                                                                } else {
                                                                    echo $datarow['balticomments'];
                                                                } ?></textarea>
                                                        <div id="counterbalticomments" class="my-input-margin"></div>
                                                    </div>
                                                </div>
                                            </div><!-- panel-body -->
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane" id="tab4-4">
                                <div class="first-block">
                                    <input autocorrect="off" name="memberid" type="hidden" value="<?= $a; ?>">
                                    <input autocorrect="off" name="visitid" type="hidden" value="<?= $b; ?>">
                                    <div>
                                        <div class="col-sm-5"><b> The Purpose of these funds is for</b></div>
                                        <div class="col-sm-2">
                                            <input autocorrect="off" type="radio" value="Institution"
                                                   onclick="yesnoCheck(this.value, this);"
                                                   checked
                                                   name="funds"
                                                   id="yesCheck">
                                            <label class="label-funds" for="yesCheck">Institution</label>
                                        </div>
                                        <div class="col-sm-2">
                                            <input autocorrect="off" type="radio" value="Individual"
                                                   onclick="yesnoCheck(this.value, this);"<?php if ($datarow['funds'] == 'Individual') {
                                                echo 'checked';
                                            } ?> name="funds" id="noCheck">
                                            <label class="label-funds" for="noCheck">Individual</label>
                                        </div>
                                        <br><!-- rdio -->
                                    </div>
                                    <div id="result1">
                                        <input autocorrect="off" name="bothval" type="hidden" value="<?= $bothval; ?>">
                                        <?php $bothval = $_POST['bothval']; ?>
                                        <div class="panel-body"
                                            <?php if ($datarow['funds'] == 'Individual'): ?>
                                                style="display: none;"
                                            <?php endif; ?>
                                        >
                                            <div class="row">
                                                <div class="col-sm-1 my-label"
                                                    <?php
                                                    if (findAssignment($required_fields, 'funds', 'institname')) {
                                                        echo " style='color:#428bca;' ";
                                                    }

                                                    ?>
                                                >
                                                    Name
                                                </div>
                                                <div class="col-sm-10">
                                                    <input autocorrect="off" type="text" tabindex="1"
                                                           onkeypress="Maxval(this.value,26,this.id);"
                                                           name="institname" id="institname"
                                                           value="<?php echo $datarow['institname']; ?>"
                                                           class="form-control textarea-comment text-capitalize">
                                                    <div id="counterinstitname" class="my-input-margin"></div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-1 my-label"
                                                    <?php
                                                    if (findAssignment($required_fields, 'funds', 'institaddress1')) {
                                                        echo " style='color:#428bca;' ";
                                                    }

                                                    ?>
                                                >
                                                    Address 1
                                                </div>
                                                <div class="col-sm-4">
                                                    <input autocorrect="off" type="text" tabindex="2"
                                                           onkeypress="Maxval(this.value,50,this.id)"
                                                           name="institaddress1" id="institaddress1"
                                                           value="<?php echo $datarow['institaddress1']; ?>"
                                                           class="form-control my-input text-capitalize"
                                                           placeholder=""
                                                           required="">
                                                    <div id="counterinstitaddress1" class="my-input-margin"></div>
                                                </div>
                                                <div class="col-sm-1">
                                                    &nbsp;
                                                </div>
                                                <div class="col-sm-1 my-label"
                                                    <?php
                                                    if (findAssignment($required_fields, 'funds', 'institcountry')) {
                                                        echo " style='color:#428bca;' ";
                                                    }

                                                    ?>
                                                >
                                                    Country
                                                </div>
                                                <div class="col-sm-4">
                                                    <input autocorrect="off" type="text" tabindex="6"
                                                           onkeypress="Maxval(this.value,17,this.id); return onlyAlphabets(event,this)"
                                                           name="institcountry" id="institcountry"
                                                           value="<?php echo $datarow['institcountry']; ?>"
                                                           class="form-control my-input text-capitalize">
                                                    <div id="counterinstitcountry" class="my-input-margin"></div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-1 my-label"
                                                    <?php
                                                    if (findAssignment($required_fields, 'funds', 'institaddress2')) {
                                                        echo " style='color:#428bca;' ";
                                                    }

                                                    ?>
                                                >
                                                    Address 2
                                                </div>
                                                <div class="col-sm-4">
                                                    <input autocorrect="off" type="text" tabindex="2"
                                                           onkeypress="Maxval(this.value,50,this.id)"
                                                           name="institaddress2" id="institaddress2"
                                                           value="<?= $datarow['institaddress2']; ?>"
                                                           class="form-control my-input text-capitalize"
                                                           placeholder="">
                                                    <div id="counterinstitaddress2" class="my-input-margin"></div>
                                                </div>
                                                <div class="col-sm-1">
                                                    &nbsp;
                                                </div>
                                                <div class="col-sm-1 my-label"
                                                    <?php
                                                    if (findAssignment($required_fields, 'funds', 'institzipcode')) {
                                                        echo " style='color:#428bca;' ";
                                                    }

                                                    ?>
                                                >
                                                    Zip / Postal Code
                                                </div>
                                                <div class="col-sm-4">
                                                    <input autocorrect="off" type="text" tabindex="7"
                                                           onkeypress="Maxval(this.value,10,this.id)"
                                                           class="form-control my-input" name="institzip"
                                                           id="institzipcode"
                                                           value="<?php echo $datarow['institzip']; ?>"
                                                           placeholder="" required=""
                                                           onkeypress="return onlyAlphabets(event,this)">
                                                    <div id="counterinstitzipcode" class="my-input-margin"></div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-1 my-label"
                                                    <?php
                                                    if (findAssignment($required_fields, 'funds', 'institcity')) {
                                                        echo " style='color:#428bca;' ";
                                                    }

                                                    ?>
                                                >
                                                    City

                                                    <div class="instit-city-buttons">
                                                        <div class="btn btn-primary btn-xs"
                                                             id="BB">BB
                                                        </div>
                                                        <div class="btn btn-primary btn-xs"
                                                             id="B">B
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="col-sm-4">
                                                    <input autocorrect="off" type="text" tabindex="3"
                                                           onkeypress="Maxval(this.value,25,this.id); return onlyAlphabets(event,this)"
                                                           name="institcity"
                                                           id="institcity"
                                                           value="<?php echo $datarow['institcity']; ?>"
                                                           class="form-control my-input text-capitalize">
                                                    <div id="counterinstitcity" class="my-input-margin"></div>
                                                </div>
                                                <div class="col-sm-1">
                                                    &nbsp;
                                                </div>
                                                <div class="col-sm-1 my-label field" id="phone_number"
                                                    <?php
                                                    if (findAssignment($required_fields, 'funds', 'institphone')) {
                                                        echo " style='color:#428bca;' ";
                                                    }

                                                    ?>
                                                >
                                                    Phone
                                                </div>
                                                <div class="col-sm-4">
                                                    <input autocorrect="off" type="text" tabindex="8"
                                                           onkeypress="Maxval(this.value,14,this.id)"
                                                           name="institphone" class="form-control my-input"
                                                           id="institphone"
                                                           value="<?php echo $datarow['institphone']; ?>">
                                                    <div id="counterinstitphone" class="my-input-margin"></div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-1 my-label"
                                                    <?php
                                                    if (findAssignment($required_fields, 'funds', 'institdate')) {
                                                        echo " style='color:#428bca;' ";
                                                    }

                                                    ?>
                                                >
                                                    State / Province
                                                </div>
                                                <div class="col-sm-4">
                                                    <input autocorrect="off" type="text" tabindex="4"
                                                           onkeypress="Maxval(this.value,17,this.id)"
                                                           name="institstate" id="institstate"
                                                           value="<?php echo $datarow['institstate']; ?>"
                                                           class="form-control my-input text-capitalize"
                                                           required="">
                                                    <div id="counterinstitstate" class="my-input-margin"></div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-1 my-label"
                                                    <?php
                                                    if (findAssignment($required_fields, 'fudns', 'institcomments')) {
                                                        echo " style='color:#428bca;' ";
                                                    }

                                                    ?>
                                                >
                                                    Comments
                                                </div>
                                                <div class="col-sm-10">
                                                    <textarea autocorrect="off" class="form-control textarea-comment"
                                                              tabindex="5" rows="4" cols="10"
                                                              id="institcomments" placeholder=""
                                                              name="institcomments" id="institcomments"
                                                              required=""><?php echo $datarow['institcomments']; ?></textarea>
                                                    <div id="counterinstitcomments" class="my-input-margin"></div>
                                                </div>
                                            </div>
                                        </div><!--panel-body -->
                                    </div>
                                </div>
                            </div><!-- tab-pane -->
                            <div class="tab-pane" id="tab5-4">
                                <div class="first-block">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <b style
                                               ="<?php
                                            if (findAssignment($required_fields, 'purpose', 'active[]')) {
                                                echo " style='color:#428bca;' ";
                                            }

                                            ?>">Enter all that apply</b></div>
                                    </div>
                                    <input autocorrect="off" name="memberid" type="hidden" value="<?= $a; ?>">
                                    <input autocorrect="off" name="visitid" type="hidden" value="<?= $b; ?>">
                                    <div class="panel-body tab-purpose">
                                        <div class="row purpose-inner">
                                            <div class="purpose-item">
                                                <div class="ckbox ckbox-primary">
                                                    <?php
                                                    $purposedata = explode(",", $datarow['purpose']);
                                                    ?>
                                                    <input autocorrect="off" type="checkbox" id="Yeshiva"
                                                           value="Yeshiva"
                                                        <?php
                                                        if (in_array("Yeshiva", $purposedata)
                                                            || preg_match('/Yeshiva/', $datarow['purpose'])
                                                        ) {
                                                            echo "checked";
                                                        }
                                                        ?> name="active[]" required>
                                                    <label class="my-label" for="Yeshiva""
                                                    >Yeshiva</label>
                                                </div>
                                            </div>
                                            <div class="purpose-item" id="fix">
                                                <div class="ckbox ckbox-primary">
                                                    <input autocorrect="off" type="checkbox" id="Kiruv" value="Kiruv"
                                                        <?php
                                                        if (in_array("Kiruv", $purposedata)
                                                            || preg_match('/Kiruv/', $datarow['purpose'])
                                                        ) {
                                                            echo "checked";
                                                        }
                                                        ?>
                                                           name="active[]" required="">
                                                    <label class="my-label" for="Kiruv">Kiruv </label>
                                                </div>
                                            </div>
                                            <div class="purpose-item">
                                                <div class="ckbox ckbox-primary">
                                                    <input autocorrect="off" type="checkbox" id="Free Loan"
                                                           value="Free Loan"
                                                        <?php
                                                        if (in_array("Free Loan", $purposedata)
                                                            || preg_match('/Free Loan/', $datarow['purpose'])
                                                        ) {
                                                            echo "checked";
                                                        }
                                                        ?>
                                                           name="active[]" required="">
                                                    <label class="my-label" for="Free Loan">Free Loan </label>
                                                </div>
                                            </div>
                                            <div class="purpose-item">
                                                <div class="ckbox ckbox-primary">
                                                    <input autocorrect="off" type="checkbox" id="Hanchnosas"
                                                           value="Hanchnosas"
                                                        <?php
                                                        if (in_array("Hanchnosas", $purposedata)
                                                            || preg_match('/Hanchnosas/', $datarow['purpose'])
                                                        ) {
                                                            echo "checked";
                                                        }
                                                        ?>
                                                           name="active[]" required="">
                                                    <label class="my-label" for="Hanchnosas">Hachnosas
                                                        <br/>Kallah</label>
                                                </div>
                                            </div>
                                            <div class="purpose-item" id="fix">
                                                <div class="ckbox ckbox-primary">
                                                    <input autocorrect="off" type="checkbox" id="Medical"
                                                           value="Medical"
                                                        <?php
                                                        if (in_array("Medical", $purposedata)
                                                            || preg_match('/Medical/', $datarow['purpose'])
                                                        ) {
                                                            echo "checked";
                                                        }
                                                        ?>
                                                           name="active[]" required="">
                                                    <label class="my-label" for="Medical">Medical</label>
                                                </div>
                                            </div>
                                            <div class="purpose-item">
                                                <div class="ckbox ckbox-primary">
                                                    <input autocorrect="off" type="checkbox" id="Mental" value="Mental"
                                                        <?php
                                                        if (in_array("Mental", $purposedata)
                                                            || preg_match('/Mental/', $datarow['purpose'])
                                                        ) {
                                                            echo "checked";
                                                        }
                                                        ?>
                                                           name="active[]" required="">
                                                    <label class="my-label" for="Mental">Mental </label>
                                                </div>
                                            </div>
                                            <div class="purpose-item">
                                                <div class="ckbox ckbox-primary">
                                                    <input autocorrect="off" type="checkbox" id="Torah" value="Torah"
                                                        <?php
                                                        if (in_array("Torah", $purposedata)
                                                            || preg_match('/Torah/', $datarow['purpose'])
                                                        ) {
                                                            echo "checked";
                                                        }
                                                        ?>
                                                           name="active[]" required="">
                                                    <label class="my-label" for="Torah">Torah<br>Education</label>
                                                </div>
                                            </div>
                                            <div class="purpose-item">
                                                <div class="ckbox ckbox-primary">
                                                    <input autocorrect="off" type="checkbox" id="Kollel" value="Kollel"
                                                        <?php
                                                        if (in_array("Kollel", $purposedata)
                                                            || preg_match('/Kollel/', $datarow['purpose'])
                                                        ) {
                                                            echo "checked";
                                                        }
                                                        ?>
                                                           name="active[]" required="">
                                                    <label class="my-label" for="Kollel">Kollel</label>
                                                </div>
                                            </div>
                                            <div class="purpose-item">
                                                <div class="ckbox ckbox-primary">
                                                    <input autocorrect="off" type="checkbox" id="Charity"
                                                           value="Charity"
                                                        <?php
                                                        if (in_array("Charity", $purposedata)
                                                            || preg_match('/Charity/', $datarow['purpose'])
                                                        ) {
                                                            echo "checked";
                                                        }
                                                        ?>
                                                           name="active[]" required="">
                                                    <label class="my-label" for="Charity">Charity</label>
                                                </div>
                                            </div>
                                            <div class="purpose-item">
                                                <div class="ckbox ckbox-primary">
                                                    <input autocorrect="off" type="checkbox" id="Business"
                                                           value="Business"
                                                        <?php
                                                        if (in_array("Business", $purposedata)
                                                            || preg_match('/Business/', $datarow['purpose'])
                                                        ) {
                                                            echo "checked";
                                                        }
                                                        ?>
                                                           name="active[]" required="">
                                                    <label class="my-label" for="Business">Business </label>
                                                </div>
                                            </div>
                                            <div class="purpose-item">
                                                <div class="ckbox ckbox-primary">
                                                    <input autocorrect="off" type="checkbox" id="Parnosso"
                                                           value="Parnosso"
                                                        <?php
                                                        if (in_array("Parnosso", $purposedata)
                                                            || preg_match('/Parnosso/', $datarow['purpose'])
                                                        ) {
                                                            echo "checked";
                                                        }
                                                        ?>
                                                           name="active[]" required="">
                                                    <label class="my-label" for="Parnosso">Parnosso </label>
                                                </div>
                                            </div>
                                            <div class="purpose-item" id="fix">
                                                <div class="ckbox ckbox-primary">
                                                    <input autocorrect="off" type="checkbox" id="Debets" value="Debets"
                                                        <?php
                                                        if (in_array("Debets", $purposedata)
                                                            || preg_match('/Debets/', $datarow['purpose'])
                                                        ) {
                                                            echo "checked";
                                                        }
                                                        ?>
                                                           name="active[]" required="">
                                                    <label class="my-label" for="Debets">Debts</label>
                                                </div>
                                            </div>
                                            <div class="purpose-item">
                                                <div class="ckbox ckbox-primary">
                                                    <input autocorrect="off" type="checkbox" id="Girls" value="Girls"
                                                        <?php
                                                        if (in_array("Girls", $purposedata)
                                                            || preg_match('/Girls/', $datarow['purpose'])
                                                        ) {
                                                            echo "checked";
                                                        }
                                                        ?>
                                                           name="active[]" required="">
                                                    <label class="my-label" for="Girls">Girls <br/> Education
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="purpose-item">
                                                <div class="ckbox ckbox-primary">
                                                    <input autocorrect="off" type="checkbox" id="Sefer" value="Sefer"
                                                        <?php
                                                        if (in_array("Sefer", $purposedata)
                                                            || preg_match('/Sefer/', $datarow['purpose'])
                                                        ) {
                                                            echo "checked";
                                                        }
                                                        ?>
                                                           name="active[]" required="">
                                                    <label class="my-label" for="Sefer">Sefer </label>
                                                </div>
                                            </div>
                                            <div class="purpose-item">
                                                <div class="ckbox ckbox-primary">
                                                    <input autocorrect="off" type="checkbox" id="Chesed" value="Chesed"
                                                        <?php
                                                        if (in_array("Chesed", $purposedata)
                                                            || preg_match('/Chesed/', $datarow['purpose'])
                                                        ) {
                                                            echo "checked";
                                                        }
                                                        ?>
                                                           name="active[]" required="">
                                                    <label class="my-label" for="Chesed">Chesed </label>
                                                </div>
                                            </div>
                                            <div class="purpose-item" id="fix">
                                                <div class="ckbox ckbox-primary">
                                                    <input autocorrect="off" type="checkbox" id="Judaism"
                                                           value="Judaism"
                                                        <?php
                                                        if (in_array("Judaism", $purposedata)
                                                            || preg_match('/Judaism/', $datarow['purpose'])
                                                        ) {
                                                            echo "checked";
                                                        }
                                                        ?>
                                                           name="active[]" required="">
                                                    <label class="my-label" for="Judaism">Judaism
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="purpose-item">
                                                <div class="ckbox ckbox-primary">
                                                    <input autocorrect="off" type="checkbox" id="Adult" value="Adult"
                                                        <?php
                                                        if (in_array("Adult", $purposedata)
                                                            || preg_match('/Adult/', $datarow['purpose'])
                                                        ) {
                                                            echo "checked";
                                                        }
                                                        ?>
                                                           name="active[]" required="">
                                                    <label class="my-label" for="Adult">Adult <br/>Education
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="purpose-item">
                                                <div class="ckbox ckbox-primary">
                                                    <input autocorrect="off" type="checkbox" id="Chinuch"
                                                           value="Chinuch"
                                                        <?php
                                                        if (in_array("Chinuch", $purposedata)
                                                            || preg_match('/Chinuch/', $datarow['purpose'])
                                                        ) {
                                                            echo "checked";
                                                        }
                                                        ?>
                                                           name="active[]" required="">
                                                    <label class="my-label" for="Chinuch">Chinuch</label>
                                                </div>
                                            </div>
                                            <div class="purpose-item width" id="fix">
                                                <div class="ckbox ckbox-primary">
                                                    <input autocorrect="off" type="checkbox" id="Other"
                                                           onclick="purpose_other(this.checked)" value="Other"
                                                        <?php
                                                        if (in_array("Other", $purposedata) || $datarow['purposeother']) {
                                                            echo "checked";
                                                        }
                                                        ?>

                                                           name="active[]" required="">
                                                    <label class="my-label" for="Other">Other </label>
                                                    <div class="col-xs-9">
                                                        <input autocorrect="off" type="text"
                                                               onkeypress="Maxval(this.value,30,this.id)"
                                                               id="target"
                                                            <?php if (!$datarow['purposeother']): ?>
                                                                style="visibility:hidden;"
                                                            <?php endif; ?>
                                                               name="purposeother"
                                                               value="<?php echo $datarow['purposeother']; ?>"
                                                               class="form-control my-input">
                                                        <div id="countertarget" class="my-input-margin"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-3 my-label"
                                                <?php
                                                if (findAssignment($required_fields, 'purpose', 'purposecomments')) {
                                                    echo " style='color:#428bca;' ";
                                                }
                                                ?>
                                            >Public Purpose for Fund Comment:
                                                <?php if ($lastVisitData): ?>
                                                    <br><br>
                                                    <button class="btn btn-primary"
                                                            id="copyPublicComment">Copy prev.
                                                    </button>
                                                <?php endif; ?>
                                            </div>
                                            <div class="col-sm-8">
                                                <textarea autocorrect="off" class="form-control my-input" rows="8"
                                                          cols="15" required placeholder=""
                                                          name="purposecomments"
                                                          id="purposecomments"><?php echo $datarow['purposecomments']; ?></textarea>
                                                <div id="counterpurposecomments" class="purpose-text"></div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-3 my-label"
                                                <?php
                                                if (findAssignment($required_fields, 'purpose', 'purposedonation')) {
                                                    echo " style='color:#428bca;' ";
                                                }

                                                ?>
                                            >We suggest you help the above with a :
                                            </div>
                                            <div class="col-sm-8">
                                                <select onchange="handleSelect()"
                                                        style="border: 1px solid #ccc;" id="purposedonation"
                                                        data-placeholder="Choose One" name="donation"
                                                        class="form-control my-input"
                                                        style="border: none;"
                                                        tabindex="-1" title="" required>
                                                    <option value="">Choose One</option>
                                                    <option
                                                        value="Substantial Donation" <?php if ($datarow['donation'] == 'Substantial Donation' || ($defaultDonation['value'] == 'Substantial Donation') && $data1['donation'] == '') {
                                                        echo 'selected';
                                                    } ?>>Substantial Donation
                                                    </option>
                                                    <option
                                                        value="Generous Donation" <?php if ($datarow['donation'] == 'Generous Donation' || ($defaultDonation['value'] == 'Generous Donation') && $data1['donation'] == '') {
                                                        echo 'selected';
                                                    } ?>>Generous Donation
                                                    </option>
                                                    <option
                                                        value="Standard Donation" <?php if ($datarow['donation'] == 'Standard Donation' || ($defaultDonation['value'] == 'Standard Donation') && $data1['donation'] == '') {
                                                        echo 'selected';
                                                    } ?>>Standard Donation
                                                    </option>
                                                    <option
                                                        value="One Meal Equivalent" <?php if ($datarow['donation'] == 'One Meal Equivalent' || ($defaultDonation['value'] == 'One Meal Equivalent') && $data1['donation'] == '') {
                                                        echo 'selected';
                                                    } ?>>One Meal Equivalent
                                                    </option>
                                                    <option
                                                        value="Token Donation" <?php if ($datarow['donation'] == 'Token Donation' || ($defaultDonation['value'] == 'Token Donation') && $data1['donation'] == '') {
                                                        echo 'selected';
                                                    } ?>>Token Donation
                                                    </option>
                                                    <option
                                                        value="No Special Donation" <?php if ($datarow['donation'] == 'No Special Donation' || ($defaultDonation['value'] == 'No Special Donation') && $data1['donation'] == '') {
                                                        echo 'selected';
                                                    } ?>>No Specific Recommendation
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-3 my-label">Internal Purpose for Fund Comment:
                                                <?php if ($lastVisitData): ?>
                                                    <br><br>
                                                    <button class="btn btn-primary"
                                                            id="copyInternalComment">Copy prev.
                                                    </button>
                                                <?php endif; ?>
                                            </div>
                                            <div class="col-sm-8">
                                                <textarea autocorrect="off" class="form-control my-input"
                                                          rows="8"
                                                          cols="15"
                                                          name="internal_purpose"
                                                          id="internal-purpose"><?php echo $datarow['internal_purpose']; ?></textarea>
                                                <div id="counter-internal-comments" class="purpose-text"></div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-3 my-label">General Comments:
                                                <?php if ($lastVisitData): ?>
                                                    <br><br>
                                                    <button class="btn btn-primary"
                                                            id="copyGeneralComment">Copy prev.
                                                    </button>
                                                <?php endif; ?>
                                            </div>
                                            <div class="col-sm-8">
                                            <textarea autocorrect="off" class="form-control my-input" rows="8"
                                                      cols="15" required placeholder="" name="general_comments"
                                                      id="general-comments"><?php echo $datarow['general_comments']; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- tab-pane -->
                            <div class="tab-pane" id="tab6-4">
                                <div class="first-block">
                                    <div class="row ml10">
                                        <input autocorrect="off" name="memberid" type="hidden" value="<?= $a; ?>">
                                        <input autocorrect="off" name="visitid" type="hidden" value="<?= $b; ?>">
                                        <div class="col-sm-6 my-label">Examination Type : (check all that apply)</div>
                                    </div>
                                    <div class="panel-body">
                                        <div class="row msg-reply">
                                            <div class="col-sm-2">
                                                <div class="ckbox ckbox-primary">
                                                    <?php $fundsdata = explode(",", $datarow['fundsreference']); ?>
                                                    <input autocorrect="off"
                                                           type="checkbox" <?php if (in_array("Documents", $fundsdata)) {
                                                        echo "checked";
                                                    } ?> id="Documents" value="Documents" name="active1[]">
                                                    <label class="my-label" for="Documents">Documents </label>
                                                </div>
                                            </div>
                                            <div class="col-sm-2">
                                                <div class="ckbox ckbox-primary">
                                                    <input autocorrect="off"
                                                           type="checkbox" <?php if (in_array("Identification", $fundsdata)) {
                                                        echo "checked";
                                                    } ?> id="Identification" value="Identification"
                                                           name="active1[]" required="">
                                                    <label class="my-label" for="Identification">Identification </label>
                                                </div>
                                            </div>
                                            <div class="col-sm-2">
                                                <div class="ckbox ckbox-primary">
                                                    <?php $fundsdata = explode(",", $data1['fundsreference']); ?>
                                                    <input autocorrect="off"
                                                           type="checkbox" <?php if (in_array("References", $fundsdata)) {
                                                        echo "checked";
                                                    } ?> id="References" value="References" name="active1[]"
                                                           required="">
                                                    <label class="my-label" for="References">References </label>
                                                </div>
                                            </div>
                                            <div class="col-sm-1 width">
                                                <div class="ckbox ckbox-primary">
                                                    <?php $fundsdata = explode(",", $data1['fundsreference']); ?>
                                                    <input autocorrect="off" type="checkbox"
                                                           onclick="reference_other(this.checked)"
                                                        <?php if (in_array("Other", $fundsdata)) {
                                                            echo "checked";
                                                        } ?> id="Other1" value="Other1" name="active1[]"
                                                           required="">
                                                    <label class="my-label" for="Other1">Other </label>
                                                    <div class="col-xs-9">
                                                        <input autocorrect="off" id="target1" style="visibility:hidden;"
                                                               onkeypress="Maxval(this.value,30,this.id)" type="text"
                                                               name="referenceother"
                                                               class="form-control textarea-comment"
                                                               value="<?php echo $datarow['referenceother']; ?>"
                                                               placeholder="">
                                                        <div id="countertarget1" class="my-input-margin"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row tab-reference">
                                            <div class="col-sm-2 my-label">Comments
                                            </div>
                                            <div class="col-sm-8">
                                            <textarea autocorrect="off"
                                                      class="form-control textarea-comment tab-reference" rows="8"
                                                      cols="15" placeholder="" name="referencecomments"
                                                      id="referencecomments"><?php echo $datarow['referencecomments']; ?></textarea>
                                                <div id="counterreferencecomments" class="purpose-text"></div>
                                            </div>
                                        </div>
                                    </div><!-- panel-body -->
                                </div>
                            </div><!-- tab-pane -->
                            <div class="tab-pane" id="tab7-4">
                                <div class="">
                                    <div class="col-md-12">
                                        <input autocorrect="off" name="memberid" type="hidden" value="<?= $a; ?>">
                                        <input autocorrect="off" name="visitid" type="hidden" value="<?= $b; ?>">

                                        <div class="row">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title" style="padding: 10px 0px 0px 10px;">
                                                        Photo
                                                        <span style="float:right;"> Print</span</h4>
                                                </div><!-- panel-heading -->
                                                <div class="panel-body">
                                                    <div class="col-md-4">
                                                        <input autocorrect="off" name="imgsrc1" id="imgsrc1"
                                                               type="hidden">
                                                        <input autocorrect="off" id="a1" type="hidden" name="a1"/>
                                                        <input autocorrect="off" id="b1" type="hidden" name="b1"/>
                                                        <div id="my_camera"></div>
                                                        <button id="webButton1" class="btn btn-primary"
                                                                type="button"
                                                                onClick="funsign()">Camera
                                                        </button>
                                                        <button id="webButton2" class="btn btn-success"
                                                                type="button"
                                                                onClick="take_snapshot('<?= $a; ?>','<?= $b; ?>')">
                                                            Take
                                                            Snapshot
                                                        </button>

                                                        <div id="mobilePhotoDiv"
                                                             style="margin-right:5px; float:left; padding-top: 8px; border-radius: 3px; text-align: center; overflow: hidden; width: 125px; height: 37px; background-color: #5eb95e;">
                                                            <div style="color: #fff; font-size: 14px;">Take Snapshot
                                                            </div>

                                                            <input autocorrect="off" type="file"
                                                                   capture="camera"
                                                                   accept="image/*"
                                                                   id="mobilePhoto"
                                                                   name="mobilePhoto"
                                                                   class="mobile-photo-div">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4  picture">

                                                        <h4 class="panel-title" style="padding: 10px 0px 0px 10px;">
                                                            Current Photo</h4>

                                                        <div class="loading" id="current-photo"></div>

                                                        <div id="results" style="display: inline-block;">
                                                            <?php if ($datarow['photoname'] && file_exists($defaultPathPhoto . $datarow['photoname'])): ?>
                                                                <div class="doc-wrapper"
                                                                     id="<?= $datarow['photoname']; ?>">
                                                                    <a href="<?= 'uploads/' . $datarow['photoname']; ?>?v=<?= time() ?>"
                                                                       class="doc-picture"><img class="docPicture"
                                                                                                src="<?= $defaultPathPhoto . $datarow['photoname'] ?>?v=<?= time() ?>"
                                                                                                height="150"></a>
                                                                    <div data-animate-angle="0" data-rotate-angle="0"
                                                                         class="rotate btn btn-warning">ROTATE
                                                                        PICTURE
                                                                    </div>
                                                                    <div class="cross btn btn-danger">DELETE
                                                                        PICTURE
                                                                    </div>
                                                                </div>
                                                            <?php endif; ?>
                                                        </div>

                                                        <div id="preview"
                                                             style="float: left; margin-top: 5%; margin-right: 2%;"></div>

                                                        <script type="text/javascript" src="webcam.js"></script>
                                                        <script language="JavaScript">
                                                            function funsign() {
                                                                Webcam.set({
                                                                    width: 210,
                                                                    height: 210,
                                                                    dest_width: 210,
                                                                    dest_height: 210,
                                                                    image_format: 'jpg',
                                                                    jpeg_quality: 100,
                                                                    fps: 100
                                                                });
                                                                Webcam.attach('#my_camera');
                                                            }
                                                        </script>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div id="sub3" style="float:right;">
                                                            <a id="printRegistrationOfIdentity"
                                                               href="Registration_Identity/Registration_Identity/Registration_Identity.php?a=<?= $a; ?>&&b=<?= $b; ?>"
                                                               style="color:#fff" target="blank" class="">
                                                                <img src="images/pdf_yes1.png"
                                                                     class="btn btn-success"
                                                                     title="REGISTRATION OF IDENTITY"
                                                                     rel="tooltip" style="width:72px; height:56px;">
                                                            </a>
                                                            <a id="printRegistrationOfIdentityFake"
                                                               href="Registration_Identity/Registration_Identity/Registration_Identity.php?a=<?= $a; ?>&&b=<?= $b; ?>"
                                                               style="color:#fff; visibility:hidden;" target="blank"
                                                               class="">

                                                            </a><br/><br/>
                                                            <a id="printCharityInformation"
                                                               href="charity_Baltimore/pdfprint.php?a=<?= $a; ?>&&b=<?= $b; ?>&green=<?= true ?>"
                                                               style="color:#fff" target="blank" class="">
                                                                <img src="images/pdf_yes2.png"
                                                                     class="btn btn-success"
                                                                     title="CHARITY INFORMATION"
                                                                     rel="tooltip" style="width:72px; height:56px;">
                                                            </a>
                                                            <a id="printCharityInformationFake"
                                                               href="charity_Baltimore/pdfprint.php?a=<?= $a; ?>&&b=<?= $b; ?>&green=<?= true ?>"
                                                               style="color:#fff; visibility:hidden;" target="blank"
                                                               class="">

                                                            </a><br/><br/>
                                                            <a id="printCharityInformationBlank"
                                                               href="charity_Baltimore/pdfprint_blank.php?a=<?= $a; ?>&&b=<?= $b; ?>"
                                                               style="color:#fff; display: none; " target="blank">
                                                                <img src="images/pdf_yes2.png"
                                                                     class="btn btn-success"
                                                                     title="CHARITY INFORMATION BLANK"
                                                                     rel="tooltip" style="width:72px; height:56px;">
                                                            </a>
                                                            <a id="printCharityInformationBlankFake"
                                                               href="charity_Baltimore/pdfprint_blank.php?a=<?= $a; ?>&&b=<?= $b; ?>"
                                                               style="color:#fff; visibility:hidden;" target="blank"
                                                               class="">

                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title" style="padding: 10px 0px 0px 10px;">
                                                        Collector's Signature </h4>
                                                </div><!-- panel-heading -->
                                                <div class="panel-body">
                                                    <?php include "visitssignature.php"; ?>
                                                    <div class="col-sm-6">
                                                        Previously Saved Signature
                                                        <div id="imgDiv">
                                                            <?php $signid = $data1['Id'];
                                                            $sql1 = $dbh->prepare("select * from visitstable where Id='$signid'");
                                                            $sql1->execute();
                                                            $data1 = $sql1->fetch();
                                                            if ($data1['signname'] != '') {
                                                                echo '<img style="width:350px;" src="' . $defaultPathSign . $data1['signname'] . '">';
                                                            }
                                                            ?>
                                                        </div>
                                                        <div id="newUserSignature">
                                                            Updated Signature
                                                            <div>
                                                                <img id="saveSignature1" form="basicForm"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><!-- panel -->
                                        </div><!-- row -->
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane" id="tab8-4">
                                <div class="">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="loading"></div>
                                            <p></p>
                                            <?php
                                            $no = $_GET['a'];
                                            $sqlquery = $dbh->prepare("select *
                                                               from `visitstable`
                                                               where `collectorsid` = '$no'");
                                            $sqlquery->execute();
                                            $dataquery = $sqlquery->fetch();
                                            ?>
                                            <table id="basicTable"
                                                   class="basic-table table table-striped table-bordered responsive">
                                                <thead class="">
                                                <tr>
                                                    <th style="text-align:center"> Last Name, Names</th>
                                                    <th style="text-align:center"> Reference Id</th>
                                                    <th style="text-align:center"> Visit Id</th>
                                                    <th style="text-align:center"> Authorized From
                                                    </th>
                                                    <th style="text-align:center"> To</th>
                                                    <?php if ($showSite) { ?>
                                                        <th style="text-align:center"> Sites</th>
                                                    <?php } ?>
                                                    <th style="text-align:center"> Status</th>
                                                    <th style="text-align:center"> Edit</th>
                                                    <th style="text-align:center"> Pictures</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                $collectorSql = $dbh->prepare("select * from collectors where Id='$no'");
                                                $collectorSql->execute();
                                                $collecrtorData = $collectorSql->fetch();

                                                $sql2 = $dbh->prepare("
                                                                   SELECT *
                                                                   FROM
                                                                     `visitstable`
                                                                   WHERE
                                                                     `collectorsid` = '$no'
                                                               ");
                                                $sql2->execute();
                                                while ($data2 = $sql2->fetch()) {
                                                    ?>

                                                    <tr class="test <?php if ($data2['visitid'] == $_GET['b']) {
                                                        echo 'visit-row';
                                                    } ?>" style=" ">
                                                        <td style=""><label
                                                                style="visibility:hidden;position: absolute;"><?= $collecrtorData['lastname']; ?></label>
                                                            <?= $collecrtorData['title'] . '  ' . $collecrtorData['firstname'] . ' ' . $collecrtorData['lastname']; ?>
                                                        </td>
                                                        <td>
                                                            <?= $data2['Refid']; ?>
                                                        </td>
                                                        <td>
                                                            <?= $data2['visitid']; ?>
                                                        </td>
                                                        <td>
                                                            <?= $data2['authofromdate']; ?>
                                                        </td>
                                                        <td>
                                                            <?= $data2['authotodate']; ?>
                                                        </td>
                                                        <?php if ($showSite) { ?>
                                                            <td>
                                                                <?php
                                                                $s = $data2['sites'];
                                                                $sqlq = $dbh->prepare("SELECT * FROM `sites` WHERE `Id`='$s'");
                                                                $sqlq->execute();
                                                                $dataq = $sqlq->fetch();
                                                                echo $dataq['Sites'];
                                                                ?>

                                                            </td>
                                                        <?php } ?>
                                                        <td>
                                                            <?php
                                                            $curdate = date('M-d-Y');
                                                            $authdate = $data2['authotodate'];
                                                            $authfrmdate = $data2['authofromdate'];

                                                            if (strtotime($curdate) < strtotime($authfrmdate)) {
                                                                echo 'Planned';
                                                            } elseif (strtotime($curdate) > strtotime($authdate)) {
                                                                echo 'Expired';
                                                            } elseif (strtotime($curdate) >= strtotime($authfrmdate) && strtotime($curdate) <= strtotime($authdate)) {
                                                                echo 'Current';
                                                            } else {
                                                                echo 'Error';
                                                            }
                                                            ?>
                                                        </td>
                                                        <td>
                                                            <a href="emvs.php?action=editvisit1&&a=<?= $data2['collectorsid']; ?>&&b=<?= $data2['visitid']; ?>">
                                                                <img class="visit-edit"
                                                                     src="images/edit1.png"
                                                                     title="edit"
                                                                     data-toggle="tooltip"
                                                                     border="0">
                                                            </a>
                                                        </td>
                                                        <td>
                                                            <?php
                                                            $sql = $dbh->prepare("SELECT * FROM `visitstable` WHERE `collectorsid` = '$no' AND `visitid` = {$data2['visitid']}");
                                                            $sql->execute();
                                                            $visitData = $sql->fetch();
                                                            ?>

                                                            <?php if ($visitData['photoname'] && file_exists('uploads/' . $visitData['photoname'])): ?>
                                                                <a class="image-popup-vertical-fit"
                                                                   href="<?= 'uploads/' . $visitData['photoname']; ?>"
                                                                   id="<?= $visitData['Id'] ?>">view</a>
                                                            <?php else: ?>
                                                                0
                                                            <?php endif; ?>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                    <?php if ($lastVisitData['internal_purpose']): ?>
                                        <div id="last-internal-comment" class="my-block">
                                            <div class="row font-size">
                                                <span style="color:#428bca; font-weight: bold;">Internal Purpose for Fund Comment:&nbsp;&nbsp;</span>
                                                <?= $lastVisitData['internal_purpose'] ?>
                                            </div>
                                        </div>
                                    <?php endif; ?>

                                    <?php if ($lastVisitData['general_comments']): ?>
                                        <div id="last-general-comment" class="my-block">
                                            <div class="row font-size">
                                                <span style="color:#428bca; font-weight: bold;">General Comments:&nbsp;&nbsp;</span>
                                                <?= $lastVisitData['general_comments'] ?>
                                            </div>
                                        </div>
                                    <?php endif; ?>

                                    <div class="my-block">
                                        <div id="docMobilePhotoDiv"
                                             style="margin-right:5px; float:left; padding-top: 8px; border-radius: 3px; text-align: center; overflow: hidden; width: 125px; height: 37px; background-color: #5eb95e;">
                                            <div style="color: #fff; font-size: 14px;">Take Picture
                                            </div>

                                            <input autocorrect="off" type="file"
                                                   capture="camera"
                                                   accept="image/*"
                                                   id="docMobilePhoto"
                                                   name="docMobilePhoto"
                                                   class="mobile-photo-div">
                                        </div>

                                        <div id="coll-documents-galery"
                                             class="coll-documents-gallery">

                                            <?php
                                            $id = $_GET['a'];
                                            $sql = $dbh->prepare('SELECT *
                                                                      FROM `collphoto`
                                                                      WHERE `coll_id` = :id
                                                                      AND `deleted` = 0
                                                                      ORDER BY `id`
                                                                      DESC;');
                                            $sql->execute(['id' => $id]);
                                            $docPictures = $sql->fetchAll();
                                            ?>
                                            <?php if (count($docPictures) > 0): ?>
                                                <?php foreach ($docPictures as $picture): ?>
                                                    <?php if (file_exists('uploads/' . $picture['name'])): ?>
                                                        <div class="doc-wrapper crop picture"
                                                             id="<?= $picture['name']; ?>">
                                                            <div class="loading"></div>
                                                            <a href="<?= 'uploads/' . $picture['name']; ?>?v=<?= time() ?>"
                                                               class="doc-picture"><img
                                                                    class="docPicture"
                                                                    src="<?= 'uploads/' . $picture['name']; ?>?v=<?= time() ?>"
                                                                    height="150px"></a>
                                                            <div data-animate-angle="0" data-rotate-angle="0"
                                                                 class="rotate btn btn-warning">ROTATE PICTURE
                                                            </div>
                                                            <div class="cross btn btn-danger">DELETE PICTURE
                                                            </div>
                                                        </div>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                            <?php if ($datarow['photoname'] && file_exists($defaultPathPhoto . $datarow['photoname'])): ?>
                                                <div class="doc-wrapper crop picture"
                                                     id="<?= $datarow['photoname']; ?>">
                                                    <div class="loading"></div>
                                                    <a href="<?= $defaultPathPhoto . $datarow['photoname']; ?>?v=<?= time() ?>"
                                                       class="doc-picture"><img class="docPicture"
                                                                                src="<?= $defaultPathPhoto . $datarow['photoname']; ?>?v=<?= time() ?>"
                                                                                height="150px"></a>
                                                    <div data-animate-angle="0" data-rotate-angle="0"
                                                         class="rotate btn btn-warning">ROTATE PICTURE
                                                    </div>
                                                    <div class="cross btn btn-danger">DELETE PICTURE</div>
                                                </div>
                                            <?php endif; ?>
                                        </div>

                                    </div>
                                </div><!-- tab-content -->
                            </div><!-- tab-pane -->
                        </div><!-- row -->

                        <form enctype="multipart/form-data" method="post" accept-charset="utf-8" class="panel-wizard"
                              novalidate="novalidate" name="basicForm" id="basicForm">
                        </form>

</section>

<!--DataTables-->
<script src="js/jquery-migrate-1.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/modernizr.min.js"></script>
<script src="js/ppace.min.js"></script>

<script src="js/jquery.cookies.js"></script>

<script src="js/jquery.dataTables.min.js"></script>
<script src="js/dataTables.bootstrap.js"></script>
<script src="js/dataTables.responsive.js"></script>
<script src="js/select2.min.js"></script>

<script src="js/custom.js"></script>

<script src="signature.js"></script>

<script type="text/javascript" src="js/jquery.limit.js"></script>

<script>
    function removephne(id) {
        if (confirm("Do you really wish to delete this record?")) {
            var visitid = $('#the-visit-id').val();
            var type = 'contact';
            var dataString = 'id=' + id +
                '&visitid=' + visitid +
                '&type=' + type +
                '&thisTable=visit_details';
            $.ajax({
                type: "POST",
                url: "ajax_delete.php",
                data: dataString,
                success: function (msg) {
                    if (msg == 'true') {
                        $("div[data-id='" + id + "']").css('display', 'none');
                    } else {
                        alert('At least one row of Contact Details is required');
                    }
                }
            });
        }
    }

    //Checking visit id duplication
    function visitIdDuplicationCheck() {
        var collId = $('input[name="memberid"]').val();
        var thisVisitId = $('input[name="visitsid"]').val();
        var visitId = $('#visitId').val();
        var data = 'collId=' + collId + '&visitId=' + visitId + '&thisVisitId=' + thisVisitId;
        $.ajax({
            type: 'POST',
            url: 'visitIdDuplicationCheck.php',
            data: data,
            success: function (msg) {
                if (msg == 'duplication') {
                    $('#visitId').val('');
                    $('#visitIdErr').html('Visit #' + visitId + ' already exists');
                } else if (msg == 'ok') {
                    $('#visitIdErr').html(' ');
                }
            }
        });
    }

    //Checking reference id duplication
    function refIdDuplicationCheck(value, id) {
        var reference = $('#' + id);
        var refId = reference.val();
        var prevId = reference.attr('lang');
        var data = 'refId=' + refId + '&prevId=' + prevId;
        $.ajax({
            type: 'POST',
            url: 'refIdDuplicationCheck.php',
            data: data,
            success: function (msg) {
                if (msg == 'duplication') {
                    reference.parent().find('.ref-id-err').html('Reference Id #' + refId + ' already exists');
                    reference.parent().find('.ref-id-err').css('display', 'inline');
                } else if (msg == 'ok') {
                    reference.parent().find('.ref-id-err').html(' ');
                    reference.parent().find('.ref-id-err').css('display', 'none');
                }
            }
        });
    }

    jQuery(document).ready(function () {
        var collectorId = $('input[name="memberid"]').val();
        var visitId = $('input[name="visitsid"]').val();

        $('#copyPublicComment').on('click', function (event) {
            event.preventDefault();
            getPrevComment('purposecomments', 'purposecomments', collectorId, visitId);
        });

        $('#copyInternalComment').on('click', function (event) {
            event.preventDefault();
            getPrevComment('internal-purpose', 'internal_purpose', collectorId, visitId);
        });

        $('#copyGeneralComment').on('click', function (event) {
            event.preventDefault();
            getPrevComment('general-comments', 'general_comments', collectorId, visitId);
        });

        //Setting duplication check for visit id
        $('#visitId').on('keyup', function () {
            visitIdDuplicationCheck();

        });

        $('#visitId').on('keypress', function (e) {
            var k = e.keyCode;
            return (k == 8 || (k >= 48 && k <= 57) || k == 43);
        });

        //Setting duplication check for ref id
        $('input[name="last-refid"]').on('keyup', function () {
            refIdDuplicationCheck(this.value, this.id);
        });

        $('input[name="refid"]').on('keyup', function () {
            refIdDuplicationCheck(this.value, this.id);
        });

        $('input[name="last-refid"]').on('keypress', function (e) {
            var k = e.keyCode;
            return (k == 8 || (k >= 48 && k <= 57) || k == 43);
        });

        //Setting mask for USA numbers
        $('#contact').find('input[name="country"]').each(function () {
            if ($.trim($(this).val()) == 'USA' || $.trim($(this).val()) == 'usa') {
                $(this).closest('div[data-id]').find('input[name="detail_number"]').simpleMask({
                    'mask': 'usa',
                    'nextInput': false
                });
            }
        });

        //Copying address from the personal tab
        function copyPersonalAddress() {
            var address1 = $('#address1').val();
            var address2 = $('#address2').val();
            var city = $('#city').val();
            var state = $('#state').val();
            var country = $('#country').val();
            var zipcode = $('#zipcode').val();
            var phoneNumber = $("input[name='CPrimary']:checked").closest('.form-group').find("input[name='Phone_Number']").val();

            $('#usaddress1').val(address1);
            $('#usaddress2').val(address2);
            $('#zipcity').val(city);
            $('#tempstate').val(state);
            $('#uscountry').val(country);
            $('#tempzipcode').val(zipcode);
            $('#tempphone').val(phoneNumber);

            $('#usaddress1').trigger('change');
            $('#usaddress2').trigger('change');
            $('#zipcity').trigger('change');
            $('#tempstate').trigger('change');
            $('#uscountry').trigger('change');
            $('#tempzipcode').trigger('change');
            $('#tempphone').trigger('change');
        }

        $('#copyPersonalAddress').on('click', copyPersonalAddress);

        //Copying address from US address
        function copyUsAddress() {
            var address1 = $('#usaddress1').val();
            var address2 = $('#usaddress2').val();
            var city = $('#zipcity').val();
            var state = $('#tempstate').val();
            var country = $('#uscountry').val();
            var zipcode = $('#tempzipcode').val();
            var phoneNumber = $("#tempphone").val();

            $('#baltiaddress1').val(address1);
            $('#baltiaddress2').val(address2);
            $('#balticity').val(city);
            $('#baltistate').val(state);
            $('#balticountry').val(country);
            $('#baltizipcode').val(zipcode);
            $('#baltiphone').val(phoneNumber);

            $('#baltiaddress1').trigger('change');
            $('#baltiaddress2').trigger('change');
            $('#balticity').trigger('change');
            $('#baltistate').trigger('change');
            $('#balticountry').trigger('change');
            $('#baltizipcode').trigger('change');
            $('#baltiphone').trigger('change');
        }

        $('#copyUsAddress').on('click', copyUsAddress);

        jQuery('#basicTable').DataTable({
            responsive: true

        });

        var shTable = jQuery('#shTable').DataTable({
            "fnDrawCallback": function (oSettings) {
                jQuery('#shTable_paginate ul').addClass('pagination-active-dark');
            },
            responsive: true
        });

        // Show/Hide Columns Dropdown
        jQuery('#shCol').click(function (event) {
            event.stopPropagation();
        });

        jQuery('#shCol input').on('click', function () {

            // Get the column API object
            var column = shTable.column($(this).val());

            // Toggle the visibility
            if ($(this).is(':checked'))
                column.visible(true);
            else
                column.visible(false);
        });

        // Add event listener for opening and closing details
        jQuery('#exRowTable tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = exRowTable.row(tr);

            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            }
            else {
                // Open this row
                row.child(format(row.data())).show();
                tr.addClass('shown');
            }
        });


        // DataTables Length to Select2
        jQuery('div.dataTables_length select').removeClass('form-control input-sm');
        jQuery('div.dataTables_length select').css({width: '60px'});
        jQuery('div.dataTables_length select').select2({
            minimumResultsForSearch: -1
        });

    });

    function format(d) {
        // `d` is the original data object for the row
        return '<table class="table table-bordered nomargin">' +
            '<tr>' +
            '<td>Full name:</td>' +
            '<td>' + d.name + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Extension number:</td>' +
            '<td>' + d.extn + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Extra info:</td>' +
            '<td>And any further details here (images etc)...</td>' +
            '</tr>' +
            '</table>';
    }

    $(document).ready(function () {
        myfun();

        $('#siteshost').on('change', function () {
            var object = $(this);
            hostChange(object);
        });

        $('input[name="free_formed_host"]').on('change', function () {
            var object = $(this);
            hostChange(object);
        });

        //setting comment length limit
        $("#purposecomments").limit({
            limit: 370,
            id_result: "counterpurposecomments",
            alertClass: "alert"
        });

        //setting comment length limit
        $("#internal-purpose").limit({
            limit: 510,
            id_result: "counter-internal-comments",
            alertClass: "alert"
        });

        //setting comment length limit
        $("#referencecomments").limit({
            limit: 890,
            id_result: "counterreferencecomments",
            alertClass: "alert"
        });

        //getting user's time zone
        var timeZone = jstz.determine();
        var zone = timeZone.name();
        var format = 'MM-DD-YYYY';
        var today = moment().tz(zone).format(format);

        //adding today's date to the pdf links
        var link;
        var registration = $('#printRegistrationOfIdentityFake');
        link = registration.attr('href');
        link = link + '&&c=' + today;
        registration.attr('href', link);

        var charity = $('#printCharityInformationFake');
        link = charity.attr('href');
        link = link + '&&c=' + today;
        charity.attr('href', link);

        $('body').on('click', '.cross', deleteDoc);
        $('body').on('click', '.rotate', rotateDoc);

        $('#view-alert').magnificPopup({
            type: 'inline',
            preloader: false
        });

        $('.image-popup-vertical-fit').magnificPopup({
            type: 'image',
            closeOnContentClick: true,
            mainClass: 'mfp-img-mobile',
            image: {
                verticalFit: true
            }
        });

        $('#coll-documents-galery').magnificPopup({
            delegate: 'a',
            type: 'image',
            tLoading: 'Loading image #%curr%...',
            mainClass: 'mfp-img-mobile',
            gallery: {
                enabled: true,
                navigateByImgClick: true,
                preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
            },
            image: {
                tError: '<a href="%url%">The image #%curr%</a> could not be loaded.'
            }
        });

        $('#results').magnificPopup({
            delegate: 'a',
            type: 'image',
            tLoading: 'Loading image #%curr%...',
            mainClass: 'mfp-img-mobile',
            gallery: {
                enabled: true,
                navigateByImgClick: true,
                preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
            },
            image: {
                tError: '<a href="%url%">The image #%curr%</a> could not be loaded.'
            }
        });

        $('#preview').magnificPopup({
            delegate: 'a',
            type: 'image',
            tLoading: 'Loading image #%curr%...',
            mainClass: 'mfp-img-mobile',
            gallery: {
                enabled: true,
                navigateByImgClick: true,
                preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
            },
            image: {
                tError: '<a href="%url%">The image #%curr%</a> could not be loaded.'
            }
        });

        $('.popup-gallery').magnificPopup({
            delegate: 'a',
            type: 'image',
            tLoading: 'Loading image #%curr%...',
            mainClass: 'mfp-img-mobile',
            closeOnBgClick: true,
            gallery: {
                enabled: true,
                navigateByImgClick: true,
                preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
            },
            image: {
                tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
                titleSrc: function (item) {
                    return item.el.attr('title') + '<small>by Marsel Van Oosten</small>';
                }
            }
        });

        $('.visit-edit').on('click', function (event) {
            if (!confirm('Are you sure you want to change visits?')) {
                event.preventDefault();
            }
        });

        function blockSpecialCharNum(e) {
            var k = e.keyCode;
            return (k == 8 || (k >= 48 && k <= 57) || k == 40 || k == 41 || k == 45 || k == 43);
        }

        $('#docMobilePhoto').on('click', function (event) {
            photoResize(this.id)
        });

        $('#mobilePhoto').on('click', function (event) {
            photoResize(this.id)
        });

        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
            $("#mobilePhotoDiv").css("display", "inline-block");
            $("#preview").css("display", "inline-block");
            $("#webButton1").css("display", "none");
            $("#webButton2").css("display", "none");
        } else {
            $("#preview").css("display", "none");
            $("#mobilePhotoDiv").css("display", "none");
        }
    });

    $(function () {
        $("#printRegistrationOfIdentity").on("click",

            function (event) {
                event.preventDefault();

                var fakeToBeCalled = "#printRegistrationOfIdentityFake";
                validateBeforePrintPost(fakeToBeCalled);

            });

        $("#printCharityInformation").on("click",

            function (event) {

                event.preventDefault();

                var fakeToBeCalled = "#printCharityInformationFake";
                validateBeforePrintPost(fakeToBeCalled);

            });

        $("#printCharityInformationBlank").on("click",

            function (event) {

                event.preventDefault();

                var fakeToBeCalled = "#printCharityInformationBlankFake";
                validateBeforePrintPost(fakeToBeCalled);

            });

    });

    function validateBeforePrintPost(fakeToBeCalled) {
        //var redirectWindow = window.open('http://google.com', '_blank');
        $.post(
            "check.php",
            {
                requiredFields: "required"
            }
        ).done(function (response) {
            if (response != "ERROR") {
                validateBeforePrintProcessing(response, fakeToBeCalled);
            } else {
                allowPrintProcessing(fakeToBeCalled);
            }
        });
    }

    function allowPrintProcessing(fakeToBeCalled) {
        $link = $(fakeToBeCalled);
        $link[0].click();
    }

    function validateBeforePrintProcessing(data, fakeToBeCalled) {

        var printable = true;

        var tabs = {
            visits: "Visits",
            collectorDetails: "Personal Info",
            address: "Address",
            funds: "Funds",
            purpose: "Purpose",
            reference: "Reference"
        };

        var tabAssignments = Object.keys(tabs);

        for (i = 0; i < tabAssignments.length; i++) {

            var tabToHighlight = "#" + tabAssignments[i] + "Tab a";
            $(tabToHighlight).css("text-decoration", "none");
            $(tabToHighlight).css("color", "white");
        }
        var required_fields_list = jQuery.parseJSON(data);

        for (var i = 0; i < required_fields_list.length; i++) {

            var el = "[name='" + required_fields_list[i].assignment + "']";

            if ($(el).get(0) !== undefined) {
                var isChecked = false;
                var elVal = $(el).val();
                var elType = $(el).get(0).tagName;
                var name = required_fields_list[i].assignment;
                if (name == 'active[]') {
                    var isChecked = $(el).is(':checked') ? false : true;
                } else {
                    isChecked = false;
                }
                if (isChecked || $(el).val() == "" || elVal == null) {

                    //console.log("empty on the tab " + tabs[required_fields_list[i].tab]);
                    var tabToHighlight = "#" + required_fields_list[i].tab + "Tab a";
                    $(tabToHighlight).css("text-decoration", "underline");
                    $(tabToHighlight).css("color", "red");

                    printable = false;
                }
            }
        }

        if (printable) {
            $link = $(fakeToBeCalled);
            $link[0].click();
        } else {
            alert("Please, fill required fields to print out the document");
        }
    }

    $("#valWizard").on("submit",
        function () {

            var hasClass = $("#purposeTab").hasClass("active");

            if (hasClass) {

                if ($("input[name='requiredFunds']").val() == 'true') {


                    if (!$("input[name='active[]']:checked").length) {

                        alert("At least one Purpose of funds needs to be checked");
                        return false;
                    }
                }
            }
            hasClass = $("#referenceTab").hasClass("active");
            if (hasClass) {
                if ($("input[name='reference']").val() == 'true') {
                    if (!$("input[name='active1[]']:checked").length) {
                        alert("At least one Purpose of funds needs to be checked");
                        return false;
                    }
                }
            }

        });
</script>

<script src="javascript-image-upload/vendor/canvas-to-blob.min.js"></script>
<script src="javascript-image-upload/resize.js"></script>
<?php
echo '<script src="javascript-image-upload/app.js?v=' . time() . '" ></script>';
?>

<script>
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });

    $(function () {

        var targets = $('[rel~=tooltip]'),
            target = false,
            tooltip = false,
            title = false;

        targets.bind('mouseenter', function () {
            target = $(this);
            tip = target.attr('title');
            tooltip = $('<div id="tooltip"></div>');

            if (!tip || tip == '')
                return false;

            target.removeAttr('title');
            tooltip.css('opacity', 0)
                .html(tip)
                .appendTo('body');

            var init_tooltip = function () {
                if ($(window).width() < tooltip.outerWidth() * 1.5)
                    tooltip.css('max-width', $(window).width() / 2);
                else
                    tooltip.css('max-width', 340);

                var pos_left = target.offset().left + ( target.outerWidth() / 2 ) - ( tooltip.outerWidth() / 2 ),
                    pos_top = target.offset().top - tooltip.outerHeight() - 20;

                if (pos_left < 0) {
                    pos_left = target.offset().left + target.outerWidth() / 2 - 20;
                    tooltip.addClass('left');
                }
                else
                    tooltip.removeClass('left');

                if (pos_left + tooltip.outerWidth() > $(window).width()) {
                    pos_left = target.offset().left - tooltip.outerWidth() + target.outerWidth() / 2 + 20;
                    tooltip.addClass('right');
                }
                else
                    tooltip.removeClass('right');

                if (pos_top < 0) {
                    var pos_top = target.offset().top + target.outerHeight();
                    tooltip.addClass('top');
                }
                else
                    tooltip.removeClass('top');

                tooltip.css({left: pos_left, top: pos_top})
                    .animate({top: '+=10', opacity: 1}, 50);
            };

            init_tooltip();
            $(window).resize(init_tooltip);

            var remove_tooltip = function () {
                tooltip.animate({top: '-=10', opacity: 0}, 50, function () {
                    $(this).remove();
                });

                target.attr('title', tip);
            };

            target.bind('mouseleave', remove_tooltip);
            tooltip.bind('click', remove_tooltip);
        });
    });
</script>

<!--jQuery Image Rotate-->
<script src="js/jquery-rotate.js"></script>
<!-- Magnific Popup core JS file -->
<script src="magnific-popupp/dist/jquery.magnific-popup.js"></script>
<!--Magnific Popup min JS file-->
<script src="magnific-popupp/dist/jquery.magnific-popup.min.js"></script>
