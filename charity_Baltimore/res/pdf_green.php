<?php
error_reporting(0);
include '../../includes/dbcon.php';
session_start();
$a = $_GET['a'];
$b = $_GET['b'];

$season = [
    'Jul' => ['#FF0000', 'Summer'],
    'Jun' => ['#FF0000', 'Summer'],
    'Aug' => ['#FF0000', 'Summer'],
    'Sep' => ['#D57500', 'Fall'],
    'Oct' => ['#D57500', 'Fall'],
    'Nov' => ['#D57500', 'Fall'],
    'Dec' => ['#FCFF94', 'Winter'],
    'Jan' => ['#FCFF94', 'Winter'],
    'Feb' => ['#FCFF94', 'Winter'],
    'Mar' => ['#00B300', 'Spring'],
    'Apr' => ['#00B300', 'Spring'],
    'May' => ['#00B300', 'Spring']
];

$seasonNumeric = [
    '07' => ['#FF0000', 'Summer'],
    '06' => ['#FF0000', 'Summer'],
    '08' => ['#FF0000', 'Summer'],
    '09' => ['#D57500', 'Fall'],
    '10' => ['#D57500', 'Fall'],
    '11' => ['#D57500', 'Fall'],
    '12' => ['#FCFF94', 'Winter'],
    '01' => ['#FCFF94', 'Winter'],
    '02' => ['#FCFF94', 'Winter'],
    '03' => ['#00B300', 'Spring'],
    '04' => ['#00B300', 'Spring'],
    '05' => ['#00B300', 'Spring']
];

$sql = $dbh->prepare("select * from visitstable where collectorsid=$a && visitid=$b order by visitid desc");

$sql->execute();

$data = $sql->fetch();

$sql = $dbh->prepare("select * from visitstable where collectorsid=$a && visitid=$b - 1 order by visitid desc");
$sql->execute();
$previousVisitData = $sql->fetch();

$sql = $dbh->prepare("SELECT * FROM `collectors` WHERE `id` = :id; ");
$sql->execute([':id' => $a]);
$collData = $sql->fetch();

$query = $dbh->prepare(
    "
	  SELECT
	  *
	  FROM
	  `app_options`
	  WHERE
	  	`app_options`.`key` = 'default_path_photo'
	  "
);

$query->execute();

$defaultPathPhoto = $query->fetch();
$defaultPathPhoto = $defaultPathPhoto['value'] == '' ? "uploads/" : $defaultPathPhoto['value'];

$query = $dbh->prepare(
    "
	  	SELECT
	  	*
	  	FROM
	  	`app_options`
	  	WHERE
	  	`app_options`.`key` = 'default_path_sign'
	  "
);
$query->execute();

$defaultPathSign = $query->fetch();
$defaultPathSign = ($defaultPathSign['value'] == '') ? "uploads/" : $defaultPathSign['value'];

function dateFormat($dateStr)
{
    $dateArr = parserDate($dateStr);
    switch ($dateArr['month']) {
        case('Jan'):
            $dateArr['month'] = '01';
            break;
        case('Feb'):
            $dateArr['month'] = '02';
            break;
        case('Mar'):
            $dateArr['month'] = '03';
            break;
        case('Apr'):
            $dateArr['month'] = '04';
            break;
        case('May'):
            $dateArr['month'] = '05';
            break;
        case('Jun'):
            $dateArr['month'] = '06';
            break;
        case('Jul'):
            $dateArr['month'] = '07';
            break;
        case('Aug'):
            $dateArr['month'] = '08';
            break;
        case('Sep'):
            $dateArr['month'] = '09';
            break;
        case('Oct'):
            $dateArr['month'] = '10';
            break;
        case('Nov'):
            $dateArr['month'] = '11';
            break;
        case('Dec'):
            $dateArr['month'] = '12';
            break;
        default:
            $dateArr['month'] = '01';
    }
    return implode('-', [$dateArr['month'], $dateArr['day'], $dateArr['year']]);
}

function hebrewReverse($strings, $separator)
{
    //checking empty elements and creating new array
    $array = [];
    foreach ($strings as $string) {
        if ($string) {
            $array[] = $string;
        }
    }
    //changing words order in hebrew names
    $newArray = [];
    foreach ($array as $item) {
        if (preg_match('/[^\x00-\x7F]/', $item)) {
            $newArray[] = hebrewWrap($item);
        } else {
            $newArray[] = $item;
        }
    }
    return implode($separator, $newArray);
}

function hebrewWrap($text)
{
    return preg_replace('/([^\x00-\x7F][^a-z]+[^\x00-\x7F])/', '<span>$1</span>', $text);
}

function textLimit($text, $limit)
{
    $strlen = strlen($text);
    $lineBreaks = preg_match_all("/\n/", $text);
    $textLength = $strlen + ($lineBreaks * 100);

    if ($textLength >= $limit) {
        $text = preg_replace("/\n/", ' ', $text);

        if (strlen($text) >= $limit) {
            return substr($text, 0, ($limit + $lineBreaks));
        } else {
            return $text;
        }
    } else {
        return $text;
    }
}

?>
<style type="text/css">

    .border-color {
        border-bottom: 1px solid #338f7a;
    }

    .text-color {

    }

</style>
<page style="font-size: 7pt; color: #338f7a;">
    <table style="width: 111%; padding:5px;margin-left:-45px; margin-top:-20px;">
        <tr>
            <td style="width: 100%;">
                <table style="width: 100%;">
                    <tr>
                        <td valign="middle"
                            style="width: 45%; font-size:24px;font-weight:bold;padding-left:20px; padding-bottom: -20px;">
                            AGUDATH ISRAEL
                        </td>
                        <td style="width: 10%;" rowspan="2"><img src="res/imgs/emvs_logo_green.png" width="60"
                                                                 height="60"></td>
                        <td style="padding-left:90px;width: 45%;font-size:24px;font-weight:bold; padding-bottom: -20px;">
                            <br>OF BALTIMORE
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 45%;padding-left:190px;font-size:15px;">CHARITY</td>
                        <td style="padding-left:90px; width: 45%;font-size:15px;">DIVISION</td>

                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="width: 100%;">
                <table style="width: 100%;font-size:12px;">
                    <tr>
                        <td style="width: 65%;">6200 PARK HEIGHTS AVE., BALTIMORE, MD 21215</td>
                        <td style="width: 35%;">
                    <tr>
                        <div style="margin-left: 80px; width: 60px;
                                background-color:
                        <?php
                        $temp = parserDate($data['authofromdate']);

                        if (strlen($temp['month']) == 3) {
                            switch ($temp['month']) {
                                case ('Mar'):
                                    if ($temp['day'] < 20) {
                                        echo '#FCFF94';
                                    } else {
                                        echo $season[$temp['month']][0];
                                    }
                                    break;
                                case ('Jun'):
                                    if ($temp['day'] < 21) {
                                        echo '#00B300';
                                    } else {
                                        echo $season[$temp['month']][0];
                                    }
                                    break;
                                case ('Sep'):
                                    if ($temp['day'] < 22) {
                                        echo '#FF0000';
                                    } else {
                                        echo $season[$temp['month']][0];
                                    }
                                    break;
                                case ('Dec'):
                                    if ($temp['day'] < 21) {
                                        echo '#D57500';
                                    } else {
                                        echo $season[$temp['month']][0];
                                    }
                                    break;
                                default:
                                    echo $season[$temp['month']][0];
                            }
                        } else {
                            switch ($temp[0]) {
                                case ('3'):
                                    if ($temp['day'] < 20) {
                                        echo '#FCFF94';
                                    } else {
                                        echo $seasonNumeric[$temp['month']][0];
                                    }
                                    break;
                                case ('6'):
                                    if ($temp['day'] < 21) {
                                        echo '#00B300';
                                    } else {
                                        echo $seasonNumeric[$temp['month']][0];
                                    }
                                    break;
                                case ('9'):
                                    if ($temp['day'] < 22) {
                                        echo '#FF0000';
                                    } else {
                                        echo $seasonNumeric[$temp['month']][0];
                                    }
                                    break;
                                case ('12'):
                                    if ($temp['day'] < 21) {
                                        echo '#D57500';
                                    } else {
                                        echo $seasonNumeric[$temp['month']][0];
                                    }
                                    break;
                                default:
                                    echo $seasonNumeric[$temp['month']][0];
                            }
                        }
                        ?>;
                                height: 40px;
                                border-radius: 30px / 15px;
                                display: block;
                                position: absolute;
                                top: 100px;
                                left: -65px;
                                text-align: center;
                                color:
                        <?php
                        if (strlen($temp['month']) == 3) {
                            if ($season[$temp['month']][1] !== 'Winter'
                                || ($temp['month'] == 'Dec' && $temp['day'] < 21)
                            ) {
                                echo 'white';
                            }

                            if ($temp['month'] == 'Mar' && $temp['day'] < 20) {
                                echo 'black';
                            }
                        } else {
                            if ($seasonNumeric[$temp['month']][1] !== 'Winter'
                                || ($temp['month'] == '12' && $temp['day'] < 21)
                            ) {
                                echo 'white';
                            }

                            if ($temp['month'] == '3' && $temp['day'] < 20) {
                                echo 'black';
                            }
                        }
                        ?>;">
                            <br>
                            <?php
                            if (strlen($temp['month']) == 3) {
                                switch ($temp['month']) {
                                    case ('Mar'):
                                        if ($temp['day'] < 20) {
                                            echo 'Winter';
                                        } else {
                                            echo $season[$temp['month']][1];
                                        }
                                        break;
                                    case ('Jun'):
                                        if ($temp['day'] < 21) {
                                            echo 'Spring';
                                        } else {
                                            echo $season[$temp['month']][1];
                                        }
                                        break;
                                    case ('Sep'):
                                        if ($temp['day'] < 22) {
                                            echo 'Summer';
                                        } else {
                                            echo $season[$temp['month']][1];
                                        }
                                        break;
                                    case ('Dec'):
                                        if ($temp['day'] < 21) {
                                            echo 'Fall';
                                        } else {
                                            echo $season[$temp['month']][1];
                                        }
                                        break;
                                    default:
                                        echo $season[$temp['month']][1];
                                }
                            } else {
                                switch ($temp['month']) {
                                    case ('Mar'):
                                        if ($temp['day'] < 20) {
                                            echo 'Winter';
                                        } else {
                                            echo $seasonNumeric[$temp['month']][1];
                                        }
                                        break;
                                    case ('Jun'):
                                        if ($temp['day'] < 21) {
                                            echo 'Spring';
                                        } else {
                                            echo $seasonNumeric[$temp['month']][1];
                                        }
                                        break;
                                    case ('Sep'):
                                        if ($temp['day'] < 22) {
                                            echo 'Summer';
                                        } else {
                                            echo $seasonNumeric[$temp['month']][1];
                                        }
                                        break;
                                    case ('Dec'):
                                        if ($temp['day'] < 21) {
                                            echo 'Fall';
                                        } else {
                                            echo $seasonNumeric[$temp['month']][1];
                                        }
                                        break;
                                    default:
                                        echo $seasonNumeric[$temp['month']][1];
                                }
                            }
                            ?>
                        </div>
                    </tr>
                    <table style="width: 30%;">
                        <tr>
                            <td style="padding-left:80px; width: 50%;font-weight:bold;">TEL</td>
                            <td style="width: 30%;font-weight:bold;">:</td>
                            <td style="width: 250%;">(410) 764-7778</td>
                        </tr>
                    </table>
                    </td>

                    </tr>

                </table>
            </td>
        </tr>
        <tr>
            <td style="width: 100%;">
                <table style="width: 100%;font-size:12px;">
                    <tr>
                        <td style="width: 65%;">
                            <table style="width: 30%;">
                                <tr>
                                    <td style="width: 20%;font-weight:bold;">No</td>
                                    <td style="width: 10%;font-weight:bold;">:</td>
                                    <td style="text-align: center; width: 50%;" class="border-color text-color"><?php
                                        if ($data['Refid']) {
                                            echo '&nbsp;' . $data['Refid'];
                                        } else {
                                            echo "{$data['visitid']} - {$data['collectorsid']}";
                                        } ?></td>
                                </tr>
                            </table>
                        </td>
                        <td style="width: 35%;">
                            <table style="width: 20%;">
                                <tr style="">
                                    <td style="padding-left:80px; width: 50%;font-weight:bold;">DATE</td>
                                    <td style="width: 30%;font-weight:bold;">:</td>
                                    <td style="text-align: center; width: 200%;  "
                                        class="border-color text-color"><?= dateFormat($data['regdate']); ?></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="width: 100%;font-size:18px;text-align:center;">AUTHORIZATION TO COLLECT FUNDS</td>
        </tr>
        <tr>
            <td style="width: 100%;font-size:12px;text-align:center;">Subject to conditions on reverse side*</td>
        </tr>
        <tr>
            <td style="width: 100%;font-size:12px;text-align:center;">&nbsp;</td>
        </tr>
        <tr>
            <td style="width: 100%;font-size:11px;text-align:center;">BE IT KNOWN THAT WE EXAMINED THE</td>
        </tr>
        <tr>
            <td style="width: 100%;font-size:12px;text-align:left;">
                <table style="width: 100%;font-size:11px;">
                    <tr>
                        <td style="width: 65%;">
                            <table style="width: 30%;">
                                <?php $fundsdata = explode(",", $data['fundsreference']); ?>
                                <tr>
                                    <td style="width: 10%;"><?php if (in_array("Documents", $fundsdata)) {
                                            echo '<img style="height: 15px;" src="res/imgs/tick.png">';
                                        } else {
                                            echo '<img src="res/imgs/empty-green.png">';
                                        } ?></td>
                                    <td style="width: 75%;">DOCUMENTS</td>
                                    <td style="width: 10%;"><?php if (in_array("Identification", $fundsdata)) {
                                            echo '<img style="height: 15px;" src="res/imgs/tick.png">';
                                        } else {
                                            echo '<img src="res/imgs/empty-green.png">';
                                        } ?></td>
                                    <td style="width: 75%;">IDENTIFICATION</td>
                                    <td style="width: 10%;"><?php if (in_array("References", $fundsdata)) {
                                            echo '<img style="height: 15px;" src="res/imgs/tick.png">';
                                        } else {
                                            echo '<img src="res/imgs/empty-green.png">';
                                        } ?></td>
                                    <td style="width: 75%;">REFERENCES</td>
                                    <td style="width: 10%;"><?php if (in_array("Other1", $fundsdata)) {
                                            echo '<img style="height: 15px;" src="res/imgs/tick.png">';
                                        } else {
                                            echo '<img src="res/imgs/empty-green.png">';
                                        } ?></td>
                                    <td style="width: 30%;">OTHER</td>
                                    <td valign="top"
                                        style="width: 100%;"
                                        class="border-color text-color"><?= $data['referenceother']; ?></td>
                                </tr>
                            </table>
                        </td>

                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="width: 100%;font-size:11px;text-align:left;">
                <table style="width: 100%;font-size:11px;">
                    <tr>
                        <td style="width: 65%;">
                            <table style="width: 30%;">

                                <tr>
                                    <td style="width: 53%;font-weight:bold;">OF (NAME)</td>
                                    <td style="width: 222%;font-size: 17px;font-weight: bolder"
                                        class="border-color text-color">
                                    &lrm;&lrm;
                                    <?php
                                    $strings = [
                                        $data['title'],
                                        $data['firstname'],
                                        $data['lastname']
                                    ];

                                    echo hebrewReverse($strings, ' ');

                                    ?>
                                    </td>
                                    <?php
                                    $colid = $data['collectorsid'];
                                    $sql = $dbh->prepare('SELECT *
                                          FROM `visit_details`
                                          WHERE `detail_type` = :detailtype
                                          AND `type` = :thistype
                                          AND `visit_id` = :visit_id
                                          AND `delete` = 0 LIMIT 1');

                                    $sql->execute([
                                        'visit_id'   => $data['Id'],
                                        'detailtype' => 'contact',
                                        'thistype'   => 'Mobile'
                                    ]);
                                    $Mobile = $sql->fetch();
                                    ?>
                                    <td style="width: 40%;font-weight:bold;">TEMP TEL</td>
                                    <td style="width: 104%;" class="border-color text-color">
                                        <?php
                                        echo $Mobile['detail_number'];
                                        ?>
                                    </td>
                                </tr>
                            </table>
                        </td>

                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="width: 100%;font-size:11px;text-align:left;">
                <table style="width: 100%;font-size:11px;">
                    <tr>
                        <td style="width: 65%;">
                            <table style="width: 30%;">

                                <tr>
                                    <td style="width: 68%;font-weight:bold;">BALT.RESIDENCE</td>
                                    <td style="width: 351%;line-height:1.5" class="border-color text-color">&lrm;&lrm;
                                    <?php
                                    $strings = [
                                        $data['baltiaddress1'],
                                        $data['balticity'],
                                        $data['baltistate'],
                                        $data['balticountry']
                                    ];

                                    if ($data['host']) {
                                        echo hebrewWrap($data['host']) . ' ';
                                    } else {
                                        echo hebrewWrap($data['free_formed_host']) . ' ';
                                    }

                                    echo hebrewReverse($strings, ' , ');
                                    ?>
                                    </td>
                                </tr>
                            </table>
                        </td>

                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="width: 100%;font-size:11px;text-align:left;">
                <table style="width: 100%;font-size:11px;">
                    <tr>
                        <td style="width: 65%;">
                            <table style="width: 30%;">
                                <tr>
                                    <td style="width: 62%;font-weight:bold;">PERM.ADDRESS</td>
                                    <td style="width: 216%;line-height:1.5" class="border-color text-color">&lrm;&lrm;
                                    <?php
                                    $strings = [
                                        $data['address1'],
                                        $data['city'],
                                        $data['statecountry'],
                                        $data['country'],
                                        $data['zipcode']
                                    ];

                                    echo hebrewReverse($strings, ' , ');
                                    ?>
                                    </td>
                                    <td style="width: 20%;font-weight:bold; text-align: center">TEL</td>
                                    <td style="width: 121%;line-height:1.5" class="border-color text-color">
                                        <?php
                                    $sql = $dbh->prepare('SELECT *
                                          FROM `visit_details`
                                          WHERE `visit_id` = :visitid
                                          AND `detail_type` = :detailtype
                                          AND `type` = :thistype
                                          AND `delete` = 0 LIMIT 1;');

                                    $sql->execute([
                                        'visitid'    => $data['Id'],
                                        'detailtype' => 'contact',
                                        'thistype'   => 'Home'
                                    ]);
                                    $Home = $sql->fetch(); ?>
                                    <?= '&nbsp;' . $Home['detail_number']; ?>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr>
            <td style="width: 100%;font-size:12px;text-align:left;">
                <table style="width: 100%;font-size:12px;">
                    <tr>
                        <td style="width:100%;font-size:12px;text-align:center;">
                            <table style="width:30%;margin-top:-12px;" align="center">
                                <tr>
                                    <td style="width: 250%;font-size:10px;line-height:1.5;">
                                        AND HAVE FOUND THEM TO BE IN GOOD STANDING <br><br>
                                        THE ABOVE NAMED VOLUNTEER HAS BEEN AUTHORIZED TO COLLECT FUNDS<br>
                                        WITHIN THE GREATER BALTIMORE COMMUNITY FOR THE AGUDATH ISRAEL CHARITY FUND,
                                        FOR<br>
                                        GENERAL CHARITY PURPOSES INCLUDING, BUT NOT LIMITED TO, THE PURPOSE STATED BELOW<br>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="padding-left: -90px; width: 100%;font-size:12px;text-align:left;">
                <table style="width: 100%;font-size:12px;" align="center">
                    <tr>
                        <td style="width:100%;font-size:12px;text-align:center;">
                            <table style="width:30%;margin-top:0px;" align="center">
                                <tr>
                                    <?php if ($data['funds'] == 'Individual'): ?>
                                        <td colspan="5" style="font-weight: bold; font-size: 14px; ">INDIVIDUAL</td>
                                    <?php elseif ($data['funds'] == 'Institution' || $data['funds'] == ''): ?>
                                        <td colspan="5" style="font-weight: bold; font-size: 14px; ">INSTITUTION</td>
                                    <?php endif; ?>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <?php if ($data['funds'] == 'Institution' || $data['funds'] == ''): ?>
            <tr>
                <td colspan="3">
                    <table style="width: 400%;">
                        <tr>
                            <td valign="top" style="width: 130px;text-align: left; font-weight:bold;">NAME OF
                                INSTITUTION :
                            </td>
                            <td style="font-size: 9pt; width: 617px; " class="border-color text-color">&lrm;
                                <?php
                                $institName = $data['institname'];
                                if (preg_match('/[^\x00-\x7F]/', $data['institname'])) {
                                    $parts = explode(' ', $data['institname']);
                                    $institName = implode(' ', array_reverse($parts));
                                }
                                echo $institName;
                                ?>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="3">

                    <table style="width: 400%;">
                        <tr>
                            <td style="width: 65px;font-weight:bold;">ADDRESS :</td>
                            <td style="font-size: 9pt; width: 400px; " class="border-color text-color">&lrm;
                                <?php
                                $words = [
                                    $data['institaddress1'],
                                    $data['institaddress2'],
                                    $data['institcity'],
                                    $data['institstate'],
                                    $data['institcountry'],
                                    $data['institzip']
                                ];

                                echo hebrewReverse($words, ' , ');
                                ?>
                            </td>
                            <td style="width: 32px; font-weight:bold;"> TEL :</td>
                            <td style="font-size: 9pt; width: 237px; " class="border-color text-color"
                            "> <?= $data['institphone'] ?></td>
                        </tr>
                    </table>
                </td>
            </tr>
        <?php endif; ?>
        <tr style="width: 30%">
            <td style="width: 100%;font-size:12px;text-align:left;">
                <table style="width: 100%;font-size:10px;">
                    <tr>
                        <td style="width: 100%;"><?php $purposedata = explode(",", $data['purpose']); ?>
                            <table align="center" style="text-align: center; width: 100%;">
                                <tr>
                                    <td>THE PURPOSE OF THESE FUNDS WILL BE USED FOR:</td>
                                </tr>
                                <tr>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td style="font-size: 16px; font-weight: bold">
                                        <?php
                                        $purposes = [];
                                        foreach ($purposedata as $purpose) {
                                            if (in_array($purpose, $purposedata)) {
                                                switch ($purpose) {
                                                    case('Hanchnosas'):
                                                        $purposes[] = 'HACHNOSAS KALLAH';
                                                        break;
                                                    case('Torah'):
                                                        $purposes[] = 'TORAH EDUCATION';
                                                        break;
                                                    case('Girls'):
                                                        $purposes[] = 'GIRLS EDUCATION';
                                                        break;
                                                    case('Adult'):
                                                        $purposes[] = 'ADULT EDUCATION';
                                                        break;
                                                    case('Debets'):
                                                        $purposes[] = 'DEBTS';
                                                        break;
                                                    case('Other'):
                                                        $purposes[] = 'OTHER (' . $data['purposeother'] . ')';
                                                        break;
                                                    default:
                                                        $purposes[] = strtoupper($purpose);
                                                }
                                            }
                                        }
                                        $purposes = implode(' and ', $purposes);
                                        $purposes = trim($purposes, ' and ');
                                        echo $purposes;
                                        ?>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="width: 100%;font-size:12px;text-align:left;">
                <table style="width: 100%;font-size:12px;">
                    <tr>
                        <td style="width:100%;font-size:12px;text-align:center;">
                            <table style="width:30%;" align="center">
                                <tr>
                                    <td style="width: 100%;font-size:10px;line-height:1.5;">
                                        YOU MAY WANT TO HELP WITH A
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="width: 100%;font-size:12px;text-align:left;">
                <table style="width: 100%;font-size:11px;">
                    <tr>
                        <td style="width: 65%;">
                            <table style="width: 30%;">
                                <tr>
                                    <td style="width: 2%;"><?php if ($data['donation'] == 'Substantial Donation') {
                                            echo '<img src="res/imgs/tick.png">';
                                        } else {
                                            echo '<img src="res/imgs/empty-green.png">';
                                        } ?></td>
                                    <td style="width: 58%;">SUBSTANTIAL DONATION</td>
                                    <td style="width: 2%;"><?php if ($data['donation'] == 'Generous Donation') {
                                            echo '<img style="width:15px; height:15px;" src="res/imgs/tick.png">';
                                        } else {
                                            echo '<img src="res/imgs/empty-green.png">';
                                        } ?></td>
                                    <td style="width: 58%;">GENEROUS DONATION</td>
                                    <td style="width: 2%;"><?php if ($data['donation'] == 'Standard Donation' || $data['donation'] == '') {
                                            echo '<img src="res/imgs/tick.png">';
                                        } else {
                                            echo '<img src="res/imgs/empty-green.png">';
                                        } ?></td>
                                    <td style="width: 67%;">STANDARD DONATION</td>
                                    <td style="width: 2%;"><?php if ($data['donation'] == 'One Meal Equivalent') {
                                            echo '<img src="res/imgs/tick.png">';
                                        } else {
                                            echo '<img src="res/imgs/empty-green.png">';
                                        } ?></td>
                                    <td style="width: 57%;">ONE MEAL EQUIVALENT</td>
                                    <td style="width: 2%;"><?php if ($data['donation'] == 'Token Donation') {
                                            echo '<img src="res/imgs/tick.png">';
                                        } else {
                                            echo '<img src="res/imgs/empty-green.png">';
                                        } ?></td>
                                    <td style="width: 57%;"> TOKEN<br>DONATION</td>
                                    <td style="width: 2%;"><?php if ($data['donation'] == 'No Special Donation') {
                                            echo '<img src="res/imgs/tick.png">';
                                        } else {
                                            echo '<img src="res/imgs/empty-green.png">';
                                        } ?></td>
                                    <td style="width: 69%;">NO SPECIFIC RECOMMENDATION</td>

                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="width: 100%;font-size:10px;">
                <table style="width: 100%;">
                    <tr>
                        <td style="font-weight: bold; font-size: 13.5px">
                            <span class="text-color">
                                <?php
                                if ($data['purposecomments']) {
                                    $purpComments = textLimit($data['purposecomments'], 370);

                                    $order = array("\r\n", "\n", "\r");
                                    $replace = '<br/>';
                                    $purpComments = str_replace($order, $replace, $purpComments);

                                    echo hebrewWrap($purpComments);
                                }
                                ?></span>
                            <br><br>
                            <div style="width:99%; font-size:10px; text-align: center; font-weight: normal; ">
                                IN THE MERIT OF THE MITZVO OF TZEDOKO MAY YOU AND YOUR FAMILY BE BLESSED WITH
                                LIFE, HEALTH AND HAPPINESS.<br>
                                THIS CERTIFICATE IS THE PROPERTY OF AGUDATH ISRAEL OF BALTIMORE, INC. AND MAY BE
                                RECALLED OR CONFISCATED<br>
                                WITHOUT WARNING AT ANY TIME
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:99%;">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="width: 100%;font-size:12px; padding-top: -14px; ">
                <table style="width: 100%;">
                    <tr>
                        <td valign="top" height="200" width="200" rowspan="5"
                            style="width:49%;font-size:12px; border:1px solid #FFFFFF;">
                            <?php if ($data['photoname'] && file_exists("../" . $defaultPathPhoto . $data['photoname'])) {
                                echo '<img src="../' . $defaultPathPhoto . $data['photoname'] . '" width="290" height="220"/>';
                            } else {
                                echo '<div style="color: slategray; text-align:center; font-size: 20px"><br><br><br><br>Photo</div>';
                            }
                            ?>
                        </td>

                        <td style="width:5%;font-size:12px; padding-left: 2px;">
                            <img src="res/imgs/stamp.png"
                                 style="height: 120px;"/>
                        </td>
                        <td style="height: 60px; width:10%;font-size:12px;">
                            <table style="width: 100%;">
                                <tr>
                                    <td style="width:30%;font-size:12px;">SIGNATURE</td>
                                    <td style="width:5%;font-size:12px;"></td>
                                    <td style="width:66%;font-size:12px; " class="border-color">
                                        <?php
                                        if ($data['signname'] && file_exists('../' . $defaultPathSign . $data['signname'])) {
                                            echo '<img src="../' . $defaultPathSign . $data['signname'] . '" width=120 height=60>';
                                        } else {
                                            echo '<div style="width:180px; height:15px; color: red; font-size: 15px">
                                         Collector\'s signature is missing. Please check if he has one.</div>';
                                        }
                                        ?>
                                    </td>
                                </tr>

                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td style="width:10%;font-size:12px; "></td>
                        <td style="width:39%;font-size:10px; text-align:center;padding-top:-40px;">
                            <br>
                            <div style="margin-left: 1px;">THIS AUTHORIZATION IS VALID FROM</div>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:10%;font-size:12px; "></td>
                        <td style="width:10%;font-size:12px;">
                            <table style="width: 100%;">
                                <tr>
                                    <td style="text-align:center; width:45%;font-size:11px; "
                                        class="border-color text-color"
                                    "><?php
                                    $date = dateFormat($data['authofromdate']);
                                    echo '&nbsp;&nbsp;' . $date;
                                    ?></td>
                                    <td style="width:11%;font-size:11px;text-align:center;"> TO</td>
                                    <td style="text-align:center;width:45%;font-size:12px; "
                                        class="border-color text-color"
                                    "><?php
                                    $date = dateFormat($data['authotodate']);
                                    echo '&nbsp;&nbsp;' . $date;
                                    ?></td>
                                </tr>

                            </table>

                    <tr style="width:100%;font-size:10px; text-align:center;margin-top: 9px">CHECKS SHOULD BE MADE OUT
                        TO AGUDATH ISRAEL CHARITY FUND
                    </tr>
                    </td>
                    </tr>
                    <tr>
                        <td style="width:10%;font-size:12px; ">&nbsp;</td>
                        <td style="width:39%;font-size:12px;">&nbsp;</td>
                    </tr>

                    <tr>
                        <?php
                        $exploded = explode(" ", $data['rabbis']);
                        $rabsql = $dbh->prepare(
                            "
					  SELECT
					  *
					  FROM
					  `newmember`
					  WHERE `Firstname` = '" . $exploded[0] . "'
					  AND `Lastname` = '" . $exploded[1] . "'
					  "
                        );
                        $rabsql->execute();
                        $rabdata = $rabsql->fetch();
                        $rabisign = $rabdata['Id'];
                        $sql1 = $dbh->prepare("select * from rabbissign where signid='$rabisign'");
                        $sql1->execute();
                        $data1 = $sql1->fetch();
                        $signname = $data1['signname'];

                        $loginemail = $_SESSION['user'];
                        $loginsql = $dbh->prepare("SELECT * FROM `newmember` WHERE emailid='$loginemail' AND `Delete` = 0");
                        $loginsql->execute();
                        $logindata = $loginsql->fetch();
                        $r = $logindata['roles'];
                        ?>
                        <td style="width:10%;font-size:12px; ">&nbsp;</td>
                        <td style="width:39%;font-size:12px; text-align:center;" class="border-color">
                            <br>
                            <br>
                            <?php
                            $rabbisName = '';
                            if ($signname && $data1['signid'] && file_exists("../" . $defaultPathSign . $signname)) {
                                echo '<br/><img src="../' . $defaultPathSign . $signname . '" border="0" width="110" height="50">';
                                $rabbisName = $rabdata['Title'] . ' ' . $rabdata['Firstname'] . ' ' . $rabdata['Lastname'];
                            } elseif ($r == 'Manager' && !$signname) {
                                $id = $logindata['Id'];
                                $signName = $dbh->prepare("SELECT `signname` FROM `rabbissign` WHERE `signid` = '$id' AND `signid` != '';");
                                $signName->execute();
                                $sign = $signName->fetch();
                                $rabbisName = $logindata['Title'] . ' ' . $logindata['Firstname'] . ' ' . $logindata['Lastname'];
                                if ($sign && file_exists('../' . $defaultPathSign . $sign[0])) {
                                    echo '<br/><img src="../' . $defaultPathSign . $sign[0] . '" border="0" width="110" height="50">';
                                } else {
                                    echo '<div style="width:300px; height:15px; color: red; font-size: 15px">
                                         Rabbi\'s signature is missing. Please check if he has one.</div>';
                                }
                            } else {
                                echo '<div style="width:300px; height:15px; color: red; font-size: 15px">
                                         Rabbi\'s signature is missing. Please check if he has one.</div><br>';
                            }
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:10%;font-size:12px; ">&nbsp;</td>
                        <td style="width:10%;font-size:12px; ">&nbsp;</td>
                        <td style="width:39%;font-size:10.5px; text-align:center;">AUTHORIZING SIGNATURE
                            / <?= $rabbisName ?>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3" style="width: 100%;font-size:12px; padding-top: -5px; ">
                <table style="width: 100%;">
                    <tr>
                        <td style="width: 100%;font-size:11px;line-height:1.5;text-align:center;">
                            <table align="center" style="width: 120%;border:1px solid #000;">
                                <tr>
                                    <td style="width: 10%;font-size:11px;line-height:1.5;text-align:center; font-weight:bold;">
                                        LAST VISIT
                                    </td>
                                    <td style="width: 1%;font-size:11px;line-height:1.5;text-align:center;">
                                        :
                                    </td>
                                    <td style="width: 10%;font-size:11px;line-height:1.5;text-align:center;">
                                        <?php
                                        if ($data['free_formed_last_visit_date'] && $data['free_formed_last_visit_date'] !== 'First Visit') {
                                            echo dateFormat($data['free_formed_last_visit_date']);
                                        } elseif ($previousVisitData['authofromdate'] && $previousVisitData['authofromdate'] !== 'First Visit') {
                                            echo dateFormat($previousVisitData['authofromdate']);
                                        }
                                        ?>
                                    </td>
                                    <td style="width:15 %;"></td>
                                    <td style="width:10 %;font-size:11px;line-height:1.5;text-align:center; font-weight:bold;">
                                        # :
                                    </td>
                                    <td style="width: 10%;font-size:11px;line-height:1.5;text-align:center;">
                                        <?php
                                        if ($data['free_formed_last_visit_number']) {
                                            echo $data['free_formed_last_visit_number'];
                                        } elseif ($previousVisitData['Refid']) {
                                            echo $previousVisitData['Refid'];
                                        } else {
                                            echo 'First Visit';
                                        }
                                        ?>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3" style="text-align:center;font-size:10px;">ONLY THE ORIGINAL DOCUMENT WITH THE APPROPRIATE
                RAISED AND STAMPED SEALS IS VALID
            </td>
        </tr>

    </table>
    <br><br>
    <img src="res/imgs/newsecondpage1.jpg" style="height: 530px; width: 600px"/><br>
    <img src="res/imgs/secondpage2.png" style="height: 530px; width: 600px"/>
</page>
