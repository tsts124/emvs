<?php

	include '../../includes/dbcon.php';
	$a = $_GET['a'];
	$b = $_GET['b'];

	$sql = $dbh->prepare("select * from visitstable where collectorsid=$a && visitid=$b order by visitid desc");

	$sql->execute();

	$data = $sql->fetch();

	$query = $dbh->prepare(
	  "
	  SELECT
	  *
	  FROM
	  `app_options`
	  WHERE
	  `app_options`.`key` = 'default_path_photo'"
	);

	$query->execute();

	$defaultPathPhoto = $query->fetch();
	$defaultPathPhoto = $defaultPathPhoto['value'] == '' ? 'uploads/' : $defaultPathPhoto['value'];

  $query = $dbh->prepare(
	"
		  SELECT
		  *
		  FROM
		  `app_options`
		  WHERE
		  `app_options`.`key` = 'default_path_sign'
		"
  );
  $query->execute();

  $defaultPathSign = $query->fetch();
  $defaultPathSign = ($defaultPathSign['value'] == '') ? "uploads/" : $defaultPathSign['value'];

$query = $dbh->prepare(
  "
	  	SELECT
	  	*
	  	FROM
	  	`app_options`
	  	WHERE
	  	`app_options`.`key` = 'default_path_sign'
	  "
);
$query->execute();

$defaultPathSign = $query->fetch();
$defaultPathSign = ($defaultPathSign['value'] == '') ? "uploads/" : $defaultPathSign['value'];

?>

<style type="text/css">

  <!--
  	div.zone
  	{
		border: solid 2mm #66AACC;
		border-radius: 3mm;
		padding: 1mm;
		background-color: #FFEEEE;
		color: #440000;
  	}
  	div.zone_over
  	{
		width: 30mm;
		height: 35mm;
		overflow: hidden;
  	}
  -->

</style>

<page style="font-size: 10pt">

  <br>
  <table style="width: 113%;border: solid 5px #fff; padding:5px;margin-left:-45px; margin-top:-20px;" >
	<tr>
	  <td style="width: 100%;">
		<table style="width: 100%;">
		  <tr>
			<td valign="middle" style="width: 45%; font-size:15px;font-weight:bold;padding-left:20px;">&nbsp;</td>
			<td style="width: 10%;"rowspan="2">
			  <img src="images/emvs_logo_blank.png" width="60" height="60">
			</td>
			<td style="width: 45%; padding-left:100px;font-size:15px;font-weight:bold;">&nbsp;</td>
		  </tr>
		  <tr>
			<td style="width: 45%;padding-left:50px;font-size:15px;">&nbsp;</td>
			<td style="width: 45%;padding-left:110px;font-size:15px;">&nbsp;</td>
		  </tr>
		</table></td>
	</tr>
	<tr>
	  <td style="width: 100%;">
		<table style="width: 100%;font-size:12px;">
		  <tr>
			<td style="width: 65%;">&nbsp;</td>
			<td style="width: 35%;"><table style="width: 30%;">
				<tr>
				  <td style="width: 50%;font-weight:bold;">&nbsp;</td>
				  <td style="width: 30%;font-weight:bold;">&nbsp;</td>
				  <td style="width: 250%;">&nbsp;</td>
				</tr>
			  </table></td>

		  </tr>
		</table>
	  </td>
	</tr>
	<tr>
	  <td style="width: 100%;">
		<table style="width: 100%;font-size:12px;">
		  <tr>
			<td style="width: 65%;">
			  <table style="width: 30%;">
				<tr>
				  <td style="width: 20%;font-weight:bold;">&nbsp;</td>
				  <td style="width: 10%;font-weight:bold;">&nbsp;</td>
				  <td style="width: 100%; border-bottom:1px solid #fff;"><?php if($data['Refid']){
						  echo '&nbsp;'.$data['Refid'];
					  }else{
						  echo "{$data['visitid']} - {$data['collectorsid']}";
					  }?></td>
				</tr>
			  </table>
			</td>
			<td style="width: 35%;">
			  <table style="width: 30%;">
				<tr>
				  <td style="width: 50%;font-weight:bold;">&nbsp;</td>
				  <td style="width: 30%;font-weight:bold;">&nbsp;</td>
				  <td style="width: 250%;  border-bottom:1px solid #fff;"><?='&nbsp;'.date("m-d-Y");?></td>
				</tr>
			  </table></td>
		  </tr>
		</table>
	  </td>
	</tr>
	<tr>
	  <td style="width: 100%;font-size:12px;text-align:center;">&nbsp;</td>
	</tr>
	<tr>
	  <td style="width: 100%;font-size:12px;text-align:left;">
		<table style="width: 100%;font-size:12px;">
		  <tr>
			<td style="width: 65%;">
			  <table style="width: 30%;">
				<?php  $fundsdata  = explode(",",$data['fundsreference']); ?>
				<tr>
				  <td style="width: 10%;"><?php if (in_array("Documents", $fundsdata)) {
					  echo '<img src="res/imgs/tick.png">';
					} else {
					  echo '<img src="res/imgs/blank.png">';
					}?></td>
				  <td style="width: 75%;">&nbsp;</td>
				  <td style="width: 10%;"><?php if (in_array("Identification", $fundsdata)) {
					  echo '<img src="res/imgs/tick.png">';
					} else {
					  echo '<img src="res/imgs/blank.png">';
					}?></td>
				  <td style="width: 75%;">&nbsp;</td>
				  <td style="width: 10%;"><?php if (in_array("References", $fundsdata)) {
					  echo '<img src="res/imgs/tick.png">';
					} else {
					  echo '<img src="res/imgs/blank.png">';
					}?></td>
				  <td style="width: 75%;">&nbsp;</td>
				  <td style="width: 10%;"><?php if (in_array("Other1", $fundsdata)) {
					  echo '<img src="res/imgs/tick.png">';
					} else {
					  echo '<img src="res/imgs/blank.png">';
					}?></td>
				  <td style="width: 30%;">&nbsp;</td>
				  <td valign="top" style="width: 121%;border-bottom:1px solid #fff;"><?=$data['referenceother'];?></td>
				</tr>
			  </table></td>

		  </tr>
		</table>
	  </td>
	</tr>
	<tr>
	  <td style="width: 100%;font-size:12px;text-align:left;">
		<table style="width: 100%;font-size:12px;">
		  <tr>
			<td style="width: 65%;">
			  <table style="width: 30%;">
				<tr>
				  <td style="width: 53%;font-weight:bold; color:white;">OF (NAME)</td>
				  <td style="width: 222%;border-bottom:1px solid #fff;"><?php
					  if($data['lastname']!=''){
						echo '&nbsp;'.$data['title'].'&nbsp;'.$data['lastname'].',&nbsp;'.$data['firstname'].'&nbsp;'.$data['middlename'];
					  }
					?>
				  </td>
				  <td style="width: 40%;font-weight:bold; color:white;">TEMP TEL</td>
				  <td style="width: 103%;border-bottom:1px solid #fff;"><?='&nbsp;'.$data['baltiphone'];?></td>
				</tr>
			  </table>
			</td>
		  </tr>
		</table></td>
	</tr>
	<tr>
	  <td style="width: 100%;font-size:12px;text-align:left;">
		<table style="width: 100%;font-size:12px;">
		  <tr>
			<td style="width: 65%;">
			  <table style="width: 30%;">
				<tr>
				  <td style="width: 62%;font-weight:bold; color:white;">BALT.RESIDENCE</td>
				  <td style="width: 354%;border-bottom:1px solid #fff;line-height:1.5"><?php
					  if($data['baltiaddress1']!=''){
						echo '&nbsp;'.$data['baltiaddress1'].',&nbsp;'.$data['baltiaddress2'].',&nbsp;'.$data['balticity'].',&nbsp;'.$data['baltistate'].',&nbsp;'.$data['balticountry'].'&nbsp;-&nbsp;'.$data['baltizip'].'.';
					  }
					?>
				  </td>
				</tr>
			  </table>
			</td>
		  </tr>
		</table>
	  </td>
	</tr>
	<tr>
	  <td style="width: 100%;font-size:12px;text-align:left;">
		<table style="width: 100%;font-size:12px;">
		  <tr>
			<td style="width: 65%;">
			  <table style="width: 30%;">
				<tr>
				  <td style="width: 62%;font-weight:bold; color:white;">PERM.ADDRESS</td>
				  <td style="width: 354%;border-bottom:1px solid #fff;line-height:1.5">
					<?php if($data['address1']!=''){
					  echo '&nbsp;'.$data['address1'].',&nbsp;'.$data['address1'].'.';
					}?>
				  </td>
				</tr>
			  </table>
			</td>
		  </tr>
		</table>
	  </td>
	</tr>
	<tr>
	  <td style="width: 100%;font-size:12px;text-align:left;">
		<table style="width: 100%;font-size:12px;">
		  <tr>
			<td style="width: 65%;">
			  <table style="width: 30%;">
				<tr>
				  <td style="width: 282%;border-bottom:1px solid #fff;line-height:1.5"><?php
						if($data['city']!=''){
					  		echo '&nbsp;'.$data['city'].',&nbsp;'.$data['statecountry'].',&nbsp;'.$data['country'].'&nbsp;-&nbsp;'.$data['zipcode'].'.';
						}
					?>
				  </td>
				  <td style="width: 15%;font-weight:bold; color:white;">TEL</td>
				  <td style="width: 119%;border-bottom:1px solid #fff;line-height:1.5">
					<?php
						$colid = $data['collectorsid'];
						$sqlqyry = $dbh->prepare("select * from collectorphone where collid=$colid && CPrimary!=''");
						$sqlqyry->execute();
						$sqldata = $sqlqyry->fetch();	?>
						<?='&nbsp;'.$sqldata['Phone_Number'];?>
				  </td>
				</tr>
			  </table>
			</td>
		  </tr>
		</table></td>
	</tr>
	<tr>
	  <td style="width: 100%;font-size:12px;text-align:left;">
		<table style="width: 100%;font-size:12px;">
		  <tr>
			<td style="width:100%;font-size:12px;text-align:center;">
			  <table style="width:30%;margin-top:-12px;" align="center">
				<tr>
				  <td style="width: 250%;font-size:11px;line-height:1.5; color:white;">
					AND HAVE FOUND THEM TO BE IN GOOD STANDING <br>
					WE BELIEVE THE ABOVE MENTIONED INDIVIDUAL TO BE HONEST. HIS/HER<br>
					INTEGRITY HAS BEEN VOUCHED FOR BY PEOPLE KNOWN TO US. THIS INDIVIDUAL<br>
					HAS COME TO BALTIMORE FOR THE PURPOSE STATED BELOW.<br>
				  </td>
				</tr>
			  </table>
			</td>
		  </tr>
		</table>
	  </td>
	</tr>
	<tr>
	  <td style="width: 100%;font-size:12px;text-align:left;">
		<table style="width: 100%;font-size:12px;">
		  <tr>
			<td style="width:100%;font-size:12px;text-align:center;">
			  <table style="width:30%;margin-top:0px;" align="center">
				<tr>
				  <td style="width: 1%;font-size:12px;line-height:1.5;"> <!-- ??? -->
					<?php if($data['funds']=='Institution') {
					  echo '<img src="res/imgs/tick.png">';
					} else {
					  echo '<img src="res/imgs/blank.png">';
					}?>
				  </td>
				  <td style="width: 40%;font-size:12px;line-height:1.5; color:white;">
					INSTITUTION
				  </td>
				  <td style="width: 150%;font-size:12px;line-height:1.5;border-bottom:1px solid #fff;">
					<?=$data['institname'];?>
				  </td>
				  <td style="width:5px;"></td>
				  <td style="width: 1%;font-size:12px;line-height:1.5;">
					<?php if($data['funds']=='Individual') {
					  echo '<img src="res/imgs/tick.png">';
					} else {
					  echo '<img src="res/imgs/blank.png">';
					}
					?>
				  </td>
				  <td style="width: 50%;font-size:12px;line-height:1.5; color:white;">
					PERSONAL
				  </td>
				</tr>
			  </table>
			</td>
		  </tr>
		</table></td>
	</tr>
	<tr>
	  <td style="width: 100%;font-size:12px;text-align:left;">
		<table style="width: 100%;font-size:12px;">
		  <tr>
			<td style="width: 65%;">
			  <table style="width: 30%;">
				<tr>
				  <td style="width: 62%;font-weight:bold; color:white;">ADDRESS</td>
				  <td style="width: 354%;border-bottom:1px solid #fff;line-height:1.5"><?php
					if($data['institaddress1']!=''){
					  echo $data['institaddress1'].',&nbsp;'.$data['institaddress2'];
					}
					?>
				  </td>
				</tr>
			  </table>
			</td>
		  </tr>
		</table>
	  </td>
	</tr>
	<tr>
	  <td style="width: 100%;font-size:12px;text-align:left;">
		<table style="width: 100%;font-size:12px;">
		  <tr>
			<td style="width: 65%;">
			  <table style="width: 30%;">
				<tr>
				  <td style="width: 282%;border-bottom:1px solid #fff;line-height:1.5"><?php
					if($data['institcity']!=''){
					  echo $data['institcity'].',&nbsp;'.$data['institstate'].',&nbsp;'.$data['institcountry'].',&nbsp;'.$data['institzip'].'.';
					}?></td>
				  <td style="width: 15%;font-weight:bold; color:white;">TEL</td>
				  <td style="width: 119%;border-bottom:1px solid #fff;line-height:1.5"><?='&nbsp;'.$data['institphone'];?></td>
				</tr>
			  </table></td>

		  </tr>
		</table></td>
	</tr>
	<tr>
	  <td style="width: 100%;font-size:12px;text-align:left;">
		<table style="width: 100%;font-size:11px;margin-top:-0px;">
		  <tr>
			<td style="width: 65%;"><?php  $purposedata  = explode(",",$data['purpose']); ?>
			  <table style="width: 30%;">
				<tr>
				  <td style="width: 2%;"><?php if (in_array("Yeshiva", $purposedata)) {
					  echo '<img src="res/imgs/tick.png">';
					} else {
					  echo '<img src="res/imgs/blank.png">';
					}?>
				  </td>
				  <td style="width: 50%; color:white;">YESHIVA</td>
				  <td style="width: 2%;"><?php if (in_array("Kiruv", $purposedata)) {
					  echo '<img src="res/imgs/tick.png">';
					} else {
					  echo '<img src="res/imgs/blank.png">';
					}?>
				  </td>
				  <td style="width: 40%; color:white;">KIRUV</td>
				  <td style="width: 2%;"><?php if (in_array("Free Loan", $purposedata)) {
					  echo '<img src="res/imgs/tick.png">';
					} else {
					  echo '<img src="res/imgs/blank.png">';
					}?></td>
				  <td style="width: 65%; color:white;">FREE LOAN</td>
				  <td style="width: 2%;"><?php if (in_array("Hanchnosas", $purposedata)) {
					  echo '<img src="res/imgs/tick.png">';
					} else {
					  echo '<img src="res/imgs/blank.png">';
					}?></td>
				  <td valign="middle" style="width: 55%; color:white;"> HACHNOSAS KALLAH</td>
				  <td style="width: 2%;"><?php if (in_array("Medical", $purposedata)) {
					  echo '<img src="res/imgs/tick.png">';
					} else {
					  echo '<img src="res/imgs/blank.png">';
					}?>
				  </td>
				  <td style="width: 52%; color: white;">MEDICAL</td>

				  <td style="width: 2%;"><?php if (in_array("Mental", $purposedata)) {
					  echo '<img src="res/imgs/tick.png">';
					} else {
					  echo '<img src="res/imgs/blank.png">';
					}?>
				  </td>
				  <td style="width: 46%; color:white;">MENTAL</td>
				  <td style="width: 2%;"><?php if (in_array("Torah", $purposedata)) {
					  echo '<img src="res/imgs/tick.png">';
					} else {
					  echo '<img src="res/imgs/blank.png">';
					}?>
				  </td>
				  <td style="width: 46%; color:white;">TORAH EDUCATION</td>
				</tr>
				<tr>
				  <td style="width: 2%;"><?php if (in_array("Kollel", $purposedata)) {
					  echo '<img src="res/imgs/tick.png">';
					} else {
					  echo '<img src="res/imgs/blank.png">';
					}?>
				  </td>
				  <td style="width: 50%; color:white;">KOLLEL</td>
				  <td style="width: 2%;"><?php if (in_array("Charity", $purposedata)) {
					  echo '<img src="res/imgs/tick.png">';
					} else {
					  echo '<img src="res/imgs/blank.png">';
					}?>
				  </td>
				  <td style="width: 40%; color:white;">CHARITY</td>
				  <td style="width: 2%;"><?php if (in_array("Business", $purposedata))  {
					  echo '<img src="res/imgs/tick.png">';
					} else {
					  echo '<img src="res/imgs/blank.png">';
					}?>
				  </td>
				  <td style="width: 65%; color:white;">BUSINESS</td>
				  <td style="width: 2%;"><?php  if (in_array("Parnosso", $purposedata))  {
					  echo '<img src="res/imgs/tick.png">';
					} else {
					  echo '<img src="res/imgs/blank.png">';
					}?></td>
				  <td style="width: 52%; color: white;">PARNOSSO</td>
				  <td style="width: 2%;"><?php if (in_array("Debets", $purposedata))  {
					  echo '<img src="res/imgs/tick.png">';
					} else {
					  echo '<img src="res/imgs/blank.png">';
					}?>
				  </td>
				  <td valign="middle" style="width: 55%; color: white;"> DEBTS</td>
				  <td style="width: 2%;"><?php if (in_array("Girls", $purposedata)) {
					  echo '<img src="res/imgs/tick.png">';
					} else {
					  echo '<img src="res/imgs/blank.png">';
					}?></td>
				  <td style="width: 46%; color: white;"> GIRLS EDUCATION</td>
				  <td style="width: 2%;"><?php if (in_array("Sefer", $purposedata)) {
					  echo '<img src="res/imgs/tick.png">';
					} else {
					  echo '<img src="res/imgs/blank.png">';
					}?></td>
				  <td style="width: 25%; color: white; ">SEFER</td>
				</tr>
				<tr>
				  <td style="width: 2%;"><?php if (in_array("Chesed", $purposedata)) {
					  echo '<img src="res/imgs/tick.png">';
					} else {
					  echo '<img src="res/imgs/blank.png">';
					}?>
				  </td>
				  <td style="width: 50%; color:white;">CHESED</td>
				  <td style="width: 2%;"><?php  if (in_array("Judaism", $purposedata))  {
					  echo '<img src="res/imgs/tick.png">';
					} else {
					  echo '<img src="res/imgs/blank.png">';
					}?></td>
				  <td style="width: 40%; color: white;">JUDAISM</td>
				  <td style="width: 2%;"><?php if (in_array("Adult", $purposedata)) {
					  echo '<img src="res/imgs/tick.png">';
					} else {
					  echo '<img src="res/imgs/blank.png">';
					}?></td>
				  <td style="width: 65%; color:white;"> ADULT EDUCATION</td>
				  <td style="width: 2%;"><?php if (in_array("Chinuch", $purposedata))  {
					  echo '<img src="res/imgs/tick.png">';
					} else {
					  echo '<img src="res/imgs/blank.png">';
					}?>
				  </td>
				  <td style="width: 52%; color:white;">CHINUCH</td>
				  <td style="width: 2%;"><?php if (in_array("Other", $purposedata)) {
					  echo '<img src="res/imgs/tick.png">';
					} else {
					  echo '<img src="res/imgs/blank.png">';
					}?></td>
				  <td style="width: 52%; color:white;">OTHER</td>
				  <td style="width:40%;border-bottom:1px solid #fff;" colspan="4"><?=$data['purposeother'];?></td>
				</tr>

			  </table>
			</td>
		  </tr>
		</table>
	  </td>
	</tr>
	<tr>
	  <td style="width: 100%;font-size:12px;text-align:left;">
		<table style="width: 100%;font-size:12px;">
		  <tr>
			<td style="width:100%;font-size:12px;text-align:center;">
			  <table style="width:30%;" align="center">
				<tr>
				  <td style="width: 100%;font-size:11px;line-height:1.5; color: white;">
					YOU MAY WANT TO HELP WITH A
				  </td>
				</tr>
			  </table>
			</td>
		  </tr>
		</table>
	  </td>
	</tr>
	<tr>
	  <td style="width: 100%;font-size:12px;text-align:left;">
		<table style="width: 100%;font-size:11px;">
		  <tr>
			<td style="width: 65%;">
			  <table style="width: 30%;">
				<tr>
				  <td style="width: 2%;"><?php if($data['donation']=='Substantial Donation') {
					  echo '<img src="res/imgs/tick.png">';
					} else {
					  echo '<img src="res/imgs/blank.png">';
					}?>
				  </td>
				  <td style="width: 58%; color:white;">SUBSTANTIAL DONATION </td>
				  <td style="width: 2%;"><?php if($data['donation']=='Generous Donation') {
					  echo '<img src="res/imgs/tick.png">';
					} else {
					  echo '<img src="res/imgs/blank.png">';
					}?>
				  </td>
				  <td style="width: 58%; color:white;">GENEROUS DONATION</td>
				  <td style="width: 2%;"><?php if($data['donation']=='Standard Donation') {
					  echo '<img src="res/imgs/tick.png">';
					} else {
					  echo '<img src="res/imgs/blank.png">';
					}?>
				  </td>
				  <td style="width: 67%; color:white;">STANDARD DONATION </td>
				  <td style="width: 2%;"><?php if($data['donation']=='One Meal Donation') {
					  echo '<img src="res/imgs/tick.png">';
					} else {
					  echo '<img src="res/imgs/blank.png">';
					}?>
				  </td>
				  <td style="width: 57%; color:white;">ONE MEAL EQUIVALENT</td>
				  <td style="width: 2%;"><?php if($data['donation']=='Token Donation') {
					  echo '<img src="res/imgs/tick.png">';
					} else {
					  echo '<img src="res/imgs/blank.png">';
					}?>
				  </td>
				  <td style="width: 57%; color:white"> TOKEN DONATION</td>
				  <td style="width: 2%;"><?php if($data['donation']=='No Special Donation') {
					  echo '<img src="res/imgs/tick.png">';
					} else {
					  echo '<img src="res/imgs/blank.png">';
					}?>
				  </td>
				  <td style="width: 69%; color: white;">NO SPECIAL RECOMMENDATION</td>
				</tr>
			  </table>
			</td>
		  </tr>
		</table></td>
	</tr>
	<tr>
	  <td style="width: 100%;font-size:12px;">
		<table style="width: 100%;">
		  <tr>
			<td style="width:99%;font-size:12px;">
			  <table style="width:30%;">
				<tr>
				  <td style="width: 340%;font-size:11px;line-height:1.5;text-align:center; color: white;">
					(IF APPLICABLE) WE HAVE DETERMINED THAT THIS ORGANIZATION EXISTS AND IS PERFORMING AN IMPORTANT SERVICE <br>
					FOR RELIGIOUS OR HUMANITARIAN CAUSES. WE ARE ALSO CONVINCED THAT THE ABOVE IS A REPRESENTATIVE OF SAID<br>
					ORGANIZATION <br>
					IN THE MERIT OF TZEDOKO MAY YOU AND YOUR FAMILY BE BLESSED WITH LIFE. HEALTH, AND HAPPINESS<br>
					THIS CERTIFICATE IS THE PROPERTY OF AGUDATH ISREAL OF BALTIMORE.INC. AND MAY BE RECALLED OR CONFISCATED <br>
					WITHOUT WARNING AT ANY TIME


				  </td>
				</tr>
			  </table>
			</td>
		  </tr>
		</table>
	  </td>
	</tr>
	<tr>
	  <td style="width: 100%;font-size:12px;">
		<table style="width: 100%;">
		  <tr>
			<td valign="top" height="200" width="200" rowspan="5" style="width:49%;font-size:12px; border:1px solid #fff;">

			  <?php
			  	if ($data['photoname']!='') {

					$photoname = $data['photoname'];
					if(file_exists("../".$defaultPathPhoto.$photoname)) {

						echo '<img src="../'.$defaultPathPhoto.$photoname . '" width="210" height="210"/>';
					}

			  	} else {
					echo '&nbsp;'.'';
			  	}
			  ?>
			</td>
			<td style="width:30%;font-size:12px; "></td>
			<td style="width:10%;font-size:12px;"><table style="width: 100%;padding-top:-20px;">
				<tr>
				  <td style="width:30%;font-size:12px; color: white;">SIGNATURE</td>
				  <td style="width:5%;font-size:12px;"></td>
				  <td style="width:66%;font-size:12px; border-bottom:1px solid #fff;">
					<?php
					if ($data['sign']!='') {

					  $signname = $data['sign'];
					  if(file_exists("../".$defaultPathSign. $signname)) {
						echo '<img src="../' .$defaultPathSign. $signname . '" width=170 height=50>';
					  }
					}  else {
					  echo '<div style="width:170px; height:50px;">&nbsp;</div>';
					} ?></td>
				</tr>

			  </table></td>
		  </tr>
		  <tr>
			<td style="width:10%;font-size:12px; "></td>
			<td style="width:39%;font-size:11px; text-align:center;padding-top:-10px; color: white;">THIS INFORMATION IS VALID FROM</td>
		  </tr>

		  <tr>
			<td style="width:10%;font-size:12px; "></td>
			<td style="width:10%;font-size:12px;"><table style="width: 100%;padding-top:0px;">
				<tr>
				  <td style="width:45%;font-size:12px; border-bottom:1px solid #fff;"><?='&nbsp;'.$data['authofromdate'];?></td>
				  <td style="width:11%;font-size:12px;text-align:center;"> TO </td>
				  <td style="width:45%;font-size:12px; border-bottom:1px solid #fff;"><?='&nbsp;'.$data['authotodate'];?></td>
				</tr>

			  </table></td>
		  </tr>
		  <tr><?php

			/*$wordcount = str_word_count($data['rabbis']); // old version
			if($wordcount=='1'){
			  $rabiname = $data['rabbis'];
			}else {
			  $rabiname = strstr($data['rabbis'],' ',true);
			}

			$rabsql = $dbh->prepare("select * from newmember where Lastname='$rabiname'");*/

			$exploded = explode(" ", $data['rabbis']); // new version
			$rabsql = $dbh->prepare(
			  "
					  SELECT
					  *
					  FROM
					  `newmember`
					  WHERE Firstname = '".$exploded[1]."'
					  AND Lastname = '".$exploded[0]."'
					  "
			);

			$rabsql->execute();
			$rabdata = $rabsql->fetch();
			$rabisign = $rabdata['Id'];
			$sql1 = $dbh->prepare("select * from rabbissign where signid='$rabisign'");
			$sql1->execute();
			$data1 = $sql1->fetch();
			$signname = $data1['signname'];
			?>
			<td style="width:10%;font-size:12px; "></td>
			<td style="width:39%;font-size:12px; text-align:center;border-bottom:1px solid #fff;padding-top:-10px;">
			  <?php

			  if ($signname!='' && file_exists("../".$defaultPathSign . $signname)) {
				?>
				<img src="../<?= $defaultPathSign; ?><?= $signname; ?>" border="0" width=170 height=50>
			  <?php } else {
				echo '<div style="width:170px; height:50px;">&nbsp;</div>'; 
			  }

			  ?>
			</td>
		  </tr>
		  <tr>
			<td style="width:10%;font-size:12px; "></td>
			<td style="width:39%;font-size:12px; text-align:center; color: white;">AUTHORIZING SIGNATURE
			</td>
		  </tr>
		</table></td>
	</tr>
	<tr>
	  <td colspan="3" style="width: 100%;font-size:12px;">
		<table style="width: 100%;">
		  <tr>
			<td style="width: 100%;font-size:12px;line-height:1.5;text-align:center;">
			  <table align="center" style="width: 120%;border:1px solid #fff;">
				<tr>
				  <td style="width: 10%;font-size:12px;line-height:1.5;text-align:center; font-weight:bold; color: white;">
					LAST VISIT
				  </td>
				  <td style="width: 1%;font-size:12px;line-height:1.5;text-align:center; color: white;">
					:
				  </td>
				  <td style="width: 10%;font-size:12px;line-height:1.5;text-align:center;">
					<?=$data['authotodate'];?>
				  </td>
				  <td style="width:15 %;"> </td>
				  <td style="width:10 %;font-size:12px;line-height:1.5;text-align:center; font-weight:bold; color: white;">
					Visit No # :
				  </td>
				  <td style="width: 5%;font-size:12px;line-height:1.5;text-align:center;">
					<?=$data['visitid'];?>
				  </td>
				</tr>
			  </table>
			</td>
		  </tr>
		</table></td>
	</tr>
	<tr>
	  <td colspan="3" style="text-align:center;font-size:11px; color:white;">ONLY THE ORIGINAL DOCUMENT WITH THE APPROPRIATE RAISED AND STAMPED SEALS IS VALID</td>
	</tr>

  </table>
  <!--<img src="res/imgs/emvs.png" />-->
  <br> <br>
</page>