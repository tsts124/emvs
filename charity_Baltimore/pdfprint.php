<?php
/**
 * HTML2PDF Librairy - example
 *
 * HTML => PDF convertor
 * distributed under the LGPL License
 *
 * @author      Laurent MINGUET <webmaster@html2pdf.fr>
 *
 * isset($_GET['vuehtml']) is not mandatory
 * it allow to display the result in the HTML format
 */
include '../includes/dbcon.php';
// get the HTML

/**
 * @param string $date
 * @return array
 */
function parserDate($date)
{
    $parsedate = [
        'year' => '',
        'month' => '',
        'day' => ''
    ];
    $dates = explode('-', $date);

    switch (count($dates)) {
        case 1:
            $parsedate['year'] = $dates[0];
            break;
        case 2: {
            $parsedate['day'] = $dates[0];
            $parsedate['year'] = $dates[1];
            break;
        }
        case 3: {
            $parsedate['month'] = $dates[0];
            $parsedate['day'] = $dates[1];
            $parsedate['year'] = $dates[2];
            break;
        }
        default:
            break;
    }

    return $parsedate;
}

ob_start();

isset($_GET['green'])
    ? include(dirname(__FILE__) . '/res/pdf_green.php')
    : include(dirname(__FILE__) . '/res/pdf.php');

$content = ob_get_clean();

// convert in PDF
require_once(dirname(__FILE__) . '/html2pdf.class.php');
try {
    $html2pdf = new HTML2PDF('P', 'A4', 'en', true, 'UTF-8', array(15, 5, 15, 5));
    $html2pdf->pdf->SetDisplayMode('fullpage');
    $html2pdf->setDefaultFont('dejavusans');
    $preferences = [
        'Duplex' => 'DuplexFlipLongEdge', // Simplex, DuplexFlipShortEdge, DuplexFlipLongEdge
    ];
    $html2pdf->pdf->setViewerPreferences($preferences);
    $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
    $html2pdf->Output('exemple02.pdf');
} catch (HTML2PDF_exception $e) {
    echo $e;
    exit;
}
