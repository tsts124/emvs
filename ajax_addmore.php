<?php
include 'includes/dbcon.php';
$count = $_POST['countClicks'];
if ($_POST['pagename'] == 'adddrivers') {
    $sqlquery = $dbh->prepare("SELECT `Id` 
FROM  `driversphone` 
ORDER BY  `driversphone`.`Id` DESC ");
    $sqlquery->execute();
    $finishId = $sqlquery->fetch();
    $finishId = $finishId['Id'] + 1;
    $element = base64_decode($_POST['e']);

    if (!$element) {
        $sql = $dbh->prepare("SELECT * FROM `drivers` ORDER BY `Id` DESC; ");
        $sql->execute();
        $drivers = $sql->fetch();
        $element = $drivers['Id'] + 1;
    }

    $sql = $dbh->prepare("INSERT INTO `driversphone` (`Id`, `D_id`, `Addeddate`) VALUES(:id, :element, :now);");
    $sql->execute([':id' => $finishId, ':element' => $element, ':now' => date('Y-m-d')]);

} elseif ($_POST['pagename'] == 'member') {
    $sqlquery = $dbh->prepare("SELECT `id` 
FROM  `collectorphone` 
ORDER BY  `collectorphone`.`Id` DESC ");
    $sqlquery->execute();
    $finishId = $sqlquery->fetch();
    $finishId = $finishId['id'] + $_POST['countClicks'];
    $element = $_POST['abc'];
} elseif ($_POST['pagename'] == 'addhosts') {
    $element = base64_decode($_POST['e']);
    $sqlquery = $dbh->prepare("select * from `hostphone`  order by `hostphone` .`Id` desc");
    $sqlquery->execute();
    $finishId = $sqlquery->fetch();
    $finishId = $finishId['Id'] + $_POST['countClicks'];
} elseif ($_POST['pagename'] == 'editvisit1' || $_POST['pagename'] == 'visitsadd1') {
    $visitid = $_POST['visitid'];

    $sqlquery = $dbh->prepare('INSERT INTO `visit_details` (`visit_id`,
                                                       `detail_type`)
                               VALUES (:visitid,
                                       :detail_type) ; ');
    $sqlquery->execute(['visitid' => $visitid,
        'detail_type' => 'contact']);

    $sqlquery = $dbh->prepare('SELECT *
                               FROM `visit_details`
                               ORDER BY `id`
                               DESC ;');
    $sqlquery->execute();
    $finishId = $sqlquery->fetch();

    $finishId = $finishId['id'];
    $element = $_POST['a'];

} elseif ($_POST['pagename'] == 'addrabbis') {
    $sqlquery = $dbh->prepare("SELECT * 
                               FROM  `managerphone` 
                               ORDER BY `Id` DESC ;");
    $sqlquery->execute();
    $finishId = $sqlquery->fetch();
    $finishId = $finishId['Id'] + $_POST['countClicks'];
    $element = $_POST['rabbisId'];

    $sql = $dbh->prepare('SELECT *
                          FROM  `managerphone`
                          WHERE `D_id` = :id ; ');
    $sql->execute(['id' => $element]);

    $firstContact = $sql->fetch();

} ?>
<div class="row form-group" id="remove<?= $finishId; ?>" data-id="<?= $finishId; ?>" name="newDiv[]">
    <div id="addinput">
        <div class="row col-sm-1 col-xs-1 field input-primary">
            <input autocorrect="off"  name="primary_phone_count_new[]" id="primary_phone_count" value="<?php echo $finishId; ?>"
                   type="hidden">
            <input autocorrect="off"  lang="<?= $finishId; ?>"
                   id="<?= $finishId; ?>"
                   name="<?php if ($_POST['pagename'] == 'adddrivers') {
                       echo 'DPrimary';
                   } else {
                       echo 'CPrimary';
                   } ?>"
                   onclick="newid1(<?= $finishId; ?>,<?= $element ?>)"
                   value="<?= $finishId; ?>" <?php
            switch (true) {
                case (($element == '?action=member' || $element == '')
                    && $count == 1):
                case ($count == 1
                    && ($_POST['pagename'] == 'adddrivers'
                        || ($_POST['pagename'] == 'addrabbis' && $firstContact))):
                    echo 'checked';
                    break;
                default:
                    echo '';
            }
            ?>
                   type="radio">
        </div>
        <div class="row col-sm-1 col-xs-1" style="width: 150px; ">
            <select class="form-control" lang="<?= $finishId; ?>"
                    id="Phonetpe<?= $finishId; ?>"
                    name="Phone_Type[]"
                    onchange="contactCountryFocus(this);
                              chkphonetypeselect(this.value,this.lang);">
                <option value=" " disabled selected>Choose One</option>
                <option>Work</option>
                <option>Home</option>
                <option>Mobile</option>
                <option>Fax</option>
                <option>Other</option>
            </select>
        </div>
        <div class="col-sm-3 col-xs-3">
            <input autocorrect="off"  type="text" style="text-transform:uppercase;"
                   class="form-control valid"
                   lang="<?= $finishId; ?>"
                   onchange="chkphonetypeselect(this.value,this.lang)"
                   onfocus="contactCountryListener(this.value, this.id, this.lang);"
                   onkeypress="Maxval(this.value,30,this.id); return onlyAlphabets(event,this)"
                   id="countrycode<?= $finishId; ?>"
                   name="Phone_Country[]"
                   placeholder="Country">
            <div id="countercountrycode<?= $finishId; ?>"></div>
        </div>
        <div class="row col-sm-3 col-xs-3 field" id='phone_number'>
            <input autocorrect="off"  name="pagename"
                   id="pagename"
                   type="hidden"
                   value="<?= $_POST['pagename']; ?>">
            <?php
            $inputName = '';

            switch ($_POST['pagename']) {
                case ('adddrivers'):
                    $inputName = 'Phone_Number';
                    break;
                case ('addrabbis'):
                case ('addhosts'):
                    $inputName = 'phoneno[]';
                    break;
                default:
                    $inputName = 'Phone_Number[]';
                    break;
            }
            ?>
            <input autocorrect="off"  type="text"
                   name="<?= $inputName ?>"
                   onkeypress="Maxval(this.value,20,this.id); return blockSpecialCharNum(event)"
                   lang="<?= $finishId; ?>"
                   id="phone<?= $finishId; ?>"
                   class="form-control"
                   placeholder="Number">
            <div id="counterphone<?= $finishId; ?>"></div>

            <span id="phonedup<?= $finishId; ?>" class="ValidationErrors phonedup">This phone number is already in use</span>
        </div>

        <div class="row col-sm-1 field" id='phone_number'>
            <a name="removerows[]"
               lang="<?= $finishId; ?>"
               id="removerow<?= $finishId; ?>"
                <?php if ($_POST['pagename'] == 'editvisit1'
                    || $_POST['pagename'] == 'visitsadd1'
                    || $_POST['pagename'] == 'member'
                ): ?>
                    onclick="removephne(this.lang);"
                <?php else: ?>
                    onclick="removephone(this.lang);"
                <?php endif; ?>
               class="add-button"
               title="Remove field">
                <img class="delete-img" src="images/remove-icon.png"/>
            </a>
        </div>
    </div>
</div>
