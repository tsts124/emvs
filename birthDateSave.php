<?php
include 'includes/dbcon.php';

/**
 * @param $year
 * @param null $day
 * @param null $month
 * @return null|string
 */
function createDatefromDB($year, $day = null, $month = null)
{
    $dob = null;

    if ($year && $day && empty($month)) {
        $dob = $day . '-' . $year;
    } elseif ($year && $month && empty($day)) {
        $dob = $month . '- -' . $year;
    } elseif ($year && $day && $month) {
        $dob = $month . '-' . $day . '-' . $year;
    }

    return $dob ? $dob : $year;
}

$month = $_POST['month'];
$day = $_POST['day'];
$year = $_POST['year'];
$collectorId = $_POST['collectorId'];
$visitId = $_POST['visitId'];

$dob = createDatefromDB($year,$day,$month);

//$sql = $dbh->prepare('UPDATE `collectors`
//                      SET `dob` = :dob
//                      WHERE `Id` = :id ');
//$sql->execute([':dob' => $dob, ':id' => $collectorId]);

$sql = $dbh->prepare('UPDATE `visitstable`
                                  SET `dob`= :dob
                                  WHERE `visitid`= :visitId
                                  AND `collectorsid`= :collId ;');
$sql->execute([':dob' => $dob, ':collId' => $collectorId, ':visitId' => $visitId]);
var_dump($_POST);
echo $sql->fetch();
