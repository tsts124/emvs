<?php
include 'includes/header.php';
include 'includes/dbcon.php';
echo $e = base64_decode($_GET['e']);

if ($_SESSION['user'] == '') {
    header('Location: emvs.php?action=index');
}
$sites = addslashes(htmlspecialchars($_POST['sites']));
$active = $_POST['active'];
$noofvisits = addslashes(htmlspecialchars($_POST['noofvisits']));
if (isset($_POST['createsites'])) {
    $sqlid1 = $dbh->prepare("select * from sites where Sites='$sites'");
    $sqlid1->execute();
    $dataid1 = $sqlid1->fetch();
    if ($dataid1['Sites'] == '' && strlen($sites) >= 4) {
        $count = $dbh->exec("INSERT INTO sites(Sites,visitsday,active,Addeddate) VALUES ('$sites','$noofvisits','$active',now())");
        if ($count == '1') {
            $err = '<div class="success1"><p><img src="images/success.png" border="none" width="18" height="18"> Site Added Successfully</p></div>';
            echo '<meta http-equiv="refresh" content="0; URL=emvs.php?action=sites" />';
        }
    } else {
        $err = '<div class="alert alert-danger"> This site is already in use or it has less than 4 characters. </div>';
    }
}
if ($e != '') {
    $sqlq = $dbh->prepare("select * from sites where Id='$e'");
    $sqlq->execute();
    $data1 = $sqlq->fetch();
}
if (isset($_POST['updatesites']) && strlen($sites) >= 4) {
    $count = $dbh->exec("UPDATE sites SET Sites='$sites',visitsday='$noofvisits',active='$active',Updateddate=now() WHERE Id='$e'");
    if ($count == '1') {
        $err = '<div class="success1"><p><img src="images/success.png" border="none" width="18" height="18"> Site Updated Successfully</p></div>';
        echo '<meta http-equiv="refresh" content="0; URL=emvs.php?action=sites" />';
    }
}
?>

<script type="text/javascript" src="js/jquery-1.2.6.min.js"></script>
<SCRIPT type="text/javascript">
    function mysites() {
        var val = document.getElementById("sites").value;

        if (val.length >= 4) {
            var dataString = 'sites=' + val;
            $.ajax({
                type: "POST",
                url: "check.php",
                data: dataString,
                success: function (msg) {
                    $('#status').html('');
                    if (msg == 'OK' && val.length >= 4) {
                        document.getElementById('createsites').disabled = false;
                        document.getElementById('updatesites').disabled = false;
                    }
                    else {
                        document.getElementById('createsites').disabled = true;
                        document.getElementById('updatesites').disabled = true;
                        $("#status").html('<font color="red">The site <strong>' + val + '</strong> is already in use.</font>');
                    }
                }
            });
        } else {
            document.getElementById('createsites').disabled = true;
            document.getElementById('updatesites').disabled = true;
            $("#status").html('<font color="red">The site name should have at least <strong>4</strong> characters.</font>');
        }

    }
</script>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.8.2.js"></script>
<script type="text/javascript">
    $(function () {
        $('#basicForm').keydown(function (e) {
            var key = e.keyCode;
            if (!((key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
                e.preventDefault();
            }
        });
    });
</script>

<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="js/jquery.validate.min.js"></script>

<script>

    (function ($) {

        jQuery.validator.setDefaults({
            errorPlacement: function (error, element) {
                error.appendTo('#invalid-' + element.attr('id'));
            }
        });
        $("#basicForm").validate({
            rules: {

                sites: {
                    required: true,
                    minlength: 3,
                },

                title: "required",
                image: "required",
                subject: "required",
                femail: {
                    required: true,
                    email: true,
                }
            },
            messages: {
                SSN: {
                    required: "Please enter your SSN",
                    minlength: "Invalid SSN(9 Digits atleast)",
                    maxlength: "Invalid SSN(9 Digits only)"
                },
                sites: {
                    required: "Please enter your sites",
                    minlength: "Please enter a valid site name",
                },
                title: "Please Choose your image",
                image: "Please Choose your image",
                subject: "Please enter your subject",
                femail: {
                    required: "Please enter your email",
                    minlength: "Please enter a valid email address",
                }
            }
        });
    })($);

</script>
<!-- Validaion-->
<link rel="stylesheet" type="text/css" href="css/jquery.validate.css"/>
<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/jquery.validate.js" type="text/javascript"></script>
<script src="js/jquery.validation.functions.js" type="text/javascript">
</script>
<script type="text/javascript">
    /* <![CDATA[ */
    jQuery(function () {
        jQuery("#sites").validate({
            expression: "if (VAL) return true; else return false;",
            message: "Please enter the Required field"
        });
        jQuery("#firstname").validate({
            expression: "if (VAL) return true; else return false;",
            message: "Please enter the Required field"
        });
        jQuery("#lastname").validate({
            expression: "if (VAL) return true; else return false;",
            message: "Please enter the Required field"
        });
        jQuery("#phoneno").validate({
            expression: "if (VAL.match(/^[0-9]*$/) && VAL) return true; else return false;",
            message: "Please enter a valid integer"
        });
        jQuery('.AdvancedForm').validated(function () {
            alert("Use this call to make AJAX submissions.");
        });
    });
    /* ]]> */
</script>
<section>
    <div class="mainwrapper">
        <?php include 'includes/leftpanel.php'; ?>

        <div class="mainpanel">
            <div class="pageheader">
                <div>
                    <div class="media-body">
                        <h4><?php if ($e == '') {
                                echo 'Add Site';
                            } else {
                                echo 'Update Site';
                            } ?></h4>

                    </div>
                </div><!-- media -->
            </div><!-- pageheader -->

            <div class="contentpanel">
                <div class="row">
                    <div class="col-md-6">
                        <form id="basicForm" action="#" method="post" novalidate="novalidate">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="form-group">
                                            <?= $err; ?>
                                        </div>


                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Site<span
                                                    class="asterisk">*</span></label>
                                            <div class="col-sm-9">
                                                <input autocorrect="off"  type="text" name="sites" onkeyup="mysites()" id="sites"
                                                       class="form-control" value="<?= $data1['Sites']; ?>"
                                                       placeholder="" required="">
                                            </div>
                                        </div><!-- form-group -->

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"></label>
                                            <div class="col-sm-9">
                                                <div id="status"></div>
                                            </div>
                                        </div><!-- form-group -->
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Visit Days</label>

                                            <div class="col-sm-9">
                                                <select data-placeholder="Choose One" id="noofvisits" name="noofvisits"
                                                        class="form-control" style="width:100%;margin-bottom: 1%;">
                                                    <?php
                                                    if ($e != '') {
                                                        echo '<option value=' . $data1['visitsday'] . '>' . $data1['visitsday'] . ' days </option>';
                                                    } else {
                                                        echo '<option value="">Choose One</option>';
                                                    }
                                                    for ($i = 1; $i <= 31; $i++) {
                                                        echo '<option value=' . $i . '&nbsp;' . '' . days . '>' . $i . '&nbsp;' . days . '</option>';
                                                    }
                                                    ?>


                                                </select>
                                            </div>
                                        </div><!-- form-group -->
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Active</label>
                                            <div class="col-sm-9">
                                                <?php if ($e == '') { ?>
                                                    <div class="ckbox ckbox-primary">
                                                        <input autocorrect="off"  type="checkbox" checked id="activesites" value="1"
                                                               name="active" required="">
                                                        <label for="activesites"></label>
                                                    </div><!-- rdio --><?php } else { ?>
                                                    <div class="ckbox ckbox-primary">
                                                        <input autocorrect="off"  type="checkbox" value="1" name="active"
                                                               id="active<?= $data1['Id']; ?>"
                                                               onclick="fnactive(<?= $data1['Id']; ?>)" <?php if ($data1['active'] == 1) {
                                                            echo "checked";
                                                        } ?>/>
                                                        <label for="active<?= $data1['Id']; ?>"></label>
                                                    </div>

                                                <?php } ?>
                                            </div>
                                        </div><!-- form-group -->
                                    </div><!-- row -->
                                </div><!-- panel-body -->
                                <div class="panel-footer">
                                    <div class="row">
                                        <div class="col-sm-9 col-sm-offset-3">
                                            <?php if ($e == '') {
                                                echo '<button class="btn btn-primary mr5" id="createsites" name="createsites" type="submit">Add Site</button>';
                                                echo '<input autocorrect="off"  name="updatesitess" id="updatesites" type="hidden">';
                                            } else {
                                                echo '<button class="btn btn-primary mr5" name="updatesites" id="updatesites" type="submit">Update Site</button>';
                                                echo '<input autocorrect="off"  name="createsitess" id="createsites" type="hidden">';
                                            } ?>
                                            <a href="emvs.php?action=sites" class="btn btn-dark"> Cancel</a>
                                        </div>
                                    </div>
                                </div><!-- panel-footer -->
                            </div><!-- panel -->
                        </form>

                    </div>
                </div><!-- row -->

            </div>
        </div><!-- mainpanel -->
    </div><!-- mainwrapper -->
</section>


<script src="js/jquery-migrate-1.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/modernizr.min.js"></script>
<script src="js/ppace.min.js"></script>

<script src="js/jquery.cookies.js"></script>

<script src="js/jquery.dataTables.min.js"></script>
<script src="js/dataTables.bootstrap.js"></script>
<script src="js/dataTables.responsive.js"></script>
<script src="js/select2.min.js"></script>

<script src="js/custom.js"></script>
<script>
    jQuery(document).ready(function () {

        jQuery('#basicTable').DataTable({
            responsive: true
        });

        var shTable = jQuery('#shTable').DataTable({
            "fnDrawCallback": function (oSettings) {
                jQuery('#shTable_paginate ul').addClass('pagination-active-dark');
            },
            responsive: true
        });

        // Show/Hide Columns Dropdown
        jQuery('#shCol').click(function (event) {
            event.stopPropagation();
        });

        jQuery('#shCol input').on('click', function () {

            // Get the column API object
            var column = shTable.column($(this).val());

            // Toggle the visibility
            if ($(this).is(':checked'))
                column.visible(true);
            else
                column.visible(false);
        });

        var exRowTable = jQuery('#exRowTable').DataTable({
            responsive: true,
            "fnDrawCallback": function (oSettings) {
                jQuery('#exRowTable_paginate ul').addClass('pagination-active-success');
            },
            "ajax": "ajax/objects.txt",
            "columns": [
                {
                    "class": 'details-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": ''
                },
                {"data": "name"},
                {"data": "position"},
                {"data": "office"},
                {"data": "salary"}
            ],
            "order": [[1, 'asc']]
        });

        // Add event listener for opening and closing details
        jQuery('#exRowTable tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = exRowTable.row(tr);

            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            }
            else {
                // Open this row
                row.child(format(row.data())).show();
                tr.addClass('shown');
            }
        });


        // DataTables Length to Select2
        jQuery('div.dataTables_length select').removeClass('form-control input-sm');
        jQuery('div.dataTables_length select').css({width: '60px'});
        jQuery('div.dataTables_length select').select2({
            minimumResultsForSearch: -1
        });

    });

    function format(d) {
        // `d` is the original data object for the row
        return '<table class="table table-bordered nomargin">' +
            '<tr>' +
            '<td>Full name:</td>' +
            '<td>' + d.name + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Extension number:</td>' +
            '<td>' + d.extn + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Extra info:</td>' +
            '<td>And any further details here (images etc)...</td>' +
            '</tr>' +
            '</table>';
    }
</script>

</body>
</html>
