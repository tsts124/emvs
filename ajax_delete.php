<?php
include 'includes/dbcon.php';

$id = $_POST['id'];
$visitid = $_POST['visitid'];
$type = $_POST['type'];

$sql = $dbh->prepare('SELECT `id`
                      FROM `visit_details`
                      WHERE `visit_id` = :visitid
                      AND `detail_type` = :detail_type
                      AND `delete` = 0 ;');
$sql->execute([
    'visitid' => $visitid,
    'detail_type' => $type]);

if (count($sql->fetchAll()) > 1) {
    $sql = $dbh->prepare('UPDATE `visit_details`
                          SET `delete` = 1
                          WHERE `id` = :id ;');
    $sql->execute(['id' => $id]);

    echo 'true';
}

