<?php
include 'includes/dbcon.php';

if ((isset($_POST['lastname'])) && (isset($_POST['firstname']))) {

    $firstname = $_POST['firstname'];
    $lastname = $_POST['lastname'];

    #echo "select * from hosts where Firstname='".$firstname."' &&Lastname='".$lastname."' && `Delete`=0";

    $sql_check = $dbh->prepare("
			select * from hosts where Firstname='" . $firstname . "' &&Lastname='" . $lastname . "' && `Delete`=0
		");
    $sql_check->execute();
    $count = $sql_check->rowCount();
    if ($count != '') {
        echo '<font color="red"><STRONG style="color:#F7410F;text-transform: uppercase;">' . $firstname . '&nbsp;' . $lastname . '</STRONG> already in used.</font>';
    } else {
        echo 'OK';
    }
}

if ((isset($_POST['driverlname'])) && (isset($_POST['driverfname']))) {

    $driverlname = $_POST['driverlname'];
    $driverfname = $_POST['driverfname'];
    #echo "select * from drivers where Firstname='".$driverfname."' && Lastname='".$driverlname."' && `Delete`=0";
    $sql_check = $dbh->prepare("select * from drivers where Firstname='" . $driverfname . "' && Lastname='" . $driverlname . "' && `Delete`=0");
    $sql_check->execute();
    $count = $sql_check->rowCount();

    if ($count != '') {

        echo '<font color="red"><STRONG style="color:#F7410F;text-transform: uppercase;">' . $driverfname . '&nbsp;' . $driverlname . '</STRONG> already in used.</font>';
    } else {
        echo 'OK';
    }
}

if (isset($_POST['addtitle'])) {

    $title = $_POST['addtitle'];
    $sql_check = $dbh->prepare("select * from title where title='" . $title . "' && `Delete`=0");
    $sql_check->execute();
    $count = $sql_check->rowCount();

    if ($count != '') {

        echo '<font color="red">Title <STRONG>' . $title . '</STRONG> is already in use.</font>';
    } else {
        echo 'OK';
    }
}

if (isset($_POST['sites'])) {

    $sites = $_POST['sites'];
    //echo "select * from sites where Sites='".$sites."' && `Delete`=0";
    $sql_check = $dbh->prepare("select * from sites where Sites='" . $sites . "' && `Delete`=0");
    $sql_check->execute();
    $count = $sql_check->rowCount();

    if ($count != '') {
        echo '<font color="red">The Site <STRONG>' . $sites . '</STRONG> is already in use.</font>';
    } else {
        echo 'OK';
    }
}

if (isset($_POST['emailid'])) {

    $emailid = $_POST['emailid'];
    $sql_check = $dbh->prepare("select * from rabbis where emailid='" . $emailid . "' && `Delete`=0");
    $sql_check->execute();
    $count = $sql_check->rowCount();

    if ($count != '') {
        echo '<font color="red">The Email <STRONG>' . $emailid . '</STRONG> is already in use.</font>';
    } else {
        echo 'OK';
    }
}

if (isset($_POST['requiredFieldTitle'])) {

    $paramRequiredFieldTitle = $_POST['requiredFieldTitle'];
    $paramRequiredFieldType = $_POST['fieldType'];

    $sql_check = $dbh->prepare("
		SELECT * FROM `required_fields` WHERE `required_fields`.`title`='" . $paramRfCollectorDetailsTitle . "' &&
		 `required_fields`.`deleted`= 0 AND `required_fields`.`tab` = '.$paramRequiredFieldType.'
	");
    $sql_check->execute();
    $count = $sql_check->rowCount();

    if ($count != '') {
        echo '<font color="red">The Required Field <STRONG>' . $paramRequiredFieldTitle . '</STRONG> is already in use.</font>';
    } else {
        echo 'OK';
    }
}

/*
* get fields that can be considered as not required, so the requirement of field will be set to false
* and validation message will not be displayed
*/
if (isset($_POST['allowedFields'])) {

    $query = $dbh->prepare("
		SELECT * FROM `required_fields` WHERE `required_fields`.`deleted`= 0 AND `required_fields`.`active` = 0
	");
    $query->execute();
    $list = $query->fetchAll();

    if (sizeof($list) != 0) {
        echo json_encode($list);
    } else {
        echo 'ERROR';
    }
}

if (isset($_POST['requiredFields'])) {

    $query = $dbh->prepare("
		SELECT * FROM `required_fields` WHERE `required_fields`.`deleted`= 0 AND `required_fields`.`active` = 1
	");
    $query->execute();
    $list = $query->fetchAll();

    if (sizeof($list) != 0) {
        echo json_encode($list);
    } else {
        echo 'ERROR';
    }
}

if (isset($_POST['method']) && $_POST['method'] == "show_site") {
    $query = $dbh->prepare("
		UPDATE `app_options` 
		SET `app_options`.`value` = '" . $_POST['isChecked'] . "'
		WHERE `app_options`.`key` = 'show_site'
	");
    if ($query->execute()) {
        echo "OK";
    } else {
        echo "error";
    }
}

if (isset($_POST['method']) && $_POST['method'] == "default_site") {

    $query = $dbh->prepare("
			UPDATE `app_options`
			SET `app_options`.`value` = '" . $_POST['defaultValue'] . "'
			WHERE `app_options`.`key` = 'default_site'
		");
    $query->execute();
    // END UPDATE defaulSitetValue

    $query = $dbh->prepare("
			UPDATE `app_options`
			SET `app_options`.`value` = '" . $_POST['defaultVisitDayCount'] . "'
			WHERE `app_options`.`key` ='default_site_visitday_count'
		");
    $query->execute();
    // END UPDATE defaultVisitDayCount

    $query = $dbh->prepare(
        "
				SELECT
					*
				FROM
					`sites`
				WHERE
					`sites`.`Sites` = '" . $_POST['defaultValue'] . "' AND
					`sites`.`visitsday` = '" . $_POST['defaultVisitDayCount'] . "' AND
					`sites`.`Delete` = 0
			"
    );
    $query->execute();
    if ($_POST['defaultValue']) {


        if ($query->rowCount()) {
            echo "EXIST\n";
            // update Active status => Active = 1
            $exist = $query->fetch();
            $query = $dbh->prepare(
                "
					UPDATE
						`sites`
					SET
						`sites`.`active` = 1
					WHERE
						`sites`.`Id` = " . $exist['Id'] . "
				"
            );
            $query->execute();
        } else {
            echo "NEW\n";
            // create new entry with Active = 1 And Delete = 0
            $query = $dbh->exec(
                "
					INSERT INTO `sites`(`Sites`, `visitsday`, `active`, `Delete`, `Addeddate`) VALUES('" . $_POST['defaultValue'] . "', '" . $_POST['defaultVisitDayCount'] . "', '1', '0', now())
				"
            );
        }
        echo "OK";
    }else{
        echo "error";
    }
}

?>