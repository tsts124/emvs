<?php

include 'includes/header.php';
include 'includes/dbcon.php';

if ($_SESSION['user'] == '') {
    header('Location: emvs.php?action=index');
}

$query = $dbh->prepare(
    "
        SELECT
        *
        FROM
        `app_options`
        WHERE `app_options`.`key` = 'show_site' AND `app_options`.`value` = 1;
        "
);

$query->execute();
$showSite = $query->rowCount();

$no = $_GET['no'];

$sql = $dbh->prepare("SELECT *
                      FROM `collectors`
                      WHERE `Id` = '$no';");
$sql->execute();
$collData = $sql->fetch();

$sqlquery = $dbh->prepare("select *
                           from `visitstable`
                           where `collectorsid` = '$no'; ");
$sqlquery->execute();
$dataquery = $sqlquery->fetch();

$sql = $dbh->prepare("SELECT *
                      FROM `visitstable`
                      WHERE `collectorsid` = :id
                      ORDER BY `visitid`
                      DESC; ");
$sql->execute([':id' => $no]);
$lastVisitData = $sql->fetch();

if ($lastVisitData['authofromdate']) {
    $lastVisit = date_create($lastVisitData['authofromdate']);
    $lastVisit = date_format($lastVisit, 'm/d/y');
} else {
    $lastVisit = '';
}


if (strlen($collData['dob']) > 4) {
    $parts = explode('-', $collData['dob']);
    $year = $parts[2] ? $parts[2] : $parts[1];

    if (preg_match('/[a-zA-Z]/', $parts[0])) {
        $date = date_create($collData['dob']);
        $date = date_format($date, 'm/d/y');
        $newParts = explode('/', $date);

        $date = $newParts[0] . '/' . $newParts[1] . '/' . $year[2] . $year[3];
    } else {
        if (strlen($parts[0]) == 1) {
            $parts[0] = '0' . $parts[0];
        }

        if (strlen($parts[1]) == 1) {
            $parts[1] = '0' . $parts[1];
        }

        $date = $parts[0] . '/' . $parts[1] . '/' . $year[2] . $year[3];
    }

} else {
    $date = $collData['dob'];
}

$pagename = $collData['title'] . ' ' .
    $collData['firstname'] . ' ' .
    $collData['lastname'] .
    '&nbsp;&nbsp; Birthdate: ' .
    $date . '&nbsp;&nbsp; Last Visit: ' .
    $lastVisit;
?>

<script type="text/javascript">

    function myfun() {

        var a = document.getElementById("collname").value;
        var dataString = 'id=' + a;

        $.ajax({
                type: "POST",
                url: "ajax_sites1.php",
                data: dataString,
                success: function (msg) {
                    $('#sites1').html(msg);
                }
            }
        );
        //return false;
    }

</script>


<style type="text/css">

    .test {
        color: #ffffff;
    }

    .test:hover {
        color: #ffffff;
        background-color: #EEEEEE;
        padding: 5px;
    }

    .alertRow a {
        color: red;
    }

</style>

<style>

    .first {
        display: none;
    }

    .second {
        display: show;
    }

    #tooltip {
        text-align: center;
        color: #fff;
        background: #111;
        position: absolute;
        z-index: 100;
        padding: 15px;
    }

    #tooltip:after /* triangle decoration */
    {
        width: 0;
        height: 0;
        border-left: 10px solid transparent;
        border-right: 10px solid transparent;
        border-top: 10px solid #111;
        content: '';
        position: absolute;
        left: 50%;
        bottom: -10px;
        margin-left: -10px;
    }

    #tooltip.top:after {
        border-top-color: transparent;
        border-bottom: 10px solid #111;
        top: -20px;
        bottom: auto;
    }

    #tooltip.left:after {
        left: 10px;
        margin: 0;
    }

    #tooltip.right:after {
        right: 10px;
        left: auto;
        margin: 0;
    }

</style>

<script>
    setInterval(function () {
        var alert = document.getElementById('alertComment');
        if (alert) {
            if (alert.style.color == 'rgb(132, 147, 168)') {
                alert.style.color = '#fff';
            } else {
                alert.style.color = 'rgb(132, 147, 168)';
            }
        }
    }, 600);

    $(function () {

        var targets = $('[rel~=tooltip]'),
            target = false,
            tooltip = false,
            title = false;

        targets.bind('mouseenter', function () {
            target = $(this);
            tip = target.attr('title');
            tooltip = $('<div id="tooltip"></div>');

            if (!tip || tip == '')
                return false;

            target.removeAttr('title');
            tooltip.css('opacity', 0)
                .html(tip)
                .appendTo('body');

            var init_tooltip = function () {
                if ($(window).width() < tooltip.outerWidth() * 1.5)
                    tooltip.css('max-width', $(window).width() / 2);
                else
                    tooltip.css('max-width', 340);

                var pos_left = target.offset().left + ( target.outerWidth() / 2 ) - ( tooltip.outerWidth() / 2 ),
                    pos_top = target.offset().top - tooltip.outerHeight() - 20;

                if (pos_left < 0) {
                    pos_left = target.offset().left + target.outerWidth() / 2 - 20;
                    tooltip.addClass('left');
                }
                else
                    tooltip.removeClass('left');

                if (pos_left + tooltip.outerWidth() > $(window).width()) {
                    pos_left = target.offset().left - tooltip.outerWidth() + target.outerWidth() / 2 + 20;
                    tooltip.addClass('right');
                }
                else
                    tooltip.removeClass('right');

                if (pos_top < 0) {
                    var pos_top = target.offset().top + target.outerHeight();
                    tooltip.addClass('top');
                }
                else
                    tooltip.removeClass('top');

                tooltip.css({left: pos_left, top: pos_top})
                    .animate({top: '+=10', opacity: 1}, 50);
            };

            init_tooltip();
            $(window).resize(init_tooltip);

            var remove_tooltip = function () {
                tooltip.animate({top: '-=10', opacity: 0}, 50, function () {
                    $(this).remove();
                });

                target.attr('title', tip);
            };

            target.bind('mouseleave', remove_tooltip);
            tooltip.bind('click', remove_tooltip);
        });
    });
</script>

<section>
    <input autocorrect="off"  type="hidden" id="page-name" value="<?= $pagename ?>"/>
    <div class="mainwrapper">
        <?php include 'includes/leftpanel.php'; ?>
        <div class="mainpanel">
            <div class="pageheader">
                <div>
                    <div class="media-body">
                        <h4>Collector's Visit History</h4>
                    </div>
                </div><!-- media -->
            </div><!-- pageheader -->

            <div class="contentpanel">

                <?php if ($collData['personalcomments']): ?>
                    <script>
                        $(document).ready(function () {
                            $('.alert-block').css('display', 'block');
                        });
                    </script>
                <?php endif; ?>

                <div class="alert-block">
                    <div class="row font-size">
                        <span id="alertComment"
                              class="alert-comment">&nbsp;ALERT </span>&nbsp;- <?= $collData['personalcomments']; ?>
                    </div>
                </div>

                <div class="panel panel-primary-head content-padding">
                    <div class="panel-heading">
                        <a href="emvs.php?action=visitsadd1&&a=<?= $no; ?>" class="btn btn-warning">New Visit</a>
                    </div><!-- panel-heading -->
                    <?php

                    if ($dataquery['collectorsid']) {
                        ?>
                        <table id="basicTable"
                               class="basic-table table table-striped table-bordered responsive collector-table">
                            <thead class="">
                            <tr>
                                <th style="text-align:center"> Last Name, Names</th>
                                <th style="text-align:center"> Reference Id</th>
                                <th style="text-align:center"> Visit Id</th>
                                <th style="text-align:center"> Authorized From</th>
                                <th style="text-align:center"> To</th>
                                <?php if ($showSite) { ?>
                                    <th style="text-align:center"> Sites</th>
                                <?php } ?>
                                <th style="text-align:center"> Status</th>
                                <th style="text-align:center"> Edit</th>
                                <th style="text-align:center"> Print</th>
                                <th style="text-align:center"> Delete</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $visitsQuery = $dbh->prepare("SELECT * 
                                                           FROM `visitstable` 
                                                           WHERE `collectorsid` = :collectorId
                                                           ORDER BY `visitid`;");
                            $visitsQuery->execute(['collectorId' => $no]);
                            while ($visitData = $visitsQuery->fetch()) {

                                $alertClass = $visitData['personalcomments'] ? 'alertRow' : '';
                                ?>
                                <tr class="test <?= $alertClass ?>">
                                    <td>
                                        <a href="emvs.php?action=editvisit1&&a=<?= $visitData['collectorsid']; ?>&&b=<?= $visitData['visitid']; ?>">
                                            <label
                                                style="visibility:hidden;position: absolute;"> <?= $collData['lastname']; ?> </label>
                                            <?= $collData['title'] . '  ' . $collData['firstname'] . ' ' . $collData['lastname'] . ' ' . $collData['middlename']; ?>
                                        </a>
                                    </td>
                                    <td>
                                        <a href="emvs.php?action=editvisit1&&a=<?= $visitData['collectorsid']; ?>&&b=<?= $visitData['visitid']; ?>"><?= $visitData['Refid']; ?></a>
                                    </td>
                                    <td>
                                        <a href="emvs.php?action=editvisit1&&a=<?= $visitData['collectorsid']; ?>&&b=<?= $visitData['visitid']; ?>"><?= $visitData['visitid']; ?></a>
                                    </td>
                                    <td>
                                        <a href="emvs.php?action=editvisit1&&a=<?= $visitData['collectorsid']; ?>&&b=<?= $visitData['visitid']; ?>"><?= $visitData['authofromdate']; ?></a>
                                    </td>
                                    <td>
                                        <a href="emvs.php?action=editvisit1&&a=<?= $visitData['collectorsid']; ?>&&b=<?= $visitData['visitid']; ?>"><?= $visitData['authotodate']; ?></a>
                                    </td>
                                    <?php if ($showSite) { ?>
                                        <td>
                                            <a href="emvs.php?action=editvisit1&&a=<?= $visitData['collectorsid']; ?>&&b=<?= $visitData['visitid']; ?>">
                                                <?php
                                                $s = $visitData['sites'];
                                                $sqlq = $dbh->prepare("SELECT * FROM `sites` WHERE `Id`='$s'");
                                                $sqlq->execute();
                                                $dataq = $sqlq->fetch();
                                                echo $dataq['Sites'];
                                                ?>
                                            </a>
                                        </td>
                                    <?php } ?>
                                    <td>
                                        <a href="emvs.php?action=editvisit1&&a=<?= $visitData['collectorsid']; ?>&&b=<?= $visitData['visitid']; ?>">
                                            <?php
                                            $curdate = date('M-d-Y');
                                            $authdate = $visitData['authotodate'];
                                            $authfrmdate = $visitData['authofromdate'];

                                            if (strtotime($curdate) < strtotime($authfrmdate)) {
                                                echo 'Planned';
                                            } else if (strtotime($curdate) > strtotime($authdate)) {
                                                echo 'Expired';
                                            } else if (strtotime($curdate) >= strtotime($authfrmdate) && strtotime($curdate) <= strtotime($authdate)) {
                                                echo 'Current';
                                            } else
                                                echo 'Error';
                                            ?>
                                        </a>
                                    </td>
                                    <td>
                                        <a href="emvs.php?action=editvisit1&&a=<?= $visitData['collectorsid']; ?>&&b=<?= $visitData['visitid']; ?>">
                                            <img src="images/edit1.png" title="Edit" data-toggle="tooltip" border="0">
                                        </a>
                                        <!--<img src="images/delete1.gif" border="0" title="Delete"></a>     -->
                                    </td>
                                    <td>
                                        <a name="registrationLink"
                                           href="Registration_Identity/Registration_Identity/Registration_Identity.php?a=<?= $visitData['collectorsid']; ?>&&b=<?= $visitData['visitid']; ?>"
                                           target="_blank">
                                            <img src="images/pdf1.jpg" border="0" title="REGISTRATION OF IDENTITY"
                                                 data-toggle="tooltip">
                                        </a>
                                        <a name="charityGreenLink"
                                           href="charity_Baltimore/pdfprint.php?a=<?= $visitData['collectorsid']; ?>&&b=<?= $visitData['visitid']; ?>&green=<?= true ?>"
                                           target="_blank">
                                            <img src="images/pdf2.jpg" border="0" title="CHARITY INFORMATION"
                                                 data-toggle="tooltip">
                                        </a>
                                    </td>
                                    <td>
                                        <a href="javascript:fndelete(<?= $visitData['Id']; ?>)">
                                            <img src="images/delete1.gif" border="0" data-toggle="tooltip"
                                                 title="Delete" alt="EMVS">
                                        </a>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    <?php } else {
                        $sqlquery = $dbh->prepare("select * from collectors where Id='$no'");
                        $sqlquery->execute();
                        $dataquery = $sqlquery->fetch();
                        $name = $dataquery['firstname'];
                        ?>
                        <div class="row">
                            <div class="col-sm-12">
                                <?php echo "<h3><span>{$dataquery['title']} {$dataquery['firstname']} {$dataquery['lastname']}</span>&nbsp;Visits Empty</h3>"; ?>
                            </div>
                        </div>
                    <?php } ?>
                </div><!-- panel -->
                <br/>
            </div><!-- contentpanel -->
        </div><!-- mainpanel -->
    </div><!-- mainwrapper -->
</section>

<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/jquery-migrate-1.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/modernizr.min.js"></script>
<script src="js/ppace.min.js"></script>

<script src="js/jquery.cookies.js"></script>

<script src="js/jquery.dataTables.min.js"></script>
<script src="js/dataTables.bootstrap.js"></script>
<script src="js/dataTables.responsive.js"></script>
<script src="js/select2.min.js"></script>

<script src="js/custom.js"></script>

<script src="pellepim-jstimezonedetect/dist/jstz.min.js"></script>
<script src="js/moment.min.js"></script>
<script src="js/moment-timezone-with-data-2010-2020.min.js"></script>

<script>

    $(document).ready(function () {
        var pageName = $('#page-name').val();
        $('.pagename').html('<span class="two"> ' + pageName + '</span>');

        $('[data-toggle="tooltip"]').tooltip();
    });

    function fndelete(id) {
        if (confirm("Do you really wish to delete this record?")) {
            var data = 'id=' + id;
            $.ajax({
                type: 'POST',
                url: 'ajax_delete_visit.php',
                data: data,
                success: function (msg) {
                    location.reload();
                }
            });
        }
    }

    jQuery(document).ready(function () {


        //getting user's time zone
        var timeZone = jstz.determine();
        var zone = timeZone.name();
        var format = 'MM-DD-YYYY';
        var today = moment().tz(zone).format(format);

        //adding today's date to the pdf links
        var link;

        $('a[name="registrationLink"]').each(function () {
            link = $(this).attr('href');
            link = link + '&&c=' + today;
            $(this).attr('href', link);
        });

        $('a[name="charityLink"]').each(function () {
            link = $(this).attr('href');
            link = link + '&&c=' + today;
            $(this).attr('href', link);
        });

        $('a[name="charityGreenLink"]').each(function () {
            link = $(this).attr('href');
            link = link + '&&c=' + today;
            $(this).attr('href', link);
        });


        jQuery('#basicTable').DataTable({
            responsive: true
        });

        var shTable = jQuery('#shTable').DataTable({
            "fnDrawCallback": function (oSettings) {
                jQuery('#shTable_paginate ul').addClass('pagination-active-dark');
            },
            responsive: true
        });

        // Show/Hide Columns Dropdown
        jQuery('#shCol').click(function (event) {
            event.stopPropagation();
        });

        jQuery('#shCol input').on('click', function () {

            // Get the column API object
            var column = shTable.column($(this).val());

            // Toggle the visibility
            if ($(this).is(':checked'))
                column.visible(true);
            else
                column.visible(false);
        });

        var exRowTable = jQuery('#exRowTable').DataTable({
            responsive: true,
            "fnDrawCallback": function (oSettings) {
                jQuery('#exRowTable_paginate ul').addClass('pagination-active-success');
            },
            "ajax": "ajax/objects.txt",
            "columns": [
                {
                    "class": 'details-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": ''
                },
                {"data": "name"},
                {"data": "position"},
                {"data": "office"},
                {"data": "salary"}
            ],
            "order": [[1, 'asc']]
        });


        // Add event listener for opening and closing details
        jQuery('#exRowTable tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = exRowTable.row(tr);

            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            }
            else {
                // Open this row
                row.child(format(row.data())).show();
                tr.addClass('shown');
            }
        });


        // DataTables Length to Select2
        jQuery('div.dataTables_length select').removeClass('form-control input-sm');
        jQuery('div.dataTables_length select').css({width: '60px'});
        jQuery('div.dataTables_length select').select2({
            minimumResultsForSearch: -1
        });

    });

    function format(d) {
        // `d` is the original data object for the row
        return '<table class="table table-bordered nomargin">' +
            '<tr>' +
            '<td>Full name:</td>' +
            '<td>' + d.name + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Extension number:</td>' +
            '<td>' + d.extn + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Extra info:</td>' +
            '<td>And any further details here (images etc)...</td>' +
            '</tr>' +
            '</table>';
    }
</script>

</body>
</html>
