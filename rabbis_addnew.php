<?php

include 'includes/header.php';
include 'includes/dbcon.php';

$e = base64_decode($_GET['e']);

$query = $dbh->prepare(
    "
	  		SELECT
	  		*
	  		FROM
	  			`app_options`
	  		WHERE 
	  			`app_options`.`key` = 'show_site' 
	  			AND 
	  			`app_options`.`value` = 1;
	  	"
);

$query->execute();
$showSite = $query->rowCount();

$query = $dbh->prepare(
    "
	  	SELECT
	  	*
	  	FROM
	  		`app_options`
	  	WHERE 
	  		`app_options`.`key` = 'default_path_sign'
	  "
);
$query->execute();
$defaultPathSign = $query->fetch();
$defaultPathSign = ($defaultPathSign['value'] == '') ? "uploads/" : $defaultPathSign['value'];


if ($_SESSION['user'] == '') {
    header('Location: emvs.php?action=index');
}

$new = $_GET['new'];
$sqlquery15 = $dbh->prepare('select *
                             from `newmember`
                             order by `Id`
                             desc');
$sqlquery15->execute();
$dataquery12 = $sqlquery15->fetch();
$d = $dataquery12['Id'] + 1;
$emailid = $_POST['emailid'];
$title = $_POST['title'];
$firstname = $_POST['firstname'];
$lastname = $_POST['lastname'];
$phoneno = $_POST['phoneno'];
$active = $_POST['active'];
$sites = $_POST['sites'];
$sqlquery1 = $dbh->prepare('select *
                            from `newmember`
                            order by `Id`
                            desc');
$sqlquery1->execute();
$dataquery1 = $sqlquery1->fetch();


if (isset($_POST['addrabbis'])) {
    $roles = 'Manager';
    $sqlid1 = $dbh->prepare('select *
                             from `rabbis`
                             where `emailid` = :id ;');
    $sqlid1->execute(['id' => $emailid]);
    $dataid1 = $sqlid1->fetch();

    if ($dataid1['emailid'] == '') {
        $count = $dbh->prepare('INSERT INTO `rabbis` (`Title`, `Firstname`, `Lastname`,
                                                   `emailid`, `Phoneno`, `Sites`,
                                                   `Active`, `Addeddate`)
                                VALUES (:title, :firstname, :lastname,
                                        :id, :phoneno, :sites,
                                        :active, now())');
        $count->execute([
            'title' => $title,
            'firstname' => $firstname,
            'lastname' => $lastname,
            'id' => $emailid,
            'phoneno' => $phoneno,
            'sites' => $sites,
            'active' => $active
        ]);

        $count = $dbh->prepare('INSERT INTO `newmember` (`Firstname`, `Lastname`,
                                                         `emailid`, `roles`)
                                VALUES (:firstname, :lastname,
                                        :id, :roles)');
        $count->execute([
            'firstname' => $firstname,
            'lastname' => $lastname,
            'id' => $emailid,
            'roles' => $roles
        ]);
    }
    $sqlquery = $dbh->prepare("select * from rabbis order by Id desc");
    $sqlquery->execute();
    $dataquery = $sqlquery->fetch();
}

if (isset($_POST['createrabbis'])) {
    $sqlid1 = $dbh->prepare('select *
                             from `rabbis`
                             where `emailid` = :id ');
    $sqlid1->execute(['id' => $emailid]);
    $dataid1 = $sqlid1->fetch();

    if ($dataid1['emailid'] == '') {
        $count = $dbh->prepare("INSERT INTO rabbis(`Title`, `Firstname`, `Lastname`,
                                                   `emailid`, `Phoneno`, `Sites`,
                                                   `Active`, `Addeddate`)
                             VALUES (:title, :firstname, :lastname,
                                     :id, :phoneno, :sites,
                                     :active, now())");
        $count->execute([
            'title' => $title,
            'firstname' => $firstname,
            'lastname' => $lastname,
            'id' => $emailid,
            'phoneno' => $phoneno,
            'sites' => $sites,
            'active' => $active
        ]);

        if ($count == '1') {
            $err = '<div class="success1">
                        <p>
                            <img src="images/success.png" border="none" width="18" height="18">
                             Manager Inserted Successfully
                        </p>
                    </div>';
        }
    }
    $sqlquery = $dbh->prepare("select * from rabbis order by Id desc");
    $sqlquery->execute();
    $dataquery = $sqlquery->fetch();
}

if (($e != '')) {
    $sqll = $dbh->prepare('select *
                           from `newmember`
                           where `Id` = :id ;');
    $sqll->execute(['id' => $e]);
    $dataa = $sqll->fetch();
}

if (isset($_POST['updaterabbis'])) {
    $title = $_POST['title'];
    $sites = $_POST['sites'];
    $title1 = addslashes($_POST['title1']);
    $sites1 = $_POST['sites1'];
    $phoneno1 = $_POST['phoneno'];
    $phoneno = implode(",", $phoneno1);
    $Phonenum1 = $_POST['Phonenum'];
    $Phonenum = implode(",", $Phonenum1);

    $count = $dbh->prepare('UPDATE `newmember` 
			                SET `Title` = :title,
			                    `Firstname` = :firstname,
			                    `Lastname` = :lastname,
			                    `emailid` = :email,
			                    `Phonenum` = :phonenum,
				                `Phoneno` = :phoneno,
				                `Sites` = :sites,
				                `activity` = :active,
				                `Updateddate` = now() 
			                WHERE `Id` = :id ');
    $count->execute([
        'title' => $title,
        'firstname' => $firstname,
        'lastname' => $lastname,
        'id' => $e,
        'email' => $emailid,
        'phonenum' => $Phonenum,
        'phoneno' => $phoneno,
        'sites' => $sites,
        'active' => $active
    ]);

    $dcount = count($_POST['phoneno']);

    for ($i = 0; $i < $dcount; $i++) {
        $primary_phone_count = $_POST['primary_phone_count_new'][$i];
        $phone_primary1 = $_POST['CPrimary'];
        $Phonetype = $_POST['Phone_Type'][$i];
        $Phonecntry = addslashes($_POST['Phone_Country'][$i]);
        $phoneno = addslashes($_POST['phoneno'][$i]);
        $id = $_POST['id'][$i];

        if ($_POST['phoneno'][$i]) {
            $count = $dbh->prepare('INSERT INTO `managerphone`(`Id`,
                                                                `D_id`,
                                                                `PrimaryCount`,
                                                                `Phone_Type`,
                                                                `Phone_Country`,
                                                                `Phone_Number`,
                                                                `Addeddate`) 
					                 VALUES (:primaryPhone,
					                         :id,
					                         :primaryPhone,
					                         :phoneType,
					                         :country,
					                         :phoneNumber,
					                         now())
			');
            $count->execute([
                ':primaryPhone' => $primary_phone_count,
                ':id' => $e,
                ':phoneType' => $Phonetype,
                ':country' => $Phonecntry,
                ':phoneNumber' => $phoneno
            ]);
        }
    }

    $oldContacts = count($_POST['old-id']);
    for ($i = 0; $i < $oldContacts; $i++) {
        $Phonetype = $_POST['Phone_Type1'][$i];
        $Phonecntry = addslashes($_POST['Phone_Country1'][$i]);
        $phoneno = addslashes($_POST['phoneno1'][$i]);
        $id = $_POST['old-id'][$i];

        $sql = $dbh->prepare('UPDATE `managerphone`
                                  SET `Phone_Type` = :phoneType,
                                      `Phone_Country` = :country,
                                      `Phone_Number` = :phoneNumber
                                  WHERE `Id` = :id ;');
        $sql->execute([
            'phoneType' => $Phonetype,
            'country' => $Phonecntry,
            'phoneNumber' => $phoneno,
            'id' => $id
        ]);
    }

    if (isset($_POST['action1']) && ($_POST['action1'] == "phoneupdate")) {
        $upquery1 = $dbh->prepare('update `managerphone`
                                   set `DPrimary` = :dprimary
                                   where `D_id` = :id ;');
        $upquery1->execute([
            'id' => $_POST['act'],
            'dprimary' => ''
        ]);
        $upquery = $dbh->prepare('update `managerphone`
                                  set `DPrimary` = :dprimary
                                  where `Id` = :id ;');
        $upquery->execute([
            'dprimary' => $_POST['act'],
            'id' => $_POST['hid']
        ]);
    }

    if ($_GET['look'] == 'look') {
        echo '<meta http-equiv="refresh" content="0; URL=emvs.php?action=manager" />';
    } else if ($_GET['look'] != 'look') {
        echo '<meta http-equiv="refresh" content="0; URL=emvs.php?action=rabbis" />';
    }
}

if ($_GET['look'] == 'look') {
    if (isset($_POST['action']) && ($_POST['action'] == "delete")) {
        $act = $_POST['action'];
        $delquery = $dbh->prepare('UPDATE `newmember`
                                   SET `Delete` = :deleted
                                   WHERE `Id` = :id ');
        $delquery->execute([
            'deleted' => '1',
            'id' => $_POST['hid']
        ]);
        echo '<meta http-equiv="refresh" content="0; URL=emvs.php?action=manager" />';
    }
}

if (isset($_POST['action']) && ($_POST['action'] == "delete")) {
    $delquery1 = $dbh->prepare('delete from `managerphone`
                                where `Id` = :id ');
    $delquery1->execute(['id' => $_POST['act']]);
}
?>

<script>

    var popupWindow = null;
    function signadd(addval) {
        popupWindow = window.open("signature.php?a=" + addval, "_blank", "toolbar=yes, scrollbars=yes, resizable=yes, top=235, left=550, width=500, height=200");
        popupWindow.focus();
        document.onmousedown = focusPopup;
        document.onkeyup = focusPopup;
        document.onmousemove = focusPopup;
    }

    function parent_disable() {

        if (popupWindow && !popupWindow.closed)
            popupWindow.focus();
    }
</script>

<script type="text/javascript" src="js/jquery-1.2.6.min.js"></script>
<script type="text/javascript">

    function fndelete(id) {
        if (confirm("Do you really wish to delete this record?")) {

            document.getElementById('action').value = "delete";
            document.getElementById('hid').value = id;
            document.basicForm.submit();
        }
    }

    pic1 = new Image(16, 16);
    pic1.src = "loader.gif";
</SCRIPT>
<script>

    function signatureCapture1() {

        var canvas = document.getElementById("newSignature1");
        var ctx = canvas.getContext("2d");
        ctx.strokeStyle = "#000000";
        ctx.fillStyle = "#000000";
        ctx.lineWidth = 2;
        ctx.lineCap = "round";

        // Set up mouse events for drawing
        var drawing = false;
        var mousePos = {x: 0, y: 0};
        var lastPos = mousePos;
        canvas.addEventListener("mousedown", function (e) {
            drawing = true;
            lastPos = getMousePos(canvas, e);
        }, false);
        canvas.addEventListener("mouseup", function (e) {
            drawing = false;
        }, false);
        canvas.addEventListener("mousemove", function (e) {
            mousePos = getMousePos(canvas, e);
        }, false);

        // Get the position of the mouse relative to the canvas
        function getMousePos(canvasDom, mouseEvent) {
            var rect = canvasDom.getBoundingClientRect();
            return {
                x: mouseEvent.clientX - rect.left,
                y: mouseEvent.clientY - rect.top
            };
        }

        // Get a regular interval for drawing to the screen
        window.requestAnimFrame = (function (callback) {
            return window.requestAnimationFrame ||
                window.webkitRequestAnimationFrame ||
                window.mozRequestAnimationFrame ||
                window.oRequestAnimationFrame ||
                window.msRequestAnimaitonFrame ||
                function (callback) {
                    window.setTimeout(callback, 1000 / 60);
                };
        })();

        // Draw to the canvas
        function renderCanvas() {
            if (drawing) {
                ctx.moveTo(lastPos.x, lastPos.y);
                ctx.lineTo(mousePos.x, mousePos.y);
                ctx.stroke();
                lastPos = mousePos;
            }
        }

        // Allow for animation
        (function drawLoop() {
            requestAnimFrame(drawLoop);
            renderCanvas();
        })();

        // Set up touch events for mobile, etc
        canvas.addEventListener("touchstart", function (e) {
            mousePos = getTouchPos(canvas, e);
            var touch = e.touches[0];
            var mouseEvent = new MouseEvent("mousedown", {
                clientX: touch.clientX,
                clientY: touch.clientY
            });
            canvas.dispatchEvent(mouseEvent);
        }, false);
        canvas.addEventListener("touchend", function (e) {
            var mouseEvent = new MouseEvent("mouseup", {});
            canvas.dispatchEvent(mouseEvent);
        }, false);
        canvas.addEventListener("touchmove", function (e) {
            var touch = e.touches[0];
            var mouseEvent = new MouseEvent("mousemove", {
                clientX: touch.clientX,
                clientY: touch.clientY
            });
            canvas.dispatchEvent(mouseEvent);
        }, false);

        // Get the position of a touch relative to the canvas
        function getTouchPos(canvasDom, touchEvent) {
            var rect = canvasDom.getBoundingClientRect();
            return {
                x: touchEvent.touches[0].clientX - rect.left,
                y: touchEvent.touches[0].clientY - rect.top
            };
        }

        // Prevent scrolling when touching the canvas
        document.body.addEventListener("touchstart", function (e) {
            if (e.target == canvas) {
                e.preventDefault();
            }
        }, false);
        document.body.addEventListener("touchend", function (e) {
            if (e.target == canvas) {
                e.preventDefault();
            }
        }, false);
        document.body.addEventListener("touchmove", function (e) {
            if (e.target == canvas) {
                e.preventDefault();
            }
        }, false);
    }

    function signatureSave1(a) {

        var canvas = document.getElementById("newSignature1");// save canvas image as data url (png format by default)
        var dataURL = canvas.toDataURL("image/png");
        var a = a;

        document.getElementById("saveSignature1").src = dataURL;
        document.getElementById('imgsrc').value = dataURL;
        document.getElementById('a').value = a;

        var fd = new FormData(document.forms["basicForm"]);
        var xhr = new XMLHttpRequest();
        xhr.open('POST', 'upload_data1.php', true);

        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4) {
                //console.log(xhr.response);
            }
        };

        xhr.upload.onprogress = function (t) {
            if (t.lengthComputable) {
                var percentComplete = (t.loaded / t.total) * 100;
                //console.log(percentComplete + '% uploaded');
                //alert('Succesfully uploaded');
            }
        };

        xhr.onload = function () {

        };

        xhr.send(fd);
    }

    function signatureClear1() {
        var canvas = document.getElementById("newSignature1");
        canvas.width = canvas.width;

        var lineWidth = $('#line-width').val();

        var ctx = canvas.getContext("2d");
        ctx.strokeStyle = "#000000";
        ctx.fillStyle = "#000000";
        ctx.lineWidth = lineWidth;
        ctx.lineCap = "round";
    }

    //Line width change
    function lineWidthChange() {
        var canvas = document.getElementById("newSignature1");
        var ctx = canvas.getContext("2d");

        ctx.lineWidth = $('#line-width').val();
    }

    $(document).ready(function () {
        $('#line-width').on('change', lineWidthChange);
    });
</script>

<script language="javascript" type="text/javascript">

    function showHide(shID) {
        //alert(shID);
        if (document.getElementById(shID)) {
            if (document.getElementById(shID + '-show').style.display != 'show') {
                document.getElementById(shID + '-show').style.display = 'show';
                document.getElementById(shID).style.display = 'block';
            } else {

                document.getElementById(shID + '-show').style.display = 'inline';
                document.getElementById(shID).style.display = 'none';
            }
        }
    }

    function showHide1(shID1) {
        //alert(shID1);
        if (document.getElementById(shID1 + '-show').style.display != 'none') {
            document.getElementById(shID1 + '-show').style.display = 'none';
            document.getElementById(shID1).style.display = 'none';
        }
    }
</script>

<link rel="stylesheet" type="text/css" href="css/jquery.validate.css"/>

<script src="js/jquery-1.11.1.min.js"></script>
<link href="datepicker/jquery-ui.css" rel="Stylesheet" type="text/css"/>
<script type="text/javascript" src="datepicker/jquery-ui.js"></script>
<script language="javascript">
    function newid1(a, b) {
        document.getElementById('act').value = b;
        document.getElementById('action1').value = "phoneupdate";
        document.getElementById('hid').value = a;
    }
    $(".ui-datepicker-month").prepend("<option value='' selected='selected'>Month</option>");

    $(".ui-datepicker-year").prepend("<option value='' selected='selected'>Year</option>");
</script>
<!-- Datepicker-->
<script type="text/javascript">
    function myfunction(a, b) {
        var dataString = 'edit=' + a + '&editcoll=' + b;
        $.ajax({
            type: "POST",
            url: "editprocess.php",
            data: dataString,
            success: function (msg) {
                $('#result').html(msg);
            }
        });
    }
</script>

<script language="JavaScript">
    function setVisibility(id, visi) {
        document.getElementById(id).style.visibility = visi;
    }
</script>

<script type="text/javascript">

    function yesnoCheck(yes) {
        var dataString = 'fundsprocess=' + yes;
        $.ajax({
            type: "POST",
            url: "fundsprocess.php",
            data: dataString,
            success: function (msg) {
                $('#result1').html(msg);
            }
        });
    }

    function other1(x) {
        alert(x);
        x.className = (x.className == "first") ? "second" : (x.className == "second") ? "first" : "second";
    }

    function removephne(val) {

        if (confirm("Do you really wish to delete this record?")) {
            document.getElementById('act').value = val;
            document.getElementById('action').value = "delete";
            //document.getElementById('hid').value = a;
            document.basicForm.submit();
        }
    }

</script>

<style>
    .first {
        display: none;
    }

    #tooltip {
        text-align: center;
        color: #fff;
        background: #111;
        position: absolute;
        z-index: 100;
        padding: 15px;
    }

    #tooltip:after /* triangle decoration */
    {
        width: 0;
        height: 0;
        border-left: 10px solid transparent;
        border-right: 10px solid transparent;
        border-top: 10px solid #111;
        content: '';
        position: absolute;
        left: 50%;
        bottom: -10px;
        margin-left: -10px;
    }

    #tooltip.top:after {
        border-top-color: transparent;
        border-bottom: 10px solid #111;
        top: -20px;
        bottom: auto;
    }

    #tooltip.left:after {
        left: 10px;
        margin: 0;
    }

    #tooltip.right:after {
        right: 10px;
        left: auto;
        margin: 0;
    }
</style>

<script>
    $(function () {

        var targets = $('[rel~=tooltip]'),
            target = false,
            tooltip = false,
            title = false;

        targets.bind('mouseenter', function () {
            target = $(this);
            tip = target.attr('title');
            tooltip = $('<div id="tooltip"></div>');

            if (!tip || tip == '')
                return false;

            target.removeAttr('title');
            tooltip.css('opacity', 0)
                .html(tip)
                .appendTo('body');

            var init_tooltip = function () {
                if ($(window).width() < tooltip.outerWidth() * 1.5)
                    tooltip.css('max-width', $(window).width() / 2);
                else
                    tooltip.css('max-width', 340);

                var pos_left = target.offset().left + ( target.outerWidth() / 2 ) - ( tooltip.outerWidth() / 2 ),
                    pos_top = target.offset().top - tooltip.outerHeight() - 20;

                if (pos_left < 0) {
                    pos_left = target.offset().left + target.outerWidth() / 2 - 20;
                    tooltip.addClass('left');
                }
                else
                    tooltip.removeClass('left');

                if (pos_left + tooltip.outerWidth() > $(window).width()) {
                    pos_left = target.offset().left - tooltip.outerWidth() + target.outerWidth() / 2 + 20;
                    tooltip.addClass('right');
                }
                else
                    tooltip.removeClass('right');

                if (pos_top < 0) {
                    var pos_top = target.offset().top + target.outerHeight();
                    tooltip.addClass('top');
                }
                else
                    tooltip.removeClass('top');

                tooltip.css({left: pos_left, top: pos_top})
                    .animate({top: '+=10', opacity: 1}, 50);
            };

            init_tooltip();
            $(window).resize(init_tooltip);

            var remove_tooltip = function () {
                tooltip.animate({top: '-=10', opacity: 0}, 50, function () {
                    $(this).remove();
                });

                target.attr('title', tip);
            };

            target.bind('mouseleave', remove_tooltip);
            tooltip.bind('click', remove_tooltip);
        });
    });
    function updatemanager() {
        //alert("ok");
    }
</script>

<script type="text/javascript">

    function myfun() {
        var a = document.getElementById("sites").value;
        var dataString = 'id=' + a;
        $.ajax({
            type: "POST",
            url: "ajax_sites.php",
            data: dataString,
            success: function (msg) {
                $('#sites1').html(msg);
            }
        });
        //return false;
    }
</script>
<style type="text/css">
    .step_box {
        border: 1px solid rgb(247, 247, 247);
    }

    .step_box:hover, #selected_step_box, .QuickStartLong:hover {
        background: rgb(247, 247, 247);
    }

    .selected {
        background-color: #ddd;
    }
</style>

<script type="text/javascript">
    $(document).ready(function () {
        $('.step_wrapper').on('click', '.step_box', function () {
            $('.step_box').removeClass('selected');
            $(this).addClass('selected')
        });
    });
</script>

<script language="JavaScript">

    function take_snapshot(a1, b1) {

        Webcam.snap(function (data_uri) {
            document.getElementById('a1').value = a1;
            document.getElementById('b1').value = b1;
            document.getElementById('imgsrc1').value = data_uri;

            document.getElementById('results').innerHTML =
                '<img src="' + data_uri + '"/>';

            var fd = new FormData(document.forms["valWizard"]);
            var xhr = new XMLHttpRequest();
            xhr.open('POST', 'uploadphoto.php', true);

            xhr.upload.onprogress = function (e) {
                if (e.lengthComputable) {
                    var percentComplete = (e.loaded / e.total) * 100;
                    console.log(percentComplete + '% uploaded');
                }
            };
            xhr.onload = function () {
            };
            xhr.send(fd);
        });
    }
</script>

<script src="js/jquery.maskedinput.js" type="text/javascript"></script>
<script type="text/javascript">

    jQuery(function ($) {
        $("#phone0").mask("(999) 999-9999");
    });

    function chkAllVal() {
        var pno = document.getElementsByName("phoneno[]");

        for (var i = 0; i < pno.length; i++) {
            var id = document.getElementById("phone" + i);
            var val = id.value;
            var actVal = val.replace("_", "");
            if (actVal.length < 14) {
                document.getElementById("phone" + i).focus();
                return false;
            }
        }
    }
</script>
<script type="text/javascript">

    function hello1(a) {
        $("#remove" + a).remove();
        a--;
    }
    function hello() {
        //alert("Hello there!");
        countClicks++;
        document.getElementById('phoneid').value = countClicks;
        var dataString = 'countClicks=' + countClicks;

        $.ajax({
            type: "POST",
            url: "ajax_addmore_rabbis.php",
            data: dataString,
            success: function (msg) {
                //$('#addmore').html(msg);
                var strVal = msg.split("$$$$$");
                var strDiv = document.getElementById("addmore").outerHTML;
                document.getElementById("addmore").outerHTML = msg + strDiv;
                $("#phone" + countClicks).mask("(999) 999-9999");

            }
        });
    }
    var countClicks = 0;

</script>

<body onFocus="parent_disable();" onclick="parent_disable();">

<section>
    <div class="mainwrapper">
        <?php include 'includes/leftpanel.php'; ?>
        <div class="mainpanel">
            <div class="pageheader">
                <div>
                    <div class="media-body">
                        <?php if ($e != '') { ?>
                            <h4>Edit Manager</h4>
                        <?php } else { ?>
                            <h4>Add Manager</h4>
                        <?php } ?>
                    </div>
                </div><!-- media -->
            </div><!-- pageheader -->

            <div class="panel panel-primary" style="border:none;">
                <form enctype="multipart/form-data" method="post" accept-charset="utf-8" class="panel-wizard"
                      novalidate="novalidate" name="basicForm" id="basicForm">
                    <input autocorrect="off"  name="action1" id="action1" type="hidden">
                    <input autocorrect="off"  name="visitval" id="visitval" type="hidden">
                    <div class="tab-content">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                            </div><!-- panel-heading -->
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-sm-8">
                                        <div class="row" style="margin-bottom:1%;">
                                            <div class="col-sm-4">
                                                Title<span class="asterisk">*</span>
                                            </div>
                                            <div class="col-sm-6">
                                                <input autocorrect="off"  id="coll-id" type="hidden" value="<?= $e ?>">
                                                <input autocorrect="off"  id="title1" name="title1" type="hidden"
                                                       value="<?= $dataa['Title']; ?>">
                                                <?php
                                                $sql = $dbh->prepare('SELECT *
                                                                      FROM `title`
                                                                      WHERE `Delete` = 0 ;');
                                                $sql->execute();
                                                $titles = $sql->fetchAll();
                                                ?>

                                                <select name="title"
                                                        id="title"
                                                        class="form-control">
                                                    <option value=" ">Choose One</option>

                                                    <?php foreach ($titles as $title): ?>
                                                        <option <?php if ($title['title'] == $dataa['Title']) {
                                                            echo ' selected ';
                                                        } ?>><?= $title['title'] ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row" style="margin-bottom:1%;">
                                            <div class="col-sm-4">
                                                Login <span class="asterisk">*</span>
                                            </div>
                                            <div class="col-sm-6" id="emailDiv">
                                                <input autocorrect="off"  type="hidden"
                                                       id="current-email"
                                                       value="<?= $dataa['emailid']; ?>">
                                                <input autocorrect="off"  type="text"
                                                       name="emailid"
                                                       id="emailid"
                                                       value="<?php echo $dataa['emailid']; ?>"
                                                       class="form-control"
                                                       onkeypress="Maxval(this.value,30,this.id); loginChars()"
                                                       onblur="checkEmail(this.value, 'updaterabbis');"
                                                       required
                                                       autocomplete="off">
                                                <div id="counteremailid"></div>
                                            </div>
                                            <div class="col-sm-1">
                                                &nbsp;
                                            </div>
                                            <div id="status"></div>
                                        </div>
                                        <div class="row" style="margin-bottom:1%;">
                                            <div class="col-sm-4">
                                                First Name <span class="asterisk">*</span>
                                            </div>
                                            <div class="col-sm-6">
                                                <input autocorrect="off"  type="text" name="firstname" id="firstname"
                                                       value="<?= $dataa['Firstname']; ?>" class="form-control"
                                                       placeholder="First Name" required>
                                            </div>
                                            <div class="col-sm-1">
                                                &nbsp;
                                            </div>
                                        </div>
                                        <div class="row" style="margin-bottom:1%;">
                                            <div class="col-sm-4">
                                                Last Name <span class="asterisk">*</span>
                                            </div>
                                            <div class="col-sm-6">
                                                <input autocorrect="off"  type="text" name="lastname" id="lastname"
                                                       value="<?= $dataa['Lastname']; ?>" class="form-control"
                                                       placeholder="Last Name" required>
                                            </div>
                                            <div class="col-sm-1">
                                                &nbsp;
                                            </div>
                                        </div>

                                        <div class="form-group" style=" border-bottom: 1px solid #ededed;">
                                            <label class="col-sm-6 control-label"
                                                   style="width: 200px; font-weight:bold;">
                                                Contact Details

                                                <input autocorrect="off"  type="hidden" value="0" name="phoneid[]" id="phoneid">
                                                <a href="javascript:void(0);" onclick="chkphoneval();"
                                                   class="add-button" title="Add field">
                                                    <img src="images/add-icon.png" style="height: 25px;"/><br/>
                                                </a>
                                            </label>
                                            <div>
                                                <button class="btn btn-primary" type="button" id="contact_isr">
                                                    ISR
                                                </button>
                                                <button class="btn btn-primary" type="button" id="contact_usa">
                                                    USA
                                                </button>
                                                <button class="btn btn-primary" type="button" id="contact_uk">
                                                    UK
                                                </button>
                                            </div>
                                        </div>
                                        <div class="form-group" style="margin-left: -1.5%;">
                                            <div class="col-sm-2 field">
                                                Primary
                                            </div>
                                            <label class="col-sm-2 control-label">
                                                Type
                                            </label>
                                            <div class="col-sm-3">
                                                Country
                                            </div>
                                            <div class="col-sm-3 field">
                                                Phone
                                            </div>
                                        </div>
                                        <div class="add-info">
                                            <?php
                                            if ($e != '') {
                                                $mid = $dataa['Id'];
                                                $sqlquery = $dbh->prepare("select * from managerphone where D_id ='$mid'");
                                                $sqlquery->execute();
                                                while ($dataquery1 = $sqlquery->fetch()) {
                                                    ?>
                                                    <div class="form-group row" id="addinput">
                                                        <input autocorrect="off"  type="hidden" name="old-id[]"
                                                               value="<?= $dataquery1['Id']; ?>">
                                                        <div class="row col-sm-1 col-xs-1 field new-input-primary">
                                                            <input autocorrect="off"  name="primary_phone_count[]"
                                                                   id="primary_phone_count"
                                                                   value="<?= $dataquery1['Id']; ?>"
                                                                   type="hidden">
                                                            <input autocorrect="off"  required
                                                                   id="<?= $dataquery1['Id']; ?>"
                                                                   name="CPrimary"
                                                                   value="1"
                                                                   style="margin: 15px 0 0 0;"
                                                                   onclick="newid1(<?= $dataquery1['Id']; ?>,<?= $dataquery1['D_id']; ?>)"
                                                                   type="radio"<?php if ($dataquery1['DPrimary'] != '') {
                                                                echo 'checked';
                                                            } ?>>
                                                        </div>
                                                        <div class="row col-sm-1 col-xs-1 control-label"
                                                             style="width: 150px;">
                                                            <select class="form-control"
                                                                    name="Phone_Type1[]">
                                                                <option
                                                                    <?php if (!$dataquery1['Phone_Type']) {
                                                                        echo 'disabled selected';
                                                                    } ?>><?php
                                                                    if ($dataquery1['Phone_Type']) {
                                                                        echo $dataquery1['Phone_Type'];
                                                                    } else {
                                                                        echo 'Choose One';
                                                                    }
                                                                    ?></option>
                                                                <option>Work</option>
                                                                <option>Home</option>
                                                                <option>Mobile</option>
                                                                <option>Fax</option>
                                                                <option>Other</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <?php $domain = $dataquery1['Phone_Country']; ?>
                                                            <input autocorrect="off"  type="text" style="text-transform: uppercase"
                                                                   lang="<?= $dataquery1['Id']; ?>"
                                                                   id="countrycode<?= $dataquery1['Id']; ?>"
                                                                   name="Phone_Country1[]"
                                                                   value="<?= $domain ?>"
                                                                   class="form-control"
                                                                   onfocus="contactCountryListener(this.value, this.id, this.lang);">
                                                        </div>
                                                        <div class="row col-sm-3" id='phone_number'>
                                                            <input autocorrect="off"  type="text"
                                                                   name="phoneno1[]"
                                                                   value="<?= $dataquery1['Phone_Number']; ?>"
                                                                   id="p_new"
                                                                   class="form-control" placeholder="Phone Number"
                                                                   required
                                                            >
                                                        </div>
                                                        <div id='phone_number'>
                                                            <img class="delete-img" src="images/edit1.png"
                                                                 border="0" data-toggle="tooltip" title="" alt="EMVS"
                                                                 data-original-title="Edit Details">
                                                            <a href="#" id="<?= $dataquery1['Id']; ?>"
                                                               onclick="removephne(<?= $dataquery1['Id']; ?>)"><img
                                                                    class="delete-img"
                                                                    src="images/remove-icon.png" border="0"
                                                                    data-toggle="tooltip" title="Remove Details"
                                                                    alt="Remove"
                                                                    data-original-title="Remove Details"></a>
                                                        </div>
                                                    </div>
                                                <?php }
                                            }
                                            ?>

                                            <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
                                            <script type="text/javascript" src="js/jquery.SimpleMask.js"></script>
                                            <?php include 'add_phone_wo_row.php'; ?>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12 field" id='phone_number'
                                                 style="text-align:center;">
                                                <input autocorrect="off"  type="hidden" value="0" name="phoneid[]" id="phoneid">
                                            </div>
                                        </div>
                                        <div class="row" style="margin-bottom:1%;">
                                            <div class="col-sm-4">
                                                Sites
                                            </div>
                                            <div class="col-sm-6">
                                                <?php
                                                $s = $dataa['Sites'];
                                                $sitessql = $dbh->prepare("select * from sites where Id='$s' AND `Delete`='0'");
                                                $sitessql->execute();
                                                $sitesdata = $sitessql->fetch();
                                                ?>
                                                <input autocorrect="off"  id="sites1" name="sites1" type="hidden"
                                                       value="<?= $sitesdata['Id']; ?>">
                                                <select required='' data-placeholder="" id="sites" name="sites"
                                                        class="form-control">
                                                    <?php
                                                    $t = $sitesdata['Sites'];
                                                    if ($dataa['Sites'] != '') {
                                                        $sitessql = $dbh->prepare("SELECT * FROM `sites` WHERE Sites!='" . $t . "' && `Active`=1 && `Delete`=0 order by Id desc");
                                                    } else {
                                                        $sitessql = $dbh->prepare("SELECT * FROM `sites` WHERE `Delete`=0 && `Active`=1 && `Delete`=0 order by Id desc");
                                                    }
                                                    $sitessql->execute(); ?>
                                                    <option value="<?php if ($sitesdata['Sites'] != '') {
                                                        echo $sitesdata['Id'];
                                                    } else {
                                                        echo 'Choose One';
                                                    } ?>"><?php if ($sitesdata['Sites'] != '') {
                                                            echo $sitesdata['Sites'];
                                                        } else {
                                                            echo 'Choose One';
                                                        } ?>
                                                    </option>
                                                    <?php
                                                    while ($sitesdata = $sitessql->fetch()) { ?>
                                                        <option
                                                            value="<?= $sitesdata['Id'] ?>"><?= $sitesdata['Sites']; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div class="col-sm-1">
                                                &nbsp;
                                            </div>
                                        </div>

                                        <div class="row" style="margin-bottom:1%;">
                                            <div class="col-sm-4">
                                                Active
                                            </div>
                                            <div class="col-sm-6">
                                                <?php
                                                if ($e == '') { ?>
                                                    <div class="ckbox ckbox-primary">
                                                        <input autocorrect="off"  type="checkbox" checked id="activerabbis" value="1"
                                                               name="active" required="">
                                                        <label for="activerabbis"></label>
                                                    </div><!-- rdio --><?php
                                                } else {
                                                    ?>
                                                    <div class="ckbox ckbox-primary">
                                                        <input autocorrect="off"  type="checkbox" value="1" name="active"
                                                               id="active<?= $dataa['Id']; ?>"
                                                               onclick="fnactive(<?= $dataa['Id']; ?>)"
                                                            <?php if ($dataa['activity'] == 1) {
                                                                echo "checked";
                                                            } ?>/>
                                                        <label for="active<?= $dataa['Id']; ?>"></label>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                            <div class="col-sm-1">
                                                &nbsp;
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="row">
                                            <div id="mydiv">
                                                <div id="imgDiv">
                                                    <?php
                                                    $signid = $dataa['Id'];
                                                    $sql1 = $dbh->prepare("select * from rabbissign where signid='$signid'");
                                                    $sql1->execute();
                                                    $data1 = $sql1->fetch();
                                                    if ($data1['signname'] != '') {
                                                        echo '<img style="width:100%;" src="' . $defaultPathSign . $data1['signname'] . '">';
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row rabbis-signature">
                                            <div>
                                                <div class="row">
                                                    <input autocorrect="off"  id="imgsrc" type="hidden" name="imgsrc"/>
                                                    <input autocorrect="off"  id="a" type="hidden" name="a"
                                                           value="<?= $data['Id']; ?>"/>
                                                    <div>
                                                        <?php echo 'New Signature'; ?>
                                                    </div>
                                                    <img class="edit-manager-img" id="saveSignature1"/>
                                                    <div id="canvas">
                                                        <canvas class="roundCorners" id="newSignature1"
                                                                style="position: relative; padding: 0; border: 1px solid #c4caac;"></canvas>
                                                    </div>
                                                    <script>signatureCapture1();</script>
                                                    <div style="display: flex; ">

                                                        <?php
                                                        if ($e == ''): ?>
                                                            <button type="button"
                                                                    class="btn btn-success edit-manager-btn"
                                                                    style="margin-right: 3px;"
                                                                    onclick="signatureSave1(<?= $d ?>)">Add
                                                            </button>
                                                        <?php else: ?>
                                                            <button class="btn btn-success edit-manager-btn"
                                                                    style="margin-right: 3px;"
                                                                    onclick="signatureSave1(<?= $dataa['Id'] ?>)"
                                                                    type="button">Update
                                                            </button>
                                                        <?php endif; ?>

                                                        <button type="button"
                                                                class="btn btn-danger edit-manager-btn"
                                                                style="margin-right: 5px;"
                                                                onclick="signatureClear1()">
                                                            Clear&nbsp;&nbsp;&nbsp;</button>

                                                        <select id="line-width"
                                                                class="form-control"
                                                                style="width: 60px; ">
                                                            <option selected>&nbsp;1</option>
                                                            <option>&nbsp;2</option>
                                                            <option>&nbsp;3</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-1">
                                                &nbsp;
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <input autocorrect="off"  type="hidden" name="action" id="action" value="delete"/>
                                <input autocorrect="off"  type="hidden" name="hid" id="hid"/>
                                <input autocorrect="off"  type="hidden" name="act" id="act"/>
                            </div><!-- panel-body -->
                            <div class="panel-footer">
                                <div class="row">
                                    <div class="col-sm-9 col-sm-offset-3"><?php $dataid = $dataquery1['Id'] + 1; ?>
                                        <?php if ($e == '') {
                                            echo '<button class="btn btn-primary mr5"
                                                          id="submitchk"
                                                          name="addrabbis"
                                                          type="submit">Add Manager</button>';
                                        } else {
                                            echo '<button class="btn btn-primary mr5"
                                                          name="updaterabbis"
                                                          type="submit"
                                                          id="submitchk"
                                                          onclick="updatemanager()">Update Manager</button>';
                                        } ?>
                                        <?php if ($_GET['look'] == 'look') {
                                            echo ' <a href="emvs.php?action=manager" class="btn btn-dark"> Cancel</a> ';
                                        } else {
                                            echo '<a href="emvs.php?action=rabbis" class="btn btn-dark"> Cancel</a> ';
                                        }
                                        ?>
                                        <input autocorrect="off"  id="upsubmitchk" name="next123" type="text"
                                               style="margin-left: 5px;margin-top: 1px;display:none;">
                                        <?php if ($_GET['look'] == 'look') {
                                            echo '<a class="btn btn-danger" href="javascript:fndelete(' .
                                                $dataa['Id'] . ')">Delete</a>';
                                        } ?>
                                    </div>
                                </div>
                            </div><!-- panel-footer -->
                        </div><!-- panel -->
                    </div><!-- tab-pane -->
            </div><!-- panel-body -->
        </div><!-- panel -->
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>

    </div>
    </div>
    </div>
    </div><!-- tab-pane -->
    </div><!-- tab-content -->
    </form>
    </div>
</section>


<script type="text/javascript" src="js/jquery.validate.min.js"></script>

<script>
    $("#basicForm").validate({
        rules: {
            Identno: {
                required: true,
                minlength: 9,
                maxlength: 9,
                digits: true
            },
            gender: {
                required: true
            }
        }
    });
</script>
<script src="js/jquery-migrate-1.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/modernizr.min.js"></script>
<script src="js/ppace.min.js"></script>

<script src="js/jquery.cookies.js"></script>

<script src="js/jquery.dataTables.min.js"></script>
<script src="js/dataTables.bootstrap.js"></script>
<script src="js/dataTables.responsive.js"></script>
<script src="js/select2.min.js"></script>

<script src="js/custom.js"></script>

<script src="signature.js"></script>


</body>
</html>
