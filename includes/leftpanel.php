<?php

error_reporting(0);
session_start();

include 'includes/dbcon.php';

$user1 = $_SESSION['user'];

$action = $_GET['action'];

$ab = $_SERVER['REQUEST_URI'];

$user = str_replace('=', '', (strstr($ab, '=')));

$pos = strchr($user, '&&', true);

$sql = $dbh->prepare("select * from newmember where emailid='$user1'");
$sql->execute();
$data = $sql->fetch();

$fname = $data['Firstname'];
$lname = $data['Lastname'];
?>
<div class="leftpanel">

    <div class="media profile-left">
        <a class="pull-left">
            <img height="55px" src="images/avator.png">
        </a>
        <div class="media-body user-name">
            <h4 class="media-heading " style="text-transform: capitalize;">
                <?= $fname . '&nbsp;' . $lname; ?>
            </h4>
            <small class="my-text-muted">
                <?= $data['roles']; ?>
            </small>
        </div>
    </div>

    <?php
    $roles = $_SESSION['roles'];

    if (is_array($roles)) {
        $roles = $roles[1];
    }

    $key = array_search('Administrator', $roles);
    ?>
    <ul class="nav nav-pills nav-stacked level1" id="level1">
        <li class="<?php
        if ($user == 'welcome') {
            echo 'active';
        }
        ?>">
            <a tabindex="-1" href="emvs.php?action=welcome">
                <i class="fa fa-home" aria-hidden="true"></i>
                <span style="vertical-align:middle;">&nbsp;&nbsp;Home</span>
            </a>
        </li>
        <li class="<?php
        if ($user == 'collector') {
            echo 'active';
        }
        ?>">
            <a tabindex="-1" href="emvs.php?action=collector">
                <i class="fa fa-user" aria-hidden="true"></i>
                <span style="vertical-align:middle;">&nbsp;&nbsp;Collectors</span>
            </a>
        </li>

        <li class="<?php
        if ($pos == '') {
            if (($user == 'drivers') || ($user == 'adddrivers') || ($user == 'title') || ($user == 'hosts') || ($user == 'addhosts') || ($user == 'addrabbis') || ($user == 'rabbis')) {
                echo 'active parent';
            } else {
                echo 'parent';
            }
        } else {
            $u1 = strstr($user, '&&', true);
            if (($u1 == 'drivers') || ($u1 == 'adddrivers') || ($u1 == 'hosts') || ($u1 == 'addhosts') || ($u1 == 'addrabbis') || ($u1 == 'rabbis') || ($u1 == 'sites') || ($u1 == 'sites_addnew')) {
                echo 'active parent';
            } else {
                echo 'parent';
            }
        }
        ?>">
            <a tabindex="-1" href="">
                <i class="fa fa-search" aria-hidden="true"></i>
                <span>&nbsp;&nbsp;Lookups</span>
            </a>
            <ul class="children">
                <li class="<?php
                if ($user == 'title') {
                    echo 'active';
                }
                ?>">
                    <a href="emvs.php?action=title">
                        <span style="vertical-align:middle;">Title</span>
                    </a>
                </li>
                <!--<li class="<?php
                if ($user == 'rabbis') {
                    echo 'active';
                }
                ?><?php
                if ($pos == '') {
                    if ($user == 'addrabbis') {
                        echo 'active';
                    }
                } else {
                    $u1 = strstr($user, '&&', true);
                    if ($u1 == 'addrabbis') {
                        echo 'active';
                    }
                }
                ?>">
                                <a href="emvs.php?action=rabbis">Manager</a>
                            </li>-->
                <li class="<?php
                if ($user == 'drivers') {
                    echo 'active';
                }
                ?>
                            <?php
                if ($pos == '') {
                    if ($user == 'adddrivers') {
                        echo 'active';
                    }
                } else {
                    $u1 = strstr($user, '&&', true);
                    if ($u1 == 'adddrivers') {
                        echo 'active';
                    }
                }
                ?>">
                    <a href="emvs.php?action=drivers">Drivers</a>
                </li>
                <li class="<?php
                if ($user == 'hosts') {
                    echo 'active';
                }
                ?>
                            <?php
                if ($pos == '') {
                    if ($user == 'addhosts') {
                        echo 'active';
                    }
                } else {
                    $u1 = strstr($user, '&&', true);
                    if ($u1 == 'addhosts') {
                        echo 'active';
                    }
                }
                ?>">
                    <a href="emvs.php?action=hosts">Hosts</a>
                </li>
            </ul>
        </li>
        <?php
        if ($roles == 'Administrator' ) { ?>
            <li class="<?php
            if ($pos == '') {

                if (($user == 'manager') || ($user == 'Administrator') || ($user == 'sites') ||
                    ($user == 'sites_addnew') ||
                    ($user == 'requiredFields&fieldTypecollectorDetails') ||
                    ($user == 'requiredFields&fieldTypevisits') ||
                    ($user == 'requiredFields&fieldTypeaddress') ||
                    ($user == 'requiredFields&fieldTypefunds') ||
                    ($user == 'requiredFields&fieldTypepurpose') ||
                    ($user == 'modificationDocs') || ($user == 'appoptions')
                ) {
                    echo 'active parent';
                } else {
                    echo 'parent';
                }
            } else {

                $u1 = strstr($user, '&&', true);

                if (($u1 == 'drivers') || ($u1 == 'adddrivers') || ($u1 == 'hosts') || ($u1 == 'addhosts') ||
                    ($u1 == 'addrabbis') || ($u1 == 'rabbis') || ($u1 == 'sites') || ($u1 == 'sites_addnew') ||
                    ($u1 == 'addadmin') || ($u1 == 'addmanager') || ($u1 == 'addRequiredField&fieldTypecollectorDetails') ||
                    ($u1 == 'addRequiredField&fieldTypevisits') || ($u1 == 'addRequiredField&fieldTypeaddress') ||
                    ($u1 == 'addRequiredField&fieldTypefunds') || ($u1 == 'addRequiredField&fieldTypepurpose') ||
                    ($u1 == 'addappoption')
                ) {
                    echo 'active parent';
                } else {
                    echo 'parent';
                }
            }
            ?>	">
                <a tabindex="-1" href="">
                    <i class="fa fa-user-secret" aria-hidden="true"></i>
                    <span>&nbsp;&nbsp;Admin</span>
                </a>
                <ul class="children">
                    <li class="<?php
                    if ($user == 'manager') {
                        echo 'active';
                    }
                    ?>">
                        <a href="emvs.php?action=manager">
                            <span style="vertical-align:middle;">Manager</span>
                        </a>
                    </li>
                    <li class="<?php
                    if ($user == 'Administrator') {
                        echo 'active';
                    }
                    ?><?php
                    if ($pos == '') {
                        if ($user == 'addadmin') {
                            echo 'active';
                        }
                    } else {
                        $u1 = strstr($user, '&&', true);
                        if ($u1 == 'addadmin') {
                            echo 'active';
                        }
                    }
                    ?>">
                        <a href="emvs.php?action=Administrator">
                            <span style="vertical-align:middle;">Administrator</span>
                        </a>
                    </li>
                    <li class="<?php
                    if ($user == 'sites') {
                        echo 'active';
                    }
                    ?><?php
                    if ($pos == '') {
                        if ($user == 'sites_addnew') {
                            echo 'active';
                        }
                    } else {
                        $u1 = strstr($user, '&&', true);
                        if ($u1 == 'sites_addnew') {
                            echo 'active';
                        }
                    }
                    ?>">
                        <a href="emvs.php?action=sites">Sites</a>
                    </li>
                    <li class="<?php
                    if ($user == 'modificationDocs') {
                        echo 'active';
                    }
                    ?>">
                        <a href="emvs.php?action=modificationDocs">Modification Documents</a>
                    </li>

                    <li class="<?php
                    if ($user == 'appoptions') {
                        echo 'active';
                    }
                    ?><?php
                    if ($pos == '') {
                        if ($user == 'addappoption') {
                            echo 'active';
                        }
                    } else {
                        $u1 = strstr($user, '&&', true);
                        if ($u1 == 'addappoption') {
                            echo 'active';
                        }
                    }
                    ?>">
                        <a href="emvs.php?action=appoptions">Application Options</a>
                    </li>

                    <li

                        class="<?php
                        if ($pos == '') {
                            if (($user == 'requiredFields&fieldTypecollectorDetails') ||
                                ($user == 'requiredFields&fieldTypevisits') ||
                                ($user == 'requiredFields&fieldTypeaddress') ||
                                ($user == 'requiredFields&fieldTypefunds') ||
                                ($user == 'requiredFields&fieldTypepurpose')
                            ) {
                                echo 'active2 parent2';
                            } else {
                                echo 'parent2';
                            }
                        } else {
                            $u1 = strstr($user, '&&', true);
                            if (($u1 == 'requiredFields&fieldTypecollectorDetails') ||
                                ($u1 == 'requiredFields&fieldTypevisits') ||
                                ($u1 == 'requiredFields&fieldTypeaddress') ||
                                ($u1 == 'requiredFields&fieldTypefunds') ||
                                ($u1 == 'requiredFields&fieldTypepurpose') || ($u1 == 'addRequiredField&fieldTypecollectorDetails') ||
                                ($u1 == 'addRequiredField&fieldTypevisits') || ($u1 == 'addRequiredField&fieldTypeaddress') ||
                                ($u1 == 'addRequiredField&fieldTypefunds') || ($u1 == 'addRequiredField&fieldTypepurpose')
                            ) {
                                echo 'active2 parent2';
                            } else {
                                echo 'parent2';
                            }
                        }
                        ?>	">
                        <a href="">
                            <img width="15" height="15" src="images/lookup.png">
                            <span>Required Fields</span>
                        </a>
                        <ul class="children2" id="level2"
                            <?php
                            if ($pos == '') {
                                if (($user == 'sites') || ($user == 'sites_addnew') ||
                                    ($user == 'Administrator') || ($user == 'addadmin') ||
                                    ($user == 'manager') || ($user == 'addmanager') ||
                                    ($user == 'modificationDocs') || ($user == 'appoptions')
                                ) {
                                    echo ' style= "display:none;" ';
                                }
                            } else {
                                $u1 = strstr($user, '&&', true);
                                if (($u1 == 'sites') || ($u1 == 'sites_addnew') ||
                                    ($u1 == 'Administrator') || ($u1 == 'addadmin') || ($u1 == 'manager') ||
                                    ($u1 == 'addmanager') || ($u1 == 'addappoption')
                                ) {
                                    echo ' style="display:none;" ';
                                }
                            }
                            ?>
                        >
                            <li class="<?php

                            ?><?php
                            if ($pos == '') {
                                if ($user == 'requiredFields&fieldTypecollectorDetails') {
                                    echo 'active';
                                }
                            } else {
                                $u1 = strstr($user, '&&', true);
                                if (($u1 == 'requiredFields&fieldTypecollectorDetails') || ($u1 == 'addRequiredField&fieldTypecollectorDetails')) {
                                    echo 'active';
                                }
                            }
                            ?>">
                                <a href="emvs.php?action=requiredFields&fieldType=collectorDetails">New Collector
                                    Details And Personal Info</a>
                            </li>
                            <li class="<?php

                            ?><?php
                            if ($pos == '') {
                                if ($user == 'requiredFields&fieldTypevisits') {
                                    echo 'active';
                                }
                            } else {
                                $u1 = strstr($user, '&&', true);
                                if (($u1 == 'requiredFields&fieldTypevisits') || ($u1 == 'addRequiredField&fieldTypevisits')) {
                                    echo 'active';
                                }
                            }
                            ?>">
                                <a href="emvs.php?action=requiredFields&fieldType=visits">Visits Tab</a>
                            </li>
                            <li class="<?php
                            ?><?php
                            if ($pos == '') {
                                if ($user == 'requiredFields&fieldTypeaddress') {
                                    echo 'active';
                                }
                            } else {
                                $u1 = strstr($user, '&&', true);
                                if ($u1 == 'requiredFields&fieldTypeaddress' || ($u1 == 'addRequiredField&fieldTypeaddress')) {
                                    echo 'active';
                                }
                            }
                            ?>">
                                <a href="emvs.php?action=requiredFields&fieldType=address">Address Tab</a>
                            </li>
                            <li class="<?php
                            ?><?php
                            if ($pos == '') {
                                if ($user == 'requiredFields&fieldTypefunds') {
                                    echo 'active';
                                }
                            } else {
                                $u1 = strstr($user, '&&', true);
                                if ($u1 == 'requiredFields&fieldTypefunds' || ($u1 == 'addRequiredField&fieldTypefunds')) {
                                    echo 'active';
                                }
                            }
                            ?>">
                                <a href="emvs.php?action=requiredFields&fieldType=funds">Funds Tab</a>
                            </li>
                            <li class="<?php

                            ?><?php
                            if ($pos == '') {
                                if ($user == 'requiredFields&fieldTypepurpose') {
                                    echo 'active';
                                }
                            } else {
                                $u1 = strstr($user, '&&', true);
                                if ($u1 == 'requiredFields&fieldTypepurpose' || ($u1 == 'addRequiredField&fieldTypepurpose')) {
                                    echo 'active';
                                }
                            }
                            ?>">
                                <a href="emvs.php?action=requiredFields&fieldType=purpose">Purpose Tab</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            <?php
            if ($user == 'tools') {
                echo 'active';
            }
            ?>

            <li class="<?= ($user == 'import') ? 'active' : "" ?>">
                <a tabindex="-1" href="emvs.php?action=import">
                    <i class="fa fa-mail-forward" aria-hidden="true"></i>
                    <span style="vertical-align:middle;">&nbsp;&nbsp;Import</span>
                </a>
            </li>
        <?php } ?>
        <li>
            <a tabindex="-1" href="emvs.php?action=signout">
                <i class="fa fa-sign-out" aria-hidden="true"></i>
                <span>&nbsp;&nbsp;Sign Out</span>
            </a>
        </li>
    </ul>
</div>
<!-- leftpanel -->