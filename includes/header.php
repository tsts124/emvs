<?php
ini_set("display_errors", 0);
if (empty($_SESSION['user'])) {
    header('Location: emvs.php?action=index');
    $_POST['messagesession'] = 'you lost session';
} else {
    $_POST['messagesession'] = '';
}
$user = $_SESSION['user'];

/**
 * @param $year
 * @param null $day
 * @param null $month
 * @return null|string
 */
function createDatefromDB($year, $day = null, $month = null)
{
    $dob = null;

    if ($year && $day && empty($month)) {
        $dob = $day . '-' . $year;
    } elseif ($year && $month && empty($day)) {
        $dob = $month . '- -' . $year;
    } elseif ($year && $day && $month) {
        $dob = $month . '-' . $day . '-' . $year;
    }

    return $dob ? $dob : $year;
}


/**
 * @param string $date
 * @return array
 */
function parserDate($date)
{
    $parsedate = [
        'year'  => '',
        'month' => '',
        'day'   => ''
    ];
    $dates = explode('-', $date);

    switch (count($dates)) {
        case 1:
            $parsedate['year'] = $dates[0];
            break;
        case 2: {
            $parsedate['day'] = $dates[0];
            $parsedate['year'] = $dates[1];
            break;
        }
        case 3: {
            $parsedate['month'] = $dates[0];
            $parsedate['day'] = $dates[1];
            $parsedate['year'] = $dates[2];
            break;
        }
        default:
            break;
    }

    return $parsedate;
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description">
    <meta name="author">
    <title>Electronic Meshulach Verification System</title>
    <link href="font-awesome-4.6.3/css/font-awesome.min.css" rel="stylesheet">
    <?php
    echo '<link href="css/style.default.css?v=' . time() . '" rel="stylesheet">';
    ?>
    <link href="css/select2.css" rel="stylesheet"/>
    <link href="css/dataTables.bootstrap.css" rel="stylesheet">
    <link href="css/style.datatables.css" rel="stylesheet">
    <link href="css/dataTables.responsive.css" rel="stylesheet">

    <script src="pellepim-jstimezonedetect/dist/jstz.min.js"></script>
    <script src="js/moment.min.js"></script>
    <script src="js/moment-timezone-with-data-2010-2020.min.js"></script>

    <script src="signature.js"></script>
    <link rel="stylesheet" href="css/pygment_trac.css">
    <script src="js/jquery-1.9.1.min.js"></script>
    <script src="js/scale.fix.js"></script>
    <script src="js/jquery.formance.min.js"></script>
    <script src="js/awesome_form.js"></script>
    <script src="js/birthDate.js"></script>

    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/jquery.datetextentry.js"></script>
    <!-- Maxium Number of Character-->
    <script type="text/javascript" src="js/textvalid.js?v=<?= time() ?>"></script>
    <!-- Magnific Popup core CSS file -->
    <link rel="stylesheet" href="magnific-popupp/dist/magnific-popup.css">
    <!-- Tool Tip -->
    <style type="text/css">
        .first {
            display: none;
        }

        .second {
            display: show;
        }

        #tooltip {
            text-align: center;
            color: #fff;
            background: #111;
            position: absolute;
            z-index: 100;
            padding: 15px;
        }

        #tooltip:after /* triangle decoration */
        {
            width: 0;
            height: 0;
            border-left: 10px solid transparent;
            border-right: 10px solid transparent;
            border-top: 10px solid #111;
            content: '';
            position: absolute;
            left: 50%;
            bottom: -10px;
            margin-left: -10px;
        }

        #tooltip.top:after {
            border-top-color: transparent;
            border-bottom: 10px solid #111;
            top: -20px;
            bottom: auto;
        }

        #tooltip.left:after {
            left: 10px;
            margin: 0;
        }

        #tooltip.right:after {
            right: 10px;
            left: auto;
            margin: 0;
        }
    </style>
    <script>
        $(document).ready(function () {
            $('body').on('focus', '#basicTable_filter input', function () {
                $(this).attr('type', 'text');
                $(this).attr('autocorrect', 'off');
                $(this).attr('spellcheck', 'false');
            });

            $('#personalcomments').on('change focusout', function () {
                $('#personal-comments-div').html($(this).val());
            });

            $('.city-buttons > div').on('click touch', function () {
                var $city = $('#city');

                if ($(this).attr('id') == 'BB') {
                    $city.val("B'nai Brak");
                } else {
                    $city.val('Beitar');
                }

                $city.change();
            });

            $('.instit-city-buttons > div').on('click touch', function () {
                var $city = $('#institcity');

                if ($(this).attr('id') == 'BB') {
                    $city.val("B'nai Brak");
                } else {
                    $city.val('Beitar');
                }

                $city.change();
            });
        });

        function deleteDoc(event) {
            var del = confirm('Do you want to delete this picture?');
            if (del) {
                var pictureName = $(this).parent().attr('id');
                $(this).parent().remove();
                var data = 'pictureName=' + pictureName;
                $.ajax({
                    data: data,
                    type: 'POST',
                    url: 'deleteDoc.php'
                });
            }
        }

        function loginChars() {
            var charCode = event.charCode;
            switch (true) {
                case(charCode === 46):
                case(charCode === 64):
                case(charCode > 47 && charCode < 58):
                case(charCode > 64 && charCode < 91):
                case(charCode > 96 && charCode < 123):
                    break;
                default:
                    event.preventDefault();
            }
        }

        function checkEmail(value, submitButton) {
            $('button[name="' + submitButton + '"]').attr('disabled', 'disabled');
            var result = '';
            var data = 'email=' + value;
            var $emailField = $('#emailid');
            var email = $emailField.val();
            var currentEmail = $('#current-email').val();

            if (email !== currentEmail) {
                $.ajax({
                    type: 'POST',
                    url: 'ajax_emailValidation.php',
                    data: data,
                    success: function (msg) {
                        result = msg;
                        if (result === 'error') {
                            $emailField.val('');
                            $emailField.css('border', '1px solid red');
                            $('#emailError').remove();
                            $('#emailDiv').append('<span class="ValidationErrors" id="emailError" >Email ' + value + ' is already in use</span>');
                        } else {
                            $('button[name="' + submitButton + '"]').removeAttr('disabled');
                            $emailField.css('border', '');
                            $emailField.parent().find('.ValidationErrors').remove();
                        }
                    }
                });
            } else {
                $('button[name="' + submitButton + '"]').removeAttr('disabled');
            }
        }

        function hostChange(object) {
            var nameField = $('#free-formed-host');
            var freeHost = $('input[name="free_formed_host"]');
            var $sitesHost = $('#siteshost');

            if (object.val() != 0) {

                if (object.attr('id') == 'siteshost') {
                    nameField.val(object.val());
                } else if ($sitesHost.val() == 0) {
                    nameField.val(object.val());
                }

            } else if ((object.val() == 0) && (object.attr('id') == 'siteshost')) {
                nameField.val(freeHost.val());
            }

            var data = 'hostName=' + $sitesHost.val();

            $.ajax({
                method: 'POST',
                url: 'host-info.php',
                data: data,
                success: function (msg) {
                    var address = $.parseJSON(msg)['address'];
                    var phone = $.parseJSON(msg)['phone'];

                    $('#baltiaddress1').val(address);
                    $('#baltiphone').val(phone);
                }
            });
        }

        function getPrevComment(commentFieldId, column, collectorId, visitId) {
            console.log(arguments);

            var data = 'column=' + column +
                '&collectorId=' + collectorId +
                '&visitId=' + visitId;
            $.ajax({
                type: 'POST',
                url: 'getPrevComment.php',
                data: data,
                success: function (msg) {
                    $('#' + commentFieldId).html(msg);
                }
            });
        }

        function yesnoCheck(yes, element) {
            if (yes == 'Individual') {
                $(element).closest('.first-block').find('.panel-body').css('display', 'none');
            } else {
                $(element).closest('.first-block').find('.panel-body').css('display', 'block');
            }
        }

        function alertcomments(status) {
            status = !status;
            if (status) {
                document.getElementById("personalcomments").style.display = 'none';
                document.getElementById("view-alert").style.display = 'none';
            } else {
                document.getElementById("personalcomments").style.display = 'block';
                document.getElementById("view-alert").style.display = 'inline-block';
            }
        }

        function purpose_other(status) {
            if (status) {
                document.getElementById('target').style.visibility = 'visible';
            } else {
                document.getElementById('target').style.visibility = 'hidden';
            }

        }

        //Check SSN Id format
        function myssn(ssn, count, obj) {
            var $select = $('#' + obj.id);

            var country = $select.closest('.collident').find('input[name="identificationcountry1[]"]').val();
            if (!country) {
                country = $select.closest('.collident').find('input[name="country"]').val();
            }
            if (!country) {
                country = $select.closest('.collident').find('input[name="Collcountry[]"]').val();
            }

            var number = $select.closest('.collident').find('input[name="Identno1[]"]').val();
            if (!number) {
                number = $select.closest('.collident').find('input[name="detail_number"]').val();
            }
            if (!number) {
                number = $select.closest('.collident').find('input[name="Collphone[]"]').val();
            }

            var dataString = 'ssn=' + ssn +
                '&country=' + country +
                '&number=' + number +
                '&ssncount=' + count;
            $.ajax({
                type: "POST",
                url: "ajax_ssnid.php",
                data: dataString,
                success: function (msg) {
                    $('#myssn' + count).html(msg);

                    $(obj).closest('.row').find('input[name="Collcountry[]"]').focus();
                }
            });
        }

        function reference_other(status) {
            if (status) {
                document.getElementById('target1').style.visibility = 'visible';
            } else {
                document.getElementById('target1').style.visibility = 'hidden';
            }

        }

        function identCountryFocus(obj) {
            $(obj).closest('.row').find('input[name="country"]').focus();
        }

        function contactCountryFocus(obj) {
            $(obj).closest('#addinput').find('input[name="Phone_Country[]"]').focus();
        }

        function removeIdent(id) {
            if (confirm("Do you really wish to delete this record?")) {
                var visitid = $('#the-visit-id').val();
                var type = 'identification';
                var dataString = 'id=' + id +
                    '&visitid=' + visitid +
                    '&type=' + type +
                    '&thisTable=visit_details';
                $.ajax({
                    type: "POST",
                    url: "ajax_delete.php",
                    data: dataString,
                    success: function (msg) {
                        if (msg == 'true') {
                            $("div[data-id='" + id + "']").css('display', 'none');
                        } else {
                            alert('At least one row of Identification Details is required');
                        }
                    }
                });
            }
        }

        function rotateDoc() {
            var _this = this;
            var animate = $(this).attr('data-animate-angle');
            var rotate = $(this).attr('data-rotate-angle');

            $(this).attr('data-animate-angle', +animate + +90);
            $(this).attr('data-rotate-angle', +rotate + +90);

            $(this).parent().find('img').rotate({animateTo: +animate + +90});
            var pictureName = $(this).parent().attr('id');

            var degrees = -$(this).attr('data-rotate-angle');
            setTimeout(function () {
                var save = confirm('Re-save the picture?');

                if (save) {
                    $(_this).closest('.picture').find('.loading').css('display', 'block');
                    $(_this).attr('data-rotate-angle', 0);

                    var data = 'pictureName=' + pictureName + '&degrees=' + degrees;
                    $.ajax({
                        url: 'rotate.php',
                        type: 'POST',
                        data: data,
                        success: function (msg) {
                            $(_this).closest('.picture').find('.loading').css('display', 'none');
                        }
                    });
                }
            }, 1000);
        }

        function take_snapshot(a1, b1) {
            $('#results').closest('.picture').find('.loading:first-of-type').css('display', 'block');
            Webcam.snap(function (data_uri) {
                document.getElementById('a1').value = a1;
                document.getElementById('b1').value = b1;
                document.getElementById('imgsrc1').value = data_uri;

                var fd = new FormData(document.forms["valWizard"]);
                var xhr = new XMLHttpRequest();
                xhr.open('POST', 'uploadphoto.php', true);

                xhr.onload = function (e) {
                    if (xhr.readyState === 4) {
                        if (xhr.status === 200) {
                            $('#results').closest('.picture').find('.loading').css('display', 'none');
                            var imageName = xhr.responseText;
                            document.getElementById('results').innerHTML =
                                '<div class="doc-wrapper" id="' + imageName + '">' +
                                '<a href="uploads/' + imageName + '" class="doc-picture">' +
                                '<img class="docPicture" src="uploads/' + imageName + '" height="150px">' +
                                '</a>' +
                                '<div data-animate-angle="0" data-rotate-angle="0" ' +
                                'class="rotate btn btn-warning">ROTATE PICTURE</div>' +
                                '<div class="cross btn btn-danger">DELETE PICTURE</div>' +
                                '</div>';
                        }
                    }
                };
                xhr.send(fd);
            });
        }

        function signatureCapture() {
            var canvas = document.getElementById("newSignature");
            var context = canvas.getContext("2d");
            canvas.width = 400;
            canvas.height = 100;
            context.fillStyle = "#fff";
            context.strokeStyle = "#444";
            context.lineWidth = 1.5;
            context.lineCap = "round";
            context.fillRect(0, 0, canvas.width, canvas.height);
            var disableSave = true;
            var pixels = [];
            var cpixels = [];
            var xyLast = {};
            var xyAddLast = {};
            var calculate = false;
            { 	//functions
                function remove_event_listeners() {
                    canvas.removeEventListener('mousemove', on_mousemove, false);
                    canvas.removeEventListener('mouseup', on_mouseup, false);
                    canvas.removeEventListener('touchmove', on_mousemove, false);
                    canvas.removeEventListener('touchend', on_mouseup, false);

                    document.body.removeEventListener('mouseup', on_mouseup, false);
                    document.body.removeEventListener('touchend', on_mouseup, false);
                }

                function get_coords(e) {
                    var x, y;

                    if (e.changedTouches && e.changedTouches[0]) {
                        var offsety = canvas.offsetTop || 0;
                        var offsetx = canvas.offsetLeft || 0;

                        x = e.changedTouches[0].pageX - offsetx;
                        y = e.changedTouches[0].pageY - offsety;
                    } else if (e.layerX || 0 == e.layerX) {
                        x = e.layerX;
                        y = e.layerY;
                    } else if (e.offsetX || 0 == e.offsetX) {
                        x = e.offsetX;
                        y = e.offsetY;
                    }

                    return {
                        x: x, y: y
                    };
                };

                function on_mousedown(e) {
                    e.preventDefault();
                    e.stopPropagation();

                    canvas.addEventListener('mouseup', on_mouseup, false);
                    canvas.addEventListener('mousemove', on_mousemove, false);

                    canvas.addEventListener('touchstart', on_touchstart, false);
                    canvas.addEventListener('touchmove', on_touchmove, false);
                    canvas.addEventListener('touchend', on_touchend, false);
                    canvas.addEventListener('touchcancel', on_touchcancel, false);

                    canvas.addEventListener('touchend', on_mouseup, false);
                    canvas.addEventListener('touchmove', on_mousemove, false);
                    document.body.addEventListener('mouseup', on_mouseup, false);
                    document.body.addEventListener('touchend', on_mouseup, false);

                    empty = false;
                    var xy = get_coords(e);
                    context.beginPath();
                    pixels.push('moveStart');
                    context.moveTo(xy.x, xy.y);
                    pixels.push(xy.x, xy.y);
                    xyLast = xy;
                };

                function on_mousemove(e, finish) {
                    e.preventDefault();
                    e.stopPropagation();

                    var xy = get_coords(e);
                    var xyAdd = {
                        x: (xyLast.x + xy.x) / 2,
                        y: (xyLast.y + xy.y) / 2
                    };

                    if (calculate) {
                        var xLast = (xyAddLast.x + xyLast.x + xyAdd.x) / 3;
                        var yLast = (xyAddLast.y + xyLast.y + xyAdd.y) / 3;
                        pixels.push(xLast, yLast);
                    } else {
                        calculate = true;
                    }

                    context.quadraticCurveTo(xyLast.x, xyLast.y, xyAdd.x, xyAdd.y);
                    pixels.push(xyAdd.x, xyAdd.y);
                    context.stroke();
                    context.beginPath();
                    context.moveTo(xyAdd.x, xyAdd.y);
                    xyAddLast = xyAdd;
                    xyLast = xy;

                };

                function on_mouseup(e) {
                    remove_event_listeners();
                    disableSave = false;
                    context.stroke();
                    pixels.push('e');
                    calculate = false;
                };
                function on_touchstart(e, finish) {
                    e.preventDefault();
                    e.stopPropagation();

                    var xy = get_coords(e);
                    var xyAdd = {
                        x: (xyLast.x + xy.x) / 2,
                        y: (xyLast.y + xy.y) / 2
                    };

                    if (calculate) {
                        var xLast = (xyAddLast.x + xyLast.x + xyAdd.x) / 3;
                        var yLast = (xyAddLast.y + xyLast.y + xyAdd.y) / 3;
                        pixels.push(xLast, yLast);
                    } else {
                        calculate = true;
                    }

                    context.quadraticCurveTo(xyLast.x, xyLast.y, xyAdd.x, xyAdd.y);
                    pixels.push(xyAdd.x, xyAdd.y);
                    context.stroke();
                    context.beginPath();
                    context.moveTo(xyAdd.x, xyAdd.y);
                    xyAddLast = xyAdd;
                    xyLast = xy;

                };
            }
            canvas.addEventListener('touchstart', on_mousedown, false);
            canvas.addEventListener('mousedown', on_mousedown, false);
        }

        function signatureSave(a, b) {
            var canvas = document.getElementById("newSignature");// save canvas image as data url (png format by default)
            //alert(canvas);
            var dataURL = canvas.toDataURL("image/png");
            var a = a;
            var b = b;
            document.getElementById("saveSignature").src = dataURL;
            document.getElementById('imgsrc').value = dataURL;
            document.getElementById('a').value = a;
            document.getElementById('b').value = b;
            //alert(dataURL);
            var fd = new FormData(document.forms["valWizard"]);
            var xhr = new XMLHttpRequest();
            xhr.open('POST', 'upload_data.php', true);

            xhr.upload.onprogress = function (e) {
                if (e.lengthComputable) {
                    var percentComplete = (e.loaded / e.total) * 100;
                    console.log(percentComplete + '% uploaded');
                    // alert('Succesfully uploaded');
                }
            };

            xhr.onload = function () {

            };
            xhr.send(fd);
        }

        function signatureClear() {
            var canvas = document.getElementById("newSignature");
            var context = canvas.getContext("2d");
            document.getElementById("imgsrc").src = context;
            context.clearRect(0, 0, canvas.width, canvas.height);
        }
    </script>
    <!--Text Number Validation-->
    <script>
        $(document).ready(function () {

            //attach keypress to input
            $('.input').keydown(function (event) {
                // Allow special chars + arrows 
                if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9
                    || event.keyCode == 27 || event.keyCode == 13
                    || (event.keyCode == 65 && event.ctrlKey === true)
                    || (event.keyCode >= 35 && event.keyCode <= 39)) {
                    return;
                } else {
                    // If it's not a number stop the keypress
                    if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                        event.preventDefault();
                    }
                }
            });

            $('.alertRow').on('click touch', 'a', promptPassword);
        });

        function promptPassword() {
            var passcode = prompt('Enter the passcode:');
            if (+passcode !== 435) {
                event.preventDefault();
                if (passcode) {
                    alert('Wrong passcode!');
                }
            }
        }

        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }

        function onlyAlphabetsAndNumbers(e, t) {
            try {
                if (window.event) {
                    var charCode = window.event.keyCode;
                }
                else if (e) {
                    var charCode = e.which;
                }
                else {
                    return true;
                }
                if ((charCode > 47 && charCode < 58) || (charCode > 47 && charCode < 58) || (charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || (charCode === 32))
                    return true;
                else
                    return false;
            }
            catch (err) {
                alert(err.Description);
            }
        }

        function onlyAlphabets(e, t) {
            try {
                if (window.event) {
                    var charCode = window.event.keyCode;
                }
                else if (e) {
                    var charCode = e.which;
                }
                else {
                    return true;
                }

                if ((charCode > 32 && charCode < 45) || (charCode > 45 && charCode < 48) || (charCode > 57 && charCode < 65) || (charCode > 90 && charCode < 97) || (charCode > 122 && charCode < 127)) {
                    return false;
                }
                else {
                    return true;
                }
            }
            catch (err) {
                alert(err.Description);
            }
        }
    </script>
</head>
<body>
<header>
    <div class="headerwrapper">
        <div class="header-left">
            <img class="logo" src="images/logo.png" alt=""/>
            <div class="pull-right">
                <a href="" class="menu-collapse">
                    <img src="images/left-right.png"/>
                </a>
            </div>

            <div class="pull-right">
                <div class="btn-group btn-group-option" style="margin-left:97%;">
                    <ul class="dropdown-menu pull-right" role="menu">
                        <li><a href="emvs.php?action=signout"> Sign Out</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="pagename"></div>
    </div>
</header>
