<?php

include 'includes/header.php';
include 'includes/dbcon.php';

$user1 = $_SESSION['user'];
$sql = $dbh->prepare("select * from `newmember` where `emailid`='$user1' and `Delete` = 0; ");
$sql->execute();
$data = $sql->fetch();
$_SESSION['roles'] = $data['roles'];
$roles = $_SESSION['roles'];

?>

<section>
    <div class="mainwrapper">
        <?php include 'includes/leftpanel.php'; ?>
        <div class="mainpanel">
            <div class="pageheader">
                <div>
                    <div class="media-body">
                        <h4>Welcome EMVS</h4>
                    </div>
                </div>
                <!-- media -->
            </div>
            <!-- pageheader -->
            <div class="contentpanel">
                <div class="row row-stat">
                    <div class="col-md-4">
                        <div class="panel panel-success-alt noborder">
                            <div class="panel-heading noborder">
                                <div class="media-body">
                                    <?php
                                    #echo date('T');
                                    #echo $roles;
                                    if ($roles == 'Manager') {

                                        echo '<a href="emvs.php?action=collector">';
                                        echo '<h5 style="color:#fff"><center>Collectors</center>';
                                        echo '</h5></a>';

                                        $collquery1 = $dbh->prepare("select count(Id)as Id from collectors order by Id desc");
                                        $collquery1->execute();
                                        $colldata1 = $collquery1->fetch();
                                        $sqldata1 = $colldata1['user'];
                                        echo '<a href="emvs.php?action=collector">';
                                        echo '<h1 style="color:#fff">';
                                        echo '<center>' . $colldata1['Id'] . '</center>';
                                        echo '</h1>';
                                        echo '</a>';
                                    } else {

                                        echo '<a href="emvs.php?action=collector">';
                                        echo '<h5 style="color:#fff">';
                                        echo '<center>Collectors</center>';
                                        echo '</h5>';
                                        echo '</a>';

                                        $collquery1 = $dbh->prepare("select count(Id)as Id from collectors order by Id desc");
                                        $collquery1->execute();
                                        $colldata1 = $collquery1->fetch();

                                        echo '<a href="emvs.php?action=collector">';
                                        echo '<h1 style="color:#fff">';
                                        echo '<center>' . $colldata1['Id'] . '</center>';
                                        echo '</h1>';
                                        echo '</a>';
                                    }
                                    ?>
                                </div>
                                <!-- media-body -->
                                <div class="clearfix mt20">
                                    <div class="pull-left">
                                        <h5 class="md-title nomargin"></h5>
                                        <h4 class="nomargin"></h4>
                                    </div>
                                </div>
                            </div>
                            <!-- panel-body -->
                        </div>
                        <!-- panel -->
                    </div>
                    <?php if ($roles == 'Manager'): ?>
                        <div class="col-md-4">
                            <div class="panel panel-pink noborder">
                                <div class="panel-heading noborder">
                                    <div class="media-body">
                                        <?php
                                        echo '<a href="emvs.php?action=drivers">';
                                        echo '<h5 style="color:#fff"><center>Drivers</center></h5>';
                                        echo '</a>';
                                        $collquery1 = $dbh->prepare("SELECT COUNT(Sites) AS Sites FROM drivers WHERE drivers.Delete !=1");
                                        $collquery1->execute();
                                        $colldata1 = $collquery1->fetch();
                                        echo '<a href="emvs.php?action=drivers">';
                                        echo '<h1 style="color:#fff"><center>' . $colldata1['Sites'] . '</center></h1>';
                                        echo '</a>';
                                        ?>
                                    </div>
                                    <!-- media-body -->
                                    <div class="clearfix mt20">
                                        <div class="pull-left">
                                            <h5 class="md-title nomargin"></h5>
                                            <h4 class="nomargin"></h4>
                                        </div>
                                    </div>
                                </div>
                                <!-- panel-body -->
                            </div>
                            <!-- panel -->
                        </div>
                        <div class="col-md-4">
                            <div class="panel panel-orange noborder">
                                <div class="panel-heading noborder">
                                    <div class="media-body">
                                        <?php
                                        echo '<a href="emvs.php?action=hosts">';
                                        echo '<h5 style="color:#fff">';
                                        echo '<center>Hosts</center>';
                                        echo '</h5>';
                                        echo '</a>';
                                        $collquery1 = $dbh->prepare("select count(Sites) as Sites from hosts");
                                        $collquery1->execute();
                                        $colldata1 = $collquery1->fetch();
                                        echo '<a href="emvs.php?action=hosts">';
                                        echo '<h1 style="color:#fff">';
                                        echo '<center>' . $colldata1['Sites'] . '</center>';
                                        echo '</h1>';
                                        echo '</a>';
                                        ?>
                                    </div>
                                    <!-- media-body -->
                                    <div class="clearfix mt20">
                                        <div class="pull-left">
                                            <h5 class="md-title nomargin"></h5>
                                            <h4 class="nomargin"></h4>
                                        </div>
                                    </div>
                                </div>
                                <!-- panel-body -->
                            </div>
                            <!-- panel -->
                        </div>
                        <div class="col-md-4">
                            <div class="panel panel-red noborder">
                                <div class="panel-heading noborder">
                                    <div class="media-body">
                                        <?php

                                        echo '<a href="emvs.php?action=title">';
                                        echo '<h5 style="color:#fff">';
                                        echo '<center>Titles</center>';
                                        echo '</h5>';
                                        echo '</a>';

                                        $collquery1 = $dbh->prepare("select count(`Id`) as `Id` from  `title` where `Delete`=0 ;");
                                        $collquery1->execute();
                                        $colldata1 = $collquery1->fetch();

                                        echo '<a href="emvs.php?action=title">';
                                        echo '<h1 style="color:#fff">';
                                        echo '<center>' . $colldata1['Id'] . '</center>';
                                        echo '</h1>';
                                        echo '</a>';

                                        ?>
                                    </div>
                                    <div class="clearfix mt20">
                                        <div class="pull-left">
                                            <h5 class="md-title nomargin"></h5>
                                            <h4 class="nomargin"></h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <!-- col-md-4 -->
                    <?php if ($roles == 'Administrator') { ?>

                        <div class="col-md-4">
                            <div class="panel panel-dark noborder">
                                <div class="panel-heading noborder">
                                    <div class="media-body">
                                        <?php

                                        echo '<a href="emvs.php?action=rabbis">';
                                        echo '<h5 style="color:#fff">';
                                        echo '<center>Managers</center>';
                                        echo '</h5>';
                                        echo '</a>';

                                        $collquery1 = $dbh->prepare("select count(Id)as Id from  newmember where `Delete`=0 AND `roles`='Manager'");
                                        $collquery1->execute();
                                        $colldata1 = $collquery1->fetch();

                                        echo '<a href="emvs.php?action=rabbis">';
                                        echo '<h1 style="color:#fff">';
                                        echo '<center>' . $colldata1['Id'] . '</center>';
                                        echo '</h1>';
                                        echo '</a>';

                                        ?>
                                    </div>
                                    <div class="clearfix mt20">
                                        <div class="pull-left">
                                            <h5 class="md-title nomargin"></h5>
                                            <h4 class="nomargin"></h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php }
                    if ($roles == 'Administrator') { ?>
                        <div class="col-md-4">
                            <div class="panel panel-pink noborder">
                                <div class="panel-heading noborder">
                                    <div class="media-body">
                                        <?php
                                        echo '<a href="emvs.php?action=drivers">';
                                        echo '<h5 style="color:#fff"><center>Drivers</center></h5>';
                                        echo '</a>';
                                        $collquery1 = $dbh->prepare("SELECT COUNT(Sites) AS Sites FROM drivers WHERE drivers.Delete !=1");
                                        $collquery1->execute();
                                        $colldata1 = $collquery1->fetch();
                                        echo '<a href="emvs.php?action=drivers">';
                                        echo '<h1 style="color:#fff"><center>' . $colldata1['Sites'] . '</center></h1>';
                                        echo '</a>';
                                        ?>
                                    </div>
                                    <!-- media-body -->
                                    <div class="clearfix mt20">
                                        <div class="pull-left">
                                            <h5 class="md-title nomargin"></h5>
                                            <h4 class="nomargin"></h4>
                                        </div>
                                    </div>
                                </div>
                                <!-- panel-body -->
                            </div>
                            <!-- panel -->
                        </div>
                    <?php }
                    if ($roles == 'Administrator') { ?>
                        <!-- col-md-4 -->
                        <div class="col-md-4">
                            <div class="panel panel-orange noborder">
                                <div class="panel-heading noborder">
                                    <div class="media-body">
                                        <?php
                                        echo '<a href="emvs.php?action=hosts">';
                                        echo '<h5 style="color:#fff">';
                                        echo '<center>Hosts</center>';
                                        echo '</h5>';
                                        echo '</a>';
                                        $collquery1 = $dbh->prepare("select count(Sites) as Sites from hosts");
                                        $collquery1->execute();
                                        $colldata1 = $collquery1->fetch();
                                        echo '<a href="emvs.php?action=hosts">';
                                        echo '<h1 style="color:#fff">';
                                        echo '<center>' . $colldata1['Sites'] . '</center>';
                                        echo '</h1>';
                                        echo '</a>';
                                        ?>
                                    </div>
                                    <!-- media-body -->
                                    <div class="clearfix mt20">
                                        <div class="pull-left">
                                            <h5 class="md-title nomargin"></h5>
                                            <h4 class="nomargin"></h4>
                                        </div>
                                    </div>
                                </div>
                                <!-- panel-body -->
                            </div>
                            <!-- panel -->
                        </div>
                    <?php }
                    if ($roles == 'Administrator') { ?>
                        <!-- col-md-4 -->
                        <div class="col-md-4">
                            <div class="panel panel-red noborder">
                                <div class="panel-heading noborder">
                                    <div class="media-body">
                                        <?php
                                        echo '<a href="emvs.php?action=sites">';
                                        echo '<h5 style="color:#fff">';
                                        echo '<center>Sites</center>';
                                        echo '</h5>';
                                        echo '</a>';
                                        #echo "select count(Id) as Id from sites where active='1'";
                                        $collquery1 = $dbh->prepare("select count(Id) as Id from sites where active='1' AND sites.Delete != 1");
                                        $collquery1->execute();
                                        $colldata1 = $collquery1->fetch();
                                        echo '<a href="emvs.php?action=sites">';
                                        echo '<h1 style="color:#fff">';
                                        echo '<center>' . $colldata1['Id'] . '</center>';
                                        echo '</h1>';
                                        echo '</a>';
                                        ?>
                                    </div>
                                    <!-- media-body -->
                                    <div class="clearfix mt20">
                                        <div class="pull-left">
                                            <h5 class="md-title nomargin"></h5>
                                            <h4 class="nomargin"></h4>
                                        </div>
                                    </div>
                                </div>
                                <!-- panel-body -->
                            </div>
                            <!-- panel -->
                        </div>
                    <?php }
                    if ($roles == 'Administrator') { ?>
                        <!-- col-md-4 -->
                        <div class="col-md-4">
                            <div class="panel-primary noborder">
                                <div class="panel-heading noborder">
                                    <div class="media-body">
                                        <?php
                                        echo '<a href="emvs.php?action=Administrator">';
                                        echo '<h5 style="color:#fff">';
                                        echo '<center>Admin</center>';
                                        echo '</h5>';
                                        echo '</a>';
                                        #echo "select count(Id) as Id from sites where active='1'";
                                        $sqlquery1 = $dbh->prepare("select count(Id)as Id from  newmember where `Delete`=0 AND `roles`='Administrator'");
                                        $sqlquery1->execute();
                                        $sqldata1 = $sqlquery1->fetch();
                                        echo '<a href="emvs.php?action=Administrator">';
                                        echo '<h1 style="color:#fff">';
                                        echo '<center>' . $sqldata1['Id'] . '</center>';
                                        echo '</h1>';
                                        echo '</a>';
                                        ?>
                                    </div>
                                    <!-- media-body -->
                                    <div class="clearfix mt20">
                                        <div class="pull-left">
                                            <h5 class="md-title nomargin"></h5>
                                            <h4 class="nomargin"></h4>
                                        </div>
                                    </div>
                                </div>
                                <!-- panel-body -->
                            </div>
                            <!-- panel -->
                        </div>

                    <?php } ?>
                </div>
                <br/>
            </div>
            <!-- contentpanel -->
        </div>
        <!-- mainpanel -->
    </div>
    <!-- mainwrapper -->
</section>
<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/jquery-migrate-1.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/modernizr.min.js"></script>
<script src="js/ppace.min.js"></script>

<script src="js/jquery.cookies.js"></script>
<script src="js/jquery.dataTables.min.js"></script>
<script src="js/dataTables.bootstrap.js"></script>
<script src="js/dataTables.responsive.js"></script>
<script src="js/select2.min.js"></script>
<script src="js/custom.js"></script>

</body>
</html>
