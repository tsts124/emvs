<script>

    var idcountClicks = 0;
    var flag = false;

    //Identification details
    function identChangeCountry(country, id) {
        var countrySelect = undefined;
        countrySelect = $("#" + id);
        //console.log(id + 'two');
        if (countrySelect) {
            var newVal = countrySelect.val() + country;
            countrySelect.val(newVal);
            countrySelect.focus();
            countrySelect.trigger('change');
            $("[name='Collcountry[]']").trigger('change');
        }
    }

    function identCountryListener(id) {
        var btn_isr = $('#ident_isr');
        var btn_usa = $('#ident_usa');
        var btn_uk = $('#ident_uk');
        //console.log(id + 'one');
        btn_isr.off('click');
        btn_usa.off('click');
        btn_uk.off('click');
        btn_isr.on('click', function () {
            identChangeCountry(' ISRAEL', id)
        });
        btn_usa.on('click', function () {
            identChangeCountry(' USA', id)
        });
        btn_uk.on('click', function () {
            identChangeCountry(' UNITED KINGDOM', id)
        });

    }

    function removeIdentity(a, pageName) {
        if (pageName == '?action=editvisit1' || pageName == '?action=visitsadd1') {
            if (confirm("Do you really wish to delete this record?")) {
                var visitid = $('#the-visit-id').val();
                var dataString = 'id=' + a +
                    '&visitid=' + visitid +
                    '&type=identification&thisTable=visit_details';
                $.ajax({
                    type: "POST",
                    url: "ajax_delete.php",
                    data: dataString,
                    success: function (msg) {
                        if (msg == 'true') {
                            $("div[data-id='" + a + "']").css('display', 'none');
                        } else {
                            alert('At least one row of Identification Details is required');
                        }
                    }
                });
            }
        } else {
            $('[data-id =' + a + ']').css('display', 'none');
        }
    }

    function myselectId(val, rowno) {

        document.getElementById("identificationcountry" + rowno).disabled = false;
    }
    function myselectId1(val, rowno) {
        //alert("ssnid"+rowno);
        //document.getElementById("ssnid" + rowno).disabled = false;
    }
    //Check SSN Number Value set
    function Chkval() {
        var pno = document.getElementsByName("Identno[]");
        for (var i = 0; i < pno.length; i++) {
            var id = document.getElementById("ssn" + i);
            var val = id.value;

            if (val.length < 11) {
                //document.getElementById("ssnerror" + i).style.visibility = "visible";
                return false;
            } else {
                //document.getElementById("ssnerror" + i).style.visibility = "hidden";
            }
        }
    }

    // check whether new row can be added or not
    function ChkvalId() {
        helloidenti1();
        var duperrdispaly = 0; //document.getElementById('duperrordispaly').style.visibility;

        if (duperrdispaly == 'visible') {

        } else {
            //helloidenti1();
            // validate current form state
            if (validateFormID() == true) {
                //if validation goes successful print out new row from pattern file
                //helloidenti1();
            }
        }
    }
    //Add Identification Number using AJAX Page
    function helloidenti() {

        var getval = idcountClicks; ///document.getElementById('identiid').value
        var addval = 1;
        var idcountClicksval = (parseInt(getval) + addval);
        //document.getElementById('identiid').value = idcountClicksval;
        var dataString = 'idcountClicks=' + idcountClicksval + '&abc=' + window.location.search;
        $.ajax({
            type: "POST",
            url: "ajax_addmore_memberidenti.php",
            data: dataString,
            success: function (msg) {
                var strVal = msg.split("$$$$$");
                var strDiv = document.getElementById("addmore1").outerHTML;
                document.getElementById("addmore1").outerHTML = msg + strDiv;
                idcountClicks++;
            }
        });
    }
    function helloidenti1() {

        var getval = idcountClicks;
        var addval = 1;
        var idcountClicksval = (parseInt(getval) + addval);
        var id = $("input[name='memberid']").val();
        var visitid = $('#the-visit-id').val();

        var dataString = 'idcountClicks=' + idcountClicksval +
            '&abc=' + window.location.search +
            '&collid=' + id +
            '&visitid=' + visitid;
        $.ajax({
            type: "POST",
            url: "ajax_addmore_memberidenti1.php",
            data: dataString,
            success: function (msg) {
                var strVal = msg.split("$$$$$");
                var strDiv = document.getElementById("addmore1").outerHTML;
                document.getElementById("addmore1").outerHTML = msg + strDiv;
                idcountClicks++;

                //setting passport name label
                $("select[name='type']").each(function (index, element) {
                    $(this).on('change', function () {
                        if ($(this).val() == 'Passport') {
                            $('#passport-name-label').html('Passport');
                        }
                    });
                });
            }
        });
    }
    function DuplicateIdtype(val, count) {
        return true;
    }
    function duplication(val, lang) {
        /*var collctorid = document.getElementById("collctorid").value;
         var visitid = document.getElementById("visitid").value;
         var dataString = 'value=' + val + '&lang=' + lang + '&collctorid=' + collctorid + '&visitid=' + visitid;
         $.ajax({
         type: "POST",
         url: "duplication.php",
         data: dataString,
         success: function (msg) {
         $('#duperr').html(msg);
         var vists = document.getElementById("duperrordispaly").style.visibility;
         //alert("visits :"+vists);
         if (vists == 'visible') {
         var idname = document.getElementById("Identnum" + lang).value;
         if (idname == 'Teudat Zeut') {
         document.getElementById("ssnid" + lang).disabled = true;
         } else if (idname == 'SSN ID') {
         document.getElementById("ssn" + lang).disabled = true;
         }
         document.getElementById("submitchk").disabled = true;
         document.getElementById('upsubmitchk').disabled = true;
         document.getElementById("Cntsubmitchk").disabled = true;
         } else {
         document.getElementById("ssn" + lang).disabled = false;
         document.getElementById("ssnid" + lang).disabled = false;
         document.getElementById("submitchk").disabled = false;
         document.getElementById('upsubmitchk').disabled = false;
         document.getElementById("Cntsubmitchk").disabled = false;
         }
         }
         });*/
    }
    function cntryduplication(cntryval, cntrylang) {
        var collctorid = document.getElementById("collctorid").value;
        var idname = document.getElementById("Identnum" + cntrylang).value;
        var dataString = 'cntryval=' + cntryval + '&cntrylang=' + cntrylang + '&value=' + idname + '&collctorid=' + collctorid;
        $.ajax({
            type: "POST",
            url: "duplication.php",
            data: dataString,
            success: function (msg) {
                $('#duperr').html(msg);
                var vists = document.getElementById("duperrordispaly").style.visibility;
                if (vists == 'visible') {
                    document.getElementById("ssnid" + cntrylang).disabled = true;
                    document.getElementById("submitchk").disabled = true;
                    document.getElementById('upsubmitchk').disabled = true;
                    document.getElementById("Cntsubmitchk").disabled = true;
                } else {
                    document.getElementById("ssnid" + cntrylang).disabled = false;
                    document.getElementById("submitchk").disabled = false;
                    document.getElementById('upsubmitchk').disabled = false;
                    document.getElementById("Cntsubmitchk").disabled = true;
                }
            }
        });
    }


    function IdValidation(count, val) {

        if (count > 0) {
            var valcount = 0;
            for (var i = count; i > 0; i--) {

                var Idtype = document.getElementById("Identnum" + count).value;
                var phone = document.getElementById("Identnum" + (i - 1)).value;
                var Idcoun = document.getElementById("identificationcountry" + (i - 1)).value;
                var Idnum = document.getElementsByName("Identno[]")[i].value;
                if (Idtype == phone && val == Idcoun) {
                    valcount++;
                }
            }
        }
        if (valcount > 0) {

            document.getElementById('submitchk').disabled = true;
            document.getElementById('upsubmitchk').disabled = true;

        } else {
            //document.getElementById("contryerror" + count).style.visibility = "hidden";
            //document.getElementById("ssnid" + count).disabled = false;
            //document.getElementById('submitchk').disabled = false;
            //document.getElementById('upsubmitchk').disabled = false;
        }
    }
    /////// Duplicate Id
    function ValidPhone(count, lang) {
        var Idtype = document.getElementById('Identnum' + lang).value;
        var Idcontry = document.getElementById('identificationcountry' + lang).value;
        var dataString = 'IdValDup=' + count + '&Idtype=' + Idtype + '&Idcontry=' + Idcontry;
        //alert(dataString);
        $.ajax({
            type: "POST",
            url: "ajax_driver_number.php",
            data: dataString,
            success: function (msg) {
                $('#duperr').html(msg);
                var hidval = document.getElementById('duperrordispaly').style.visibility;
                if (hidval == 'hidden') {
                    //document.getElementById("duperr" + lang).style.visibility = "hidden";
                    document.getElementById('submitchk').disabled = false;
                    document.getElementById('upsubmitchk').disabled = false;
                    document.getElementById('Cntsubmitchk').disabled = false;
                    return true;
                }
                else {
                    //document.getElementById("duperr" + lang).style.visibility = "visible";
                    document.getElementById('submitchk').disabled = true;
                    document.getElementById('upsubmitchk').disabled = true;
                    document.getElementById('Cntsubmitchk').disabled = true;
                    $(this).html(msg);
                    return false;
                }
            }
        });
    }

    //Remove Identification Number using AJAX Page
    function RemoveId(a) {

        var Rmvedata = document.getElementById("Identnum" + a).value;

        var newItemsCount = $("div[name='Idremoveval[]']").length;

        $("#Idremoveval" + a).remove();

        var countItemsToAdd = $("select[name='Identnum[]']").length;

        if (countItemsToAdd == 0) {
            //helloidenti();
        }
    }

    function validateFormID() {

        var flag = false;
        var valididflag = true;

        var Idtype = document.getElementsByName("Collidenti");
        var cnt = Idtype.length - 1;

        $("select[name='Collcountry']").each(function (index) {


            var cn = $(this).attr("id").slice(8);
            var Idtypeval = document.getElementById("Identnum" + cn).value;
            var Idcountry = document.getElementById("identificationcountry" + cn).value;
            //var IdNum = document.getElementsByName("Identno[]")[cn].value;

            if ((Idtypeval == '') || (Idcountry == '')) { // ||(IdNum=='')
                document.getElementById("iderror" + cn).style.visibility = "visible";
                return false;
            } else {
                if (Idtypeval != 'SSN ID') {
                    document.getElementById("iderror" + cn).style.visibility = "hidden";
                    flag = true;
                } else if (Idtypeval == 'SSN ID') {
                    var id = document.getElementById("ssn" + cn);
                    var val = id.value;
                    if (val.length == 11) {
                        document.getElementById("iderror" + cn).style.visibility = "hidden";
                        flag = true;
                    } else {

                        flag = false;
                    }
                }
            }

        });
        if ($("div[name='Collidenti']").length == 0) {
            flag = true;
        }

        if (flag == true) {
            return true;
        }
        else {
            document.getElementById('submitchk').disabled = true;
            document.getElementById('upsubmitchk').disabled = true;
            document.getElementById('Cntsubmitchk').disabled = true;
            return false;
        }
    }
</script>
<div id="addmore1" class="col-sm-12 field" style="text-align:center;">
</div>
