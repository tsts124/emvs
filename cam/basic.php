<!doctype html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>WebcamJS Test Page</title>
	<script language="JavaScript">
		function take_snapshot() {
			// take snapshot and get image data
			Webcam.snap(function(data_uri) {
				// display results in page
				document.getElementById('imgsrc').value = data_uri;
				document.getElementById('results').innerHTML = 
				'<img src="'+data_uri+'"/>';
					
				var fd = new FormData(document.forms["basicForm"]);
                var xhr = new XMLHttpRequest();
                xhr.open('POST', 'uploadphoto.php', true);
 
                xhr.upload.onprogress = function(e) {
                    if (e.lengthComputable) {
                        var percentComplete = (e.loaded / e.total) * 100;
						console.log(percentComplete + '% uploaded');
                        alert('Succesfully uploaded');
                    }
                };
				xhr.onload = function() {
                 };
                xhr.send(fd);
			});
		}
	</script>
</head>
<body>
	<form id="basicForm" name="basicForm" method="post" enctype="multipart/form-data">
	<div id="results">Your captured image will appear here...</div>
	<input name="imgsrc" id="imgsrc" type="text">
	<div id="my_camera"></div>
	
	<!-- First, include the Webcam.js JavaScript Library -->
	<script type="text/javascript" src="webcam.js"></script>
	
	<!-- Configure a few settings and attach camera -->
	<script language="JavaScript">
		Webcam.set({
			width: 320,
			height: 240,
			image_format: 'jpg',
			jpeg_quality: 90
		});
		Webcam.attach( '#my_camera' );
	</script>
	<input type="button" value="Take Snapshot" onClick="take_snapshot()">
	</form>
	
	
	
</body>
</html>
