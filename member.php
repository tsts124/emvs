<?php
include 'includes/header.php';
include  __DIR__ . '/includes/dbcon.php';
//TODO UNCOMMENT FOR COMMITING TO THE TEST SERVER
include 'config.php';

if ($_SESSION['user'] == '') {

    header('Location: emvs.php?action=index');
}

$required_fields_query = $dbh->prepare("select * from required_fields where deleted='0'");
$required_fields_query->execute();
$required_fields = $required_fields_query->fetchAll();

/* ----------------------------------------- */
$query = $dbh->prepare(
    "SELECT
	  *
	  FROM
	  `app_options`
	  WHERE
	  `app_options`.`key` = 'identification_details_required'
	  "
);
$query->execute();
$requiredIdentDetails = $query->fetch();

$query = $dbh->prepare(
    "
	  SELECT
	  *
	  FROM
	  `app_options`
	  WHERE
	  `app_options`.`key` = 'contact_details_required'
	  "
);
$query->execute();
$requiredContactDetails = $query->fetch();
/* ------------------------------------------ */

function findAssignment($requiredFields, $tab, $assignment)
{

    $isRequired = false;
    foreach ($requiredFields as $row) {
        if ($row['tab'] == $tab && $row['assignment'] == $assignment) {
            if ($row['active'] == 1)
                $isRequired = true;
            break;
        }
    }
    return $isRequired;
}

$abc = $_GET['abc'];
$a = $_GET['a'];
$user = $_SESSION['user'];
$sql = $dbh->prepare('select *
                      from `collectors`
                      where `Id` = :id ;');
$sql->execute(['id' => $abc]);
$dataquery = $sql->fetch();

/************* Variable declarations*************/
$title = $_POST['title'];
$firstname = addslashes(trim(ucwords($_POST['firstname'])));
$middlename = addslashes(trim(ucwords($_POST['middlename'])));
$lastname = addslashes(trim(ucwords($_POST['lastname'])));
//$gender = $_POST['gender'];
$address1 = addslashes(trim(ucwords($_POST['address1'])));
$address2 = addslashes(trim(ucwords($_POST['address2'])));
$city = trim(ucwords($_POST['city']));
$state = addslashes(trim(ucwords($_POST['statecountry'])));
$zipcode = addslashes(trim($_POST['zipcode']));
$country = addslashes(trim(ucwords($_POST['country'])));
$personalcomments = addslashes(trim($_POST['personalcomments']));
$alertcheck = addslashes(trim($_POST['alerts']));

$birthMonth = $_POST['month'];
$birthDay = $_POST['day'];
$birthYear = $_POST['year'];
$dob = '';

$dob = createDatefromDB($birthYear, $birthDay, $birthMonth);

if (empty($alertcheck)) {
    $alertcheck = 0;
}

if ((isset($_POST['next1'])) || (isset($_POST['newnext1'])) || (isset($_POST['newnext1new']))) {

    $memberphone = $_POST['memberphone'];
    if ($personalcomments == '') {
        $alertcheck = 0;
    }

    if ($memberphone == '') {

        $collid = ($_POST['memberids']);

        $count = $dbh->prepare('INSERT INTO `collectors` (`Id`,
                                                          `user`,
                                                          `title`,
                                                          `firstname`,
                                                          `lastname`,
                                                          `city`,
                                                          `statecountry`,
                                                          `country`,
                                                          `zipcode`,
                                                          `address1`,
                                                          `address2`,
                                                          `personalcomments`,
                                                          `dob`,
                                                          `Addeddate`,
                                                          `alertcheck`) 
					         VALUES (:id,
					                 :userid,
					                 :title,
					                 :firstname,
					                 :lastname,
					                 :city,
					                 :state,
					                 :country,
					                 :zip,
					                 :addr1,
					                 :addr2,
					                 :personalcomments,
					                 :dob,
					                 now(),
					                 :alertcheck) ; ');
        $count->execute([
            'id' => $collid,
            'userid' => $user,
            'title' => $title,
            'firstname' => $firstname,
            'lastname' => $lastname,
            'city' => $city,
            'state' => $state,
            'country' => $country,
            'zip' => $zipcode,
            'addr1' => $address1,
            'addr2' => $address2,
            'personalcomments' => $personalcomments,
            'dob' => $dob,
            'alertcheck' => $alertcheck
        ]);

        $sql = $dbh->prepare("
				select * from collectors order by Id desc
			");
        $sql->execute();
        $data = $sql->fetch();
        $id = $data['Id'];


        $upidenti = count($_POST['Phone_Number']);
        $phone_primary1 = $_POST['CPrimary'];
        for ($i = 0; $i < $upidenti; $i++) {
            $primary_phone_count = $_POST['primary_phone_count_new'][$i];
            $Phonenum1 = $_POST['Phone_Type'][$i];
            $phoneno1 = addslashes(trim($_POST['Phone_Number'][$i]));
            $countrycode1 = addslashes(trim($_POST['Phone_Country'][$i]));
            if ($phoneno1) {
                $count = $dbh->exec("INSERT INTO collectorphone(collid,PrimaryCount,Phone_Type,Phone_Country,Phone_Number)
                                                VALUES ( '$id','$primary_phone_count','$Phonenum1','$countrycode1','$phoneno1')");
                if ($phone_primary1) {
                    $updateval = $dbh->prepare("update `collectorphone` set CPrimary='' where `collid`=$id");
                    $updateval->execute();
                    $dval = $dbh->prepare("UPDATE `collectorphone` SET `CPrimary`='$id' WHERE `collid`='$id' AND `PrimaryCount`='$phone_primary1';");
                    $dval->execute();
                }
            }
        }

        $Identnum = count($_POST['Collphone']);
        $primary_contact = $_POST['Primary1'];
        $countPassNames = 0;
        for ($i = 0; $i < $Identnum; $i++) {
            $primary_count = $_POST['primary_count'][$i];
            $collidenty = $_POST['Collidenti'][$i];
            $collphone = addslashes($_POST['Collphone'][$i]);
            $refdetails = $_POST['Refdetails'][$i];
            $collcountry = addslashes(trim($_POST['Collcountry'][$i]));
            $attachment = $_FILES['scandocument']['tmp_name'][$i];
            if ($collidenty == 'Passport') {
                $passName = htmlspecialchars($_POST['passport_name'][$countPassNames]);
                $countPassNames++;
            } else {
                $passName = '';
            }
            $cjj = 0;
            $imagename = $img;
            if ($collphone) {
                $Inssql = $dbh->prepare("INSERT INTO collident(Collid,PrimaryCount,Collidenti,Collcountry,
                                                               Collphone,Countcolldoc,Colldoc,Refdetails,
                                                               Addeddate, passport_name)
                          VALUES ('$collid','$primary_count','$collidenty','$collcountry','$collphone',
                                  '$cjj','$imagename','$refdetails',now(), '$passName')");
                $Inssql->execute();

                if ($primary_contact) {
                    $updateval = $dbh->prepare("update `collident` set Primary1='' where `Collid`=$collid");
                    $updateval->execute();
                    $dval = $dbh->prepare("UPDATE `collident` SET `Primary1`='$collid' WHERE `Collid`='$collid' AND `PrimaryCount`='$primary_contact';");
                    $dval->execute();
                }
            }
        }

//        if ($_POST['newnext1new']) {
//            $sql = $dbh->prepare("SELECT * FROM `collectors` WHERE `Id` = '$collid'");
//            $sql->execute();
//            if ($sql->fetch()) {
//                echo '<meta http-equiv="refresh" content="0; URL=emvs.php?action=visitsadd1&&a=' . $collid . '" />';
//            } else {
//                echo '<meta http-equiv="refresh" content="0; URL=emvs.php?action=visitsadd1&&a=1" />';
//            }
//
//        } else {
//            echo '<meta http-equiv="refresh" content="0; URL=emvs.php?action=collector" />';
//        }

    } else {
        $error = '<div class="alert alert-danger">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<strong>Oh snap!</strong> Phone number Already Avaliable!.
									</div>';
        if ($_POST['newnext1']) {
            echo '<meta http-equiv="refresh" content="0; URL=emvs.php?action=novisit&&no=' . $collid . '" />';
        }
        if ($_POST['newnext1new']) {
            echo '<meta http-equiv="refresh" content="0; URL=emvs.php?action=novisit&&no=' . $collid . '" />';
        }
        echo '<meta http-equiv="refresh" content="0; URL=emvs.php?action=collector" />';
    }
}

if ((isset($_POST['update'])) || (isset($_POST['newupdate']))) {

    $sql = $dbh->prepare("UPDATE `collectors`
                          SET `user` = :userid,
                              `title` = :title,
                              `firstname` = :firstname,
                              `lastname` = :lastname,
                              `city` = :city,
                              `statecountry` = :state,
                              `country` = :country,
                              `zipcode` = :zip,
                              `address1` = :addr1,
                              `address2` = :addr2,
                              `personalcomments` = :personalcomments,
                              `dob` = :dob,
                              `Updateddate` = now(),
                              `alertcheck` = :alertcheck
                               WHERE `Id` = :id ");
    $sql->execute([
        'id' => $abc,
        'userid' => $user,
        'title' => $title,
        'firstname' => $firstname,
        'lastname' => $lastname,
        'city' => $city,
        'state' => $state,
        'country' => $country,
        'zip' => $zipcode,
        'addr1' => $address1,
        'addr2' => $address2,
        'personalcomments' => $personalcomments,
        'dob' => $dob,
        'alertcheck' => $alertcheck
    ]);
    $upidenti = count($_POST['Phone_Number']);

    $phone_primary1 = $_POST['CPrimary'];
    $primary_phone_count = $_POST['primary_phone_count'][0];

    for ($i = 0; $i < $upidenti; $i++) {
        $Phonenum1 = $_POST['Phone_Type'][$i];
        $phoneno1 = addslashes($_POST['Phone_Number'][$i]);
        $countrycode1 = $_POST['Phone_Country'][$i];
        if ($phoneno1) {
            $updateval = $dbh->prepare("update `collectorphone` set PrimaryCount='' where `collid`=$abc");
            $updateval->execute();
            $count = $dbh->exec("INSERT INTO collectorphone(collid,PrimaryCount,Phone_Type,Phone_Country,Phone_Number)
                                                VALUES ('$abc','$primary_phone_count','$Phonenum1','$countrycode1','$phoneno1')");
            $phsql = $dbh->prepare("select * from collectorphone order by Id desc");
            $phsql->execute();
            $phdata = $phsql->fetch();
            $phdataval = $phdata['collid'];
            if ($phone_primary1 != '') {
                $dval = $dbh->prepare("update collectorphone set CPrimary=$phone_primary1 where `collid`=$phdataval AND `PrimaryCount`=$phone_primary1");
                $dval->execute();
            }
        }
    }

    $editedNumbersCount = count($_POST['phoneno1']);
    for ($i = 0; $i < $editedNumbersCount; $i++) {
        $type = $_POST['Phonenum1'][$i];
        $country = addslashes(trim($_POST['countrycode1'][$i]));
        $phoneNum = addslashes($_POST['phoneno1'][$i]);
        $contactDetailId = $_POST['contactDetailId'][$i];
        $phoneNumber = $phoneNum;
        $sql = $dbh->prepare("UPDATE `collectorphone` SET `Phone_Number` = '$phoneNumber', `Phone_Country` = '$country', `Phone_Type` = '$type'
                                     WHERE `Id` = $contactDetailId AND `delete` = 0 AND `collid` = $abc;");
        $sql->execute();
    }

    $Identnum = count($_POST['Collphone']);
    $primary_contact = $_POST['Primary1'];
    $countNewPassNames = 0;
    for ($i = 0; $i < $Identnum; $i++) {
        $primary_count = $_POST['primary_count'][$i];
        $collidenty = $_POST['Collidenti'][$i];
        $collphone = addslashes($_POST['Collphone'][$i]);
        $refdetails = $_POST['Refdetails'][$i];
        $collcountry = addslashes(trim($_POST['Collcountry'][$i]));
        if ($collidenty == 'Passport') {
            $passName = htmlspecialchars($_POST['passport_name'][$countNewPassNames]);
            $countNewPassNames++;
        } else {
            $passName = '';
        }

        $cjj = 0;
        $imagename = $img;
        if ($collphone) {
            $Inssql = $dbh->prepare("INSERT INTO collident(Collid,PrimaryCount,Collidenti,Collcountry,
                                                           Collphone,Countcolldoc,Colldoc,Refdetails,
                                                           Addeddate, passport_name)
                          VALUES ('$abc','$primary_count','$collidenty','$collcountry','$collphone',
                                  '$cjj','$imagename','$refdetails',now(), '$passName')");
            $Inssql->execute();

            if ($primary_contact && $primary_contact == $primary_count) {
                $updateval = $dbh->prepare("update `collident` set Primary1='' where `Collid`=$abc");
                $updateval->execute();
                $dval = $dbh->prepare("UPDATE `collident` SET `Primary1`='$abc' WHERE `Collid`='$abc' AND `PrimaryCount`='$primary_contact';");
                $dval->execute();
            }
        }
    }

    $oldIdent = count($_POST['Identno1']);
    $countOldPassNames = 0;
    for ($i = 0; $i < $oldIdent; $i++) {
        $primary_count = $_POST['update_val'][$i];
        $collidenty = $_POST['Identnum1'][$i];
        $collphone = addslashes($_POST['Identno1'][$i]);
        $refdetails = $_POST['Refdetails'][$i];
        $collcountry = $_POST['identificationcountry1'][$i];
        if ($collidenty == 'Passport') {
            $passName = htmlspecialchars($_POST['old-passport-name'][$countOldPassNames]);
            $countOldPassNames++;
        } else {
            $passName = 0;
        }

        $sql = $dbh->prepare('UPDATE `collident`
                              SET `passport_name` = :passname,
                                  `Collidenti` = :collident,
                                  `Collcountry` = :country,
                                  `Collphone` = :phone
                              WHERE `Id` = :id ');
        $sql->execute([
            'passname' => $passName,
            'collident' => $collidenty,
            'country' => $collcountry,
            'phone' => $collphone,
            'id' => $primary_count
        ]);
        if ($primary_contact && $primary_contact == $primary_count) {
            $updateval = $dbh->prepare("update `collident` set Primary1='' where `Collid`=$abc");
            $updateval->execute();
            $dval = $dbh->prepare("UPDATE `collident`
                                   SET `Primary1`='$abc'
                                   WHERE `Collid`='$abc'
                                   AND `Id` = '$primary_count';");
            $dval->execute();
        }
    }

    if (isset($_POST['action']) && ($_POST['action'] == "collphoneupdate")) {
        $upquery1 = $dbh->prepare("update collectorphone set CPrimary=' ' where Collid='" . $_POST['act'] . "'");
        $upquery1->execute();
        $upquery = $dbh->prepare("update collectorphone set CPrimary='" . $_POST['act'] . "' where Id='" . $_POST['hid'] . "'");
        $upquery->execute();
    }

    if ($_POST['newupdate']) {
        echo '<meta http-equiv="refresh" content="0; URL=emvs.php?action=visitsadd1&&a=' . $abc . '" />';
    } else {
        echo '<meta http-equiv="refresh" content="0; URL=emvs.php?action=collector" />';
    }
}

if (isset($_POST['action']) && ($_POST['action'] == "deleteid")) {
    $selsql = $dbh->prepare("select * from collident where Id='" . $_POST['act'] . "'");
    $selsql->execute();
    $deldata = $selsql->fetch();
    if ($_POST['act'] < $deldata['Primary1']) {
        $upquery = $dbh->prepare("update collident set Primary1='" . $_POST['act'] . "' where Id='" . $_POST['hid'] . "'");
        $upquery->execute();
    }

    $delquery1 = $dbh->prepare("update collident set `delete`= 1 where Id='" . $_POST['act'] . "'");
    $delquery1->execute();

}

if (isset($_POST['action']) && ($_POST['action'] == "deletephone")) {
    $selsql = $dbh->prepare("select * from collectorphone where Id='" . $_POST['act'] . "'");
    $selsql->execute();
    $deldata = $selsql->fetch();
    $delquery1 = $dbh->prepare("update collectorphone set `delete`= 1 where Id='" . $_POST['act'] . "'");
    $delquery1->execute();
}

$sql = $dbh->prepare("SELECT *
                      FROM `visitstable`
                      WHERE `collectorsid` = :id
                      ORDER BY `visitid`
                      DESC; ");
$sql->execute([':id' => $abc]);
$lastVisitData = $sql->fetch();

$lastVisit = date_create($lastVisitData['authofromdate']);
$lastVisit = date_format($lastVisit, 'm/d/y');

if (strlen($dataquery['dob']) > 4) {
    $parts = explode('-', $dataquery['dob']);
    $year = $parts[2] ? $parts[2] : $parts[1];

    if (preg_match('/[a-zA-Z]/', $parts[0])) {
        $date = date_create($dataquery['dob']);
        $date = date_format($date, 'm/d/y');
        $newParts = explode('/', $date);

        $date = $newParts[0] . '/' . $newParts[1] . '/' . $year[2] . $year[3];
    } else {
        if (strlen($parts[0]) == 1) {
            $parts[0] = '0' . $parts[0];
        }

        if (strlen($parts[1]) == 1) {
            $parts[1] = '0' . $parts[1];
        }

        $date = $parts[0] . '/' . $parts[1] . '/' . $year[2] . $year[3];
    }

} else {
    $date = $dataquery['dob'];
}


$pagename = $dataquery['title'] . ' ' .
    $dataquery['firstname'] . ' ' .
    $dataquery['lastname'] .
    '&nbsp;&nbsp; Birthdate: ' .
    $date . '&nbsp;&nbsp; Last Visit: ' .
    $lastVisit;

?>

<!-- popup window-->
<script type="text/javascript" src="js/slimbox2.js"></script>
<link rel="stylesheet" href="css/slimbox2.css" type="text/css" media="screen"/>
<!-- Autocomplete-->
<script src="js/jquery-1.11.1.min.js"></script>
<!-- Date Picker -->
<link href="datepicker/jquery-ui.css" rel="Stylesheet" type="text/css"/>
<script type="text/javascript" src="datepicker/jquery-ui.js"></script>
<script language="javascript">

    function checkPhoneDup(object) {
        var data = 'number=' + object.value;
        var $element = $('#' + object.id);
        $('#upsubmitchk').attr('disabled', true);
        $('#submitchk').attr('disabled', true);

        $.ajax({
            method: 'POST',
            url: 'check-phone-dup.php',
            data: data,
            success: function (msg) {
                if (msg === 'error') {
                    $element.css('border-color', 'red');
                    $element.closest('.row').find('.phonedup').css('display', 'block');
                } else {
                    $('#upsubmitchk').attr('disabled', false);
                    $('#submitchk').attr('disabled', false);

                    $element.css('border-color', '');
                    $element.closest('.row').find('.phonedup').css('display', 'none');
                }
            }
        });
    }

    $(document).ready(function () {

        function checkField(returnObject) {
            var valid = true, object = {
                'title': null,
                'firstName': null,
                'lastName': null,
                'mont': null,
                'day': null,
                'year': null
            };

            var title = $('.personal-details #title');
            if (title.val() === ' ') {
                title.attr('style', 'border-color:red;');
                valid = false;
            } else {
                object.title = title.val();
                title.attr('style', 'border-color:;');
            }

            var firstname = $('.personal-details #firstname');
            if (firstname.val() === '') {
                firstname.attr('style', 'border-color:red;');
                valid = false;
            } else {
                object.firstName = firstname.val();
                firstname.attr('style', 'border-color:;');
            }

            var lastname = $('.personal-details #lastname');
            if (lastname.val() === '') {
                lastname.attr('style', 'border-color:red;');
                valid = false;
            } else {
                object.lastName = lastname.val();
                lastname.attr('style', 'border-color:;');
            }

            var month = $('.personal-details #month');
            if (month.val() !== '0') {
                object.mont = month.val();
            }

            var day = $('.personal-details #day');
            if (day.val() !== '0') {
                object.day = day.val();
            }

            var year = $('.personal-details #year');
            if (year.val() === '0') {
                year.attr('style', 'border-color:red;');
                valid = false;
            } else {
                object.year = year.val();
                year.attr('style', 'border-color:;');
            }

            $('#addNewVisit').attr('disabled', !valid);

            return returnObject ? object : valid;
        }
        checkField();

        var memberids = "<?= $_SESSION['user'] ?>" , isInsert = 0;
        $(document).on('keyup, change', '.personal-details #month, .personal-details #day, .personal-details #year, .personal-details #title, .personal-details #lastname , .personal-details #firstname', function () {
            if (checkField()) {
                var dataString = checkField(true);
                dataString.requiredFields = true;
                dataString.memberids = $('[name=memberids]').val();
                dataString.user = memberids;
                dataString.city = $('#city').val();
                dataString.country = $('#country').val();

                $.ajax({
                    type: "POST",
                    url: "ajax_add_collector.php",
                    data: dataString,
                    success: function (msg) {
                        isInsert = msg;
                        $('.bb-alert').css('display', 'block');
                        $('#msg').text(msg);
                        setTimeout(function () {
                            $('.bb-alert').css('display', 'none');
                        },2000);
                    }
                });
            }
        });
        $('.personal-details #year').trigger('change');
        $('#addNewVisit').click(function (e) {
            e.preventDefault();
            if (checkField()){
                window.location = 'emvs.php?action=visitsadd1&&a=' + $('[name=memberids]').val();
            }
        });

        $(document).on('change','#address1, #address2, #city, #statecountry, #country, #zipcode, #alerts, #personalcomments', function () {
            if(checkField() || isInsert){
                var dataString = {};
                dataString.field = $(this).attr('name') === 'alerts' ? 'alertcheck' : $(this).attr('name');
                dataString.value = $(this).attr('name') === 'alerts' ? + $(this).is(':checked') : $(this).val();
                dataString.updateField = true;
                dataString.memberids = $('[name=memberids]').val();

                $.ajax({
                    type: "POST",
                    url: "ajax_add_collector.php",
                    data: dataString,
                    success: function (msg) {
                        $('.bb-alert').css('display', 'block');
                        $('#msg').text(msg);
                        setTimeout(function () {
                            $('.bb-alert').css('display', 'none');
                        },2000);
                    }
                });
            } else {
                $('.bb-alert').css('display', 'block');
                $('#msg').text('Fill in the required fields first');
                setTimeout(function () {
                    $('.bb-alert').css('display', 'none');
                },2000);
            }
        });

        $(document).on('change', "select[name='Collidenti[]'], input[name='Collcountry[]'], input[name='Collphone[]'], input[name='passport_name[]'], input[name=Primary1]", function () {
            var _this = this;

            if (checkField() || isInsert) {
                var dataString = {};
                dataString.collident = $(_this).parents('#addinput1').data('id');
                dataString.value = $(_this).val();
                dataString.field = $(_this).attr('name');
                dataString.identification = true;
                dataString.primaryCount = $('#addinput1').find('input[name=Primary1]:checked').val();
                dataString.memberids = $('[name=memberids]').val();

                $.ajax({
                    type: "POST",
                    url: "ajax_add_collector.php",
                    data: dataString,
                    success: function (msg) {
                        $('#msg').text('');
                        $('.bb-alert').css('display', 'block');
                        $('#msg').text(msg);
                        setTimeout(function () {
                            $('.bb-alert').css('display', 'none');
                        },2000);

                        if (dataString.value == 'SSN / ID Number' || dataString.value == 'Teudat Zeut') {
                            dataString.value = $(_this).parents('#addinput1').find("input[name='Collcountry[]']").val();
                            dataString.field = 'Collcountry[]';

                            $.ajax({
                                type: "POST",
                                url: "ajax_add_collector.php",
                                data: dataString,
                                success: function () {
                                }
                            });
                        }
                    }
                });
            } else {
                $('.bb-alert').css('display', 'block');
                $('#msg').text('Fill in the required fields first');
                setTimeout(function () {
                    $('.bb-alert').css('display', 'none');
                },2000);
            }
        });

        $(document).on('click', 'a[name="RemoveIdRows[]"]', (function () {
            var dataString = {};
                dataString.collident = $('a[name="RemoveIdRows[]"]').parents('#addinput1').find('#primary_count').val();
                dataString.deleteField = true;
            $.ajax({
                type: "POST",
                url: "ajax_add_collector.php",
                data: dataString,
                success: function (msg) {

                    $('.bb-alert').css('display', 'block');
                    $('#msg').text(msg);
                    setTimeout(function () {
                        $('.bb-alert').css('display', 'none');
                    },2000);
                }
            });
            })
        );

        $(document).on('change', "select[name='Phone_Type[]'], input[name='Phone_Country[]'], input[name='CPrimary']", function () {
            var _this = $(this);
            updatePhoneData(_this);
        });

        var delayTimer;
        $(document).on('keydown', 'input[name="Phone_Number[]"]', function () {
            var _this = $(this);

            clearTimeout(delayTimer);
            delayTimer = setTimeout(function () {
                if (_this.val()) {
                    checkPhoneDup(_this);
                }

                updatePhoneData(_this);
            }, 900);

        });

        function updatePhoneData(element){
            if (checkField()) {
                var dataString = {};
                dataString.collectorphoneid = element.parents('[data-id]').data('id');
                dataString.value = element.val();
                dataString.phoneTypeValue = element.parents('#addinput').find("select[name='Phone_Type[]']").val();
                dataString.field = element.attr('name');
                dataString.collectorphone = true;
                dataString.CPrimary = $(document).find('input[name=CPrimary]:checked').val();
                dataString.memberids = $('[name=memberids]').val();

                $('#addNewVisit').attr('disabled', true);
                $('.fa.add-visit-loading').css('display', 'inline-block');

                $.ajax({
                    type: "POST",
                    url: "ajax_add_collector.php",
                    data: dataString,
                    success: function (msg) {

                        $('.bb-alert').css('display', 'block');
                        $('#msg').text(msg);

                        $('#addNewVisit').attr('disabled', false);
                        $('.fa.add-visit-loading').css('display', 'none');

                        setTimeout(function () {
                            $('.bb-alert').css('display', 'none');
                        },2000);
                    }
                });
            } else {
                $('.bb-alert').css('display', 'block');
                $('#msg').text('Fill in the required fields first');
                setTimeout(function () {
                    $('.bb-alert').css('display', 'none');
                },2000);
            }
        }

        $(document).on('click', 'a[name="removerows[]"]', (function () {
                var dataString = {};
            dataString.collectorphoneid = $(this).prop('lang');
                dataString.deleteFieldPhone = true;
                $.ajax({
                    type: "POST",
                    url: "ajax_add_collector.php",
                    data: dataString
                });
            })
        );

        $('#view-alert').magnificPopup({
            type: 'inline',
            preloader: false
        });

        //Setting mask for USA numbers
        $('input[name="Phone_Country[]"]').each(function () {
            if ($.trim($(this).val()) == 'USA' || $.trim($(this).val()) == 'usa') {
                $(this).closest('div[data-id]').find('input[name="Phone_Number[]"]').simpleMask({
                    'mask': 'usa',
                    'nextInput': false
                });
            }
        });

        //setting passport name label for identification details
        $("select[name='Collidenti[]']").each(function () {
            if ($(this).val() == 'Passport') {
                $('#passport-name-label').html('Passport');
            }
        });

        $("#txtdate").datepicker({
            maxDate: 0,
            changeMonth: true,
            changeYear: true,
            yearRange: "1950:+0",

            onChangeMonthYear: function (year, month) {
                $("#startYear").val(year);
                $("#startMonth").val(month);
            }
        });

        $('select[name="Collidenti[]"]').on('change', function () {
            $(this).closest('div').parent().find('input[name="Collcountry[]"]').focus();
        });

        $('select[name="Phone_Type[]"]').on('change', function () {
            $(this).closest('div').parent().find('input[name="Phone_Country[]"]').focus();
        });
    });

    $(".ui-datepicker-month").prepend("<option value='' selected='selected'>Month</option>");

    $(".ui-datepicker-year").prepend("<option value='' selected='selected'>Year</option>");

    function newid(a, b) {
        document.getElementById('act').value = b;
        document.getElementById('action').value = "collupdate";
        document.getElementById('hid').value = a;
        //document.novalidate.submit();
    }


    function newid1(a, b) {
        document.getElementById('act').value = b;
        document.getElementById('action').value = "collphoneupdate";
        document.getElementById('hid').value = a;
        //document.novalidate.submit();
    }

</script>
<!-- Tool Tip -->
<style type="text/css">
    .override-error {
        color: #428bca;
        font-size: 9pt;
    }

    .first {
        display: none;
    }

    .second {
        display: show;
    }

    #tooltip {
        text-align: center;
        color: #fff;
        background: #111;
        position: absolute;
        z-index: 100;
        padding: 15px;
    }

    #tooltip:after /* triangle decoration */
    {
        width: 0;
        height: 0;
        border-left: 10px solid transparent;
        border-right: 10px solid transparent;
        border-top: 10px solid #111;
        content: '';
        position: absolute;
        left: 50%;
        bottom: -10px;
        margin-left: -10px;
    }

    #tooltip.top:after {
        border-top-color: transparent;
        border-bottom: 10px solid #111;
        top: -20px;
        bottom: auto;
    }

    #tooltip.left:after {
        left: 10px;
        margin: 0;
    }

    #tooltip.right:after {
        right: 10px;
        left: auto;
        margin: 0;
    }

</style>
<!--*****Ajax Avoid Duplication*****-->
<script type="text/javascript">

    function mykeyup(d) {
        //alert(d);
        var dataString = 'member=' + d;
        $.ajax({
            type: "POST",
            url: "check_member.php",
            data: dataString,
            cache: false,
            success: function (html) {
                $("#user-result").html(html);
            }
        });
    }
</script>
<!----------- Add more JS------------>
<script src="js/jquery.maskedinput.js" type="text/javascript"></script>
<script type="text/javascript">
    function myselect(val, rowno) {
        document.getElementById("countrycode" + rowno).disabled = false;
    }

    function Validdate() {
        document.getElementById("dateerror").style.visibility = "hidden";
    }

    function deletephone(val) {
        if (confirm("Do you really wish to delete this record?")) {
            document.getElementById('act').value = val;
            document.getElementById('action').value = "deletephone";
            document.novalidate.submit();
        }
    }

    function removephne(id) {
        if (confirm("Do you really wish to delete this record?")) {
            $('#remove' + id).remove();
        }
    }

    function idremovephne(val) {
        if (confirm("Do you really wish to delete this record?")) {
            document.getElementById('act').value = val;
            document.getElementById('action').value = "deleteid";
            //document.getElementById('hid').value = a;
            document.novalidate.submit();
        }
    }


    setInterval(function () {
        var alert = document.getElementById('alertComment');
        if (alert) {
            if (alert.style.color == 'rgb(132, 147, 168)') {
                alert.style.color = '#fff';
            } else {
                alert.style.color = 'rgb(132, 147, 168)';
            }
        }
    }, 600);
    function alertCheck(statusAlert) {
        if (statusAlert) {
            document.getElementById("isVisible").style.display = 'block';
        } else {
            document.getElementById("isVisible").style.display = 'none';
        }
    }

</script>

<?php if ($abc == '' && $requiredContactDetails['value'] == 'true'): ?>
    <script>
        $(document).ready(function () {
            chkphoneval();
            chkphoneval();
            setTimeout(function () {
                $("select[name='Phone_Type[]']:first").val('Home');
                $("select[name='Phone_Type[]']:last").val('Mobile');
                $("input[name='CPrimary']:last").attr('checked', 'checked');
            }, 500);
        });
    </script>
<?php endif; ?>

<?php if ($abc): ?>
    <script>
        $(document).ready(function () {
            $('.pagename').html("<span>Collector Details : </span><span class='two'> <?= $pagename ?></span>");
        });
    </script>
<?php else: ?>
    <script>
        $(document).ready(function () {
            $('div.pagename').html('<span>New Collector Details</span>');
        });
    </script>
<?php endif; ?>


<body>
<section>
    <div class="mainwrapper">
        <?php include 'includes/leftpanel.php'; ?>
        <div class="mainpanel">
            <div class="pageheader">
                <div class="small-header">Personal Information</div>
            </div>
            <div class="media">
                <!-- pageheader -->
                <form action=""
                      autocomplete="off"
                      enctype="multipart/form-data"
                      method="post"
                      accept-charset="utf-8"
                      class="panel-wizard"
                      novalidate="novalidate"
                      name="novalidate"
                      id="novalidate">

                    <input autocorrect="off"  name="requiredIdentDetails" id="requiredIdentDetails" type="hidden" value="<?php
                    if (isset($requiredIdentDetails))
                        echo $requiredIdentDetails['value'];
                    else
                        echo "true";
                    ?>"/>

                    <input autocorrect="off"  name="requiredContactDetails" id="requiredContactDetails" type="hidden" value="<?php
                    if (isset($requiredContactDetails))
                        echo $requiredContactDetails['value'];
                    else
                        echo "true";
                    ?>"/>
                    <input autocorrect="off"  name="visitval" id="visitval" type="hidden">
                    <input autocorrect="off"  name="action" id="action" type="hidden" value="">
                    <input autocorrect="off"  name="act" id="act" type="hidden" value="">
                    <input autocorrect="off"  name="hid" id="hid" type="hidden" value="">
                    <!-- Hidden Values -->
                    <?php
                    $sqlcountry = $dbh->prepare("select * from collectors order by id desc");
                    $sqlcountry->execute();
                    $datacountry = $sqlcountry->fetch();
                    $memberid = $datacountry['Id'];
                    ?>
                    <input autocorrect="off"  name="memberids" type="hidden" value="<?php if ($abc != '') {
                        echo $abc;
                    } elseif ($memberid) {
                        echo $memberid + 1;
                    } else {
                        echo 1;
                    } ?>">
                    <?php if ($dataquery['personalcomments']): ?>
                        <script>
                            $(document).ready(function () {
                                $('.alert-block').css('display', 'block');
                            });
                        </script>
                    <?php endif; ?>

                    <div class="tab-content member">
                        <div class="alert-block">
                            <div class="row font-size">
                                <span
                                        id="alertComment"
                                        class="alert-comment">ALERT</span>&nbsp;- <?php echo $dataquery['personalcomments']; ?>
                            </div>
                        </div>
                        <div class="bb-alert alert alert-info">
                            <div class="row font-size">
                                <span id="msg" class="msg"></span>
                            </div>
                        </div>
                        <div class="first-block">
                            <div class="row">
                                <?php
                                $sql = $dbh->prepare("SELECT *
                                                      FROM `visitstable`
                                                      WHERE `collectorsid` = :id
                                                      AND `photoname` != ''
                                                      ORDER BY `visitid`
                                                      DESC; ");
                                $sql->execute([':id' => $abc]);
                                $collPhoto = $sql->fetch();
                                ?>

                                <?php if ($collPhoto): ?>
                                    <?php if (file_exists('uploads/' . $collPhoto['photoname'])): ?>
                                        <img src="<?= 'uploads/' . $collPhoto['photoname']; ?>?v=<?= time() ?>"
                                             width="12%"
                                             style="border-radius: 100%;
                                                    float: left;
                                                    margin-top: -0.5%;
                                                    margin-bottom: 2%;
                                                    margin-left: 0.5%;"/>
                                    <?php endif; ?>
                                <?php else: ?>
                                    <img src="images/collector-photo.png"
                                         style="float: left; margin-top: -0.5%; margin-bottom: 2%; margin-left: 0.5%;"/>
                                <?php endif; ?>

                                <div class="block-name-personal">Personal <span style="font-weight: 400">Details</span>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="personal-details personal-label my-label" <?php
                                if (findAssignment($required_fields, 'collectorDetails', 'title')) {
                                    echo " style='color:#428bca;' ";
                                } ?>>
                                    Title
                                </div>
                                <div class="personal-details">
                                    <select id="title"
                                            name="title"
                                            class="form-control personal-input text-capitalize "
                                            required
                                            tabindex="1">
                                        <option value=" ">Choose One</option>
                                        <?php
                                        $sitessql = $dbh->prepare('SELECT *
                                                                   FROM `title`
                                                                   WHERE `Delete` = 0; ');
                                        $sitessql->execute();
                                        while ($sitesdata = $sitessql->fetch()) : ?>
                                            <option <?php
                                            if ($dataquery['title'] == $sitesdata['title'] || (!empty($_GET['title']) && $sitesdata['title'] == $_GET['title'])) {
                                                echo 'selected';
                                            } ?> ><?= $sitesdata['title'] ?></option>
                                        <?php endwhile; ?>
                                    </select>
                                </div>

                                <div class="personal-details personal-label my-label"
                                    <?php
                                    if (findAssignment($required_fields, 'collectorDetails', 'firstname')) {
                                        echo " style='color:#428bca;' ";
                                    } ?>>
                                    First Name
                                </div>
                                <div class="personal-details">
                                    <input autocorrect="off"  type="text"
                                           tabindex="2"
                                           onkeypress="Maxval(this.value,30,this.id)"
                                           name="firstname"
                                           id="firstname"
                                           value="<?php if ($a != '') {
                                               echo $dataquery['firstname'];
                                           } else {
                                               if ($dataquery['firstname'] != '') {
                                                   echo $dataquery['firstname'];
                                               } else {
                                                   if (!empty($_GET['firstname'])) {
                                                       echo $_GET['firstname'];
                                                   } else {
                                                       echo $firstname;
                                                   }
                                               }
                                           } ?>"
                                           class="form-control personal-input text-capitalize  text-capitalize"
                                           required>
                                    <div id="counterfirstname"></div>
                                </div>

                                <div class="personal-details personal-label my-label" <?php
                                if (findAssignment($required_fields, 'collectorDetails', 'lastname')) {
                                    echo " style='color:#428bca;' ";
                                } ?>>
                                    Last Name
                                </div>
                                <div class="personal-details">
                                    <input autocorrect="off"  type="text"
                                           tabindex="3"
                                           onkeypress="Maxval(this.value,20,this.id)"
                                           name="lastname"
                                           id="lastname"
                                           value="<?php if ($a != '') {
                                               echo $dataquery['lastname'];
                                           } else {
                                               if ($dataquery['lastname'] != '') {
                                                   echo $dataquery['lastname'];
                                               } else {
                                                   if (!empty($_GET['lastname'])) {
                                                       echo $_GET['lastname'];
                                                   } else {
                                                       echo $lastname;
                                                   }
                                               }
                                           } ?>" class="form-control personal-input text-capitalize "
                                           required>
                                    <div id="counterlastname"></div>
                                </div>

                                <div class="personal-details personal-label my-label" <?php
                                if (findAssignment($required_fields, 'collectorDetails', 'dob')) {
                                    echo " style='color:#428bca;' ";
                                } ?>>
                                    Date of Birth
                                </div>
                                <div class="personal-details">
                                    <div>
                                        <?php
                                        if (strlen($dataquery['dob']) > 4) {
                                            $onlyYear = false;

                                            $parts = parserDate($dataquery['dob']);
                                            $month = $parts['month'];
                                            $day = $parts['day'];
                                            $year = $parts['year'];
                                        } elseif (strlen($dataquery['dob']) < 5 && $dataquery['dob']) {
                                            $onlyYear = true;
                                        }

                                        $months = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];

                                        $days = 31;
                                        switch ($month) {
                                            case '2':
                                                $days = 29;
                                                break;
                                            case '4':
                                            case '6':
                                            case '9':
                                            case '11':
                                                $days = 30;
                                                break;
                                            //Jan, Mar, May, Jul, Aug, Oct, Dec
                                            default:
                                                $days = 31;
                                        }
                                        
                                        $years = [];
                                        for ($i = 1930; $i <= 2017; $i++) {
                                            $years[] = $i;
                                        }
                                        ?>
                                        <select tabindex="4" name="month" id="month" class="select-birth-collector">
                                            <option value="0">Month</option>
                                            <?php foreach ($months as $m): ?>
                                                <option <?php if ($m == $month || (!empty($_GET['month']) && $m == $_GET['month'])) {
                                                    echo 'selected';
                                                } ?> ><?= $m; ?></option>
                                            <?php endforeach; ?>
                                        </select>

                                        <select tabindex="5" name="day" id="day" class="select-birth-collector">
                                            <option value="0">Day</option>
                                            <?php for ($d = 1; $d <= $days; $d++): ?>
                                                <option <?php if ($day == $d || (!empty($_GET['day']) && $d == $_GET['day'])) {
                                                    echo 'selected';
                                                } ?> ><?= $d; ?></option>
                                            <?php endfor; ?>
                                        </select>

                                        <select tabindex="6" name="year" id="year" class="select-birth-collector">
                                            <option value="0">Year</option>
                                            <?php foreach ($years as $y): ?>
                                                <option <?php
                                                if (($y == $year) || ($y == $dataquery['dob']) || (!empty($_GET['year']) && $y == $_GET['year'])) {
                                                    echo 'selected';
                                                } ?> ><?= $y; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="personal-details personal-label my-label align-top" <?php
                                if (findAssignment($required_fields, 'collectorDetails', 'personalcomments')) {
                                    echo " style='color:#428bca;' ";
                                } ?>>Alert
                                </div>
                                <div class="ckbox ckbox-primary personal-details">
                                    <input autocorrect="off"  name="alerts" id="alerts" type="checkbox" value="1"
                                           onClick="alertcomments(this.checked)"
                                        <?php
                                        if ($dataquery['personalcomments']) {
                                            echo 'checked';
                                        } ?>>
                                    <label class="my-ckbox box"
                                           tabindex="7"
                                           for="alerts"
                                           style="position: absolute; ">
                                    </label>

                                    <textarea autocorrect="off"  class="personal-input personal-comments"
                                              tabindex="8"
                                              style="<?php
                                              if (!$dataquery['personalcomments']) {
                                                  echo 'display: none; ';
                                              } ?>"
                                              name="personalcomments"
                                              id="personalcomments"
                                              rows="5"
                                              cols="97"><?= $dataquery['personalcomments'] ?></textarea>

                                    <a class="btn btn-primary btn-xs view-alert"
                                       id="view-alert"
                                       href="#personal-comments-div"
                                       style="<?php
                                       if (!$dataquery['personalcomments']) {
                                           echo 'display: none; ';
                                       } ?>">
                                        View
                                    </a>

                                    <pre id="personal-comments-div"
                                         class="personal-comments-div white-popup-block mfp-hide">
                                        <?= ($dataquery['personalcomments']) ?>
                                    </pre>

                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="personal-details personal-label my-label"
                                    <?php
                                    if (findAssignment($required_fields, 'collectorDetails', 'address1')) {
                                        echo " style='color:#428bca;' ";
                                    } ?>>
                                    Address 1
                                </div>
                                <div class="personal-details">
                                    <input autocorrect="off"  type="text"
                                           tabindex="9"
                                           onkeypress="Maxval(this.value,50,this.id)"
                                           name="address1"
                                           id="address1"
                                           value="<?php if ($a != '') {
                                               echo $dataquery['address1'];
                                           } else {
                                               if ($dataquery['address1'] != '') {
                                                   echo $dataquery['address1'];
                                               } else {
                                                   echo $address1;
                                               }
                                           } ?>" class="form-control personal-input text-capitalize ">
                                    <div id="counteraddress1"></div>
                                </div>


                                <div class="personal-details personal-label my-label"
                                    <?php
                                    if (findAssignment($required_fields, 'collectorDetails', 'address2')) {
                                        echo " style='color:#428bca;' ";
                                    } ?>>
                                    Address 2
                                </div>
                                <div class="personal-details">
                                    <input autocorrect="off"  type="text"
                                           tabindex="10"
                                           onkeypress="Maxval(this.value,50,this.id)"
                                           name="address2"
                                           id="address2"
                                           value="<?php if ($a != '') {
                                               echo $dataquery['address2'];
                                           } else {
                                               if ($dataquery['address2'] != '') {
                                                   echo $dataquery['address2'];
                                               } else {
                                                   echo $address2;
                                               }
                                           } ?>" class="form-control personal-input text-capitalize ">
                                    <div id="counteraddress2"></div>
                                </div>

                                <div class="personal-details personal-label my-label" <?php
                                if (findAssignment($required_fields, 'collectorDetails', 'city')) {
                                    echo " style='color:#428bca;' ";
                                } ?>>
                                    City
                                    <div class="city-buttons">
                                        <div class="btn btn-primary btn-xs"
                                             id="BB">BB
                                        </div>
                                        <div class="btn btn-primary btn-xs"
                                             id="B">B
                                        </div>
                                    </div>
                                </div>
                                <div class="personal-details">
                                    <input autocorrect="off"  type="text"
                                           tabindex="11"
                                           name="city"
                                           onkeypress="Maxval(this.value,25,this.id); return onlyAlphabets(event,this)"
                                           id="city"
                                           value="<?php if ($a) {
                                               echo $dataquery['city'];
                                           } else {
                                               if ($dataquery['city'] != '') {
                                                   echo $dataquery['city'];
                                               } else {
                                                   echo 'Jerusalem';
                                               }
                                           } ?>" class="form-control personal-input text-capitalize "
                                           required>
                                    <div id="countercity"></div>
                                </div>

                                <div class="personal-details personal-label my-label" <?php
                                if (findAssignment($required_fields, 'collectorDetails', 'statecountry')) {
                                    echo " style='color:#428bca;' ";
                                } ?>>
                                    State/Province
                                </div>
                                <div class="personal-details">
                                    <input autocorrect="off"  type="text"
                                           tabindex="12"
                                           onkeypress="Maxval(this.value,17,this.id)"
                                           name="statecountry"
                                           id="statecountry"
                                           value="<?php if ($a) {
                                               echo $dataquery['statecountry'];
                                           } else {
                                               if ($dataquery['statecountry']) {
                                                   echo $dataquery['statecountry'];
                                               } else {
                                                   echo $state;
                                               }
                                           } ?>" class="form-control personal-input text-capitalize ">
                                    <div id="counterstate"></div>
                                </div>

                                <div class="personal-details personal-label my-label" <?php
                                if (findAssignment($required_fields, 'collectorDetails', 'country')) {
                                    echo " style='color:#428bca;' ";
                                } ?>>
                                    Country
                                </div>
                                <div class="personal-details">
                                    <input autocorrect="off"  type="text"
                                           tabindex="13"
                                           onkeypress="Maxval(this.value,17,this.id)"
                                           class="form-control valid personal-input text-capitalize "
                                           name="country"
                                           id="country"
                                           value="<?php if ($a && !empty($dataquery['country'])) {
                                               echo $dataquery['country'];
                                           } else {
                                               if (!empty($dataquery['country'])) {
                                                   echo $dataquery['country'];
                                               } else {
                                                   echo 'Israel';
                                               }
                                           } ?>"
                                           required>
                                    <div id="countercountry"></div>
                                </div>

                                <div class="personal-details personal-label my-label" <?php
                                if (findAssignment($required_fields, 'collectorDetails', 'zipcode')) {
                                    echo " style='color:#428bca;' ";
                                } ?>>
                                    Zip/Postal Code
                                </div>
                                <div class="personal-details">
                                    <input autocorrect="off"  type="text"
                                           tabindex="14"
                                           onkeypress="Maxval(this.value,10,this.id); return onlyAlphabets(event,this)"
                                           class="personal-input text-capitalize form-control valid"
                                           name="zipcode"
                                           id="zipcode"
                                           value="<?php if ($a != '' && !empty($dataquery['zipcode'])) {
                                               echo $dataquery['zipcode'];
                                           } else {
                                               if (!empty($dataquery['zipcode'])) {
                                                   echo $dataquery['zipcode'];
                                               } else {
                                                   echo $zipcode;
                                               }
                                           } ?>"
                                           required>
                                    <div id="counterzipcode"></div>
                                </div>
                            </div>
                        </div>
                        <div class="second-block">
                            <div class="row">
                                <div class="block-name panel-wizard">
                                    Identification <span style="font-weight: 400">Details</span>
                                </div>
                            </div>
                            <div class="row form-group my-label headers">
                                <div class="col-sm-1 col-xs-1 field primary-ident-label">
                                    Primary
                                </div>
                                <div class="col-sm-1 col-xs-1 ident-type">
                                    Type
                                </div>
                                <div class="col-sm-2 col-xs-2 field ident-country-label">
                                    <div style="float: left; margin-right: 5%; ">Country</div>
                                    <div class="country-buttons">
                                        <button class="btn btn-primary btn-xs" type="button" id="ident_isr">ISR
                                        </button>
                                        <button class="btn btn-primary btn-xs" type="button" id="ident_usa">USA
                                        </button>
                                        <button class="btn btn-primary btn-xs" type="button" id="ident_uk">UK
                                        </button>
                                    </div>
                                </div>
                                <div class="col-sm-2 col-xs-2 field ident-number">
                                    Number
                                </div>
                                <div class="col-sm-2 col-xs-2 field" id="passport-name-label">
                                </div>
                            </div>

                            <?php
                            if ($abc != '') {
                                ?>
                                    <?php
                                    $collphone = $dataquery['Id'];
                                    $sqlquery = $dbh->prepare('SELECT *
                                                               FROM `collident`
                                                               WHERE `Collid` = :collid
                                                               AND `delete` = 0; ');
                                    $sqlquery->execute(['collid' => $collphone]);
                                    while ($dataquery2 = $sqlquery->fetch()) { ?>
                                        <div id="addinput1" data-id =<?= $dataquery2['Id']; ?>>
                                        <div class="row collident form-group">
                                            <div class="col-sm-1 col-xs-1 field new-primary-ident">
                                                <input autocorrect="off"  name="update_val[]" id="update_val"
                                                       value="<?= $dataquery2['Id']; ?>"
                                                       type="hidden">

                                                <input autocorrect="off"  name="Primary1"
                                                       value="<?= $dataquery2['Id']; ?>"
                                                       onClick="newid(<?= $dataquery2['Id']; ?>,<?= $collphone; ?>)"
                                                       type="radio"
                                                    <?php
                                                    if ($dataquery2['Primary1']) {
                                                        echo 'checked';
                                                    }
                                                    ?>>
                                            </div>
                                            <div class="col-sm-2 col-xs-2">
                                                <select data-placeholder="Choose One"
                                                        id="Identnum<?= $dataquery2['Id']; ?>"
                                                        name="Collidenti[]"
                                                        required
                                                        class="form-control"
                                                        lang="<?= $dataquery2['Id']; ?>"
                                                        onchange="myssn(this.value, this.lang, this);">

                                                    <option value=" ">Choose One</option>
                                                    <?php $types = [
                                                        'Passport',
                                                        'Teudat Zeut',
                                                        'SSN / ID Number',
                                                        'NIN',
                                                        'Driver License',
                                                        'Green Card',
                                                        'Number']; ?>
                                                    <?php foreach ($types as $type): ?>
                                                        <option <?php
                                                        if ($type == $dataquery2['Collidenti']) {
                                                            echo 'selected';
                                                        } ?> ><?= $type ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                            <div id="myssn<?= $dataquery2['Id'] ?>">
                                                <div class="col-sm-2 col-xs-2 field  ident-country">
                                                    <input autocorrect="off"  type="text" style="text-transform:uppercase"
                                                           class="form-control"
                                                           onKeyPress="return onlyAlphabets(event,this)"
                                                           onkeyup="validateFormID()"
                                                           id="identificationcountry<?= $dataquery2['Id']; ?>"
                                                           onfocus="identCountryListener(this.id)"
                                                           name="Collcountry[]"
                                                           placeholder="Country"
                                                           onchange="myselectId1(this.value,this.lang);IdValidation(this.lang,this.value);"
                                                           value="<?= $dataquery2['Collcountry']; ?>">
                                                </div>
                                                <div class="col-sm-2 col-xs-2">
                                                    <input autocorrect="off"  type="text" class="form-control"
                                                           value="<?= $dataquery2['Collphone']; ?>"
                                                           id="Identno"
                                                           onkeypress="return blockSpecialChar(event)"
                                                           name="Collphone[]"
                                                           placeholder="Number"
                                                        <?php if ($dataquery2['Collidenti'] == 'SSN / ID Number'): ?>
                                                            onkeydown="$(this).simpleMask( { 'mask': 'ssn', 'nextInput': false } );"
                                                        <?php endif; ?>
                                                    >
                                                </div>

                                                <?php if ($dataquery2['Collidenti'] == 'Passport'): ?>
                                                    <div class="col-sm-2 col-xs-2 field" id="passport-name">
                                                        <input autocorrect="off"  type="text" class="form-control"
                                                               onKeyPress="return onlyAlphabets(event,this)"
                                                               id="old-passport-name<?= $dataquery2['Id']; ?>"
                                                               name="old-passport-name[]"
                                                               placeholder="Passport Name"
                                                               value="<?php if ($dataquery2['passport_name']) {
                                                                   echo $dataquery2['passport_name'];
                                                               } ?>">
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                            <div class="col-sm-2 col-xs-2 row field">
                                                <a onClick="idremovephne(<?= $dataquery2['Id']; ?>)">
                                                    <img style="margin-top: 6%;"
                                                         class="delete-img"
                                                         src="images/remove-icon.png"
                                                         title="Remove Details"
                                                         alt="Remove">
                                                </a>
                                            </div>
                                        </div>
                                        </div>
                                    <?php } ?>

                            <?php } ?>
                            <?php include 'addidenti.php'; ?>
                            <div class="row">
                                <a href="javascript:void(0); " onclick="ChkvalId();"
                                   style="margin-left: 0.5%; "
                                   title="Add New Identification Details Item">
                                    <img src="images/add-icon.png"
                                         style="height:25px;"/>
                                </a>
                            </div>
                        </div>
                        <div class="second-block">
                            <div class="row">
                                <div class="block-name panel-wizard">
                                    Contact <span style="font-weight: 400">Details</span>
                                </div>
                            </div>

                            <div class="row my-label headers">
                                <div class="col-sm-1 col-xs-1 primary-contact-label">
                                    Primary
                                </div>
                                <div class="col-sm-1 col-xs-1 phone-type-label" style="">
                                    Type
                                </div>
                                <div class="col-sm-2 col-xs-2" style="margin-right: 8.4%;">
                                    <div style="float: left; margin-right: 5%; ">Country</div>
                                    <div class="country-buttons">
                                        <button class="btn btn-primary btn-xs" type="button" id="contact_isr">
                                            ISR
                                        </button>
                                        <button class="btn btn-primary btn-xs" type="button" id="contact_usa">
                                            USA
                                        </button>
                                        <button class="btn btn-primary btn-xs" type="button" id="contact_uk">
                                            UK
                                        </button>
                                    </div>
                                </div>
                                <div class="col-sm-2 col-xs-1 contact-number-label">
                                    Phone
                                </div>
                            </div>
                            <?php if ($abc != '') {

                                $memid = $dataquery['Id'];
                                $sqlquery = $dbh->prepare('select *
                                                           from `collectorphone`
                                                           where `collid` = :collid
                                                            and `delete` = 0 ;');
                                $sqlquery->execute(['collid' => $memid]);

                                while ($dataquery1 = $sqlquery->fetch()) { ?>
                                    <div class="row form-group" data-id="<?= $dataquery1['Id']; ?>"
                                         id="addinput">
                                        <div class="row col-sm-1 field input-primary">
                                            <input autocorrect="off"  type="hidden" name="contactDetailId[]"
                                                   value="<?php echo $dataquery1['Id']; ?>"/>
                                            <input autocorrect="off"  name="CPrimary"
                                                   value="1"
                                                   onClick="newid1(<?= $dataquery1['Id']; ?>,<?= $dataquery1['collid']; ?>)"
                                                   type="radio"
                                                <?php
                                                if ($dataquery1['CPrimary'] != '') {
                                                    echo 'checked';
                                                }
                                                ?>>
                                        </div>
                                        <div class="row col-sm-1 phone-type" style="width: 150px">
                                            <select class="form-control"
                                                    name="Phone_Type[]">

                                                <option value=" ">Choose One</option>

                                                <?php $types = [
                                                    'Work',
                                                    'Home',
                                                    'Mobile',
                                                    'Fax',
                                                    'Other']; ?>
                                                <?php foreach ($types as $type): ?>
                                                    <option <?php
                                                    if ($type == $dataquery1['Phone_Type']) {
                                                        echo 'selected';
                                                    } ?> ><?= $type ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>

                                        <div class="col-sm-3 col-xs-3">

                                            <?php $domain = $dataquery1['Phone_Country']; ?>

                                            <input autocorrect="off"  type="text" style="text-transform:uppercase;"
                                                   class="form-control valid"
                                                   onchange="chkphonetypeselect(this.value,this.lang);"
                                                   onfocus="contactCountryListener(this.value, this.id, this.lang);"
                                                   id="countrycode<?php echo $dataquery1['Id']; ?>"
                                                   name="Phone_Country[]"
                                                   placeholder="Country"
                                                   onKeyPress="return onlyAlphabets(event,this)"
                                                   lang="<?php echo $dataquery1['Id']; ?>"
                                                   value="<?php if ($dataquery1['Phone_Country']) {
                                                       echo $domain;
                                                   } ?>">
                                        </div>
                                        <div class="row col-sm-3 col-xs-3 field">
                                            <input autocorrect="off"  type="text"
                                                   name="Phone_Number[]"
                                                   value="<?= $dataquery1['Phone_Number']; ?>"
                                                   id="phone<?php echo $dataquery1['Id']; ?>"
                                                   class="form-control"
                                                   placeholder="Number"
                                                   onkeypress="return blockSpecialCharNum(event)"
                                            >
                                        </div>
                                        <div class="col-sm-1 col-xs-1 row field">
                                            <a onclick="deletephone(<?= $dataquery1['Id']; ?>)">
                                                <img class="delete-img"
                                                     src="images/remove-icon.png"
                                                     border="0"
                                                     title="Remove Details"></a>
                                        </div>
                                    </div>
                                    <?php
                                }
                            } ?>
                            <script type="text/javascript" src="js/jquery.SimpleMask.js"></script>
                            <?php include 'add_phone_wo_row.php'; ?>

                            <div class="row">
                                <div class="col-sm-12 field" id='phone_number'
                                     style="text-align:center;">
                                    <input autocorrect="off"  type="hidden" value="0" name="phoneid[]" id="phoneid">
                                </div>
                            </div>
                            <div class="row">
                                <a href="javascript:void(0);" onclick="chkphoneval();"
                                   class="add-button add-contact"
                                   title="Add field"
                                   id="addContact"><img src="images/add-icon.png" height="25px"></a>
                            </div>
                        </div>
                    </div>

                    <div class="my-footer">
                        <input autocorrect="off"  id="Cntsubmitchk" name="next123" type="text"
                               style="margin-left: 5px;margin-top: 1px;display:none;">

                        <button class="btn btn-primary mr5"
                                disabled
                                id="addNewVisit"
                                value="new"
                                name="new"
                                type="submit"
                                style="margin-left: 5px;margin-top: 1px;">
                            Add New Visit
                            <i class="fa fa-circle-o-notch fa-spin fa-fw add-visit-loading"
                               style="display: none; "></i>
                        </button>
                    </div>
            </div>
            </form>
        </div><!-- tab-pane -->
    </div>

    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    <script>
        function blockSpecialChar(e) {
            var k = e.keyCode;
            return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 45 ||
            (k >= 48 && k <= 57) || k == 40 || k == 41 || k == 45);
        }

        function blockSpecialCharNum(e) {
            var k = e.keyCode;
            return (k == 46 || k == 32 || k == 8 || (k >= 48 && k <= 57) ||
            k == 40 || k == 41 || k == 45 || k == 43);
        }

        function attachmentCheck(lang) {
            var input = $("#p_new" + lang);
            var file = input.prop('files')[0];
            if (file.size > 300000) {
                alert('File is too large. Please choose file smaller than 300 kb');
                input.remove();
                $("#attachmentDiv" + lang).append('<input autocorrect="off"  required type="file" name="scandocument[]" id="p_new' + lang + '" class="form-control" placeholder="Scan document" lang="' + lang + '" onchange="attachmentCheck(' + lang + ');">');
            }
            if (file.type != 'image/png' && file.type != 'image/jpeg') {
                alert('Wrong file type. Please upload .jpg or .png file.');
                input.remove();
                $("#attachmentDiv" + lang).append('<input autocorrect="off"  type="file" name="scandocument[]" id="p_new' + lang + '" class="form-control" placeholder="Scan document" lang="' + lang + '" onchange="attachmentCheck(' + lang + ');">');
            }
        }
        // ---------------------    NEW IDENTITY DETAILS FUNCTIONALITY ---------------------- //
        function newIdentityDetailsItem() {

            var dataString = 'idcountClicks=' + 0;
            $.ajax({
                type: "POST",
                url: "ajax/newIdentity.php",
                data: dataString,
                cache: false,
                success: function (html) {
                    $("#itemList > tbody").append(html);

                }
            });
        }

        function validatememberForm() {
            var Idtype = document.getElementsByName("Identnum[]");

            if (Idtype.length > 0) {
                //var countId=-1;
                for (var j = 0; j < Idtype.length; j++) {
                    var Idtypeval = document.getElementById("Identnum" + j).value;
                    var Idcountry = document.getElementById("identificationcountry" + j).value;
                    var IdNum = document.getElementsByName("Identno[]")[j].value;
                    //countId++;

                }

                var txtdate = document.getElementById("txtdate").value;
                var duperr = document.getElementById("duperrordispaly").style.visibility;
                if (duperr == 'visible') {
                    document.getElementById('submitchk').disabled = true;
                    document.getElementById('upsubmitchk').disabled = true;
                } else {
                    document.getElementById('submitchk').disabled = false;
                    document.getElementById('upsubmitchk').disabled = false;
                }
                if (txtdate == '') {
                    document.getElementById("dateerror").style.visibility = "visible";
                } else {
                    document.getElementById("dateerror").style.visibility = "hidden";
                }
            } else {
                //return false;
            }

        }

        function checkCountryRequirement() {

            var valueToBeTested = $("#country").val().toLowerCase();
            var allowedValues = ["us", "usa", "united states"];

            return ($.inArray(valueToBeTested, allowedValues) == -1) ? true : false;

        }

        function validatePost() {
            $.post("check.php", {allowedFields: "allowed"}, function (data) {
                validateFunction(data);
            });
        }

        function validateFunction(data) {


            var customedObj = {};
            customed = {};
            customed.zipcode = {required: checkCountryRequirement}; // ?
            customed.state = {required: checkCountryRequirement}; //?

            customedObj = jQuery.parseJSON(data);

            for (var i = 0; i < customedObj.length; i++) {

                field = customedObj[i].assignment;

                customed[field] = {required: false};
            }


            //customed.gender = {required: true};

            $("#novalidate").validate({
                rules: customed,
                errorClass: "override-error"
            });
        }

        validatePost();

        /*$("#novalidate").validate({
         rules: {
         gender: {
         required: true
         },
         zipcode: {
         required: checkCountryRequirement
         },
         state:{
         required: checkCountryRequirement
         }
         }
         });*/

    </script>
    <script src="js/jquery-migrate-1.2.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/modernizr.min.js"></script>
    <script src="js/ppace.min.js"></script>

    <script src="js/jquery.cookies.js"></script>
    <script src="js/jquery.dataTables.min.js"></script>
    <script src="js/dataTables.bootstrap.js"></script>
    <script src="js/dataTables.responsive.js"></script>
    <script src="js/custom.js"></script>
    <script src="signature.js"></script>
    <script>
        $(function () {

            $("#novalidate").on("submit", function () {
                var address = window.location.search;
                var isAbc = /abc=/i.test(address);

                var requiredIdentificationDetails = $("#requiredIdentDetails").val();
                var requiredContactDetails = $("#requiredContactDetails").val();

                var identificationCurrentDetailsCount = $("input[name='Primary1']").length;
                var contactCurrentDetailsCount = $("select[name='Collidenti[]']").length;

                var identificationDetailsCount = $("select[name='Phone_Type[]']").length;
                var contactDetailsCount = $("select[name='Phone_Number[]']").length;

                var oneRowEntered = false;

                $("input[name='Phone_Country[]']").each(function () {
                    if ($(this).val() != '') {
                        oneRowEntered = true;
                    }
                });

                if (requiredIdentificationDetails == "true" && !isAbc &&
                    (identificationDetailsCount == 0 &&
                    identificationCurrentDetailsCount == 0)) {

                    //alert("At least one row of Identification Details is required..");
                    //return false;
                }


                if (requiredContactDetails == "true" &&
                    contactDetailsCount == 0 &&
                    contactCurrentDetailsCount == 0 && !isAbc && !oneRowEntered) {
                    //alert("At least one row of Contact Details is required..");
                    //return false;
                }

            });

            function format(item) {
                return '<i class="fa ' + ((item.element[0].getAttribute('rel') === undefined) ? "" : item.element[0].getAttribute('rel')) + ' mr10"></i>' + item.text;
            }

            // This will empty first option in select to enable placeholder
            jQuery('select option:first-child').text('');
        });

    </script>
    <script src="javascript-image-upload/vendor/canvas-to-blob.min.js"></script>
    <script src="javascript-image-upload/resize.js"></script>
    <script src="javascript-image-upload/app.js"></script>
    <!-- Magnific Popup core JS file -->
    <script src="magnific-popupp/dist/jquery.magnific-popup.js"></script>
    <!--Magnific Popup min JS file-->
    <script src="magnific-popupp/dist/jquery.magnific-popup.min.js"></script>
</body>


