<?php
error_reporting(0);
include '../../../includes/dbcon.php';
session_start();
$a = $_GET['a'];
$b = $_GET['b'];
$today = $_GET['c'];

$sql = $dbh->prepare(' SELECT *
		               FROM `visitstable`
		               WHERE `collectorsid` = :collid
		               AND `visitid` = :visitid 
		               ORDER BY `visitid`
		               DESC ;');
$sql->execute([
    'collid'  => $a,
    'visitid' => $b
]);
$data = $sql->fetch();

$sql = $dbh->prepare('
		SELECT *
		FROM `visitstable`
		WHERE `collectorsid` = :collid
		AND `visitid` = :visitid
		ORDER BY `visitid`
		DESC
	');
$sql->execute(['collid' => $a, 'visitid' => ($b - 1)]);
$previousVisitData = $sql->fetch();

$query = $dbh->prepare(
    "
	  	SELECT
	  	*
	  	FROM
	  	`app_options`
	  	WHERE 
	  		`app_options`.`key` = 'default_path_photo'
	  "
);
$query->execute();
$defaultPathPhoto = $query->fetch();

$defaultPathPhoto = $defaultPathPhoto['value'] == '' ? 'uploads/' : $defaultPathPhoto['value'];

$query = $dbh->prepare(
    "
	  	SELECT
	  	*
	  	FROM
	  	`app_options`
	  	WHERE
	  	`app_options`.`key` = 'default_path_sign'
	  "
);
$query->execute();
$defaultPathSign = $query->fetch();

$defaultPathSign = ($defaultPathSign['value'] == '') ? 'uploads/' : $defaultPathSign['value'];

function dateFormat($dateStr)
{
    $dateArr = parserDate($dateStr);
    switch ($dateArr['month']) {
        case('Jan'):
            $dateArr['month'] = '01';
            break;
        case('Feb'):
            $dateArr['month'] = '02';
            break;
        case('Mar'):
            $dateArr['month'] = '03';
            break;
        case('Apr'):
            $dateArr['month'] = '04';
            break;
        case('May'):
            $dateArr['month'] = '05';
            break;
        case('Jun'):
            $dateArr['month'] = '06';
            break;
        case('Jul'):
            $dateArr['month'] = '07';
            break;
        case('Aug'):
            $dateArr['month'] = '08';
            break;
        case('Sep'):
            $dateArr['month'] = '09';
            break;
        case('Oct'):
            $dateArr['month'] = '10';
            break;
        case('Nov'):
            $dateArr['month'] = '11';
            break;
        case('Dec'):
            $dateArr['month'] = '12';
            break;
        default:
            $dateArr['month'] = '01';
    }
    return implode('-', [$dateArr['month'], $dateArr['day'], $dateArr['year']]);
}

function hebrewReverse($strings, $separator)
{
    //checking empty elements and creating new array
    $array = [];
    foreach ($strings as $string) {
        if ($string) {
            $array[] = $string;
        }
    }
    //changing words order in hebrew names
    $newArray = [];
    foreach ($array as $item) {
        if (preg_match('/[^\x00-\x7F]/', $item)) {
            $newArray[] = hebrewWrap($item);
        } else {
            $newArray[] = $item;
        }
    }
    return implode($separator, $newArray);
}

function hebrewWrap($text)
{
    return preg_replace('/([^\x00-\x7F][^a-z]+[^\x00-\x7F])+/', '<span>$1</span>', $text);
}

$sql = $dbh->prepare("SELECT * FROM `collectors` WHERE `Id` = '$a'");
$sql->execute();
$collData = $sql->fetch();

function textLimit($text, $limit)
{
    $strlen = strlen($text);
    $lineBreaks = preg_match_all("/\n/", $text);
    $textLength = $strlen + ($lineBreaks * 100);

    if ($textLength >= $limit) {
        $text = preg_replace("/\n/", ' ', $text);

        if (strlen($text) >= $limit) {
            return substr($text, 0, ($limit + $lineBreaks));
        } else {
            return $text;
        }
    } else {
        return $text;
    }
}

?>

<style type="text/css">
    div.zone {
        border: solid 2mm #66AACC;
        border-radius: 3mm;
        padding: 1mm;
        background-color: #FFEEEE;
        color: #440000;
    }

    div.zone_over {
        width: 30mm;
        height: 35mm;
        overflow: hidden;
    }

    .big-font {
        font-size: 10.5pt;
    }
</style>
<page style="font-size: 8.6pt;">

    <br> <?php $lv = $data['visitid'] - 1;
    $lvsql = $dbh->prepare("select * from visitstable where collectorsid=$a && visitid=$lv order by visitid desc");
    $lvsql->execute();
    $lvdata = $lvsql->fetch();
    ?>

    <table style="width: 111%; padding:5px; margin-left:-40px; margin-top:-15px;">
        <tr>
            <td style="width: 50%;border: solid 1px #000;" align="center">
                <table style="width: 400%;padding:2px;">
                    <tr>
                        <td style="width: 6%;text-align: left;">LAST VISIT :</td>
                        <td style="width: 5%;"><?php
                            if ($data['free_formed_last_visit_date'] && $data['free_formed_last_visit_date'] !== 'First Visit') {
                                echo dateFormat($data['free_formed_last_visit_date']);
                            } elseif ($previousVisitData['authotodate'] && $previousVisitData['authotodate'] !== 'First Visit') {
                                echo dateFormat($previousVisitData['authotodate']);
                            }
                            ?></td>
                        <td style="width: 5%;"></td>
                        <td style="width: 3%;"># :</td>
                        <td><?php
                            if ($data['free_formed_last_visit_number']) {
                                echo $data['free_formed_last_visit_number'];
                            } elseif ($previousVisitData['Refid']) {
                                echo $previousVisitData['Refid'];
                            } else {
                                echo 'First Visit';
                            } ?></td>
                    </tr>
                </table>
            </td>
            <td style="width: 10%; text-align: center;"></td>
            <td style="width: 40%; ">
                <table style="width: 400%;padding:2px;">
                    <tr>
                        <td style="width: 5%;text-align: left;">Driver :</td>
                        <td class="big-font" style="width: 225px; border-bottom:1px solid #000;padding:1px;">&lrm;&lrm;
                            <?php
                            if ($data['driver']) {
                                if (preg_match('/:/', $data['driver'])) {
                                    $driver = explode(':', $data['driver']);
                                    $driver = $driver[1];
                                } else {
                                    $driver = $data['driver'];
                                }
                            } else {
                                $driver = $data['free_formed_driver'];
                            }

                            echo $driver;
                            ?>
                        </td>

                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3" style="height:1px;"></td>
        </tr>
        <tr>
            <td colspan="3" align="center"><span style="font-size:23px;font-weight:bold; text-align:center;">REGISTRATION OF IDENTITY</span>
            </td>
        </tr>
        <tr>
            <td colspan="3" style="height:1px;"></td>
        </tr>
        <tr>
            <td colspan="3" align="center">
                <table style="width: 400%;">
                    <tr>
                        <td style="width: 27px;text-align: left; font-weight:bold;">No :</td>
                        <td class="big-font"
                            style="width: 60px; border-bottom:1px solid #000;"><?php if ($data['Refid']) {
                                echo $data['Refid'];
                            } else {
                                echo "{$data['visitid']} - {$data['collectorsid']}";
                            } ?></td>
                        <td style="width: 95px;text-align: left; font-weight:bold;">Today`s Date :</td>
                        <td class="big-font"
                            style="width: 4%; border-bottom:1px solid #000;padding:1px;"><?= $today; ?></td>
                        <td style="width: 150px; font-weight:bold;">authorized period from</td>
                        <td class="big-font" style="width: 100px; border-bottom:1px solid #000;padding:1px;">
                            <?php
                            $date = dateFormat($data['authofromdate']);
                            echo $date;
                            ?>
                        </td>
                        <td style="width: 1%; font-weight:bold;">to</td>
                        <td class="big-font" style="width: 111px; border-bottom:1px solid #000;padding:1px;">
                            <?php
                            $date = dateFormat($data['authotodate']);
                            echo $date;
                            ?>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        
        <tr>
            <td colspan="3">
                <table style="width: 400%;">
                    <?php
                    $sql = $dbh->prepare('SELECT *
                                          FROM `visit_details`
                                          WHERE `visit_id` = :visitid
                                          AND `detail_type` = :detailtype
                                          AND `type` = :thistype
                                          AND `delete` = 0 ;');

                    $sql->execute([
                        'visitid'    => $data['Id'],
                        'detailtype' => 'identification',
                        'thistype'   => 'Passport'
                    ]);
                    $passport = $sql->fetch();
                    $passportName = $passport['passport_name'];

                    if ($passportName) {
                        $width = 287;
                    } else {
                        $width = 587;
                    }
                    ?>

                    <tr>
                        <td valign="top" style="width: 130px;text-align: left; font-weight:bold;">Name of Collector :
                        </td>
                        <td class="big-font" style="width: <?= $width ?>px; border-bottom:1px solid #000;">&lrm;&lrm;
                            <?php
                            $words = [
                                $data['title'],
                                $data['firstname'],
                                $data['lastname']
                            ];

                            echo hebrewReverse($words, ' ');
                            ?>
                        </td>

                        <?php if ($passportName): ?>
                            <td valign="top" style="width: 110px;text-align: left; font-weight:bold;">
                                Passport Name:
                            </td>
                            <td class="big-font" style="width: 180px; border-bottom:1px solid #000;">
                                <?= $passportName ?>
                            </td>
                        <?php endif; ?>
                    </tr>
                </table>
            </td>
        </tr>
        
        <tr>
            <td colspan="3">
                <table style="width: 400%;">
                    <tr>
                        <td valign="top" style="width: 210px; text-align: left; font-weight:bold;">Permanent Address and
                            Phone :
                        </td>
                        <td class="big-font" style="width: 508px; border-bottom:1px solid #000;">&lrm;&lrm;
                            <?php
                            $words = [
                                $data['address1'],
                                $data['city'],
                                $data['statecountry'],
                                $data['country'],
                                $data['zipcode']
                            ];

                            echo hebrewReverse($words, ' , ');
                            ?>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        
        <tr>
            <td colspan="3">

                <table style="width: 400%;">
                    <tr>
                        <td style="width: 485px; border-bottom:1px solid #000;">

                        </td>
                        <td style="width: 50px; font-weight:bold;"> Phone :</td>
                        <td class="big-font" style="width: 177px; border-bottom:1px solid #000;"> <?php
                            $sql = $dbh->prepare('SELECT *
                                          FROM `visit_details`
                                          WHERE `visit_id` = :visitid
                                          AND `detail_type` = :detailtype
                                          AND `type` = :thistype
                                          AND `delete` = 0 LIMIT 1;');

                            $sql->execute([
                                'visitid'    => $data['Id'],
                                'detailtype' => 'contact',
                                'thistype'   => 'Home'
                            ]);
                            $Home = $sql->fetch(); ?>
                            <?= '&nbsp;' . $Home['detail_number']; ?></td>
                    </tr>
                </table>
            </td>
        </tr>
        
        <tr>
            <td colspan="3">

                <table style="width: 400%;">
                    <tr>
                        <td valign="top" style="width: 250px;text-align: left; font-weight:bold;">Temporary Address and
                            phone in U.S :
                        </td>
                        <td class="big-font" style="width: 468px; border-bottom:1px solid #000;">&lrm;&lrm;
                            <?php
                            $words = [
                                $data['usaddress1'],
                                $data['zipcity'],
                                $data['zipstate'],
                                $data['uscountry'],
                                $data['uszipcode']
                            ];

                            echo hebrewReverse($words, ' , ');
                            ?>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        
        <tr>
            <td colspan="3">

                <table style="width: 400%;">
                    <tr>
                        <td style="width: 485px; border-bottom:1px solid #000;">

                        </td>
                        <td style="width: 50px; font-weight:bold;"> Phone :</td>
                        <td class="big-font"
                            style="width: 177px; border-bottom:1px solid #000;"> <?= $data['zipphone']; ?></td>
                    </tr>
                </table>
            </td>
        </tr>
        
        <tr>
            <td colspan="3">

                <table style="width: 400%;">
                    <tr>
                        <td valign="top" style="width: 224px;text-align: left; font-weight:bold;">Temporary Address in
                            Baltimore :
                        </td>
                        <td class="big-font" style="width: 495px; border-bottom:1px solid #000;">&lrm;&lrm;
                            <?php
                            if ($data['host']) {
                                $hostNames = explode(' ', $data['host']);
                            } elseif ($data['free_formed_host']) {
                                $hostNames = explode(' ', $data['free_formed_host']);
                            } else {
                                $hostNames = '';
                            }


                            $sql = $dbh->prepare('SELECT `Address`
                                                  FROM `hosts`
                                                  WHERE `Firstname` = :firstname
                                                  AND `Lastname` = :lastname ;');
                            $sql->execute([
                                'firstname' => $hostNames[0],
                                'lastname'  => $hostNames[1]
                            ]);

                            $hostAddress = $sql->fetch()[0];

                            $words = [
                                $hostAddress,
                                $data['baltiaddress1'],
                                $data['balticity'],
                                $data['baltistate'],
                                $data['balticountry']
                            ];

                            if ($data['host']) {
                                echo hebrewWrap($data['host']) . ' ';
                            } else {
                                echo hebrewWrap($data['free_formed_host']) . ' ';
                            }

                            echo hebrewReverse($words, ' , ');
                            ?>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        
        <tr>
            <td colspan="3">
                <?php
                $sql = $dbh->prepare('SELECT *
                                      FROM `visit_details`
                                      WHERE `visit_id` = :visitid
                                      AND `detail_type` = :detailtype
                                      AND `type` = :numbertype
                                      AND `delete` = 0 ;');
                $sql->execute([
                    'visitid'    => $data['Id'],
                    'detailtype' => 'contact',
                    'numbertype' => 'Mobile'
                ]);
                $phoneData = $sql->fetch(); ?>
                <table style="width: 400%;">
                    <tr>
                        <td style="width: 110px; font-weight: bold"> AMERICAN CELL:
                        </td>
                        <td class="big-font"
                            style="border-bottom: 1px solid black; width: 300px; "> <?php if (in_array($phoneData['country'],
                                ['USA', 'United States', 'UNITED STATES'])) {
                                echo $phoneData['detail_number'];
                            } ?></td>
                        <td style="width: 55px; font-weight:bold;"> Phone :</td>
                        <td class="big-font"
                            style="width: 243px; border-bottom:1px solid #000;"> <?= $data['baltiphone'] ?></td>
                    </tr>
                </table>
            </td>
        </tr>
        
        <tr>
            <td colspan="3">
                <table style="width: 400%;">
                    <tr>
                        <td valign="top" style="width: 90px;text-align: left; font-weight:bold;">Date of Birth :</td>
                        <td style="width: 180px; border-bottom:1px solid #000;">
                            <table>
                                <tr>
                                    <?php $dobdata = parserDate($data['dob']); ?>
                                    <td class="big-font" style="width:40px;"><?= $dobdata['month'] ?></td>
                                    <td style="width:10px;">-</td>
                                    <td class="big-font" style="width:40px;"><?= $dobdata['day']; ?></td>
                                    <td style="width:10px;">-</td>
                                    <td class="big-font" style="width:40px;"><?= $dobdata['year']; ?></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <table style="width: 400%; margin-top:-10px;">
                    <tr>
                        <td valign="top" style="width: 90px;text-align: left; font-weight:bold;"></td>
                        <td style="width: 180px;">
                            <table>
                                <tr>
                                    <td style="width:40px;">month</td>
                                    <td style="width:10px;"></td>
                                    <td style="width:40px;">day</td>
                                    <td style="width:10px;"></td>
                                    <td style="width:40px;">year</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3" style="height:1px;"></td>
        </tr>
        <tr>
            <td colspan="3" align="center"> THE PURPOSE OF THESE FUNDS IS
                FOR AN <?php
                if ($data['funds'] == 'Individual') {
                    echo 'INDIVIDUAL';
                } else {
                    echo 'INSTITUTION';
                } ?>
            </td>
        </tr>
        <tr>
            <td colspan="3" style="height:1px;"></td>
        </tr>
        <?php
        if ($data['funds'] == 'Institution' || $data['funds'] != 'Individual'):
            ?>
            <tr>
                <td colspan="3">
                    <table style="width: 400%;">
                        <tr>
                            <td valign="top" style="width: 160px;text-align: left; font-weight:bold;">NAME OF
                                INSTITUTION:
                            </td>
                            <td style="width: 557px; border-bottom:1px solid #000;"><?= '&nbsp;' . $data['institname']; ?></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="3">

                    <table style="width: 400%;">
                        <tr>
                            <td style="width: 75px;font-weight:bold;">ADDRESS :</td>
                            <td style="width: 400px; border-bottom:1px solid #000;">
                                <?php
                                $words = [
                                    $data['institaddress1'],
                                    $data['institaddress2'],
                                    $data['institcity'],
                                    $data['institstate'],
                                    $data['institcountry'],
                                    $data['institzip']
                                ];

                                echo hebrewReverse($words, ' , ');
                                ?>
                            </td>
                            <td style="width: 50px; font-weight:bold;"> Tel :</td>
                            <td style="width: 177px; border-bottom:1px solid #000;"> <?= $data['institphone'] ?></td>
                        </tr>
                    </table>
                </td>
            </tr>
        <?php endif; ?>
        <tr>
            <td colspan="3" style="width: 485px; border-bottom:1px solid #000;text-align:justify;">
                <table style="width: 400%;">
                    <tr>
                        <td valign="middle"
                            style="width:  690px;text-align: justify;line-height:1.4;">&lrm;
                            <?php
                            if ($data['internal_purpose']) {
                                $purpComments = textLimit($data['internal_purpose'], 510);
                                $order = array("\r\n", "\n", "\r");
                                $replace = '<br/>';
                                $purpComments = str_replace($order, $replace, $purpComments);

                                echo hebrewWrap($purpComments);
                            } ?>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3" style="height:3px;"></td>
        </tr>
        <tr>
            <td colspan="3" valign="top" style="width: 280px;text-align: center; font-weight:bold;">WE SUGGEST YOU HELP
                WITH THE ABOVE WITH A
            </td>
        </tr>

        <tr>
            <td colspan="3" valign="top" style="width: 280px;text-align: left;">
                <table style="width: 400%;">
                    <tr style="font-size: 10px; ">
                        <td valign="middle"
                            style="width: 10px;text-align: center;"><?php if ($data['donation'] == 'Substantial Donation') {
                                echo '&nbsp;<img src="res/imgs/tick.png">&nbsp;';
                            } else {
                                echo '&nbsp;<img src="res/imgs/empty.png">&nbsp;';
                            } ?></td>
                        <td valign="top" style="width: 100px;text-align: left;">SUBSTANTIAL DONATION</td>
                        <td valign="middle"
                            style="width: 10px;text-align: center;"><?php if ($data['donation'] == 'Generous Donation') {
                                echo '&nbsp;<img src="res/imgs/tick.png">&nbsp;';
                            } else {
                                echo '&nbsp;<img src="res/imgs/empty.png">&nbsp;';
                            } ?></td>
                        <td valign="top" style="width: 90px;text-align: left;">GENEROUS DONATION</td>
                        <td valign="middle"
                            style="width: 10px;text-align: center;"><?php if ($data['donation'] == 'Standard Donation' || !$data['donation']) {
                                echo '&nbsp;<img src="res/imgs/tick.png">&nbsp;';
                            } else {
                                echo '&nbsp;<img src="res/imgs/empty.png">&nbsp;';
                            } ?></td>
                        <td valign="top" style="width: 100px;text-align: left;">STANDARD DONATION</td>
                        <td valign="middle"
                            style="width: 10px;text-align: center;"><?php if ($data['donation'] == 'One Meal Equivalent') {
                                echo '&nbsp;<img src="res/imgs/tick.png">&nbsp;';
                            } else {
                                echo '&nbsp;<img src="res/imgs/empty.png">&nbsp;';
                            } ?></td>
                        <td valign="top" style="width: 100px;text-align: left;">ONE MEAL EQUIVALENT</td>
                        <td valign="middle"
                            style="width: 10px;text-align: center;"><?php if ($data['donation'] == 'Token Donation') {
                                echo '&nbsp;<img src="res/imgs/tick.png">&nbsp;';
                            } else {
                                echo '&nbsp;<img src="res/imgs/empty.png">&nbsp;';
                            } ?></td>
                        <td valign="top" style="width: 80px;text-align: left;">TOKEN DONATION</td>
                        <td valign="middle"
                            style="width: 10px;text-align: center;"><?php if ($data['donation'] == 'No Special Donation') {
                                echo '<img src="res/imgs/tick.png">';
                            } else {
                                echo '<img src="res/imgs/empty.png"> ';
                            } ?></td>
                        <td valign="top" style="width: 100px;text-align: left;">NO SPECIAL RECOMMENDATION</td>
                    </tr>

                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3" valign="top" style="width: 280px;text-align: left; font-weight:bold;">References and
                Comments :
            </td>
        </tr>
        <tr>
            <td colspan="3" style="width: 485px;text-align:justify;">
                <table style="width: 400%;">
                    <tr>
                        <?php
                        $refComments = textLimit($data['referencecomments'], 890);

                        $order = array("\r\n", "\n", "\r");
                        $replace = '<br/>';
                        $refComments = str_replace($order, $replace, $refComments);

                        $refComments = hebrewWrap($refComments);
                        ?>
                        <td valign="top"
                            style="width: 420px; height:240px;  border-bottom:1px solid #000;text-align: justify;line-height:1.4;"><?php echo $refComments; ?>
                        </td>
                        <td valign="top" style="width:300px; height:100px;border:1px solid #FFFFFF;">
                            <?php
                            if ($data['photoname']) {
                                if (file_exists("../../" . $defaultPathPhoto . $data['photoname'])) {
                                    echo '<img src="../../' . $defaultPathPhoto . $data['photoname'] . '" width="330" height="239">';
                                }
                            } else {
                                echo '<div style="color: slategray; text-align:center; font-size: 20px"><br><br><br><br>Photo</div>';
                            } ?>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <?php

                $rabiname = $data['rabbis'];

                $exploded = explode(" ", $data['rabbis']);

                $rabsql = $dbh->prepare(
                    "
	  SELECT
	  *
	  FROM
	  `newmember`
	  WHERE `Firstname` = '" . $exploded[0] . "'
	  AND `Lastname` = '" . $exploded[1] . "'
	  "
                );
                $rabsql->execute();
                $rabdata = $rabsql->fetch();

                $rabisign = $rabdata['Id'];

                $sql1 = $dbh->prepare("select * from rabbissign where signid='$rabisign'");
                $sql1->execute();
                $data1 = $sql1->fetch();
                $signname = $data1['signname'];
                ?>
                <table style="width: 400%;">
                    <tr>
                        <?php
                        $rabbisName = '';

                        $loginemail = $_SESSION['user'];
                        $loginsql = $dbh->prepare("SELECT * FROM `newmember` WHERE emailid='$loginemail' AND 'Delete' = 0 ");
                        $loginsql->execute();
                        $logindata = $loginsql->fetch();
                        $r = $logindata['roles'];

                        if ($data1['signname'] && $data1['signid'] &&
                            file_exists("../../" . $defaultPathSign . $data1['signname'])
                        ) {
                            $rabbisName = $rabdata['Title'] . ' ' . $rabdata['Firstname'] . ' ' . $rabdata['Lastname'];
                        } elseif ($r == 'Manager') {
                            $rabbisName = $logindata['Title'] . ' ' .
                                $logindata['Firstname'] . ' ' . $logindata['Lastname'];
                        }
                        ?>
                        <td valign="bottom" style="width: 390px;text-align: left; font-weight:bold;">Signed by
                            Authorizing Rabbi : <?= $rabbisName; ?>
                        </td>
                        <td style="width: 260px; border-bottom:1px solid #000;">
                            <?php
                            if ($data1['signname'] && $data1['signid'] && file_exists("../../" . $defaultPathSign . $data1['signname'])) {
                                echo '<img style="width:110px;height:50px;" src="../../' . $defaultPathSign . $data1['signname'] . '">';
                            } elseif ($r == 'Manager') {
                                $id = $logindata['Id'];
                                $signName = $dbh->prepare("SELECT `signname` FROM `rabbissign` WHERE `signid` = '$id' AND `signid` != '';");
                                $signName->execute();
                                $sign = $signName->fetch();
                                if ($sign && file_exists("../../" . $defaultPathSign . $sign[0])) {
                                    echo '<br/><img src="../../' . $defaultPathSign . $sign[0] . '" border="0" width="110" height="50">';
                                } else {
                                    echo '<div style="width:260px; height:15px; color: red; font-size: 15px">
                                         Rabbi\'s signature is missing. Please check if he has one.</div>';
                                }
                            } else {
                                echo '<div style="width:260px; height:15px; color: red; font-size: 15px">
                                         Rabbi\'s signature is missing. Please check if he has one.</div>';
                            }
                            ?>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3">

                <table style="width: 400%;">
                    <tr>
                        <td valign="top" style="width: 650px;text-align: left;line-height:1.4;">
                            Note: The following
                            vital information is the only reliable means of accessing interstate and international
                            records. If available, please record:
                        </td>

                    </tr>
                </table>
            </td>
        </tr>
        
        <tr>
            <td colspan="3">
                <table style="width: 400%;">
                    <tr>
                        <td valign="top" style="width: 60px;text-align: left;">Id details:</td>
                        <td valign="top"
                            style="padding-bottom:-80px; width: 650px; text-align: left; border-bottom:1px solid #000;">
                            <?php
                            $sql = $dbh->prepare('SELECT *
                                                  FROM `visit_details`
                                                  WHERE `visit_id` = :visitid
                                                  AND `detail_type` = :detailtype
                                                  AND `delete` = 0 ;');

                            $sql->execute(['visitid' => $data['Id'], 'detailtype' => 'identification']);
                            $collident = $sql->fetchAll();
                            $details = [];
                            $country = '';
                            foreach ($collident as $ident) {
                                if ($ident['type'] == 'SSN / ID Number') {
                                    $ident['type'] = 'SSN';
                                }
                                $details[] = $ident['type'] . ': ' . $ident['detail_number'];

                                if ($ident['primary_number']) {
                                    $country = $ident['country'];
                                }
                            }
                            if (count($details) <= 4) {
                                echo implode(' / ', $details);
                                echo '</td></tr>';
                            } else {
                            $firstLine[] = $details[0];
                            $firstLine[] = $details[1];
                            $firstLine[] = $details[2];
                            $firstLine[] = $details[3];

                            echo implode(' / ', $firstLine);
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" colspan="2"
                            style="padding-top: 5px; padding-bottom: 0px; width: 700px;text-align: left;border-bottom:1px solid #000;">
                            <?php
                            $i = count($details);
                            for ($c = 0; $c < $i; $c++) {
                                if ($c > 3) {
                                    $secondLine[] = $details[$c];
                                }
                            }
                            if (count($secondLine) > 1) {
                                echo implode(' / ', $secondLine);
                            } else {
                                echo $secondLine[0];
                            }
                            ?>
                        </td>
                    </tr>
                    <?php } ?>

                </table>
                <br>
                <table style="width: 400%">
                    <tr>
                        <td valign="top" style="width: 90px;text-align: left;line-height:1.4;">State/Province:</td>
                        <td valign="top"
                            style="width:250px;text-align: left;line-height:1.4;border-bottom:1px solid #000;"><?php
                            if ($data['statecountry']) {
                                echo $data['statecountry'];
                            } ?></td>
                        <td style="width:0px;"></td>
                        <td valign="top" style="width: 50px;text-align: left;line-height:1.4;">Country:</td>
                        <td valign="top"
                            style="width: 305px;text-align: left;line-height:1.4;border-bottom:1px solid #000;"><?= $country ?> </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <img src="res/imgs/emvs.png"/>
    <br> <br>
</page>
