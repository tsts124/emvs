<?php
$isSearch = $_POST['isSearch'];
if ($isSearch == 'RefId') {
    header('Content-Type: application/json');
    include 'includes/dbcon.php';
    $selsql = $dbh->prepare('SELECT *
                             FROM `visitstable`
                             WHERE `Refid` = :refid ; ');
    $selsql->execute(['refid' => $_POST['refVal']]);
    $deldata = $selsql->fetch();
    if ($deldata) {
        $url = "emvs.php?action=editvisit1&&a={$deldata['collectorsid']}&&b={$deldata['visitid']}";
        http_response_code('200');
        echo json_encode(array('urlRef' => $url));
        exit;
    }
}
?>
<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/jquery.SimpleMask.js"></script>
<script>
    $(document).ready(function () {

        $('#phn_format').simpleMask({'mask': 'usa', 'nextInput': false});
        $('#phn_format').focus();

    });
    function phn_num(val) {

        if (val.length == 10) {
            document.getElementById('go').disabled = true;
        } else {
            document.getElementById('go').disabled = false;
        }
    }
</script>
<?php
$seacrh_val = $_POST['seacrh_val'];
$search = $_POST['search'];
if ($search == 'Phone Number') {
    echo '<input autocorrect="off"  name="search_name" id="phn_format" value="' . $seacrh_val .
        '" onkeyup="phn_num(this.value)" type="text" class="form-control" required>';
} else {
    echo '<input autocorrect="off"  name="search_name" id="search_name" type="text" value="' . $seacrh_val .
        '" class="form-control" required>';
}
?>

