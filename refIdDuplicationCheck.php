<?php
include 'includes/dbcon.php';

$refId = $_POST['refId'];
$prevId = $_POST['prevId'];

$sql = $dbh->prepare("SELECT *
                      FROM `visitstable`
                      WHERE `Refid` = :refId
                      AND `Refid` != :prevId; ");
$sql->execute([':refId' => $refId, ':prevId' => $prevId]);

if ($sql->fetch()) {
    echo 'duplication';
} else {
    echo 'ok';
}
