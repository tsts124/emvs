<?php

class ImportHelper
{

    private static $tempRow = [];
    private static $collidentFlag = null;
    private static $visitstableFlag = null;
    private static $tempCollidentRow = [];
    private static $tempDublicateCollidentRow = [];
    private static $tempDublicateVisitstableRow = [];

    public static function readDocument($pathToFile)
    {
        set_time_limit(0);

        /** Include PHPExcel_IOFactory */
        require_once dirname(__FILE__) . '/phpexcel/Classes/PHPExcel/IOFactory.php';

        //  Read your Excel workbook
        try {
            $inputFileType = PHPExcel_IOFactory::identify($pathToFile);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($pathToFile);
        } catch (Exception $e) {
            die('Error loading file "' . pathinfo($pathToFile, PATHINFO_BASENAME) . '": ' . $e->getMessage());
        }

        $data = [];
        /* @var $worksheet PHPExcel_Worksheet */
        foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
            $worksheetTitle = $worksheet->getTitle();
            if ($worksheetTitle === 'reference') {
                continue; // skip reading reference sheet
            }
            $highestRow = $worksheet->getHighestRow(); // e.g. 10
            $highestColumn = $worksheet->getHighestColumn(); // e.g 'F'

            $sheetData = $worksheet->rangeToArray('A1:' . $highestColumn . $highestRow, '', TRUE, FALSE);
            $cols = $sheetData[0];
            unset($sheetData[0]);
            foreach ($sheetData as $n => $rowData) {
                $rowDataAssoc = array_combine($cols, $rowData);
                $CollectorID = $worksheet->getCell('A' . ($n + 1))->getCalculatedValue();
                unset($rowDataAssoc['']);
                if (!empty($rowDataAssoc['CollectorID'])) {
                    $data[$CollectorID][$worksheetTitle][$n + 1] = $rowDataAssoc;
                }
            }
        }

        return $data;
    }

    public static function printErrors($errors)
    {
        echo "<div class='alert alert-danger' style='margin-top: 50px; margin-left: 1%;'>";
        echo "<h2>Can not import because the document contains errors</h2>";
        echo "<h3>Correct the error, and load the document again</h3>";
        echo "<table class='table'>";
        echo "<tr><th>Sheet</th><th>Error desc</th></tr>";
        foreach ($errors as $sheet => $errorList) {
            foreach ($errorList as $error) {
                echo "<tr><td>{$sheet}</td><td>{$error}</td></tr>";
            }
        }
        echo "</table>";
        echo "</div>";
    }

    public static function printDocument($pathToFile)
    {

        /** Include PHPExcel_IOFactory */
        require_once dirname(__FILE__) . '/phpexcel/Classes/PHPExcel/IOFactory.php';

        //  Read your Excel workbook
        try {
            $inputFileType = PHPExcel_IOFactory::identify($pathToFile);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($pathToFile);
        } catch (Exception $e) {
            die('Error loading file "' . pathinfo($pathToFile, PATHINFO_BASENAME) . '": ' . $e->getMessage());
        }

        foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
            $worksheetTitle = $worksheet->getTitle();
            if ($worksheetTitle === 'reference') {
                continue; // skip reading reference sheet
            }

            $highestRow = $worksheet->getHighestRow(); // e.g. 10
            $highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
            $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
            $nrColumns = ord($highestColumn) - 64;


            echo "<br>The worksheet <b>" . $worksheetTitle . "</b> has ";
            echo $nrColumns . ' columns (A-' . $highestColumn . ') ';
            echo ' and ' . $highestRow . ' row.';
            echo '<br>Data: <table class="table"><tr>';

            for ($row = 1; $row <= $highestRow; ++$row) {
                if (empty($worksheet->getCellByColumnAndRow(0, $row)->getCalculatedValue())) {
                    continue;
                }
                echo '<tr>';
                for ($col = 0; $col < $highestColumnIndex; ++$col) {
                    $cell = $worksheet->getCellByColumnAndRow($col, $row);
                    $val = $cell->getValue();
                    $dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
                    if ($dataType === 'f') {
                        $val = $cell->getCalculatedValue();
                    }
                    #echo '<td>' . $val . '[' . $dataType . ']' . '</td>';
                    echo '<td>' . $val . '</td>';
                }
                echo '</tr>';
            }
            echo '</table>';
        }
    }

    public static function validateData(&$documentData)
    {
        set_time_limit(0);

        $required = [
            'collectors' => ['firstname', 'lastname', 'dob'],
            'collectorphone' => ['Phone_Type', 'Phone_Country', 'Phone_Number'],
            'collident' => ['Collidenti', 'Collcountry', 'Collphone'],
            'visitstable' => ['Refid'],
        ];
        $required_date = [
            'collectors' => ['dob' => 'MM-DD-YYYY'],
            'collectorphone' => [],
            'collident' => [],
            'visitstable' => [
                'regdate' => 'MM-DD-YYYY',
                'authofromdate' => 'MM-DD-YYYY',
                'authotodate' => 'MM-DD-YYYY'
            ]
        ];

        $fundsreference = ['Documents', 'Identification', 'References', 'Other'];
        $purpose = [
            'Yeshiva', 'Mental', 'Parnosso',
            'Judaism', 'Kiruv', 'Torah',
            'Debts', 'Adult', 'Free Loan',
            'Kollel', 'Girls', 'Chinuch',
            'Hachnosas', 'Charity', 'Sefer',
            'Other', 'Medical', 'Business',
            'Chesed'
        ];
        $required_format = [
            'collectors' => [],
            'collectorphone' => ['Phone_Number' => '/[0-9]*/'],
            'collident' => [],
            'visitstable' => ['purpose' => $purpose, 'fundsreference' => $fundsreference]
        ];
        $errors = [];

        $startTime = microtime(true);

        foreach ($documentData as $CollectorID => $data) { // by all document rows
            if (isset($data['collectors']) && count($data['collectors']) > 1) {
                $errors['collectors'][] = 'Duplication of collectors on the sheet with the ID=' . $CollectorID;
            }
            error_log(var_export([1, microtime(true) - $startTime], true));

            foreach ($required as $sheet => $fields) { // by all required fields
                if (isset($data[$sheet]) && count($data[$sheet]) > 0) {
                    foreach ($fields as $field) { // by all required fields in current sheet
                        foreach ($data[$sheet] as $row => &$values) {
                            if (!isset($values[$field]) || empty($values[$field])) {
                                $errors[$sheet][] = "Field <b>{$field}</b> cannot be empty at row {$row}";
                            }
                        }
                    }
                }
            }

            error_log(var_export([2, microtime(true) - $startTime], true));

            foreach ($required_date as $sheet => $fields) {
                if (isset($data[$sheet]) && count($data[$sheet]) >= 1) {
                    foreach ($fields as $field => $format) {
                        foreach ($data[$sheet] as $row => &$values) {
                            $result = ImportHelper::checkDate(@$values[$field], $format);
                            if (!isset($values[$field]) || !$result) {
                                $errors[$sheet][] = "Wrong format in field <b>{$field}</b> at row {$row}";
                            } else {
                                // parsing date from Excel to string
                                $documentData[$CollectorID][$sheet][$row][$field] = $result;
                            }
                        }
                    }
                }
            }

            error_log(var_export([3, microtime(true) - $startTime], true));

            foreach ($required_format as $sheet => $fields) {
                if (isset($data[$sheet]) && count($data[$sheet]) > 0) {
                    foreach ($fields as $field => $format) {
                        foreach ($data[$sheet] as $row => &$values) {
                            switch ($field) {
                                case 'Phone_Number':
                                    $check = preg_match($format, $values[$field]);
                                    if (!$check) {
                                        $errors[$sheet][] = "Wrong format in field <b>{$field}</b> at row {$row}";
                                    }
                                    break;
                                case 'purpose':
                                    if (!$values[$field]) {
                                        break;
                                    }
                                    if (strripos($values[$field], ',') !== false) {
                                        $purposes = explode(',', $values[$field]);
                                        foreach ($purposes as $key => $val) {
                                            $purposes[$key] = trim($val);
                                        }
                                    } else {
                                        $purposes[] = trim($values[$field]);
                                    }
                                    foreach ($purposes as $number => $val) {
                                        if (!in_array($val, $format)) {
                                            $number++;
                                            $errors[$sheet][] = "Wrong format of entry <b>{$number}</b> in field <b>{$field}</b> at row {$row}";
                                        }
                                    }
                                    break;
                                case 'fundsreference':
                                    if (!$values[$field]) {
                                        break;
                                    }
                                    if (is_array($values[$field])) {
                                        $fundsReferences = explode(',', $values[$field]);
                                        foreach ($fundsReferences as $number => $val) {
                                            if (!in_array($val, $format)) {
                                                $number++;
                                                $errors[$sheet][] = "Wrong format of entry <b>{$number}</b> in field <b>{$field}</b> at row {$row}";
                                            }
                                        }
                                    } else {
                                        //if(!in_array($values[$field], $format)){
                                        //   $errors[$sheet][] = "Wrong format of entry in field <b>{$field}</b> at row {$row}";
                                        //}
                                    }
                                    break;
                                default:
                                    break;
                            };
                        }
                    }
                }
            }

            error_log(var_export([4, microtime(true) - $startTime], true));
        }
        return $errors;
    }

    public static function checkDate($date, $format)
    {
        if ($date != (int)$date) {
            return FALSE;
        } else {
            $date = PHPExcel_Style_NumberFormat::toFormattedString($date, $format);
            return $date;
        }
    }

    public static function checkCollectorID($CollectorID)
    {
        /* @var $dbh PDO */
        include 'includes/dbcon.php';
        $query = "SELECT collectors.* FROM collectors WHERE concat(firstname, lastname, dob) = '{$CollectorID}'";
        /* @var $sth PDOStatement */
        $sth = $dbh->prepare($query);
        $sth->execute();
        $result = $sth->rowCount();
        error_log($CollectorID . ' ' . var_export($result, TRUE));
        return $result;
    }

    public static function findColectorByID($CollectorID, $idOnly = true)
    {
        if (!in_array($CollectorID, array_keys(self::$tempRow)) || self::$tempRow[$CollectorID] == false) {
            /* @var $dbh PDO */
            include 'includes/dbcon.php';
            $query = "SELECT * FROM collectors WHERE concat(firstname, lastname, dob) = '{$CollectorID}'";
            /* @var $sth PDOStatement */
            $sth = $dbh->prepare($query);
            $sth->execute();
            $result = $sth->fetch(PDO::FETCH_ASSOC);

            self::$tempRow[$CollectorID] = $result;
        }
        if ($idOnly && isset($result['Id'])) {
            return self::$tempRow[$CollectorID]['Id'];
        } else {
            return self::$tempRow[$CollectorID];
        }
    }

    private static function scanAddCollIdForDubs($documentData)
    {
        $dubs = [];
        foreach ($documentData as $collID => $data) {
            if (!(isset($data['collident']) && count($data['collident']))) {
                continue;
            }
            unset($documentData[$collID]); // unsetting one we are checking with
            foreach ($data['collident'] as $ident) {
                $result = self::scanOneCollIdForDubs($ident, $documentData);
                if ($result) {
                    $dubs = array_merge($dubs, $result);
                }
            }
        }
        if (count($dubs)) {
            return $dubs;
        }
        return false;
    }

    private static function scanOneCollIdForDubs($mainIdent, $documentData)
    {
        $dubKey = $mainIdent['Collidenti'] . ': ' . $mainIdent['Collphone'];
        $dubs = [$dubKey => [$mainIdent['CollectorID']]];
        foreach ($documentData as $collID => $data) {
            if (!(isset($data['collident']) && count($data['collident']))) {
                continue;
            }
            foreach ($data['collident'] as $ident) {
                if ($mainIdent['Collidenti'] == $ident['Collidenti'] AND $mainIdent['Collphone'] == $ident['Collphone']) {
                    $dubs[$dubKey][] = $collID;
                }
            }
        }
        if (count($dubs[$dubKey]) > 1) {
            return $dubs;
        }
        return false;
    }

    private static function outputDubsIdData($dubs)
    {
        $timeStamp = date("d-m-Y H:i:s");
        $file = fopen('./error.log', 'w');
        foreach ($dubs as $dub => $collIds) {
            $output = "Duplicated ID '$dub' in following CollectorIDs: ";

            foreach ($collIds as $collId) {
                $output .= $collId . ', ';
            }
            $output = trim($output, ' ,');
            fwrite($file, "[" . $timeStamp . "] " . $output . "\n");
            echo "<div class='alert alert-warning' style='margin-top: 50px; margin-left: 1%;'>" . $output . '</div>';
        }
        fclose($file);
    }

    public static function importData($documentData)
    {
        set_time_limit(0);

        /* @var $dbh PDO */
        include __DIR__ . '/includes/dbcon.php';

        $dubs = self::scanAddCollIdForDubs($documentData);
        if ($dubs) {
            self::outputDubsIdData($dubs);
        }

        try {
            $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $spreadsheetLine = 1;
            foreach ($documentData as $CollectorID => $data) {
                $spreadsheetLine++;
                if (isset($data['collectors']) && count($data['collectors']) == 1) {
                    $collectorsId = ImportHelper::findColectorByID($CollectorID);

                    $collectors = array_pop($data['collectors']);
                    unset($collectors['CollectorID']);

                    if ($collectorsId) {
                        $fieldsparams = [];
                        foreach (array_keys($collectors) as $key) {
                            $fieldsparams[] = "{$key}=:{$key}";
                        }
                        $collectors = self::prepare($collectors);
                        $fieldsparams = implode(',', $fieldsparams);
                        $query = "UPDATE collectors SET {$fieldsparams} WHERE id=" . $collectorsId;
                    } else {
                        $fields = implode(',', array_keys($collectors));
                        $collectors = self::prepare($collectors);
                        $params = implode(',', array_keys($collectors));

                        $query = "INSERT INTO collectors({$fields},Addeddate) VALUES ({$params},now())";
                    }
                    /* @var $sth PDOStatement */
                    $sth = $dbh->prepare($query);
                    $sth->execute($collectors);
                    if ($sth->errorCode() !== PDO::ERR_NONE) {
                        print_r($sth->errorInfo());
                    }
                }

                $dublicatedDataFields = [
                    'title',
                    'firstname',
                    'middlename',
                    'lastname',
                    'city',
                    'statecountry',
                    'zipcode',
                    'address1',
                    'address2',
                    'dob'
                ];
                $collectorinfo = ImportHelper::findColectorByID($CollectorID, false);
                $collectorsId = $collectorinfo['Id'];

                if (isset($data['collectorphone']) && count($data['collectorphone'])) {
                    foreach ($data['collectorphone'] as $collectorphone) {
                        unset($collectorphone['CollectorID']);
                        $collectorphone['collid'] = $collectorsId;
                        $fields = implode(',', array_keys($collectorphone));
                        if ($collectorphone['CPrimary']) {
                            $collectorphone['CPrimary'] = $collectorsId;

                            $sql = $dbh->prepare('UPDATE `collectorphone`
                                                  SET `CPrimary` = :cprimary
                                                  WHERE `collid` = :collid ;');
                            $sql->execute([
                                'cprimary' => '',
                                'collid' => $collectorsId
                            ]);
                        }

                        $collectorphone = self::prepare($collectorphone);
                        $params = implode(',', array_keys($collectorphone));
                        $query = "INSERT INTO collectorphone({$fields}) VALUES ({$params})";
                        $sth = $dbh->prepare($query);
                        $sth->execute($collectorphone);
                        if ($sth->errorCode() !== PDO::ERR_NONE) {
                            print_r($sth->errorInfo());
                        }
                    }
                }
                if (isset($data['collident']) && count($data['collident'])) {
                    self::fillCollidentTemp();
                    foreach ($data['collident'] as $collident) {
                        $key = $collident['CollectorID'] . '+' . $collident['Collidenti'] . '+' . $collident['Collphone'];
                        if (!isset(self::$tempCollidentRow[$key])) {
                            self::$tempCollidentRow[$key] = $collident;
                            unset($collident['CollectorID']);
                            $collident['Collid'] = $collectorsId;
                            $fields = implode(',', array_keys($collident));
                            if ($collident['Primary1']) {
                                $collident['Primary1'] = $collectorsId;

                                $sql = $dbh->prepare('UPDATE `collident`
                                                  SET `Primary1` = :primary1
                                                  WHERE `Collid` = :collid ;');
                                $sql->execute([
                                    'primary1' => '',
                                    'collid' => $collectorsId
                                ]);
                            }

                            $collident = self::prepare($collident);
                            $params = implode(',', array_keys($collident));
                            $query = "INSERT INTO collident({$fields},Addeddate) VALUES ({$params},now())";
                            $sth = $dbh->prepare($query);
                            $sth->execute($collident);
                            if ($sth->errorCode() !== PDO::ERR_NONE) {
                                print_r($sth->errorInfo());
                            }
                        } else {
                            self::$tempDublicateCollidentRow[$key] = $collident;
                        }
                    }
                }
                if (isset($data['visitstable']) && count($data['visitstable'])) {
                    self::fillVisitableTemp();
                    foreach ($data['visitstable'] as $visitstable) {
                        $key = $visitstable['CollectorID'] . '+' . $visitstable['Refid'] . '+' . $visitstable['sites'];
                        if (!isset(self::$tempDublicateVisitstableRow[$key])) {
                            self::$tempDublicateVisitstableRow[$key] = $visitstable;
                            $sth = $dbh->prepare("SELECT MAX(visitstable.visitid) FROM visitstable WHERE visitstable.collectorsid=" . $collectorsId);
                            $sth->execute();
                            $visitid = $sth->fetchColumn() + 1;

                            $sth = $dbh->prepare("SELECT id FROM `sites` WHERE sites='{$visitstable['sites']}'");
                            $sth->execute();
                            $sites = $sth->fetchColumn();

                            unset($visitstable['CollectorID']);
                            $visitstable['collectorsid'] = $collectorsId;
                            $visitstable['user'] = isset($_SESSION['user']) ? $_SESSION['user'] : 'admin@gmail.com';
                            $visitstable['visitid'] = $visitid;
                            $visitstable['sites'] = $sites;

                            // Filling collectors info from Collectors table
                            $visitstable['title'] = $collectorinfo['title'];
                            $visitstable['firstname'] = $collectorinfo['firstname'];
                            $visitstable['middlename'] = $collectorinfo['middlename'];
                            $visitstable['lastname'] = $collectorinfo['lastname'];
                            $visitstable['city'] = $collectorinfo['city'];
                            $visitstable['statecountry'] = $collectorinfo['statecountry'];
                            $visitstable['country'] = $collectorinfo['country'];
                            $visitstable['zipcode'] = $collectorinfo['zipcode'];
                            $visitstable['address1'] = $collectorinfo['address1'];
                            $visitstable['address2'] = $collectorinfo['address2'];
                            $visitstable['dob'] = $collectorinfo['dob'];

                            foreach ($dublicatedDataFields as $field) {
                                if (empty($visitstable[$field])) {
                                    $visitstable[$field] = $collectorinfo[$field];
                                }
                            }
                            $fields = implode(',', array_keys($visitstable));

                            $visitstable = self::prepare($visitstable);
                            $params = implode(',', array_keys($visitstable));
                            $query = "INSERT INTO visitstable({$fields},Addeddate) VALUES ({$params},now())";
                            $sth = $dbh->prepare($query);
                            $sth->execute($visitstable);

                            $sth = $dbh->prepare('SELECT *
                                                  FROM `visitstable`
                                                  ORDER BY `Id`
                                                  DESC');
                            $sth->execute();
                            $visitId = $sth->fetch()['Id'];

                            $sth = $dbh->prepare('SELECT *
                                                  FROM `collident`
                                                  WHERE `Collid` = :collid
                                                  AND `delete` = 0 ;');
                            $sth->execute(['collid' => $collectorsId]);
                            $identDetails = $sth->fetchAll();

                            $sth = $dbh->prepare('SELECT *
                                                  FROM `collectorphone`
                                                  WHERE `collid` = :collid
                                                  AND `delete` = 0 ;');
                            $sth->execute(['collid' => $collectorsId]);
                            $contactDetails = $sth->fetchAll();

                            foreach ($identDetails as $detail) {
                                $sth = $dbh->prepare('INSERT INTO `visit_details` (`visit_id`,
                                                                                   `detail_type`,
                                                                                   `primary_number`,
                                                                                   `type`,
                                                                                   `country`,
                                                                                   `detail_number`,
                                                                                   `passport_name`)
                              VALUES (:visit_id,
                                      :detail_type,
                                      :primary_number,
                                      :number_type,
                                      :country,
                                      :detail_number,
                                      :passport_name) ; ');
                                $sth->execute([
                                    'visit_id' => $visitId,
                                    'detail_type' => 'identification',
                                    'primary_number' => $detail['Primary1'],
                                    'number_type' => $detail['Collidenti'],
                                    'country' => $detail['Collcountry'],
                                    'detail_number' => $detail['Collphone'],
                                    'passport_name' => $detail['passport_name']
                                ]);
                            }

                            foreach ($contactDetails as $detail) {
                                $sth = $dbh->prepare('INSERT INTO `visit_details` (`visit_id`,
                                                               `detail_type`,
                                                               `primary_number`,
                                                               `type`,
                                                               `country`,
                                                               `detail_number`)
                                  VALUES (:visit_id,
                                          :detail_type,
                                          :primary_number,
                                          :number_type,
                                          :country,
                                          :detail_number) ; ');
                                $sth->execute([
                                    'visit_id' => $visitId,
                                    'detail_type' => 'contact',
                                    'primary_number' => $detail['CPrimary'],
                                    'number_type' => $detail['Phone_Type'],
                                    'country' => $detail['Phone_Country'],
                                    'detail_number' => $detail['Phone_Number']
                                ]);
                            }

                            if ($sth->errorCode() !== PDO::ERR_NONE) {
                                print_r($sth->errorInfo());
                            }
                        }
                    }
                }
            }

            echo "<div class='alert alert-success' style='margin-top: 50px; margin-left: 1%;'><h2>The data was successfully loaded</h2></div>";
        } catch (Exception $e) {

            echo "<div class='alert alert-danger'  style='margin-top: 50px; margin-left: 1%;'>" . $e->getMessage() . "<br/><br/>CollectorID:" . $CollectorID . "<br/>spreadsheet line: " . $spreadsheetLine . "</div>";
        }
    }

    private static function fillCollidentTemp()
    {
        if (is_null(self::$collidentFlag)) {
            $query = 'SELECT concat(collectors.firstname, collectors.lastname, collectors.dob) CollectorID, c.Primary1, c.Collidenti, c.Collcountry, c.Collphone
                    FROM collident as c
                    LEFT JOIN collectors ON c.Collid = collectors.Id';
            /* @var $dbh PDO */
            include 'includes/dbcon.php';
            $sth = $dbh->prepare($query);
            $sth->execute();
            $result = $sth->fetchAll(PDO::FETCH_ASSOC);
            self::$collidentFlag = $result;

            foreach ($result as $val) {
                $key = $val['CollectorID'] . '+' . $val['Collidenti'] . '+' . $val['Collphone'];
                self::$tempCollidentRow[$key] = $val;
            }
        }
    }

    private static function fillVisitableTemp()
    {
        if (is_null(self::$visitstableFlag)) {
            $query = 'SELECT s.Sites,concat(collectors.firstname, collectors.lastname, collectors.dob) CollectorID, v.* FROM visitstable as v
                          LEFT JOIN collectors ON v.collectorsid = collectors.Id
                          LEFT JOIN sites s on v.sites = s.Id';
            /* @var $dbh PDO */
            include 'includes/dbcon.php';
            $sth = $dbh->prepare($query);
            $sth->execute();
            $result = $sth->fetchAll(PDO::FETCH_ASSOC);
            self::$visitstableFlag = $result;
            foreach ($result as $val) {
                $key = $val['CollectorID'] . '+' . $val['Refid'] . '+' . $val['Sites'];
                self::$tempDublicateVisitstableRow[$key] = $val;
            }
        }
    }

    private static function prepare($array)
    {
        foreach ($array as $key => $val) {
            $array[':' . $key] = $val;
            unset($array[$key]);
        }
        return $array;
    }

}
