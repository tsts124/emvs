<?php
include 'includes/function.php';
include 'includes/dbcon.php';

$query = $dbh->prepare(
    "
		SELECT * FROM `app_options` WHERE `key` IN('show_site', 'default_path_sign', 'default_site')
		  "
);
$query->execute();
$arr = $query->fetchAll();
$defaultPathSign = $arr[2];//default_path_sign
$ishowsite = $arr[0]['value'];// show_site
$defaultSite = $arr[1]['value']; //default_site
$defaultPathSign = ($defaultPathSign['value'] == '') ? "uploads/" : $defaultPathSign['value'];

$query = $dbh->prepare(
    "
  	SELECT
  	MAX(`newmember`.`id`) AS `value`
  	FROM
  	`newmember`
  "
);
$query->execute();
$nextMember = $query->fetch();


if ($_SESSION['user'] == '') {
    header('Location: emvs.php?action=index');
}
$query = $dbh->prepare('SELECT `Sites`
                        FROM `newmember`
                        WHERE `emailid` = :id');
$query->execute(['id' => $_SESSION['user']]);
$defaultSitesAdmin = $query->fetch();

if (isset($_POST['createuser'])) {

    $userroles = addslashes(htmlspecialchars($_POST['userroles']));
    $title = $_POST['title'];
    $firstname = $_POST['firstname'];
    $lastname = $_POST['lastname'];
    $emailid = $_POST['emailid'];
    $status = addslashes(htmlspecialchars($_POST['status']));
    $pass1 = $_POST['password1'];
    $password = mc_encrypt($pass1, ENCRYPTION_KEY);
    $sites = addslashes(htmlspecialchars($_POST['sites']));

    $usercomments = $_POST['usercomments'];

    $today = date('YmdHi');
    $startDate = date('YmdHi', strtotime('2012-03-14 09:06:00'));
    $range = $today - $startDate;
    $rand = rand(126234, $range);
    $a1 = "$rand" . ($startDate + $rand);
    $b = 'abcdef' . 'UVWXYZ';
    $d = $a1 . $b;
    $c = str_shuffle($d);
    $arr = str_split($c, 6);
    $userid = implode("-", $arr);

    $count = $dbh->prepare('INSERT INTO `newmember`(`Sites`, `Firstname`,
                                                    `Lastname`, `password`, `emailid`,
                                                    `userid`, `roles`, `status`,
                                                    `Addeddate`, `Delete`, `comments`,
                                                    `Title`) 
                            VALUES (:sites, :firstname,
                                    :lastname, :password, :email,
                                    :userid, :role, :status,
                                    now(), :deleted, :comments,
                                    :title) ;');
    $count->execute([
        'sites' => $sites,
        'firstname' => $firstname,
        'lastname' => $lastname,
        'password' => $password,
        'email' => $emailid,
        'userid' => $emailid,
        'role' => $userroles,
        'status' => $status,
        'deleted' => '0',
        'comments' => $usercomments,
        'title' => $title
    ]);

    header('Location: emvs.php?action=manager');
}

$a = $_GET['a'];
if ($a) {
    $sqlquery = $dbh->prepare('SELECT *
                               FROM `newmember`
                               WHERE `emailid` = :id ');
    $sqlquery->execute(['id' => $a]);
    $dataquery = $sqlquery->fetch();
}

if (isset($_POST['updatemember'])) {
    $userid = addslashes(htmlspecialchars($_POST['userid']));
    $title = $_POST['title'];
    $firstname = $_POST['firstname'];
    $lastname = $_POST['lastname'];
    $emailid = $_POST['emailid'];
    $status = addslashes(htmlspecialchars($_POST['status']));
    $pass1 = $_POST['password2'];
    $password = mc_encrypt($pass1, ENCRYPTION_KEY);
    $usercomments = $_POST['usercomments'];
    $userroles = addslashes(htmlspecialchars($_POST['userroles']));
    $sites = addslashes(htmlspecialchars($_POST['sites']));
    $a = $_POST['useridd'];

    $count = $dbh->prepare('UPDATE `newmember`
                            SET `Sites` = :sites,
                                `Firstname` = :firstname,
                                `Lastname` = :lastname,
                                `emailid` = :email,
                                `roles` = :role,
                                `status` = :status,
                                `comments` = :comments,
                                `Title` = :title
                            WHERE `Id` = :userid ; ');
    $count->execute([
        'sites' => $sites,
        'firstname' => $firstname,
        'lastname' => $lastname,
        'email' => $emailid,
        'userid' => $a,
        'role' => $userroles,
        'status' => $status,
        'comments' => $usercomments,
        'title' => $title
    ]);

    if ($pass1) {

        $sql2 = $dbh->prepare('SELECT *
                               FROM `newmember`
                               WHERE `emailid` = :email ;');
        $sql2->execute(['email' => $a]);
        $data2 = $sql2->fetch();

        $pl = $data2['pwdcount'] + 1;
        $password = mc_encrypt($pass1, ENCRYPTION_KEY);

        $count1 = $dbh->prepare('UPDATE `newmember`
                                 SET `password` = :password,
                                     `passwordchanged` = now(),
                                     `pwdcount` = :passcount
                                 WHERE `Id` = :userid ; ');
        $count1->execute([
            'password' => $password,
            'passcount' => $pl,
            'userid' => $a
        ]);
    }

    header('Location: emvs.php?action=manager');
}

include 'includes/header.php';

?>


<style type="text/css">
    .step_box {
        border: 1.0px solid rgb(247, 247, 247);
    }

    .step_box:hover, #selected_step_box, .QuickStartLong:hover {
        background: rgb(247, 247, 247);
    }

    .selected {
        background-color: #ddd;
    }
</style>


<script>

    function signatureCapture1() {

        var canvas = document.getElementById("newSignature1");
        var ctx = canvas.getContext("2d");
        ctx.strokeStyle = "#000000";
        ctx.fillStyle = "#000000";
        ctx.lineWidth = 2;
        ctx.lineCap = "round";

        // Set up mouse events for drawing
        var drawing = false;
        var mousePos = {x: 0, y: 0};
        var lastPos = mousePos;
        canvas.addEventListener("mousedown", function (e) {
            drawing = true;
            lastPos = getMousePos(canvas, e);
        }, false);
        canvas.addEventListener("mouseup", function (e) {
            drawing = false;
        }, false);
        canvas.addEventListener("mousemove", function (e) {
            mousePos = getMousePos(canvas, e);
        }, false);

        // Get the position of the mouse relative to the canvas
        function getMousePos(canvasDom, mouseEvent) {
            var rect = canvasDom.getBoundingClientRect();
            return {
                x: mouseEvent.clientX - rect.left,
                y: mouseEvent.clientY - rect.top
            };
        }

        // Get a regular interval for drawing to the screen
        window.requestAnimFrame = (function (callback) {
            return window.requestAnimationFrame ||
                window.webkitRequestAnimationFrame ||
                window.mozRequestAnimationFrame ||
                window.oRequestAnimationFrame ||
                window.msRequestAnimaitonFrame ||
                function (callback) {
                    window.setTimeout(callback, 1000 / 60);
                };
        })();

        // Draw to the canvas
        function renderCanvas() {
            if (drawing) {
                ctx.moveTo(lastPos.x, lastPos.y);
                ctx.lineTo(mousePos.x, mousePos.y);
                ctx.stroke();
                lastPos = mousePos;
            }
        }

        // Allow for animation
        (function drawLoop() {
            requestAnimFrame(drawLoop);
            renderCanvas();
        })();

        // Set up touch events for mobile, etc
        canvas.addEventListener("touchstart", function (e) {
            mousePos = getTouchPos(canvas, e);
            var touch = e.touches[0];
            var mouseEvent = new MouseEvent("mousedown", {
                clientX: touch.clientX,
                clientY: touch.clientY
            });
            canvas.dispatchEvent(mouseEvent);
        }, false);
        canvas.addEventListener("touchend", function (e) {
            var mouseEvent = new MouseEvent("mouseup", {});
            canvas.dispatchEvent(mouseEvent);
        }, false);
        canvas.addEventListener("touchmove", function (e) {
            var touch = e.touches[0];
            var mouseEvent = new MouseEvent("mousemove", {
                clientX: touch.clientX,
                clientY: touch.clientY
            });
            canvas.dispatchEvent(mouseEvent);
        }, false);

        // Get the position of a touch relative to the canvas
        function getTouchPos(canvasDom, touchEvent) {
            var rect = canvasDom.getBoundingClientRect();
            return {
                x: touchEvent.touches[0].clientX - rect.left,
                y: touchEvent.touches[0].clientY - rect.top
            };
        }

        // Prevent scrolling when touching the canvas
        document.body.addEventListener("touchstart", function (e) {
            if (e.target == canvas) {
                e.preventDefault();
            }
        }, false);
        document.body.addEventListener("touchend", function (e) {
            if (e.target == canvas) {
                e.preventDefault();
            }
        }, false);
        document.body.addEventListener("touchmove", function (e) {
            if (e.target == canvas) {
                e.preventDefault();
            }
        }, false);
    }

    function signatureSave1(a) {

        var canvas = document.getElementById("newSignature1");// save canvas image as data url (png format by default)
        var dataURL = canvas.toDataURL("image/png");
        var a = a;

        document.getElementById("saveSignature1").src = dataURL;
        document.getElementById('imgsrc').value = dataURL;
        document.getElementById('a').value = a;

        var fd = new FormData(document.forms["valWizard"]);
        var xhr = new XMLHttpRequest();
        xhr.open('POST', 'upload_data1.php', true);

        xhr.upload.onprogress = function (t) {
            if (t.lengthComputable) {
                var percentComplete = (t.loaded / t.total) * 100;
                console.log(percentComplete + '% uploaded');
                //alert('Succesfully uploaded');
            }
        };

        xhr.onload = function () {

        };

        xhr.send(fd);
    }

    function signatureClear1() {
        var canvas = document.getElementById("newSignature1");
        canvas.width = canvas.width;

        var lineWidth = $('#line-width').val();

        var ctx = canvas.getContext("2d");
        ctx.strokeStyle = "#000000";
        ctx.fillStyle = "#000000";
        ctx.lineWidth = lineWidth;
        ctx.lineCap = "round";
    }

    //Line width change
    function lineWidthChange() {
        var canvas = document.getElementById("newSignature1");
        var ctx = canvas.getContext("2d");

        ctx.lineWidth = $('#line-width').val();
    }

    $(document).ready(function () {
        $('#line-width').on('change', lineWidthChange);
    });

</script>

<script language="JavaScript">

    function take_snapshot(a1, b1) {

        Webcam.snap(function (data_uri) {

            // display results in page
            //alert(a1);
            //alert(b1);
            document.getElementById('a1').value = a1;
            document.getElementById('b1').value = b1;
            document.getElementById('imgsrc1').value = data_uri;

            document.getElementById('results').innerHTML =
                '<img src="' + data_uri + '"/>';

            var fd = new FormData(document.forms["valWizard"]);
            var xhr = new XMLHttpRequest();
            xhr.open('POST', 'uploadphoto.php', true);

            xhr.upload.onprogress = function (e) {
                if (e.lengthComputable) {
                    var percentComplete = (e.loaded / e.total) * 100;
                    console.log(percentComplete + '% uploaded');
                    // alert('Succesfully uploaded');
                }
            };
            xhr.onload = function () {
            };
            xhr.send(fd);
        });
    }
</script>


<script type="text/javascript" src="js/jquery-1.2.6.min.js"></script>
<script type="text/javascript">

    function myfun(b) {
        var dataString = 'id=' + b;
        $.ajax({
            type: "POST",
            url: "ajax_changepwd.php",
            data: dataString,
            success: function (msg) {
                $('#pwd').html(msg);
            }
        });
        //return false;
    }

    function mykeyup() {

        arg1 = document.getElementById('emailid').value;
        x = document.getElementById("userroles").value;
        var dataString = 'admin=' + arg1 + '&roles=' + x;
        $.ajax({
            type: "POST",
            url: "ajax_checkname.php",
            data: dataString,
            success: function (msg) {
                //$('#checkres').html(msg);
            }
        });
        //return false;
    }
</script>
<script type="text/javascript">


    function removephne(val) {

        if (confirm("Do you really wish to delete this record?")) {

            document.getElementById('act').value = val;
            document.getElementById('action').value = "delete";
            //document.getElementById('hid').value = a;
            document.basicForm.submit();
        }
    }
</script>
<script>

    function isNumber(id, number) {

        number = number.replace(/[A-Za-z+@!#\/$%^&*\[\]{}|=()<>:\\;"'?.,~`-]/ig, "");
        id.value = number;
        return true;
    }

    function isName(id, name) {

        name = name.replace(/[0-9]/, "");
        id.value = name;
        return true;
    }

    function isNumOrLetter(evt) {


        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;

        if (charCode == 45 || charCode == 32)
            return true;

        if (charCode < 48 || (charCode > 57 && charCode < 65) ||
            (charCode > 90 && charCode < 97) ||
            charCode > 122) {
            return false;
        }

        return true;
    }
</script>
<link rel="stylesheet" href="css/jquery-ui-1.10.3.custom.min.css"/>
<script src="js/jquery-1.10.2.min.js"></script>
<script src="js/jquery-ui-1.10.3.custom.min.js"></script>
<!-- Validaion-->
<link rel="stylesheet" type="text/css" href="css/jquery.validate.css"/>

<script src="js/jquery.validate.js" type="text/javascript"></script>
<script src="js/jquery.validation.functions.js" type="text/javascript">
</script>
<script type="text/javascript">
    jQuery(function () {
        jQuery("#password1").validate({
            expression: "if (VAL.length > 5 && VAL) return true; else return false;",
            message: "Please enter a valid 6 digit Password"
        });

        jQuery("#confirm").validate({
            expression: "if ((VAL == jQuery('#password1').val()) && VAL) return true; else return false;",
            message: "Confirm password field doesn't match the password field"
        });

        jQuery("#userroles").validate({
            expression: "if (VAL != '0') return true; else return false;",
            message: "Please make a selection"
        });
        jQuery("#sites").validate({
            expression: "if (VAL != 'Choose One') return true; else return false;",
            message: "Please make a selection"
        });

        jQuery('.AdvancedForm').validated(function () {
            alert("Use this call to make AJAX submissions.");
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.step_wrapper').on('click', '.step_box', function () {
            $('.step_box').removeClass('selected');
            $(this).addClass('selected')
        });
    });
</script>

<script src="js/jquery.maskedinput.js" type="text/javascript"></script>
<script type="text/javascript">

    jQuery(function ($) {
        $("#phone0").mask("(999) 999-9999");
    });

    function chkAllVal() {
        var pno = document.getElementsByName("phoneno[]");

        for (var i = 0; i < pno.length; i++) {
            var id = document.getElementById("phone" + i);
            var val = id.value;
            var actVal = val.replace("_", "");
            if (actVal.length < 14) {
                document.getElementById("phone" + i).focus();
                //document.getElementById("phone"+i).setSelectionRange(actVal.length-1,actVal.length);
                return false;
            }
        }
    }
</script>
<script type="text/javascript">

    function hello1(a) {
        $("#remove" + a).remove();
        a--;
    }
    function hello() {
        //alert("Hello there!");
        countClicks++;
        document.getElementById('phoneid').value = countClicks;
        var dataString = 'countClicks=' + countClicks;

        $.ajax({
            type: "POST",
            url: "ajax_addmore_rabbis.php",
            data: dataString,
            success: function (msg) {
                //$('#addmore').html(msg);
                var strVal = msg.split("$$$$$");
                var strDiv = document.getElementById("addmore").outerHTML;
                document.getElementById("addmore").outerHTML = msg + strDiv;
                $("#phone" + countClicks).mask("(999) 999-9999");

            }
        });
    }
    var countClicks = 0;

</script>

<section>
    <div class="mainwrapper">
        <?php include 'includes/leftpanel.php'; ?>
        <div class="mainpanel">
            <div class="pageheader">
                <div>
                    <div class="media-body">
                        <h4>
                            <?php if ($a == '') {
                                echo 'Add Manager';
                            } else {
                                echo 'Update Manager';
                            } ?>
                        </h4>
                    </div>
                </div><!-- media -->
            </div><!-- pageheader -->

            <div class="contentpanel">
                <div class="row">
                    <div class="col-md-12">
                        <form enctype="multipart/form-data" method="post" id="valWizard" name="valWizard"
                              accept-charset="utf-8"
                              class="panel-wizard" novalidate="novalidate">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="form-group">
                                            <?= $err; ?>
                                        </div>
                                    </div>
                                    <div class="row">

                                        <?php $m = $_GET['m']; ?>
                                        <div class="form-group col-sm-6">
                                            <label class="col-sm-6 control-label">Role<span
                                                        class="asterisk">*</span></label>
                                            <div class="col-sm-6">
                                                <input autocorrect="off"  type="text" readonly value="Manager" name="userroles"
                                                       id="userroles" class="form-control"
                                                       placeholder="" required="">
                                            </div>
                                        </div><!-- form-group -->
                                        <div class="form-group col-sm-6" style="border:2px;">

                                            <label class="col-sm-3 control-label">Login Access<span
                                                        class="asterisk">*</span></label>

                                            <div class="col-sm-9 field" style="border:2px;">

                                                <div class="row">

                                                    <div class="col-md-4">
                                                        <div class="rdio rdio-primary">
                                                            <input autocorrect="off"  type="hidden"
                                                                   name="useridd"
                                                                   value="<?= $dataquery['Id'] ?>"/>
                                                            <input autocorrect="off"  type="radio" value="Access" checked
                                                                   id="Access"<?php if ($dataquery['status'] == 'Access') {
                                                                echo 'checked';
                                                            } ?> name="status">
                                                            <label for="Access">Access</label>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <div class="rdio rdio-primary">
                                                            <input autocorrect="off"  type="radio" value="Denied"
                                                                   id="Denied" <?php if ($dataquery['status'] == 'Denied') {
                                                                echo 'checked';
                                                            } ?> name="status">
                                                            <label for="Denied">Denied</label>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div><!-- form-group -->
                                    </div>
                                    <div class="row">

                                        <div class="form-group col-sm-6">
                                            <label class="col-sm-6 control-label">Login<span
                                                        class="asterisk">*</span></label>
                                            <div class="col-sm-6" id="emailDiv">
                                                <input autocorrect="off"  type="hidden"
                                                       id="current-email"
                                                       value="<?= $dataquery['emailid']; ?>">
                                                <input autocorrect="off"  type="text" name="emailid" id="emailid"
                                                       value="<?= $dataquery['emailid']; ?>"
                                                       class="form-control"
                                                       onkeypress="Maxval(this.value,30,this.id); loginChars()"
                                                       required
                                                       onblur="<?php if ($a == ''): ?>
                                                               checkEmail(this.value, 'createuser');
                                                       <?php else: ?>
                                                               checkEmail(this.value, 'updatemember');
                                                       <?php endif; ?>">
                                                <div id="counteremailid"></div>
                                            </div>
                                        </div><!-- form-group -->
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-sm-6">
                                            <label class="col-sm-6 control-label">Title</label>
                                            <div class="col-sm-6">
                                                <?php
                                                $sql = $dbh->prepare('SELECT *
                                                                      FROM `title`
                                                                      WHERE `Delete` = 0 ;');
                                                $sql->execute();
                                                $titles = $sql->fetchAll();
                                                ?>

                                                <select name="title"
                                                        id="title"
                                                        class="form-control">
                                                    <option value=" ">Choose One</option>

                                                    <?php foreach ($titles as $title): ?>
                                                        <option <?php if ($title['title'] == $dataquery['Title']) {
                                                            echo ' selected ';
                                                        } ?>><?= $title['title'] ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group col-sm-6">
                                            <label class="col-sm-6 control-label">Notes</label>
                                            <div class="col-sm-6 field">
                                                <input autocorrect="off"  type="text" name="usercomments" id="usercomments"
                                                       value="<?= $dataquery['comments']; ?>"
                                                       class="form-control">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-sm-6">
                                            <label class="col-sm-6 control-label">First Name</label>
                                            <div class="col-sm-6">
                                                <input autocorrect="off"  type="text" name="firstname" onkeyup="isName(this,this.value)" id="firstname"
                                                       value="<?= $dataquery['Firstname']; ?>" class="form-control"
                                                       placeholder="" required=""
                                                       onFocus="mykeyup();">
                                            </div>
                                        </div><!-- form-group -->
                                        <div class="form-group col-sm-6">
                                            <label class="col-sm-6 control-label">Last Name</label>
                                            <div class="col-sm-6">
                                                <input autocorrect="off"  type="text" name="lastname" onkeyup="isName(this,this.value);"
                                                       id="lastname"
                                                       value="<?= $dataquery['Lastname']; ?>" class="form-control"
                                                       placeholder="" required="">
                                            </div>
                                        </div><!-- form-group -->

                                    </div>
                                    <div class="row">
                                        <div class="result" id="result">
                                            <div id="checkres">
                                                <?php if ($a != '') { ?>
                                                    <div id="pwd">
                                                        <div class="form-group col-sm-12">
                                                            <label class="col-sm-3 control-label">Password<span
                                                                        class="asterisk">*</span></label>
                                                            <div class="col-sm-3 field password">
                                                                <input autocorrect="off"  type="password" readonly name="userpassword1"
                                                                       id="userpassword1"
                                                                       value="<?= $result = substr($dataquery['password'], 0, 10); ?>"
                                                                       class="form-control" placeholder="" required="">
                                                            </div>
                                                            <div class="col-sm-4 field">
                                                                <input autocorrect="off"  class="change btn btn-primary mr5" type="text"
                                                                       name="change" id="change"
                                                                       onclick="myfun('<?= $dataquery['Id']; ?>')"
                                                                       value="Change Password"
                                                                >
                                                            </div>
                                                        </div><!-- form-group -->
                                                    </div>
                                                <?php } else { ?>
                                                    <div class="form-group col-sm-6">
                                                        <label class="col-sm-6 control-label">Password<span
                                                                    class="asterisk">*</span></label>
                                                        <div class="col-sm-6 field">
                                                            <input autocorrect="off"  type="password" name="password1" id="password1"
                                                                   value="<?= $result = substr($dataquery['password'], 0, 10); ?>"
                                                                   class="form-control" placeholder="" required="">
                                                        </div>
                                                    </div><!-- form-group -->
                                                    <div class="form-group col-sm-6">
                                                        <label class="col-sm-6 control-label">Confirm Password<span
                                                                    class="asterisk">*</span></label>
                                                        <div class="col-sm-6 field">
                                                            <input autocorrect="off"  type="password" name="confirm" id="confirm"
                                                                   value="<?= $result = substr($dataquery['password'], 0, 10); ?>"
                                                                   class="form-control" placeholder="" required="">
                                                        </div>
                                                    </div><!-- form-group -->
                                                <?php }
                                                if ($ishowsite) { ?>
                                                <div class="form-group col-sm-6" style="margin-bottom:1%;">
                                                    <?php }else{ ?>
                                                    <div class="form-group" style="margin-bottom:1%;display: none">
                                                        <?php } ?>
                                                        <div class="col-sm-6">
                                                            Sites
                                                        </div>
                                                        <div class="col-sm-6 field">
                                                            <?php
                                                            $sql = $dbh->prepare("SELECT * FROM `sites`
                                                                                  WHERE `Delete`=0 && `Active`=1 ;");
                                                            $sql->execute();
                                                            $sites = $sql->fetchAll();

                                                            $s = $dataquery['Sites'];
                                                            $sitessql = $dbh->prepare("select * from sites where Id='$s' AND `Delete`='0'");
                                                            $sitessql->execute();
                                                            $sitesdata = $sitessql->fetch();
                                                            ?>
                                                            <input autocorrect="off"  id="sites1" name="sites1" type="hidden"
                                                                   value="<?= $sitesdata['Id']; ?>">
                                                            <select required='' data-placeholder="" id="sites"
                                                                    name="sites" class="form-control">
                                                                <?php if (!$a): ?>
                                                                    <option disabled selected>Choose One</option>
                                                                    <?php foreach ($sites as $site): ?>
                                                                        <option
                                                                                value="<?= $site['Id'] ?>">
                                                                            <?= $site['Sites'] ?>
                                                                        </option>
                                                                    <?php endforeach; ?>
                                                                <?php else: ?>
                                                                    <option selected
                                                                            value="<?= $sitesdata['Id'] ?>">
                                                                        <?= $sitesdata['Sites'] ?>
                                                                    </option>
                                                                    <?php foreach ($sites as $site): ?>
                                                                        <?php if ($site['Id'] != $sitesdata['Id']): ?>
                                                                            <option
                                                                                    value="<?= $site['Id'] ?>">
                                                                                <?= $site['Sites'] ?>
                                                                            </option>
                                                                        <?php endif; ?>
                                                                    <?php endforeach; ?>
                                                                <?php endif; ?>
                                                            </select>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            &nbsp;
                                                        </div>
                                                    </div>


                                                    <!-- row -->
                                                    <!-- panel-body -->
                                                    <div class="panel-footer">

                                                    </div><!-- panel-footer -->
                                                </div><!-- form-group -->
                                            </div>
                                        </div>

                                        <!-- signature feature -->
                                        <div class="row">
                                            <div class="col-sm-3" id="mydiv">
                                                <div>
                                                    Previously Saved Signature
                                                </div>
                                                <div id="imgDiv" class="col-sm-12 add-manager-img">
                                                    <?php
                                                    $signid = $dataquery['Id'];
                                                    $sql1 = $dbh->prepare("select * from rabbissign where signid='$signid'");
                                                    $sql1->execute();
                                                    $data1 = $sql1->fetch();
                                                    if ($data1['signname'] != '' && file_exists($defaultPathSign . $data1['signname'])) {
                                                        echo '<img class="add-manager-img" src="' . $defaultPathSign . $data1['signname'] . '">';
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div>
                                                    <input autocorrect="off"  id="imgsrc" type="hidden" name="imgsrc"/>
                                                    <input autocorrect="off"  id="a" type="hidden" name="a"
                                                           value="<?= $dataquery['Id']; ?>"/>
                                                    <div>
                                                        <?php echo 'New Signature'; ?>
                                                    </div>
                                                    <img id="saveSignature1" class="add-manager-img"/>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">

                                                <div id="canvas">
                                                    <canvas class="roundCorners" id="newSignature1"
                                                            style="position: relative; margin: 0; padding: 0; border: 1px solid #c4caac;"></canvas>
                                                </div>
                                                <script>signatureCapture1();</script>
                                                <div style="display: flex; ">
                                                    <?php
                                                    if ($a == ''): ?>
                                                        <button type="button"
                                                                class="btn btn-success add-manager-btn"
                                                                style="margin-right: 3px;"
                                                                onclick="signatureSave1(<?= $nextMember['value'] + 1 ?>)">
                                                            Add
                                                        </button>
                                                    <?php else: ?>
                                                        <button class="btn btn-success add-manager-btn"
                                                                style="margin-right: 3px;"
                                                                onclick="signatureSave1(<?= $dataquery['Id'] ?>)"
                                                                type="button">Update
                                                        </button>
                                                    <?php endif; ?>

                                                    <button type="button"
                                                            style="margin-right: 5px;"
                                                            class="btn btn-danger add-manager-btn"
                                                            onclick="signatureClear1()">
                                                        Clear&nbsp;&nbsp;&nbsp;</button>

                                                    <select id="line-width"
                                                            class="form-control"
                                                            style="width: 60px; ">
                                                        <option>&nbsp;1</option>
                                                        <option selected>&nbsp;2</option>
                                                        <option>&nbsp;3</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-9 col-sm-offset-3">
                                                <?php if ($a != '') { ?>
                                                    <button class="btn btn-primary mr5" name="updatemember"
                                                            type="submit">Update Manager
                                                    </button>
                                                <?php } else {
                                                    echo '<button class="btn btn-primary mr5" disabled
                                                              name="createuser" type="submit">Add Manager</button>';
                                                }
                                                ?>
                                                <a href="emvs.php?action=manager" class="btn btn-primary mr5"
                                                   type="submit">Cancel</a>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- panel -->
                        </form>
                    </div>
                </div><!-- row -->
            </div>
        </div><!-- mainpanel -->
    </div><!-- mainwrapper -->
</section>

<script src="js/jquery-migrate-1.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/modernizr.min.js"></script>
<script src="js/ppace.min.js"></script>

<script src="js/jquery.cookies.js"></script>

<script src="js/jquery.dataTables.min.js"></script>
<script src="js/dataTables.bootstrap.js"></script>
<script src="js/dataTables.responsive.js"></script>
<script src="js/select2.min.js"></script>

<script src="js/custom.js"></script>
<script>


    jQuery(document).ready(function () {

        $("#emailError").css('display', 'none');

        jQuery('#basicTable').DataTable({
            responsive: true
        });

        var shTable = jQuery('#shTable').DataTable({
            "fnDrawCallback": function (oSettings) {
                jQuery('#shTable_paginate ul').addClass('pagination-active-dark');
            },
            responsive: true
        });

        // Show/Hide Columns Dropdown
        jQuery('#shCol').click(function (event) {
            event.stopPropagation();
        });

        jQuery('#shCol input').on('click', function () {

            // Get the column API object
            var column = shTable.column($(this).val());

            // Toggle the visibility
            if ($(this).is(':checked'))
                column.visible(true);
            else
                column.visible(false);
        });

        var exRowTable = jQuery('#exRowTable').DataTable({
            responsive: true,
            "fnDrawCallback": function (oSettings) {
                jQuery('#exRowTable_paginate ul').addClass('pagination-active-success');
            },
            "ajax": "ajax/objects.txt",
            "columns": [
                {
                    "class": 'details-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": ''
                },
                {"data": "name"},
                {"data": "position"},
                {"data": "office"},
                {"data": "salary"}
            ],
            "order": [[1, 'asc']]
        });

        // Add event listener for opening and closing details
        jQuery('#exRowTable tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = exRowTable.row(tr);

            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            }
            else {
                // Open this row
                row.child(format(row.data())).show();
                tr.addClass('shown');
            }
        });


        // DataTables Length to Select2
        jQuery('div.dataTables_length select').removeClass('form-control input-sm');
        jQuery('div.dataTables_length select').css({width: '60px'});
        jQuery('div.dataTables_length select').select2({
            minimumResultsForSearch: -1
        });

    });

    function format(d) {
        // `d` is the original data object for the row
        return '<table class="table table-bordered nomargin">' +
            '<tr>' +
            '<td>Full name:</td>' +
            '<td>' + d.name + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Extension number:</td>' +
            '<td>' + d.extn + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Extra info:</td>' +
            '<td>And any further details here (images etc)...</td>' +
            '</tr>' +
            '</table>';
    }
</script>
<script src="js/auto.js"></script>
