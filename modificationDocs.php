<?php

include 'includes/header.php';
include 'includes/dbcon.php';

if ($_SESSION['user'] == '') {
    header('Location: emvs.php?action=index');
}

?>


<style>

    .first {
        display: none;
    }

    .second {
        display: show;
    }

    #tooltip {
        text-align: center;
        color: #fff;
        background: #111;
        position: absolute;
        z-index: 100;
        padding: 15px;
    }

    #tooltip:after { /* triangle decoration */

        width: 0;
        height: 0;
        border-left: 10px solid transparent;
        border-right: 10px solid transparent;
        border-top: 10px solid #111;
        content: '';
        position: absolute;
        left: 50%;
        bottom: -10px;
        margin-left: -10px;
    }

    #tooltip.top:after {

        border-top-color: transparent;
        border-bottom: 10px solid #111;
        top: -20px;
        bottom: auto;
    }

    #tooltip.left:after {

        left: 10px;
        margin: 0;
    }

    #tooltip.right:after {

        right: 10px;
        left: auto;
        margin: 0;
    }

</style>


<section>
    <div class="mainwrapper">
        <?php include 'includes/leftpanel.php'; ?>
        <div class="mainpanel">
            <div class="pageheader">
                <div>
                    <div class="media-body">
                        <h4>Modification Docs: Completion Progress Report</h4>
                    </div>
                </div><!-- media -->
            </div><!-- pageheader -->

            <div class="contentpanel">
                <div class="panel panel-primary-head">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>
                                Date
                            </th>
                            <th>
                                Issue
                            </th>
                            <th>
                                File | Database | Table
                            </th>
                            <th>
                                notes
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                June 18, 2016
                            </td>
                            <td style="background-color:lightgreen; font-weight:bold; font-size: 12pt;">
                                22
                            </td>
                            <td>
                                file4.php
                            </td>
                            <td>
                                a note about made changes 4
                            </td>
                        </tr>
                        <tr>
                            <td>

                            </td>
                            <td style="background-color:yellow; font-weight:bold; font-size: 12pt;">
                                25
                            </td>
                            <td>
                                file5.php
                            </td>
                            <td>
                                a note about made changes 5
                            </td>
                        </tr>
                        <tr>
                            <td>
                                June 17, 2016
                            </td>
                            <td style="background-color:red; font-weight:bold; font-size: 12pt;">
                                20
                            </td>
                            <td>
                                file.php
                            </td>
                            <td>
                                a note about made changes
                            </td>
                        </tr>
                        <tr>
                            <td>

                            </td>
                            <td style="background-color:red; font-weight:bold; font-size: 12pt;">

                            </td>
                            <td>
                                `some_db_name`.`some_table_name`
                            </td>
                            <td>
                                a note about made changes 2
                            </td>
                        </tr>
                        <tr>
                            <td>

                            </td>
                            <td style="background-color:lightblue; font-weight:bold; font-size: 12pt;">
                                21
                            </td>
                            <td>
                                file3.php
                            </td>
                            <td>
                                a note about made changes 3
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div><!-- panel -->
            </div><!-- contentpanel -->
        </div><!-- mainpanel -->
    </div><!-- mainwrapper -->
</section>

<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/jquery-migrate-1.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/modernizr.min.js"></script>
<script src="js/ppace.min.js"></script>

<script src="js/jquery.cookies.js"></script>

<script src="js/jquery.dataTables.min.js"></script>
<script src="js/dataTables.bootstrap.js"></script>
<script src="js/dataTables.responsive.js"></script>
<script src="js/select2.min.js"></script>

<script src="js/custom.js"></script>

</body>
</html>
