<?php

/*
 * This file was created to manage look ups for Required Fields
 * This code handles rendering of list of required fields that depends on chosen option from menu
 * It works same as for previously coded look ups, but here is added possibility to handle different kinds of options
 * in one place
 * */

include 'includes/header.php';
include 'includes/dbcon.php';

if ($_SESSION['user'] == '') {
    header('Location: emvs.php?action=index');
}

switch ($fieldsType) {

    case "collectorDetails":
        $subjectTitle = "New Collector Details and Personal Info";
        break;
    case "visits":
        $subjectTitle = "Visits Tab";
        break;
    case "address":
        $subjectTitle = "Address Tab";
        break;
    case "funds":
        $subjectTitle = "Funds Tab";
        break;
    case "purpose":
        $subjectTitle = "Purpose Tab";
        break;
    case "reference":
        $subjectTitle = "Reference Tab";
        break;
    default:
        $subjectTitle = "unknown";
}

if (isset($_POST['action']) && ($_POST['action'] == "update")) {

    echo $_POST['act'];
    $upquery = $dbh->prepare("
            UPDATE `required_fields` 
            SET `active`=" . $_POST['act'] . " 
            WHERE (`id`=" . $_POST['hid'] . ")
        ");
    $upquery->execute();
}

if (isset($_POST['action']) && ($_POST['action'] == "delete")) {

    $delquery = $dbh->prepare("
            UPDATE `required_fields` 
            SET `deleted`='1' 
            WHERE `id`=" . $_POST['hid'] . "
        ");
    $delquery->execute();
}

if (isset($_POST['action']) && ($_POST['action'] == "multipleDeletion")) {

    $checkboxes = explode(",", $_POST['hid']);

    for ($i = 0; $i < count($checkboxes); $i++) {

        $itemToDelete = $checkboxes[$i];
        $delquery = $dbh->prepare("
                UPDATE `required_fields` 
                SET `deleted`='1'
                WHERE `id` = " . $itemToDelete . "
            ");
        $delquery->execute();
    }
}
?>

<script>

    function fnactive(id) {

        if (document.getElementById('active' + id).checked == true)
            document.getElementById('act').value = 1;
        else
            document.getElementById('act').value = 0;

        document.getElementById('action').value = "update";
        document.getElementById('hid').value = id;
        document.requiredFields.submit();
    }

    function fndelete(id) {

        if (confirm("Do you really wish to delete this record?")) {

            document.getElementById('action').value = "delete";
            document.getElementById('hid').value = id;
            document.requiredFields.submit();
        }
    }

    function actMultipleDeletion() {

        var allVals = [];
        $.each($("input[name=requiredFieldsMultipleDelete]:checked"), function () {
            allVals.push($(this).val());
        });
        if (allVals.length > 0) {
            document.getElementById('action').value = "multipleDeletion";
            document.getElementById('hid').value = allVals;
            document.requiredFields.submit();
        } else {
            alert("It is required to select at least one entry to execute multiple deletion");
        }
    }

</script>

<section>
    <div class="mainwrapper">
        <?php include 'includes/leftpanel.php'; ?>
        <div class="mainpanel">
            <div class="pageheader">
                <div>
                    <div class="media-body">
                        <h4>Required Fields: <?php echo $subjectTitle; ?></h4>
                    </div>
                </div><!-- media -->
            </div><!-- pageheader -->

            <div class="contentpanel">
                <div class="panel panel-primary-head content-padding">
                    <div class="panel-heading">
                        <a href="emvs.php?action=addRequiredField&fieldType=<?php echo $fieldsType; ?>"
                           class="btn btn-warning">Add New Field:
                            <?php echo $subjectTitle; ?>
                        </a>
                    </div><!-- panel-heading -->
                    <p></p>
                    <p></p>
                    <!--<p class="mb20">Click on row to edit collector. Use header row to search and filter.</p>  -->
                    <form name="requiredFields" id="requiredFields" method="post">
                        <table id="basicTable"
                               class="table table-striped table-bordered responsive basic-table">
                            <thead class="">
                            <tr>
                                <th style="text-align:center;">#</th>
                                <th style="text-align:center">Assignment</th>
                                <th style="text-align:center">Title</th>
                                <th style="text-align:center;">Requirement</th>
                                <th style="text-align:center;">Edit</th>
                                <th style="text-align:center;">Delete</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $sql = $dbh->prepare("
                                SELECT * 
                                FROM required_fields 
                                WHERE `deleted`!='1' AND `tab` = '$fieldsType' 
                                ORDER BY title ASC
                            ");
                            $sql->execute();
                            $count = $sql->rowCount();
                            $j = 1;
                            while ($data = $sql->fetch()) {
                                ?>
                                <tr>
                                    <td><?= $j; ?></td>
                                    <td>
                                        <label
                                            style="visibility:hidden;position: absolute;"><?= $data['assignment']; ?></label>
                                        <?= $data['assignment']; ?>
                                    </td>
                                    <td>
                                        <label
                                            style="visibility:hidden;position: absolute;"><?= $data['title']; ?></label>
                                        <?= $data['title']; ?>
                                    </td>
                                    <td>
                                        <input autocorrect="off"  type="checkbox" value="1" name="active" id="active<?= $data['id']; ?>"
                                               onclick="fnactive(<?= $data['id']; ?>)" <?php if ($data['active'] == 1) echo "checked"; ?>/>
                                        <label for="active<?= $data['id']; ?>"></label>
                                    </td>
                                    <td>
                                        <a href="emvs.php?action=addRequiredField&fieldType=<?php echo $fieldsType; ?>&&e=<?= base64_encode($data['id']); ?>">
                                            <img src="images/edit1.png" border="0" data-toggle="tooltip" title="Edit"
                                                 alt="EMVS">
                                        </a>
                                    </td>
                                    <td>
                                        <a href="javascript:fndelete(<?= $data['id']; ?>)">
                                            <img src="images/delete1.gif" border="0" data-toggle="tooltip"
                                                 title="Delete" alt="EMVS">
                                        </a>
                                        <input autocorrect="off"  type="checkbox" id="requiredFieldsMultipleDelete<?= $data['id']; ?>"
                                               name="requiredFieldsMultipleDelete" data-toggle="tooltip"
                                               title="Multiple Deletion" value="<?= $data['id']; ?>"
                                               style="float:right;"/>
                                    </td>
                                </tr>
                                <?php $j++;
                            } ?>
                            </tbody>
                            <tfoot>
                            <tr>
                                <td colspan="7">
                                    <input autocorrect="off"  type="button" value="Multiple Deletion" class="btn btn-primary"
                                           style="float: right;" onclick="actMultipleDeletion();"/>
                                </td>
                            </tr>
                            </tfoot>
                        </table>

                        <input autocorrect="off"  type="hidden" name="action" id="action"/>
                        <input autocorrect="off"  type="hidden" name="hid" id="hid"/>
                        <input autocorrect="off"  type="hidden" name="act" id="act"/>
                    </form>

                </div><!-- panel -->
            </div><!-- contentpanel -->
        </div><!-- mainpanel -->
    </div><!-- mainwrapper -->
</section>

<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/jquery-migrate-1.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/modernizr.min.js"></script>
<script src="js/ppace.min.js"></script>

<script src="js/jquery.cookies.js"></script>

<script src="js/jquery.dataTables.min.js"></script>
<script src="js/dataTables.bootstrap.js"></script>
<script src="js/dataTables.responsive.js"></script>
<script src="js/select2.min.js"></script>

<script src="js/custom.js"></script>

<script>

    jQuery(document).ready(
        function () {

            jQuery('#basicTable').DataTable(
                {
                    responsive: true
                }
            );

            var shTable = jQuery('#shTable').DataTable(
                {
                    "fnDrawCallback": function (oSettings) {
                        jQuery('#shTable_paginate ul').addClass('pagination-active-dark');
                    },
                    responsive: true
                }
            );

            // Show/Hide Columns Dropdown
            jQuery('#shCol').click(
                function (event) {
                    event.stopPropagation();
                }
            );

            jQuery('#shCol input').on('click',
                function () {

                    // Get the column API object
                    var column = shTable.column($(this).val());

                    // Toggle the visibility
                    if ($(this).is(':checked'))
                        column.visible(true);
                    else
                        column.visible(false);
                }
            );

            var exRowTable = jQuery('#exRowTable').DataTable(
                {
                    responsive: true,
                    "fnDrawCallback": function (oSettings) {
                        jQuery('#exRowTable_paginate ul').addClass('pagination-active-success');
                    },
                    "ajax": "ajax/objects.txt",
                    "columns": [
                        {
                            "class": 'details-control',
                            "orderable": false,
                            "data": null,
                            "defaultContent": ''
                        },
                        {"data": "name"},
                        {"data": "position"},
                        {"data": "office"},
                        {"data": "salary"}
                    ],
                    "order": [[1, 'asc']]
                }
            );

            // Add event listener for opening and closing details
            jQuery('#exRowTable tbody').on('click', 'td.details-control',
                function () {

                    var tr = $(this).closest('tr');
                    var row = exRowTable.row(tr);

                    if (row.child.isShown()) {

                        // This row is already open - close it
                        row.child.hide();
                        tr.removeClass('shown');
                    } else {
                        // Open this row
                        row.child(format(row.data())).show();
                        tr.addClass('shown');
                    }
                }
            );

            // DataTables Length to Select2
            jQuery('div.dataTables_length select').removeClass('form-control input-sm');
            jQuery('div.dataTables_length select').css({width: '60px'});
            jQuery('div.dataTables_length select').select2(
                {
                    minimumResultsForSearch: -1
                }
            );
        }
    );

    function format(d) {
        // `d` is the original data object for the row
        return '<table class="table table-bordered nomargin">' +
            '<tr>' +
            '<td>Full name:</td>' +
            '<td>' + d.name + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Extension number:</td>' +
            '<td>' + d.extn + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Extra info:</td>' +
            '<td>And any further details here (images etc)...</td>' +
            '</tr>' +
            '</table>';
    }
</script>

</body>
</html>
