<?php
error_reporting(0);
include 'includes/function.php';
include 'includes/dbcon.php';
$firstname = $_POST['firstname'];
$lastname = $_POST['lastname'];
$emailaddress = $_POST['emailaddress'];
$password = $_POST['password'];
$agree = $_POST['agree'];
if (isset($_POST['account'])) {
    $pass = mc_encrypt($password, ENCRYPTION_KEY);
    $count = $dbh->exec("INSERT INTO emvsadmin(firstname,lastname,emailaddress,password,aggree,Addeddate) VALUES ('$firstname','$lastname','$emailaddress','$pass','$agree',now())");
    if ($count == '1') {
        echo '<div class="success"><p><img src="images/success.png" border="none"> This is a successful event box. This can be used for successfully sent messages, ecommerce orders, etc.</p></div>';
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Electronic Meshulach Verification System</title>

    <link href="css/style.default.css" rel="stylesheet">

</head>

<body class="signin">


<section>

    <div class="panel panel-signup">
        <div class="panel-body">
            <div class="logo text-center">
                <img src="images/logo.png" alt="Chain Logo">
            </div>
            <br/>
            <h4 class="text-center mb5">Create a new account</h4>
            <p class="text-center">Please enter your credentials below</p>

            <div class="mb30"></div>

            <form action="#" method="post">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="input-group mb15">
                            <span class="input-group-addon"></span>
                            <input autocorrect="off"  type="text" class="form-control" name="firstname" placeholder="Enter Firstname">
                        </div><!-- input-group -->
                    </div>
                    <div class="col-sm-6">
                        <div class="input-group mb15">
                            <span class="input-group-addon"></span>
                            <input autocorrect="off"  type="text" class="form-control" name="lastname" placeholder="Enter Lastname">
                        </div><!-- input-group -->
                    </div>
                </div><!-- row -->
                <br/>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="input-group mb15">
                            <span class="input-group-addon"></span>
                            <input autocorrect="off"  type="email" class="form-control" name="emailaddress"
                                   placeholder="Enter Email Address">
                        </div><!-- input-group -->
                    </div>
                    <div class="col-sm-6">
                        <div class="input-group mb15">
                            <span class="input-group-addon"></span>
                            <input autocorrect="off"  type="password" class="form-control" name="password" placeholder="Enter Password">
                        </div><!-- input-group -->
                    </div>
                </div><!-- row -->
                <br/>
                <div class="clearfix">
                    <div class="pull-left">
                        <div class="ckbox ckbox-primary mt5">
                            <input autocorrect="off"  type="checkbox" name="agree" id="agree" value="1">
                            <label for="agree">I agree with <a href="">Terms and Conditions</a></label>
                        </div>
                    </div>
                    <div class="pull-right">
                        <button type="submit" name="account" class="btn btn-success">Create New Account</button>
                    </div>
                </div>
            </form>

        </div><!-- panel-body -->
        <div class="panel-footer">
            <a href="#" class="btn btn-primary btn-block">Already a Member? Sign In</a>
        </div><!-- panel-footer -->
    </div><!-- panel -->

</section>


<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/jquery-migrate-1.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/modernizr.min.js"></script>
<script src="js/ppace.min.js"></script>

<script src="js/jquery.cookies.js"></script>

<script src="js/custom.js"></script>

</body>
</html>
