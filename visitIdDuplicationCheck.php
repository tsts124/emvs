<?php
include 'includes/dbcon.php';

$collId = $_POST['collId'];
$visitId = $_POST['visitId'];
$thisVisitId = $_POST['thisVisitId'];

$sql = $dbh->prepare("SELECT *
                      FROM `visitstable`
                      WHERE `collectorsid` = :collId
                      AND `visitid` = :visitId
                      AND `visitid` != :thisVisId ; ");
$sql->execute([':collId' => $collId, ':visitId' => $visitId, ':thisVisId' => $thisVisitId]);
if ($sql->fetch()) {
    echo 'duplication';
} else {
    echo 'ok';
}
