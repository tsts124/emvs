

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Electronic Meshulach Verification System</title>

        <link href="css/style.default.css" rel="stylesheet">
        <link href="css/select2.css" rel="stylesheet" />
        <link href="css/style.datatables.css" rel="stylesheet">
        <link href="css/dataTables.responsive.css" rel="stylesheet">
        <script src="todataurl.js"></script>
        <script src="signature.js"></script>
		  
  <script>
 
<script>
			function signatureCapture() {
	var canvas = document.getElementById("newSignature");
	var context = canvas.getContext("2d");
	canvas.width = 276;
	canvas.height = 180;
	context.fillStyle = "#fff";
	context.strokeStyle = "#444";
	context.lineWidth = 1.5;
	context.lineCap = "round";
	context.fillRect(0, 0, canvas.width, canvas.height);
	var disableSave = true;
	var pixels = [];
	var cpixels = [];
	var xyLast = {};
	var xyAddLast = {};
	var calculate = false;
	{ 	//functions
		function remove_event_listeners() {
			canvas.removeEventListener('mousemove', on_mousemove, false);
			canvas.removeEventListener('mouseup', on_mouseup, false);
			canvas.removeEventListener('touchmove', on_mousemove, false);
			canvas.removeEventListener('touchend', on_mouseup, false);

			document.body.removeEventListener('mouseup', on_mouseup, false);
			document.body.removeEventListener('touchend', on_mouseup, false);
		}

		function get_coords(e) {
			var x, y;

			if (e.changedTouches && e.changedTouches[0]) {
				var offsety = canvas.offsetTop || 0;
				var offsetx = canvas.offsetLeft || 0;

				x = e.changedTouches[0].pageX - offsetx;
				y = e.changedTouches[0].pageY - offsety;
			} else if (e.layerX || 0 == e.layerX) {
				x = e.layerX;
				y = e.layerY;
			} else if (e.offsetX || 0 == e.offsetX) {
				x = e.offsetX;
				y = e.offsetY;
			}

			return {
				x : x, y : y
			};
		};

		function on_mousedown(e) {
			e.preventDefault();
			e.stopPropagation();

			canvas.addEventListener('mouseup', on_mouseup, false);
			canvas.addEventListener('mousemove', on_mousemove, false);
			canvas.addEventListener('touchend', on_mouseup, false);
			canvas.addEventListener('touchmove', on_mousemove, false);
			document.body.addEventListener('mouseup', on_mouseup, false);
			document.body.addEventListener('touchend', on_mouseup, false);

			empty = false;
			var xy = get_coords(e);
			context.beginPath();
			pixels.push('moveStart');
			context.moveTo(xy.x, xy.y);
			pixels.push(xy.x, xy.y);
			xyLast = xy;
		};

		function on_mousemove(e, finish) {
			e.preventDefault();
			e.stopPropagation();

			var xy = get_coords(e);
			var xyAdd = {
				x : (xyLast.x + xy.x) / 2,
				y : (xyLast.y + xy.y) / 2
			};

			if (calculate) {
				var xLast = (xyAddLast.x + xyLast.x + xyAdd.x) / 3;
				var yLast = (xyAddLast.y + xyLast.y + xyAdd.y) / 3;
				pixels.push(xLast, yLast);
			} else {
				calculate = true;
			}

			context.quadraticCurveTo(xyLast.x, xyLast.y, xyAdd.x, xyAdd.y);
			pixels.push(xyAdd.x, xyAdd.y);
			context.stroke();
			context.beginPath();
			context.moveTo(xyAdd.x, xyAdd.y);
			xyAddLast = xyAdd;
			xyLast = xy;

		};

		function on_mouseup(e) {
			remove_event_listeners();
			disableSave = false;
			context.stroke();
			pixels.push('e');
			calculate = false;
		};
	}
	canvas.addEventListener('touchstart', on_mousedown, false);
	canvas.addEventListener('mousedown', on_mousedown, false);
}

function signatureSave() {
	var canvas = document.getElementById("newSignature");// save canvas image as data url (png format by default)
	var dataURL = canvas.toDataURL("image/png");
	document.getElementById("saveSignature").src = dataURL;
};

function signatureClear() {
	var canvas = document.getElementById("newSignature");
	var context = canvas.getContext("2d");
	context.clearRect(0, 0, canvas.width, canvas.height);
}
		</script>
   <body>
        
        <header>
            <div class="headerwrapper">
                <div class="header-left">
                    <a href="#" class="logo">
                        <img src="images/logo.png" alt="" /> 
                    </a>
                    <div class="pull-right">
                        <a href="" class="menu-collapse">
                        <img src="images/left-right.png" />
                        </a>
                    </div>
                </div><!-- header-left -->
                
                <div class="header-right">
                    
                    <div class="pull-right">
                        <form class="form form-search" action="#">
                       <h3 style="margin-left:-1040px; margin-top:5px; color:#28EB55; font-weight:800">Electronic Meshulach Verification System</h3> 
                        </form>                         
                        <div class="btn-group btn-group-option">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                              <img src="images/login.png" />
                            </button>
                            <ul class="dropdown-menu pull-right" role="menu">                            
                              <li><a href="#"> Sign Out</a></li>
                            </ul>
                        </div><!-- btn-group -->
                        
                    </div><!-- pull-right -->
                    
                </div><!-- header-right -->
                
            </div><!-- headerwrapper -->
        </header>
        <section>
            <div class="mainwrapper"> 
 <div class="leftpanel">
                    <div class="media profile-left">
                        <a class="pull-left profile-thumb">
                            <img class="img-circle" src="images/avator.png" alt="">
                        </a>
                        <div class="media-body">
                            <h4 class="media-heading">Prabu Vasanth</h4>
                           <small class="text-muted"><strong>Volunteer</strong></small>
                        </div>
                    </div><!-- media -->
                    
                   <ul class="nav nav-pills nav-stacked">
                        <li><a href="welcome.php"><img src="images/menu1.png"><span style="vertical-align:middle;">Home</span></a></li>
						 <li><a href="collector.php"><img src="images/collectors.png"><span style="vertical-align:middle;">Collectors</span></a></li>
                        <li class="parent"><a href=""><img src="images/menu2.png"><span>Lookups</span></a>
                            <ul class="children" style="display: none;">
                                <li><a href="drivers.php">Drivers</a></li>
								<li><a href="hosts.php">Hosts</a></li>
                                <li><a href="rabbis.php">Rabbis</a></li> 
								<li><a href="#">Sites</a></li> 								
                            </ul>
                        </li>
						<li><a href="admin.php"><img src="images/menu3.png"><span>Admin</span></a></li>						
						<li><a href="tools.php"><img src="images/menu4.png"><span>Tools</span></a></li>
						<!--<!--<li><a href="photo.php"><img src="images/sign.png"><span>Signature</span></a></li>-->
                    </ul>
                    
                </div><!-- leftpanel -->			
                <div class="mainpanel">
                    <div class="pageheader">
                        <div class="media">                            
                            <div class="media-body">                               
                                <h4>Visit Entry Screen</h4>
                            </div>
                        </div><!-- media -->
                    </div><!-- pageheader -->
					
             <div class="panel panel-primary" style="border:none;">    
					 <form method="post" id="valWizard" class="panel-wizard" novalidate="novalidate">
                                    <ul class="nav nav-justified nav-wizard nav-disabled-click nav-pills">
                                        <li class="active"><a href="#tab1-4" data-toggle="tab">Personal Info</a></li>
                                        <li><a href="#tab2-4" data-toggle="tab">Visits</a></li>
                                        <li><a href="#tab3-4" data-toggle="tab">Address</a></li>
										<li><a href="#tab4-4" data-toggle="tab">Information</a></li>
										<li><a href="#tab5-4" data-toggle="tab">Purpose</a></li>
										<li><a href="#tab6-4" data-toggle="tab">Reference</a></li>
										<li><a href="#tab7-4" data-toggle="tab">Sign</a></li>
										<li><a href="#tab8-4" data-toggle="tab">Print</a></li>
                                    </ul>
                
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="tab1-4">
										<div class="panel panel-default">
                                    <div class="panel-heading">
									<div class="col-sm-2" style="margin-left:-10px; margin-top:-10px">
									 <strong>Member ID : 999999</strong>
									</div>
                                        
                                    </div><!-- panel-heading -->
                                    <div class="panel-body">
										 <div class="row" style="margin-bottom:1%;">
										 
											  <div class="col-sm-2">
											  Title <span class="asterisk">*</span>
											  </div>
											  <div class="col-sm-3">
											  <input autocorrect="off"  type="text" name="title" id="title" value="" class="form-control" placeholder="Title" required="">
											  </div>
											  <div class="col-sm-1">
											  &nbsp;
											  </div>
											  <div class="col-sm-2">
											  Address1<span class="asterisk">*</span>
											  </div>
											  <div class="col-sm-3">
											  <input autocorrect="off"  type="text" name="title" id="title" value="" class="form-control" placeholder="Address1" required="">
											  </div>
										 </div>
										  <div class="row" style="margin-bottom:1%;">
											  <div class="col-sm-2">
											  First Name <span class="asterisk">*</span>
											  </div>
											  <div class="col-sm-3">
											  <input autocorrect="off"  type="text" name="title" id="title" value="" class="form-control" placeholder="First Name" required="">
											  </div>
											  <div class="col-sm-1">
											  &nbsp;
											  </div>
											  <div class="col-sm-2">
											  Address2 <span class="asterisk">*</span>
											  </div>
											  <div class="col-sm-3">
											  <input autocorrect="off"  type="text" name="title" id="title" value="" class="form-control" placeholder="Address2" required="">
											  </div>
										 </div>
										 	  <div class="row" style="margin-bottom:1%;">
											  <div class="col-sm-2">
											  Middle Name <span class="asterisk">*</span>
											  </div>
											  <div class="col-sm-3">
											  <input autocorrect="off"  type="text" name="title" id="title" value="" class="form-control" placeholder="Middle Name" required="">
											  </div>
											  <div class="col-sm-1">
											  &nbsp;
											  </div>
											  <div class="col-sm-2">
											 City <span class="asterisk">*</span>
											  </div>
											  <div class="col-sm-3">
											  <input autocorrect="off"  type="text" name="title" id="title" value="" class="form-control" placeholder="City" required="">
											  </div>
										 </div>
										 <div class="row" style="margin-bottom:1%;">
											  <div class="col-sm-2">
											  Last Name <span class="asterisk">*</span>
											  </div>
											  <div class="col-sm-3">
											  <input autocorrect="off"  type="text" name="title" id="title" value="" class="form-control" placeholder="Last Name" required="">
											  </div>
											  <div class="col-sm-1">
											  &nbsp;
											  </div>
											  <div class="col-sm-2">
											 State/Country <span class="asterisk">*</span>
											  </div>
											  <div class="col-sm-3">
											  <input autocorrect="off"  type="text" name="title" id="title" value="" class="form-control" placeholder="State/Country" required="">
											  </div>
										 </div>
										 <div class="row" style="margin-bottom:1%;">
											  <div class="col-sm-2">
											  Phone <span class="asterisk">*</span>
											  </div>
											  <div class="col-sm-3">
											  <input autocorrect="off"  type="text" name="title" id="title" value="" class="form-control" placeholder="Phone" required="">
											  </div>
											  <div class="col-sm-1">
											  &nbsp;
											  </div>
											  <div class="col-sm-2">
											 Zip / Postal Code <span class="asterisk">*</span>
											  </div>
											  <div class="col-sm-3">
											  <input autocorrect="off"  type="text" name="title" id="title" value="" class="form-control" placeholder="Zip / Postal Code" required="">
											  </div>
										 </div>
										 <div class="row" style="margin-bottom:1%;">
											  <div class="col-sm-2">
											   SSN / ID Numbe <span class="asterisk">*</span>
											  </div>
											  <div class="col-sm-3">
											  <input autocorrect="off"  type="text" name="title" id="title" value="" class="form-control" placeholder=" SSN / ID Numbe" required="">
											  </div>
											  <div class="col-sm-1">
											  &nbsp;
											  </div>
											  <div class="col-sm-2">
											 Gender <span class="asterisk">*</span>
											  </div>
											  <div class="col-sm-3">
											   <div class="col-md-4" style="padding:0px;">
<div class="rdio rdio-primary">
                                                    <input autocorrect="off"  type="radio" id="male" value="m" name="gender" required="">
                                                    <label for="male">Male</label>
                                                </div></div>
												 <div class="col-md-6">
                                               <div class="rdio rdio-primary">
                                                    <input autocorrect="off"  type="radio" value="f" id="female" name="gender">
                                                    <label for="female">Female</label>
                                                </div> </div>
											  </div>
										 </div>
										
										  <div class="row" style="margin-bottom:1%;">
											  <div class="col-sm-2">
											  Date of Birth <span class="asterisk">*</span>
											  </div>
											  
											  <div class="col-sm-9">
											  <div class="col-sm-2" style=" padding: 0px; width:16%">
                                                   <select id="select-search-hide" data-placeholder="Choose One" name="regmonth" class="form-control" style="width:90%;margin-bottom: 1%;" tabindex="-1" title="">
                                            <option value="">March</option>
                                            <option value="1-January">January</option>
                                            <option value="2-February">February</option>
                                            <option value="3-March">March</option>
                                            <option value="4-April">April</option>
                                            <option value="5-May">May</option>
                                            <option value="6-June">June</option>
                                            <option value="7-July">July</option>
                                            <option value="8-August">August</option>
                                            <option value="9-September">September</option>
                                            <option value="10-October">October</option>
                                            <option value="11-November">November</option>
                                            <option value="12-December">December</option>
                                        </select>
										 

												 </div>	
												 <div class="col-sm-1" style="padding: 0;">
												
                                                   <select id="select-search-hide" data-placeholder="Choose One" name="regdate" class="form-control" style="width:90%;margin-bottom: 1%;" tabindex="-1" title="">
												     <option value="">22</option>
														 <option value="1">1</option> <option value="2">2</option> <option value="3">3</option> <option value="4">4</option> <option value="5">5</option> <option value="6">6</option> <option value="7">7</option> <option value="8">8</option> <option value="9">9</option> <option value="10">10</option> <option value="11">11</option> <option value="12">12</option> <option value="13">13</option> <option value="14">14</option> <option value="15">15</option> <option value="16">16</option> <option value="17">17</option> <option value="18">18</option> <option value="19">19</option> <option value="20">20</option> <option value="21">21</option> <option value="22">22</option> <option value="23">23</option> <option value="24">24</option> <option value="25">25</option> <option value="26">26</option> <option value="27">27</option> <option value="28">28</option> <option value="29">29</option> <option value="30">30</option> <option value="31">31</option>                                          
                                        </select>
										 

												 </div>	
												 <div class="col-sm-2" style="padding: 0; width:10%">
												                                                    <select id="select-search-hide" data-placeholder="Choose One" name="regyear" class="form-control" style="width:90%;margin-bottom: 1%;" tabindex="-1" title="">
                                            <option value="">2015</option>
														 <option value="1947">1947</option> <option value="1948">1948</option> <option value="1949">1949</option> <option value="1950">1950</option> <option value="1951">1951</option> <option value="1952">1952</option> <option value="1953">1953</option> <option value="1954">1954</option> <option value="1955">1955</option> <option value="1956">1956</option> <option value="1957">1957</option> <option value="1958">1958</option> <option value="1959">1959</option> <option value="1960">1960</option> <option value="1961">1961</option> <option value="1962">1962</option> <option value="1963">1963</option> <option value="1964">1964</option> <option value="1965">1965</option> <option value="1966">1966</option> <option value="1967">1967</option> <option value="1968">1968</option> <option value="1969">1969</option> <option value="1970">1970</option> <option value="1971">1971</option> <option value="1972">1972</option> <option value="1973">1973</option> <option value="1974">1974</option> <option value="1975">1975</option> <option value="1976">1976</option> <option value="1977">1977</option> <option value="1978">1978</option> <option value="1979">1979</option> <option value="1980">1980</option> <option value="1981">1981</option> <option value="1982">1982</option> <option value="1983">1983</option> <option value="1984">1984</option> <option value="1985">1985</option> <option value="1986">1986</option> <option value="1987">1987</option> <option value="1988">1988</option> <option value="1989">1989</option> <option value="1990">1990</option> <option value="1991">1991</option> <option value="1992">1992</option> <option value="1993">1993</option> <option value="1994">1994</option> <option value="1995">1995</option> <option value="1996">1996</option> <option value="1997">1997</option> <option value="1998">1998</option> <option value="1999">1999</option> <option value="2000">2000</option> <option value="2001">2001</option> <option value="2002">2002</option> <option value="2003">2003</option> <option value="2004">2004</option> <option value="2005">2005</option> <option value="2006">2006</option> <option value="2007">2007</option> <option value="2008">2008</option> <option value="2009">2009</option> <option value="2010">2010</option> <option value="2011">2011</option> <option value="2012">2012</option> <option value="2013">2013</option> <option value="2014">2014</option> <option value="2015">2015</option> <option value="2016">2016</option> <option value="2017">2017</option> <option value="2018">2018</option> <option value="2019">2019</option> <option value="2020">2020</option>                                           
                                        </select>
										 

												 </div>	
											  </div>
										 </div>
										 
										  
                                         
                                    </div><!-- panel-body -->
                                    <div class="panel-footer">
									 <div class="row" style="margin-bottom:1%;">
											  <div class="col-sm-2">
											 <button class="btn btn-primary mr5">Prev</button>
											  </div>
											  <div class="col-sm-9">
											   &nbsp;
											  </div>
											 
											  
											  <div class="col-sm-1">
											  <button class="btn btn-primary mr5">Next</button>
											  </div>
										 </div>
                                      
                                    </div>
                                </div><!-- panel -->
                                            
                                          
                                        </div><!-- tab-pane -->
                                        
                                        <div class="tab-pane" id="tab2-4">
                                           
                                            
                                     <div class="row">
                               <div class="col-md-12">
                                <form id="basicForm" action="#" novalidate="novalidate">
                                <div class="panel panel-default">
                                   <div class="panel-heading">
									<div class="col-sm-2" style="margin-left:-10px; margin-top:-10px">
									 <strong>Today Date : xx-xx-xxxx</strong>
									</div>
                                        
                                    </div><!-- panel-heading -->
                                    <div class="panel-body">
									<div class="col-md-12">
										
										
                                                
												<div class="row">
																								 
										 <div class="panel-body">
                                <div class="col-md-6">
                                
                                <div class="panel panel-default">
                                  
                                    <div class="panel-body">
                                         <div class="row">
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Visit #</label>
                                                <div class="col-sm-7">
                                                    <input autocorrect="off"  type="text" name="visit" id="visit" value="1" readonly="" class="form-control" placeholder="Type your name..." required="" style="width: 95%;">
                                                </div>
                                            </div><!-- form-group -->
											
											<div class="form-group">
                                                <label class="col-sm-3 control-label">Registration Date</label>
                                                <div class="col-sm-9" style="padding: 0;">
												 <div class="col-sm-4">
                                                   <select id="select-search-hide" data-placeholder="Choose One" name="regmonth" class="form-control" style="width:90%;margin-bottom: 1%;" tabindex="-1" title="">
                                            <option value="">Mar</option>
                                            <option value="1-January">Jan</option>
                                            <option value="2-February">Feb</option>
                                            <option value="3-March">Mar</option>
                                            <option value="4-April">Apr</option>
                                            <option value="5-May">May</option>
                                            <option value="6-June">Jun</option>
                                            <option value="7-July">Jul</option>
                                            <option value="8-August">Aug</option>
                                            <option value="9-September">Sep</option>
                                            <option value="10-October">Oct</option>
                                            <option value="11-November">Nov</option>
                                            <option value="12-December">Dec</option>
                                        </select>
										 

												 </div>	
												 <div class="col-sm-3">
												
                                                   <select id="select-search-hide" data-placeholder="Choose One" name="regdate" class="form-control" style="width:90%;margin-bottom: 1%;" tabindex="-1" title="">
												     <option value="">12</option>
														 <option value="1">1</option> <option value="2">2</option> <option value="3">3</option> <option value="4">4</option> <option value="5">5</option> <option value="6">6</option> <option value="7">7</option> <option value="8">8</option> <option value="9">9</option> <option value="10">10</option> <option value="11">11</option> <option value="12">12</option> <option value="13">13</option> <option value="14">14</option> <option value="15">15</option> <option value="16">16</option> <option value="17">17</option> <option value="18">18</option> <option value="19">19</option> <option value="20">20</option> <option value="21">21</option> <option value="22">22</option> <option value="23">23</option> <option value="24">24</option> <option value="25">25</option> <option value="26">26</option> <option value="27">27</option> <option value="28">28</option> <option value="29">29</option> <option value="30">30</option> <option value="31">31</option>                                          
                                        </select>
										 

												 </div>	
												 <div class="col-sm-4">
												                                                    <select id="select-search-hide" data-placeholder="Choose One" name="regyear" class="form-control" style="width:90%;margin-bottom: 1%;" tabindex="-1" title="">
                                            <option value="">2015</option>
														 <option value="1947">1947</option> <option value="1948">1948</option> <option value="1949">1949</option> <option value="1950">1950</option> <option value="1951">1951</option> <option value="1952">1952</option> <option value="1953">1953</option> <option value="1954">1954</option> <option value="1955">1955</option> <option value="1956">1956</option> <option value="1957">1957</option> <option value="1958">1958</option> <option value="1959">1959</option> <option value="1960">1960</option> <option value="1961">1961</option> <option value="1962">1962</option> <option value="1963">1963</option> <option value="1964">1964</option> <option value="1965">1965</option> <option value="1966">1966</option> <option value="1967">1967</option> <option value="1968">1968</option> <option value="1969">1969</option> <option value="1970">1970</option> <option value="1971">1971</option> <option value="1972">1972</option> <option value="1973">1973</option> <option value="1974">1974</option> <option value="1975">1975</option> <option value="1976">1976</option> <option value="1977">1977</option> <option value="1978">1978</option> <option value="1979">1979</option> <option value="1980">1980</option> <option value="1981">1981</option> <option value="1982">1982</option> <option value="1983">1983</option> <option value="1984">1984</option> <option value="1985">1985</option> <option value="1986">1986</option> <option value="1987">1987</option> <option value="1988">1988</option> <option value="1989">1989</option> <option value="1990">1990</option> <option value="1991">1991</option> <option value="1992">1992</option> <option value="1993">1993</option> <option value="1994">1994</option> <option value="1995">1995</option> <option value="1996">1996</option> <option value="1997">1997</option> <option value="1998">1998</option> <option value="1999">1999</option> <option value="2000">2000</option> <option value="2001">2001</option> <option value="2002">2002</option> <option value="2003">2003</option> <option value="2004">2004</option> <option value="2005">2005</option> <option value="2006">2006</option> <option value="2007">2007</option> <option value="2008">2008</option> <option value="2009">2009</option> <option value="2010">2010</option> <option value="2011">2011</option> <option value="2012">2012</option> <option value="2013">2013</option> <option value="2014">2014</option> <option value="2015">2015</option> <option value="2016">2016</option> <option value="2017">2017</option> <option value="2018">2018</option> <option value="2019">2019</option> <option value="2020">2020</option>                                           
                                        </select>
										 

												 </div>	
                                                </div>
                                            </div><!-- form-group -->
                                            
										   <div class="form-group">
                                                <label class="col-sm-3 control-label">Authorized Period From</label>
                                                <div class="col-sm-9" style="padding: 0;">
												 <div class="col-sm-4">
                                                   <select id="select-search-hide" data-placeholder="Choose One" name="regmonth" class="form-control" style="width:90%;margin-bottom: 1%;" tabindex="-1" title="">
                                            <option value="">Mar</option>
                                            <option value="1-January">Jan</option>
                                            <option value="2-February">Feb</option>
                                            <option value="3-March">Mar</option>
                                            <option value="4-April">Apr</option>
                                            <option value="5-May">May</option>
                                            <option value="6-June">Jun</option>
                                            <option value="7-July">Jul</option>
                                            <option value="8-August">Aug</option>
                                            <option value="9-September">Sep</option>
                                            <option value="10-October">Oct</option>
                                            <option value="11-November">Nov</option>
                                            <option value="12-December">Dec</option>
                                        </select>
										 

												 </div>	
												 <div class="col-sm-3">
												
                                                   <select id="select-search-hide" data-placeholder="Choose One" name="regdate" class="form-control" style="width:90%;margin-bottom: 1%;" tabindex="-1" title="">
												     <option value="">12</option>
														 <option value="1">1</option> <option value="2">2</option> <option value="3">3</option> <option value="4">4</option> <option value="5">5</option> <option value="6">6</option> <option value="7">7</option> <option value="8">8</option> <option value="9">9</option> <option value="10">10</option> <option value="11">11</option> <option value="12">12</option> <option value="13">13</option> <option value="14">14</option> <option value="15">15</option> <option value="16">16</option> <option value="17">17</option> <option value="18">18</option> <option value="19">19</option> <option value="20">20</option> <option value="21">21</option> <option value="22">22</option> <option value="23">23</option> <option value="24">24</option> <option value="25">25</option> <option value="26">26</option> <option value="27">27</option> <option value="28">28</option> <option value="29">29</option> <option value="30">30</option> <option value="31">31</option>                                          
                                        </select>
										 

												 </div>	
												 <div class="col-sm-4">
												                                                    <select id="select-search-hide" data-placeholder="Choose One" name="regyear" class="form-control" style="width:90%;margin-bottom: 1%;" tabindex="-1" title="">
                                            <option value="">2015</option>
														 <option value="1947">1947</option> <option value="1948">1948</option> <option value="1949">1949</option> <option value="1950">1950</option> <option value="1951">1951</option> <option value="1952">1952</option> <option value="1953">1953</option> <option value="1954">1954</option> <option value="1955">1955</option> <option value="1956">1956</option> <option value="1957">1957</option> <option value="1958">1958</option> <option value="1959">1959</option> <option value="1960">1960</option> <option value="1961">1961</option> <option value="1962">1962</option> <option value="1963">1963</option> <option value="1964">1964</option> <option value="1965">1965</option> <option value="1966">1966</option> <option value="1967">1967</option> <option value="1968">1968</option> <option value="1969">1969</option> <option value="1970">1970</option> <option value="1971">1971</option> <option value="1972">1972</option> <option value="1973">1973</option> <option value="1974">1974</option> <option value="1975">1975</option> <option value="1976">1976</option> <option value="1977">1977</option> <option value="1978">1978</option> <option value="1979">1979</option> <option value="1980">1980</option> <option value="1981">1981</option> <option value="1982">1982</option> <option value="1983">1983</option> <option value="1984">1984</option> <option value="1985">1985</option> <option value="1986">1986</option> <option value="1987">1987</option> <option value="1988">1988</option> <option value="1989">1989</option> <option value="1990">1990</option> <option value="1991">1991</option> <option value="1992">1992</option> <option value="1993">1993</option> <option value="1994">1994</option> <option value="1995">1995</option> <option value="1996">1996</option> <option value="1997">1997</option> <option value="1998">1998</option> <option value="1999">1999</option> <option value="2000">2000</option> <option value="2001">2001</option> <option value="2002">2002</option> <option value="2003">2003</option> <option value="2004">2004</option> <option value="2005">2005</option> <option value="2006">2006</option> <option value="2007">2007</option> <option value="2008">2008</option> <option value="2009">2009</option> <option value="2010">2010</option> <option value="2011">2011</option> <option value="2012">2012</option> <option value="2013">2013</option> <option value="2014">2014</option> <option value="2015">2015</option> <option value="2016">2016</option> <option value="2017">2017</option> <option value="2018">2018</option> <option value="2019">2019</option> <option value="2020">2020</option>                                           
                                        </select>
										 

												 </div>	
                                                </div>
                                            </div><!-- form-group -->
                                            
                                            
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Authorized Period To</label>
                                                <div class="col-sm-9" style="padding: 0;">
												 <div class="col-sm-4">
                                                   <select id="select-search-hide" data-placeholder="Choose One" name="regmonth" class="form-control" style="width:90%;margin-bottom: 1%;" tabindex="-1" title="">
                                            <option value="">Mar</option>
                                            <option value="1-January">Jan</option>
                                            <option value="2-February">Feb</option>
                                            <option value="3-March">Mar</option>
                                            <option value="4-April">Apr</option>
                                            <option value="5-May">May</option>
                                            <option value="6-June">Jun</option>
                                            <option value="7-July">Jul</option>
                                            <option value="8-August">Aug</option>
                                            <option value="9-September">Sep</option>
                                            <option value="10-October">Oct</option>
                                            <option value="11-November">Nov</option>
                                            <option value="12-December">Dec</option>
                                        </select>
										 

												 </div>	
												 <div class="col-sm-3">
												
                                                   <select id="select-search-hide" data-placeholder="Choose One" name="regdate" class="form-control" style="width:90%;margin-bottom: 1%;" tabindex="-1" title="">
												     <option value="">22</option>
														 <option value="1">1</option> <option value="2">2</option> <option value="3">3</option> <option value="4">4</option> <option value="5">5</option> <option value="6">6</option> <option value="7">7</option> <option value="8">8</option> <option value="9">9</option> <option value="10">10</option> <option value="11">11</option> <option value="12">12</option> <option value="13">13</option> <option value="14">14</option> <option value="15">15</option> <option value="16">16</option> <option value="17">17</option> <option value="18">18</option> <option value="19">19</option> <option value="20">20</option> <option value="21">21</option> <option value="22">22</option> <option value="23">23</option> <option value="24">24</option> <option value="25">25</option> <option value="26">26</option> <option value="27">27</option> <option value="28">28</option> <option value="29">29</option> <option value="30">30</option> <option value="31">31</option>                                          
                                        </select>
										 

												 </div>	
												 <div class="col-sm-4">
												                                                    <select id="select-search-hide" data-placeholder="Choose One" name="regyear" class="form-control" style="width:90%;margin-bottom: 1%;" tabindex="-1" title="">
                                            <option value="">2015</option>
														 <option value="1947">1947</option> <option value="1948">1948</option> <option value="1949">1949</option> <option value="1950">1950</option> <option value="1951">1951</option> <option value="1952">1952</option> <option value="1953">1953</option> <option value="1954">1954</option> <option value="1955">1955</option> <option value="1956">1956</option> <option value="1957">1957</option> <option value="1958">1958</option> <option value="1959">1959</option> <option value="1960">1960</option> <option value="1961">1961</option> <option value="1962">1962</option> <option value="1963">1963</option> <option value="1964">1964</option> <option value="1965">1965</option> <option value="1966">1966</option> <option value="1967">1967</option> <option value="1968">1968</option> <option value="1969">1969</option> <option value="1970">1970</option> <option value="1971">1971</option> <option value="1972">1972</option> <option value="1973">1973</option> <option value="1974">1974</option> <option value="1975">1975</option> <option value="1976">1976</option> <option value="1977">1977</option> <option value="1978">1978</option> <option value="1979">1979</option> <option value="1980">1980</option> <option value="1981">1981</option> <option value="1982">1982</option> <option value="1983">1983</option> <option value="1984">1984</option> <option value="1985">1985</option> <option value="1986">1986</option> <option value="1987">1987</option> <option value="1988">1988</option> <option value="1989">1989</option> <option value="1990">1990</option> <option value="1991">1991</option> <option value="1992">1992</option> <option value="1993">1993</option> <option value="1994">1994</option> <option value="1995">1995</option> <option value="1996">1996</option> <option value="1997">1997</option> <option value="1998">1998</option> <option value="1999">1999</option> <option value="2000">2000</option> <option value="2001">2001</option> <option value="2002">2002</option> <option value="2003">2003</option> <option value="2004">2004</option> <option value="2005">2005</option> <option value="2006">2006</option> <option value="2007">2007</option> <option value="2008">2008</option> <option value="2009">2009</option> <option value="2010">2010</option> <option value="2011">2011</option> <option value="2012">2012</option> <option value="2013">2013</option> <option value="2014">2014</option> <option value="2015">2015</option> <option value="2016">2016</option> <option value="2017">2017</option> <option value="2018">2018</option> <option value="2019">2019</option> <option value="2020">2020</option>                                           
                                        </select>
										 

												 </div>	
                                                </div>
                                            </div><!-- form-group -->
                                        </div><!-- row -->
                                    </div><!-- panel-body -->
                                     
                                </div><!-- panel -->
								
                                
                                
                            </div>
							
							<div class="col-md-6">
                                <div class="table-responsive">
								<button class="btn btn-primary" style="margin-bottom:1%; width:100%">Visits Table</button>
                                    <table class="table table-primary mb30">
                                        <thead>
                                          <tr>
                                            
                                            <th>First Name</th>
                                            <th>From</th>
                                            <th>To</th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                          <tr>
                                            
                                            <td>Prabu Vasanth</td>
                                            <td>Mar - 11 - 2015</td>
                                            <td>Mar - 21 - 2015</td>
                                          </tr>
                                          <tr>
                                            
                                            <td>Naren Kumar</td>
                                            <td>Feb - 10 - 2015</td>
                                            <td>Feb - 20 - 2015</td>
                                          </tr>
                                          <tr>
                                            
                                            <td>Zaheer Khan</td>
                                            <td>Feb - 05 - 2015</td>
                                            <td>Feb - 15 - 2015</td>
                                          </tr>
										  
										  <tr>
                                            
                                            <td>Sachin</td>
                                            <td>Jan - 24 - 2015</td>
                                            <td>Feb - 04 - 2015</td>
                                          </tr>
                                        </tbody>
                                    </table>
                                </div><!-- table-responsive -->
                            </div>
							
                           
                                            
                                           </div>
										</div>
									</div>
								</div>                                        
                             <div class="panel-footer">
									 <div class="row" style="margin-bottom:1%;">
											  <div class="col-sm-2">
											 <button class="btn btn-primary mr5">Prev</button>
											  </div>
											  <div class="col-sm-9">
											   &nbsp;
											  </div>
											 
											  
											  <div class="col-sm-1">
											  <button class="btn btn-primary mr5">Next</button>
											  </div>
										 </div>
                                      
                                    </div>
						</div></form>
					</div>			
					
                 </div>
				 
                                        </div><!-- tab-pane -->
                                        
                                        <div class="tab-pane" id="tab3-4">
                                           <div class="panel panel-default">
                                    <div class="panel-heading" style="padding:9px 0 2px 12px">
                                        <button class="btn btn-primary mr5" style="margin-bottom:1%; ">Temporary US Address</button>
                                    </div><!-- panel-heading -->
                                   <div class="panel-body">
										 <div class="row" style="margin-bottom:1%;">
										 
											  <div class="col-sm-2">
											  Address1 <span class="asterisk">*</span>
											  </div>
											  <div class="col-sm-3">
											  <input autocorrect="off"  type="text" name="title" id="title" value="" class="form-control" placeholder="Address1" required="">
											  </div>
											  <div class="col-sm-1">
											  &nbsp;
											  </div>
											  <div class="col-sm-2">
											  Country<span class="asterisk">*</span>
											  </div>
											  <div class="col-sm-3">
											  <input autocorrect="off"  type="text" name="title" id="title" value="" class="form-control" placeholder="Country" required="">
											  </div>
										 </div>
										 <div class="row" style="margin-bottom:1%;">
											  <div class="col-sm-2">
											  Address2<span class="asterisk">*</span>
											  </div>
											  <div class="col-sm-3">
											  <input autocorrect="off"  type="text" name="title" id="title" value="" class="form-control" placeholder="Address2" required="">
											  </div>
											 	<div class="col-sm-1">
											  &nbsp;
											  </div>
											  <div class="col-sm-2">
											 Zip / Postal Code <span class="asterisk">*</span>
											  </div>
											  <div class="col-sm-3">
											  <input autocorrect="off"  type="text" name="title" id="title" value="" class="form-control" placeholder="Zip / Postal Code" required="">
											  </div>								  
											  
											  
											  
										 </div>									 	 
										
										
										   <div class="row" style="margin-bottom:1%;">
											  <div class="col-sm-2">
											  City <span class="asterisk">*</span>
											  </div>
											  <div class="col-sm-3">
											  <input autocorrect="off"  type="text" name="title" id="title" value="" class="form-control" placeholder="City" required="">
											  </div>
											  <div class="col-sm-1">
											  &nbsp;
											  </div>
											  
											   <div class="col-sm-2">
											  Phone <span class="asterisk">*</span>
											  </div>
											  <div class="col-sm-3">
											  <input autocorrect="off"  type="text" name="title" id="title" value="" class="form-control" placeholder="Phone" required="">
											  </div>
											  
											 											
										 </div>
										 <div class="row" style="margin-bottom:1%;">
											  <div class="col-sm-2">
											  State / Country <span class="asterisk">*</span>
											  </div>
											  <div class="col-sm-3">
											  <input autocorrect="off"  type="text" name="title" id="title" value="" class="form-control" placeholder="State / Country" required="">
											  </div>
											  											
										 </div>
										 <div class="row" style="margin-bottom:1%;">
											 
											  
										 </div>
										 <div class="row" style="margin-bottom:1%;">
											  <div class="col-sm-2">
											   Comments <span class="asterisk">*</span>
											  </div>
											  <div class="col-sm-9">
											  <textarea autocorrect="off"  class="form-control" rows="4" cols="10" placeholder="Comments" required=""></textarea>
											  
											  </div>											  
											 
											  
										 </div> 
										
										  									 
										  
                                         
                                    </div><!-- panel-body -->
								
							<div class="col-md-12" style="padding: 0px;  margin-top: 20px;">
							
							
                                <form id="basicForm" action="#" novalidate="novalidate">
                                <div class="panel panel-default">
                                   <div class="panel-heading" style="padding:9px 0 2px 12px">
                                        <button class="btn btn-primary mr5" style="margin-bottom:1%; ">Baltimore Address same as US Address</button>
                                    </div><!-- panel-heading -->
                                    <div class="panel-body">
										<div class="row" style="margin-bottom:1%;">
										 
											  <div class="col-sm-2">
											  Address1 <span class="asterisk">*</span>
											  </div>
											  <div class="col-sm-3">
											  <input autocorrect="off"  type="text" name="title" id="title" value="" class="form-control" placeholder="Address1" required="">
											  </div>
											  <div class="col-sm-1">
											  &nbsp;
											  </div>
											  <div class="col-sm-2">
											  Country<span class="asterisk">*</span>
											  </div>
											  <div class="col-sm-3">
											  <input autocorrect="off"  type="text" name="title" id="title" value="" class="form-control" placeholder="Country" required="">
											  </div>
										 </div>
										 <div class="row" style="margin-bottom:1%;">
											  <div class="col-sm-2">
											  Address2<span class="asterisk">*</span>
											  </div>
											  <div class="col-sm-3">
											  <input autocorrect="off"  type="text" name="title" id="title" value="" class="form-control" placeholder="Address2" required="">
											  </div>
											 	<div class="col-sm-1">
											  &nbsp;
											  </div>
											  <div class="col-sm-2">
											 Zip / Postal Code <span class="asterisk">*</span>
											  </div>
											  <div class="col-sm-3">
											  <input autocorrect="off"  type="text" name="title" id="title" value="" class="form-control" placeholder="Zip / Postal Code" required="">
											  </div>								  
											  
											  
											  
										 </div>									 	 
										
										
										   <div class="row" style="margin-bottom:1%;">
											  <div class="col-sm-2">
											  City <span class="asterisk">*</span>
											  </div>
											  <div class="col-sm-3">
											  <input autocorrect="off"  type="text" name="title" id="title" value="" class="form-control" placeholder="City" required="">
											  </div>
											  <div class="col-sm-1">
											  &nbsp;
											  </div>
											  
											   <div class="col-sm-2">
											  Phone <span class="asterisk">*</span>
											  </div>
											  <div class="col-sm-3">
											  <input autocorrect="off"  type="text" name="title" id="title" value="" class="form-control" placeholder="Phone" required="">
											  </div>
											  
											 											
										 </div>
										 <div class="row" style="margin-bottom:1%;">
											  <div class="col-sm-2">
											  State / Country <span class="asterisk">*</span>
											  </div>
											  <div class="col-sm-3">
											  <input autocorrect="off"  type="text" name="title" id="title" value="" class="form-control" placeholder="State / Country" required="">
											  </div>
											  											
										 </div>
										 <div class="row" style="margin-bottom:1%;">
											 
											  
										 </div>
										 <div class="row" style="margin-bottom:1%;">
											  <div class="col-sm-2">
											   Comments <span class="asterisk">*</span>
											  </div>
											  <div class="col-sm-9">
											  <textarea autocorrect="off"  class="form-control" rows="4" cols="10" placeholder="Comments" required=""></textarea>
											  
											  </div>											  
											 
											  
										 </div> 
										  
                                         
                                    </div><!-- panel-body -->
                                    <div class="panel-footer">
									 <div class="row" style="margin-bottom:1%;">
											  <div class="col-sm-2">
											 <button class="btn btn-primary mr5">Prev</button>
											  </div>
											  <div class="col-sm-9">
											   &nbsp;
											  </div>
											 
											  
											  <div class="col-sm-1">
											  <button class="btn btn-primary mr5">Next</button>
											  </div>
										 </div>
                                      
                                    </div><!-- panel-footer -->  
                                </div><!-- panel -->
                                </form>                                
                            </div>
                                    
                                </div><!-- panel -->
                      
                                           
                      
                                        </div><!-- tab-pane -->
										 <div class="tab-pane" id="tab4-4">
                                           <form id="basicForm" action="#" novalidate="novalidate">
                                <div class="panel panel-default">
                                  
                                    <div class="panel-body">
										<div class="row">
										<div class="col-md-9">
										<div class="col-md-4">The Purpose of these funds is for</div>
										
              <script type="text/javascript">

function yesnoCheck() {
    if (document.getElementById('yesCheck').checked) {
        document.getElementById('ifYes').style.display = 'block';
    }
    else document.getElementById('ifYes').style.display = 'none';

}

</script>
  	
                                               <div class="col-md-2"> <!--<div class="rdio rdio-primary">
                                                    <input autocorrect="off"  type="radio" id="male" value="m" name="gender" required="">
                                                    
                                                </div>-->
												<input autocorrect="off"  type="radio" onclick="javascript:yesnoCheck();" name="yesno" id="yesCheck">
												<label for="male">Instituation</label>
												</div><!-- rdio -->
                                             <div class="col-md-2"> 
 <input autocorrect="off"  type="radio" onclick="javascript:yesnoCheck();" name="yesno" id="noCheck">
 <label for="female">Individual</label>
											 <!--<div class="rdio rdio-primary">
                                                    <input autocorrect="off"  type="radio" value="f" id="female" name="gender">
                                                    
                                                </div>--></div><br><!-- rdio -->
												<div class="row" id="ifYes" style="display:visible">
												 <div class="panel-body">
										 <div class="row" style="margin-bottom:1%;">
											  <div class="col-sm-2">
											  Title <span class="asterisk">*</span>
											  </div>
											  <div class="col-sm-3">
											  <input autocorrect="off"  type="text" name="title" id="title" value="" class="form-control" placeholder="Title" required="">
											  </div>
											  <div class="col-sm-1">
											  &nbsp;
											  </div>
											  <div class="col-sm-2">
											  First Name <span class="asterisk">*</span>
											  </div>
											  <div class="col-sm-3">
											  <input autocorrect="off"  type="text" name="title" id="title" value="" class="form-control" placeholder="First Name" required="">
											  </div>
										 </div>
										  <div class="row" style="margin-bottom:1%;">
											  <div class="col-sm-2">
											  Middle Name <span class="asterisk">*</span>
											  </div>
											  <div class="col-sm-3">
											  <input autocorrect="off"  type="text" name="title" id="title" value="" class="form-control" placeholder="Middle Name" required="">
											  </div>
											  <div class="col-sm-1">
											  &nbsp;
											  </div>
											  <div class="col-sm-2">
											  Address1 <span class="asterisk">*</span>
											  </div>
											  <div class="col-sm-3">
											  <input autocorrect="off"  type="text" name="title" id="title" value="" class="form-control" placeholder="Address1" required="">
											  </div>
										 </div>
										 	  <div class="row" style="margin-bottom:1%;">
											  <div class="col-sm-2">
											  Last Name <span class="asterisk">*</span>
											  </div>
											  <div class="col-sm-3">
											  <input autocorrect="off"  type="text" name="title" id="title" value="" class="form-control" placeholder="Address1" required="">
											  </div>
											  <div class="col-sm-1">
											  &nbsp;
											  </div>
											  <div class="col-sm-2">
											 Address2 <span class="asterisk">*</span>
											  </div>
											  <div class="col-sm-3">
											  <input autocorrect="off"  type="text" name="title" id="title" value="" class="form-control" placeholder="Address2" required="">
											  </div>
										 </div>
										 <div class="row" style="margin-bottom:1%;">
											  <div class="col-sm-2">
											  City <span class="asterisk">*</span>
											  </div>
											  <div class="col-sm-3">
											  <input autocorrect="off"  type="text" name="title" id="title" value="" class="form-control" placeholder="City" required="">
											  </div>
											  <div class="col-sm-1">
											  &nbsp;
											  </div>
											  <div class="col-sm-2">
											 State/Country <span class="asterisk">*</span>
											  </div>
											  <div class="col-sm-3">
											  <input autocorrect="off"  type="text" name="title" id="title" value="" class="form-control" placeholder="State/Country" required="">
											  </div>
										 </div>
										 <div class="row" style="margin-bottom:1%;">
											  <div class="col-sm-2">
											  Phone <span class="asterisk">*</span>
											  </div>
											  <div class="col-sm-3">
											  <input autocorrect="off"  type="text" name="title" id="title" value="" class="form-control" placeholder="Phone" required="">
											  </div>
											  <div class="col-sm-1">
											  &nbsp;
											  </div>
											  <div class="col-sm-2">
											 Zip / Postal Code <span class="asterisk">*</span>
											  </div>
											  <div class="col-sm-3">
											  <input autocorrect="off"  type="text" name="title" id="title" value="" class="form-control" placeholder="Zip / Postal Code" required="">
											  </div>
										 </div>
										  <div class="row" style="margin-bottom:1%;">
											  <div class="col-sm-2">
											  Comments <span class="asterisk">*</span>
											  </div>
											  <div class="col-sm-3">
											  <input autocorrect="off"  type="text" name="title" id="title" value="" class="form-control" placeholder="Comments" required="">
											  </div>
											 
										 </div>	
										  
                                         
                                    </div><!-- panel-body -->
												</div>                                            
										</div>
									</div>
                                        
                                    </div><!-- panel-body -->
                                    <div class="panel-footer">
                                      <div class="row" style="margin-bottom:1%;">
											  <div class="col-sm-2">
											 <button class="btn btn-primary mr5">Prev</button>
											  </div>
											  <div class="col-sm-9">
											   &nbsp;
											  </div>
											 
											  
											  <div class="col-sm-1">
											  <button class="btn btn-primary mr5">Next</button>
											  </div>
										 </div>
                                    </div><!-- panel-footer -->  
                                </div><!-- panel -->
                                </form>
                      
                                          
                      
                                            
                                        </div><!-- tab-pane -->
										 <div class="tab-pane" id="tab5-4">
                                            
									<form id="basicForm" action="#" novalidate="novalidate">
                                <div class="panel panel-default">
                                 
                                    <div class="panel-body">
									<div class="col-md-12">
										<div class="row">
										
										<div class="col-md-4">Enter all that apply</div>
										</div>
										
                                                
												<div class="row">
												 <div class="panel-body">
												 
										 <div class="row" style="margin-bottom:1%;">
											  <div class="col-md-2">
											 <div class="ckbox ckbox-primary">
                                                    <input autocorrect="off"  type="checkbox" id="Yeshiva" value="Yeshiva" name="active" required="">
                                                    <label for="Yeshiva">Yeshiva  </label>
                                                </div>
											  </div>
											  <div class="col-md-2">
											 <div class="ckbox ckbox-primary">
                                                    <input autocorrect="off"  type="checkbox" id="Kiruv" value="Kiruv" name="active" required="">
                                                    <label for="Kiruv">Kiruv </label>
                                                </div>
											  </div>
											  <div class="col-md-2">
											<div class="ckbox ckbox-primary">
                                                    <input autocorrect="off"  type="checkbox" id="Free Loan" value="Free Loan" name="active" required="">
                                                    <label for="Free Loan">Free Loan </label>
                                                </div>
											  </div>
											  <div class="col-md-2">
											 <div class="ckbox ckbox-primary">
                                                    <input autocorrect="off"  type="checkbox" id="Hanchnosas" value="Hanchnosas" name="active" required="">
                                                    <label for="Hanchnosas">Hanchnosas </label>
                                                </div>
											  </div>
											  <div class="col-md-2">
											 <div class="ckbox ckbox-primary">
                                                    <input autocorrect="off"  type="checkbox" id="Medical" value="Medical" name="active" required="">
                                                    <label for="Medical">Medical</label>
                                                </div>
											  </div>
										 </div>
										 <div class="row" style="margin-bottom:1%;">
											  <div class="col-md-2">
											 <div class="ckbox ckbox-primary">
                                                    <input autocorrect="off"  type="checkbox" id="Mental" value="Mental" name="active" required="">
                                                    <label for="Mental">Mental   </label>
                                                </div>
											  </div>
											  <div class="col-md-2">
											 <div class="ckbox ckbox-primary">
                                                    <input autocorrect="off"  type="checkbox" id="Torah" value="Torah" name="active" required="">
                                                    <label for="Torah">Torah </label>
                                                </div>
											  </div>
											  <div class="col-md-2">
											<div class="ckbox ckbox-primary">
                                                    <input autocorrect="off"  type="checkbox" id="Kollel" value="Kollel" name="active" required="">
                                                    <label for="Kollel">Kollel </label>
                                                </div>
											  </div>
											  <div class="col-md-2">
											 <div class="ckbox ckbox-primary">
                                                    <input autocorrect="off"  type="checkbox" id="Charity" value="Charity" name="active" required="">
                                                    <label for="Charity">Charity </label>
                                                </div>
											  </div>
											  <div class="col-md-2">
											 <div class="ckbox ckbox-primary">
                                                    <input autocorrect="off"  type="checkbox" id="Business" value="Business" name="active" required="">
                                                    <label for="Business">Business </label>
                                                </div>
											  </div>
										 </div>
										 <div class="row" style="margin-bottom:1%;">
											  <div class="col-md-2">
											 <div class="ckbox ckbox-primary">
                                                    <input autocorrect="off"  type="checkbox" id="Parnosso" value="Parnosso" name="active" required="">
                                                    <label for="Parnosso">Parnosso  </label>
                                                </div>
											  </div>
											  <div class="col-md-2">
											 <div class="ckbox ckbox-primary">
                                                    <input autocorrect="off"  type="checkbox" id="Debets" value="Debets" name="active" required="">
                                                    <label for="Debets">Debets </label>
                                                </div>
											  </div>
											  <div class="col-md-2">
											<div class="ckbox ckbox-primary">
                                                    <input autocorrect="off"  type="checkbox" id="Girls" value="Girls" name="active" required="">
                                                    <label for="Girls">Girls </label>
                                                </div>
											  </div>
											  <div class="col-md-2">
											 <div class="ckbox ckbox-primary">
                                                    <input autocorrect="off"  type="checkbox" id="Sefer" value="Sefer" name="active" required="">
                                                    <label for="Sefer">Sefer </label>
                                                </div>
											  </div>
											  <div class="col-md-2">
											 <div class="ckbox ckbox-primary">
                                                    <input autocorrect="off"  type="checkbox" id="Chesed" value="Chesed" name="active" required="">
                                                    <label for="Chesed">Chesed </label>
                                                </div>
											  </div>
										 </div>
										 <div class="row" style="margin-bottom:1%;">
											  <div class="col-md-2">
											 <div class="ckbox ckbox-primary">
                                                    <input autocorrect="off"  type="checkbox" id="Judaism" value="Judaism" name="active" required="">
                                                    <label for="Judaism">Judaism  </label>
                                                </div>
											  </div>
											  <div class="col-md-2">
											 <div class="ckbox ckbox-primary">
                                                    <input autocorrect="off"  type="checkbox" id="Adult" value="Adult" name="active" required="">
                                                    <label for="Adult">Adult </label>
                                                </div>
											  </div>
											  <div class="col-md-2">
											<div class="ckbox ckbox-primary">
                                                    <input autocorrect="off"  type="checkbox" id="Chinuch" value="Chinuch" name="active" required="">
                                                    <label for="Chinuch">Chinuch</label>
                                                </div>
											  </div>
											  <div class="col-md-2">
											 <div class="ckbox ckbox-primary">
                                                    <input autocorrect="off"  type="checkbox" id="Other" value="Other" name="active" required="">
                                                    <label for="Other">Other </label>
                                                </div>
											  </div>
											  <div class="col-md-2">
											 <div class="ckbox ckbox-primary">
                                                    <input autocorrect="off"  type="text" name="other" class="form-control" placeholder="Other" required="">
                                                   
                                                </div>
											  </div>
										 </div>
										  <div class="row" style="margin-bottom:1%;">
										   <div class="col-md-3">Purpose for Funds Comments
										   </div>
										    <div class="col-md-7">
											<textarea autocorrect="off"  class="form-control" rows="8" cols="15" placeholder="Purpose for Funds Comments"></textarea>
											
											</div>
										  </div>
											<div class="row" style="margin-bottom:1%;">
										   <div class="col-md-4">We suggest you help the above with a : 
										   </div>
										    <div class="col-md-3">
											<select id="select-search-hide" data-placeholder="Choose One" name="regmonth" class="form-control" style="width:90%;margin-bottom: 1%;" tabindex="-1" title="">
                                            <option value="">Substantial Donation</option>
                                            <option value="Substantial Donation">Substantial Donation</option>
                                            <option value="Generous Donation">Generous Donation</option>
                                            <option value="Standard Donation">Standard Donation</option>
                                            <option value="One Meal Donation">One Meal Donation</option>
                                            <option value="Token Donation">Token Donation</option>
                                            <option value="No Special Donation">No Special Donation</option>
                                        </select>
											
											</div>
										  </div>
		                                         
                                    </div><!-- panel-body -->									   
												</div>                                            
                                           </div>										   
									   </div>	
									<div class="panel-footer">
									 <div class="row" style="margin-bottom:1%;">
											  <div class="col-sm-2">
											 <button class="btn btn-primary mr5">Prev</button>
											  </div>
											  <div class="col-sm-9">
											   &nbsp;
											  </div>											 
											  
											  <div class="col-sm-1">
											  <button class="btn btn-primary mr5">Next</button>
											  </div>
										 </div>
                                      
                                    </div><!-- panel-footer -->									   
									</div>									
							    </form>		
                      
                                           
                      
                                        </div><!-- tab-pane -->
										 <div class="tab-pane" id="tab6-4">
                                            
										<form id="basicForm" action="#" novalidate="novalidate">
                                <div class="panel panel-default">
                                   
                                    <div class="panel-body">
									<div class="col-md-12">
										<div class="row">
										
										<div class="col-md-6">Be it known that we have examined the: (check all that apply)</div>
										</div>
										
                                                
												<div class="row">
												 <div class="panel-body">
												 
										 <div class="row" style="margin-bottom:1%;">
											  <div class="col-md-2">
											 <div class="ckbox ckbox-primary">
                                                    <input autocorrect="off"  type="checkbox" id="Documents" value="Documents" name="active" required="">
                                                    <label for="Documents">Documents  </label>
                                                </div>
											  </div>
											  <div class="col-md-2">
											 <div class="ckbox ckbox-primary">
                                                    <input autocorrect="off"  type="checkbox" id="Identification" value="Identification" name="active" required="">
                                                    <label for="Identification">Identification </label>
                                                </div>
											  </div>
											  <div class="col-md-2">
											<div class="ckbox ckbox-primary">
                                                    <input autocorrect="off"  type="checkbox" id="References" value="References" name="active" required="">
                                                    <label for="References">References </label>
                                                </div>
											  </div>
											  	  <div class="col-md-1">
											 <div class="ckbox ckbox-primary">
                                                    <input autocorrect="off"  type="checkbox" id="Other" value="Other" name="active" required="">
                                                    <label for="Other">Other </label>
                                                </div>
											  </div>
											  <div class="col-md-3">
											 <div class="ckbox ckbox-primary">
                                                    <input autocorrect="off"  type="text" name="other" class="form-control" placeholder="Other" required="">
                                                   
                                                </div>
											  </div>
											 
										 </div>
										 
										 
										 
										  <div class="row" style="margin-bottom:1%;">
										   <div class="col-md-3">Comments
										   </div>
										    <div class="col-md-7">
											<textarea autocorrect="off"  class="form-control" rows="8" cols="15" placeholder="Comments"></textarea>
											
											</div>
										  </div>
											<div class="row" style="margin-bottom:1%;">
										   <div class="col-md-3">Authorizing Rabbis : 
										   </div>
										    <div class="col-md-3">
											<select id="select-search-hide" data-placeholder="Choose One" name="regmonth" class="form-control" style="width:90%;margin-bottom: 1%;" tabindex="-1" title="">
                                            <option value="">Substantial Donation</option>
                                            <option value="Substantial Donation">Substantial Donation</option>
                                            <option value="Generous Donation">Generous Donation</option>
                                            <option value="Standard Donation">Standard Donation</option>
                                            <option value="One Meal Donation">One Meal Donation</option>
                                            <option value="Token Donation">Token Donation</option>
                                            <option value="No Special Donation">No Special Donation</option>
                                        </select>
											
											</div>
										  </div>
		                                  <div class="row" style="margin-bottom:1%;">
										   <div class="col-md-3">Driver : 
										   </div>
										    <div class="col-md-3">
											<select id="select-search-hide" data-placeholder="Choose One" name="regmonth" class="form-control" style="width:90%;margin-bottom: 1%;" tabindex="-1" title="">
                                            <option value="">Substantial Donation</option>
                                            <option value="Substantial Donation">Substantial Donation</option>
                                            <option value="Generous Donation">Generous Donation</option>
                                            <option value="Standard Donation">Standard Donation</option>
                                            <option value="One Meal Donation">One Meal Donation</option>
                                            <option value="Token Donation">Token Donation</option>
                                            <option value="No Special Donation">No Special Donation</option>
                                        </select>
											
											</div>
										  </div>
<div class="row" style="margin-bottom:1%;">
										   <div class="col-md-3">Host : 
										   </div>
										    <div class="col-md-3">
											<select id="select-search-hide" data-placeholder="Choose One" name="regmonth" class="form-control" style="width:90%;margin-bottom: 1%;" tabindex="-1" title="">
                                            <option value="">Substantial Donation</option>
                                            <option value="Substantial Donation">Substantial Donation</option>
                                            <option value="Generous Donation">Generous Donation</option>
                                            <option value="Standard Donation">Standard Donation</option>
                                            <option value="One Meal Donation">One Meal Donation</option>
                                            <option value="Token Donation">Token Donation</option>
                                            <option value="No Special Donation">No Special Donation</option>
                                        </select>
											
											</div>
										  </div>	
										<div class="row" style="margin-bottom:1%;">
										   <div class="col-md-12">Note : The following vital information is the only reliable means of accessing interstate and international records. If avaliable, Please record
										   </div>
										    
										  </div>	
										  <div class="row">
										  <h4>Drivers License</h4>
										  </div>
									<div class="row" style="margin-bottom:1%;">
										   <div class="col-md-3">Drivers License/Other ID Number  </div>
										   <div class="col-md-3"><input autocorrect="off"  type="text" name="title" id="title" value="" class="form-control" placeholder="Drivers License/Other ID Number" required=""></div>
										    
										  </div>	
										<div class="row" style="margin-bottom:1%;">
										   <div class="col-md-3">State </div>
										   <div class="col-md-3"><input autocorrect="off"  type="text" name="State" id="State" value="" class="form-control" placeholder="State" required=""></div>
										    
										  </div>
											<div class="row" style="margin-bottom:1%;">
										   <div class="col-md-3">City  </div>
										   <div class="col-md-3"><input autocorrect="off"  type="text" name="title" id="title" value="" class="form-control" placeholder="City" required=""></div>
										    
										  </div>	
<div class="row">
										  <h4>Passport</h4>
										  </div>
									<div class="row" style="margin-bottom:1%;">
										   <div class="col-md-3">Passport Number</div>
										   <div class="col-md-3"><input autocorrect="off"  type="text" name="title" id="title" value="" class="form-control" placeholder="Passport Number" required=""></div>
										    
										  </div>	
										<div class="row" style="margin-bottom:1%;">
										   <div class="col-md-3">Country </div>
										   <div class="col-md-3"><input autocorrect="off"  type="text" name="State" id="State" value="" class="form-control" placeholder="Country" required=""></div>
										    
										  </div>
																					  
                                    </div><!-- panel-body -->
									 
												</div>
                                            
                                           </div>
										</div>
										<div class="panel-footer">
									 <div class="row" style="margin-bottom:1%;">
											  <div class="col-sm-2">
											 <button class="btn btn-primary mr5">Prev</button>
											  </div>
											  <div class="col-sm-9">
											   &nbsp;
											  </div>
											 
											  
											  <div class="col-sm-1">
											  <button class="btn btn-primary mr5">Next</button>
											  </div>
										 </div>
                                      
                                    </div><!-- panel-footer -->  
									</div>
								</form>
                                           
                      
                                            
                                        </div><!-- tab-pane -->
										 <div class="tab-pane" id="tab7-4">
                                            <div class="panel-body">
									<div class="col-md-12">
										
										
                                                
												<div class="row">
												 <div class="panel-body">
												 
										 <div class="row" style="margin-bottom:1%;">
											  
											  <div class="col-md-8">
											 <div class="col-md-3">Photos
										   </div>
										   <div class="col-md-8">
										   <input autocorrect="off"  type="file" name="photos">
										   </div>
											  </div>
											  	 
										
											 
										 </div>
										<div class="row">
                            <div class="col-md-6">
                               <div class="panel panel-default">
                                    <div class="panel-heading">                                        
                                        <h4 class="panel-title">Signature</h4>                                      
                                    </div><!-- panel-heading -->
                                    <div class="panel-body">
                                       <div id="canvas">
            <canvas class="roundCorners" id="newSignature" style="position: relative; margin: 0; padding: 0; border: 1px solid #c4caac;" width="276" height="180"></canvas>
        </div>
        <script>signatureCapture();</script>
        <button type="button" onclick="signatureSave()">Save signature</button>
        <button type="button" onclick="signatureClear()">Clear signature</button>
        <br>
        Saved Image
        <br>
        <img id="saveSignature" alt="Saved image png">
                                    </div><!-- btn-body -->
									
                                </div>
                                 <div class="panel-footer">
									 <div class="row" style="margin-bottom:1%;">
											  <div class="col-sm-2">
											 <button class="btn btn-primary mr5">Prev</button>
											  </div>
											  <div class="col-sm-9">
											   &nbsp;
											  </div>
											 
											  
											  <div class="col-sm-1">
											  <button class="btn btn-primary mr5">Next</button>
											  </div>
										 </div>
                                      
                                    </div><!-- panel-footer -->  
                            </div><!-- col-md-6 -->
                             
                        </div><!-- row -->
						
												</div>
                                            
                                           </div>
										</div>
									</div>
                      
                                            
                      
                                        </div><!-- tab-pane -->
                                    </div><!-- tab-content -->
                                                    
                                </form>
										<!-- BASIC WIZARD -->
                              
                            </div>
                             
                        </div><!-- row -->
						
												</div>
                                            
                                           </div>
										</div>
									</div>
				 
                                               
                    </div><!-- contentpanel -->
                </div><!-- mainpanel -->
            </div><!-- mainwrapper -->
        </section>

        <script src="js/jquery-1.11.1.min.js"></script>
        <script src="js/jquery-migrate-1.2.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/modernizr.min.js"></script>
        <script src="js/ppace.min.js"></script>

        <script src="js/jquery.cookies.js"></script>
        
        <script src="js/jquery.dataTables.min.js"></script>
        <script src="js/dataTables.bootstrap.js"></script>
        <script src="js/dataTables.responsive.js"></script>
        <script src="js/select2.min.js"></script>

        <script src="js/custom.js"></script>
        <script>
            jQuery(document).ready(function(){
                
                jQuery('#basicTable').DataTable({
                    responsive: true
                });
                
                var shTable = jQuery('#shTable').DataTable({
                    "fnDrawCallback": function(oSettings) {
                        jQuery('#shTable_paginate ul').addClass('pagination-active-dark');
                    },
                    responsive: true
                });
                
                // Show/Hide Columns Dropdown
                jQuery('#shCol').click(function(event){
                    event.stopPropagation();
                });
                
                jQuery('#shCol input').on('click', function() {

                    // Get the column API object
                    var column = shTable.column($(this).val());
 
                    // Toggle the visibility
                    if ($(this).is(':checked'))
                        column.visible(true);
                    else
                        column.visible(false);
                });
                
                var exRowTable = jQuery('#exRowTable').DataTable({
                    responsive: true,
                    "fnDrawCallback": function(oSettings) {
                        jQuery('#exRowTable_paginate ul').addClass('pagination-active-success');
                    },
                    "ajax": "ajax/objects.txt",
                    "columns": [
                        {
                            "class":          'details-control',
                            "orderable":      false,
                            "data":           null,
                            "defaultContent": ''
                        },
                        { "data": "name" },
                        { "data": "position" },
                        { "data": "office" },
                        { "data": "salary" }
                    ],
                    "order": [[1, 'asc']] 
                });
                
                // Add event listener for opening and closing details
                jQuery('#exRowTable tbody').on('click', 'td.details-control', function () {
                    var tr = $(this).closest('tr');
                    var row = exRowTable.row( tr );
             
                    if ( row.child.isShown() ) {
                        // This row is already open - close it
                        row.child.hide();
                        tr.removeClass('shown');
                    }
                    else {
                        // Open this row
                        row.child( format(row.data()) ).show();
                        tr.addClass('shown');
                    }
                });
               
                
                // DataTables Length to Select2
                jQuery('div.dataTables_length select').removeClass('form-control input-sm');
                jQuery('div.dataTables_length select').css({width: '60px'});
                jQuery('div.dataTables_length select').select2({
                    minimumResultsForSearch: -1
                });
    
            });
            
            function format (d) {
                // `d` is the original data object for the row
                return '<table class="table table-bordered nomargin">'+
                    '<tr>'+
                        '<td>Full name:</td>'+
                        '<td>'+d.name+'</td>'+
                    '</tr>'+
                    '<tr>'+
                        '<td>Extension number:</td>'+
                        '<td>'+d.extn+'</td>'+
                    '</tr>'+
                    '<tr>'+
                        '<td>Extra info:</td>'+
                        '<td>And any further details here (images etc)...</td>'+
                    '</tr>'+
                '</table>';
            }
        </script>
		
		<script>
            jQuery(document).ready(function(){
              
                // Basic Form
                jQuery("#basicForm").validate({
                    highlight: function(element) {
                        jQuery(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                    },
                    success: function(element) {
                        jQuery(element).closest('.form-group').removeClass('has-error');
                    }
                });
              
                // Error Message In One Container
                jQuery("#basicForm2").validate({
                    errorLabelContainer: jQuery("#basicForm2 div.errorForm")
                });
              
                // With Checkboxes and Radio Buttons
                
                jQuery('#genderError').attr('for','gender');
                jQuery('#intError').attr('for','int[]');
                
                jQuery("#basicForm3").validate({
                    highlight: function(element) {
                        jQuery(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                    },
                    success: function(element) {
                        jQuery(element).closest('.form-group').removeClass('has-error');
                    }
                });
                
                jQuery("#basicForm4").validate({
                    highlight: function(element) {
                        jQuery(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                    },
                    success: function(element) {
                        jQuery(element).closest('.form-group').removeClass('has-error');
                    },
                    ignore: null
                });
                
                // Validation with select boxes
                jQuery("#flowers, #fruits").select2({
                    minimumResultsForSearch: -1
                });
              
            });
        </script>
<script src="signature.js"></script>
		<script>
			function signatureCapture() {
	var canvas = document.getElementById("newSignature");
	var context = canvas.getContext("2d");
	canvas.width = 276;
	canvas.height = 180;
	context.fillStyle = "#fff";
	context.strokeStyle = "#444";
	context.lineWidth = 1.5;
	context.lineCap = "round";
	context.fillRect(0, 0, canvas.width, canvas.height);
	var disableSave = true;
	var pixels = [];
	var cpixels = [];
	var xyLast = {};
	var xyAddLast = {};
	var calculate = false;
	{ 	//functions
		function remove_event_listeners() {
			canvas.removeEventListener('mousemove', on_mousemove, false);
			canvas.removeEventListener('mouseup', on_mouseup, false);
			canvas.removeEventListener('touchmove', on_mousemove, false);
			canvas.removeEventListener('touchend', on_mouseup, false);

			document.body.removeEventListener('mouseup', on_mouseup, false);
			document.body.removeEventListener('touchend', on_mouseup, false);
		}

		function get_coords(e) {
			var x, y;

			if (e.changedTouches && e.changedTouches[0]) {
				var offsety = canvas.offsetTop || 0;
				var offsetx = canvas.offsetLeft || 0;

				x = e.changedTouches[0].pageX - offsetx;
				y = e.changedTouches[0].pageY - offsety;
			} else if (e.layerX || 0 == e.layerX) {
				x = e.layerX;
				y = e.layerY;
			} else if (e.offsetX || 0 == e.offsetX) {
				x = e.offsetX;
				y = e.offsetY;
			}

			return {
				x : x, y : y
			};
		};

		function on_mousedown(e) {
			e.preventDefault();
			e.stopPropagation();

			canvas.addEventListener('mouseup', on_mouseup, false);
			canvas.addEventListener('mousemove', on_mousemove, false);
			canvas.addEventListener('touchend', on_mouseup, false);
			canvas.addEventListener('touchmove', on_mousemove, false);
			document.body.addEventListener('mouseup', on_mouseup, false);
			document.body.addEventListener('touchend', on_mouseup, false);

			empty = false;
			var xy = get_coords(e);
			context.beginPath();
			pixels.push('moveStart');
			context.moveTo(xy.x, xy.y);
			pixels.push(xy.x, xy.y);
			xyLast = xy;
		};

		function on_mousemove(e, finish) {
			e.preventDefault();
			e.stopPropagation();

			var xy = get_coords(e);
			var xyAdd = {
				x : (xyLast.x + xy.x) / 2,
				y : (xyLast.y + xy.y) / 2
			};

			if (calculate) {
				var xLast = (xyAddLast.x + xyLast.x + xyAdd.x) / 3;
				var yLast = (xyAddLast.y + xyLast.y + xyAdd.y) / 3;
				pixels.push(xLast, yLast);
			} else {
				calculate = true;
			}

			context.quadraticCurveTo(xyLast.x, xyLast.y, xyAdd.x, xyAdd.y);
			pixels.push(xyAdd.x, xyAdd.y);
			context.stroke();
			context.beginPath();
			context.moveTo(xyAdd.x, xyAdd.y);
			xyAddLast = xyAdd;
			xyLast = xy;

		};

		function on_mouseup(e) {
			remove_event_listeners();
			disableSave = false;
			context.stroke();
			pixels.push('e');
			calculate = false;
		};
	}
	canvas.addEventListener('touchstart', on_mousedown, false);
	canvas.addEventListener('mousedown', on_mousedown, false);
}

function signatureSave() {
	var canvas = document.getElementById("newSignature");// save canvas image as data url (png format by default)
	var dataURL = canvas.toDataURL("image/png");
	document.getElementById("saveSignature").src = dataURL;
};

function signatureClear() {
	var canvas = document.getElementById("newSignature");
	var context = canvas.getContext("2d");
	context.clearRect(0, 0, canvas.width, canvas.height);
}
		</script>
		<script>
            jQuery(document).ready(function() {
                
                // This will empty first option in select to enable placeholder
                jQuery('select option:first-child').text('');
                
                // Select2
                jQuery("select").select2({
                    minimumResultsForSearch: -1
                });
                
                // Basic Wizard
                jQuery('#basicWizard').bootstrapWizard({
                    onTabShow: function(tab, navigation, index) {
                        tab.prevAll().addClass('done');
                        tab.nextAll().removeClass('done');
                        tab.removeClass('done');
                        
                        var $total = navigation.find('li').length;
                        var $current = index + 1;
                        
                        if($current >= $total) {
                            $('#basicWizard').find('.wizard .next').addClass('hide');
                            $('#basicWizard').find('.wizard .finish').removeClass('hide');
                        } else {
                            $('#basicWizard').find('.wizard .next').removeClass('hide');
                            $('#basicWizard').find('.wizard .finish').addClass('hide');
                        }
                    }
                });
                
                // Progress Wizard
                jQuery('#progressWizard').bootstrapWizard({
                    onTabShow: function(tab, navigation, index) {
                        tab.prevAll().addClass('done');
                        tab.nextAll().removeClass('done');
                        tab.removeClass('done');
                        
                        var $total = navigation.find('li').length;
                        var $current = index + 1;
                        
                        if($current >= $total) {
                            $('#progressWizard').find('.wizard .next').addClass('hide');
                            $('#progressWizard').find('.wizard .finish').removeClass('hide');
                        } else {
                            $('#progressWizard').find('.wizard .next').removeClass('hide');
                            $('#progressWizard').find('.wizard .finish').addClass('hide');
                        }
                        
                        var $percent = ($current/$total) * 100;
                        $('#progressWizard').find('.progress-bar').css('width', $percent+'%');
                    }
                });
                
                // Wizard With Disabled Tab Click
                jQuery('#tabWizard').bootstrapWizard({
                    onTabShow: function(tab, navigation, index) {
                        tab.prevAll().addClass('done');
                        tab.nextAll().removeClass('done');
                        tab.removeClass('done');
                        
                        var $total = navigation.find('li').length;
                        var $current = index + 1;
                        
                        if($current >= $total) {
                            $('#tabWizard').find('.wizard .next').addClass('hide');
                            $('#tabWizard').find('.wizard .finish').removeClass('hide');
                        } else {
                            $('#tabWizard').find('.wizard .next').removeClass('hide');
                            $('#tabWizard').find('.wizard .finish').addClass('hide');
                        }
                    },
                    onTabClick: function(tab, navigation, index) {
                        return false;
                    }
                });
                
                // Wizard With Form Validation
                jQuery('#valWizard').bootstrapWizard({
                    onTabShow: function(tab, navigation, index) {
                        tab.prevAll().addClass('done');
                        tab.nextAll().removeClass('done');
                        tab.removeClass('done');
                        
                        var $total = navigation.find('li').length;
                        var $current = index + 1;
                        
                        if($current >= $total) {
                            $('#valWizard').find('.wizard .next').addClass('hide');
                            $('#valWizard').find('.wizard .finish').removeClass('hide');
                        } else {
                            $('#valWizard').find('.wizard .next').removeClass('hide');
                            $('#valWizard').find('.wizard .finish').addClass('hide');
                        }
                    },
                    onTabClick: function(tab, navigation, index) {
                        return false;
                    },
                    onNext: function(tab, navigation, index) {
                        var $valid = jQuery('#valWizard').valid();
                        if (!$valid) {
                            $validator.focusInvalid();
                            return false;
                        }
                    }
                });
                
                // Wizard With Form Validation
                var $validator = jQuery("#valWizard").validate({
                    highlight: function(element) {
                        jQuery(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                    },
                    success: function(element) {
                        jQuery(element).closest('.form-group').removeClass('has-error');
                    }
                });
                
                
                // This will submit the basicWizard form
                jQuery('.panel-wizard').submit(function() {    
                    alert('This will submit the form wizard');
                    return false // remove this to submit to specified action url
                });

            });
        </script>
    </body>
</html>
