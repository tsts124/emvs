<?php
$degrees = $_POST['degrees'];
$pictureName = $_POST['pictureName'];
//this is the original file
$filename = 'uploads/' . $pictureName;

if (preg_match('/.png/', $pictureName)) {
    $source = imagecreatefrompng($filename);
    $rotate = imagerotate($source, $degrees, 0);
    //save the new image
    imagepng($rotate, $filename, 9);

    //free up the memory
    imagedestroy($source);
    //free up the memory
    imagedestroy($rotate);
} else {
    $source = imagecreatefromjpeg($filename);
    $rotate = imagerotate($source, $degrees, 0);

    imagejpeg($rotate, $filename, 100);

    imagedestroy($source);
    imagedestroy($rotate);
}

