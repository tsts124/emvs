<?php
session_start();
include 'includes/dbcon.php';
$muser = $_SESSION['user'];
$sitesvisid = $_POST['sitesvisid'];
$sitesmemid = $_POST['sitesmemid'];
$id = $_POST['id'];

$required_fields_query = $dbh->prepare("
	SELECT * 
	FROM 
		`required_fields` 
	WHERE deleted = '0'
");

$required_fields_query->execute();
$required_fields = $required_fields_query->fetchAll();

function findAssignment($required_fields, $tab, $assignment)
{

    $isRequired = false;
    foreach ($required_fields as $row) {
        if ($row['tab'] == $tab && $row['assignment'] == $assignment) {
            if ($row['active'] == 1)
                $isRequired = true;
            break;
        }
    }
    return $isRequired;
}

$managersql = $dbh->prepare("select * from newmember where emailid ='$muser' && activity ='1' && `Delete` ='0'");
$managersql->execute();
$managerdata = $managersql->fetch();
#echo $managerdata['roles'];
$sql2 = $dbh->prepare("select * from `sites` where `Sites` ='$id' AND `Delete` = '0'");
$sql2->execute();
$data2 = $sql2->fetch();
?>
<input autocorrect="off"  name="sitesvisid" id="sitesvisid" value="<?= $sitesvisid; ?>" type="hidden">
<input autocorrect="off"  name="sitesmemid" id="sitesmemid" value="<?= $sitesmemid; ?>" type="hidden">
<div class="form-group">
    <label class="col-sm-3 control-label">Visits days</label>
    <div class="col-sm-9" style="padding: 0;">

        <input autocorrect="off"  name="visitdays" id="visitdays" class="form-control" type="text" value="<?= $data2['visitsday']; ?>"
               style="width:60%;margin-bottom: 1%;" readonly>


    </div>
</div><!-- form-group -->
<div class="form-group">
    <label class="col-sm-3 control-label"
           style="<?php
           if (findAssignment($required_fields, 'visits', 'visitmanager')) {
               echo 'color:#0066FF;';
           }
           ?>">Manager</label>
    <div class="col-sm-9" style="padding: 0;">


        <?php
        #echo $managerdata['roles'];
        $siteId = $data2['Id'];
        if ($managerdata['roles'] == 'Manager') {
            $sql = $dbh->prepare("select * from newmember where Sites ='$siteId' && activity ='1' && `Delete` ='0'");
            $sql->execute();
            $data = $sql->fetch(); ?>
            <input autocorrect="off"  name="managerid" class="form-control" type="hidden"
                   value="<?= $data2['Id']; ?>"
                   style="width:60%;margin-bottom: 1%;" readonly>
            <?php
            $sql1 = $dbh->prepare("select * from visitstable where collectorsid ='$sitesmemid' and visitid='$sitesvisid'");
            $sql1->execute();
            $data1 = $sql1->fetch();
            $s = $data1['rabbis']; ?>
            <select <?php
            if (findAssignment($required_fields, 'visits', 'visitmanager')) {
                echo ' required ';
            }
            ?>
                data-placeholder="Choose One"
                name="visitmanager"
                id="visitmanager"
                class="form-control"
                style="width:60%;margin-bottom: 1%;padding:10px;">
                <option value="<?= $managerdata['Lastname'] . ' ' . $managerdata['Firstname']; ?>"
                        selected><?= $managerdata['Lastname'] . ' ' . $managerdata['Firstname']; ?></option>
                <?php
                $sql = $dbh->prepare("select * from newmember where Sites ='$siteId' && activity ='1' && `Delete` ='0'");
                $sql->execute();

                while ($data2 = $sql->fetch()) {
                    if ($managerdata['Id'] == $data2['Id'] || $data2['Lastname'] . ' ' . $data2['Firstname'] == $s) {
                        ?>
                    <?php } else {
                        ?>
                        <option
                            value="<?= $data2['Lastname'] . ' ' . $data2['Firstname']; ?>"><?= $data2['Lastname'] . ' ' . $data2['Firstname']; ?></option>
                    <?php }
                } ?>
            </select>

        <?php } else { ?>
            <input autocorrect="off"  name="managerid" class="form-control" type="hidden" value="<?= $data2['Id']; ?>"
                   style="width:60%;margin-bottom: 1%;" readonly>
            <?php
            $sql1 = $dbh->prepare("select * from visitstable where collectorsid ='$sitesmemid' and visitid='$sitesvisid'");
            $sql1->execute();
            $data1 = $sql1->fetch();
            $s = $data1['rabbis'];
            ?>
            <select <?php
            if (findAssignment($required_fields, 'visits', 'visitmanager')) {
                echo ' required ';
            }
            ?> data-placeholder="Choose One" name="visitmanager" id="visitmanager"
               class="form-control" style="width:60%;margin-bottom: 1%;padding:10px;">
                <option value="" disabled selected>Choose One</option>
                <?php
                $sql = $dbh->prepare('SELECT *
                                      FROM `newmember`
                                      WHERE `Sites` = :sites
                                      AND `activity` = :activity
                                      AND `Delete` = deleted ; ');
                $sql->execute(['sites' => $siteId, 'activity' => 1, 'deleted' => 0]);
                while ($data2 = $sql->fetch()) {
                    if ($managerdata['Id'] == $data2['Id'] || $data2['Lastname'] . ' ' . $data2['Firstname'] == $s) {
                        ?>
                        <option selected>
                            <?= $data2['Lastname'] . ' ' . $data2['Firstname']; ?>
                        </option>
                    <?php } else {
                        ?>
                        <option>
                            <?= $data2['Lastname'] . ' ' . $data2['Firstname']; ?>
                        </option>

                    <?php }
                } ?>
            </select>
        <?php } ?>

    </div>
</div><!-- form-group -->
<div class="form-group">
    <label class="col-sm-3 control-label" style="<?php
    if (findAssignment($required_fields, 'visits', 'visithost')) {
        echo 'color:#0066FF;';
    }
    ?>">Host</label>
    <div class="col-sm-9" style="padding: 0;">
        <?php
        $sql1 = $dbh->prepare("select * from visitstable where collectorsid ='$sitesmemid' and visitid='$sitesvisid'");
        $sql1->execute();
        $data1 = $sql1->fetch();
        $s = $data6['host'];
        if ($managerdata['roles'] == 'Manager') { ?>
            <select <?php
            if (findAssignment($required_fields, 'visits', 'visithost')) {
                echo ' required ';
            }
            ?> data-placeholder="Choose One" name="visithost" id="visithost" class="form-control"
               style="width:60%;margin-bottom: 1%;padding:10px;">

                <option value="<?php if ($data1['host'] != '') {
                    echo $data1['host'];
                } else {
                    echo '';
                } ?>" <?php if ($data1['host'] == '') {
                    echo 'disabled selected';
                } ?>><?php if ($data1['host'] != '') {
                        echo $data1['host'];
                    } else {
                        echo 'Choose One';
                    } ?>
                </option>
                <?php

                $sql2 = $dbh->prepare("select * from hosts where Sites ='$siteId' && Active ='1' && `Delete` ='0' && `Firstname`!='$s'");
                $sql2->execute();
                while ($data2 = $sql2->fetch()) {
                    if ($data6['host'] == $data2['Lastname'] . ' ' . $data2['Firstname']) {
                        ?>
                        <option selected
                                value="<?= $data2['Lastname'] . ' ' . $data2['Firstname']; ?>"><?= $data2['Lastname'] . ' ' . $data2['Firstname']; ?></option>
                    <?php } else {
                        ?>
                        <option
                            value="<?= $data2['Lastname'] . ' ' . $data2['Firstname']; ?>"><?= $data2['Lastname'] . ' ' . $data2['Firstname']; ?></option>
                    <?php }
                } ?>
            </select>
        <?php } else { ?>
            <select <?php
            if (findAssignment($required_fields, 'visits', 'visithost')) {
                echo ' required ';
            }
            ?> data-placeholder="Choose One" name="visithost" id="visithost" class="form-control"
               style="width:60%;margin-bottom: 1%;padding:10px;">
                <option value="" disabled selected>Choose One</option>

                <?php

                $sql1 = $dbh->prepare("select * from hosts where Sites ='$siteId' && Active ='1' && `Delete` ='0'");
                $sql1->execute();
                while ($data1 = $sql1->fetch()) {
                    if ($data6['host'] == $data2['Lastname'] . ' ' . $data2['Firstname']) {
                        ?>
                        <option selected>
                            <?= $data2['Lastname'] . ' ' . $data2['Firstname']; ?>
                        </option>
                    <?php } else {
                        ?>
                        <option>
                            <?= $data1['Lastname'] . ' ' . $data1['Firstname']; ?>
                        </option>
                    <?php }
                } ?>
            </select>
        <?php } ?>

        <!--Free formed host name-->
        <input autocorrect="off"  class="form-control" name="free_formed_host" style="width:60%; margin-bottom: 1%;"
               value="<?php if ($data1['free_formed_host']) {
                   echo $data1['free_formed_host'];
               } ?>"/>
        <!--Free formed host comment-->
        <textarea autocorrect="off"  class="form-control"
                  name="free_formed_host_comment"
                  rows="3" style="width:60%;"><?php if ($data1['free_formed_host_comment']) {
                echo $data1['free_formed_host_comment'];
            } else {
                echo '';
            } ?>
                                                    </textarea>
    </div>
</div><!-- form-group -->
<div class="form-group">
    <label class="col-sm-3 control-label" <?php
    if (findAssignment($required_fields, 'visits', 'visitdriver'))
        echo " style='color:#0066FF;' ";
    ?>>Driver</label>

    <div class="col-sm-9" style="padding: 0;">
        <?php
        $sql3 = $dbh->prepare("select * from visitstable where collectorsid ='$sitesmemid' and visitid='$sitesvisid'");
        $sql3->execute();
        $data3 = $sql3->fetch();
        $s = $data3['driver'];
        if ($managerdata['roles'] == 'Manager') {
            $driver = str_ireplace(':', '', strstr($data1['driver'], ':'));
            ?>

            <select <?php
            if (findAssignment($required_fields, 'visits', 'visitdriver')) {
                echo ' required ';
            }
            ?> data-placeholder="" name="visitdriver" id="visitdriver" class="form-control"
               style="width:60%;margin-bottom: 1%;">

                <option value="<?php if ($data1['driver'] != '') {
                    echo $data1['driver'];
                } else {
                    echo '';
                } ?>" <?php if ($data1['driver'] == '') {
                    echo 'disabled selected';
                } ?>><?php if ($data1['driver'] != '') {
                        echo $driver;
                    } else {
                        echo 'Choose One';
                    } ?></option>
                <option <?php if ($data3['driver'] == 'Self-Drive') {
                    echo 'selected ';
                } ?> value="Self-Drive">Self-Drive
                </option>
                <?php
                $sql2 = $dbh->prepare("select * from drivers where Sites ='$siteId' && Active ='1' && `Delete` ='0' && `Firstname`!='$s'");
                $sql2->execute();
                while ($data2 = $sql2->fetch()) {
                    if ($data3['driver'] == $data1['Id'] . ':' . $data1['Lastname'] . ' ' . $data1['Firstname']) {
                        ?>
                        <option selected
                                value="<?= $data1['Id'] . ':' . $data1['Lastname'] . ' ' . $data1['Firstname']; ?>"><?= $data1['Lastname'] . '&nbsp;' . $data1['Firstname']; ?></option>
                    <?php } else {
                        ?>
                        <option
                            value="<?= $data2['Id'] . ':' . $data2['Lastname'] . ' ' . $data2['Firstname']; ?>"><?= $data2['Lastname'] . '&nbsp;' . $data2['Firstname']; ?></option>
                    <?php }
                } ?>
            </select>
        <?php } else { ?>

            <select <?php
            if (findAssignment($required_fields, 'visits', 'visitdriver')) {
                echo ' required ';
            } ?> name="visitdriver" id="visitdriver" class="form-control"
                 style="width:60%;margin-bottom: 1%;">
                <option disabled selected>Choose One</option>
                <option <?php if ($data3['driver'] == 'Self-Drive') {
                    echo 'selected ';
                } ?> value="Self-Drive">Self-Drive
                </option>
                <?php
                $sql1 = $dbh->prepare("select * from drivers where Sites ='$siteId' && Active ='1' && `Delete` ='0'");
                $sql1->execute();
                while ($data1 = $sql1->fetch()) {
                    if ($data3['driver'] == $data1['Id'] . ':' . $data1['Lastname'] . ' ' . $data1['Firstname']) {
                        ?>
                        <option selected
                                value="<?= $data1['Id'] . ':' . $data1['Lastname'] . ' ' . $data1['Firstname']; ?>"><?= $data1['Lastname'] . '&nbsp;' . $data1['Firstname']; ?></option>
                    <?php } else {
                        ?>

                        <option
                            value="<?= $data1['Id'] . ':' . $data1['Lastname'] . ' ' . $data1['Firstname']; ?>"><?= $data1['Lastname'] . '&nbsp;' . $data1['Firstname']; ?></option>

                    <?php }
                } ?>
            </select>

        <?php } ?>
        <!--Free formed driver name-->
        <input autocorrect="off"  class="form-control" name="free_formed_driver" style="width:60%; margin-bottom: 1%;"
               value="<?php if ($data3['free_formed_driver']) {
                   echo $data3['free_formed_driver'];
               } ?>"/>
        <!--Free formed driver comment-->
        <textarea autocorrect="off"  class="form-control"
                  name="free_formed_driver_comment"
                  rows="3"
                  style="width:60%;"><?php if ($data3['free_formed_driver_comment']) {
                echo $data3['free_formed_driver_comment'];
            } else {
                echo '';
            } ?>
													</textarea>
    </div>
</div><!-- form-group -->
