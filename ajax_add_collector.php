<?php
error_reporting(0);
include __DIR__ . '/includes/dbcon.php';

/**
 * @param $year
 * @param null $day
 * @param null $month
 * @return null|string
 */
function createDatefromDB($year, $day = null, $month = null)
{
    $dob = null;

    if ($year && $day && empty($month)) {
        $dob = $day . '-' . $year;
    } elseif ($year && $month && empty($day)) {
        $dob = $month . '- -' . $year;
    } elseif ($year && $day && $month) {
        $dob = $month . '-' . $day . '-' . $year;
    }

    return $dob ? $dob : $year;
}

if ($_POST['requiredFields']) {
    $title = $_POST['title'];
    $firstName = addslashes(trim(ucwords($_POST['firstName'])));
    $lastName = addslashes(trim(ucwords($_POST['lastName'])));
    $memberids = addslashes(trim(ucwords($_POST['memberids'])));
    $city = $_POST['city'];
    $country = $_POST['country'];
    $user = $_POST['user'];
    $birthMonth = $_POST['mont'];
    $birthDay = $_POST['day'];
    $birthYear = $_POST['year'];
    $dob = createDatefromDB($birthYear,$birthDay,$birthMonth);

    $alertcheck = 0;
    $state = '';
    $zipcode = '';
    $address1 = '';
    $address2 = '';
    $personalcomments = '';


    $sql = $dbh->prepare("select * from collectors where Id = '$memberids'");
    $sql->execute();
    $collectors = $sql->fetchAll();

    $sql1 = $dbh->prepare("select * from collectors where Id != '$memberids' `title` = '$title' AND `firstname` = '$firstName' AND `lastname` = '$lastName' AND dob LIKE '%$birthYear'");
    $sql1->execute();
    $unique = $sql1->fetchAll();

    if ($unique){
        echo 'User already exists';
        exit;
    }

    if (!$collectors) {
        $count = $dbh->prepare('INSERT INTO `collectors` (`Id`,
                                                      `user`,
                                                      `title`,
                                                      `firstname`,
                                                      `middlename`,
                                                      `lastname`,
                                                      `city`,
                                                      `statecountry`,
                                                      `country`,
                                                      `zipcode`,
                                                      `address1`,
                                                      `address2`,
                                                      `phoneno`,
                                                      `gender`,
                                                      `personalcomments`,
                                                      `dob`,
                                                      `count`,
                                                      `Addeddate`,
                                                      `Updateddate`,
                                                      `alertcheck`) 
					            VALUES (:id,
					                    :userid,
					                    :title,
					                    :firstname,
					                    :middlename,
					                    :lastname,
					                    :city,
					                    :state,
					                    :country,
					                    :zip,
					                    :addr1,
					                    :addr2,
					                    :phoneno,
					                    :gender,
					                    :personalcomments,
					                    :dob,
					                    :count,
					                    now(),
					                    now(),
					                    :alertcheck)');
        $check = $count->execute([
            'id' => $memberids,
            'userid' => $user,
            'title' => $title,
            'firstname' => $firstName,
            'middlename' => '',
            'lastname' => $lastName,
            'city' => $city,
            'state' => $state,
            'country' => $country,
            'zip' => $zipcode,
            'addr1' => $address1,
            'addr2' => $address2,
            'phoneno' => '',
            'gender' => '',
            'personalcomments' => $personalcomments,
            'dob' => $dob,
            'count' => '',
            'alertcheck' => $alertcheck
        ]);

        if ($check) {
            echo 'Collector has been created';
        } else {
            echo 'Collector was not created';
        }

    } else {
        $title = addslashes($title);
        $check = $dbh->exec("UPDATE collectors SET `dob`='$dob', `title`='$title', `firstname`='$firstName', `lastname`='$lastName' WHERE `id`='$memberids'");

        if ($check){
            echo 'Сollector data updated';
        } else {
            echo 'Сollector data not updated';
        }
    }
}

if ($_POST['updateField']) {

    $field = $_POST['field'];
    $value = addslashes(trim(ucwords($_POST['value'])));
    if ($field == 'title') {
        $value = $_POST['value'];
    }

        $memberids = addslashes(trim(ucwords($_POST['memberids'])));

    $check = $dbh->exec("UPDATE collectors SET `$field`='$value' WHERE Id='$memberids'");

    if ($check) {
        echo 'Сollector data updated';
    } else {
        echo 'Сollector data not updated';
    }
}

if ($_POST['identification']) {

    $fieldAlias = [
        'Collidenti[]' => 'Collidenti',
        'Primary1' => 'Primary1',
        'Collcountry[]' => 'Collcountry',
        'Collphone[]' => 'Collphone',
        'passport_name[]' => 'passport_name'

    ];

    $collident = $_POST['collident'];
    $primaryCount = $_POST['primaryCount'];
    $field = $fieldAlias[$_POST['field']];
    $value = addslashes(trim(ucwords($_POST['value'])));
    $memberids = addslashes(trim(ucwords($_POST['memberids'])));


    $sql = $dbh->prepare("select * from collident where Id = '$collident'");
    $sql->execute();
    $collidents = $sql->fetchAll();

    if ($collidents) {

        $check = $dbh->exec("UPDATE collident SET `$field`='$value' WHERE Id='$collident'");

        if ($check) {
            echo 'Identification data updated';
        } else {
            echo 'Identification data not updated';
        }

    } else {

        $defaultFields = [
            'id' => $collident,
            'Collid' => $memberids,
            'PrimaryCount' => '',
            'Primary1' => $collident == $primaryCount ? $primaryCount : '',
            'Collidenti' => '',
            'Collcountry' => '',
            'Collphone' => '',
            'Countcolldoc' => 0,
            'Colldoc' => '',
            'Refdetails' => '',
            'passport_name' => ''
        ];
        $defaultFields[$field] = $value;

        $count = $dbh->prepare('INSERT INTO `collident` (
                                                      `Id`,
                                                      `Collid`,
                                                      `PrimaryCount`,
                                                      `Primary1`,
                                                      `Collidenti`,
                                                      `Collcountry`,
                                                      `Collphone`,
                                                      `Countcolldoc`,
                                                      `Colldoc`,
                                                      `Refdetails`,
                                                      `Addeddate`,
                                                      `delete`,
                                                      `passport_name`) 
					            VALUES (:id,
					                    :Collid,
					                    :PrimaryCount,
					                    :Primary1,
					                    :Collidenti,
					                    :Collcountry,
					                    :Collphone,
					                    :Countcolldoc,
					                    :Colldoc,
					                    :Refdetails,
					                    now(),
					                    0,
					                    :passport_name)');
        $check = $count->execute($defaultFields);

        if ($check) {
            echo 'Identification has been created';
        } else {
            echo 'Identification was not created';
        }
    }
}

if ($_POST['deleteField']) {
    $collident = $_POST['collident'];
    $check = $dbh->exec("UPDATE collident SET `delete`='1' WHERE Id='$collident'");

    if ($check) {
        echo 'Identification has been deleted';
    } else {
        echo 'Identification was not deleted';
    }
}

if ($_POST['collectorphone']) {

    $fieldAlias = [
        'Phone_Type[]' => 'Phone_Type',
        'Phone_Country[]' => 'Phone_Country',
        'Phone_Number[]' => 'Phone_Number',
        'CPrimary' => 'CPrimary'

    ];

    $collectorphoneid = $_POST['collectorphoneid'];
    $cPrimary = $_POST['CPrimary'];
    $field = $fieldAlias[$_POST['field']];
    $value = addslashes(trim(ucwords($_POST['value'])));
    $memberids = addslashes(trim(ucwords($_POST['memberids'])));

    $sql = $dbh->prepare("select * from collectorphone where Id = '$collectorphoneid'");
    $sql->execute();
    $collectorphones = $sql->fetchAll();

    if ($field === 'Phone_Number') {

        $query = $dbh->prepare("SELECT * FROM `visit_details` WHERE `detail_number` = $value AND `detail_type` = `contact` AND `delete` = 0 ;");
        $query->execute();
        $unique1 = $query->fetchAll();

        $query = $dbh->prepare("SELECT * FROM `collectorphone` WHERE `Phone_Number` = $value AND `delete` = 0 ;");
        $query->execute();
        $unique2 = $query->fetchAll();

        if ($unique2 || $unique1) {
            echo 'Number already exists';
            exit;
        }
    }

    if ($collectorphones) {

        $check = $dbh->exec("UPDATE collectorphone SET `$field`='$value' WHERE Id='$collectorphoneid'");

        if ($check) {
            echo 'Contact data updated';
        } else {
            echo 'Contact data not updated';
        }

    } else {

        $defaultFields = [
            'id' => $collectorphoneid,
            'Collid' => $memberids,
            'PrimaryCount' => $collectorphoneid,
            'CPrimary' => $collectorphoneid == $cPrimary ? $cPrimary : '',
            'Phone_Type' => $_POST['phoneTypeValue'],
            'Phone_Number' => '',
            'Phone_Country' => ''
        ];
        $defaultFields[$field] = $value;

        $count = $dbh->prepare('INSERT INTO `collectorphone` (
                                                      `id`,
                                                      `Collid`,
                                                      `PrimaryCount`,
                                                      `CPrimary`,
                                                      `Phone_Type`,
                                                      `Phone_Number`,
                                                      `Phone_Country`,
                                                      `delete`) 
					            VALUES (:id,
					                    :Collid,
					                    :PrimaryCount,
					                    :CPrimary,
					                    :Phone_Type,
					                    :Phone_Number,
					                    :Phone_Country,
					                    0)');
        $check = $count->execute($defaultFields);

        if ($check) {
            echo 'Contact has been created';
        } else {
            echo 'Contact was not created';
        }
    }
}

if ($_POST['deleteFieldPhone']) {
    $collectorphoneid = $_POST['collectorphoneid'];
    $check = $dbh->exec("UPDATE collectorphone SET `delete`='1' WHERE Id='$collectorphoneid'");

    if ($check) {
        echo 'Contact has been deleted';
    } else {
        echo 'Contact was not deleted';
    }
}

