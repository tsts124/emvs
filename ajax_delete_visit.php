<?php
include 'includes/dbcon.php';

$id = $_POST['id'];

$sql = $dbh->prepare("SELECT * FROM `visitstable` WHERE `Id` = '$id';");
$sql->execute();
$data = $sql->fetch();

if(file_exists('uploads/' . $data['signname'])){
    unlink('uploads/' . $data['signname']);
}
if(file_exists('uploads/' . $data['photoname'])){
    unlink('uploads/' . $data['photoname']);
}

$sql = $dbh->prepare("DELETE FROM `visitstable` WHERE `Id` = '$id';");
$sql->execute();

$sql = $dbh->prepare('DELETE FROM `visit_details`
                      WHERE `visit_id` = :id ;');
$sql->execute(['id' => $id]);
