function Maxval(val, limit, id) {
    var $countErr = $('#counter' + id);
    if (val.length >= limit) {
        document.getElementById(id).value = val.substring(0, limit);
        event.preventDefault();
        $('#' + id).css('border-color', 'red');
        $countErr.html('Max ' + limit + ' characters');
        $countErr.css('color', 'red');

        setTimeout(function () {
            $('#' + id).css('border-color', '#ccc');
            $countErr.html(' ');
        }, 3000);
    } else if (val.length < limit) {
        $('#' + id).css('border-color', '#ccc');
        $countErr.html(' ');
    }
}
