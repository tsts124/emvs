$(document).ready(function () {
    //Birthday select
    var $month = $('#month');
    var $day = $('#day');

    var daysCount = 31;
    $month.on('change', function () {
        switch ($(this).val()) {
            case '2':
                daysCount = 29;
                break;
            case '4':
            case '6':
            case '9':
            case '11':
                daysCount = 30;
                break;
            //Jan, Mar, May, Jul, Aug, Oct, Dec
            default:
                daysCount = 31;
        }

        var lastOptionValue = $day.find('option:last').val();
        var difference;
        if (lastOptionValue > daysCount) {
            difference = lastOptionValue - daysCount;

            for (var i = difference; i > 0; i--) {
                $day.find('option:last').remove();
            }
        } else if (lastOptionValue < daysCount) {
            difference = daysCount - lastOptionValue;
            for (i = difference; i > 0; i--) {
                var newDay = +$day.find('option:last').val() + 1;

                $day.append('<option>' + newDay + '</option>');
            }
        }
    });

    $('.birthDateSave').on('click touch', function (event) {
        event.preventDefault();
        birthDateChange();
    });
});

function rigthFormatDate(int) {
    if (parseInt(int) < 10) {
        return '0' + int;
    }
    return int;
}

function birthDateChange() {
    var month = $('#month').val();
    var day = $('#day').val();
    var year = $('#year').val();
    var collectorId = $('#a').val();
    var visitId = $('#sitesvisid').val();
    var $pageName = $('span[class="two"]');
    var dob = '';

    if (year !== '0' && day !== '0' && month === '0') {
        dob = rigthFormatDate(day) + '/' + year.substring(2);
    } else if (year !== '0' && (day !== '0') && (month !== '0')) {
        dob = rigthFormatDate(month) + '/' + rigthFormatDate(day) + '/' + year.substring(2);
    } else if (year !== '0' && (day === '0') && (month !== '0')) {
        dob = rigthFormatDate(month) + '/' + 0 + '/' + year.substring(2);
    } else {
        dob = year;
    }

    var str = $pageName.html();
    var res = str.replace(/Birthdate: ([0-9]+\/[0-9]+\/[0-9]+|[0-9]+|)/, 'Birthdate: ' + dob);
    $pageName.html('');
    $pageName.html(res);

    var data = 'month=' + month + '&day=' + day + '&year=' + year +
        '&collectorId=' + collectorId + '&visitId=' + visitId;
    $.ajax({
        data: data,
        url: 'birthDateSave.php',
        method: 'POST'
    });
}
