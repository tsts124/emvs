(function ($) {
    $.fn.limit = function (options) {
        var defaults = {
            limit: 200,
            id_result: false,
            alertClass: false
        };
        options = $.extend(defaults, options);
        return this.each(function () {
            var remaining = options.limit - $(this).val().length;

            if (options.id_result) {
                $('#' + options.id_result).append('You have <strong>' + remaining + '</strong> characters remaining');
            }
            $(this).keyup(function () {
                if ($(this).val().length > options.limit) {
                    $(this).val($(this).val().substr(0, options.limit));
                }
                if (options.id_result) {
                    var remaining = options.limit - $(this).val().length;

                    $('#' + options.id_result).html('You have <strong>' + remaining + '</strong> characters remaining');
                    if (remaining <= 10) {
                        $('#' + options.id_result).addClass(options.alertClass);
                    }
                    else {
                        $('#' + options.id_result).removeClass(options.alertClass);
                    }
                }
            });
        });
    };
})(jQuery);
