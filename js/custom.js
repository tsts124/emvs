jQuery(document).ready(function () {

    "use strict";

    // Tooltip
    jQuery('.tooltips').tooltip({container: 'body'});

    // Popover
    jQuery('.popovers').popover();

    // Show panel buttons when hovering panel heading
    jQuery('.panel-heading').hover(function () {
        jQuery(this).find('.panel-btns').fadeIn('fast');
    }, function () {
        jQuery(this).find('.panel-btns').fadeOut('fast');
    });

    // Close Panel
    jQuery('.panel .panel-close').click(function () {
        jQuery(this).closest('.panel').fadeOut(200);
        return false;
    });

    // Minimize Panel
    jQuery('.panel .panel-minimize').click(function () {
        var t = jQuery(this);
        var p = t.closest('.panel');
        if (!jQuery(this).hasClass('maximize')) {
            p.find('.panel-body, .panel-footer').slideUp(200);
            t.addClass('maximize');
            t.find('i').removeClass('fa-minus').addClass('fa-plus');
            jQuery(this).attr('data-original-title', 'Maximize Panel').tooltip();
        } else {
            p.find('.panel-body, .panel-footer').slideDown(200);
            t.removeClass('maximize');
            t.find('i').removeClass('fa-plus').addClass('fa-minus');
            jQuery(this).attr('data-original-title', 'Minimize Panel').tooltip();
        }
        return false;
    });

    jQuery('.leftpanel .nav .parent > a').click(function () {


        var coll = jQuery(this).parents('.collapsed').length;

        if (!coll) {
            jQuery('.leftpanel .nav .parent-focus').each(function () {

                jQuery(this).find('.children').slideUp('fast');
                jQuery(this).removeClass('parent-focus');
            });

            var child = jQuery(this).parent().find('.children');
            if (!child.is(':visible')) {

                child.slideDown('fast');
                if (!child.parent().hasClass('active'))
                    child.parent().addClass('parent-focus');
            } else {

                var child2 = jQuery(this).parent().find('.children2');
                child2.slideUp('fast');
                child2.parent().removeClass('parent2-focus');

                child.slideUp('fast');
                child.parent().removeClass('parent-focus');
            }


        }
        return false;
    });

    jQuery('.leftpanel .nav .parent2 > a').click(function () {


        var coll = jQuery(this).parents('.collapsed').length;

        if (!coll) {
            //alert("!");
            jQuery('.leftpanel .nav .parent2-focus').each(function () {
                jQuery(this).find('.children2').slideUp('fast');
                jQuery(this).removeClass('parent2-focus');
            });

            var child = jQuery(this).parent().find('.children2');
            if (!child.is(':visible')) {
                child.slideDown('fast');
                if (!child.parent().hasClass('active2'))
                    child.parent().addClass('parent2-focus');
            } else {
                child.slideUp('fast');
                child.parent().removeClass('parent2-focus');
            }
        }
        return false;
    });


    // Menu Toggle
    jQuery('.menu-collapse').click(function () {
        var $menu = $('.headerwrapper, .mainwrapper');
        var $body = $('body');
        if (!$body.hasClass('hidden-left')) {
            if ($('.headerwrapper').hasClass('collapsed')) {
                $menu.removeClass('collapsed');
            } else {
                $menu.addClass('collapsed');
                $('.children').hide(); // hide sub-menu if leave open
                $('.children2').hide(); // hide sub-menu if leave open
            }
        } else {
            if (!$body.hasClass('show-left')) {
                $body.addClass('show-left');
            } else {
                $body.removeClass('show-left');
            }
        }
        return false;
    });

    // Add class nav-hover to mene. Useful for viewing sub-menu
    jQuery('.leftpanel .nav li').hover(function () {
        $(this).addClass('nav-hover');
    }, function () {
        $(this).removeClass('nav-hover');
    });

    // For Media Queries
    jQuery(window).resize(function () {
        hideMenu();
    });

    hideMenu(); // for loading/refreshing the page
    function hideMenu() {
        var $body = $('body');
        if ($('.header-right').css('position') === 'relative') {
            $body.addClass('hidden-left');
            $('.headerwrapper, .mainwrapper').removeClass('collapsed');
        } else {
            $body.removeClass('hidden-left');
        }

        // Seach form move to left
        var $form = $('.form-search');
        if ($(window).width() <= 360 && $('.leftpanel .form-search').length === 0) {
            $form.insertAfter($('.profile-left'));
        } else if ($('.header-right .form-search').length === 0 && $form.length) {
            $form.insertBefore($('.btn-group-notification'));
        }
    }

    collapsedMenu(); // for loading/refreshing the page
    function collapsedMenu() {
        var $menu = $('.headerwrapper, .mainwrapper');
        if ($('.logo').css('position') === 'relative') {
            $menu.addClass('collapsed');
        } else {
            $menu.removeClass('collapsed');
        }
    }

    $(window).resize(function () {
        var windowWidth = $(window).width();
        var $element = $('.headerwrapper, .mainwrapper');
        if (windowWidth < 975) {
            $element.addClass('collapsed');
        } else if (windowWidth > 1065) {
            $element.removeClass('collapsed');
        }
    });

    $('input[name=emailid]').focus();

});